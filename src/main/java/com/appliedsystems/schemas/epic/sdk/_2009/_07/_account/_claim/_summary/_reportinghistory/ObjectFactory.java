
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClaimCodeItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_reportinghistory/", "ClaimCodeItem");
    private final static QName _ClaimCodeItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_reportinghistory/", "ClaimCodeItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClaimCodeItems }
     * 
     */
    public ClaimCodeItems createClaimCodeItems() {
        return new ClaimCodeItems();
    }

    /**
     * Create an instance of {@link ClaimCodeItem }
     * 
     */
    public ClaimCodeItem createClaimCodeItem() {
        return new ClaimCodeItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClaimCodeItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_reportinghistory/", name = "ClaimCodeItem")
    public JAXBElement<ClaimCodeItem> createClaimCodeItem(ClaimCodeItem value) {
        return new JAXBElement<ClaimCodeItem>(_ClaimCodeItem_QNAME, ClaimCodeItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClaimCodeItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_reportinghistory/", name = "ClaimCodeItems")
    public JAXBElement<ClaimCodeItems> createClaimCodeItems(ClaimCodeItems value) {
        return new JAXBElement<ClaimCodeItems>(_ClaimCodeItems_QNAME, ClaimCodeItems.class, null, value);
    }

}
