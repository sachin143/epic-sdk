
package com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OptionTypes_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_optiontype/", "OptionTypes");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_optiontype/", name = "OptionTypes")
    public JAXBElement<OptionTypes> createOptionTypes(OptionTypes value) {
        return new JAXBElement<OptionTypes>(_OptionTypes_QNAME, OptionTypes.class, null, value);
    }

}
