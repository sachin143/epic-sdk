
package com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityCodeGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityCodeGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ActivityCodeID"/>
 *     &lt;enumeration value="ActivityCode"/>
 *     &lt;enumeration value="All"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityCodeGetType")
@XmlEnum
public enum ActivityCodeGetType {

    @XmlEnumValue("ActivityCodeID")
    ACTIVITY_CODE_ID("ActivityCodeID"),
    @XmlEnumValue("ActivityCode")
    ACTIVITY_CODE("ActivityCode"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    ActivityCodeGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityCodeGetType fromValue(String v) {
        for (ActivityCodeGetType c: ActivityCodeGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
