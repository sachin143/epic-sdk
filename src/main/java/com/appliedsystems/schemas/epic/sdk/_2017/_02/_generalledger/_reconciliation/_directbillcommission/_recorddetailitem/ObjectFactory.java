
package com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Flags_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", "Flags");
    private final static QName _ProducerBrokerCommissionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", "ProducerBrokerCommissionItem");
    private final static QName _ProducerBrokerCommissionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", "ProducerBrokerCommissionItems");
    private final static QName _RecordRisks_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", "RecordRisks");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecordRisks }
     * 
     */
    public RecordRisks createRecordRisks() {
        return new RecordRisks();
    }

    /**
     * Create an instance of {@link ProducerBrokerCommissionItem }
     * 
     */
    public ProducerBrokerCommissionItem createProducerBrokerCommissionItem() {
        return new ProducerBrokerCommissionItem();
    }

    /**
     * Create an instance of {@link ProducerBrokerCommissionItems }
     * 
     */
    public ProducerBrokerCommissionItems createProducerBrokerCommissionItems() {
        return new ProducerBrokerCommissionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Flags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", name = "Flags")
    public JAXBElement<Flags> createFlags(Flags value) {
        return new JAXBElement<Flags>(_Flags_QNAME, Flags.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerCommissionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", name = "ProducerBrokerCommissionItem")
    public JAXBElement<ProducerBrokerCommissionItem> createProducerBrokerCommissionItem(ProducerBrokerCommissionItem value) {
        return new JAXBElement<ProducerBrokerCommissionItem>(_ProducerBrokerCommissionItem_QNAME, ProducerBrokerCommissionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerCommissionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", name = "ProducerBrokerCommissionItems")
    public JAXBElement<ProducerBrokerCommissionItems> createProducerBrokerCommissionItems(ProducerBrokerCommissionItems value) {
        return new JAXBElement<ProducerBrokerCommissionItems>(_ProducerBrokerCommissionItems_QNAME, ProducerBrokerCommissionItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecordRisks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/", name = "RecordRisks")
    public JAXBElement<RecordRisks> createRecordRisks(RecordRisks value) {
        return new JAXBElement<RecordRisks>(_RecordRisks_QNAME, RecordRisks.class, null, value);
    }

}
