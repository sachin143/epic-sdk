
package com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComparisonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ComparisonType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EqualTo"/>
 *     &lt;enumeration value="Containing"/>
 *     &lt;enumeration value="BeginsWith"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ComparisonType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_claimsummaryfilter/")
@XmlEnum
public enum ComparisonType {

    @XmlEnumValue("EqualTo")
    EQUAL_TO("EqualTo"),
    @XmlEnumValue("Containing")
    CONTAINING("Containing"),
    @XmlEnumValue("BeginsWith")
    BEGINS_WITH("BeginsWith");
    private final String value;

    ComparisonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ComparisonType fromValue(String v) {
        for (ComparisonType c: ComparisonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
