
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CashOnAccountItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CashOnAccountItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CashOnAccountItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/}CashOnAccountItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashOnAccountItems", propOrder = {
    "cashOnAccountItems"
})
public class CashOnAccountItems {

    @XmlElement(name = "CashOnAccountItem", nillable = true)
    protected List<CashOnAccountItem> cashOnAccountItems;

    /**
     * Gets the value of the cashOnAccountItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cashOnAccountItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCashOnAccountItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CashOnAccountItem }
     * 
     * 
     */
    public List<CashOnAccountItem> getCashOnAccountItems() {
        if (cashOnAccountItems == null) {
            cashOnAccountItems = new ArrayList<CashOnAccountItem>();
        }
        return this.cashOnAccountItems;
    }

}
