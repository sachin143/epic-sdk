
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.SearchStatus;


/**
 * <p>Java class for DirectBillCommissionFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DirectBillCommissionFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DirectBillCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DirectBillCommissionStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/reconciliation/_directbillcommission/}SearchStatus" minOccurs="0"/>
 *         &lt;element name="EntityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityCodeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_reconciliation/_directbillcommission/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="IncludeReversed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MasterStatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaidArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaidStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DirectBillCommissionFilter", propOrder = {
    "agencyCode",
    "createdArea",
    "directBillCommissionID",
    "directBillCommissionStatus",
    "entityCode",
    "entityCodeType",
    "entityName",
    "entityNameComparisonType",
    "includeReversed",
    "masterStatementNumber",
    "paidArea",
    "paidStatus",
    "paymentID",
    "referenceNumber",
    "referenceNumberType",
    "statementNumber"
})
public class DirectBillCommissionFilter {

    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "CreatedArea", nillable = true)
    protected String createdArea;
    @XmlElement(name = "DirectBillCommissionID", nillable = true)
    protected Integer directBillCommissionID;
    @XmlElement(name = "DirectBillCommissionStatus")
    @XmlSchemaType(name = "string")
    protected SearchStatus directBillCommissionStatus;
    @XmlElement(name = "EntityCode", nillable = true)
    protected String entityCode;
    @XmlElement(name = "EntityCodeType", nillable = true)
    protected String entityCodeType;
    @XmlElement(name = "EntityName", nillable = true)
    protected String entityName;
    @XmlElement(name = "EntityNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType entityNameComparisonType;
    @XmlElement(name = "IncludeReversed")
    protected Boolean includeReversed;
    @XmlElement(name = "MasterStatementNumber", nillable = true)
    protected String masterStatementNumber;
    @XmlElement(name = "PaidArea", nillable = true)
    protected String paidArea;
    @XmlElement(name = "PaidStatus", nillable = true)
    protected String paidStatus;
    @XmlElement(name = "PaymentID", nillable = true)
    protected String paymentID;
    @XmlElement(name = "ReferenceNumber", nillable = true)
    protected String referenceNumber;
    @XmlElement(name = "ReferenceNumberType", nillable = true)
    protected String referenceNumberType;
    @XmlElement(name = "StatementNumber", nillable = true)
    protected String statementNumber;

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the createdArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedArea() {
        return createdArea;
    }

    /**
     * Sets the value of the createdArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedArea(String value) {
        this.createdArea = value;
    }

    /**
     * Gets the value of the directBillCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDirectBillCommissionID() {
        return directBillCommissionID;
    }

    /**
     * Sets the value of the directBillCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDirectBillCommissionID(Integer value) {
        this.directBillCommissionID = value;
    }

    /**
     * Gets the value of the directBillCommissionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link SearchStatus }
     *     
     */
    public SearchStatus getDirectBillCommissionStatus() {
        return directBillCommissionStatus;
    }

    /**
     * Sets the value of the directBillCommissionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchStatus }
     *     
     */
    public void setDirectBillCommissionStatus(SearchStatus value) {
        this.directBillCommissionStatus = value;
    }

    /**
     * Gets the value of the entityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityCode() {
        return entityCode;
    }

    /**
     * Sets the value of the entityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityCode(String value) {
        this.entityCode = value;
    }

    /**
     * Gets the value of the entityCodeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityCodeType() {
        return entityCodeType;
    }

    /**
     * Sets the value of the entityCodeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityCodeType(String value) {
        this.entityCodeType = value;
    }

    /**
     * Gets the value of the entityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the value of the entityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityName(String value) {
        this.entityName = value;
    }

    /**
     * Gets the value of the entityNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getEntityNameComparisonType() {
        return entityNameComparisonType;
    }

    /**
     * Sets the value of the entityNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setEntityNameComparisonType(ComparisonType value) {
        this.entityNameComparisonType = value;
    }

    /**
     * Gets the value of the includeReversed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeReversed() {
        return includeReversed;
    }

    /**
     * Sets the value of the includeReversed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeReversed(Boolean value) {
        this.includeReversed = value;
    }

    /**
     * Gets the value of the masterStatementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterStatementNumber() {
        return masterStatementNumber;
    }

    /**
     * Sets the value of the masterStatementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterStatementNumber(String value) {
        this.masterStatementNumber = value;
    }

    /**
     * Gets the value of the paidArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaidArea() {
        return paidArea;
    }

    /**
     * Sets the value of the paidArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaidArea(String value) {
        this.paidArea = value;
    }

    /**
     * Gets the value of the paidStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaidStatus() {
        return paidStatus;
    }

    /**
     * Sets the value of the paidStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaidStatus(String value) {
        this.paidStatus = value;
    }

    /**
     * Gets the value of the paymentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentID() {
        return paymentID;
    }

    /**
     * Sets the value of the paymentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentID(String value) {
        this.paymentID = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the referenceNumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumberType() {
        return referenceNumberType;
    }

    /**
     * Sets the value of the referenceNumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumberType(String value) {
        this.referenceNumberType = value;
    }

    /**
     * Gets the value of the statementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementNumber() {
        return statementNumber;
    }

    /**
     * Sets the value of the statementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementNumber(String value) {
        this.statementNumber = value;
    }

}
