
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RevenueScheduleItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RevenueScheduleItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RevenueScheduleItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/}RevenueScheduleItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RevenueScheduleItems", propOrder = {
    "revenueScheduleItems"
})
public class RevenueScheduleItems {

    @XmlElement(name = "RevenueScheduleItem", nillable = true)
    protected List<RevenueScheduleItem> revenueScheduleItems;

    /**
     * Gets the value of the revenueScheduleItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the revenueScheduleItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRevenueScheduleItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RevenueScheduleItem }
     * 
     * 
     */
    public List<RevenueScheduleItem> getRevenueScheduleItems() {
        if (revenueScheduleItems == null) {
            revenueScheduleItems = new ArrayList<RevenueScheduleItem>();
        }
        return this.revenueScheduleItems;
    }

}
