
package com.appliedsystems.schemas.epic.sdk._2011._01._account;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.Billing;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.Commissions;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.Detail;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.InstallmentItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.Invoice;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.Payment;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.PaymentMethod;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.PaymentInstallmentItems;


/**
 * <p>Java class for Transaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttachToOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BillNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BillNumberOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="BillingValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}Billing" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}Detail" minOccurs="0"/>
 *         &lt;element name="InvoiceValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}Invoice" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ItemNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CommissionsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}Commissions" minOccurs="0"/>
 *         &lt;element name="Discrepancy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreAgencyCommissionLessThanProducerBroker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IgnoreFlatCommission" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BasicInstallmentOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="InstallmentCycles" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InstallmentDownPayment" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InstallmentNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InstallmentPlanID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstallmentSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InstallmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstallmentsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}InstallmentItems" minOccurs="0"/>
 *         &lt;element name="UpdateInstallmentsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="PackagePolicyReplaceFullAnnualizedCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PackagePolicyReplaceFullAnnualizedPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoicePaymentOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="PaymentInstallmentItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/}PaymentInstallmentItems" minOccurs="0"/>
 *         &lt;element name="PaymentLineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PaymentMethodValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}PaymentMethod" minOccurs="0"/>
 *         &lt;element name="PaymentValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/}Payment" minOccurs="0"/>
 *         &lt;element name="ServiceID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServiceItemizationID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AutoGenerateTaxes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transaction", propOrder = {
    "accountID",
    "accountTypeCode",
    "attachToOption",
    "balance",
    "billNumber",
    "billNumberOption",
    "billingValue",
    "description",
    "detailValue",
    "invoiceValue",
    "isReadOnly",
    "itemNumber",
    "policyID",
    "policyTypeCode",
    "transactionAmount",
    "transactionCode",
    "transactionID",
    "commissionsValue",
    "discrepancy",
    "ignoreAgencyCommissionLessThanProducerBroker",
    "ignoreFlatCommission",
    "basicInstallmentOption",
    "installmentCycles",
    "installmentDownPayment",
    "installmentNumber",
    "installmentPlanID",
    "installmentSequenceNumber",
    "installmentType",
    "installmentsValue",
    "updateInstallmentsOption",
    "packagePolicyReplaceFullAnnualizedCommission",
    "packagePolicyReplaceFullAnnualizedPremium",
    "invoicePaymentOption",
    "paymentInstallmentItems",
    "paymentLineID",
    "paymentMethodValue",
    "paymentValue",
    "serviceID",
    "serviceItemizationID",
    "autoGenerateTaxes"
})
public class Transaction {

    @XmlElement(name = "AccountID")
    protected Integer accountID;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "AttachToOption", nillable = true)
    protected OptionType attachToOption;
    @XmlElement(name = "Balance")
    protected BigDecimal balance;
    @XmlElement(name = "BillNumber")
    protected Integer billNumber;
    @XmlElement(name = "BillNumberOption", nillable = true)
    protected OptionType billNumberOption;
    @XmlElement(name = "BillingValue", nillable = true)
    protected Billing billingValue;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "InvoiceValue", nillable = true)
    protected Invoice invoiceValue;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "ItemNumber", nillable = true)
    protected String itemNumber;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "PolicyTypeCode", nillable = true)
    protected String policyTypeCode;
    @XmlElement(name = "TransactionAmount")
    protected BigDecimal transactionAmount;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "CommissionsValue", nillable = true)
    protected Commissions commissionsValue;
    @XmlElement(name = "Discrepancy", nillable = true)
    protected String discrepancy;
    @XmlElement(name = "IgnoreAgencyCommissionLessThanProducerBroker")
    protected Boolean ignoreAgencyCommissionLessThanProducerBroker;
    @XmlElement(name = "IgnoreFlatCommission")
    protected Boolean ignoreFlatCommission;
    @XmlElement(name = "BasicInstallmentOption", nillable = true)
    protected OptionType basicInstallmentOption;
    @XmlElement(name = "InstallmentCycles", nillable = true)
    protected Integer installmentCycles;
    @XmlElement(name = "InstallmentDownPayment", nillable = true)
    protected BigDecimal installmentDownPayment;
    @XmlElement(name = "InstallmentNumber", nillable = true)
    protected Integer installmentNumber;
    @XmlElement(name = "InstallmentPlanID", nillable = true)
    protected String installmentPlanID;
    @XmlElement(name = "InstallmentSequenceNumber", nillable = true)
    protected Integer installmentSequenceNumber;
    @XmlElement(name = "InstallmentType", nillable = true)
    protected String installmentType;
    @XmlElement(name = "InstallmentsValue", nillable = true)
    protected InstallmentItems installmentsValue;
    @XmlElement(name = "UpdateInstallmentsOption", nillable = true)
    protected OptionType updateInstallmentsOption;
    @XmlElement(name = "PackagePolicyReplaceFullAnnualizedCommission", nillable = true)
    protected BigDecimal packagePolicyReplaceFullAnnualizedCommission;
    @XmlElement(name = "PackagePolicyReplaceFullAnnualizedPremium", nillable = true)
    protected BigDecimal packagePolicyReplaceFullAnnualizedPremium;
    @XmlElement(name = "InvoicePaymentOption", nillable = true)
    protected OptionType invoicePaymentOption;
    @XmlElement(name = "PaymentInstallmentItems", nillable = true)
    protected PaymentInstallmentItems paymentInstallmentItems;
    @XmlElement(name = "PaymentLineID", nillable = true)
    protected Integer paymentLineID;
    @XmlElement(name = "PaymentMethodValue", nillable = true)
    protected PaymentMethod paymentMethodValue;
    @XmlElement(name = "PaymentValue", nillable = true)
    protected Payment paymentValue;
    @XmlElement(name = "ServiceID", nillable = true)
    protected Integer serviceID;
    @XmlElement(name = "ServiceItemizationID", nillable = true)
    protected Integer serviceItemizationID;
    @XmlElement(name = "AutoGenerateTaxes")
    protected Boolean autoGenerateTaxes;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the attachToOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAttachToOption() {
        return attachToOption;
    }

    /**
     * Sets the value of the attachToOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAttachToOption(OptionType value) {
        this.attachToOption = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the billNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillNumber() {
        return billNumber;
    }

    /**
     * Sets the value of the billNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillNumber(Integer value) {
        this.billNumber = value;
    }

    /**
     * Gets the value of the billNumberOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBillNumberOption() {
        return billNumberOption;
    }

    /**
     * Sets the value of the billNumberOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBillNumberOption(OptionType value) {
        this.billNumberOption = value;
    }

    /**
     * Gets the value of the billingValue property.
     * 
     * @return
     *     possible object is
     *     {@link Billing }
     *     
     */
    public Billing getBillingValue() {
        return billingValue;
    }

    /**
     * Sets the value of the billingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Billing }
     *     
     */
    public void setBillingValue(Billing value) {
        this.billingValue = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the invoiceValue property.
     * 
     * @return
     *     possible object is
     *     {@link Invoice }
     *     
     */
    public Invoice getInvoiceValue() {
        return invoiceValue;
    }

    /**
     * Sets the value of the invoiceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Invoice }
     *     
     */
    public void setInvoiceValue(Invoice value) {
        this.invoiceValue = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the itemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Sets the value of the itemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemNumber(String value) {
        this.itemNumber = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the policyTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyTypeCode() {
        return policyTypeCode;
    }

    /**
     * Sets the value of the policyTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyTypeCode(String value) {
        this.policyTypeCode = value;
    }

    /**
     * Gets the value of the transactionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Sets the value of the transactionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransactionAmount(BigDecimal value) {
        this.transactionAmount = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the commissionsValue property.
     * 
     * @return
     *     possible object is
     *     {@link Commissions }
     *     
     */
    public Commissions getCommissionsValue() {
        return commissionsValue;
    }

    /**
     * Sets the value of the commissionsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Commissions }
     *     
     */
    public void setCommissionsValue(Commissions value) {
        this.commissionsValue = value;
    }

    /**
     * Gets the value of the discrepancy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscrepancy() {
        return discrepancy;
    }

    /**
     * Sets the value of the discrepancy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscrepancy(String value) {
        this.discrepancy = value;
    }

    /**
     * Gets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAgencyCommissionLessThanProducerBroker() {
        return ignoreAgencyCommissionLessThanProducerBroker;
    }

    /**
     * Sets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAgencyCommissionLessThanProducerBroker(Boolean value) {
        this.ignoreAgencyCommissionLessThanProducerBroker = value;
    }

    /**
     * Gets the value of the ignoreFlatCommission property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreFlatCommission() {
        return ignoreFlatCommission;
    }

    /**
     * Sets the value of the ignoreFlatCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreFlatCommission(Boolean value) {
        this.ignoreFlatCommission = value;
    }

    /**
     * Gets the value of the basicInstallmentOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBasicInstallmentOption() {
        return basicInstallmentOption;
    }

    /**
     * Sets the value of the basicInstallmentOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBasicInstallmentOption(OptionType value) {
        this.basicInstallmentOption = value;
    }

    /**
     * Gets the value of the installmentCycles property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallmentCycles() {
        return installmentCycles;
    }

    /**
     * Sets the value of the installmentCycles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallmentCycles(Integer value) {
        this.installmentCycles = value;
    }

    /**
     * Gets the value of the installmentDownPayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInstallmentDownPayment() {
        return installmentDownPayment;
    }

    /**
     * Sets the value of the installmentDownPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInstallmentDownPayment(BigDecimal value) {
        this.installmentDownPayment = value;
    }

    /**
     * Gets the value of the installmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    /**
     * Sets the value of the installmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallmentNumber(Integer value) {
        this.installmentNumber = value;
    }

    /**
     * Gets the value of the installmentPlanID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallmentPlanID() {
        return installmentPlanID;
    }

    /**
     * Sets the value of the installmentPlanID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallmentPlanID(String value) {
        this.installmentPlanID = value;
    }

    /**
     * Gets the value of the installmentSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallmentSequenceNumber() {
        return installmentSequenceNumber;
    }

    /**
     * Sets the value of the installmentSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallmentSequenceNumber(Integer value) {
        this.installmentSequenceNumber = value;
    }

    /**
     * Gets the value of the installmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallmentType() {
        return installmentType;
    }

    /**
     * Sets the value of the installmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallmentType(String value) {
        this.installmentType = value;
    }

    /**
     * Gets the value of the installmentsValue property.
     * 
     * @return
     *     possible object is
     *     {@link InstallmentItems }
     *     
     */
    public InstallmentItems getInstallmentsValue() {
        return installmentsValue;
    }

    /**
     * Sets the value of the installmentsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstallmentItems }
     *     
     */
    public void setInstallmentsValue(InstallmentItems value) {
        this.installmentsValue = value;
    }

    /**
     * Gets the value of the updateInstallmentsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getUpdateInstallmentsOption() {
        return updateInstallmentsOption;
    }

    /**
     * Sets the value of the updateInstallmentsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setUpdateInstallmentsOption(OptionType value) {
        this.updateInstallmentsOption = value;
    }

    /**
     * Gets the value of the packagePolicyReplaceFullAnnualizedCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPackagePolicyReplaceFullAnnualizedCommission() {
        return packagePolicyReplaceFullAnnualizedCommission;
    }

    /**
     * Sets the value of the packagePolicyReplaceFullAnnualizedCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPackagePolicyReplaceFullAnnualizedCommission(BigDecimal value) {
        this.packagePolicyReplaceFullAnnualizedCommission = value;
    }

    /**
     * Gets the value of the packagePolicyReplaceFullAnnualizedPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPackagePolicyReplaceFullAnnualizedPremium() {
        return packagePolicyReplaceFullAnnualizedPremium;
    }

    /**
     * Sets the value of the packagePolicyReplaceFullAnnualizedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPackagePolicyReplaceFullAnnualizedPremium(BigDecimal value) {
        this.packagePolicyReplaceFullAnnualizedPremium = value;
    }

    /**
     * Gets the value of the invoicePaymentOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getInvoicePaymentOption() {
        return invoicePaymentOption;
    }

    /**
     * Sets the value of the invoicePaymentOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setInvoicePaymentOption(OptionType value) {
        this.invoicePaymentOption = value;
    }

    /**
     * Gets the value of the paymentInstallmentItems property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentInstallmentItems }
     *     
     */
    public PaymentInstallmentItems getPaymentInstallmentItems() {
        return paymentInstallmentItems;
    }

    /**
     * Sets the value of the paymentInstallmentItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentInstallmentItems }
     *     
     */
    public void setPaymentInstallmentItems(PaymentInstallmentItems value) {
        this.paymentInstallmentItems = value;
    }

    /**
     * Gets the value of the paymentLineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaymentLineID() {
        return paymentLineID;
    }

    /**
     * Sets the value of the paymentLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaymentLineID(Integer value) {
        this.paymentLineID = value;
    }

    /**
     * Gets the value of the paymentMethodValue property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethod }
     *     
     */
    public PaymentMethod getPaymentMethodValue() {
        return paymentMethodValue;
    }

    /**
     * Sets the value of the paymentMethodValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethod }
     *     
     */
    public void setPaymentMethodValue(PaymentMethod value) {
        this.paymentMethodValue = value;
    }

    /**
     * Gets the value of the paymentValue property.
     * 
     * @return
     *     possible object is
     *     {@link Payment }
     *     
     */
    public Payment getPaymentValue() {
        return paymentValue;
    }

    /**
     * Sets the value of the paymentValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Payment }
     *     
     */
    public void setPaymentValue(Payment value) {
        this.paymentValue = value;
    }

    /**
     * Gets the value of the serviceID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceID() {
        return serviceID;
    }

    /**
     * Sets the value of the serviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceID(Integer value) {
        this.serviceID = value;
    }

    /**
     * Gets the value of the serviceItemizationID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceItemizationID() {
        return serviceItemizationID;
    }

    /**
     * Sets the value of the serviceItemizationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceItemizationID(Integer value) {
        this.serviceItemizationID = value;
    }

    /**
     * Gets the value of the autoGenerateTaxes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoGenerateTaxes() {
        return autoGenerateTaxes;
    }

    /**
     * Sets the value of the autoGenerateTaxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoGenerateTaxes(Boolean value) {
        this.autoGenerateTaxes = value;
    }

}
