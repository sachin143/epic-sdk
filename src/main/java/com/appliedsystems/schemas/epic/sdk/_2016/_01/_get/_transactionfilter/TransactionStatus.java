
package com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="BalanceForward"/>
 *     &lt;enumeration value="OpenItem"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionStatus", namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/")
@XmlEnum
public enum TransactionStatus {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("BalanceForward")
    BALANCE_FORWARD("BalanceForward"),
    @XmlEnumValue("OpenItem")
    OPEN_ITEM("OpenItem");
    private final String value;

    TransactionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionStatus fromValue(String v) {
        for (TransactionStatus c: TransactionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
