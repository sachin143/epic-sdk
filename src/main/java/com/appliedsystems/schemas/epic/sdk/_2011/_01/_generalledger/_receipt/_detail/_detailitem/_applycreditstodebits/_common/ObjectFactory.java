
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaymentItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/_common/", "PaymentItem");
    private final static QName _PaymentItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/_common/", "PaymentItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentItems }
     * 
     */
    public PaymentItems createPaymentItems() {
        return new PaymentItems();
    }

    /**
     * Create an instance of {@link PaymentItem }
     * 
     */
    public PaymentItem createPaymentItem() {
        return new PaymentItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/_common/", name = "PaymentItem")
    public JAXBElement<PaymentItem> createPaymentItem(PaymentItem value) {
        return new JAXBElement<PaymentItem>(_PaymentItem_QNAME, PaymentItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/_common/", name = "PaymentItems")
    public JAXBElement<PaymentItems> createPaymentItems(PaymentItems value) {
        return new JAXBElement<PaymentItems>(_PaymentItems_QNAME, PaymentItems.class, null, value);
    }

}
