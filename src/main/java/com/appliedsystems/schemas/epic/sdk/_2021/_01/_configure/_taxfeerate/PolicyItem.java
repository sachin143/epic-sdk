
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.Flags;


/**
 * <p>Java class for PolicyItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/_policyitem}Flags" minOccurs="0"/>
 *         &lt;element name="PolicyLineTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyLineTypeID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyItem", propOrder = {
    "applicationDetail",
    "description",
    "flag",
    "policyLineTypeCode",
    "policyLineTypeID"
})
public class PolicyItem {

    @XmlElement(name = "ApplicationDetail", nillable = true)
    protected String applicationDetail;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "PolicyLineTypeCode", nillable = true)
    protected String policyLineTypeCode;
    @XmlElement(name = "PolicyLineTypeID")
    protected Integer policyLineTypeID;

    /**
     * Gets the value of the applicationDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationDetail() {
        return applicationDetail;
    }

    /**
     * Sets the value of the applicationDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationDetail(String value) {
        this.applicationDetail = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the policyLineTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyLineTypeCode() {
        return policyLineTypeCode;
    }

    /**
     * Sets the value of the policyLineTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyLineTypeCode(String value) {
        this.policyLineTypeCode = value;
    }

    /**
     * Gets the value of the policyLineTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyLineTypeID() {
        return policyLineTypeID;
    }

    /**
     * Sets the value of the policyLineTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyLineTypeID(Integer value) {
        this.policyLineTypeID = value;
    }

}
