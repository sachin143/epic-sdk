
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IssueNotIssueEndorsementGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IssueNotIssueEndorsementGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PolicyID"/>
 *     &lt;enumeration value="PolicyAndServiceSummaryID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IssueNotIssueEndorsementGetType")
@XmlEnum
public enum IssueNotIssueEndorsementGetType {

    @XmlEnumValue("PolicyID")
    POLICY_ID("PolicyID"),
    @XmlEnumValue("PolicyAndServiceSummaryID")
    POLICY_AND_SERVICE_SUMMARY_ID("PolicyAndServiceSummaryID");
    private final String value;

    IssueNotIssueEndorsementGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IssueNotIssueEndorsementGetType fromValue(String v) {
        for (IssueNotIssueEndorsementGetType c: IssueNotIssueEndorsementGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
