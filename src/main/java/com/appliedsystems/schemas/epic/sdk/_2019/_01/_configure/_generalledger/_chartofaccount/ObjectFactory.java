
package com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfBankAccount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/", "ArrayOfBankAccount");
    private final static QName _BankAccount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/", "BankAccount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BankAccount }
     * 
     */
    public BankAccount createBankAccount() {
        return new BankAccount();
    }

    /**
     * Create an instance of {@link ArrayOfBankAccount }
     * 
     */
    public ArrayOfBankAccount createArrayOfBankAccount() {
        return new ArrayOfBankAccount();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfBankAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/", name = "ArrayOfBankAccount")
    public JAXBElement<ArrayOfBankAccount> createArrayOfBankAccount(ArrayOfBankAccount value) {
        return new JAXBElement<ArrayOfBankAccount>(_ArrayOfBankAccount_QNAME, ArrayOfBankAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/", name = "BankAccount")
    public JAXBElement<BankAccount> createBankAccount(BankAccount value) {
        return new JAXBElement<BankAccount>(_BankAccount_QNAME, BankAccount.class, null, value);
    }

}
