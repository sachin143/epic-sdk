@javax.xml.bind.annotation.XmlSchema(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments;
