
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdditionalPartyGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AdditionalPartyGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AdditionalPartyID"/>
 *     &lt;enumeration value="ClaimID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AdditionalPartyGetType")
@XmlEnum
public enum AdditionalPartyGetType {

    @XmlEnumValue("AdditionalPartyID")
    ADDITIONAL_PARTY_ID("AdditionalPartyID"),
    @XmlEnumValue("ClaimID")
    CLAIM_ID("ClaimID");
    private final String value;

    AdditionalPartyGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdditionalPartyGetType fromValue(String v) {
        for (AdditionalPartyGetType c: AdditionalPartyGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
