
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DefaultEntry_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_common/", "DefaultEntry");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DefaultEntry }
     * 
     */
    public DefaultEntry createDefaultEntry() {
        return new DefaultEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DefaultEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_common/", name = "DefaultEntry")
    public JAXBElement<DefaultEntry> createDefaultEntry(DefaultEntry value) {
        return new JAXBElement<DefaultEntry>(_DefaultEntry_QNAME, DefaultEntry.class, null, value);
    }

}
