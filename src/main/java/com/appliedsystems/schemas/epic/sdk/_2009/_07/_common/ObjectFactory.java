
package com.appliedsystems.schemas.epic.sdk._2009._07._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfLookup_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", "ArrayOfLookup");
    private final static QName _Lookup_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", "Lookup");
    private final static QName _OptionType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", "OptionType");
    private final static QName _ArrayOfOptionType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", "ArrayOfOptionType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfLookup }
     * 
     */
    public ArrayOfLookup createArrayOfLookup() {
        return new ArrayOfLookup();
    }

    /**
     * Create an instance of {@link Lookup }
     * 
     */
    public Lookup createLookup() {
        return new Lookup();
    }

    /**
     * Create an instance of {@link ArrayOfOptionType }
     * 
     */
    public ArrayOfOptionType createArrayOfOptionType() {
        return new ArrayOfOptionType();
    }

    /**
     * Create an instance of {@link OptionType }
     * 
     */
    public OptionType createOptionType() {
        return new OptionType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLookup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", name = "ArrayOfLookup")
    public JAXBElement<ArrayOfLookup> createArrayOfLookup(ArrayOfLookup value) {
        return new JAXBElement<ArrayOfLookup>(_ArrayOfLookup_QNAME, ArrayOfLookup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Lookup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", name = "Lookup")
    public JAXBElement<Lookup> createLookup(Lookup value) {
        return new JAXBElement<Lookup>(_Lookup_QNAME, Lookup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", name = "OptionType")
    public JAXBElement<OptionType> createOptionType(OptionType value) {
        return new JAXBElement<OptionType>(_OptionType_QNAME, OptionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfOptionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/", name = "ArrayOfOptionType")
    public JAXBElement<ArrayOfOptionType> createArrayOfOptionType(ArrayOfOptionType value) {
        return new JAXBElement<ArrayOfOptionType>(_ArrayOfOptionType_QNAME, ArrayOfOptionType.class, null, value);
    }

}
