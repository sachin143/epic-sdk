
package com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ArrayOfBank;


/**
 * <p>Java class for BankGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankReconciliations" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}ArrayOfBank" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankGetResult", propOrder = {
    "bankReconciliations",
    "totalPages"
})
public class BankGetResult {

    @XmlElement(name = "BankReconciliations", nillable = true)
    protected ArrayOfBank bankReconciliations;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the bankReconciliations property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBank }
     *     
     */
    public ArrayOfBank getBankReconciliations() {
        return bankReconciliations;
    }

    /**
     * Sets the value of the bankReconciliations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBank }
     *     
     */
    public void setBankReconciliations(ArrayOfBank value) {
        this.bankReconciliations = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
