
package com.appliedsystems.schemas.epic.sdk._2021._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ArrayOfAllocationMethod;


/**
 * <p>Java class for AllocationMethodsGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationMethodsGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationMethods" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}ArrayOfAllocationMethod" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationMethodsGetResult", propOrder = {
    "allocationMethods",
    "totalPages"
})
public class AllocationMethodsGetResult {

    @XmlElement(name = "AllocationMethods", nillable = true)
    protected ArrayOfAllocationMethod allocationMethods;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the allocationMethods property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAllocationMethod }
     *     
     */
    public ArrayOfAllocationMethod getAllocationMethods() {
        return allocationMethods;
    }

    /**
     * Sets the value of the allocationMethods property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAllocationMethod }
     *     
     */
    public void setAllocationMethods(ArrayOfAllocationMethod value) {
        this.allocationMethods = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
