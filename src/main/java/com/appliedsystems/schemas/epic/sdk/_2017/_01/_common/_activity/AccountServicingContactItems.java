
package com.appliedsystems.schemas.epic.sdk._2017._01._common._activity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountServicingContactItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountServicingContactItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountServicingContactItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/}AccountServicingContactItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountServicingContactItems", propOrder = {
    "accountServicingContactItems"
})
public class AccountServicingContactItems {

    @XmlElement(name = "AccountServicingContactItem", nillable = true)
    protected List<AccountServicingContactItem> accountServicingContactItems;

    /**
     * Gets the value of the accountServicingContactItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountServicingContactItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountServicingContactItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountServicingContactItem }
     * 
     * 
     */
    public List<AccountServicingContactItem> getAccountServicingContactItems() {
        if (accountServicingContactItems == null) {
            accountServicingContactItems = new ArrayList<AccountServicingContactItem>();
        }
        return this.accountServicingContactItems;
    }

}
