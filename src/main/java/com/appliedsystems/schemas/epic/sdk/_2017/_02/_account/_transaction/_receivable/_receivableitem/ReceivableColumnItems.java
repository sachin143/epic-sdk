
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceivableColumnItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceivableColumnItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceivableColumnItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem}ReceivableColumnItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceivableColumnItems", propOrder = {
    "receivableColumnItems"
})
public class ReceivableColumnItems {

    @XmlElement(name = "ReceivableColumnItem", nillable = true)
    protected List<ReceivableColumnItem> receivableColumnItems;

    /**
     * Gets the value of the receivableColumnItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivableColumnItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivableColumnItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceivableColumnItem }
     * 
     * 
     */
    public List<ReceivableColumnItem> getReceivableColumnItems() {
        if (receivableColumnItems == null) {
            receivableColumnItems = new ArrayList<ReceivableColumnItem>();
        }
        return this.receivableColumnItems;
    }

}
