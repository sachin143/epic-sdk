
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EligibleVoucherItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", "EligibleVoucherItems");
    private final static QName _SearchWhereComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", "SearchWhereComparisonType");
    private final static QName _SearchWhereFilterType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", "SearchWhereFilterType");
    private final static QName _EligibleVoucherItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", "EligibleVoucherItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EligibleVoucherItem }
     * 
     */
    public EligibleVoucherItem createEligibleVoucherItem() {
        return new EligibleVoucherItem();
    }

    /**
     * Create an instance of {@link EligibleVoucherItems }
     * 
     */
    public EligibleVoucherItems createEligibleVoucherItems() {
        return new EligibleVoucherItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EligibleVoucherItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", name = "EligibleVoucherItems")
    public JAXBElement<EligibleVoucherItems> createEligibleVoucherItems(EligibleVoucherItems value) {
        return new JAXBElement<EligibleVoucherItems>(_EligibleVoucherItems_QNAME, EligibleVoucherItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchWhereComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", name = "SearchWhereComparisonType")
    public JAXBElement<SearchWhereComparisonType> createSearchWhereComparisonType(SearchWhereComparisonType value) {
        return new JAXBElement<SearchWhereComparisonType>(_SearchWhereComparisonType_QNAME, SearchWhereComparisonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchWhereFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", name = "SearchWhereFilterType")
    public JAXBElement<SearchWhereFilterType> createSearchWhereFilterType(SearchWhereFilterType value) {
        return new JAXBElement<SearchWhereFilterType>(_SearchWhereFilterType_QNAME, SearchWhereFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EligibleVoucherItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/", name = "EligibleVoucherItem")
    public JAXBElement<EligibleVoucherItem> createEligibleVoucherItem(EligibleVoucherItem value) {
        return new JAXBElement<EligibleVoucherItem>(_EligibleVoucherItem_QNAME, EligibleVoucherItem.class, null, value);
    }

}
