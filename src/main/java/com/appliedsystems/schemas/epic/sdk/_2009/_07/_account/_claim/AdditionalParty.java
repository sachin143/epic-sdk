
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for AdditionalParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalPartyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="BusinessExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Involvement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="WhenToContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WhereToContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobilePhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidencePhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalParty", propOrder = {
    "additionalPartyID",
    "address",
    "businessExtension",
    "businessNumber",
    "claimID",
    "comments",
    "email",
    "faxExtension",
    "faxNumber",
    "involvement",
    "mobileNumber",
    "name",
    "residenceNumber",
    "timestamp",
    "whenToContact",
    "whereToContact",
    "businessPhoneCountryCode",
    "faxCountryCode",
    "mobilePhoneCountryCode",
    "residencePhoneCountryCode",
    "siteID"
})
public class AdditionalParty {

    @XmlElement(name = "AdditionalPartyID")
    protected Integer additionalPartyID;
    @XmlElement(name = "Address", nillable = true)
    protected Address address;
    @XmlElement(name = "BusinessExtension", nillable = true)
    protected String businessExtension;
    @XmlElement(name = "BusinessNumber", nillable = true)
    protected String businessNumber;
    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "Email", nillable = true)
    protected String email;
    @XmlElement(name = "FaxExtension", nillable = true)
    protected String faxExtension;
    @XmlElement(name = "FaxNumber", nillable = true)
    protected String faxNumber;
    @XmlElement(name = "Involvement", nillable = true)
    protected String involvement;
    @XmlElement(name = "MobileNumber", nillable = true)
    protected String mobileNumber;
    @XmlElement(name = "Name", nillable = true)
    protected String name;
    @XmlElement(name = "ResidenceNumber", nillable = true)
    protected String residenceNumber;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "WhenToContact", nillable = true)
    protected String whenToContact;
    @XmlElement(name = "WhereToContact", nillable = true)
    protected String whereToContact;
    @XmlElement(name = "BusinessPhoneCountryCode", nillable = true)
    protected String businessPhoneCountryCode;
    @XmlElement(name = "FaxCountryCode", nillable = true)
    protected String faxCountryCode;
    @XmlElement(name = "MobilePhoneCountryCode", nillable = true)
    protected String mobilePhoneCountryCode;
    @XmlElement(name = "ResidencePhoneCountryCode", nillable = true)
    protected String residencePhoneCountryCode;
    @XmlElement(name = "SiteID", nillable = true)
    protected String siteID;

    /**
     * Gets the value of the additionalPartyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdditionalPartyID() {
        return additionalPartyID;
    }

    /**
     * Sets the value of the additionalPartyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdditionalPartyID(Integer value) {
        this.additionalPartyID = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the businessExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessExtension() {
        return businessExtension;
    }

    /**
     * Sets the value of the businessExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessExtension(String value) {
        this.businessExtension = value;
    }

    /**
     * Gets the value of the businessNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessNumber() {
        return businessNumber;
    }

    /**
     * Sets the value of the businessNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessNumber(String value) {
        this.businessNumber = value;
    }

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the faxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxExtension() {
        return faxExtension;
    }

    /**
     * Sets the value of the faxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxExtension(String value) {
        this.faxExtension = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the involvement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvolvement() {
        return involvement;
    }

    /**
     * Sets the value of the involvement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvolvement(String value) {
        this.involvement = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the residenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidenceNumber() {
        return residenceNumber;
    }

    /**
     * Sets the value of the residenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidenceNumber(String value) {
        this.residenceNumber = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the whenToContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhenToContact() {
        return whenToContact;
    }

    /**
     * Sets the value of the whenToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhenToContact(String value) {
        this.whenToContact = value;
    }

    /**
     * Gets the value of the whereToContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhereToContact() {
        return whereToContact;
    }

    /**
     * Sets the value of the whereToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhereToContact(String value) {
        this.whereToContact = value;
    }

    /**
     * Gets the value of the businessPhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPhoneCountryCode() {
        return businessPhoneCountryCode;
    }

    /**
     * Sets the value of the businessPhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPhoneCountryCode(String value) {
        this.businessPhoneCountryCode = value;
    }

    /**
     * Gets the value of the faxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxCountryCode() {
        return faxCountryCode;
    }

    /**
     * Sets the value of the faxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxCountryCode(String value) {
        this.faxCountryCode = value;
    }

    /**
     * Gets the value of the mobilePhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneCountryCode() {
        return mobilePhoneCountryCode;
    }

    /**
     * Sets the value of the mobilePhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneCountryCode(String value) {
        this.mobilePhoneCountryCode = value;
    }

    /**
     * Gets the value of the residencePhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePhoneCountryCode() {
        return residencePhoneCountryCode;
    }

    /**
     * Sets the value of the residencePhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePhoneCountryCode(String value) {
        this.residencePhoneCountryCode = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

}
