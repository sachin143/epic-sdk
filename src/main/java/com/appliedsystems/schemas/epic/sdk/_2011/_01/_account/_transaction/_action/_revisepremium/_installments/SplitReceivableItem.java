
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.Flags;


/**
 * <p>Java class for SplitReceivableItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitReceivableItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/_splitreceivableitem/}Flags" minOccurs="0"/>
 *         &lt;element name="GenerateInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OriginalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ParentReceivable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PlanSequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ReceivableIndex" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RevisedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitReceivableItem", propOrder = {
    "arDueDate",
    "accountLookupCode",
    "accountingMonth",
    "contact",
    "effectiveDate",
    "flag",
    "generateInvoiceDate",
    "originalAmount",
    "parentReceivable",
    "planSequenceNumber",
    "receivableIndex",
    "revisedAmount",
    "transactionCode",
    "transactionDescription",
    "transactionID"
})
public class SplitReceivableItem {

    @XmlElement(name = "ARDueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arDueDate;
    @XmlElement(name = "AccountLookupCode", nillable = true)
    protected String accountLookupCode;
    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "Contact", nillable = true)
    protected String contact;
    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "GenerateInvoiceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar generateInvoiceDate;
    @XmlElement(name = "OriginalAmount")
    protected BigDecimal originalAmount;
    @XmlElement(name = "ParentReceivable")
    protected Boolean parentReceivable;
    @XmlElement(name = "PlanSequenceNumber")
    protected Integer planSequenceNumber;
    @XmlElement(name = "ReceivableIndex")
    protected Integer receivableIndex;
    @XmlElement(name = "RevisedAmount")
    protected BigDecimal revisedAmount;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionDescription", nillable = true)
    protected String transactionDescription;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;

    /**
     * Gets the value of the arDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getARDueDate() {
        return arDueDate;
    }

    /**
     * Sets the value of the arDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setARDueDate(XMLGregorianCalendar value) {
        this.arDueDate = value;
    }

    /**
     * Gets the value of the accountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountLookupCode() {
        return accountLookupCode;
    }

    /**
     * Sets the value of the accountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountLookupCode(String value) {
        this.accountLookupCode = value;
    }

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the generateInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGenerateInvoiceDate() {
        return generateInvoiceDate;
    }

    /**
     * Sets the value of the generateInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGenerateInvoiceDate(XMLGregorianCalendar value) {
        this.generateInvoiceDate = value;
    }

    /**
     * Gets the value of the originalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    /**
     * Sets the value of the originalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOriginalAmount(BigDecimal value) {
        this.originalAmount = value;
    }

    /**
     * Gets the value of the parentReceivable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isParentReceivable() {
        return parentReceivable;
    }

    /**
     * Sets the value of the parentReceivable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParentReceivable(Boolean value) {
        this.parentReceivable = value;
    }

    /**
     * Gets the value of the planSequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlanSequenceNumber() {
        return planSequenceNumber;
    }

    /**
     * Sets the value of the planSequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlanSequenceNumber(Integer value) {
        this.planSequenceNumber = value;
    }

    /**
     * Gets the value of the receivableIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceivableIndex() {
        return receivableIndex;
    }

    /**
     * Sets the value of the receivableIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceivableIndex(Integer value) {
        this.receivableIndex = value;
    }

    /**
     * Gets the value of the revisedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRevisedAmount() {
        return revisedAmount;
    }

    /**
     * Sets the value of the revisedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRevisedAmount(BigDecimal value) {
        this.revisedAmount = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Sets the value of the transactionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDescription(String value) {
        this.transactionDescription = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

}
