
package com.appliedsystems.schemas.epic.sdk._2009._07._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ComparisonType;


/**
 * <p>Java class for ContactFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DateOfBirthBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateOfBirthEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CityComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="Classification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassificationComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DescriptionComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddressComparisontype" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactFilter", propOrder = {
    "accountID",
    "accountTypeCode",
    "contactID",
    "dateOfBirthBegins",
    "dateOfBirthEnds",
    "category",
    "city",
    "cityComparisonType",
    "classification",
    "classificationComparisonType",
    "description",
    "descriptionComparisonType",
    "emailAddress",
    "emailAddressComparisontype",
    "name",
    "nameComparisonType",
    "phone",
    "phoneComparisonType",
    "state"
})
public class ContactFilter {

    @XmlElement(name = "AccountID", nillable = true)
    protected Integer accountID;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "ContactID", nillable = true)
    protected Integer contactID;
    @XmlElement(name = "DateOfBirthBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirthBegins;
    @XmlElement(name = "DateOfBirthEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirthEnds;
    @XmlElement(name = "Category", nillable = true)
    protected String category;
    @XmlElement(name = "City", nillable = true)
    protected String city;
    @XmlElement(name = "CityComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType cityComparisonType;
    @XmlElement(name = "Classification", nillable = true)
    protected String classification;
    @XmlElement(name = "ClassificationComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType classificationComparisonType;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DescriptionComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType descriptionComparisonType;
    @XmlElement(name = "EmailAddress", nillable = true)
    protected String emailAddress;
    @XmlElement(name = "EmailAddressComparisontype")
    @XmlSchemaType(name = "string")
    protected ComparisonType emailAddressComparisontype;
    @XmlElement(name = "Name", nillable = true)
    protected String name;
    @XmlElement(name = "NameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType nameComparisonType;
    @XmlElement(name = "Phone", nillable = true)
    protected String phone;
    @XmlElement(name = "PhoneComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType phoneComparisonType;
    @XmlElement(name = "State", nillable = true)
    protected String state;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContactID(Integer value) {
        this.contactID = value;
    }

    /**
     * Gets the value of the dateOfBirthBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirthBegins() {
        return dateOfBirthBegins;
    }

    /**
     * Sets the value of the dateOfBirthBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirthBegins(XMLGregorianCalendar value) {
        this.dateOfBirthBegins = value;
    }

    /**
     * Gets the value of the dateOfBirthEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirthEnds() {
        return dateOfBirthEnds;
    }

    /**
     * Sets the value of the dateOfBirthEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirthEnds(XMLGregorianCalendar value) {
        this.dateOfBirthEnds = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the cityComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getCityComparisonType() {
        return cityComparisonType;
    }

    /**
     * Sets the value of the cityComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setCityComparisonType(ComparisonType value) {
        this.cityComparisonType = value;
    }

    /**
     * Gets the value of the classification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassification() {
        return classification;
    }

    /**
     * Sets the value of the classification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassification(String value) {
        this.classification = value;
    }

    /**
     * Gets the value of the classificationComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getClassificationComparisonType() {
        return classificationComparisonType;
    }

    /**
     * Sets the value of the classificationComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setClassificationComparisonType(ComparisonType value) {
        this.classificationComparisonType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the descriptionComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getDescriptionComparisonType() {
        return descriptionComparisonType;
    }

    /**
     * Sets the value of the descriptionComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setDescriptionComparisonType(ComparisonType value) {
        this.descriptionComparisonType = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the emailAddressComparisontype property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getEmailAddressComparisontype() {
        return emailAddressComparisontype;
    }

    /**
     * Sets the value of the emailAddressComparisontype property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setEmailAddressComparisontype(ComparisonType value) {
        this.emailAddressComparisontype = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the nameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getNameComparisonType() {
        return nameComparisonType;
    }

    /**
     * Sets the value of the nameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setNameComparisonType(ComparisonType value) {
        this.nameComparisonType = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the phoneComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getPhoneComparisonType() {
        return phoneComparisonType;
    }

    /**
     * Sets the value of the phoneComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setPhoneComparisonType(ComparisonType value) {
        this.phoneComparisonType = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

}
