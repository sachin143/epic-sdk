
package com.appliedsystems.schemas.epic.sdk._2017._01._common._activity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._01._common._activity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccountServicingContactItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/", "AccountServicingContactItems");
    private final static QName _AccountServicingContactItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/", "AccountServicingContactItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._01._common._activity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccountServicingContactItems }
     * 
     */
    public AccountServicingContactItems createAccountServicingContactItems() {
        return new AccountServicingContactItems();
    }

    /**
     * Create an instance of {@link AccountServicingContactItem }
     * 
     */
    public AccountServicingContactItem createAccountServicingContactItem() {
        return new AccountServicingContactItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountServicingContactItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/", name = "AccountServicingContactItems")
    public JAXBElement<AccountServicingContactItems> createAccountServicingContactItems(AccountServicingContactItems value) {
        return new JAXBElement<AccountServicingContactItems>(_AccountServicingContactItems_QNAME, AccountServicingContactItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountServicingContactItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/", name = "AccountServicingContactItem")
    public JAXBElement<AccountServicingContactItem> createAccountServicingContactItem(AccountServicingContactItem value) {
        return new JAXBElement<AccountServicingContactItem>(_AccountServicingContactItem_QNAME, AccountServicingContactItem.class, null, value);
    }

}
