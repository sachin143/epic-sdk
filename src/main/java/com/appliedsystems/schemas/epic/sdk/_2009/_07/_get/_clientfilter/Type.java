
package com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Insured"/>
 *     &lt;enumeration value="Prospects"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Type", namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_clientfilter/")
@XmlEnum
public enum Type {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Insured")
    INSURED("Insured"),
    @XmlEnumValue("Prospects")
    PROSPECTS("Prospects");
    private final String value;

    Type(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Type fromValue(String v) {
        for (Type c: Type.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
