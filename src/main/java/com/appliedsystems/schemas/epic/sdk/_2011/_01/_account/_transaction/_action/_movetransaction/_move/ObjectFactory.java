
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/", "TransactionItem");
    private final static QName _TransactionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/", "TransactionItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionItem }
     * 
     */
    public TransactionItem createTransactionItem() {
        return new TransactionItem();
    }

    /**
     * Create an instance of {@link TransactionItems }
     * 
     */
    public TransactionItems createTransactionItems() {
        return new TransactionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/", name = "TransactionItem")
    public JAXBElement<TransactionItem> createTransactionItem(TransactionItem value) {
        return new JAXBElement<TransactionItem>(_TransactionItem_QNAME, TransactionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/", name = "TransactionItems")
    public JAXBElement<TransactionItems> createTransactionItems(TransactionItems value) {
        return new JAXBElement<TransactionItems>(_TransactionItems_QNAME, TransactionItems.class, null, value);
    }

}
