
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AmountToPay" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionItemID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="WriteOff" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WriteOffDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayItem", propOrder = {
    "amountToPay",
    "transactionItemID",
    "writeOff",
    "writeOffDescription"
})
public class PayItem {

    @XmlElement(name = "AmountToPay", nillable = true)
    protected BigDecimal amountToPay;
    @XmlElement(name = "TransactionItemID")
    protected Integer transactionItemID;
    @XmlElement(name = "WriteOff")
    protected Boolean writeOff;
    @XmlElement(name = "WriteOffDescription", nillable = true)
    protected String writeOffDescription;

    /**
     * Gets the value of the amountToPay property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountToPay() {
        return amountToPay;
    }

    /**
     * Sets the value of the amountToPay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountToPay(BigDecimal value) {
        this.amountToPay = value;
    }

    /**
     * Gets the value of the transactionItemID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionItemID() {
        return transactionItemID;
    }

    /**
     * Sets the value of the transactionItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionItemID(Integer value) {
        this.transactionItemID = value;
    }

    /**
     * Gets the value of the writeOff property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWriteOff() {
        return writeOff;
    }

    /**
     * Sets the value of the writeOff property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWriteOff(Boolean value) {
        this.writeOff = value;
    }

    /**
     * Gets the value of the writeOffDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWriteOffDescription() {
        return writeOffDescription;
    }

    /**
     * Sets the value of the writeOffDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWriteOffDescription(String value) {
        this.writeOffDescription = value;
    }

}
