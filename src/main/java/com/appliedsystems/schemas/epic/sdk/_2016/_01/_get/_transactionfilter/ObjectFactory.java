
package com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FutureItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", "FutureItems");
    private final static QName _Service_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", "Service");
    private final static QName _TransactionStatus_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", "TransactionStatus");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FutureItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", name = "FutureItems")
    public JAXBElement<FutureItems> createFutureItems(FutureItems value) {
        return new JAXBElement<FutureItems>(_FutureItems_QNAME, FutureItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Service }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", name = "Service")
    public JAXBElement<Service> createService(Service value) {
        return new JAXBElement<Service>(_Service_QNAME, Service.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/", name = "TransactionStatus")
    public JAXBElement<TransactionStatus> createTransactionStatus(TransactionStatus value) {
        return new JAXBElement<TransactionStatus>(_TransactionStatus_QNAME, TransactionStatus.class, null, value);
    }

}
