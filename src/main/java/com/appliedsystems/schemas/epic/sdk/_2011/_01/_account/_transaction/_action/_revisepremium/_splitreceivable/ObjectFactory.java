
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendInvoiceToItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_splitreceivable/", "SendInvoiceToItems");
    private final static QName _SendInvoiceToItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_splitreceivable/", "SendInvoiceToItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendInvoiceToItems }
     * 
     */
    public SendInvoiceToItems createSendInvoiceToItems() {
        return new SendInvoiceToItems();
    }

    /**
     * Create an instance of {@link SendInvoiceToItem }
     * 
     */
    public SendInvoiceToItem createSendInvoiceToItem() {
        return new SendInvoiceToItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendInvoiceToItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_splitreceivable/", name = "SendInvoiceToItems")
    public JAXBElement<SendInvoiceToItems> createSendInvoiceToItems(SendInvoiceToItems value) {
        return new JAXBElement<SendInvoiceToItems>(_SendInvoiceToItems_QNAME, SendInvoiceToItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendInvoiceToItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_splitreceivable/", name = "SendInvoiceToItem")
    public JAXBElement<SendInvoiceToItem> createSendInvoiceToItem(SendInvoiceToItem value) {
        return new JAXBElement<SendInvoiceToItem>(_SendInvoiceToItem_QNAME, SendInvoiceToItem.class, null, value);
    }

}
