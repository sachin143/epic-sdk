
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.Flags;


/**
 * <p>Java class for ProducerBrokerCommissionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProducerBrokerCommissionItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CommissionPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/_producerbrokercommissionitem/}Flags" minOccurs="0"/>
 *         &lt;element name="LookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductionCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayableDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenuePercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultProducerBrokerCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProducerBrokerCommissionItem", propOrder = {
    "commissionAmount",
    "commissionID",
    "commissionPercentage",
    "commissionTypeCode",
    "flag",
    "lookupCode",
    "orderNumber",
    "producerBrokerCode",
    "productionCredit",
    "contractID",
    "payableDueDate",
    "shareRevenueAgencyCode",
    "shareRevenueAgencyID",
    "shareRevenueAgencyName",
    "shareRevenueAmount",
    "shareRevenueBranchCode",
    "shareRevenueBranchID",
    "shareRevenueBranchName",
    "shareRevenueDepartmentCode",
    "shareRevenueDepartmentID",
    "shareRevenueDepartmentName",
    "shareRevenuePercent",
    "shareRevenueProfitCenterCode",
    "shareRevenueProfitCenterID",
    "shareRevenueProfitCenterName",
    "defaultProducerBrokerCommissionID"
})
public class ProducerBrokerCommissionItem {

    @XmlElement(name = "CommissionAmount")
    protected BigDecimal commissionAmount;
    @XmlElement(name = "CommissionID")
    protected Integer commissionID;
    @XmlElement(name = "CommissionPercentage")
    protected BigDecimal commissionPercentage;
    @XmlElement(name = "CommissionTypeCode", nillable = true)
    protected String commissionTypeCode;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "LookupCode", nillable = true)
    protected String lookupCode;
    @XmlElement(name = "OrderNumber")
    protected Integer orderNumber;
    @XmlElement(name = "ProducerBrokerCode", nillable = true)
    protected String producerBrokerCode;
    @XmlElement(name = "ProductionCredit")
    protected BigDecimal productionCredit;
    @XmlElement(name = "ContractID", nillable = true)
    protected Integer contractID;
    @XmlElement(name = "PayableDueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payableDueDate;
    @XmlElement(name = "ShareRevenueAgencyCode", nillable = true)
    protected String shareRevenueAgencyCode;
    @XmlElement(name = "ShareRevenueAgencyID")
    protected Integer shareRevenueAgencyID;
    @XmlElement(name = "ShareRevenueAgencyName", nillable = true)
    protected String shareRevenueAgencyName;
    @XmlElement(name = "ShareRevenueAmount", nillable = true)
    protected BigDecimal shareRevenueAmount;
    @XmlElement(name = "ShareRevenueBranchCode", nillable = true)
    protected String shareRevenueBranchCode;
    @XmlElement(name = "ShareRevenueBranchID")
    protected Integer shareRevenueBranchID;
    @XmlElement(name = "ShareRevenueBranchName", nillable = true)
    protected String shareRevenueBranchName;
    @XmlElement(name = "ShareRevenueDepartmentCode", nillable = true)
    protected String shareRevenueDepartmentCode;
    @XmlElement(name = "ShareRevenueDepartmentID")
    protected Integer shareRevenueDepartmentID;
    @XmlElement(name = "ShareRevenueDepartmentName", nillable = true)
    protected String shareRevenueDepartmentName;
    @XmlElement(name = "ShareRevenuePercent", nillable = true)
    protected BigDecimal shareRevenuePercent;
    @XmlElement(name = "ShareRevenueProfitCenterCode", nillable = true)
    protected String shareRevenueProfitCenterCode;
    @XmlElement(name = "ShareRevenueProfitCenterID")
    protected Integer shareRevenueProfitCenterID;
    @XmlElement(name = "ShareRevenueProfitCenterName", nillable = true)
    protected String shareRevenueProfitCenterName;
    @XmlElement(name = "DefaultProducerBrokerCommissionID", nillable = true)
    protected Integer defaultProducerBrokerCommissionID;

    /**
     * Gets the value of the commissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionAmount() {
        return commissionAmount;
    }

    /**
     * Sets the value of the commissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionAmount(BigDecimal value) {
        this.commissionAmount = value;
    }

    /**
     * Gets the value of the commissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCommissionID() {
        return commissionID;
    }

    /**
     * Sets the value of the commissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCommissionID(Integer value) {
        this.commissionID = value;
    }

    /**
     * Gets the value of the commissionPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionPercentage() {
        return commissionPercentage;
    }

    /**
     * Sets the value of the commissionPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionPercentage(BigDecimal value) {
        this.commissionPercentage = value;
    }

    /**
     * Gets the value of the commissionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionTypeCode() {
        return commissionTypeCode;
    }

    /**
     * Sets the value of the commissionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionTypeCode(String value) {
        this.commissionTypeCode = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the lookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookupCode() {
        return lookupCode;
    }

    /**
     * Sets the value of the lookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookupCode(String value) {
        this.lookupCode = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the producerBrokerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerBrokerCode() {
        return producerBrokerCode;
    }

    /**
     * Sets the value of the producerBrokerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerBrokerCode(String value) {
        this.producerBrokerCode = value;
    }

    /**
     * Gets the value of the productionCredit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductionCredit() {
        return productionCredit;
    }

    /**
     * Sets the value of the productionCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductionCredit(BigDecimal value) {
        this.productionCredit = value;
    }

    /**
     * Gets the value of the contractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContractID() {
        return contractID;
    }

    /**
     * Sets the value of the contractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContractID(Integer value) {
        this.contractID = value;
    }

    /**
     * Gets the value of the payableDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayableDueDate() {
        return payableDueDate;
    }

    /**
     * Sets the value of the payableDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayableDueDate(XMLGregorianCalendar value) {
        this.payableDueDate = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyCode() {
        return shareRevenueAgencyCode;
    }

    /**
     * Sets the value of the shareRevenueAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyCode(String value) {
        this.shareRevenueAgencyCode = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueAgencyID() {
        return shareRevenueAgencyID;
    }

    /**
     * Sets the value of the shareRevenueAgencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueAgencyID(Integer value) {
        this.shareRevenueAgencyID = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyName() {
        return shareRevenueAgencyName;
    }

    /**
     * Sets the value of the shareRevenueAgencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyName(String value) {
        this.shareRevenueAgencyName = value;
    }

    /**
     * Gets the value of the shareRevenueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShareRevenueAmount() {
        return shareRevenueAmount;
    }

    /**
     * Sets the value of the shareRevenueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShareRevenueAmount(BigDecimal value) {
        this.shareRevenueAmount = value;
    }

    /**
     * Gets the value of the shareRevenueBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchCode() {
        return shareRevenueBranchCode;
    }

    /**
     * Sets the value of the shareRevenueBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchCode(String value) {
        this.shareRevenueBranchCode = value;
    }

    /**
     * Gets the value of the shareRevenueBranchID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueBranchID() {
        return shareRevenueBranchID;
    }

    /**
     * Sets the value of the shareRevenueBranchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueBranchID(Integer value) {
        this.shareRevenueBranchID = value;
    }

    /**
     * Gets the value of the shareRevenueBranchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchName() {
        return shareRevenueBranchName;
    }

    /**
     * Sets the value of the shareRevenueBranchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchName(String value) {
        this.shareRevenueBranchName = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentCode() {
        return shareRevenueDepartmentCode;
    }

    /**
     * Sets the value of the shareRevenueDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentCode(String value) {
        this.shareRevenueDepartmentCode = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueDepartmentID() {
        return shareRevenueDepartmentID;
    }

    /**
     * Sets the value of the shareRevenueDepartmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueDepartmentID(Integer value) {
        this.shareRevenueDepartmentID = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentName() {
        return shareRevenueDepartmentName;
    }

    /**
     * Sets the value of the shareRevenueDepartmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentName(String value) {
        this.shareRevenueDepartmentName = value;
    }

    /**
     * Gets the value of the shareRevenuePercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShareRevenuePercent() {
        return shareRevenuePercent;
    }

    /**
     * Sets the value of the shareRevenuePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShareRevenuePercent(BigDecimal value) {
        this.shareRevenuePercent = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterCode() {
        return shareRevenueProfitCenterCode;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterCode(String value) {
        this.shareRevenueProfitCenterCode = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueProfitCenterID() {
        return shareRevenueProfitCenterID;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueProfitCenterID(Integer value) {
        this.shareRevenueProfitCenterID = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterName() {
        return shareRevenueProfitCenterName;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterName(String value) {
        this.shareRevenueProfitCenterName = value;
    }

    /**
     * Gets the value of the defaultProducerBrokerCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultProducerBrokerCommissionID() {
        return defaultProducerBrokerCommissionID;
    }

    /**
     * Sets the value of the defaultProducerBrokerCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultProducerBrokerCommissionID(Integer value) {
        this.defaultProducerBrokerCommissionID = value;
    }

}
