
package com.appliedsystems.schemas.epic.sdk._2009._07._account;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.AgencyStructureItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.AttachedToItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.FileDetailItems;


/**
 * <p>Java class for Attachment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Attachment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyStructures" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/}AgencyStructureItems" minOccurs="0"/>
 *         &lt;element name="AttachedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AttachedTos" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/}AttachedToItems" minOccurs="0"/>
 *         &lt;element name="AttachmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EditedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Files" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/}FileDetailItems" minOccurs="0"/>
 *         &lt;element name="Folder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneratedByCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InactiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IsInactive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReceivedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SecurityAccessLevelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Summary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsertedByCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpdatedByCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientAccessExpireOnDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ClientAccessible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubFolder3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DoNotPurge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DoNotPurgeExpireOnDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EmployeeSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReplaceEmployeeSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", propOrder = {
    "accountID",
    "accountTypeCode",
    "agencyStructures",
    "attachedDate",
    "attachedTos",
    "attachmentID",
    "comments",
    "description",
    "editedDate",
    "files",
    "folder",
    "generatedByCode",
    "inactiveDate",
    "isInactive",
    "isSystemGenerated",
    "receivedDate",
    "securityAccessLevelCode",
    "subFolder1",
    "subFolder2",
    "summary",
    "insertedByCode",
    "updatedByCode",
    "clientAccessExpireOnDate",
    "clientAccessible",
    "subFolder3",
    "subFolder4",
    "subFolder5",
    "accessedDate",
    "doNotPurge",
    "doNotPurgeExpireOnDate",
    "employeeSignature",
    "replaceEmployeeSignature"
})
public class Attachment {

    @XmlElement(name = "AccountID")
    protected Integer accountID;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "AgencyStructures", nillable = true)
    protected AgencyStructureItems agencyStructures;
    @XmlElement(name = "AttachedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar attachedDate;
    @XmlElement(name = "AttachedTos", nillable = true)
    protected AttachedToItems attachedTos;
    @XmlElement(name = "AttachmentID")
    protected Integer attachmentID;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EditedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar editedDate;
    @XmlElement(name = "Files", nillable = true)
    protected FileDetailItems files;
    @XmlElement(name = "Folder", nillable = true)
    protected String folder;
    @XmlElement(name = "GeneratedByCode", nillable = true)
    protected String generatedByCode;
    @XmlElement(name = "InactiveDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar inactiveDate;
    @XmlElement(name = "IsInactive")
    protected Boolean isInactive;
    @XmlElement(name = "IsSystemGenerated")
    protected Boolean isSystemGenerated;
    @XmlElement(name = "ReceivedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receivedDate;
    @XmlElement(name = "SecurityAccessLevelCode", nillable = true)
    protected String securityAccessLevelCode;
    @XmlElement(name = "SubFolder1", nillable = true)
    protected String subFolder1;
    @XmlElement(name = "SubFolder2", nillable = true)
    protected String subFolder2;
    @XmlElement(name = "Summary", nillable = true)
    protected String summary;
    @XmlElement(name = "InsertedByCode", nillable = true)
    protected String insertedByCode;
    @XmlElement(name = "UpdatedByCode", nillable = true)
    protected String updatedByCode;
    @XmlElement(name = "ClientAccessExpireOnDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar clientAccessExpireOnDate;
    @XmlElement(name = "ClientAccessible")
    protected Boolean clientAccessible;
    @XmlElement(name = "SubFolder3", nillable = true)
    protected String subFolder3;
    @XmlElement(name = "SubFolder4", nillable = true)
    protected String subFolder4;
    @XmlElement(name = "SubFolder5", nillable = true)
    protected String subFolder5;
    @XmlElement(name = "AccessedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar accessedDate;
    @XmlElement(name = "DoNotPurge", nillable = true)
    protected Boolean doNotPurge;
    @XmlElement(name = "DoNotPurgeExpireOnDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar doNotPurgeExpireOnDate;
    @XmlElement(name = "EmployeeSignature", nillable = true)
    protected Boolean employeeSignature;
    @XmlElement(name = "ReplaceEmployeeSignature")
    protected Boolean replaceEmployeeSignature;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the agencyStructures property.
     * 
     * @return
     *     possible object is
     *     {@link AgencyStructureItems }
     *     
     */
    public AgencyStructureItems getAgencyStructures() {
        return agencyStructures;
    }

    /**
     * Sets the value of the agencyStructures property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyStructureItems }
     *     
     */
    public void setAgencyStructures(AgencyStructureItems value) {
        this.agencyStructures = value;
    }

    /**
     * Gets the value of the attachedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAttachedDate() {
        return attachedDate;
    }

    /**
     * Sets the value of the attachedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAttachedDate(XMLGregorianCalendar value) {
        this.attachedDate = value;
    }

    /**
     * Gets the value of the attachedTos property.
     * 
     * @return
     *     possible object is
     *     {@link AttachedToItems }
     *     
     */
    public AttachedToItems getAttachedTos() {
        return attachedTos;
    }

    /**
     * Sets the value of the attachedTos property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachedToItems }
     *     
     */
    public void setAttachedTos(AttachedToItems value) {
        this.attachedTos = value;
    }

    /**
     * Gets the value of the attachmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttachmentID() {
        return attachmentID;
    }

    /**
     * Sets the value of the attachmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttachmentID(Integer value) {
        this.attachmentID = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the editedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEditedDate() {
        return editedDate;
    }

    /**
     * Sets the value of the editedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEditedDate(XMLGregorianCalendar value) {
        this.editedDate = value;
    }

    /**
     * Gets the value of the files property.
     * 
     * @return
     *     possible object is
     *     {@link FileDetailItems }
     *     
     */
    public FileDetailItems getFiles() {
        return files;
    }

    /**
     * Sets the value of the files property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileDetailItems }
     *     
     */
    public void setFiles(FileDetailItems value) {
        this.files = value;
    }

    /**
     * Gets the value of the folder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Sets the value of the folder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolder(String value) {
        this.folder = value;
    }

    /**
     * Gets the value of the generatedByCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneratedByCode() {
        return generatedByCode;
    }

    /**
     * Sets the value of the generatedByCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneratedByCode(String value) {
        this.generatedByCode = value;
    }

    /**
     * Gets the value of the inactiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInactiveDate() {
        return inactiveDate;
    }

    /**
     * Sets the value of the inactiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInactiveDate(XMLGregorianCalendar value) {
        this.inactiveDate = value;
    }

    /**
     * Gets the value of the isInactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInactive() {
        return isInactive;
    }

    /**
     * Sets the value of the isInactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInactive(Boolean value) {
        this.isInactive = value;
    }

    /**
     * Gets the value of the isSystemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Sets the value of the isSystemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSystemGenerated(Boolean value) {
        this.isSystemGenerated = value;
    }

    /**
     * Gets the value of the receivedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceivedDate() {
        return receivedDate;
    }

    /**
     * Sets the value of the receivedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceivedDate(XMLGregorianCalendar value) {
        this.receivedDate = value;
    }

    /**
     * Gets the value of the securityAccessLevelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityAccessLevelCode() {
        return securityAccessLevelCode;
    }

    /**
     * Sets the value of the securityAccessLevelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityAccessLevelCode(String value) {
        this.securityAccessLevelCode = value;
    }

    /**
     * Gets the value of the subFolder1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder1() {
        return subFolder1;
    }

    /**
     * Sets the value of the subFolder1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder1(String value) {
        this.subFolder1 = value;
    }

    /**
     * Gets the value of the subFolder2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder2() {
        return subFolder2;
    }

    /**
     * Sets the value of the subFolder2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder2(String value) {
        this.subFolder2 = value;
    }

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummary(String value) {
        this.summary = value;
    }

    /**
     * Gets the value of the insertedByCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsertedByCode() {
        return insertedByCode;
    }

    /**
     * Sets the value of the insertedByCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsertedByCode(String value) {
        this.insertedByCode = value;
    }

    /**
     * Gets the value of the updatedByCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdatedByCode() {
        return updatedByCode;
    }

    /**
     * Sets the value of the updatedByCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdatedByCode(String value) {
        this.updatedByCode = value;
    }

    /**
     * Gets the value of the clientAccessExpireOnDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClientAccessExpireOnDate() {
        return clientAccessExpireOnDate;
    }

    /**
     * Sets the value of the clientAccessExpireOnDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClientAccessExpireOnDate(XMLGregorianCalendar value) {
        this.clientAccessExpireOnDate = value;
    }

    /**
     * Gets the value of the clientAccessible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClientAccessible() {
        return clientAccessible;
    }

    /**
     * Sets the value of the clientAccessible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClientAccessible(Boolean value) {
        this.clientAccessible = value;
    }

    /**
     * Gets the value of the subFolder3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder3() {
        return subFolder3;
    }

    /**
     * Sets the value of the subFolder3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder3(String value) {
        this.subFolder3 = value;
    }

    /**
     * Gets the value of the subFolder4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder4() {
        return subFolder4;
    }

    /**
     * Sets the value of the subFolder4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder4(String value) {
        this.subFolder4 = value;
    }

    /**
     * Gets the value of the subFolder5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder5() {
        return subFolder5;
    }

    /**
     * Sets the value of the subFolder5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder5(String value) {
        this.subFolder5 = value;
    }

    /**
     * Gets the value of the accessedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccessedDate() {
        return accessedDate;
    }

    /**
     * Sets the value of the accessedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccessedDate(XMLGregorianCalendar value) {
        this.accessedDate = value;
    }

    /**
     * Gets the value of the doNotPurge property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoNotPurge() {
        return doNotPurge;
    }

    /**
     * Sets the value of the doNotPurge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoNotPurge(Boolean value) {
        this.doNotPurge = value;
    }

    /**
     * Gets the value of the doNotPurgeExpireOnDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDoNotPurgeExpireOnDate() {
        return doNotPurgeExpireOnDate;
    }

    /**
     * Sets the value of the doNotPurgeExpireOnDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDoNotPurgeExpireOnDate(XMLGregorianCalendar value) {
        this.doNotPurgeExpireOnDate = value;
    }

    /**
     * Gets the value of the employeeSignature property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEmployeeSignature() {
        return employeeSignature;
    }

    /**
     * Sets the value of the employeeSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmployeeSignature(Boolean value) {
        this.employeeSignature = value;
    }

    /**
     * Gets the value of the replaceEmployeeSignature property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReplaceEmployeeSignature() {
        return replaceEmployeeSignature;
    }

    /**
     * Sets the value of the replaceEmployeeSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReplaceEmployeeSignature(Boolean value) {
        this.replaceEmployeeSignature = value;
    }

}
