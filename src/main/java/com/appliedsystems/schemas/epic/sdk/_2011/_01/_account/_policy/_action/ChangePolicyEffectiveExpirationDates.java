
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ServiceSummaryItems;


/**
 * <p>Java class for ChangePolicyEffectiveExpirationDates complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangePolicyEffectiveExpirationDates">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeEffectiveDateTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ChangeExpirationDateTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_changepolicyeffectiveexpirationdates/}ServiceSummaryItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangePolicyEffectiveExpirationDates", propOrder = {
    "changeEffectiveDateTo",
    "changeExpirationDateTo",
    "policyID",
    "lineID",
    "serviceSummaryItems"
})
public class ChangePolicyEffectiveExpirationDates {

    @XmlElement(name = "ChangeEffectiveDateTo")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar changeEffectiveDateTo;
    @XmlElement(name = "ChangeExpirationDateTo")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar changeExpirationDateTo;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "LineID")
    protected Integer lineID;
    @XmlElement(name = "ServiceSummaryItems", nillable = true)
    protected ServiceSummaryItems serviceSummaryItems;

    /**
     * Gets the value of the changeEffectiveDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeEffectiveDateTo() {
        return changeEffectiveDateTo;
    }

    /**
     * Sets the value of the changeEffectiveDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeEffectiveDateTo(XMLGregorianCalendar value) {
        this.changeEffectiveDateTo = value;
    }

    /**
     * Gets the value of the changeExpirationDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeExpirationDateTo() {
        return changeExpirationDateTo;
    }

    /**
     * Sets the value of the changeExpirationDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeExpirationDateTo(XMLGregorianCalendar value) {
        this.changeExpirationDateTo = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the serviceSummaryItems property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceSummaryItems }
     *     
     */
    public ServiceSummaryItems getServiceSummaryItems() {
        return serviceSummaryItems;
    }

    /**
     * Sets the value of the serviceSummaryItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceSummaryItems }
     *     
     */
    public void setServiceSummaryItems(ServiceSummaryItems value) {
        this.serviceSummaryItems = value;
    }

}
