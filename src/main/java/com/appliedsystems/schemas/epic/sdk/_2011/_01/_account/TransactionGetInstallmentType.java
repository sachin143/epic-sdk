
package com.appliedsystems.schemas.epic.sdk._2011._01._account;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionGetInstallmentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionGetInstallmentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="InstallmentSummary"/>
 *     &lt;enumeration value="SplitReceivable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionGetInstallmentType")
@XmlEnum
public enum TransactionGetInstallmentType {

    @XmlEnumValue("InstallmentSummary")
    INSTALLMENT_SUMMARY("InstallmentSummary"),
    @XmlEnumValue("SplitReceivable")
    SPLIT_RECEIVABLE("SplitReceivable");
    private final String value;

    TransactionGetInstallmentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionGetInstallmentType fromValue(String v) {
        for (TransactionGetInstallmentType c: TransactionGetInstallmentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
