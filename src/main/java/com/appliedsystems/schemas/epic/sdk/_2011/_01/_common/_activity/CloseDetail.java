
package com.appliedsystems.schemas.epic.sdk._2011._01._common._activity;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CloseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CloseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActualCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ActualTimeHours" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ActualTimeMinutes" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ClosedReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreOpenTasks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AverageCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AverageTimeHours" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="AverageTimeMinutes" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CloseDetail", propOrder = {
    "actualCost",
    "actualTimeHours",
    "actualTimeMinutes",
    "closedReason",
    "closedStatus",
    "ignoreOpenTasks",
    "averageCost",
    "averageTimeHours",
    "averageTimeMinutes",
    "duration"
})
public class CloseDetail {

    @XmlElement(name = "ActualCost", nillable = true)
    protected BigDecimal actualCost;
    @XmlElement(name = "ActualTimeHours", nillable = true)
    protected Short actualTimeHours;
    @XmlElement(name = "ActualTimeMinutes", nillable = true)
    protected Short actualTimeMinutes;
    @XmlElement(name = "ClosedReason", nillable = true)
    protected String closedReason;
    @XmlElement(name = "ClosedStatus", nillable = true)
    protected String closedStatus;
    @XmlElement(name = "IgnoreOpenTasks")
    protected Boolean ignoreOpenTasks;
    @XmlElement(name = "AverageCost", nillable = true)
    protected BigDecimal averageCost;
    @XmlElement(name = "AverageTimeHours", nillable = true)
    protected Short averageTimeHours;
    @XmlElement(name = "AverageTimeMinutes", nillable = true)
    protected Short averageTimeMinutes;
    @XmlElement(name = "Duration", nillable = true)
    protected Integer duration;

    /**
     * Gets the value of the actualCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActualCost() {
        return actualCost;
    }

    /**
     * Sets the value of the actualCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActualCost(BigDecimal value) {
        this.actualCost = value;
    }

    /**
     * Gets the value of the actualTimeHours property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getActualTimeHours() {
        return actualTimeHours;
    }

    /**
     * Sets the value of the actualTimeHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setActualTimeHours(Short value) {
        this.actualTimeHours = value;
    }

    /**
     * Gets the value of the actualTimeMinutes property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getActualTimeMinutes() {
        return actualTimeMinutes;
    }

    /**
     * Sets the value of the actualTimeMinutes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setActualTimeMinutes(Short value) {
        this.actualTimeMinutes = value;
    }

    /**
     * Gets the value of the closedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedReason() {
        return closedReason;
    }

    /**
     * Sets the value of the closedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedReason(String value) {
        this.closedReason = value;
    }

    /**
     * Gets the value of the closedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedStatus() {
        return closedStatus;
    }

    /**
     * Sets the value of the closedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedStatus(String value) {
        this.closedStatus = value;
    }

    /**
     * Gets the value of the ignoreOpenTasks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreOpenTasks() {
        return ignoreOpenTasks;
    }

    /**
     * Sets the value of the ignoreOpenTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreOpenTasks(Boolean value) {
        this.ignoreOpenTasks = value;
    }

    /**
     * Gets the value of the averageCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageCost() {
        return averageCost;
    }

    /**
     * Sets the value of the averageCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageCost(BigDecimal value) {
        this.averageCost = value;
    }

    /**
     * Gets the value of the averageTimeHours property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAverageTimeHours() {
        return averageTimeHours;
    }

    /**
     * Sets the value of the averageTimeHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAverageTimeHours(Short value) {
        this.averageTimeHours = value;
    }

    /**
     * Gets the value of the averageTimeMinutes property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAverageTimeMinutes() {
        return averageTimeMinutes;
    }

    /**
     * Sets the value of the averageTimeMinutes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAverageTimeMinutes(Short value) {
        this.averageTimeMinutes = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDuration(Integer value) {
        this.duration = value;
    }

}
