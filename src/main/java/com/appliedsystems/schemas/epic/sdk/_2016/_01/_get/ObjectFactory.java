
package com.appliedsystems.schemas.epic.sdk._2016._01._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2016._01._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SalesTeamGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/", "SalesTeamGetResult");
    private final static QName _TransactionFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/", "TransactionFilter");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2016._01._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionFilter }
     * 
     */
    public TransactionFilter createTransactionFilter() {
        return new TransactionFilter();
    }

    /**
     * Create an instance of {@link SalesTeamGetResult }
     * 
     */
    public SalesTeamGetResult createSalesTeamGetResult() {
        return new SalesTeamGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesTeamGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/", name = "SalesTeamGetResult")
    public JAXBElement<SalesTeamGetResult> createSalesTeamGetResult(SalesTeamGetResult value) {
        return new JAXBElement<SalesTeamGetResult>(_SalesTeamGetResult_QNAME, SalesTeamGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/", name = "TransactionFilter")
    public JAXBElement<TransactionFilter> createTransactionFilter(TransactionFilter value) {
        return new JAXBElement<TransactionFilter>(_TransactionFilter_QNAME, TransactionFilter.class, null, value);
    }

}
