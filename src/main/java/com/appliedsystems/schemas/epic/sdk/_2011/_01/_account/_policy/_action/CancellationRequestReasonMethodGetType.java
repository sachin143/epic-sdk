
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancellationRequestReasonMethodGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CancellationRequestReasonMethodGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CancellationID"/>
 *     &lt;enumeration value="ServiceSummaryID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CancellationRequestReasonMethodGetType")
@XmlEnum
public enum CancellationRequestReasonMethodGetType {

    @XmlEnumValue("CancellationID")
    CANCELLATION_ID("CancellationID"),
    @XmlEnumValue("ServiceSummaryID")
    SERVICE_SUMMARY_ID("ServiceSummaryID");
    private final String value;

    CancellationRequestReasonMethodGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CancellationRequestReasonMethodGetType fromValue(String v) {
        for (CancellationRequestReasonMethodGetType c: CancellationRequestReasonMethodGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
