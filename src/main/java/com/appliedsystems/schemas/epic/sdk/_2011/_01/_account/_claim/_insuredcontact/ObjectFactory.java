
package com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Contact_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", "Contact");
    private final static QName _Insured_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", "Insured");
    private final static QName _Spouse_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", "Spouse");
    private final static QName _AuthorityContacted_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", "AuthorityContacted");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Spouse }
     * 
     */
    public Spouse createSpouse() {
        return new Spouse();
    }

    /**
     * Create an instance of {@link AuthorityContacted }
     * 
     */
    public AuthorityContacted createAuthorityContacted() {
        return new AuthorityContacted();
    }

    /**
     * Create an instance of {@link Insured }
     * 
     */
    public Insured createInsured() {
        return new Insured();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", name = "Contact")
    public JAXBElement<Contact> createContact(Contact value) {
        return new JAXBElement<Contact>(_Contact_QNAME, Contact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Insured }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", name = "Insured")
    public JAXBElement<Insured> createInsured(Insured value) {
        return new JAXBElement<Insured>(_Insured_QNAME, Insured.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Spouse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", name = "Spouse")
    public JAXBElement<Spouse> createSpouse(Spouse value) {
        return new JAXBElement<Spouse>(_Spouse_QNAME, Spouse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorityContacted }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/", name = "AuthorityContacted")
    public JAXBElement<AuthorityContacted> createAuthorityContacted(AuthorityContacted value) {
        return new JAXBElement<AuthorityContacted>(_AuthorityContacted_QNAME, AuthorityContacted.class, null, value);
    }

}
