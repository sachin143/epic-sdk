
package com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for BasicSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BasicSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClosedStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedWithOpenTasks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DefaultActivityAs" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="EmailIntegrationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HiddenActivity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ManualClosedStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManualUnsuccessfulReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OwnerTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecificOwnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartActivityDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="UnsuccessfulReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShowIndioDetail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicSettings", propOrder = {
    "closedStatusCode",
    "closedWithOpenTasks",
    "defaultActivityAs",
    "emailIntegrationCode",
    "hiddenActivity",
    "manualClosedStatusCode",
    "manualUnsuccessfulReason",
    "orderNumber",
    "ownerTypeCode",
    "priorityCode",
    "specificOwnerCode",
    "startActivityDays",
    "unsuccessfulReason",
    "showIndioDetail"
})
public class BasicSettings {

    @XmlElement(name = "ClosedStatusCode", nillable = true)
    protected String closedStatusCode;
    @XmlElement(name = "ClosedWithOpenTasks", nillable = true)
    protected Boolean closedWithOpenTasks;
    @XmlElement(name = "DefaultActivityAs", nillable = true)
    protected OptionType defaultActivityAs;
    @XmlElement(name = "EmailIntegrationCode", nillable = true)
    protected String emailIntegrationCode;
    @XmlElement(name = "HiddenActivity", nillable = true)
    protected Boolean hiddenActivity;
    @XmlElement(name = "ManualClosedStatusCode", nillable = true)
    protected String manualClosedStatusCode;
    @XmlElement(name = "ManualUnsuccessfulReason", nillable = true)
    protected String manualUnsuccessfulReason;
    @XmlElement(name = "OrderNumber", nillable = true)
    protected Integer orderNumber;
    @XmlElement(name = "OwnerTypeCode", nillable = true)
    protected String ownerTypeCode;
    @XmlElement(name = "PriorityCode", nillable = true)
    protected String priorityCode;
    @XmlElement(name = "SpecificOwnerCode", nillable = true)
    protected String specificOwnerCode;
    @XmlElement(name = "StartActivityDays", nillable = true)
    protected Integer startActivityDays;
    @XmlElement(name = "UnsuccessfulReason", nillable = true)
    protected String unsuccessfulReason;
    @XmlElement(name = "ShowIndioDetail", nillable = true)
    protected Boolean showIndioDetail;

    /**
     * Gets the value of the closedStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedStatusCode() {
        return closedStatusCode;
    }

    /**
     * Sets the value of the closedStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedStatusCode(String value) {
        this.closedStatusCode = value;
    }

    /**
     * Gets the value of the closedWithOpenTasks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClosedWithOpenTasks() {
        return closedWithOpenTasks;
    }

    /**
     * Sets the value of the closedWithOpenTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClosedWithOpenTasks(Boolean value) {
        this.closedWithOpenTasks = value;
    }

    /**
     * Gets the value of the defaultActivityAs property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getDefaultActivityAs() {
        return defaultActivityAs;
    }

    /**
     * Sets the value of the defaultActivityAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setDefaultActivityAs(OptionType value) {
        this.defaultActivityAs = value;
    }

    /**
     * Gets the value of the emailIntegrationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailIntegrationCode() {
        return emailIntegrationCode;
    }

    /**
     * Sets the value of the emailIntegrationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailIntegrationCode(String value) {
        this.emailIntegrationCode = value;
    }

    /**
     * Gets the value of the hiddenActivity property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHiddenActivity() {
        return hiddenActivity;
    }

    /**
     * Sets the value of the hiddenActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHiddenActivity(Boolean value) {
        this.hiddenActivity = value;
    }

    /**
     * Gets the value of the manualClosedStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualClosedStatusCode() {
        return manualClosedStatusCode;
    }

    /**
     * Sets the value of the manualClosedStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualClosedStatusCode(String value) {
        this.manualClosedStatusCode = value;
    }

    /**
     * Gets the value of the manualUnsuccessfulReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualUnsuccessfulReason() {
        return manualUnsuccessfulReason;
    }

    /**
     * Sets the value of the manualUnsuccessfulReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualUnsuccessfulReason(String value) {
        this.manualUnsuccessfulReason = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the ownerTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerTypeCode() {
        return ownerTypeCode;
    }

    /**
     * Sets the value of the ownerTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerTypeCode(String value) {
        this.ownerTypeCode = value;
    }

    /**
     * Gets the value of the priorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorityCode() {
        return priorityCode;
    }

    /**
     * Sets the value of the priorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorityCode(String value) {
        this.priorityCode = value;
    }

    /**
     * Gets the value of the specificOwnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificOwnerCode() {
        return specificOwnerCode;
    }

    /**
     * Sets the value of the specificOwnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificOwnerCode(String value) {
        this.specificOwnerCode = value;
    }

    /**
     * Gets the value of the startActivityDays property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStartActivityDays() {
        return startActivityDays;
    }

    /**
     * Sets the value of the startActivityDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStartActivityDays(Integer value) {
        this.startActivityDays = value;
    }

    /**
     * Gets the value of the unsuccessfulReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnsuccessfulReason() {
        return unsuccessfulReason;
    }

    /**
     * Sets the value of the unsuccessfulReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnsuccessfulReason(String value) {
        this.unsuccessfulReason = value;
    }

    /**
     * Gets the value of the showIndioDetail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowIndioDetail() {
        return showIndioDetail;
    }

    /**
     * Sets the value of the showIndioDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowIndioDetail(Boolean value) {
        this.showIndioDetail = value;
    }

}
