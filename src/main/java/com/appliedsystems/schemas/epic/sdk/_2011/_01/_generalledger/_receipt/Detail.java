
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItems;


/**
 * <p>Java class for Detail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Detail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetailItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/}DetailItems" minOccurs="0"/>
 *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Detail", propOrder = {
    "detailItemsValue",
    "total"
})
public class Detail {

    @XmlElement(name = "DetailItemsValue", nillable = true)
    protected DetailItems detailItemsValue;
    @XmlElement(name = "Total")
    protected BigDecimal total;

    /**
     * Gets the value of the detailItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link DetailItems }
     *     
     */
    public DetailItems getDetailItemsValue() {
        return detailItemsValue;
    }

    /**
     * Sets the value of the detailItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailItems }
     *     
     */
    public void setDetailItemsValue(DetailItems value) {
        this.detailItemsValue = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotal(BigDecimal value) {
        this.total = value;
    }

}
