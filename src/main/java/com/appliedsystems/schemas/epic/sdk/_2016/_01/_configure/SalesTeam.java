
package com.appliedsystems.schemas.epic.sdk._2016._01._configure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.MemberItems;


/**
 * <p>Java class for SalesTeam complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesTeam">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedToBroker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AssociatedToEmployee" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ManagerLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManagerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManagerTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemberItems" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/}MemberItems" minOccurs="0"/>
 *         &lt;element name="SalesTeamID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SalesTeamName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesTeam", propOrder = {
    "associatedToBroker",
    "associatedToEmployee",
    "isActive",
    "managerLookupCode",
    "managerName",
    "managerTypeCode",
    "memberItems",
    "salesTeamID",
    "salesTeamName"
})
public class SalesTeam {

    @XmlElement(name = "AssociatedToBroker")
    protected Boolean associatedToBroker;
    @XmlElement(name = "AssociatedToEmployee")
    protected Boolean associatedToEmployee;
    @XmlElement(name = "IsActive")
    protected Boolean isActive;
    @XmlElement(name = "ManagerLookupCode", nillable = true)
    protected String managerLookupCode;
    @XmlElement(name = "ManagerName", nillable = true)
    protected String managerName;
    @XmlElement(name = "ManagerTypeCode", nillable = true)
    protected String managerTypeCode;
    @XmlElement(name = "MemberItems", nillable = true)
    protected MemberItems memberItems;
    @XmlElement(name = "SalesTeamID")
    protected Integer salesTeamID;
    @XmlElement(name = "SalesTeamName", nillable = true)
    protected String salesTeamName;

    /**
     * Gets the value of the associatedToBroker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAssociatedToBroker() {
        return associatedToBroker;
    }

    /**
     * Sets the value of the associatedToBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssociatedToBroker(Boolean value) {
        this.associatedToBroker = value;
    }

    /**
     * Gets the value of the associatedToEmployee property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAssociatedToEmployee() {
        return associatedToEmployee;
    }

    /**
     * Sets the value of the associatedToEmployee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssociatedToEmployee(Boolean value) {
        this.associatedToEmployee = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the managerLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerLookupCode() {
        return managerLookupCode;
    }

    /**
     * Sets the value of the managerLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerLookupCode(String value) {
        this.managerLookupCode = value;
    }

    /**
     * Gets the value of the managerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerName() {
        return managerName;
    }

    /**
     * Sets the value of the managerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerName(String value) {
        this.managerName = value;
    }

    /**
     * Gets the value of the managerTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerTypeCode() {
        return managerTypeCode;
    }

    /**
     * Sets the value of the managerTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManagerTypeCode(String value) {
        this.managerTypeCode = value;
    }

    /**
     * Gets the value of the memberItems property.
     * 
     * @return
     *     possible object is
     *     {@link MemberItems }
     *     
     */
    public MemberItems getMemberItems() {
        return memberItems;
    }

    /**
     * Sets the value of the memberItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberItems }
     *     
     */
    public void setMemberItems(MemberItems value) {
        this.memberItems = value;
    }

    /**
     * Gets the value of the salesTeamID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSalesTeamID() {
        return salesTeamID;
    }

    /**
     * Sets the value of the salesTeamID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSalesTeamID(Integer value) {
        this.salesTeamID = value;
    }

    /**
     * Gets the value of the salesTeamName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesTeamName() {
        return salesTeamName;
    }

    /**
     * Sets the value of the salesTeamName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesTeamName(String value) {
        this.salesTeamName = value;
    }

}
