
package com.appliedsystems.schemas.epic.sdk._2011._01._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAdjustCommission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAdjustCommission">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdjustCommission" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/}AdjustCommission" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAdjustCommission", propOrder = {
    "adjustCommissions"
})
public class ArrayOfAdjustCommission {

    @XmlElement(name = "AdjustCommission", nillable = true)
    protected List<AdjustCommission> adjustCommissions;

    /**
     * Gets the value of the adjustCommissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustCommissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustCommissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjustCommission }
     * 
     * 
     */
    public List<AdjustCommission> getAdjustCommissions() {
        if (adjustCommissions == null) {
            adjustCommissions = new ArrayList<AdjustCommission>();
        }
        return this.adjustCommissions;
    }

}
