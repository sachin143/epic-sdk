
package com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FutureItems.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FutureItems">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Exclude"/>
 *     &lt;enumeration value="Only"/>
 *     &lt;enumeration value="Include"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FutureItems", namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/")
@XmlEnum
public enum FutureItems {

    @XmlEnumValue("Exclude")
    EXCLUDE("Exclude"),
    @XmlEnumValue("Only")
    ONLY("Only"),
    @XmlEnumValue("Include")
    INCLUDE("Include");
    private final String value;

    FutureItems(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FutureItems fromValue(String v) {
        for (FutureItems c: FutureItems.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
