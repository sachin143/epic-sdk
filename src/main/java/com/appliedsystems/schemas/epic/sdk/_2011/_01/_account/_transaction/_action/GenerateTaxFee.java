
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.GenerateTaxFeeItems;


/**
 * <p>Java class for GenerateTaxFee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateTaxFee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerateTaxFeeItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/}GenerateTaxFeeItems" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IngoreIfTaxFeesAlreadyBeenGenerated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateTaxFee", propOrder = {
    "generateTaxFeeItems",
    "transactionID",
    "ingoreIfTaxFeesAlreadyBeenGenerated"
})
public class GenerateTaxFee {

    @XmlElement(name = "GenerateTaxFeeItems", nillable = true)
    protected GenerateTaxFeeItems generateTaxFeeItems;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "IngoreIfTaxFeesAlreadyBeenGenerated", nillable = true)
    protected Boolean ingoreIfTaxFeesAlreadyBeenGenerated;

    /**
     * Gets the value of the generateTaxFeeItems property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateTaxFeeItems }
     *     
     */
    public GenerateTaxFeeItems getGenerateTaxFeeItems() {
        return generateTaxFeeItems;
    }

    /**
     * Sets the value of the generateTaxFeeItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateTaxFeeItems }
     *     
     */
    public void setGenerateTaxFeeItems(GenerateTaxFeeItems value) {
        this.generateTaxFeeItems = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the ingoreIfTaxFeesAlreadyBeenGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIngoreIfTaxFeesAlreadyBeenGenerated() {
        return ingoreIfTaxFeesAlreadyBeenGenerated;
    }

    /**
     * Sets the value of the ingoreIfTaxFeesAlreadyBeenGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIngoreIfTaxFeesAlreadyBeenGenerated(Boolean value) {
        this.ingoreIfTaxFeesAlreadyBeenGenerated = value;
    }

}
