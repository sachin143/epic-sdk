
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InvoiceItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", "InvoiceItems");
    private final static QName _TaxFee_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", "TaxFee");
    private final static QName _InvoiceItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", "InvoiceItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InvoiceItem }
     * 
     */
    public InvoiceItem createInvoiceItem() {
        return new InvoiceItem();
    }

    /**
     * Create an instance of {@link InvoiceItems }
     * 
     */
    public InvoiceItems createInvoiceItems() {
        return new InvoiceItems();
    }

    /**
     * Create an instance of {@link TaxFee }
     * 
     */
    public TaxFee createTaxFee() {
        return new TaxFee();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", name = "InvoiceItems")
    public JAXBElement<InvoiceItems> createInvoiceItems(InvoiceItems value) {
        return new JAXBElement<InvoiceItems>(_InvoiceItems_QNAME, InvoiceItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", name = "TaxFee")
    public JAXBElement<TaxFee> createTaxFee(TaxFee value) {
        return new JAXBElement<TaxFee>(_TaxFee_QNAME, TaxFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/", name = "InvoiceItem")
    public JAXBElement<InvoiceItem> createInvoiceItem(InvoiceItem value) {
        return new JAXBElement<InvoiceItem>(_InvoiceItem_QNAME, InvoiceItem.class, null, value);
    }

}
