
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.EligibleVoucherItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.SearchWhereComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.SearchWhereFilterType;


/**
 * <p>Java class for EligibleVouchers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EligibleVouchers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EligibleVoucherItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/}EligibleVoucherItems" minOccurs="0"/>
 *         &lt;element name="SearchWhereComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/}SearchWhereComparisonType" minOccurs="0"/>
 *         &lt;element name="SearchWhereFilterField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchWhereFilterField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchWhereFilterType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/}SearchWhereFilterType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EligibleVouchers", propOrder = {
    "eligibleVoucherItemsValue",
    "searchWhereComparisonType",
    "searchWhereFilterField1",
    "searchWhereFilterField2",
    "searchWhereFilterType"
})
public class EligibleVouchers {

    @XmlElement(name = "EligibleVoucherItemsValue", nillable = true)
    protected EligibleVoucherItems eligibleVoucherItemsValue;
    @XmlElement(name = "SearchWhereComparisonType")
    @XmlSchemaType(name = "string")
    protected SearchWhereComparisonType searchWhereComparisonType;
    @XmlElement(name = "SearchWhereFilterField1", nillable = true)
    protected String searchWhereFilterField1;
    @XmlElement(name = "SearchWhereFilterField2", nillable = true)
    protected String searchWhereFilterField2;
    @XmlElement(name = "SearchWhereFilterType")
    @XmlSchemaType(name = "string")
    protected SearchWhereFilterType searchWhereFilterType;

    /**
     * Gets the value of the eligibleVoucherItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link EligibleVoucherItems }
     *     
     */
    public EligibleVoucherItems getEligibleVoucherItemsValue() {
        return eligibleVoucherItemsValue;
    }

    /**
     * Sets the value of the eligibleVoucherItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link EligibleVoucherItems }
     *     
     */
    public void setEligibleVoucherItemsValue(EligibleVoucherItems value) {
        this.eligibleVoucherItemsValue = value;
    }

    /**
     * Gets the value of the searchWhereComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link SearchWhereComparisonType }
     *     
     */
    public SearchWhereComparisonType getSearchWhereComparisonType() {
        return searchWhereComparisonType;
    }

    /**
     * Sets the value of the searchWhereComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchWhereComparisonType }
     *     
     */
    public void setSearchWhereComparisonType(SearchWhereComparisonType value) {
        this.searchWhereComparisonType = value;
    }

    /**
     * Gets the value of the searchWhereFilterField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchWhereFilterField1() {
        return searchWhereFilterField1;
    }

    /**
     * Sets the value of the searchWhereFilterField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchWhereFilterField1(String value) {
        this.searchWhereFilterField1 = value;
    }

    /**
     * Gets the value of the searchWhereFilterField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchWhereFilterField2() {
        return searchWhereFilterField2;
    }

    /**
     * Sets the value of the searchWhereFilterField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchWhereFilterField2(String value) {
        this.searchWhereFilterField2 = value;
    }

    /**
     * Gets the value of the searchWhereFilterType property.
     * 
     * @return
     *     possible object is
     *     {@link SearchWhereFilterType }
     *     
     */
    public SearchWhereFilterType getSearchWhereFilterType() {
        return searchWhereFilterType;
    }

    /**
     * Sets the value of the searchWhereFilterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchWhereFilterType }
     *     
     */
    public void setSearchWhereFilterType(SearchWhereFilterType value) {
        this.searchWhereFilterType = value;
    }

}
