
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.AgencyDefinedCodeItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ServicingRolesItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.History;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ProducerBrokerCommissions;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ServiceSummaryItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.Billing;


/**
 * <p>Java class for Line complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Line">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyDefinedCategoryItems" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}AgencyDefinedCodeItems" minOccurs="0"/>
 *         &lt;element name="AgreementID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AnnualizedCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AnnualizedPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BilledCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BilledPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BillingModeOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="BillingValue" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/_line/}Billing" minOccurs="0"/>
 *         &lt;element name="DefaultCommissionAgreement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Downloaded" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EstimatedCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EstimatedMonthlyCommission" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EstimatedMonthlyPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EstimatedMonthlyPremiumAsOfDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EstimatedPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="HistoryValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/}History" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssuingLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LineInformationLineID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayableContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PremiumPayableLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PremiumPayableTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCommissionsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/}ProducerBrokerCommissions" minOccurs="0"/>
 *         &lt;element name="ProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RisksInsured" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RisksInsuredDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceSummaries" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/}ServiceSummaryItems" minOccurs="0"/>
 *         &lt;element name="ServicingContacts" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}ServicingRolesItems" minOccurs="0"/>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TotalEligible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalEligibleDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreAtLeastOnePrBrCommissionRequirement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="OverrideCommissionAgreementPercentageOrAmount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PlanOptionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreLineStatusCodeOnUpdate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PolicyDetailEngineID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Line", propOrder = {
    "agencyCommissionAmount",
    "agencyCommissionPercent",
    "agencyCommissionType",
    "agencyDefinedCategoryItems",
    "agreementID",
    "annualizedCommission",
    "annualizedPremium",
    "billedCommission",
    "billedPremium",
    "billingModeOption",
    "billingValue",
    "defaultCommissionAgreement",
    "downloaded",
    "estimatedCommission",
    "estimatedMonthlyCommission",
    "estimatedMonthlyPremium",
    "estimatedMonthlyPremiumAsOfDate",
    "estimatedPremium",
    "historyValue",
    "issuingCompanyLookupCode",
    "issuingLocationCode",
    "lineGUID",
    "lineID",
    "lineInformationLineID",
    "lineTypeCode",
    "lineTypeDescription",
    "payableContractID",
    "policyID",
    "premiumPayableLookupCode",
    "premiumPayableTypeCode",
    "producerBrokerCommissionsValue",
    "profitCenterCode",
    "risksInsured",
    "risksInsuredDescription",
    "serviceSummaries",
    "servicingContacts",
    "statusCode",
    "timestamp",
    "totalEligible",
    "totalEligibleDescription",
    "ignoreAtLeastOnePrBrCommissionRequirement",
    "overrideCommissionAgreementPercentageOrAmount",
    "planOptionName",
    "ignoreLineStatusCodeOnUpdate",
    "policyDetailEngineID"
})
public class Line {

    @XmlElement(name = "AgencyCommissionAmount", nillable = true)
    protected BigDecimal agencyCommissionAmount;
    @XmlElement(name = "AgencyCommissionPercent", nillable = true)
    protected BigDecimal agencyCommissionPercent;
    @XmlElement(name = "AgencyCommissionType", nillable = true)
    protected String agencyCommissionType;
    @XmlElement(name = "AgencyDefinedCategoryItems", nillable = true)
    protected AgencyDefinedCodeItems agencyDefinedCategoryItems;
    @XmlElement(name = "AgreementID")
    protected Integer agreementID;
    @XmlElement(name = "AnnualizedCommission", nillable = true)
    protected BigDecimal annualizedCommission;
    @XmlElement(name = "AnnualizedPremium", nillable = true)
    protected BigDecimal annualizedPremium;
    @XmlElement(name = "BilledCommission", nillable = true)
    protected BigDecimal billedCommission;
    @XmlElement(name = "BilledPremium", nillable = true)
    protected BigDecimal billedPremium;
    @XmlElement(name = "BillingModeOption", nillable = true)
    protected OptionType billingModeOption;
    @XmlElement(name = "BillingValue", nillable = true)
    protected Billing billingValue;
    @XmlElement(name = "DefaultCommissionAgreement")
    protected Boolean defaultCommissionAgreement;
    @XmlElement(name = "Downloaded", nillable = true)
    protected BigDecimal downloaded;
    @XmlElement(name = "EstimatedCommission", nillable = true)
    protected BigDecimal estimatedCommission;
    @XmlElement(name = "EstimatedMonthlyCommission", nillable = true)
    protected BigDecimal estimatedMonthlyCommission;
    @XmlElement(name = "EstimatedMonthlyPremium", nillable = true)
    protected BigDecimal estimatedMonthlyPremium;
    @XmlElement(name = "EstimatedMonthlyPremiumAsOfDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar estimatedMonthlyPremiumAsOfDate;
    @XmlElement(name = "EstimatedPremium", nillable = true)
    protected BigDecimal estimatedPremium;
    @XmlElement(name = "HistoryValue", nillable = true)
    protected History historyValue;
    @XmlElement(name = "IssuingCompanyLookupCode", nillable = true)
    protected String issuingCompanyLookupCode;
    @XmlElement(name = "IssuingLocationCode", nillable = true)
    protected String issuingLocationCode;
    @XmlElement(name = "LineGUID", nillable = true)
    protected String lineGUID;
    @XmlElement(name = "LineID")
    protected Integer lineID;
    @XmlElement(name = "LineInformationLineID", nillable = true)
    protected String lineInformationLineID;
    @XmlElement(name = "LineTypeCode", nillable = true)
    protected String lineTypeCode;
    @XmlElement(name = "LineTypeDescription", nillable = true)
    protected String lineTypeDescription;
    @XmlElement(name = "PayableContractID", nillable = true)
    protected Integer payableContractID;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "PremiumPayableLookupCode", nillable = true)
    protected String premiumPayableLookupCode;
    @XmlElement(name = "PremiumPayableTypeCode", nillable = true)
    protected String premiumPayableTypeCode;
    @XmlElement(name = "ProducerBrokerCommissionsValue", nillable = true)
    protected ProducerBrokerCommissions producerBrokerCommissionsValue;
    @XmlElement(name = "ProfitCenterCode", nillable = true)
    protected String profitCenterCode;
    @XmlElement(name = "RisksInsured", nillable = true)
    protected Integer risksInsured;
    @XmlElement(name = "RisksInsuredDescription", nillable = true)
    protected String risksInsuredDescription;
    @XmlElement(name = "ServiceSummaries", nillable = true)
    protected ServiceSummaryItems serviceSummaries;
    @XmlElement(name = "ServicingContacts", nillable = true)
    protected ServicingRolesItems servicingContacts;
    @XmlElement(name = "StatusCode", nillable = true)
    protected String statusCode;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "TotalEligible", nillable = true)
    protected Integer totalEligible;
    @XmlElement(name = "TotalEligibleDescription", nillable = true)
    protected String totalEligibleDescription;
    @XmlElement(name = "IgnoreAtLeastOnePrBrCommissionRequirement")
    protected Boolean ignoreAtLeastOnePrBrCommissionRequirement;
    @XmlElement(name = "OverrideCommissionAgreementPercentageOrAmount")
    protected Boolean overrideCommissionAgreementPercentageOrAmount;
    @XmlElement(name = "PlanOptionName", nillable = true)
    protected String planOptionName;
    @XmlElement(name = "IgnoreLineStatusCodeOnUpdate")
    protected Boolean ignoreLineStatusCodeOnUpdate;
    @XmlElement(name = "PolicyDetailEngineID", nillable = true)
    protected String policyDetailEngineID;

    /**
     * Gets the value of the agencyCommissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionAmount() {
        return agencyCommissionAmount;
    }

    /**
     * Sets the value of the agencyCommissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionAmount(BigDecimal value) {
        this.agencyCommissionAmount = value;
    }

    /**
     * Gets the value of the agencyCommissionPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionPercent() {
        return agencyCommissionPercent;
    }

    /**
     * Sets the value of the agencyCommissionPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionPercent(BigDecimal value) {
        this.agencyCommissionPercent = value;
    }

    /**
     * Gets the value of the agencyCommissionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCommissionType() {
        return agencyCommissionType;
    }

    /**
     * Sets the value of the agencyCommissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCommissionType(String value) {
        this.agencyCommissionType = value;
    }

    /**
     * Gets the value of the agencyDefinedCategoryItems property.
     * 
     * @return
     *     possible object is
     *     {@link AgencyDefinedCodeItems }
     *     
     */
    public AgencyDefinedCodeItems getAgencyDefinedCategoryItems() {
        return agencyDefinedCategoryItems;
    }

    /**
     * Sets the value of the agencyDefinedCategoryItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyDefinedCodeItems }
     *     
     */
    public void setAgencyDefinedCategoryItems(AgencyDefinedCodeItems value) {
        this.agencyDefinedCategoryItems = value;
    }

    /**
     * Gets the value of the agreementID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgreementID() {
        return agreementID;
    }

    /**
     * Sets the value of the agreementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgreementID(Integer value) {
        this.agreementID = value;
    }

    /**
     * Gets the value of the annualizedCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnnualizedCommission() {
        return annualizedCommission;
    }

    /**
     * Sets the value of the annualizedCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnnualizedCommission(BigDecimal value) {
        this.annualizedCommission = value;
    }

    /**
     * Gets the value of the annualizedPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnnualizedPremium() {
        return annualizedPremium;
    }

    /**
     * Sets the value of the annualizedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnnualizedPremium(BigDecimal value) {
        this.annualizedPremium = value;
    }

    /**
     * Gets the value of the billedCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBilledCommission() {
        return billedCommission;
    }

    /**
     * Sets the value of the billedCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBilledCommission(BigDecimal value) {
        this.billedCommission = value;
    }

    /**
     * Gets the value of the billedPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBilledPremium() {
        return billedPremium;
    }

    /**
     * Sets the value of the billedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBilledPremium(BigDecimal value) {
        this.billedPremium = value;
    }

    /**
     * Gets the value of the billingModeOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBillingModeOption() {
        return billingModeOption;
    }

    /**
     * Sets the value of the billingModeOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBillingModeOption(OptionType value) {
        this.billingModeOption = value;
    }

    /**
     * Gets the value of the billingValue property.
     * 
     * @return
     *     possible object is
     *     {@link Billing }
     *     
     */
    public Billing getBillingValue() {
        return billingValue;
    }

    /**
     * Sets the value of the billingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Billing }
     *     
     */
    public void setBillingValue(Billing value) {
        this.billingValue = value;
    }

    /**
     * Gets the value of the defaultCommissionAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultCommissionAgreement() {
        return defaultCommissionAgreement;
    }

    /**
     * Sets the value of the defaultCommissionAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultCommissionAgreement(Boolean value) {
        this.defaultCommissionAgreement = value;
    }

    /**
     * Gets the value of the downloaded property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDownloaded() {
        return downloaded;
    }

    /**
     * Sets the value of the downloaded property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDownloaded(BigDecimal value) {
        this.downloaded = value;
    }

    /**
     * Gets the value of the estimatedCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedCommission() {
        return estimatedCommission;
    }

    /**
     * Sets the value of the estimatedCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedCommission(BigDecimal value) {
        this.estimatedCommission = value;
    }

    /**
     * Gets the value of the estimatedMonthlyCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedMonthlyCommission() {
        return estimatedMonthlyCommission;
    }

    /**
     * Sets the value of the estimatedMonthlyCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedMonthlyCommission(BigDecimal value) {
        this.estimatedMonthlyCommission = value;
    }

    /**
     * Gets the value of the estimatedMonthlyPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedMonthlyPremium() {
        return estimatedMonthlyPremium;
    }

    /**
     * Sets the value of the estimatedMonthlyPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedMonthlyPremium(BigDecimal value) {
        this.estimatedMonthlyPremium = value;
    }

    /**
     * Gets the value of the estimatedMonthlyPremiumAsOfDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEstimatedMonthlyPremiumAsOfDate() {
        return estimatedMonthlyPremiumAsOfDate;
    }

    /**
     * Sets the value of the estimatedMonthlyPremiumAsOfDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEstimatedMonthlyPremiumAsOfDate(XMLGregorianCalendar value) {
        this.estimatedMonthlyPremiumAsOfDate = value;
    }

    /**
     * Gets the value of the estimatedPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedPremium() {
        return estimatedPremium;
    }

    /**
     * Sets the value of the estimatedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedPremium(BigDecimal value) {
        this.estimatedPremium = value;
    }

    /**
     * Gets the value of the historyValue property.
     * 
     * @return
     *     possible object is
     *     {@link History }
     *     
     */
    public History getHistoryValue() {
        return historyValue;
    }

    /**
     * Sets the value of the historyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link History }
     *     
     */
    public void setHistoryValue(History value) {
        this.historyValue = value;
    }

    /**
     * Gets the value of the issuingCompanyLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyLookupCode() {
        return issuingCompanyLookupCode;
    }

    /**
     * Sets the value of the issuingCompanyLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyLookupCode(String value) {
        this.issuingCompanyLookupCode = value;
    }

    /**
     * Gets the value of the issuingLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingLocationCode() {
        return issuingLocationCode;
    }

    /**
     * Sets the value of the issuingLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingLocationCode(String value) {
        this.issuingLocationCode = value;
    }

    /**
     * Gets the value of the lineGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineGUID() {
        return lineGUID;
    }

    /**
     * Sets the value of the lineGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineGUID(String value) {
        this.lineGUID = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the lineInformationLineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineInformationLineID() {
        return lineInformationLineID;
    }

    /**
     * Sets the value of the lineInformationLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineInformationLineID(String value) {
        this.lineInformationLineID = value;
    }

    /**
     * Gets the value of the lineTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineTypeCode() {
        return lineTypeCode;
    }

    /**
     * Sets the value of the lineTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineTypeCode(String value) {
        this.lineTypeCode = value;
    }

    /**
     * Gets the value of the lineTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineTypeDescription() {
        return lineTypeDescription;
    }

    /**
     * Sets the value of the lineTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineTypeDescription(String value) {
        this.lineTypeDescription = value;
    }

    /**
     * Gets the value of the payableContractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayableContractID() {
        return payableContractID;
    }

    /**
     * Sets the value of the payableContractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayableContractID(Integer value) {
        this.payableContractID = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the premiumPayableLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableLookupCode() {
        return premiumPayableLookupCode;
    }

    /**
     * Sets the value of the premiumPayableLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableLookupCode(String value) {
        this.premiumPayableLookupCode = value;
    }

    /**
     * Gets the value of the premiumPayableTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableTypeCode() {
        return premiumPayableTypeCode;
    }

    /**
     * Sets the value of the premiumPayableTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableTypeCode(String value) {
        this.premiumPayableTypeCode = value;
    }

    /**
     * Gets the value of the producerBrokerCommissionsValue property.
     * 
     * @return
     *     possible object is
     *     {@link ProducerBrokerCommissions }
     *     
     */
    public ProducerBrokerCommissions getProducerBrokerCommissionsValue() {
        return producerBrokerCommissionsValue;
    }

    /**
     * Sets the value of the producerBrokerCommissionsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProducerBrokerCommissions }
     *     
     */
    public void setProducerBrokerCommissionsValue(ProducerBrokerCommissions value) {
        this.producerBrokerCommissionsValue = value;
    }

    /**
     * Gets the value of the profitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCenterCode() {
        return profitCenterCode;
    }

    /**
     * Sets the value of the profitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCenterCode(String value) {
        this.profitCenterCode = value;
    }

    /**
     * Gets the value of the risksInsured property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRisksInsured() {
        return risksInsured;
    }

    /**
     * Sets the value of the risksInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRisksInsured(Integer value) {
        this.risksInsured = value;
    }

    /**
     * Gets the value of the risksInsuredDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRisksInsuredDescription() {
        return risksInsuredDescription;
    }

    /**
     * Sets the value of the risksInsuredDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRisksInsuredDescription(String value) {
        this.risksInsuredDescription = value;
    }

    /**
     * Gets the value of the serviceSummaries property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceSummaryItems }
     *     
     */
    public ServiceSummaryItems getServiceSummaries() {
        return serviceSummaries;
    }

    /**
     * Sets the value of the serviceSummaries property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceSummaryItems }
     *     
     */
    public void setServiceSummaries(ServiceSummaryItems value) {
        this.serviceSummaries = value;
    }

    /**
     * Gets the value of the servicingContacts property.
     * 
     * @return
     *     possible object is
     *     {@link ServicingRolesItems }
     *     
     */
    public ServicingRolesItems getServicingContacts() {
        return servicingContacts;
    }

    /**
     * Sets the value of the servicingContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicingRolesItems }
     *     
     */
    public void setServicingContacts(ServicingRolesItems value) {
        this.servicingContacts = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the totalEligible property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalEligible() {
        return totalEligible;
    }

    /**
     * Sets the value of the totalEligible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalEligible(Integer value) {
        this.totalEligible = value;
    }

    /**
     * Gets the value of the totalEligibleDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalEligibleDescription() {
        return totalEligibleDescription;
    }

    /**
     * Sets the value of the totalEligibleDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalEligibleDescription(String value) {
        this.totalEligibleDescription = value;
    }

    /**
     * Gets the value of the ignoreAtLeastOnePrBrCommissionRequirement property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAtLeastOnePrBrCommissionRequirement() {
        return ignoreAtLeastOnePrBrCommissionRequirement;
    }

    /**
     * Sets the value of the ignoreAtLeastOnePrBrCommissionRequirement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAtLeastOnePrBrCommissionRequirement(Boolean value) {
        this.ignoreAtLeastOnePrBrCommissionRequirement = value;
    }

    /**
     * Gets the value of the overrideCommissionAgreementPercentageOrAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideCommissionAgreementPercentageOrAmount() {
        return overrideCommissionAgreementPercentageOrAmount;
    }

    /**
     * Sets the value of the overrideCommissionAgreementPercentageOrAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCommissionAgreementPercentageOrAmount(Boolean value) {
        this.overrideCommissionAgreementPercentageOrAmount = value;
    }

    /**
     * Gets the value of the planOptionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanOptionName() {
        return planOptionName;
    }

    /**
     * Sets the value of the planOptionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanOptionName(String value) {
        this.planOptionName = value;
    }

    /**
     * Gets the value of the ignoreLineStatusCodeOnUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreLineStatusCodeOnUpdate() {
        return ignoreLineStatusCodeOnUpdate;
    }

    /**
     * Sets the value of the ignoreLineStatusCodeOnUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreLineStatusCodeOnUpdate(Boolean value) {
        this.ignoreLineStatusCodeOnUpdate = value;
    }

    /**
     * Gets the value of the policyDetailEngineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyDetailEngineID() {
        return policyDetailEngineID;
    }

    /**
     * Sets the value of the policyDetailEngineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyDetailEngineID(String value) {
        this.policyDetailEngineID = value;
    }

}
