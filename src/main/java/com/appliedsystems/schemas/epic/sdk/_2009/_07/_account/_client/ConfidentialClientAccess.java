
package com.appliedsystems.schemas.epic.sdk._2009._07._account._client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for ConfidentialClientAccess complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfidentialClientAccess">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConfidentialClientAccessAllSelectedOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="ConfidentialClientAccessItems" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}ConfidentialClientAccessItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfidentialClientAccess", propOrder = {
    "confidentialClientAccessAllSelectedOption",
    "confidentialClientAccessItems"
})
public class ConfidentialClientAccess {

    @XmlElement(name = "ConfidentialClientAccessAllSelectedOption", nillable = true)
    protected OptionType confidentialClientAccessAllSelectedOption;
    @XmlElement(name = "ConfidentialClientAccessItems", nillable = true)
    protected ConfidentialClientAccessItems confidentialClientAccessItems;

    /**
     * Gets the value of the confidentialClientAccessAllSelectedOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getConfidentialClientAccessAllSelectedOption() {
        return confidentialClientAccessAllSelectedOption;
    }

    /**
     * Sets the value of the confidentialClientAccessAllSelectedOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setConfidentialClientAccessAllSelectedOption(OptionType value) {
        this.confidentialClientAccessAllSelectedOption = value;
    }

    /**
     * Gets the value of the confidentialClientAccessItems property.
     * 
     * @return
     *     possible object is
     *     {@link ConfidentialClientAccessItems }
     *     
     */
    public ConfidentialClientAccessItems getConfidentialClientAccessItems() {
        return confidentialClientAccessItems;
    }

    /**
     * Sets the value of the confidentialClientAccessItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfidentialClientAccessItems }
     *     
     */
    public void setConfidentialClientAccessItems(ConfidentialClientAccessItems value) {
        this.confidentialClientAccessItems = value;
    }

}
