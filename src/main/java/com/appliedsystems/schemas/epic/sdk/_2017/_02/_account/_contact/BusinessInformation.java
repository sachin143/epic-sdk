
package com.appliedsystems.schemas.epic.sdk._2017._02._account._contact;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BusinessInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessTypeOtherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditBureauIDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditBureauNameCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditBureauOtherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DUNS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateBusinessStarted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ELTOEmployerReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELTOEmployerTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELTOExemptStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NAICS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NatureOfBusinessCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NatureOfBusinessOtherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberManagersMembers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessInformation", propOrder = {
    "businessTypeCode",
    "businessTypeOtherDescription",
    "creditBureauIDNumber",
    "creditBureauNameCode",
    "creditBureauOtherDescription",
    "duns",
    "dateBusinessStarted",
    "eltoEmployerReferenceNumber",
    "eltoEmployerTypeCode",
    "eltoExemptStatusCode",
    "naics",
    "natureOfBusinessCode",
    "natureOfBusinessOtherDescription",
    "numberManagersMembers",
    "sic"
})
public class BusinessInformation {

    @XmlElement(name = "BusinessTypeCode", nillable = true)
    protected String businessTypeCode;
    @XmlElement(name = "BusinessTypeOtherDescription", nillable = true)
    protected String businessTypeOtherDescription;
    @XmlElement(name = "CreditBureauIDNumber", nillable = true)
    protected String creditBureauIDNumber;
    @XmlElement(name = "CreditBureauNameCode", nillable = true)
    protected String creditBureauNameCode;
    @XmlElement(name = "CreditBureauOtherDescription", nillable = true)
    protected String creditBureauOtherDescription;
    @XmlElement(name = "DUNS", nillable = true)
    protected String duns;
    @XmlElement(name = "DateBusinessStarted", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateBusinessStarted;
    @XmlElement(name = "ELTOEmployerReferenceNumber", nillable = true)
    protected String eltoEmployerReferenceNumber;
    @XmlElement(name = "ELTOEmployerTypeCode", nillable = true)
    protected String eltoEmployerTypeCode;
    @XmlElement(name = "ELTOExemptStatusCode", nillable = true)
    protected String eltoExemptStatusCode;
    @XmlElement(name = "NAICS", nillable = true)
    protected String naics;
    @XmlElement(name = "NatureOfBusinessCode", nillable = true)
    protected String natureOfBusinessCode;
    @XmlElement(name = "NatureOfBusinessOtherDescription", nillable = true)
    protected String natureOfBusinessOtherDescription;
    @XmlElement(name = "NumberManagersMembers", nillable = true)
    protected Integer numberManagersMembers;
    @XmlElement(name = "SIC", nillable = true)
    protected String sic;

    /**
     * Gets the value of the businessTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTypeCode() {
        return businessTypeCode;
    }

    /**
     * Sets the value of the businessTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTypeCode(String value) {
        this.businessTypeCode = value;
    }

    /**
     * Gets the value of the businessTypeOtherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTypeOtherDescription() {
        return businessTypeOtherDescription;
    }

    /**
     * Sets the value of the businessTypeOtherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTypeOtherDescription(String value) {
        this.businessTypeOtherDescription = value;
    }

    /**
     * Gets the value of the creditBureauIDNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditBureauIDNumber() {
        return creditBureauIDNumber;
    }

    /**
     * Sets the value of the creditBureauIDNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditBureauIDNumber(String value) {
        this.creditBureauIDNumber = value;
    }

    /**
     * Gets the value of the creditBureauNameCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditBureauNameCode() {
        return creditBureauNameCode;
    }

    /**
     * Sets the value of the creditBureauNameCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditBureauNameCode(String value) {
        this.creditBureauNameCode = value;
    }

    /**
     * Gets the value of the creditBureauOtherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditBureauOtherDescription() {
        return creditBureauOtherDescription;
    }

    /**
     * Sets the value of the creditBureauOtherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditBureauOtherDescription(String value) {
        this.creditBureauOtherDescription = value;
    }

    /**
     * Gets the value of the duns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNS() {
        return duns;
    }

    /**
     * Sets the value of the duns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNS(String value) {
        this.duns = value;
    }

    /**
     * Gets the value of the dateBusinessStarted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateBusinessStarted() {
        return dateBusinessStarted;
    }

    /**
     * Sets the value of the dateBusinessStarted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateBusinessStarted(XMLGregorianCalendar value) {
        this.dateBusinessStarted = value;
    }

    /**
     * Gets the value of the eltoEmployerReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELTOEmployerReferenceNumber() {
        return eltoEmployerReferenceNumber;
    }

    /**
     * Sets the value of the eltoEmployerReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELTOEmployerReferenceNumber(String value) {
        this.eltoEmployerReferenceNumber = value;
    }

    /**
     * Gets the value of the eltoEmployerTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELTOEmployerTypeCode() {
        return eltoEmployerTypeCode;
    }

    /**
     * Sets the value of the eltoEmployerTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELTOEmployerTypeCode(String value) {
        this.eltoEmployerTypeCode = value;
    }

    /**
     * Gets the value of the eltoExemptStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELTOExemptStatusCode() {
        return eltoExemptStatusCode;
    }

    /**
     * Sets the value of the eltoExemptStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELTOExemptStatusCode(String value) {
        this.eltoExemptStatusCode = value;
    }

    /**
     * Gets the value of the naics property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAICS() {
        return naics;
    }

    /**
     * Sets the value of the naics property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAICS(String value) {
        this.naics = value;
    }

    /**
     * Gets the value of the natureOfBusinessCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatureOfBusinessCode() {
        return natureOfBusinessCode;
    }

    /**
     * Sets the value of the natureOfBusinessCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatureOfBusinessCode(String value) {
        this.natureOfBusinessCode = value;
    }

    /**
     * Gets the value of the natureOfBusinessOtherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatureOfBusinessOtherDescription() {
        return natureOfBusinessOtherDescription;
    }

    /**
     * Sets the value of the natureOfBusinessOtherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatureOfBusinessOtherDescription(String value) {
        this.natureOfBusinessOtherDescription = value;
    }

    /**
     * Gets the value of the numberManagersMembers property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberManagersMembers() {
        return numberManagersMembers;
    }

    /**
     * Sets the value of the numberManagersMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberManagersMembers(Integer value) {
        this.numberManagersMembers = value;
    }

    /**
     * Gets the value of the sic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSIC() {
        return sic;
    }

    /**
     * Sets the value of the sic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSIC(String value) {
        this.sic = value;
    }

}
