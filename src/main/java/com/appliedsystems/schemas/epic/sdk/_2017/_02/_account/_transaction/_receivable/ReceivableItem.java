
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ReceivableColumnItems;


/**
 * <p>Java class for ReceivableItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceivableItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceivableColumnItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem}ReceivableColumnItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceivableItem", propOrder = {
    "receivableColumnItems"
})
public class ReceivableItem {

    @XmlElement(name = "ReceivableColumnItems", nillable = true)
    protected ReceivableColumnItems receivableColumnItems;

    /**
     * Gets the value of the receivableColumnItems property.
     * 
     * @return
     *     possible object is
     *     {@link ReceivableColumnItems }
     *     
     */
    public ReceivableColumnItems getReceivableColumnItems() {
        return receivableColumnItems;
    }

    /**
     * Sets the value of the receivableColumnItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceivableColumnItems }
     *     
     */
    public void setReceivableColumnItems(ReceivableColumnItems value) {
        this.receivableColumnItems = value;
    }

}
