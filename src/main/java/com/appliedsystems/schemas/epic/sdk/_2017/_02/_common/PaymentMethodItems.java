
package com.appliedsystems.schemas.epic.sdk._2017._02._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentMethodItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethodItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentMethodItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/}PaymentMethodItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethodItems", propOrder = {
    "paymentMethodItems"
})
public class PaymentMethodItems {

    @XmlElement(name = "PaymentMethodItem", nillable = true)
    protected List<PaymentMethodItem> paymentMethodItems;

    /**
     * Gets the value of the paymentMethodItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentMethodItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentMethodItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentMethodItem }
     * 
     * 
     */
    public List<PaymentMethodItem> getPaymentMethodItems() {
        if (paymentMethodItems == null) {
            paymentMethodItems = new ArrayList<PaymentMethodItem>();
        }
        return this.paymentMethodItems;
    }

}
