
package com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SplitReceivableItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitReceivableItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SplitReceivableItem" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/}SplitReceivableItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitReceivableItems", propOrder = {
    "splitReceivableItems"
})
public class SplitReceivableItems {

    @XmlElement(name = "SplitReceivableItem", nillable = true)
    protected List<SplitReceivableItem> splitReceivableItems;

    /**
     * Gets the value of the splitReceivableItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitReceivableItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitReceivableItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SplitReceivableItem }
     * 
     * 
     */
    public List<SplitReceivableItem> getSplitReceivableItems() {
        if (splitReceivableItems == null) {
            splitReceivableItems = new ArrayList<SplitReceivableItem>();
        }
        return this.splitReceivableItems;
    }

}
