
package com.appliedsystems.schemas.epic.sdk._2016._01._configure;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2016._01._configure package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SalesTeam_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/", "SalesTeam");
    private final static QName _ArrayOfSalesTeam_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/", "ArrayOfSalesTeam");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2016._01._configure
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SalesTeam }
     * 
     */
    public SalesTeam createSalesTeam() {
        return new SalesTeam();
    }

    /**
     * Create an instance of {@link ArrayOfSalesTeam }
     * 
     */
    public ArrayOfSalesTeam createArrayOfSalesTeam() {
        return new ArrayOfSalesTeam();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesTeam }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/", name = "SalesTeam")
    public JAXBElement<SalesTeam> createSalesTeam(SalesTeam value) {
        return new JAXBElement<SalesTeam>(_SalesTeam_QNAME, SalesTeam.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSalesTeam }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/", name = "ArrayOfSalesTeam")
    public JAXBElement<ArrayOfSalesTeam> createArrayOfSalesTeam(ArrayOfSalesTeam value) {
        return new JAXBElement<ArrayOfSalesTeam>(_ArrayOfSalesTeam_QNAME, ArrayOfSalesTeam.class, null, value);
    }

}
