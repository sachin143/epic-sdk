
package com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaymentExpenseItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/_paymentexpense/", "PaymentExpenseItem");
    private final static QName _PaymentExpenseItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/_paymentexpense/", "PaymentExpenseItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentExpenseItem }
     * 
     */
    public PaymentExpenseItem createPaymentExpenseItem() {
        return new PaymentExpenseItem();
    }

    /**
     * Create an instance of {@link PaymentExpenseItems }
     * 
     */
    public PaymentExpenseItems createPaymentExpenseItems() {
        return new PaymentExpenseItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentExpenseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/_paymentexpense/", name = "PaymentExpenseItem")
    public JAXBElement<PaymentExpenseItem> createPaymentExpenseItem(PaymentExpenseItem value) {
        return new JAXBElement<PaymentExpenseItem>(_PaymentExpenseItem_QNAME, PaymentExpenseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentExpenseItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/_paymentexpense/", name = "PaymentExpenseItems")
    public JAXBElement<PaymentExpenseItems> createPaymentExpenseItems(PaymentExpenseItems value) {
        return new JAXBElement<PaymentExpenseItems>(_PaymentExpenseItems_QNAME, PaymentExpenseItems.class, null, value);
    }

}
