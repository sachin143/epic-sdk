
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.CashOnAccountItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.PayItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.PayStatementItems;


/**
 * <p>Java class for PayablesCommissions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayablesCommissions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CashOnAccountItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/}CashOnAccountItems" minOccurs="0"/>
 *         &lt;element name="PayItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/}PayItems" minOccurs="0"/>
 *         &lt;element name="PayStatementItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/}PayStatementItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayablesCommissions", propOrder = {
    "cashOnAccountItemsValue",
    "payItemsValue",
    "payStatementItemsValue"
})
public class PayablesCommissions {

    @XmlElement(name = "CashOnAccountItemsValue", nillable = true)
    protected CashOnAccountItems cashOnAccountItemsValue;
    @XmlElement(name = "PayItemsValue", nillable = true)
    protected PayItems payItemsValue;
    @XmlElement(name = "PayStatementItemsValue", nillable = true)
    protected PayStatementItems payStatementItemsValue;

    /**
     * Gets the value of the cashOnAccountItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link CashOnAccountItems }
     *     
     */
    public CashOnAccountItems getCashOnAccountItemsValue() {
        return cashOnAccountItemsValue;
    }

    /**
     * Sets the value of the cashOnAccountItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashOnAccountItems }
     *     
     */
    public void setCashOnAccountItemsValue(CashOnAccountItems value) {
        this.cashOnAccountItemsValue = value;
    }

    /**
     * Gets the value of the payItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link PayItems }
     *     
     */
    public PayItems getPayItemsValue() {
        return payItemsValue;
    }

    /**
     * Sets the value of the payItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayItems }
     *     
     */
    public void setPayItemsValue(PayItems value) {
        this.payItemsValue = value;
    }

    /**
     * Gets the value of the payStatementItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link PayStatementItems }
     *     
     */
    public PayStatementItems getPayStatementItemsValue() {
        return payStatementItemsValue;
    }

    /**
     * Sets the value of the payStatementItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayStatementItems }
     *     
     */
    public void setPayStatementItemsValue(PayStatementItems value) {
        this.payStatementItemsValue = value;
    }

}
