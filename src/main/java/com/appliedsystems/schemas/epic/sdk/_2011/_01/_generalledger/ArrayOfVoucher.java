
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfVoucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfVoucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Voucher" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}Voucher" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfVoucher", propOrder = {
    "vouchers"
})
public class ArrayOfVoucher {

    @XmlElement(name = "Voucher", nillable = true)
    protected List<Voucher> vouchers;

    /**
     * Gets the value of the vouchers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vouchers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVouchers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVouchers() {
        if (vouchers == null) {
            vouchers = new ArrayList<Voucher>();
        }
        return this.vouchers;
    }

}
