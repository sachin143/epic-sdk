
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.SendInvoiceToItems;


/**
 * <p>Java class for Invoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Invoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendInvoiceToOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="SendInvoiceTos" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_invoice/}SendInvoiceToItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Invoice", propOrder = {
    "sendInvoiceToOption",
    "sendInvoiceTos"
})
public class Invoice {

    @XmlElement(name = "SendInvoiceToOption", nillable = true)
    protected OptionType sendInvoiceToOption;
    @XmlElement(name = "SendInvoiceTos", nillable = true)
    protected SendInvoiceToItems sendInvoiceTos;

    /**
     * Gets the value of the sendInvoiceToOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getSendInvoiceToOption() {
        return sendInvoiceToOption;
    }

    /**
     * Sets the value of the sendInvoiceToOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setSendInvoiceToOption(OptionType value) {
        this.sendInvoiceToOption = value;
    }

    /**
     * Gets the value of the sendInvoiceTos property.
     * 
     * @return
     *     possible object is
     *     {@link SendInvoiceToItems }
     *     
     */
    public SendInvoiceToItems getSendInvoiceTos() {
        return sendInvoiceTos;
    }

    /**
     * Sets the value of the sendInvoiceTos property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendInvoiceToItems }
     *     
     */
    public void setSendInvoiceTos(SendInvoiceToItems value) {
        this.sendInvoiceTos = value;
    }

}
