
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfPolicyLineType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", "ArrayOfPolicyLineType");
    private final static QName _PolicyLineTypeGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", "PolicyLineTypeGetType");
    private final static QName _PolicyStatus_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", "PolicyStatus");
    private final static QName _PolicyLineType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", "PolicyLineType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PolicyStatus }
     * 
     */
    public PolicyStatus createPolicyStatus() {
        return new PolicyStatus();
    }

    /**
     * Create an instance of {@link ArrayOfPolicyLineType }
     * 
     */
    public ArrayOfPolicyLineType createArrayOfPolicyLineType() {
        return new ArrayOfPolicyLineType();
    }

    /**
     * Create an instance of {@link PolicyLineType }
     * 
     */
    public PolicyLineType createPolicyLineType() {
        return new PolicyLineType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPolicyLineType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", name = "ArrayOfPolicyLineType")
    public JAXBElement<ArrayOfPolicyLineType> createArrayOfPolicyLineType(ArrayOfPolicyLineType value) {
        return new JAXBElement<ArrayOfPolicyLineType>(_ArrayOfPolicyLineType_QNAME, ArrayOfPolicyLineType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyLineTypeGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", name = "PolicyLineTypeGetType")
    public JAXBElement<PolicyLineTypeGetType> createPolicyLineTypeGetType(PolicyLineTypeGetType value) {
        return new JAXBElement<PolicyLineTypeGetType>(_PolicyLineTypeGetType_QNAME, PolicyLineTypeGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", name = "PolicyStatus")
    public JAXBElement<PolicyStatus> createPolicyStatus(PolicyStatus value) {
        return new JAXBElement<PolicyStatus>(_PolicyStatus_QNAME, PolicyStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyLineType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/", name = "PolicyLineType")
    public JAXBElement<PolicyLineType> createPolicyLineType(PolicyLineType value) {
        return new JAXBElement<PolicyLineType>(_PolicyLineType_QNAME, PolicyLineType.class, null, value);
    }

}
