
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/}TransactionItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionItems", propOrder = {
    "transactionItems"
})
public class TransactionItems {

    @XmlElement(name = "TransactionItem", nillable = true)
    protected List<TransactionItem> transactionItems;

    /**
     * Gets the value of the transactionItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionItem }
     * 
     * 
     */
    public List<TransactionItem> getTransactionItems() {
        if (transactionItems == null) {
            transactionItems = new ArrayList<TransactionItem>();
        }
        return this.transactionItems;
    }

}
