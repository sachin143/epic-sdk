
package com.appliedsystems.schemas.epic.sdk._2018._01._account;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2018._01._account package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SplitReceivableTemplate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/", "SplitReceivableTemplate");
    private final static QName _ArrayOfSplitReceivableTemplate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/", "ArrayOfSplitReceivableTemplate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2018._01._account
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SplitReceivableTemplate }
     * 
     */
    public SplitReceivableTemplate createSplitReceivableTemplate() {
        return new SplitReceivableTemplate();
    }

    /**
     * Create an instance of {@link ArrayOfSplitReceivableTemplate }
     * 
     */
    public ArrayOfSplitReceivableTemplate createArrayOfSplitReceivableTemplate() {
        return new ArrayOfSplitReceivableTemplate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableTemplate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/", name = "SplitReceivableTemplate")
    public JAXBElement<SplitReceivableTemplate> createSplitReceivableTemplate(SplitReceivableTemplate value) {
        return new JAXBElement<SplitReceivableTemplate>(_SplitReceivableTemplate_QNAME, SplitReceivableTemplate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSplitReceivableTemplate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/", name = "ArrayOfSplitReceivableTemplate")
    public JAXBElement<ArrayOfSplitReceivableTemplate> createArrayOfSplitReceivableTemplate(ArrayOfSplitReceivableTemplate value) {
        return new JAXBElement<ArrayOfSplitReceivableTemplate>(_ArrayOfSplitReceivableTemplate_QNAME, ArrayOfSplitReceivableTemplate.class, null, value);
    }

}
