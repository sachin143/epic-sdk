
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ApprovalStatus_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/", "ApprovalStatus");
    private final static QName _AssociationType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/", "AssociationType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApprovalStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/", name = "ApprovalStatus")
    public JAXBElement<ApprovalStatus> createApprovalStatus(ApprovalStatus value) {
        return new JAXBElement<ApprovalStatus>(_ApprovalStatus_QNAME, ApprovalStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssociationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/", name = "AssociationType")
    public JAXBElement<AssociationType> createAssociationType(AssociationType value) {
        return new JAXBElement<AssociationType>(_AssociationType_QNAME, AssociationType.class, null, value);
    }

}
