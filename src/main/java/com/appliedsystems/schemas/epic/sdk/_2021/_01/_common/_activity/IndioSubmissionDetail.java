
package com.appliedsystems.schemas.epic.sdk._2021._01._common._activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IndioSubmissionDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IndioSubmissionDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarrierSubmissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SubmissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndioSubmissionDetail", propOrder = {
    "carrierSubmissionID",
    "submissionID"
})
public class IndioSubmissionDetail {

    @XmlElement(name = "CarrierSubmissionID")
    protected Integer carrierSubmissionID;
    @XmlElement(name = "SubmissionID")
    protected Integer submissionID;

    /**
     * Gets the value of the carrierSubmissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCarrierSubmissionID() {
        return carrierSubmissionID;
    }

    /**
     * Sets the value of the carrierSubmissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCarrierSubmissionID(Integer value) {
        this.carrierSubmissionID = value;
    }

    /**
     * Gets the value of the submissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubmissionID() {
        return submissionID;
    }

    /**
     * Sets the value of the submissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubmissionID(Integer value) {
        this.submissionID = value;
    }

}
