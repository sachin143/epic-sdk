
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaidTransactionItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaidTransactionItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaidTransactionItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/_detail/_detailitem/_paiditems/_common/}PaidTransactionItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaidTransactionItems", propOrder = {
    "paidTransactionItems"
})
public class PaidTransactionItems {

    @XmlElement(name = "PaidTransactionItem", nillable = true)
    protected List<PaidTransactionItem> paidTransactionItems;

    /**
     * Gets the value of the paidTransactionItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paidTransactionItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaidTransactionItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaidTransactionItem }
     * 
     * 
     */
    public List<PaidTransactionItem> getPaidTransactionItems() {
        if (paidTransactionItems == null) {
            paidTransactionItems = new ArrayList<PaidTransactionItem>();
        }
        return this.paidTransactionItems;
    }

}
