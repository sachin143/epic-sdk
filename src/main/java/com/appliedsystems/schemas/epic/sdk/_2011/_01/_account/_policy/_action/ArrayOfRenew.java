
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRenew complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRenew">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Renew" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}Renew" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRenew", propOrder = {
    "renews"
})
public class ArrayOfRenew {

    @XmlElement(name = "Renew", nillable = true)
    protected List<Renew> renews;

    /**
     * Gets the value of the renews property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the renews property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRenews().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Renew }
     * 
     * 
     */
    public List<Renew> getRenews() {
        if (renews == null) {
            renews = new ArrayList<Renew>();
        }
        return this.renews;
    }

}
