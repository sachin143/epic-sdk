
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RevenueScheduleItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", "RevenueScheduleItems");
    private final static QName _SplitItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", "SplitItems");
    private final static QName _SplitItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", "SplitItem");
    private final static QName _RevenueScheduleItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", "RevenueScheduleItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RevenueScheduleItem }
     * 
     */
    public RevenueScheduleItem createRevenueScheduleItem() {
        return new RevenueScheduleItem();
    }

    /**
     * Create an instance of {@link SplitItem }
     * 
     */
    public SplitItem createSplitItem() {
        return new SplitItem();
    }

    /**
     * Create an instance of {@link SplitItems }
     * 
     */
    public SplitItems createSplitItems() {
        return new SplitItems();
    }

    /**
     * Create an instance of {@link RevenueScheduleItems }
     * 
     */
    public RevenueScheduleItems createRevenueScheduleItems() {
        return new RevenueScheduleItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevenueScheduleItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", name = "RevenueScheduleItems")
    public JAXBElement<RevenueScheduleItems> createRevenueScheduleItems(RevenueScheduleItems value) {
        return new JAXBElement<RevenueScheduleItems>(_RevenueScheduleItems_QNAME, RevenueScheduleItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", name = "SplitItems")
    public JAXBElement<SplitItems> createSplitItems(SplitItems value) {
        return new JAXBElement<SplitItems>(_SplitItems_QNAME, SplitItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", name = "SplitItem")
    public JAXBElement<SplitItem> createSplitItem(SplitItem value) {
        return new JAXBElement<SplitItem>(_SplitItem_QNAME, SplitItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevenueScheduleItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/", name = "RevenueScheduleItem")
    public JAXBElement<RevenueScheduleItem> createRevenueScheduleItem(RevenueScheduleItem value) {
        return new JAXBElement<RevenueScheduleItem>(_RevenueScheduleItem_QNAME, RevenueScheduleItem.class, null, value);
    }

}
