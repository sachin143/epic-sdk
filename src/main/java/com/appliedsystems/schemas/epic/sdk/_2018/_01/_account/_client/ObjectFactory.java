
package com.appliedsystems.schemas.epic.sdk._2018._01._account._client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2018._01._account._client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SplitReceivableTemplateGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_client/", "SplitReceivableTemplateGetType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2018._01._account._client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableTemplateGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_client/", name = "SplitReceivableTemplateGetType")
    public JAXBElement<SplitReceivableTemplateGetType> createSplitReceivableTemplateGetType(SplitReceivableTemplateGetType value) {
        return new JAXBElement<SplitReceivableTemplateGetType>(_SplitReceivableTemplateGetType_QNAME, SplitReceivableTemplateGetType.class, null, value);
    }

}
