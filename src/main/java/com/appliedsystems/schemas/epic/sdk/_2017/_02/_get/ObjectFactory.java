
package com.appliedsystems.schemas.epic.sdk._2017._02._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LineGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/", "LineGetResult");
    private final static QName _ContactGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/", "ContactGetResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LineGetResult }
     * 
     */
    public LineGetResult createLineGetResult() {
        return new LineGetResult();
    }

    /**
     * Create an instance of {@link ContactGetResult }
     * 
     */
    public ContactGetResult createContactGetResult() {
        return new ContactGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/", name = "LineGetResult")
    public JAXBElement<LineGetResult> createLineGetResult(LineGetResult value) {
        return new JAXBElement<LineGetResult>(_LineGetResult_QNAME, LineGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/", name = "ContactGetResult")
    public JAXBElement<ContactGetResult> createContactGetResult(ContactGetResult value) {
        return new JAXBElement<ContactGetResult>(_ContactGetResult_QNAME, ContactGetResult.class, null, value);
    }

}
