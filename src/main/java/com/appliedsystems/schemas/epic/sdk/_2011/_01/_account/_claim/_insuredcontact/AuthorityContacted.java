
package com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuthorityContacted complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuthorityContacted">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BadgeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Contacted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DivisionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Municipality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfficerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ViolationsCitations" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorityContacted", propOrder = {
    "badgeNumber",
    "contacted",
    "divisionNumber",
    "municipality",
    "officerName",
    "phoneEmail",
    "reportNumber",
    "violationsCitations"
})
public class AuthorityContacted {

    @XmlElement(name = "BadgeNumber", nillable = true)
    protected String badgeNumber;
    @XmlElement(name = "Contacted", nillable = true)
    protected String contacted;
    @XmlElement(name = "DivisionNumber", nillable = true)
    protected String divisionNumber;
    @XmlElement(name = "Municipality", nillable = true)
    protected String municipality;
    @XmlElement(name = "OfficerName", nillable = true)
    protected String officerName;
    @XmlElement(name = "PhoneEmail", nillable = true)
    protected String phoneEmail;
    @XmlElement(name = "ReportNumber", nillable = true)
    protected String reportNumber;
    @XmlElement(name = "ViolationsCitations", nillable = true)
    protected String violationsCitations;

    /**
     * Gets the value of the badgeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBadgeNumber() {
        return badgeNumber;
    }

    /**
     * Sets the value of the badgeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBadgeNumber(String value) {
        this.badgeNumber = value;
    }

    /**
     * Gets the value of the contacted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContacted() {
        return contacted;
    }

    /**
     * Sets the value of the contacted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContacted(String value) {
        this.contacted = value;
    }

    /**
     * Gets the value of the divisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivisionNumber() {
        return divisionNumber;
    }

    /**
     * Sets the value of the divisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivisionNumber(String value) {
        this.divisionNumber = value;
    }

    /**
     * Gets the value of the municipality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMunicipality() {
        return municipality;
    }

    /**
     * Sets the value of the municipality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMunicipality(String value) {
        this.municipality = value;
    }

    /**
     * Gets the value of the officerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerName() {
        return officerName;
    }

    /**
     * Sets the value of the officerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerName(String value) {
        this.officerName = value;
    }

    /**
     * Gets the value of the phoneEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneEmail() {
        return phoneEmail;
    }

    /**
     * Sets the value of the phoneEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneEmail(String value) {
        this.phoneEmail = value;
    }

    /**
     * Gets the value of the reportNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportNumber() {
        return reportNumber;
    }

    /**
     * Sets the value of the reportNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportNumber(String value) {
        this.reportNumber = value;
    }

    /**
     * Gets the value of the violationsCitations property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViolationsCitations() {
        return violationsCitations;
    }

    /**
     * Sets the value of the violationsCitations property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViolationsCitations(String value) {
        this.violationsCitations = value;
    }

}
