
package com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateRenewalStage_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/", "UpdateRenewalStage");
    private final static QName _ArrayOfUpdateRenewalStage_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/", "ArrayOfUpdateRenewalStage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfUpdateRenewalStage }
     * 
     */
    public ArrayOfUpdateRenewalStage createArrayOfUpdateRenewalStage() {
        return new ArrayOfUpdateRenewalStage();
    }

    /**
     * Create an instance of {@link UpdateRenewalStage }
     * 
     */
    public UpdateRenewalStage createUpdateRenewalStage() {
        return new UpdateRenewalStage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateRenewalStage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/", name = "UpdateRenewalStage")
    public JAXBElement<UpdateRenewalStage> createUpdateRenewalStage(UpdateRenewalStage value) {
        return new JAXBElement<UpdateRenewalStage>(_UpdateRenewalStage_QNAME, UpdateRenewalStage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfUpdateRenewalStage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/", name = "ArrayOfUpdateRenewalStage")
    public JAXBElement<ArrayOfUpdateRenewalStage> createArrayOfUpdateRenewalStage(ArrayOfUpdateRenewalStage value) {
        return new JAXBElement<ArrayOfUpdateRenewalStage>(_ArrayOfUpdateRenewalStage_QNAME, ArrayOfUpdateRenewalStage.class, null, value);
    }

}
