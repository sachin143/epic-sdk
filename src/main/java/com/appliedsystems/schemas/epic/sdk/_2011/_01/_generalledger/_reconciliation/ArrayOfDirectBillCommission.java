
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDirectBillCommission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDirectBillCommission">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DirectBillCommission" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/}DirectBillCommission" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDirectBillCommission", propOrder = {
    "directBillCommissions"
})
public class ArrayOfDirectBillCommission {

    @XmlElement(name = "DirectBillCommission", nillable = true)
    protected List<DirectBillCommission> directBillCommissions;

    /**
     * Gets the value of the directBillCommissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the directBillCommissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDirectBillCommissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DirectBillCommission }
     * 
     * 
     */
    public List<DirectBillCommission> getDirectBillCommissions() {
        if (directBillCommissions == null) {
            directBillCommissions = new ArrayList<DirectBillCommission>();
        }
        return this.directBillCommissions;
    }

}
