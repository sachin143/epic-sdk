
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.PaidTransactionItems;


/**
 * <p>Java class for PaidItemsReturnCommission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaidItemsReturnCommission">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemsPaid" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/_detail/_detailitem/_paiditems/_common/}PaidTransactionItems" minOccurs="0"/>
 *         &lt;element name="TotalItemsPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaidItemsReturnCommission", propOrder = {
    "itemsPaid",
    "totalItemsPaid"
})
public class PaidItemsReturnCommission {

    @XmlElement(name = "ItemsPaid", nillable = true)
    protected PaidTransactionItems itemsPaid;
    @XmlElement(name = "TotalItemsPaid")
    protected BigDecimal totalItemsPaid;

    /**
     * Gets the value of the itemsPaid property.
     * 
     * @return
     *     possible object is
     *     {@link PaidTransactionItems }
     *     
     */
    public PaidTransactionItems getItemsPaid() {
        return itemsPaid;
    }

    /**
     * Sets the value of the itemsPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidTransactionItems }
     *     
     */
    public void setItemsPaid(PaidTransactionItems value) {
        this.itemsPaid = value;
    }

    /**
     * Gets the value of the totalItemsPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalItemsPaid() {
        return totalItemsPaid;
    }

    /**
     * Sets the value of the totalItemsPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalItemsPaid(BigDecimal value) {
        this.totalItemsPaid = value;
    }

}
