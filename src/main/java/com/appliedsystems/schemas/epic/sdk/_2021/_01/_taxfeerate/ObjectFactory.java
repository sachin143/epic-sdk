
package com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/", "TransactionItems");
    private final static QName _PolicyItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/", "PolicyItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PolicyItems }
     * 
     */
    public PolicyItems createPolicyItems() {
        return new PolicyItems();
    }

    /**
     * Create an instance of {@link TransactionItems }
     * 
     */
    public TransactionItems createTransactionItems() {
        return new TransactionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/", name = "TransactionItems")
    public JAXBElement<TransactionItems> createTransactionItems(TransactionItems value) {
        return new JAXBElement<TransactionItems>(_TransactionItems_QNAME, TransactionItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/", name = "PolicyItems")
    public JAXBElement<PolicyItems> createPolicyItems(PolicyItems value) {
        return new JAXBElement<PolicyItems>(_PolicyItems_QNAME, PolicyItems.class, null, value);
    }

}
