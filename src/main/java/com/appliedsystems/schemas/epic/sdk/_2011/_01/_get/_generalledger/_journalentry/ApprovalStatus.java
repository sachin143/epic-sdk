
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApprovalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApprovalStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Working"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="Approved"/>
 *     &lt;enumeration value="SystemApproved"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApprovalStatus", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/")
@XmlEnum
public enum ApprovalStatus {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Working")
    WORKING("Working"),
    @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("Approved")
    APPROVED("Approved"),
    @XmlEnumValue("SystemApproved")
    SYSTEM_APPROVED("SystemApproved");
    private final String value;

    ApprovalStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApprovalStatus fromValue(String v) {
        for (ApprovalStatus c: ApprovalStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
