
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceivableItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceivableItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceivableItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/}ReceivableItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceivableItems", propOrder = {
    "receivableItems"
})
public class ReceivableItems {

    @XmlElement(name = "ReceivableItem", nillable = true)
    protected List<ReceivableItem> receivableItems;

    /**
     * Gets the value of the receivableItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivableItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivableItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceivableItem }
     * 
     * 
     */
    public List<ReceivableItem> getReceivableItems() {
        if (receivableItems == null) {
            receivableItems = new ArrayList<ReceivableItem>();
        }
        return this.receivableItems;
    }

}
