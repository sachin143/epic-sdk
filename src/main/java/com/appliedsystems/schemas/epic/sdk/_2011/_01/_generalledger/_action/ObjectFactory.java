
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransferOfFunds_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", "TransferOfFunds");
    private final static QName _ArrayOfPayVouchers_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", "ArrayOfPayVouchers");
    private final static QName _PayVouchers_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", "PayVouchers");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransferOfFunds }
     * 
     */
    public TransferOfFunds createTransferOfFunds() {
        return new TransferOfFunds();
    }

    /**
     * Create an instance of {@link ArrayOfPayVouchers }
     * 
     */
    public ArrayOfPayVouchers createArrayOfPayVouchers() {
        return new ArrayOfPayVouchers();
    }

    /**
     * Create an instance of {@link PayVouchers }
     * 
     */
    public PayVouchers createPayVouchers() {
        return new PayVouchers();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferOfFunds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", name = "TransferOfFunds")
    public JAXBElement<TransferOfFunds> createTransferOfFunds(TransferOfFunds value) {
        return new JAXBElement<TransferOfFunds>(_TransferOfFunds_QNAME, TransferOfFunds.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPayVouchers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", name = "ArrayOfPayVouchers")
    public JAXBElement<ArrayOfPayVouchers> createArrayOfPayVouchers(ArrayOfPayVouchers value) {
        return new JAXBElement<ArrayOfPayVouchers>(_ArrayOfPayVouchers_QNAME, ArrayOfPayVouchers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayVouchers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/", name = "PayVouchers")
    public JAXBElement<PayVouchers> createPayVouchers(PayVouchers value) {
        return new JAXBElement<PayVouchers>(_PayVouchers_QNAME, PayVouchers.class, null, value);
    }

}
