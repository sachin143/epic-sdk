
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.AssociatedToType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.SortType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.Status;


/**
 * <p>Java class for ActivityFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ActivityStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/}Status" minOccurs="0"/>
 *         &lt;element name="AssociatedToID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedToType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/}AssociatedToType" minOccurs="0"/>
 *         &lt;element name="DescendingSort" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EnteredOnBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EnteredOnEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FollowUpStartDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FollowUpStartDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PremiumPayableAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SortType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/}SortType" minOccurs="0"/>
 *         &lt;element name="WhoOwnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WhoOwnerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WhoOwnerNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="AssociatedToGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityFilter", propOrder = {
    "accountID",
    "accountName",
    "accountNameComparisonType",
    "accountTypeCode",
    "activityCode",
    "activityID",
    "activityStatus",
    "associatedToID",
    "associatedToType",
    "descendingSort",
    "enteredOnBegins",
    "enteredOnEnds",
    "followUpStartDateBegins",
    "followUpStartDateEnds",
    "issuingCompanyCode",
    "premiumPayableAccountCode",
    "receiptID",
    "sortType",
    "whoOwnerCode",
    "whoOwnerName",
    "whoOwnerNameComparisonType",
    "associatedToGUID"
})
public class ActivityFilter {

    @XmlElement(name = "AccountID", nillable = true)
    protected Integer accountID;
    @XmlElement(name = "AccountName", nillable = true)
    protected String accountName;
    @XmlElement(name = "AccountNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType accountNameComparisonType;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "ActivityCode", nillable = true)
    protected String activityCode;
    @XmlElement(name = "ActivityID", nillable = true)
    protected Integer activityID;
    @XmlElement(name = "ActivityStatus")
    @XmlSchemaType(name = "string")
    protected Status activityStatus;
    @XmlElement(name = "AssociatedToID", nillable = true)
    protected Integer associatedToID;
    @XmlElement(name = "AssociatedToType")
    @XmlSchemaType(name = "string")
    protected AssociatedToType associatedToType;
    @XmlElement(name = "DescendingSort")
    protected Boolean descendingSort;
    @XmlElement(name = "EnteredOnBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredOnBegins;
    @XmlElement(name = "EnteredOnEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredOnEnds;
    @XmlElement(name = "FollowUpStartDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpStartDateBegins;
    @XmlElement(name = "FollowUpStartDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpStartDateEnds;
    @XmlElement(name = "IssuingCompanyCode", nillable = true)
    protected String issuingCompanyCode;
    @XmlElement(name = "PremiumPayableAccountCode", nillable = true)
    protected String premiumPayableAccountCode;
    @XmlElement(name = "ReceiptID", nillable = true)
    protected Integer receiptID;
    @XmlElement(name = "SortType")
    @XmlSchemaType(name = "string")
    protected SortType sortType;
    @XmlElement(name = "WhoOwnerCode", nillable = true)
    protected String whoOwnerCode;
    @XmlElement(name = "WhoOwnerName", nillable = true)
    protected String whoOwnerName;
    @XmlElement(name = "WhoOwnerNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType whoOwnerNameComparisonType;
    @XmlElement(name = "AssociatedToGUID", nillable = true)
    protected String associatedToGUID;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the accountNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getAccountNameComparisonType() {
        return accountNameComparisonType;
    }

    /**
     * Sets the value of the accountNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setAccountNameComparisonType(ComparisonType value) {
        this.accountNameComparisonType = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the activityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * Sets the value of the activityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCode(String value) {
        this.activityCode = value;
    }

    /**
     * Gets the value of the activityID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivityID(Integer value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the activityStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getActivityStatus() {
        return activityStatus;
    }

    /**
     * Sets the value of the activityStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setActivityStatus(Status value) {
        this.activityStatus = value;
    }

    /**
     * Gets the value of the associatedToID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedToID() {
        return associatedToID;
    }

    /**
     * Sets the value of the associatedToID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedToID(Integer value) {
        this.associatedToID = value;
    }

    /**
     * Gets the value of the associatedToType property.
     * 
     * @return
     *     possible object is
     *     {@link AssociatedToType }
     *     
     */
    public AssociatedToType getAssociatedToType() {
        return associatedToType;
    }

    /**
     * Sets the value of the associatedToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociatedToType }
     *     
     */
    public void setAssociatedToType(AssociatedToType value) {
        this.associatedToType = value;
    }

    /**
     * Gets the value of the descendingSort property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDescendingSort() {
        return descendingSort;
    }

    /**
     * Sets the value of the descendingSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDescendingSort(Boolean value) {
        this.descendingSort = value;
    }

    /**
     * Gets the value of the enteredOnBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredOnBegins() {
        return enteredOnBegins;
    }

    /**
     * Sets the value of the enteredOnBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredOnBegins(XMLGregorianCalendar value) {
        this.enteredOnBegins = value;
    }

    /**
     * Gets the value of the enteredOnEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredOnEnds() {
        return enteredOnEnds;
    }

    /**
     * Sets the value of the enteredOnEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredOnEnds(XMLGregorianCalendar value) {
        this.enteredOnEnds = value;
    }

    /**
     * Gets the value of the followUpStartDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpStartDateBegins() {
        return followUpStartDateBegins;
    }

    /**
     * Sets the value of the followUpStartDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpStartDateBegins(XMLGregorianCalendar value) {
        this.followUpStartDateBegins = value;
    }

    /**
     * Gets the value of the followUpStartDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpStartDateEnds() {
        return followUpStartDateEnds;
    }

    /**
     * Sets the value of the followUpStartDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpStartDateEnds(XMLGregorianCalendar value) {
        this.followUpStartDateEnds = value;
    }

    /**
     * Gets the value of the issuingCompanyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyCode() {
        return issuingCompanyCode;
    }

    /**
     * Sets the value of the issuingCompanyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyCode(String value) {
        this.issuingCompanyCode = value;
    }

    /**
     * Gets the value of the premiumPayableAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableAccountCode() {
        return premiumPayableAccountCode;
    }

    /**
     * Sets the value of the premiumPayableAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableAccountCode(String value) {
        this.premiumPayableAccountCode = value;
    }

    /**
     * Gets the value of the receiptID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptID() {
        return receiptID;
    }

    /**
     * Sets the value of the receiptID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptID(Integer value) {
        this.receiptID = value;
    }

    /**
     * Gets the value of the sortType property.
     * 
     * @return
     *     possible object is
     *     {@link SortType }
     *     
     */
    public SortType getSortType() {
        return sortType;
    }

    /**
     * Sets the value of the sortType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortType }
     *     
     */
    public void setSortType(SortType value) {
        this.sortType = value;
    }

    /**
     * Gets the value of the whoOwnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhoOwnerCode() {
        return whoOwnerCode;
    }

    /**
     * Sets the value of the whoOwnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhoOwnerCode(String value) {
        this.whoOwnerCode = value;
    }

    /**
     * Gets the value of the whoOwnerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhoOwnerName() {
        return whoOwnerName;
    }

    /**
     * Sets the value of the whoOwnerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhoOwnerName(String value) {
        this.whoOwnerName = value;
    }

    /**
     * Gets the value of the whoOwnerNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getWhoOwnerNameComparisonType() {
        return whoOwnerNameComparisonType;
    }

    /**
     * Sets the value of the whoOwnerNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setWhoOwnerNameComparisonType(ComparisonType value) {
        this.whoOwnerNameComparisonType = value;
    }

    /**
     * Gets the value of the associatedToGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedToGUID() {
        return associatedToGUID;
    }

    /**
     * Sets the value of the associatedToGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedToGUID(String value) {
        this.associatedToGUID = value;
    }

}
