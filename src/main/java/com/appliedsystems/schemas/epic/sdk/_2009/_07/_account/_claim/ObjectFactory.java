
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._claim package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfServicingContacts_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ArrayOfServicingContacts");
    private final static QName _Summary_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "Summary");
    private final static QName _ArrayOfAdditionalParty_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ArrayOfAdditionalParty");
    private final static QName _ArrayOfAdjustor_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ArrayOfAdjustor");
    private final static QName _AdditionalParty_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "AdditionalParty");
    private final static QName _AdditionalPartyGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "AdditionalPartyGetType");
    private final static QName _Litigation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "Litigation");
    private final static QName _AdjustorGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "AdjustorGetType");
    private final static QName _ArrayOfLitigation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ArrayOfLitigation");
    private final static QName _ServicingContacts_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ServicingContacts");
    private final static QName _ArrayOfSummary_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "ArrayOfSummary");
    private final static QName _Adjustor_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", "Adjustor");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._claim
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfAdditionalParty }
     * 
     */
    public ArrayOfAdditionalParty createArrayOfAdditionalParty() {
        return new ArrayOfAdditionalParty();
    }

    /**
     * Create an instance of {@link AdditionalParty }
     * 
     */
    public AdditionalParty createAdditionalParty() {
        return new AdditionalParty();
    }

    /**
     * Create an instance of {@link Adjustor }
     * 
     */
    public Adjustor createAdjustor() {
        return new Adjustor();
    }

    /**
     * Create an instance of {@link Litigation }
     * 
     */
    public Litigation createLitigation() {
        return new Litigation();
    }

    /**
     * Create an instance of {@link Summary }
     * 
     */
    public Summary createSummary() {
        return new Summary();
    }

    /**
     * Create an instance of {@link ArrayOfServicingContacts }
     * 
     */
    public ArrayOfServicingContacts createArrayOfServicingContacts() {
        return new ArrayOfServicingContacts();
    }

    /**
     * Create an instance of {@link ServicingContacts }
     * 
     */
    public ServicingContacts createServicingContacts() {
        return new ServicingContacts();
    }

    /**
     * Create an instance of {@link ArrayOfLitigation }
     * 
     */
    public ArrayOfLitigation createArrayOfLitigation() {
        return new ArrayOfLitigation();
    }

    /**
     * Create an instance of {@link ArrayOfSummary }
     * 
     */
    public ArrayOfSummary createArrayOfSummary() {
        return new ArrayOfSummary();
    }

    /**
     * Create an instance of {@link ArrayOfAdjustor }
     * 
     */
    public ArrayOfAdjustor createArrayOfAdjustor() {
        return new ArrayOfAdjustor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfServicingContacts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ArrayOfServicingContacts")
    public JAXBElement<ArrayOfServicingContacts> createArrayOfServicingContacts(ArrayOfServicingContacts value) {
        return new JAXBElement<ArrayOfServicingContacts>(_ArrayOfServicingContacts_QNAME, ArrayOfServicingContacts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Summary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "Summary")
    public JAXBElement<Summary> createSummary(Summary value) {
        return new JAXBElement<Summary>(_Summary_QNAME, Summary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAdditionalParty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ArrayOfAdditionalParty")
    public JAXBElement<ArrayOfAdditionalParty> createArrayOfAdditionalParty(ArrayOfAdditionalParty value) {
        return new JAXBElement<ArrayOfAdditionalParty>(_ArrayOfAdditionalParty_QNAME, ArrayOfAdditionalParty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAdjustor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ArrayOfAdjustor")
    public JAXBElement<ArrayOfAdjustor> createArrayOfAdjustor(ArrayOfAdjustor value) {
        return new JAXBElement<ArrayOfAdjustor>(_ArrayOfAdjustor_QNAME, ArrayOfAdjustor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalParty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "AdditionalParty")
    public JAXBElement<AdditionalParty> createAdditionalParty(AdditionalParty value) {
        return new JAXBElement<AdditionalParty>(_AdditionalParty_QNAME, AdditionalParty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalPartyGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "AdditionalPartyGetType")
    public JAXBElement<AdditionalPartyGetType> createAdditionalPartyGetType(AdditionalPartyGetType value) {
        return new JAXBElement<AdditionalPartyGetType>(_AdditionalPartyGetType_QNAME, AdditionalPartyGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Litigation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "Litigation")
    public JAXBElement<Litigation> createLitigation(Litigation value) {
        return new JAXBElement<Litigation>(_Litigation_QNAME, Litigation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustorGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "AdjustorGetType")
    public JAXBElement<AdjustorGetType> createAdjustorGetType(AdjustorGetType value) {
        return new JAXBElement<AdjustorGetType>(_AdjustorGetType_QNAME, AdjustorGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLitigation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ArrayOfLitigation")
    public JAXBElement<ArrayOfLitigation> createArrayOfLitigation(ArrayOfLitigation value) {
        return new JAXBElement<ArrayOfLitigation>(_ArrayOfLitigation_QNAME, ArrayOfLitigation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServicingContacts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ServicingContacts")
    public JAXBElement<ServicingContacts> createServicingContacts(ServicingContacts value) {
        return new JAXBElement<ServicingContacts>(_ServicingContacts_QNAME, ServicingContacts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSummary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "ArrayOfSummary")
    public JAXBElement<ArrayOfSummary> createArrayOfSummary(ArrayOfSummary value) {
        return new JAXBElement<ArrayOfSummary>(_ArrayOfSummary_QNAME, ArrayOfSummary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Adjustor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/", name = "Adjustor")
    public JAXBElement<Adjustor> createAdjustor(Adjustor value) {
        return new JAXBElement<Adjustor>(_Adjustor_QNAME, Adjustor.class, null, value);
    }

}
