
package com.appliedsystems.schemas.epic.sdk._2019._01._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StructureItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/", "StructureItem");
    private final static QName _StructureItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/", "StructureItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StructureItem }
     * 
     */
    public StructureItem createStructureItem() {
        return new StructureItem();
    }

    /**
     * Create an instance of {@link StructureItems }
     * 
     */
    public StructureItems createStructureItems() {
        return new StructureItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/", name = "StructureItem")
    public JAXBElement<StructureItem> createStructureItem(StructureItem value) {
        return new JAXBElement<StructureItem>(_StructureItem_QNAME, StructureItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/", name = "StructureItems")
    public JAXBElement<StructureItems> createStructureItems(StructureItems value) {
        return new JAXBElement<StructureItems>(_StructureItems_QNAME, StructureItems.class, null, value);
    }

}
