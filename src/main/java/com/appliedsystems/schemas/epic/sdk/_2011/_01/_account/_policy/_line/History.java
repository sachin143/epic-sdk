
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for History complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="History">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateFirstWritten" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="BrokerOfRecordDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "History", propOrder = {
    "comments",
    "dateFirstWritten",
    "brokerOfRecordDate"
})
public class History {

    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "DateFirstWritten")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateFirstWritten;
    @XmlElement(name = "BrokerOfRecordDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar brokerOfRecordDate;

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the dateFirstWritten property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateFirstWritten() {
        return dateFirstWritten;
    }

    /**
     * Sets the value of the dateFirstWritten property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateFirstWritten(XMLGregorianCalendar value) {
        this.dateFirstWritten = value;
    }

    /**
     * Gets the value of the brokerOfRecordDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBrokerOfRecordDate() {
        return brokerOfRecordDate;
    }

    /**
     * Sets the value of the brokerOfRecordDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBrokerOfRecordDate(XMLGregorianCalendar value) {
        this.brokerOfRecordDate = value;
    }

}
