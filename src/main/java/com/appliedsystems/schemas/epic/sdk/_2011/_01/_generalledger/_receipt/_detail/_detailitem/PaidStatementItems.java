
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaidStatementItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaidStatementItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaidStatementItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}PaidStatementItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaidStatementItems", propOrder = {
    "paidStatementItems"
})
public class PaidStatementItems {

    @XmlElement(name = "PaidStatementItem", nillable = true)
    protected List<PaidStatementItem> paidStatementItems;

    /**
     * Gets the value of the paidStatementItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paidStatementItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaidStatementItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaidStatementItem }
     * 
     * 
     */
    public List<PaidStatementItem> getPaidStatementItems() {
        if (paidStatementItems == null) {
            paidStatementItems = new ArrayList<PaidStatementItem>();
        }
        return this.paidStatementItems;
    }

}
