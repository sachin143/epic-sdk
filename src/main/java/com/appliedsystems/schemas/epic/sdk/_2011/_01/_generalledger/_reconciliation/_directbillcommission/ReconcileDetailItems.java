
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReconcileDetailItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReconcileDetailItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReconcileDetailItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/}ReconcileDetailItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReconcileDetailItems", propOrder = {
    "reconcileDetailItems"
})
public class ReconcileDetailItems {

    @XmlElement(name = "ReconcileDetailItem", nillable = true)
    protected List<ReconcileDetailItem> reconcileDetailItems;

    /**
     * Gets the value of the reconcileDetailItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reconcileDetailItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReconcileDetailItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReconcileDetailItem }
     * 
     * 
     */
    public List<ReconcileDetailItem> getReconcileDetailItems() {
        if (reconcileDetailItems == null) {
            reconcileDetailItems = new ArrayList<ReconcileDetailItem>();
        }
        return this.reconcileDetailItems;
    }

}
