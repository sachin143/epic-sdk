
package com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttachmentSortType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttachmentSortType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AttachedDate"/>
 *     &lt;enumeration value="EditedDate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AttachmentSortType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_attachment/")
@XmlEnum
public enum AttachmentSortType {

    @XmlEnumValue("AttachedDate")
    ATTACHED_DATE("AttachedDate"),
    @XmlEnumValue("EditedDate")
    EDITED_DATE("EditedDate");
    private final String value;

    AttachmentSortType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AttachmentSortType fromValue(String v) {
        for (AttachmentSortType c: AttachmentSortType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
