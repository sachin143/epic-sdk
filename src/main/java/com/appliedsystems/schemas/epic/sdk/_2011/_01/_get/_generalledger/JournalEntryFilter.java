
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ApprovalStatus;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.AssociationType;


/**
 * <p>Java class for JournalEntryFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JournalEntryFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNameEquals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AccountingMonthBegins" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountingMonthEnds" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EffectiveDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EnteredDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EnteredDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExportBatchNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExportedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExportedByEquals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ExportedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExportedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="GLSchedule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncludeVoidSystemEntries" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="JournalEntryAssociationLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryAssociationType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/}AssociationType" minOccurs="0"/>
 *         &lt;element name="JournalEntryBatchNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LastSixMonthsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PostedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostedByEquals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PostedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PostedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReferenceNumberBegins" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumberEnds" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApprovalStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/}ApprovalStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalEntryFilter", propOrder = {
    "accountName",
    "accountNameEquals",
    "accountingMonthBegins",
    "accountingMonthEnds",
    "effectiveDateBegins",
    "effectiveDateEnds",
    "enteredDateBegins",
    "enteredDateEnds",
    "exportBatchNumber",
    "exportedBy",
    "exportedByEquals",
    "exportedDateBegins",
    "exportedDateEnds",
    "glSchedule",
    "includeVoidSystemEntries",
    "journalEntryAssociationLookupCode",
    "journalEntryAssociationType",
    "journalEntryBatchNumber",
    "journalEntryID",
    "lastSixMonthsOnly",
    "postedBy",
    "postedByEquals",
    "postedDateBegins",
    "postedDateEnds",
    "referenceNumberBegins",
    "referenceNumberEnds",
    "approvalStatus"
})
public class JournalEntryFilter {

    @XmlElement(name = "AccountName", nillable = true)
    protected String accountName;
    @XmlElement(name = "AccountNameEquals")
    protected Boolean accountNameEquals;
    @XmlElement(name = "AccountingMonthBegins", nillable = true)
    protected String accountingMonthBegins;
    @XmlElement(name = "AccountingMonthEnds", nillable = true)
    protected String accountingMonthEnds;
    @XmlElement(name = "EffectiveDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDateBegins;
    @XmlElement(name = "EffectiveDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDateEnds;
    @XmlElement(name = "EnteredDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredDateBegins;
    @XmlElement(name = "EnteredDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredDateEnds;
    @XmlElement(name = "ExportBatchNumber", nillable = true)
    protected String exportBatchNumber;
    @XmlElement(name = "ExportedBy", nillable = true)
    protected String exportedBy;
    @XmlElement(name = "ExportedByEquals")
    protected Boolean exportedByEquals;
    @XmlElement(name = "ExportedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exportedDateBegins;
    @XmlElement(name = "ExportedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar exportedDateEnds;
    @XmlElement(name = "GLSchedule", nillable = true)
    protected String glSchedule;
    @XmlElement(name = "IncludeVoidSystemEntries")
    protected Boolean includeVoidSystemEntries;
    @XmlElement(name = "JournalEntryAssociationLookupCode", nillable = true)
    protected String journalEntryAssociationLookupCode;
    @XmlElement(name = "JournalEntryAssociationType")
    @XmlSchemaType(name = "string")
    protected AssociationType journalEntryAssociationType;
    @XmlElement(name = "JournalEntryBatchNumber", nillable = true)
    protected String journalEntryBatchNumber;
    @XmlElement(name = "JournalEntryID", nillable = true)
    protected Integer journalEntryID;
    @XmlElement(name = "LastSixMonthsOnly")
    protected Boolean lastSixMonthsOnly;
    @XmlElement(name = "PostedBy", nillable = true)
    protected String postedBy;
    @XmlElement(name = "PostedByEquals")
    protected Boolean postedByEquals;
    @XmlElement(name = "PostedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar postedDateBegins;
    @XmlElement(name = "PostedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar postedDateEnds;
    @XmlElement(name = "ReferenceNumberBegins", nillable = true)
    protected String referenceNumberBegins;
    @XmlElement(name = "ReferenceNumberEnds", nillable = true)
    protected String referenceNumberEnds;
    @XmlElement(name = "ApprovalStatus")
    @XmlSchemaType(name = "string")
    protected ApprovalStatus approvalStatus;

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the accountNameEquals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccountNameEquals() {
        return accountNameEquals;
    }

    /**
     * Sets the value of the accountNameEquals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccountNameEquals(Boolean value) {
        this.accountNameEquals = value;
    }

    /**
     * Gets the value of the accountingMonthBegins property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonthBegins() {
        return accountingMonthBegins;
    }

    /**
     * Sets the value of the accountingMonthBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonthBegins(String value) {
        this.accountingMonthBegins = value;
    }

    /**
     * Gets the value of the accountingMonthEnds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonthEnds() {
        return accountingMonthEnds;
    }

    /**
     * Sets the value of the accountingMonthEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonthEnds(String value) {
        this.accountingMonthEnds = value;
    }

    /**
     * Gets the value of the effectiveDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDateBegins() {
        return effectiveDateBegins;
    }

    /**
     * Sets the value of the effectiveDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDateBegins(XMLGregorianCalendar value) {
        this.effectiveDateBegins = value;
    }

    /**
     * Gets the value of the effectiveDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDateEnds() {
        return effectiveDateEnds;
    }

    /**
     * Sets the value of the effectiveDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDateEnds(XMLGregorianCalendar value) {
        this.effectiveDateEnds = value;
    }

    /**
     * Gets the value of the enteredDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredDateBegins() {
        return enteredDateBegins;
    }

    /**
     * Sets the value of the enteredDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredDateBegins(XMLGregorianCalendar value) {
        this.enteredDateBegins = value;
    }

    /**
     * Gets the value of the enteredDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredDateEnds() {
        return enteredDateEnds;
    }

    /**
     * Sets the value of the enteredDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredDateEnds(XMLGregorianCalendar value) {
        this.enteredDateEnds = value;
    }

    /**
     * Gets the value of the exportBatchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExportBatchNumber() {
        return exportBatchNumber;
    }

    /**
     * Sets the value of the exportBatchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExportBatchNumber(String value) {
        this.exportBatchNumber = value;
    }

    /**
     * Gets the value of the exportedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExportedBy() {
        return exportedBy;
    }

    /**
     * Sets the value of the exportedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExportedBy(String value) {
        this.exportedBy = value;
    }

    /**
     * Gets the value of the exportedByEquals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExportedByEquals() {
        return exportedByEquals;
    }

    /**
     * Sets the value of the exportedByEquals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExportedByEquals(Boolean value) {
        this.exportedByEquals = value;
    }

    /**
     * Gets the value of the exportedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExportedDateBegins() {
        return exportedDateBegins;
    }

    /**
     * Sets the value of the exportedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExportedDateBegins(XMLGregorianCalendar value) {
        this.exportedDateBegins = value;
    }

    /**
     * Gets the value of the exportedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExportedDateEnds() {
        return exportedDateEnds;
    }

    /**
     * Sets the value of the exportedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExportedDateEnds(XMLGregorianCalendar value) {
        this.exportedDateEnds = value;
    }

    /**
     * Gets the value of the glSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLSchedule() {
        return glSchedule;
    }

    /**
     * Sets the value of the glSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLSchedule(String value) {
        this.glSchedule = value;
    }

    /**
     * Gets the value of the includeVoidSystemEntries property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeVoidSystemEntries() {
        return includeVoidSystemEntries;
    }

    /**
     * Sets the value of the includeVoidSystemEntries property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeVoidSystemEntries(Boolean value) {
        this.includeVoidSystemEntries = value;
    }

    /**
     * Gets the value of the journalEntryAssociationLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryAssociationLookupCode() {
        return journalEntryAssociationLookupCode;
    }

    /**
     * Sets the value of the journalEntryAssociationLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryAssociationLookupCode(String value) {
        this.journalEntryAssociationLookupCode = value;
    }

    /**
     * Gets the value of the journalEntryAssociationType property.
     * 
     * @return
     *     possible object is
     *     {@link AssociationType }
     *     
     */
    public AssociationType getJournalEntryAssociationType() {
        return journalEntryAssociationType;
    }

    /**
     * Sets the value of the journalEntryAssociationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssociationType }
     *     
     */
    public void setJournalEntryAssociationType(AssociationType value) {
        this.journalEntryAssociationType = value;
    }

    /**
     * Gets the value of the journalEntryBatchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryBatchNumber() {
        return journalEntryBatchNumber;
    }

    /**
     * Sets the value of the journalEntryBatchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryBatchNumber(String value) {
        this.journalEntryBatchNumber = value;
    }

    /**
     * Gets the value of the journalEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getJournalEntryID() {
        return journalEntryID;
    }

    /**
     * Sets the value of the journalEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setJournalEntryID(Integer value) {
        this.journalEntryID = value;
    }

    /**
     * Gets the value of the lastSixMonthsOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLastSixMonthsOnly() {
        return lastSixMonthsOnly;
    }

    /**
     * Sets the value of the lastSixMonthsOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLastSixMonthsOnly(Boolean value) {
        this.lastSixMonthsOnly = value;
    }

    /**
     * Gets the value of the postedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostedBy() {
        return postedBy;
    }

    /**
     * Sets the value of the postedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostedBy(String value) {
        this.postedBy = value;
    }

    /**
     * Gets the value of the postedByEquals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPostedByEquals() {
        return postedByEquals;
    }

    /**
     * Sets the value of the postedByEquals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPostedByEquals(Boolean value) {
        this.postedByEquals = value;
    }

    /**
     * Gets the value of the postedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostedDateBegins() {
        return postedDateBegins;
    }

    /**
     * Sets the value of the postedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostedDateBegins(XMLGregorianCalendar value) {
        this.postedDateBegins = value;
    }

    /**
     * Gets the value of the postedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostedDateEnds() {
        return postedDateEnds;
    }

    /**
     * Sets the value of the postedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostedDateEnds(XMLGregorianCalendar value) {
        this.postedDateEnds = value;
    }

    /**
     * Gets the value of the referenceNumberBegins property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumberBegins() {
        return referenceNumberBegins;
    }

    /**
     * Sets the value of the referenceNumberBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumberBegins(String value) {
        this.referenceNumberBegins = value;
    }

    /**
     * Gets the value of the referenceNumberEnds property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumberEnds() {
        return referenceNumberEnds;
    }

    /**
     * Sets the value of the referenceNumberEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumberEnds(String value) {
        this.referenceNumberEnds = value;
    }

    /**
     * Gets the value of the approvalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ApprovalStatus }
     *     
     */
    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * Sets the value of the approvalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApprovalStatus }
     *     
     */
    public void setApprovalStatus(ApprovalStatus value) {
        this.approvalStatus = value;
    }

}
