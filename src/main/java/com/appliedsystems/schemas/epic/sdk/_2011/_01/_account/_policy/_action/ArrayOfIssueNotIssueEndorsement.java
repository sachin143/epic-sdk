
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIssueNotIssueEndorsement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIssueNotIssueEndorsement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueNotIssueEndorsement" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}IssueNotIssueEndorsement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIssueNotIssueEndorsement", propOrder = {
    "issueNotIssueEndorsements"
})
public class ArrayOfIssueNotIssueEndorsement {

    @XmlElement(name = "IssueNotIssueEndorsement", nillable = true)
    protected List<IssueNotIssueEndorsement> issueNotIssueEndorsements;

    /**
     * Gets the value of the issueNotIssueEndorsements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the issueNotIssueEndorsements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIssueNotIssueEndorsements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IssueNotIssueEndorsement }
     * 
     * 
     */
    public List<IssueNotIssueEndorsement> getIssueNotIssueEndorsements() {
        if (issueNotIssueEndorsements == null) {
            issueNotIssueEndorsements = new ArrayList<IssueNotIssueEndorsement>();
        }
        return this.issueNotIssueEndorsements;
    }

}
