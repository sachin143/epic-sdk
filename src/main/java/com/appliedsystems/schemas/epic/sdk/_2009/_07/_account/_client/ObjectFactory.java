
package com.appliedsystems.schemas.epic.sdk._2009._07._account._client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Billing_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "Billing");
    private final static QName _CategoriesHistory_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "CategoriesHistory");
    private final static QName _ConfidentialClientAccessItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "ConfidentialClientAccessItems");
    private final static QName _Account_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "Account");
    private final static QName _ConfidentialClientAccess_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "ConfidentialClientAccess");
    private final static QName _ConfidentialClientAccessItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", "ConfidentialClientAccessItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConfidentialClientAccessItems }
     * 
     */
    public ConfidentialClientAccessItems createConfidentialClientAccessItems() {
        return new ConfidentialClientAccessItems();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link ConfidentialClientAccessItem }
     * 
     */
    public ConfidentialClientAccessItem createConfidentialClientAccessItem() {
        return new ConfidentialClientAccessItem();
    }

    /**
     * Create an instance of {@link CategoriesHistory }
     * 
     */
    public CategoriesHistory createCategoriesHistory() {
        return new CategoriesHistory();
    }

    /**
     * Create an instance of {@link Billing }
     * 
     */
    public Billing createBilling() {
        return new Billing();
    }

    /**
     * Create an instance of {@link ConfidentialClientAccess }
     * 
     */
    public ConfidentialClientAccess createConfidentialClientAccess() {
        return new ConfidentialClientAccess();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Billing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "Billing")
    public JAXBElement<Billing> createBilling(Billing value) {
        return new JAXBElement<Billing>(_Billing_QNAME, Billing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoriesHistory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "CategoriesHistory")
    public JAXBElement<CategoriesHistory> createCategoriesHistory(CategoriesHistory value) {
        return new JAXBElement<CategoriesHistory>(_CategoriesHistory_QNAME, CategoriesHistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfidentialClientAccessItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "ConfidentialClientAccessItems")
    public JAXBElement<ConfidentialClientAccessItems> createConfidentialClientAccessItems(ConfidentialClientAccessItems value) {
        return new JAXBElement<ConfidentialClientAccessItems>(_ConfidentialClientAccessItems_QNAME, ConfidentialClientAccessItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Account }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "Account")
    public JAXBElement<Account> createAccount(Account value) {
        return new JAXBElement<Account>(_Account_QNAME, Account.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfidentialClientAccess }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "ConfidentialClientAccess")
    public JAXBElement<ConfidentialClientAccess> createConfidentialClientAccess(ConfidentialClientAccess value) {
        return new JAXBElement<ConfidentialClientAccess>(_ConfidentialClientAccess_QNAME, ConfidentialClientAccess.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfidentialClientAccessItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/", name = "ConfidentialClientAccessItem")
    public JAXBElement<ConfidentialClientAccessItem> createConfidentialClientAccessItem(ConfidentialClientAccessItem value) {
        return new JAXBElement<ConfidentialClientAccessItem>(_ConfidentialClientAccessItem_QNAME, ConfidentialClientAccessItem.class, null, value);
    }

}
