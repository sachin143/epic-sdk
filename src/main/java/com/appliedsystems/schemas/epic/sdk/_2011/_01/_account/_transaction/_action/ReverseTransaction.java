
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.TransactionItems;


/**
 * <p>Java class for ReverseTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReverseTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemsToReverse" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_reversetransaction/}TransactionItems" minOccurs="0"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReverseTransaction", propOrder = {
    "associatedAccountID",
    "associatedAccountTypeCode",
    "itemsToReverse",
    "reason",
    "reasonDetails",
    "transactionID"
})
public class ReverseTransaction {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "ItemsToReverse", nillable = true)
    protected TransactionItems itemsToReverse;
    @XmlElement(name = "Reason", nillable = true)
    protected String reason;
    @XmlElement(name = "ReasonDetails", nillable = true)
    protected String reasonDetails;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the itemsToReverse property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionItems }
     *     
     */
    public TransactionItems getItemsToReverse() {
        return itemsToReverse;
    }

    /**
     * Sets the value of the itemsToReverse property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionItems }
     *     
     */
    public void setItemsToReverse(TransactionItems value) {
        this.itemsToReverse = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the reasonDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonDetails() {
        return reasonDetails;
    }

    /**
     * Sets the value of the reasonDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonDetails(String value) {
        this.reasonDetails = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

}
