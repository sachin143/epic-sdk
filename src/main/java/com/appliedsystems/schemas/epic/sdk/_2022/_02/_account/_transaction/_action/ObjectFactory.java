
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfModifyRevenueDeferralSchedule_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ArrayOfModifyRevenueDeferralSchedule");
    private final static QName _BalanceTransfer_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "BalanceTransfer");
    private final static QName _ReverseBalanceTransfer_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ReverseBalanceTransfer");
    private final static QName _Discount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "Discount");
    private final static QName _ModifyRevenueDeferralSchedule_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ModifyRevenueDeferralSchedule");
    private final static QName _ModifyRevenueDeferralScheduleItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ModifyRevenueDeferralScheduleItem");
    private final static QName _ModifyRevenueDeferralScheduleItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ModifyRevenueDeferralScheduleItems");
    private final static QName _ArrayOfDiscount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", "ArrayOfDiscount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Discount }
     * 
     */
    public Discount createDiscount() {
        return new Discount();
    }

    /**
     * Create an instance of {@link ReverseBalanceTransfer }
     * 
     */
    public ReverseBalanceTransfer createReverseBalanceTransfer() {
        return new ReverseBalanceTransfer();
    }

    /**
     * Create an instance of {@link ModifyRevenueDeferralSchedule }
     * 
     */
    public ModifyRevenueDeferralSchedule createModifyRevenueDeferralSchedule() {
        return new ModifyRevenueDeferralSchedule();
    }

    /**
     * Create an instance of {@link ArrayOfDiscount }
     * 
     */
    public ArrayOfDiscount createArrayOfDiscount() {
        return new ArrayOfDiscount();
    }

    /**
     * Create an instance of {@link BalanceTransfer }
     * 
     */
    public BalanceTransfer createBalanceTransfer() {
        return new BalanceTransfer();
    }

    /**
     * Create an instance of {@link ArrayOfModifyRevenueDeferralSchedule }
     * 
     */
    public ArrayOfModifyRevenueDeferralSchedule createArrayOfModifyRevenueDeferralSchedule() {
        return new ArrayOfModifyRevenueDeferralSchedule();
    }

    /**
     * Create an instance of {@link ModifyRevenueDeferralScheduleItem }
     * 
     */
    public ModifyRevenueDeferralScheduleItem createModifyRevenueDeferralScheduleItem() {
        return new ModifyRevenueDeferralScheduleItem();
    }

    /**
     * Create an instance of {@link ModifyRevenueDeferralScheduleItems }
     * 
     */
    public ModifyRevenueDeferralScheduleItems createModifyRevenueDeferralScheduleItems() {
        return new ModifyRevenueDeferralScheduleItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfModifyRevenueDeferralSchedule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ArrayOfModifyRevenueDeferralSchedule")
    public JAXBElement<ArrayOfModifyRevenueDeferralSchedule> createArrayOfModifyRevenueDeferralSchedule(ArrayOfModifyRevenueDeferralSchedule value) {
        return new JAXBElement<ArrayOfModifyRevenueDeferralSchedule>(_ArrayOfModifyRevenueDeferralSchedule_QNAME, ArrayOfModifyRevenueDeferralSchedule.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "BalanceTransfer")
    public JAXBElement<BalanceTransfer> createBalanceTransfer(BalanceTransfer value) {
        return new JAXBElement<BalanceTransfer>(_BalanceTransfer_QNAME, BalanceTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseBalanceTransfer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ReverseBalanceTransfer")
    public JAXBElement<ReverseBalanceTransfer> createReverseBalanceTransfer(ReverseBalanceTransfer value) {
        return new JAXBElement<ReverseBalanceTransfer>(_ReverseBalanceTransfer_QNAME, ReverseBalanceTransfer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Discount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "Discount")
    public JAXBElement<Discount> createDiscount(Discount value) {
        return new JAXBElement<Discount>(_Discount_QNAME, Discount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyRevenueDeferralSchedule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ModifyRevenueDeferralSchedule")
    public JAXBElement<ModifyRevenueDeferralSchedule> createModifyRevenueDeferralSchedule(ModifyRevenueDeferralSchedule value) {
        return new JAXBElement<ModifyRevenueDeferralSchedule>(_ModifyRevenueDeferralSchedule_QNAME, ModifyRevenueDeferralSchedule.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyRevenueDeferralScheduleItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ModifyRevenueDeferralScheduleItem")
    public JAXBElement<ModifyRevenueDeferralScheduleItem> createModifyRevenueDeferralScheduleItem(ModifyRevenueDeferralScheduleItem value) {
        return new JAXBElement<ModifyRevenueDeferralScheduleItem>(_ModifyRevenueDeferralScheduleItem_QNAME, ModifyRevenueDeferralScheduleItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyRevenueDeferralScheduleItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ModifyRevenueDeferralScheduleItems")
    public JAXBElement<ModifyRevenueDeferralScheduleItems> createModifyRevenueDeferralScheduleItems(ModifyRevenueDeferralScheduleItems value) {
        return new JAXBElement<ModifyRevenueDeferralScheduleItems>(_ModifyRevenueDeferralScheduleItems_QNAME, ModifyRevenueDeferralScheduleItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDiscount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/", name = "ArrayOfDiscount")
    public JAXBElement<ArrayOfDiscount> createArrayOfDiscount(ArrayOfDiscount value) {
        return new JAXBElement<ArrayOfDiscount>(_ArrayOfDiscount_QNAME, ArrayOfDiscount.class, null, value);
    }

}
