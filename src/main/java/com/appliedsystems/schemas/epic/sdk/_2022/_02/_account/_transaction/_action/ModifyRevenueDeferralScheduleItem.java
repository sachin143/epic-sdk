
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModifyRevenueDeferralScheduleItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyRevenueDeferralScheduleItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RevenueDeferralScheduleTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyRevenueDeferralScheduleItem", propOrder = {
    "accountingMonth",
    "amount",
    "orderNumber",
    "percentage",
    "revenueDeferralScheduleTypeCode"
})
public class ModifyRevenueDeferralScheduleItem {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "OrderNumber")
    protected Integer orderNumber;
    @XmlElement(name = "Percentage")
    protected BigDecimal percentage;
    @XmlElement(name = "RevenueDeferralScheduleTypeCode", nillable = true)
    protected String revenueDeferralScheduleTypeCode;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * Sets the value of the percentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentage(BigDecimal value) {
        this.percentage = value;
    }

    /**
     * Gets the value of the revenueDeferralScheduleTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevenueDeferralScheduleTypeCode() {
        return revenueDeferralScheduleTypeCode;
    }

    /**
     * Sets the value of the revenueDeferralScheduleTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevenueDeferralScheduleTypeCode(String value) {
        this.revenueDeferralScheduleTypeCode = value;
    }

}
