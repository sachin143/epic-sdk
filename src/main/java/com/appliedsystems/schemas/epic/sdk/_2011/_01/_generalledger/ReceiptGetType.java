
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiptGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReceiptGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ReceiptID"/>
 *     &lt;enumeration value="Filtered"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReceiptGetType")
@XmlEnum
public enum ReceiptGetType {

    @XmlEnumValue("ReceiptID")
    RECEIPT_ID("ReceiptID"),
    @XmlEnumValue("Filtered")
    FILTERED("Filtered");
    private final String value;

    ReceiptGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReceiptGetType fromValue(String v) {
        for (ReceiptGetType c: ReceiptGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
