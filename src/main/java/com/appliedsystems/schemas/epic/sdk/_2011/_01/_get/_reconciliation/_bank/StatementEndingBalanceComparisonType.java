
package com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatementEndingBalanceComparisonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StatementEndingBalanceComparisonType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EqualTo"/>
 *     &lt;enumeration value="GreaterThan"/>
 *     &lt;enumeration value="LessThan"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StatementEndingBalanceComparisonType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_reconciliation/_bank/")
@XmlEnum
public enum StatementEndingBalanceComparisonType {

    @XmlEnumValue("EqualTo")
    EQUAL_TO("EqualTo"),
    @XmlEnumValue("GreaterThan")
    GREATER_THAN("GreaterThan"),
    @XmlEnumValue("LessThan")
    LESS_THAN("LessThan");
    private final String value;

    StatementEndingBalanceComparisonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatementEndingBalanceComparisonType fromValue(String v) {
        for (StatementEndingBalanceComparisonType c: StatementEndingBalanceComparisonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
