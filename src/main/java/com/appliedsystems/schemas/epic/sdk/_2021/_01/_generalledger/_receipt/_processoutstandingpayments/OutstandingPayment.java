
package com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OutstandingPayment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutstandingPayment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ApplyTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerScheduleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutstandingPaymentAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutstandingPaymentItemID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutstandingPaymentLineId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutstandingPaymentPolicyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OutstandingPaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SelectOutstandingPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutstandingPayment", propOrder = {
    "accountingMonth",
    "amount",
    "applyTo",
    "description",
    "generalLedgerScheduleCode",
    "method",
    "outstandingPaymentAccountLookupCode",
    "outstandingPaymentItemID",
    "outstandingPaymentLineId",
    "outstandingPaymentPolicyId",
    "outstandingPaymentType",
    "paymentDate",
    "paymentID",
    "selectOutstandingPayment",
    "structureAgencyCode",
    "structureBranchCode",
    "transactionCode"
})
public class OutstandingPayment {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "Amount", nillable = true)
    protected BigDecimal amount;
    @XmlElement(name = "ApplyTo", nillable = true)
    protected String applyTo;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "GeneralLedgerScheduleCode", nillable = true)
    protected String generalLedgerScheduleCode;
    @XmlElement(name = "Method", nillable = true)
    protected String method;
    @XmlElement(name = "OutstandingPaymentAccountLookupCode", nillable = true)
    protected String outstandingPaymentAccountLookupCode;
    @XmlElement(name = "OutstandingPaymentItemID")
    protected Integer outstandingPaymentItemID;
    @XmlElement(name = "OutstandingPaymentLineId", nillable = true)
    protected Integer outstandingPaymentLineId;
    @XmlElement(name = "OutstandingPaymentPolicyId", nillable = true)
    protected Integer outstandingPaymentPolicyId;
    @XmlElement(name = "OutstandingPaymentType", nillable = true)
    protected String outstandingPaymentType;
    @XmlElement(name = "PaymentDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentDate;
    @XmlElement(name = "PaymentID", nillable = true)
    protected String paymentID;
    @XmlElement(name = "SelectOutstandingPayment")
    protected Boolean selectOutstandingPayment;
    @XmlElement(name = "StructureAgencyCode", nillable = true)
    protected String structureAgencyCode;
    @XmlElement(name = "StructureBranchCode", nillable = true)
    protected String structureBranchCode;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the applyTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyTo() {
        return applyTo;
    }

    /**
     * Sets the value of the applyTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyTo(String value) {
        this.applyTo = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the generalLedgerScheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerScheduleCode() {
        return generalLedgerScheduleCode;
    }

    /**
     * Sets the value of the generalLedgerScheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerScheduleCode(String value) {
        this.generalLedgerScheduleCode = value;
    }

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethod(String value) {
        this.method = value;
    }

    /**
     * Gets the value of the outstandingPaymentAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutstandingPaymentAccountLookupCode() {
        return outstandingPaymentAccountLookupCode;
    }

    /**
     * Sets the value of the outstandingPaymentAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutstandingPaymentAccountLookupCode(String value) {
        this.outstandingPaymentAccountLookupCode = value;
    }

    /**
     * Gets the value of the outstandingPaymentItemID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutstandingPaymentItemID() {
        return outstandingPaymentItemID;
    }

    /**
     * Sets the value of the outstandingPaymentItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutstandingPaymentItemID(Integer value) {
        this.outstandingPaymentItemID = value;
    }

    /**
     * Gets the value of the outstandingPaymentLineId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutstandingPaymentLineId() {
        return outstandingPaymentLineId;
    }

    /**
     * Sets the value of the outstandingPaymentLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutstandingPaymentLineId(Integer value) {
        this.outstandingPaymentLineId = value;
    }

    /**
     * Gets the value of the outstandingPaymentPolicyId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOutstandingPaymentPolicyId() {
        return outstandingPaymentPolicyId;
    }

    /**
     * Sets the value of the outstandingPaymentPolicyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOutstandingPaymentPolicyId(Integer value) {
        this.outstandingPaymentPolicyId = value;
    }

    /**
     * Gets the value of the outstandingPaymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutstandingPaymentType() {
        return outstandingPaymentType;
    }

    /**
     * Sets the value of the outstandingPaymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutstandingPaymentType(String value) {
        this.outstandingPaymentType = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDate(XMLGregorianCalendar value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the paymentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentID() {
        return paymentID;
    }

    /**
     * Sets the value of the paymentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentID(String value) {
        this.paymentID = value;
    }

    /**
     * Gets the value of the selectOutstandingPayment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelectOutstandingPayment() {
        return selectOutstandingPayment;
    }

    /**
     * Sets the value of the selectOutstandingPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelectOutstandingPayment(Boolean value) {
        this.selectOutstandingPayment = value;
    }

    /**
     * Gets the value of the structureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureAgencyCode() {
        return structureAgencyCode;
    }

    /**
     * Sets the value of the structureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureAgencyCode(String value) {
        this.structureAgencyCode = value;
    }

    /**
     * Gets the value of the structureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureBranchCode() {
        return structureBranchCode;
    }

    /**
     * Sets the value of the structureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureBranchCode(String value) {
        this.structureBranchCode = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

}
