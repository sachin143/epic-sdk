
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SplitReceivable_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/", "SplitReceivable");
    private final static QName _Installments_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/", "Installments");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Installments }
     * 
     */
    public Installments createInstallments() {
        return new Installments();
    }

    /**
     * Create an instance of {@link SplitReceivable }
     * 
     */
    public SplitReceivable createSplitReceivable() {
        return new SplitReceivable();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/", name = "SplitReceivable")
    public JAXBElement<SplitReceivable> createSplitReceivable(SplitReceivable value) {
        return new JAXBElement<SplitReceivable>(_SplitReceivable_QNAME, SplitReceivable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Installments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/", name = "Installments")
    public JAXBElement<Installments> createInstallments(Installments value) {
        return new JAXBElement<Installments>(_Installments_QNAME, Installments.class, null, value);
    }

}
