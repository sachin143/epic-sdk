
package com.appliedsystems.schemas.epic.sdk._2018._01._account;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSplitReceivableTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSplitReceivableTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SplitReceivableTemplate" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/}SplitReceivableTemplate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSplitReceivableTemplate", propOrder = {
    "splitReceivableTemplates"
})
public class ArrayOfSplitReceivableTemplate {

    @XmlElement(name = "SplitReceivableTemplate", nillable = true)
    protected List<SplitReceivableTemplate> splitReceivableTemplates;

    /**
     * Gets the value of the splitReceivableTemplates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitReceivableTemplates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitReceivableTemplates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SplitReceivableTemplate }
     * 
     * 
     */
    public List<SplitReceivableTemplate> getSplitReceivableTemplates() {
        if (splitReceivableTemplates == null) {
            splitReceivableTemplates = new ArrayList<SplitReceivableTemplate>();
        }
        return this.splitReceivableTemplates;
    }

}
