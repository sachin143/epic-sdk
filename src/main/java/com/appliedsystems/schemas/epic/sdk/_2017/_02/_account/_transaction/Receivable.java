
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ReceivableBalanceItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ReceivableItems;


/**
 * <p>Java class for Receivable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Receivable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceivableBalanceItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/}ReceivableBalanceItems" minOccurs="0"/>
 *         &lt;element name="ReceivableItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/}ReceivableItems" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Receivable", propOrder = {
    "receivableBalanceItems",
    "receivableItems",
    "transactionID"
})
public class Receivable {

    @XmlElement(name = "ReceivableBalanceItems", nillable = true)
    protected ReceivableBalanceItems receivableBalanceItems;
    @XmlElement(name = "ReceivableItems", nillable = true)
    protected ReceivableItems receivableItems;
    @XmlElement(name = "TransactionID", nillable = true)
    protected Integer transactionID;

    /**
     * Gets the value of the receivableBalanceItems property.
     * 
     * @return
     *     possible object is
     *     {@link ReceivableBalanceItems }
     *     
     */
    public ReceivableBalanceItems getReceivableBalanceItems() {
        return receivableBalanceItems;
    }

    /**
     * Sets the value of the receivableBalanceItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceivableBalanceItems }
     *     
     */
    public void setReceivableBalanceItems(ReceivableBalanceItems value) {
        this.receivableBalanceItems = value;
    }

    /**
     * Gets the value of the receivableItems property.
     * 
     * @return
     *     possible object is
     *     {@link ReceivableItems }
     *     
     */
    public ReceivableItems getReceivableItems() {
        return receivableItems;
    }

    /**
     * Sets the value of the receivableItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceivableItems }
     *     
     */
    public void setReceivableItems(ReceivableItems value) {
        this.receivableItems = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

}
