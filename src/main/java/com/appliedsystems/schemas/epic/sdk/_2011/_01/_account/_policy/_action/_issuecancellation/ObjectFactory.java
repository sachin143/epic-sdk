
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LineItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_issuecancellation/", "LineItems");
    private final static QName _LineItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_issuecancellation/", "LineItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LineItems }
     * 
     */
    public LineItems createLineItems() {
        return new LineItems();
    }

    /**
     * Create an instance of {@link LineItem }
     * 
     */
    public LineItem createLineItem() {
        return new LineItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_issuecancellation/", name = "LineItems")
    public JAXBElement<LineItems> createLineItems(LineItems value) {
        return new JAXBElement<LineItems>(_LineItems_QNAME, LineItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_issuecancellation/", name = "LineItem")
    public JAXBElement<LineItem> createLineItem(LineItem value) {
        return new JAXBElement<LineItem>(_LineItem_QNAME, LineItem.class, null, value);
    }

}
