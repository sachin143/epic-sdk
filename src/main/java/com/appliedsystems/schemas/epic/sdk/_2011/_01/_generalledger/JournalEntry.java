
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.Detail;


/**
 * <p>Java class for JournalEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JournalEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociateToAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssociateToAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverse" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/}Detail" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="JournalEntryAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryBackPost" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="JournalEntryDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="JournalEntryDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="JournalEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="JournalEntryRecurringEntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryReopenClosedYearsForBackPosting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="VoidDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidVoided" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApprovalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalEntry", propOrder = {
    "associateToAccountLookupCode",
    "associateToAccountType",
    "automaticallyReverse",
    "automaticallyReverseAccountingMonth",
    "automaticallyReverseDetails",
    "automaticallyReverseEffectiveDate",
    "automaticallyReverseReason",
    "automaticallyReverseReferNumber",
    "detailValue",
    "isReadOnly",
    "journalEntryAccountingMonth",
    "journalEntryBackPost",
    "journalEntryDefaultEntryID",
    "journalEntryDescription",
    "journalEntryEffectiveDate",
    "journalEntryID",
    "journalEntryRecurringEntry",
    "journalEntryReferNumber",
    "journalEntryReopenClosedYearsForBackPosting",
    "timestamp",
    "voidDetails",
    "voidReason",
    "voidReferNumber",
    "voidVoided",
    "approvalStatus"
})
public class JournalEntry {

    @XmlElement(name = "AssociateToAccountLookupCode", nillable = true)
    protected String associateToAccountLookupCode;
    @XmlElement(name = "AssociateToAccountType", nillable = true)
    protected String associateToAccountType;
    @XmlElement(name = "AutomaticallyReverse")
    protected Boolean automaticallyReverse;
    @XmlElement(name = "AutomaticallyReverseAccountingMonth", nillable = true)
    protected String automaticallyReverseAccountingMonth;
    @XmlElement(name = "AutomaticallyReverseDetails", nillable = true)
    protected String automaticallyReverseDetails;
    @XmlElement(name = "AutomaticallyReverseEffectiveDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar automaticallyReverseEffectiveDate;
    @XmlElement(name = "AutomaticallyReverseReason", nillable = true)
    protected String automaticallyReverseReason;
    @XmlElement(name = "AutomaticallyReverseReferNumber", nillable = true)
    protected String automaticallyReverseReferNumber;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "JournalEntryAccountingMonth", nillable = true)
    protected String journalEntryAccountingMonth;
    @XmlElement(name = "JournalEntryBackPost")
    protected Boolean journalEntryBackPost;
    @XmlElement(name = "JournalEntryDefaultEntryID")
    protected Integer journalEntryDefaultEntryID;
    @XmlElement(name = "JournalEntryDescription", nillable = true)
    protected String journalEntryDescription;
    @XmlElement(name = "JournalEntryEffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar journalEntryEffectiveDate;
    @XmlElement(name = "JournalEntryID")
    protected Integer journalEntryID;
    @XmlElement(name = "JournalEntryRecurringEntry", nillable = true)
    protected String journalEntryRecurringEntry;
    @XmlElement(name = "JournalEntryReferNumber", nillable = true)
    protected String journalEntryReferNumber;
    @XmlElement(name = "JournalEntryReopenClosedYearsForBackPosting")
    protected Boolean journalEntryReopenClosedYearsForBackPosting;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "VoidDetails", nillable = true)
    protected String voidDetails;
    @XmlElement(name = "VoidReason", nillable = true)
    protected String voidReason;
    @XmlElement(name = "VoidReferNumber", nillable = true)
    protected String voidReferNumber;
    @XmlElement(name = "VoidVoided", nillable = true)
    protected String voidVoided;
    @XmlElement(name = "ApprovalStatus", nillable = true)
    protected String approvalStatus;

    /**
     * Gets the value of the associateToAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociateToAccountLookupCode() {
        return associateToAccountLookupCode;
    }

    /**
     * Sets the value of the associateToAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociateToAccountLookupCode(String value) {
        this.associateToAccountLookupCode = value;
    }

    /**
     * Gets the value of the associateToAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociateToAccountType() {
        return associateToAccountType;
    }

    /**
     * Sets the value of the associateToAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociateToAccountType(String value) {
        this.associateToAccountType = value;
    }

    /**
     * Gets the value of the automaticallyReverse property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutomaticallyReverse() {
        return automaticallyReverse;
    }

    /**
     * Sets the value of the automaticallyReverse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticallyReverse(Boolean value) {
        this.automaticallyReverse = value;
    }

    /**
     * Gets the value of the automaticallyReverseAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticallyReverseAccountingMonth() {
        return automaticallyReverseAccountingMonth;
    }

    /**
     * Sets the value of the automaticallyReverseAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticallyReverseAccountingMonth(String value) {
        this.automaticallyReverseAccountingMonth = value;
    }

    /**
     * Gets the value of the automaticallyReverseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticallyReverseDetails() {
        return automaticallyReverseDetails;
    }

    /**
     * Sets the value of the automaticallyReverseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticallyReverseDetails(String value) {
        this.automaticallyReverseDetails = value;
    }

    /**
     * Gets the value of the automaticallyReverseEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAutomaticallyReverseEffectiveDate() {
        return automaticallyReverseEffectiveDate;
    }

    /**
     * Sets the value of the automaticallyReverseEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAutomaticallyReverseEffectiveDate(XMLGregorianCalendar value) {
        this.automaticallyReverseEffectiveDate = value;
    }

    /**
     * Gets the value of the automaticallyReverseReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticallyReverseReason() {
        return automaticallyReverseReason;
    }

    /**
     * Sets the value of the automaticallyReverseReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticallyReverseReason(String value) {
        this.automaticallyReverseReason = value;
    }

    /**
     * Gets the value of the automaticallyReverseReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutomaticallyReverseReferNumber() {
        return automaticallyReverseReferNumber;
    }

    /**
     * Sets the value of the automaticallyReverseReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutomaticallyReverseReferNumber(String value) {
        this.automaticallyReverseReferNumber = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the journalEntryAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryAccountingMonth() {
        return journalEntryAccountingMonth;
    }

    /**
     * Sets the value of the journalEntryAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryAccountingMonth(String value) {
        this.journalEntryAccountingMonth = value;
    }

    /**
     * Gets the value of the journalEntryBackPost property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJournalEntryBackPost() {
        return journalEntryBackPost;
    }

    /**
     * Sets the value of the journalEntryBackPost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJournalEntryBackPost(Boolean value) {
        this.journalEntryBackPost = value;
    }

    /**
     * Gets the value of the journalEntryDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getJournalEntryDefaultEntryID() {
        return journalEntryDefaultEntryID;
    }

    /**
     * Sets the value of the journalEntryDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setJournalEntryDefaultEntryID(Integer value) {
        this.journalEntryDefaultEntryID = value;
    }

    /**
     * Gets the value of the journalEntryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryDescription() {
        return journalEntryDescription;
    }

    /**
     * Sets the value of the journalEntryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryDescription(String value) {
        this.journalEntryDescription = value;
    }

    /**
     * Gets the value of the journalEntryEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJournalEntryEffectiveDate() {
        return journalEntryEffectiveDate;
    }

    /**
     * Sets the value of the journalEntryEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJournalEntryEffectiveDate(XMLGregorianCalendar value) {
        this.journalEntryEffectiveDate = value;
    }

    /**
     * Gets the value of the journalEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getJournalEntryID() {
        return journalEntryID;
    }

    /**
     * Sets the value of the journalEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setJournalEntryID(Integer value) {
        this.journalEntryID = value;
    }

    /**
     * Gets the value of the journalEntryRecurringEntry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryRecurringEntry() {
        return journalEntryRecurringEntry;
    }

    /**
     * Sets the value of the journalEntryRecurringEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryRecurringEntry(String value) {
        this.journalEntryRecurringEntry = value;
    }

    /**
     * Gets the value of the journalEntryReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJournalEntryReferNumber() {
        return journalEntryReferNumber;
    }

    /**
     * Sets the value of the journalEntryReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJournalEntryReferNumber(String value) {
        this.journalEntryReferNumber = value;
    }

    /**
     * Gets the value of the journalEntryReopenClosedYearsForBackPosting property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isJournalEntryReopenClosedYearsForBackPosting() {
        return journalEntryReopenClosedYearsForBackPosting;
    }

    /**
     * Sets the value of the journalEntryReopenClosedYearsForBackPosting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setJournalEntryReopenClosedYearsForBackPosting(Boolean value) {
        this.journalEntryReopenClosedYearsForBackPosting = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the voidDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidDetails() {
        return voidDetails;
    }

    /**
     * Sets the value of the voidDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidDetails(String value) {
        this.voidDetails = value;
    }

    /**
     * Gets the value of the voidReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReason() {
        return voidReason;
    }

    /**
     * Sets the value of the voidReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReason(String value) {
        this.voidReason = value;
    }

    /**
     * Gets the value of the voidReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReferNumber() {
        return voidReferNumber;
    }

    /**
     * Sets the value of the voidReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReferNumber(String value) {
        this.voidReferNumber = value;
    }

    /**
     * Gets the value of the voidVoided property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidVoided() {
        return voidVoided;
    }

    /**
     * Sets the value of the voidVoided property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidVoided(String value) {
        this.voidVoided = value;
    }

    /**
     * Gets the value of the approvalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * Sets the value of the approvalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalStatus(String value) {
        this.approvalStatus = value;
    }

}
