
package com.appliedsystems.schemas.epic.sdk._2011._01._account;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionGetInstallmentType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "TransactionGetInstallmentType");
    private final static QName _ArrayOfPolicy_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "ArrayOfPolicy");
    private final static QName _Transaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "Transaction");
    private final static QName _AttachedWithinType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "AttachedWithinType");
    private final static QName _ArrayOfTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "ArrayOfTransaction");
    private final static QName _Policy_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", "Policy");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Policy }
     * 
     */
    public Policy createPolicy() {
        return new Policy();
    }

    /**
     * Create an instance of {@link ArrayOfTransaction }
     * 
     */
    public ArrayOfTransaction createArrayOfTransaction() {
        return new ArrayOfTransaction();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link ArrayOfPolicy }
     * 
     */
    public ArrayOfPolicy createArrayOfPolicy() {
        return new ArrayOfPolicy();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionGetInstallmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "TransactionGetInstallmentType")
    public JAXBElement<TransactionGetInstallmentType> createTransactionGetInstallmentType(TransactionGetInstallmentType value) {
        return new JAXBElement<TransactionGetInstallmentType>(_TransactionGetInstallmentType_QNAME, TransactionGetInstallmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPolicy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "ArrayOfPolicy")
    public JAXBElement<ArrayOfPolicy> createArrayOfPolicy(ArrayOfPolicy value) {
        return new JAXBElement<ArrayOfPolicy>(_ArrayOfPolicy_QNAME, ArrayOfPolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "Transaction")
    public JAXBElement<Transaction> createTransaction(Transaction value) {
        return new JAXBElement<Transaction>(_Transaction_QNAME, Transaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachedWithinType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "AttachedWithinType")
    public JAXBElement<AttachedWithinType> createAttachedWithinType(AttachedWithinType value) {
        return new JAXBElement<AttachedWithinType>(_AttachedWithinType_QNAME, AttachedWithinType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "ArrayOfTransaction")
    public JAXBElement<ArrayOfTransaction> createArrayOfTransaction(ArrayOfTransaction value) {
        return new JAXBElement<ArrayOfTransaction>(_ArrayOfTransaction_QNAME, ArrayOfTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Policy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/", name = "Policy")
    public JAXBElement<Policy> createPolicy(Policy value) {
        return new JAXBElement<Policy>(_Policy_QNAME, Policy.class, null, value);
    }

}
