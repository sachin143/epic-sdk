
package com.appliedsystems.schemas.epic.sdk._2019._01._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StructureItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructureItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StructureItem" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/}StructureItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructureItems", propOrder = {
    "structureItems"
})
public class StructureItems {

    @XmlElement(name = "StructureItem", nillable = true)
    protected List<StructureItem> structureItems;

    /**
     * Gets the value of the structureItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the structureItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStructureItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructureItem }
     * 
     * 
     */
    public List<StructureItem> getStructureItems() {
        if (structureItems == null) {
            structureItems = new ArrayList<StructureItem>();
        }
        return this.structureItems;
    }

}
