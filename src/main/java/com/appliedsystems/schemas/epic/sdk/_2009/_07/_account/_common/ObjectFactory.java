
package com.appliedsystems.schemas.epic.sdk._2009._07._account._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgencyStructureItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "AgencyStructureItem");
    private final static QName _ServicingRolesItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "ServicingRolesItems");
    private final static QName _AgencyDefinedCodeItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "AgencyDefinedCodeItem");
    private final static QName _AgencyDefinedCodeItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "AgencyDefinedCodeItems");
    private final static QName _RelationshipItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "RelationshipItems");
    private final static QName _RelationshipItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "RelationshipItem");
    private final static QName _AgencyStructureItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "AgencyStructureItems");
    private final static QName _ServicingRoleItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", "ServicingRoleItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AgencyDefinedCodeItems }
     * 
     */
    public AgencyDefinedCodeItems createAgencyDefinedCodeItems() {
        return new AgencyDefinedCodeItems();
    }

    /**
     * Create an instance of {@link ServicingRolesItems }
     * 
     */
    public ServicingRolesItems createServicingRolesItems() {
        return new ServicingRolesItems();
    }

    /**
     * Create an instance of {@link AgencyStructureItems }
     * 
     */
    public AgencyStructureItems createAgencyStructureItems() {
        return new AgencyStructureItems();
    }

    /**
     * Create an instance of {@link AgencyStructureItem }
     * 
     */
    public AgencyStructureItem createAgencyStructureItem() {
        return new AgencyStructureItem();
    }

    /**
     * Create an instance of {@link RelationshipItems }
     * 
     */
    public RelationshipItems createRelationshipItems() {
        return new RelationshipItems();
    }

    /**
     * Create an instance of {@link ServicingRoleItem }
     * 
     */
    public ServicingRoleItem createServicingRoleItem() {
        return new ServicingRoleItem();
    }

    /**
     * Create an instance of {@link RelationshipItem }
     * 
     */
    public RelationshipItem createRelationshipItem() {
        return new RelationshipItem();
    }

    /**
     * Create an instance of {@link AgencyDefinedCodeItem }
     * 
     */
    public AgencyDefinedCodeItem createAgencyDefinedCodeItem() {
        return new AgencyDefinedCodeItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyStructureItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "AgencyStructureItem")
    public JAXBElement<AgencyStructureItem> createAgencyStructureItem(AgencyStructureItem value) {
        return new JAXBElement<AgencyStructureItem>(_AgencyStructureItem_QNAME, AgencyStructureItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServicingRolesItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "ServicingRolesItems")
    public JAXBElement<ServicingRolesItems> createServicingRolesItems(ServicingRolesItems value) {
        return new JAXBElement<ServicingRolesItems>(_ServicingRolesItems_QNAME, ServicingRolesItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyDefinedCodeItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "AgencyDefinedCodeItem")
    public JAXBElement<AgencyDefinedCodeItem> createAgencyDefinedCodeItem(AgencyDefinedCodeItem value) {
        return new JAXBElement<AgencyDefinedCodeItem>(_AgencyDefinedCodeItem_QNAME, AgencyDefinedCodeItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyDefinedCodeItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "AgencyDefinedCodeItems")
    public JAXBElement<AgencyDefinedCodeItems> createAgencyDefinedCodeItems(AgencyDefinedCodeItems value) {
        return new JAXBElement<AgencyDefinedCodeItems>(_AgencyDefinedCodeItems_QNAME, AgencyDefinedCodeItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelationshipItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "RelationshipItems")
    public JAXBElement<RelationshipItems> createRelationshipItems(RelationshipItems value) {
        return new JAXBElement<RelationshipItems>(_RelationshipItems_QNAME, RelationshipItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelationshipItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "RelationshipItem")
    public JAXBElement<RelationshipItem> createRelationshipItem(RelationshipItem value) {
        return new JAXBElement<RelationshipItem>(_RelationshipItem_QNAME, RelationshipItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyStructureItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "AgencyStructureItems")
    public JAXBElement<AgencyStructureItems> createAgencyStructureItems(AgencyStructureItems value) {
        return new JAXBElement<AgencyStructureItems>(_AgencyStructureItems_QNAME, AgencyStructureItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServicingRoleItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/", name = "ServicingRoleItem")
    public JAXBElement<ServicingRoleItem> createServicingRoleItem(ServicingRoleItem value) {
        return new JAXBElement<ServicingRoleItem>(_ServicingRoleItem_QNAME, ServicingRoleItem.class, null, value);
    }

}
