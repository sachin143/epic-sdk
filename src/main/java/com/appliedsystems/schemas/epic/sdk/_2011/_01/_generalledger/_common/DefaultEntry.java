
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for DefaultEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NewDefaultEntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewDefaultEntryOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="OverrideExistingDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultEntry", propOrder = {
    "newDefaultEntry",
    "newDefaultEntryOption",
    "overrideExistingDefaultEntryID"
})
public class DefaultEntry {

    @XmlElement(name = "NewDefaultEntry", nillable = true)
    protected String newDefaultEntry;
    @XmlElement(name = "NewDefaultEntryOption", nillable = true)
    protected OptionType newDefaultEntryOption;
    @XmlElement(name = "OverrideExistingDefaultEntryID")
    protected Integer overrideExistingDefaultEntryID;

    /**
     * Gets the value of the newDefaultEntry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewDefaultEntry() {
        return newDefaultEntry;
    }

    /**
     * Sets the value of the newDefaultEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewDefaultEntry(String value) {
        this.newDefaultEntry = value;
    }

    /**
     * Gets the value of the newDefaultEntryOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getNewDefaultEntryOption() {
        return newDefaultEntryOption;
    }

    /**
     * Sets the value of the newDefaultEntryOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setNewDefaultEntryOption(OptionType value) {
        this.newDefaultEntryOption = value;
    }

    /**
     * Gets the value of the overrideExistingDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOverrideExistingDefaultEntryID() {
        return overrideExistingDefaultEntryID;
    }

    /**
     * Sets the value of the overrideExistingDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOverrideExistingDefaultEntryID(Integer value) {
        this.overrideExistingDefaultEntryID = value;
    }

}
