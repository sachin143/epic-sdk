
package com.appliedsystems.schemas.epic.sdk._2018._01._account._client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SplitReceivableTemplateGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SplitReceivableTemplateGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SplitReceivableTemplateID"/>
 *     &lt;enumeration value="ClientID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SplitReceivableTemplateGetType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_client/")
@XmlEnum
public enum SplitReceivableTemplateGetType {

    @XmlEnumValue("SplitReceivableTemplateID")
    SPLIT_RECEIVABLE_TEMPLATE_ID("SplitReceivableTemplateID"),
    @XmlEnumValue("ClientID")
    CLIENT_ID("ClientID");
    private final String value;

    SplitReceivableTemplateGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SplitReceivableTemplateGetType fromValue(String v) {
        for (SplitReceivableTemplateGetType c: SplitReceivableTemplateGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
