
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModifyRevenueDeferralScheduleItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyRevenueDeferralScheduleItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModifyRevenueDeferralScheduleItem" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ModifyRevenueDeferralScheduleItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyRevenueDeferralScheduleItems", propOrder = {
    "modifyRevenueDeferralScheduleItems"
})
public class ModifyRevenueDeferralScheduleItems {

    @XmlElement(name = "ModifyRevenueDeferralScheduleItem", nillable = true)
    protected List<ModifyRevenueDeferralScheduleItem> modifyRevenueDeferralScheduleItems;

    /**
     * Gets the value of the modifyRevenueDeferralScheduleItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifyRevenueDeferralScheduleItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifyRevenueDeferralScheduleItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModifyRevenueDeferralScheduleItem }
     * 
     * 
     */
    public List<ModifyRevenueDeferralScheduleItem> getModifyRevenueDeferralScheduleItems() {
        if (modifyRevenueDeferralScheduleItems == null) {
            modifyRevenueDeferralScheduleItems = new ArrayList<ModifyRevenueDeferralScheduleItem>();
        }
        return this.modifyRevenueDeferralScheduleItems;
    }

}
