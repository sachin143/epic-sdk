
package com.appliedsystems.schemas.epic.sdk._2017._02._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.Flags;


/**
 * <p>Java class for PaymentMethodItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentMethodItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthorizationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AuthorizationStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckingSavingsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryOfOperationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/_paymentmethoditem/}Flags" minOccurs="0"/>
 *         &lt;element name="IgnoreDuplicatePaymentMethods" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InstitutionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstitutionPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstitutionPhoneExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InstitutionPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameOfAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameOfInstitution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Primary" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransitNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpdateProcessedTransmittedItems" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethodItem", propOrder = {
    "accountNumber",
    "authorizationDate",
    "authorizationStatusCode",
    "checkingSavingsOption",
    "comments",
    "countryOfOperationCode",
    "description",
    "flag",
    "ignoreDuplicatePaymentMethods",
    "institutionNumber",
    "institutionPhoneCountryCode",
    "institutionPhoneExtension",
    "institutionPhoneNumber",
    "methodCode",
    "nameOfAccount",
    "nameOfInstitution",
    "paymentMethodID",
    "primary",
    "routingNumber",
    "serviceClassCode",
    "transitNumber",
    "updateProcessedTransmittedItems"
})
public class PaymentMethodItem {

    @XmlElement(name = "AccountNumber", nillable = true)
    protected String accountNumber;
    @XmlElement(name = "AuthorizationDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar authorizationDate;
    @XmlElement(name = "AuthorizationStatusCode", nillable = true)
    protected String authorizationStatusCode;
    @XmlElement(name = "CheckingSavingsOption", nillable = true)
    protected OptionType checkingSavingsOption;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "CountryOfOperationCode", nillable = true)
    protected String countryOfOperationCode;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "IgnoreDuplicatePaymentMethods")
    protected Boolean ignoreDuplicatePaymentMethods;
    @XmlElement(name = "InstitutionNumber", nillable = true)
    protected String institutionNumber;
    @XmlElement(name = "InstitutionPhoneCountryCode", nillable = true)
    protected String institutionPhoneCountryCode;
    @XmlElement(name = "InstitutionPhoneExtension", nillable = true)
    protected String institutionPhoneExtension;
    @XmlElement(name = "InstitutionPhoneNumber", nillable = true)
    protected String institutionPhoneNumber;
    @XmlElement(name = "MethodCode", nillable = true)
    protected String methodCode;
    @XmlElement(name = "NameOfAccount", nillable = true)
    protected String nameOfAccount;
    @XmlElement(name = "NameOfInstitution", nillable = true)
    protected String nameOfInstitution;
    @XmlElement(name = "PaymentMethodID")
    protected Integer paymentMethodID;
    @XmlElement(name = "Primary", nillable = true)
    protected Boolean primary;
    @XmlElement(name = "RoutingNumber", nillable = true)
    protected String routingNumber;
    @XmlElement(name = "ServiceClassCode", nillable = true)
    protected String serviceClassCode;
    @XmlElement(name = "TransitNumber", nillable = true)
    protected String transitNumber;
    @XmlElement(name = "UpdateProcessedTransmittedItems")
    protected Boolean updateProcessedTransmittedItems;

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the authorizationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAuthorizationDate() {
        return authorizationDate;
    }

    /**
     * Sets the value of the authorizationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAuthorizationDate(XMLGregorianCalendar value) {
        this.authorizationDate = value;
    }

    /**
     * Gets the value of the authorizationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationStatusCode() {
        return authorizationStatusCode;
    }

    /**
     * Sets the value of the authorizationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationStatusCode(String value) {
        this.authorizationStatusCode = value;
    }

    /**
     * Gets the value of the checkingSavingsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCheckingSavingsOption() {
        return checkingSavingsOption;
    }

    /**
     * Sets the value of the checkingSavingsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCheckingSavingsOption(OptionType value) {
        this.checkingSavingsOption = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the countryOfOperationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfOperationCode() {
        return countryOfOperationCode;
    }

    /**
     * Sets the value of the countryOfOperationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfOperationCode(String value) {
        this.countryOfOperationCode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the ignoreDuplicatePaymentMethods property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreDuplicatePaymentMethods() {
        return ignoreDuplicatePaymentMethods;
    }

    /**
     * Sets the value of the ignoreDuplicatePaymentMethods property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreDuplicatePaymentMethods(Boolean value) {
        this.ignoreDuplicatePaymentMethods = value;
    }

    /**
     * Gets the value of the institutionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionNumber() {
        return institutionNumber;
    }

    /**
     * Sets the value of the institutionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionNumber(String value) {
        this.institutionNumber = value;
    }

    /**
     * Gets the value of the institutionPhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPhoneCountryCode() {
        return institutionPhoneCountryCode;
    }

    /**
     * Sets the value of the institutionPhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPhoneCountryCode(String value) {
        this.institutionPhoneCountryCode = value;
    }

    /**
     * Gets the value of the institutionPhoneExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPhoneExtension() {
        return institutionPhoneExtension;
    }

    /**
     * Sets the value of the institutionPhoneExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPhoneExtension(String value) {
        this.institutionPhoneExtension = value;
    }

    /**
     * Gets the value of the institutionPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstitutionPhoneNumber() {
        return institutionPhoneNumber;
    }

    /**
     * Sets the value of the institutionPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstitutionPhoneNumber(String value) {
        this.institutionPhoneNumber = value;
    }

    /**
     * Gets the value of the methodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodCode() {
        return methodCode;
    }

    /**
     * Sets the value of the methodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodCode(String value) {
        this.methodCode = value;
    }

    /**
     * Gets the value of the nameOfAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOfAccount() {
        return nameOfAccount;
    }

    /**
     * Sets the value of the nameOfAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOfAccount(String value) {
        this.nameOfAccount = value;
    }

    /**
     * Gets the value of the nameOfInstitution property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOfInstitution() {
        return nameOfInstitution;
    }

    /**
     * Sets the value of the nameOfInstitution property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOfInstitution(String value) {
        this.nameOfInstitution = value;
    }

    /**
     * Gets the value of the paymentMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaymentMethodID() {
        return paymentMethodID;
    }

    /**
     * Sets the value of the paymentMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaymentMethodID(Integer value) {
        this.paymentMethodID = value;
    }

    /**
     * Gets the value of the primary property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimary(Boolean value) {
        this.primary = value;
    }

    /**
     * Gets the value of the routingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Sets the value of the routingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoutingNumber(String value) {
        this.routingNumber = value;
    }

    /**
     * Gets the value of the serviceClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceClassCode() {
        return serviceClassCode;
    }

    /**
     * Sets the value of the serviceClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceClassCode(String value) {
        this.serviceClassCode = value;
    }

    /**
     * Gets the value of the transitNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitNumber() {
        return transitNumber;
    }

    /**
     * Sets the value of the transitNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitNumber(String value) {
        this.transitNumber = value;
    }

    /**
     * Gets the value of the updateProcessedTransmittedItems property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUpdateProcessedTransmittedItems() {
        return updateProcessedTransmittedItems;
    }

    /**
     * Sets the value of the updateProcessedTransmittedItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpdateProcessedTransmittedItems(Boolean value) {
        this.updateProcessedTransmittedItems = value;
    }

}
