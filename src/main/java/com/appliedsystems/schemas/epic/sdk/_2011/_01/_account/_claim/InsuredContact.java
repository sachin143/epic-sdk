
package com.appliedsystems.schemas.epic.sdk._2011._01._account._claim;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.AuthorityContacted;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.Contact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.Insured;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.Spouse;


/**
 * <p>Java class for InsuredContact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuredContact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorityContactedValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/}AuthorityContacted" minOccurs="0"/>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ContactValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/}Contact" minOccurs="0"/>
 *         &lt;element name="InsuredLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuredReportNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuredValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/}Insured" minOccurs="0"/>
 *         &lt;element name="JurisdictionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JurisdictionLogNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OSHACaseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportPurposeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpouseValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/_insuredcontact/}Spouse" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuredContact", propOrder = {
    "authorityContactedValue",
    "claimID",
    "contactValue",
    "insuredLocationCode",
    "insuredReportNumber",
    "insuredValue",
    "jurisdictionCode",
    "jurisdictionLogNumber",
    "oshaCaseNumber",
    "reportPurposeCode",
    "spouseValue",
    "timestamp"
})
public class InsuredContact {

    @XmlElement(name = "AuthorityContactedValue", nillable = true)
    protected AuthorityContacted authorityContactedValue;
    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "ContactValue", nillable = true)
    protected Contact contactValue;
    @XmlElement(name = "InsuredLocationCode", nillable = true)
    protected String insuredLocationCode;
    @XmlElement(name = "InsuredReportNumber", nillable = true)
    protected String insuredReportNumber;
    @XmlElement(name = "InsuredValue", nillable = true)
    protected Insured insuredValue;
    @XmlElement(name = "JurisdictionCode", nillable = true)
    protected String jurisdictionCode;
    @XmlElement(name = "JurisdictionLogNumber", nillable = true)
    protected String jurisdictionLogNumber;
    @XmlElement(name = "OSHACaseNumber", nillable = true)
    protected String oshaCaseNumber;
    @XmlElement(name = "ReportPurposeCode", nillable = true)
    protected String reportPurposeCode;
    @XmlElement(name = "SpouseValue", nillable = true)
    protected Spouse spouseValue;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the authorityContactedValue property.
     * 
     * @return
     *     possible object is
     *     {@link AuthorityContacted }
     *     
     */
    public AuthorityContacted getAuthorityContactedValue() {
        return authorityContactedValue;
    }

    /**
     * Sets the value of the authorityContactedValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorityContacted }
     *     
     */
    public void setAuthorityContactedValue(AuthorityContacted value) {
        this.authorityContactedValue = value;
    }

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the contactValue property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContactValue() {
        return contactValue;
    }

    /**
     * Sets the value of the contactValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContactValue(Contact value) {
        this.contactValue = value;
    }

    /**
     * Gets the value of the insuredLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredLocationCode() {
        return insuredLocationCode;
    }

    /**
     * Sets the value of the insuredLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredLocationCode(String value) {
        this.insuredLocationCode = value;
    }

    /**
     * Gets the value of the insuredReportNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredReportNumber() {
        return insuredReportNumber;
    }

    /**
     * Sets the value of the insuredReportNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredReportNumber(String value) {
        this.insuredReportNumber = value;
    }

    /**
     * Gets the value of the insuredValue property.
     * 
     * @return
     *     possible object is
     *     {@link Insured }
     *     
     */
    public Insured getInsuredValue() {
        return insuredValue;
    }

    /**
     * Sets the value of the insuredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insured }
     *     
     */
    public void setInsuredValue(Insured value) {
        this.insuredValue = value;
    }

    /**
     * Gets the value of the jurisdictionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdictionCode() {
        return jurisdictionCode;
    }

    /**
     * Sets the value of the jurisdictionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdictionCode(String value) {
        this.jurisdictionCode = value;
    }

    /**
     * Gets the value of the jurisdictionLogNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdictionLogNumber() {
        return jurisdictionLogNumber;
    }

    /**
     * Sets the value of the jurisdictionLogNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdictionLogNumber(String value) {
        this.jurisdictionLogNumber = value;
    }

    /**
     * Gets the value of the oshaCaseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOSHACaseNumber() {
        return oshaCaseNumber;
    }

    /**
     * Sets the value of the oshaCaseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOSHACaseNumber(String value) {
        this.oshaCaseNumber = value;
    }

    /**
     * Gets the value of the reportPurposeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportPurposeCode() {
        return reportPurposeCode;
    }

    /**
     * Sets the value of the reportPurposeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportPurposeCode(String value) {
        this.reportPurposeCode = value;
    }

    /**
     * Gets the value of the spouseValue property.
     * 
     * @return
     *     possible object is
     *     {@link Spouse }
     *     
     */
    public Spouse getSpouseValue() {
        return spouseValue;
    }

    /**
     * Sets the value of the spouseValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Spouse }
     *     
     */
    public void setSpouseValue(Spouse value) {
        this.spouseValue = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
