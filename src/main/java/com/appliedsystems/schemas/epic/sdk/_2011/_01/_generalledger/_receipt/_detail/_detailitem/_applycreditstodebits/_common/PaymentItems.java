
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/_common/}PaymentItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItems", propOrder = {
    "paymentItems"
})
public class PaymentItems {

    @XmlElement(name = "PaymentItem", nillable = true)
    protected List<PaymentItem> paymentItems;

    /**
     * Gets the value of the paymentItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentItem }
     * 
     * 
     */
    public List<PaymentItem> getPaymentItems() {
        if (paymentItems == null) {
            paymentItems = new ArrayList<PaymentItem>();
        }
        return this.paymentItems;
    }

}
