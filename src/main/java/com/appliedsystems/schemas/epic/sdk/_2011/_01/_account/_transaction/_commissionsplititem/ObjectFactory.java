
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProducerBrokerCommissionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/", "ProducerBrokerCommissionItem");
    private final static QName _ProducerBrokerCommissionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/", "ProducerBrokerCommissionItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProducerBrokerCommissionItem }
     * 
     */
    public ProducerBrokerCommissionItem createProducerBrokerCommissionItem() {
        return new ProducerBrokerCommissionItem();
    }

    /**
     * Create an instance of {@link ProducerBrokerCommissionItems }
     * 
     */
    public ProducerBrokerCommissionItems createProducerBrokerCommissionItems() {
        return new ProducerBrokerCommissionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerCommissionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/", name = "ProducerBrokerCommissionItem")
    public JAXBElement<ProducerBrokerCommissionItem> createProducerBrokerCommissionItem(ProducerBrokerCommissionItem value) {
        return new JAXBElement<ProducerBrokerCommissionItem>(_ProducerBrokerCommissionItem_QNAME, ProducerBrokerCommissionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerCommissionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/", name = "ProducerBrokerCommissionItems")
    public JAXBElement<ProducerBrokerCommissionItems> createProducerBrokerCommissionItems(ProducerBrokerCommissionItems value) {
        return new JAXBElement<ProducerBrokerCommissionItems>(_ProducerBrokerCommissionItems_QNAME, ProducerBrokerCommissionItems.class, null, value);
    }

}
