
package com.appliedsystems.schemas.epic.sdk._2009._07._account._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.Flags;


/**
 * <p>Java class for AgencyDefinedCodeItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgencyDefinedCodeItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ADCCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ADCOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/_agencydefinedcodeitem/}Flags" minOccurs="0"/>
 *         &lt;element name="AppliesTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgencyDefinedCodeItem", propOrder = {
    "adcCategory",
    "adcOption",
    "flag",
    "appliesTo"
})
public class AgencyDefinedCodeItem {

    @XmlElement(name = "ADCCategory", nillable = true)
    protected String adcCategory;
    @XmlElement(name = "ADCOption", nillable = true)
    protected String adcOption;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "AppliesTo", nillable = true)
    protected String appliesTo;

    /**
     * Gets the value of the adcCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADCCategory() {
        return adcCategory;
    }

    /**
     * Sets the value of the adcCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADCCategory(String value) {
        this.adcCategory = value;
    }

    /**
     * Gets the value of the adcOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADCOption() {
        return adcOption;
    }

    /**
     * Sets the value of the adcOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADCOption(String value) {
        this.adcOption = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the appliesTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliesTo() {
        return appliesTo;
    }

    /**
     * Sets the value of the appliesTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliesTo(String value) {
        this.appliesTo = value;
    }

}
