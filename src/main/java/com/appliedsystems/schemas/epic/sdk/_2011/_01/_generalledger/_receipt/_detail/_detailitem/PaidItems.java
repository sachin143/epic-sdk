
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaidItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaidItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaidItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}PaidItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaidItems", propOrder = {
    "paidItems"
})
public class PaidItems {

    @XmlElement(name = "PaidItem", nillable = true)
    protected List<PaidItem> paidItems;

    /**
     * Gets the value of the paidItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paidItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaidItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaidItem }
     * 
     * 
     */
    public List<PaidItem> getPaidItems() {
        if (paidItems == null) {
            paidItems = new ArrayList<PaidItem>();
        }
        return this.paidItems;
    }

}
