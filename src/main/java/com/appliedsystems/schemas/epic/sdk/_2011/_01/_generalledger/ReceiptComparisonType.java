
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiptComparisonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReceiptComparisonType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="EqualTo"/>
 *     &lt;enumeration value="WithinRange"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReceiptComparisonType")
@XmlEnum
public enum ReceiptComparisonType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("EqualTo")
    EQUAL_TO("EqualTo"),
    @XmlEnumValue("WithinRange")
    WITHIN_RANGE("WithinRange");
    private final String value;

    ReceiptComparisonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReceiptComparisonType fromValue(String v) {
        for (ReceiptComparisonType c: ReceiptComparisonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
