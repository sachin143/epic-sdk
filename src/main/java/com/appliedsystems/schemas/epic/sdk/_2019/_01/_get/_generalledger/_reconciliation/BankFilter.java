
package com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.StatementEndingBalanceComparisonType;


/**
 * <p>Java class for BankFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankReconciliationID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BankSubAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnteredDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EnteredDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LastSixMonthsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="StatementDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="StatementEndingBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="StatementEndingBalanceComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_reconciliation/_bank/}StatementEndingBalanceComparisonType" minOccurs="0"/>
 *         &lt;element name="StatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SuspendedOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankFilter", propOrder = {
    "bankAccountCode",
    "bankReconciliationID",
    "bankSubAccountCode",
    "enteredDateBegins",
    "enteredDateEnds",
    "lastSixMonthsOnly",
    "statementDateBegins",
    "statementDateEnds",
    "statementEndingBalance",
    "statementEndingBalanceComparisonType",
    "statementNumber",
    "suspendedOnly"
})
public class BankFilter {

    @XmlElement(name = "BankAccountCode", nillable = true)
    protected String bankAccountCode;
    @XmlElement(name = "BankReconciliationID", nillable = true)
    protected Integer bankReconciliationID;
    @XmlElement(name = "BankSubAccountCode", nillable = true)
    protected String bankSubAccountCode;
    @XmlElement(name = "EnteredDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredDateBegins;
    @XmlElement(name = "EnteredDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredDateEnds;
    @XmlElement(name = "LastSixMonthsOnly", nillable = true)
    protected Boolean lastSixMonthsOnly;
    @XmlElement(name = "StatementDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementDateBegins;
    @XmlElement(name = "StatementDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementDateEnds;
    @XmlElement(name = "StatementEndingBalance", nillable = true)
    protected BigDecimal statementEndingBalance;
    @XmlElement(name = "StatementEndingBalanceComparisonType")
    @XmlSchemaType(name = "string")
    protected StatementEndingBalanceComparisonType statementEndingBalanceComparisonType;
    @XmlElement(name = "StatementNumber", nillable = true)
    protected String statementNumber;
    @XmlElement(name = "SuspendedOnly", nillable = true)
    protected Boolean suspendedOnly;

    /**
     * Gets the value of the bankAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountCode() {
        return bankAccountCode;
    }

    /**
     * Sets the value of the bankAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountCode(String value) {
        this.bankAccountCode = value;
    }

    /**
     * Gets the value of the bankReconciliationID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBankReconciliationID() {
        return bankReconciliationID;
    }

    /**
     * Sets the value of the bankReconciliationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBankReconciliationID(Integer value) {
        this.bankReconciliationID = value;
    }

    /**
     * Gets the value of the bankSubAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankSubAccountCode() {
        return bankSubAccountCode;
    }

    /**
     * Sets the value of the bankSubAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankSubAccountCode(String value) {
        this.bankSubAccountCode = value;
    }

    /**
     * Gets the value of the enteredDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredDateBegins() {
        return enteredDateBegins;
    }

    /**
     * Sets the value of the enteredDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredDateBegins(XMLGregorianCalendar value) {
        this.enteredDateBegins = value;
    }

    /**
     * Gets the value of the enteredDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredDateEnds() {
        return enteredDateEnds;
    }

    /**
     * Sets the value of the enteredDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredDateEnds(XMLGregorianCalendar value) {
        this.enteredDateEnds = value;
    }

    /**
     * Gets the value of the lastSixMonthsOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLastSixMonthsOnly() {
        return lastSixMonthsOnly;
    }

    /**
     * Sets the value of the lastSixMonthsOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLastSixMonthsOnly(Boolean value) {
        this.lastSixMonthsOnly = value;
    }

    /**
     * Gets the value of the statementDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementDateBegins() {
        return statementDateBegins;
    }

    /**
     * Sets the value of the statementDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementDateBegins(XMLGregorianCalendar value) {
        this.statementDateBegins = value;
    }

    /**
     * Gets the value of the statementDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementDateEnds() {
        return statementDateEnds;
    }

    /**
     * Sets the value of the statementDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementDateEnds(XMLGregorianCalendar value) {
        this.statementDateEnds = value;
    }

    /**
     * Gets the value of the statementEndingBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStatementEndingBalance() {
        return statementEndingBalance;
    }

    /**
     * Sets the value of the statementEndingBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStatementEndingBalance(BigDecimal value) {
        this.statementEndingBalance = value;
    }

    /**
     * Gets the value of the statementEndingBalanceComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link StatementEndingBalanceComparisonType }
     *     
     */
    public StatementEndingBalanceComparisonType getStatementEndingBalanceComparisonType() {
        return statementEndingBalanceComparisonType;
    }

    /**
     * Sets the value of the statementEndingBalanceComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatementEndingBalanceComparisonType }
     *     
     */
    public void setStatementEndingBalanceComparisonType(StatementEndingBalanceComparisonType value) {
        this.statementEndingBalanceComparisonType = value;
    }

    /**
     * Gets the value of the statementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementNumber() {
        return statementNumber;
    }

    /**
     * Sets the value of the statementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementNumber(String value) {
        this.statementNumber = value;
    }

    /**
     * Gets the value of the suspendedOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuspendedOnly() {
        return suspendedOnly;
    }

    /**
     * Sets the value of the suspendedOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuspendedOnly(Boolean value) {
        this.suspendedOnly = value;
    }

}
