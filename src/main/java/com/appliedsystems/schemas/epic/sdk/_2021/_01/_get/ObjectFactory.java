
package com.appliedsystems.schemas.epic.sdk._2021._01._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AllocationMethodsGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", "AllocationMethodsGetResult");
    private final static QName _AllocationStructureGroupingsGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", "AllocationStructureGroupingsGetResult");
    private final static QName _TaxFeeRateGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", "TaxFeeRateGetResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AllocationMethodsGetResult }
     * 
     */
    public AllocationMethodsGetResult createAllocationMethodsGetResult() {
        return new AllocationMethodsGetResult();
    }

    /**
     * Create an instance of {@link TaxFeeRateGetResult }
     * 
     */
    public TaxFeeRateGetResult createTaxFeeRateGetResult() {
        return new TaxFeeRateGetResult();
    }

    /**
     * Create an instance of {@link AllocationStructureGroupingsGetResult }
     * 
     */
    public AllocationStructureGroupingsGetResult createAllocationStructureGroupingsGetResult() {
        return new AllocationStructureGroupingsGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationMethodsGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", name = "AllocationMethodsGetResult")
    public JAXBElement<AllocationMethodsGetResult> createAllocationMethodsGetResult(AllocationMethodsGetResult value) {
        return new JAXBElement<AllocationMethodsGetResult>(_AllocationMethodsGetResult_QNAME, AllocationMethodsGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationStructureGroupingsGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", name = "AllocationStructureGroupingsGetResult")
    public JAXBElement<AllocationStructureGroupingsGetResult> createAllocationStructureGroupingsGetResult(AllocationStructureGroupingsGetResult value) {
        return new JAXBElement<AllocationStructureGroupingsGetResult>(_AllocationStructureGroupingsGetResult_QNAME, AllocationStructureGroupingsGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFeeRateGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/", name = "TaxFeeRateGetResult")
    public JAXBElement<TaxFeeRateGetResult> createTaxFeeRateGetResult(TaxFeeRateGetResult value) {
        return new JAXBElement<TaxFeeRateGetResult>(_TaxFeeRateGetResult_QNAME, TaxFeeRateGetResult.class, null, value);
    }

}
