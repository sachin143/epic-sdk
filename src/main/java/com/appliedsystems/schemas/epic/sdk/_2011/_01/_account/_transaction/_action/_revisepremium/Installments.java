
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.InstallmentSummaryItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ReviseInstallmentsBasedOn;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.SplitReceivableItems;


/**
 * <p>Java class for Installments complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Installments">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InstallmentSummary" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/}InstallmentSummaryItems" minOccurs="0"/>
 *         &lt;element name="ReviseInstallmentsBasedOn" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/}ReviseInstallmentsBasedOn" minOccurs="0"/>
 *         &lt;element name="SplitReceivable" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/}SplitReceivableItems" minOccurs="0"/>
 *         &lt;element name="TotalInstallmentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Installments", propOrder = {
    "installmentSummary",
    "reviseInstallmentsBasedOn",
    "splitReceivable",
    "totalInstallmentAmount"
})
public class Installments {

    @XmlElement(name = "InstallmentSummary", nillable = true)
    protected InstallmentSummaryItems installmentSummary;
    @XmlElement(name = "ReviseInstallmentsBasedOn")
    @XmlSchemaType(name = "string")
    protected ReviseInstallmentsBasedOn reviseInstallmentsBasedOn;
    @XmlElement(name = "SplitReceivable", nillable = true)
    protected SplitReceivableItems splitReceivable;
    @XmlElement(name = "TotalInstallmentAmount")
    protected BigDecimal totalInstallmentAmount;

    /**
     * Gets the value of the installmentSummary property.
     * 
     * @return
     *     possible object is
     *     {@link InstallmentSummaryItems }
     *     
     */
    public InstallmentSummaryItems getInstallmentSummary() {
        return installmentSummary;
    }

    /**
     * Sets the value of the installmentSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstallmentSummaryItems }
     *     
     */
    public void setInstallmentSummary(InstallmentSummaryItems value) {
        this.installmentSummary = value;
    }

    /**
     * Gets the value of the reviseInstallmentsBasedOn property.
     * 
     * @return
     *     possible object is
     *     {@link ReviseInstallmentsBasedOn }
     *     
     */
    public ReviseInstallmentsBasedOn getReviseInstallmentsBasedOn() {
        return reviseInstallmentsBasedOn;
    }

    /**
     * Sets the value of the reviseInstallmentsBasedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReviseInstallmentsBasedOn }
     *     
     */
    public void setReviseInstallmentsBasedOn(ReviseInstallmentsBasedOn value) {
        this.reviseInstallmentsBasedOn = value;
    }

    /**
     * Gets the value of the splitReceivable property.
     * 
     * @return
     *     possible object is
     *     {@link SplitReceivableItems }
     *     
     */
    public SplitReceivableItems getSplitReceivable() {
        return splitReceivable;
    }

    /**
     * Sets the value of the splitReceivable property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitReceivableItems }
     *     
     */
    public void setSplitReceivable(SplitReceivableItems value) {
        this.splitReceivable = value;
    }

    /**
     * Gets the value of the totalInstallmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalInstallmentAmount() {
        return totalInstallmentAmount;
    }

    /**
     * Sets the value of the totalInstallmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalInstallmentAmount(BigDecimal value) {
        this.totalInstallmentAmount = value;
    }

}
