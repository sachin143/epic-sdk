
package com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancelPolicyRemarksGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CancelPolicyRemarksGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CancellationID"/>
 *     &lt;enumeration value="CancelPolicyRemarkID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CancelPolicyRemarksGetType")
@XmlEnum
public enum CancelPolicyRemarksGetType {

    @XmlEnumValue("CancellationID")
    CANCELLATION_ID("CancellationID"),
    @XmlEnumValue("CancelPolicyRemarkID")
    CANCEL_POLICY_REMARK_ID("CancelPolicyRemarkID");
    private final String value;

    CancelPolicyRemarksGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CancelPolicyRemarksGetType fromValue(String v) {
        for (CancelPolicyRemarksGetType c: CancelPolicyRemarksGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
