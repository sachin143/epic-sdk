
package com.appliedsystems.schemas.epic.sdk._2016._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._configure.ArrayOfSalesTeam;


/**
 * <p>Java class for SalesTeamGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesTeamGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesTeams" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/}ArrayOfSalesTeam" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesTeamGetResult", propOrder = {
    "salesTeams",
    "totalPages"
})
public class SalesTeamGetResult {

    @XmlElement(name = "SalesTeams", nillable = true)
    protected ArrayOfSalesTeam salesTeams;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the salesTeams property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSalesTeam }
     *     
     */
    public ArrayOfSalesTeam getSalesTeams() {
        return salesTeams;
    }

    /**
     * Sets the value of the salesTeams property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSalesTeam }
     *     
     */
    public void setSalesTeams(ArrayOfSalesTeam value) {
        this.salesTeams = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
