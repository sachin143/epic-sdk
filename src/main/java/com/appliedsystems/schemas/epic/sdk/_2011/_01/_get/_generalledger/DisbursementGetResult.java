
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ArrayOfDisbursement;


/**
 * <p>Java class for DisbursementGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DisbursementGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Disbursements" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ArrayOfDisbursement" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisbursementGetResult", propOrder = {
    "disbursements",
    "totalPages"
})
public class DisbursementGetResult {

    @XmlElement(name = "Disbursements", nillable = true)
    protected ArrayOfDisbursement disbursements;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the disbursements property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDisbursement }
     *     
     */
    public ArrayOfDisbursement getDisbursements() {
        return disbursements;
    }

    /**
     * Sets the value of the disbursements property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDisbursement }
     *     
     */
    public void setDisbursements(ArrayOfDisbursement value) {
        this.disbursements = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
