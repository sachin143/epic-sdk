
package com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.PolicyItem;


/**
 * <p>Java class for PolicyItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyItem" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/}PolicyItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyItems", propOrder = {
    "policyItems"
})
public class PolicyItems {

    @XmlElement(name = "PolicyItem", nillable = true)
    protected List<PolicyItem> policyItems;

    /**
     * Gets the value of the policyItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyItem }
     * 
     * 
     */
    public List<PolicyItem> getPolicyItems() {
        if (policyItems == null) {
            policyItems = new ArrayList<PolicyItem>();
        }
        return this.policyItems;
    }

}
