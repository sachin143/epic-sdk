
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.Status;


/**
 * <p>Java class for PolicyFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BrokerCommissionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BrokerCommissionCodeComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_policyfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientServicingRoleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EffectiveDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExpirationDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExpirationDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyNumberComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_policyfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="PolicyTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerCommissionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerCommissionCodeComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_policyfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="ServicingRoleEmployeeLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_policyfilter/}Status" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyFilter", propOrder = {
    "brokerCommissionCode",
    "brokerCommissionCodeComparisonType",
    "clientID",
    "clientServicingRoleCode",
    "departmentCode",
    "effectiveDateBegins",
    "effectiveDateEnds",
    "expirationDateBegins",
    "expirationDateEnds",
    "policyID",
    "policyNumber",
    "policyNumberComparisonType",
    "policyTypeCode",
    "producerCommissionCode",
    "producerCommissionCodeComparisonType",
    "servicingRoleEmployeeLookupCode",
    "status"
})
public class PolicyFilter {

    @XmlElement(name = "BrokerCommissionCode", nillable = true)
    protected String brokerCommissionCode;
    @XmlElement(name = "BrokerCommissionCodeComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType brokerCommissionCodeComparisonType;
    @XmlElement(name = "ClientID", nillable = true)
    protected Integer clientID;
    @XmlElement(name = "ClientServicingRoleCode", nillable = true)
    protected String clientServicingRoleCode;
    @XmlElement(name = "DepartmentCode", nillable = true)
    protected String departmentCode;
    @XmlElement(name = "EffectiveDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDateBegins;
    @XmlElement(name = "EffectiveDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDateEnds;
    @XmlElement(name = "ExpirationDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDateBegins;
    @XmlElement(name = "ExpirationDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDateEnds;
    @XmlElement(name = "PolicyID", nillable = true)
    protected Integer policyID;
    @XmlElement(name = "PolicyNumber", nillable = true)
    protected String policyNumber;
    @XmlElement(name = "PolicyNumberComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType policyNumberComparisonType;
    @XmlElement(name = "PolicyTypeCode", nillable = true)
    protected String policyTypeCode;
    @XmlElement(name = "ProducerCommissionCode", nillable = true)
    protected String producerCommissionCode;
    @XmlElement(name = "ProducerCommissionCodeComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType producerCommissionCodeComparisonType;
    @XmlElement(name = "ServicingRoleEmployeeLookupCode", nillable = true)
    protected String servicingRoleEmployeeLookupCode;
    @XmlElement(name = "Status")
    @XmlSchemaType(name = "string")
    protected Status status;

    /**
     * Gets the value of the brokerCommissionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrokerCommissionCode() {
        return brokerCommissionCode;
    }

    /**
     * Sets the value of the brokerCommissionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrokerCommissionCode(String value) {
        this.brokerCommissionCode = value;
    }

    /**
     * Gets the value of the brokerCommissionCodeComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getBrokerCommissionCodeComparisonType() {
        return brokerCommissionCodeComparisonType;
    }

    /**
     * Sets the value of the brokerCommissionCodeComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setBrokerCommissionCodeComparisonType(ComparisonType value) {
        this.brokerCommissionCodeComparisonType = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the clientServicingRoleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientServicingRoleCode() {
        return clientServicingRoleCode;
    }

    /**
     * Sets the value of the clientServicingRoleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientServicingRoleCode(String value) {
        this.clientServicingRoleCode = value;
    }

    /**
     * Gets the value of the departmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * Sets the value of the departmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentCode(String value) {
        this.departmentCode = value;
    }

    /**
     * Gets the value of the effectiveDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDateBegins() {
        return effectiveDateBegins;
    }

    /**
     * Sets the value of the effectiveDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDateBegins(XMLGregorianCalendar value) {
        this.effectiveDateBegins = value;
    }

    /**
     * Gets the value of the effectiveDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDateEnds() {
        return effectiveDateEnds;
    }

    /**
     * Sets the value of the effectiveDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDateEnds(XMLGregorianCalendar value) {
        this.effectiveDateEnds = value;
    }

    /**
     * Gets the value of the expirationDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDateBegins() {
        return expirationDateBegins;
    }

    /**
     * Sets the value of the expirationDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDateBegins(XMLGregorianCalendar value) {
        this.expirationDateBegins = value;
    }

    /**
     * Gets the value of the expirationDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDateEnds() {
        return expirationDateEnds;
    }

    /**
     * Sets the value of the expirationDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDateEnds(XMLGregorianCalendar value) {
        this.expirationDateEnds = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the policyNumberComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getPolicyNumberComparisonType() {
        return policyNumberComparisonType;
    }

    /**
     * Sets the value of the policyNumberComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setPolicyNumberComparisonType(ComparisonType value) {
        this.policyNumberComparisonType = value;
    }

    /**
     * Gets the value of the policyTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyTypeCode() {
        return policyTypeCode;
    }

    /**
     * Sets the value of the policyTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyTypeCode(String value) {
        this.policyTypeCode = value;
    }

    /**
     * Gets the value of the producerCommissionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerCommissionCode() {
        return producerCommissionCode;
    }

    /**
     * Sets the value of the producerCommissionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerCommissionCode(String value) {
        this.producerCommissionCode = value;
    }

    /**
     * Gets the value of the producerCommissionCodeComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getProducerCommissionCodeComparisonType() {
        return producerCommissionCodeComparisonType;
    }

    /**
     * Sets the value of the producerCommissionCodeComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setProducerCommissionCodeComparisonType(ComparisonType value) {
        this.producerCommissionCodeComparisonType = value;
    }

    /**
     * Gets the value of the servicingRoleEmployeeLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingRoleEmployeeLookupCode() {
        return servicingRoleEmployeeLookupCode;
    }

    /**
     * Sets the value of the servicingRoleEmployeeLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingRoleEmployeeLookupCode(String value) {
        this.servicingRoleEmployeeLookupCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

}
