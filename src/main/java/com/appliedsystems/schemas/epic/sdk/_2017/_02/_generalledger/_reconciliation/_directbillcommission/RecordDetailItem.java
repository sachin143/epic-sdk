
package com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ProducerBrokerCommissionItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.RecordRisks;


/**
 * <p>Java class for RecordDetailItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecordDetailItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DeleteReversesTheTransaction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/}Flags" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MultiCommissionLineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyLineOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCommissionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/}ProducerBrokerCommissionItems" minOccurs="0"/>
 *         &lt;element name="RecordRisksValue" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/_recorddetailitem/}RecordRisks" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IgnoreAgencyCommissionLessThanProducerBroker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CommissionPremiumOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="IgnoreFlatCommissionRate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecordDetailItem", propOrder = {
    "agencyCommissionAmount",
    "agencyCommissionPercentage",
    "agencyCommissionTypeCode",
    "amount",
    "deleteReversesTheTransaction",
    "description",
    "effectiveDate",
    "flag",
    "lineID",
    "multiCommissionLineID",
    "policyID",
    "policyLineOption",
    "producerBrokerCommissionItems",
    "recordRisksValue",
    "serviceSummaryID",
    "transactionCode",
    "transactionID",
    "ignoreAgencyCommissionLessThanProducerBroker",
    "commissionPremiumOption",
    "ignoreFlatCommissionRate"
})
public class RecordDetailItem {

    @XmlElement(name = "AgencyCommissionAmount")
    protected BigDecimal agencyCommissionAmount;
    @XmlElement(name = "AgencyCommissionPercentage")
    protected BigDecimal agencyCommissionPercentage;
    @XmlElement(name = "AgencyCommissionTypeCode", nillable = true)
    protected String agencyCommissionTypeCode;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "DeleteReversesTheTransaction")
    protected Boolean deleteReversesTheTransaction;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "LineID")
    protected Integer lineID;
    @XmlElement(name = "MultiCommissionLineID")
    protected Integer multiCommissionLineID;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "PolicyLineOption", nillable = true)
    protected OptionType policyLineOption;
    @XmlElement(name = "ProducerBrokerCommissionItems", nillable = true)
    protected ProducerBrokerCommissionItems producerBrokerCommissionItems;
    @XmlElement(name = "RecordRisksValue", nillable = true)
    protected RecordRisks recordRisksValue;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "IgnoreAgencyCommissionLessThanProducerBroker")
    protected Boolean ignoreAgencyCommissionLessThanProducerBroker;
    @XmlElement(name = "CommissionPremiumOption", nillable = true)
    protected OptionType commissionPremiumOption;
    @XmlElement(name = "IgnoreFlatCommissionRate")
    protected Boolean ignoreFlatCommissionRate;

    /**
     * Gets the value of the agencyCommissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionAmount() {
        return agencyCommissionAmount;
    }

    /**
     * Sets the value of the agencyCommissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionAmount(BigDecimal value) {
        this.agencyCommissionAmount = value;
    }

    /**
     * Gets the value of the agencyCommissionPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionPercentage() {
        return agencyCommissionPercentage;
    }

    /**
     * Sets the value of the agencyCommissionPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionPercentage(BigDecimal value) {
        this.agencyCommissionPercentage = value;
    }

    /**
     * Gets the value of the agencyCommissionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCommissionTypeCode() {
        return agencyCommissionTypeCode;
    }

    /**
     * Sets the value of the agencyCommissionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCommissionTypeCode(String value) {
        this.agencyCommissionTypeCode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the deleteReversesTheTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteReversesTheTransaction() {
        return deleteReversesTheTransaction;
    }

    /**
     * Sets the value of the deleteReversesTheTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteReversesTheTransaction(Boolean value) {
        this.deleteReversesTheTransaction = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the multiCommissionLineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMultiCommissionLineID() {
        return multiCommissionLineID;
    }

    /**
     * Sets the value of the multiCommissionLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMultiCommissionLineID(Integer value) {
        this.multiCommissionLineID = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the policyLineOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getPolicyLineOption() {
        return policyLineOption;
    }

    /**
     * Sets the value of the policyLineOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setPolicyLineOption(OptionType value) {
        this.policyLineOption = value;
    }

    /**
     * Gets the value of the producerBrokerCommissionItems property.
     * 
     * @return
     *     possible object is
     *     {@link ProducerBrokerCommissionItems }
     *     
     */
    public ProducerBrokerCommissionItems getProducerBrokerCommissionItems() {
        return producerBrokerCommissionItems;
    }

    /**
     * Sets the value of the producerBrokerCommissionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProducerBrokerCommissionItems }
     *     
     */
    public void setProducerBrokerCommissionItems(ProducerBrokerCommissionItems value) {
        this.producerBrokerCommissionItems = value;
    }

    /**
     * Gets the value of the recordRisksValue property.
     * 
     * @return
     *     possible object is
     *     {@link RecordRisks }
     *     
     */
    public RecordRisks getRecordRisksValue() {
        return recordRisksValue;
    }

    /**
     * Sets the value of the recordRisksValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordRisks }
     *     
     */
    public void setRecordRisksValue(RecordRisks value) {
        this.recordRisksValue = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAgencyCommissionLessThanProducerBroker() {
        return ignoreAgencyCommissionLessThanProducerBroker;
    }

    /**
     * Sets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAgencyCommissionLessThanProducerBroker(Boolean value) {
        this.ignoreAgencyCommissionLessThanProducerBroker = value;
    }

    /**
     * Gets the value of the commissionPremiumOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCommissionPremiumOption() {
        return commissionPremiumOption;
    }

    /**
     * Sets the value of the commissionPremiumOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCommissionPremiumOption(OptionType value) {
        this.commissionPremiumOption = value;
    }

    /**
     * Gets the value of the ignoreFlatCommissionRate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreFlatCommissionRate() {
        return ignoreFlatCommissionRate;
    }

    /**
     * Sets the value of the ignoreFlatCommissionRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreFlatCommissionRate(Boolean value) {
        this.ignoreFlatCommissionRate = value;
    }

}
