
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.LineItems;


/**
 * <p>Java class for IssueCancellation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IssueCancellation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IgnoreOpenActivities" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Lines" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_issuecancellation/}LineItems" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IssueCancellation", propOrder = {
    "expirationDate",
    "ignoreOpenActivities",
    "lines",
    "policyID",
    "serviceSummaryID"
})
public class IssueCancellation {

    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "IgnoreOpenActivities")
    protected Boolean ignoreOpenActivities;
    @XmlElement(name = "Lines", nillable = true)
    protected LineItems lines;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the ignoreOpenActivities property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreOpenActivities() {
        return ignoreOpenActivities;
    }

    /**
     * Sets the value of the ignoreOpenActivities property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreOpenActivities(Boolean value) {
        this.ignoreOpenActivities = value;
    }

    /**
     * Gets the value of the lines property.
     * 
     * @return
     *     possible object is
     *     {@link LineItems }
     *     
     */
    public LineItems getLines() {
        return lines;
    }

    /**
     * Sets the value of the lines property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItems }
     *     
     */
    public void setLines(LineItems value) {
        this.lines = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

}
