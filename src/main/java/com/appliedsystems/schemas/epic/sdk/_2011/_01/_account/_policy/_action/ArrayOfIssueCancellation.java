
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIssueCancellation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIssueCancellation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueCancellation" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}IssueCancellation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIssueCancellation", propOrder = {
    "issueCancellations"
})
public class ArrayOfIssueCancellation {

    @XmlElement(name = "IssueCancellation", nillable = true)
    protected List<IssueCancellation> issueCancellations;

    /**
     * Gets the value of the issueCancellations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the issueCancellations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIssueCancellations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IssueCancellation }
     * 
     * 
     */
    public List<IssueCancellation> getIssueCancellations() {
        if (issueCancellations == null) {
            issueCancellations = new ArrayList<IssueCancellation>();
        }
        return this.issueCancellations;
    }

}
