
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaidItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "PaidItems");
    private final static QName _PaidStatementItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "PaidStatementItem");
    private final static QName _Flags_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "Flags");
    private final static QName _PaidItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "PaidItem");
    private final static QName _PaidStatementItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "PaidStatementItems");
    private final static QName _ApplyCreditsToDebits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "ApplyCreditsToDebits");
    private final static QName _PayablesCommissions_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", "PayablesCommissions");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaidStatementItem }
     * 
     */
    public PaidStatementItem createPaidStatementItem() {
        return new PaidStatementItem();
    }

    /**
     * Create an instance of {@link PaidItem }
     * 
     */
    public PaidItem createPaidItem() {
        return new PaidItem();
    }

    /**
     * Create an instance of {@link PaidItems }
     * 
     */
    public PaidItems createPaidItems() {
        return new PaidItems();
    }

    /**
     * Create an instance of {@link ApplyCreditsToDebits }
     * 
     */
    public ApplyCreditsToDebits createApplyCreditsToDebits() {
        return new ApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link PayablesCommissions }
     * 
     */
    public PayablesCommissions createPayablesCommissions() {
        return new PayablesCommissions();
    }

    /**
     * Create an instance of {@link PaidStatementItems }
     * 
     */
    public PaidStatementItems createPaidStatementItems() {
        return new PaidStatementItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "PaidItems")
    public JAXBElement<PaidItems> createPaidItems(PaidItems value) {
        return new JAXBElement<PaidItems>(_PaidItems_QNAME, PaidItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidStatementItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "PaidStatementItem")
    public JAXBElement<PaidStatementItem> createPaidStatementItem(PaidStatementItem value) {
        return new JAXBElement<PaidStatementItem>(_PaidStatementItem_QNAME, PaidStatementItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Flags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "Flags")
    public JAXBElement<Flags> createFlags(Flags value) {
        return new JAXBElement<Flags>(_Flags_QNAME, Flags.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "PaidItem")
    public JAXBElement<PaidItem> createPaidItem(PaidItem value) {
        return new JAXBElement<PaidItem>(_PaidItem_QNAME, PaidItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidStatementItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "PaidStatementItems")
    public JAXBElement<PaidStatementItems> createPaidStatementItems(PaidStatementItems value) {
        return new JAXBElement<PaidStatementItems>(_PaidStatementItems_QNAME, PaidStatementItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplyCreditsToDebits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "ApplyCreditsToDebits")
    public JAXBElement<ApplyCreditsToDebits> createApplyCreditsToDebits(ApplyCreditsToDebits value) {
        return new JAXBElement<ApplyCreditsToDebits>(_ApplyCreditsToDebits_QNAME, ApplyCreditsToDebits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayablesCommissions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/", name = "PayablesCommissions")
    public JAXBElement<PayablesCommissions> createPayablesCommissions(PayablesCommissions value) {
        return new JAXBElement<PayablesCommissions>(_PayablesCommissions_QNAME, PayablesCommissions.class, null, value);
    }

}
