
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JournalEntryVoid complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JournalEntryVoid">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetailAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JournalEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalEntryVoid", propOrder = {
    "detailAccountingMonth",
    "detailDescription",
    "journalEntryID",
    "reason",
    "reasonDetails"
})
public class JournalEntryVoid {

    @XmlElement(name = "DetailAccountingMonth", nillable = true)
    protected String detailAccountingMonth;
    @XmlElement(name = "DetailDescription", nillable = true)
    protected String detailDescription;
    @XmlElement(name = "JournalEntryID")
    protected Integer journalEntryID;
    @XmlElement(name = "Reason", nillable = true)
    protected String reason;
    @XmlElement(name = "ReasonDetails", nillable = true)
    protected String reasonDetails;

    /**
     * Gets the value of the detailAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailAccountingMonth() {
        return detailAccountingMonth;
    }

    /**
     * Sets the value of the detailAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailAccountingMonth(String value) {
        this.detailAccountingMonth = value;
    }

    /**
     * Gets the value of the detailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescription() {
        return detailDescription;
    }

    /**
     * Sets the value of the detailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescription(String value) {
        this.detailDescription = value;
    }

    /**
     * Gets the value of the journalEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getJournalEntryID() {
        return journalEntryID;
    }

    /**
     * Sets the value of the journalEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setJournalEntryID(Integer value) {
        this.journalEntryID = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the reasonDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonDetails() {
        return reasonDetails;
    }

    /**
     * Sets the value of the reasonDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonDetails(String value) {
        this.reasonDetails = value;
    }

}
