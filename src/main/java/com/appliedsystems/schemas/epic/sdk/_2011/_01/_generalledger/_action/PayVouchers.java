
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.EligibleVouchers;


/**
 * <p>Java class for PayVouchers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PayVouchers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EligibleVouchersValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/}EligibleVouchers" minOccurs="0"/>
 *         &lt;element name="LeaveNetCreditVouchersUnpaid" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayVouchers", propOrder = {
    "accountingMonth",
    "agencyCode",
    "balance",
    "bankAccountNumberCode",
    "bankSubAccountNumberCode",
    "eligibleVouchersValue",
    "leaveNetCreditVouchersUnpaid"
})
public class PayVouchers {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "Balance")
    protected BigDecimal balance;
    @XmlElement(name = "BankAccountNumberCode", nillable = true)
    protected String bankAccountNumberCode;
    @XmlElement(name = "BankSubAccountNumberCode", nillable = true)
    protected String bankSubAccountNumberCode;
    @XmlElement(name = "EligibleVouchersValue", nillable = true)
    protected EligibleVouchers eligibleVouchersValue;
    @XmlElement(name = "LeaveNetCreditVouchersUnpaid")
    protected Boolean leaveNetCreditVouchersUnpaid;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the bankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNumberCode() {
        return bankAccountNumberCode;
    }

    /**
     * Sets the value of the bankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNumberCode(String value) {
        this.bankAccountNumberCode = value;
    }

    /**
     * Gets the value of the bankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankSubAccountNumberCode() {
        return bankSubAccountNumberCode;
    }

    /**
     * Sets the value of the bankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankSubAccountNumberCode(String value) {
        this.bankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the eligibleVouchersValue property.
     * 
     * @return
     *     possible object is
     *     {@link EligibleVouchers }
     *     
     */
    public EligibleVouchers getEligibleVouchersValue() {
        return eligibleVouchersValue;
    }

    /**
     * Sets the value of the eligibleVouchersValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link EligibleVouchers }
     *     
     */
    public void setEligibleVouchersValue(EligibleVouchers value) {
        this.eligibleVouchersValue = value;
    }

    /**
     * Gets the value of the leaveNetCreditVouchersUnpaid property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLeaveNetCreditVouchersUnpaid() {
        return leaveNetCreditVouchersUnpaid;
    }

    /**
     * Sets the value of the leaveNetCreditVouchersUnpaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLeaveNetCreditVouchersUnpaid(Boolean value) {
        this.leaveNetCreditVouchersUnpaid = value;
    }

}
