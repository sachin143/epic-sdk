
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherComparisonType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherComparisonType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="EqualTo"/>
 *     &lt;enumeration value="Containing"/>
 *     &lt;enumeration value="WithinRange"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VoucherComparisonType")
@XmlEnum
public enum VoucherComparisonType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("EqualTo")
    EQUAL_TO("EqualTo"),
    @XmlEnumValue("Containing")
    CONTAINING("Containing"),
    @XmlEnumValue("WithinRange")
    WITHIN_RANGE("WithinRange");
    private final String value;

    VoucherComparisonType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoucherComparisonType fromValue(String v) {
        for (VoucherComparisonType c: VoucherComparisonType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
