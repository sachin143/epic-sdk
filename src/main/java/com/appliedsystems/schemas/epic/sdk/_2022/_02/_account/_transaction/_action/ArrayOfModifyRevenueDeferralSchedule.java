
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfModifyRevenueDeferralSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfModifyRevenueDeferralSchedule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModifyRevenueDeferralSchedule" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ModifyRevenueDeferralSchedule" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfModifyRevenueDeferralSchedule", propOrder = {
    "modifyRevenueDeferralSchedules"
})
public class ArrayOfModifyRevenueDeferralSchedule {

    @XmlElement(name = "ModifyRevenueDeferralSchedule", nillable = true)
    protected List<ModifyRevenueDeferralSchedule> modifyRevenueDeferralSchedules;

    /**
     * Gets the value of the modifyRevenueDeferralSchedules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifyRevenueDeferralSchedules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifyRevenueDeferralSchedules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModifyRevenueDeferralSchedule }
     * 
     * 
     */
    public List<ModifyRevenueDeferralSchedule> getModifyRevenueDeferralSchedules() {
        if (modifyRevenueDeferralSchedules == null) {
            modifyRevenueDeferralSchedules = new ArrayList<ModifyRevenueDeferralSchedule>();
        }
        return this.modifyRevenueDeferralSchedules;
    }

}
