
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AllocationEntriesGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "AllocationEntriesGetType");
    private final static QName _ArrayOfAllocationMethod_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "ArrayOfAllocationMethod");
    private final static QName _AllocationStructureGrouping_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "AllocationStructureGrouping");
    private final static QName _ArrayOfAllocationStructureGrouping_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "ArrayOfAllocationStructureGrouping");
    private final static QName _StructureCombinationItemFlags_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "StructureCombinationItemFlags");
    private final static QName _StructureCombination_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "StructureCombination");
    private final static QName _AllocationStructureGroupingsGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "AllocationStructureGroupingsGetType");
    private final static QName _StructureCombinations_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "StructureCombinations");
    private final static QName _AllocationMethod_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", "AllocationMethod");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AllocationStructureGrouping }
     * 
     */
    public AllocationStructureGrouping createAllocationStructureGrouping() {
        return new AllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link AllocationMethod }
     * 
     */
    public AllocationMethod createAllocationMethod() {
        return new AllocationMethod();
    }

    /**
     * Create an instance of {@link ArrayOfAllocationStructureGrouping }
     * 
     */
    public ArrayOfAllocationStructureGrouping createArrayOfAllocationStructureGrouping() {
        return new ArrayOfAllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link StructureCombinations }
     * 
     */
    public StructureCombinations createStructureCombinations() {
        return new StructureCombinations();
    }

    /**
     * Create an instance of {@link ArrayOfAllocationMethod }
     * 
     */
    public ArrayOfAllocationMethod createArrayOfAllocationMethod() {
        return new ArrayOfAllocationMethod();
    }

    /**
     * Create an instance of {@link StructureCombination }
     * 
     */
    public StructureCombination createStructureCombination() {
        return new StructureCombination();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationEntriesGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "AllocationEntriesGetType")
    public JAXBElement<AllocationEntriesGetType> createAllocationEntriesGetType(AllocationEntriesGetType value) {
        return new JAXBElement<AllocationEntriesGetType>(_AllocationEntriesGetType_QNAME, AllocationEntriesGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAllocationMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "ArrayOfAllocationMethod")
    public JAXBElement<ArrayOfAllocationMethod> createArrayOfAllocationMethod(ArrayOfAllocationMethod value) {
        return new JAXBElement<ArrayOfAllocationMethod>(_ArrayOfAllocationMethod_QNAME, ArrayOfAllocationMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationStructureGrouping }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "AllocationStructureGrouping")
    public JAXBElement<AllocationStructureGrouping> createAllocationStructureGrouping(AllocationStructureGrouping value) {
        return new JAXBElement<AllocationStructureGrouping>(_AllocationStructureGrouping_QNAME, AllocationStructureGrouping.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAllocationStructureGrouping }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "ArrayOfAllocationStructureGrouping")
    public JAXBElement<ArrayOfAllocationStructureGrouping> createArrayOfAllocationStructureGrouping(ArrayOfAllocationStructureGrouping value) {
        return new JAXBElement<ArrayOfAllocationStructureGrouping>(_ArrayOfAllocationStructureGrouping_QNAME, ArrayOfAllocationStructureGrouping.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureCombinationItemFlags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "StructureCombinationItemFlags")
    public JAXBElement<StructureCombinationItemFlags> createStructureCombinationItemFlags(StructureCombinationItemFlags value) {
        return new JAXBElement<StructureCombinationItemFlags>(_StructureCombinationItemFlags_QNAME, StructureCombinationItemFlags.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureCombination }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "StructureCombination")
    public JAXBElement<StructureCombination> createStructureCombination(StructureCombination value) {
        return new JAXBElement<StructureCombination>(_StructureCombination_QNAME, StructureCombination.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationStructureGroupingsGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "AllocationStructureGroupingsGetType")
    public JAXBElement<AllocationStructureGroupingsGetType> createAllocationStructureGroupingsGetType(AllocationStructureGroupingsGetType value) {
        return new JAXBElement<AllocationStructureGroupingsGetType>(_AllocationStructureGroupingsGetType_QNAME, AllocationStructureGroupingsGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StructureCombinations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "StructureCombinations")
    public JAXBElement<StructureCombinations> createStructureCombinations(StructureCombinations value) {
        return new JAXBElement<StructureCombinations>(_StructureCombinations_QNAME, StructureCombinations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/", name = "AllocationMethod")
    public JAXBElement<AllocationMethod> createAllocationMethod(AllocationMethod value) {
        return new JAXBElement<AllocationMethod>(_AllocationMethod_QNAME, AllocationMethod.class, null, value);
    }

}
