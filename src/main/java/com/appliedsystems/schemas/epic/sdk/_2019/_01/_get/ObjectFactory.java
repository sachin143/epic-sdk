
package com.appliedsystems.schemas.epic.sdk._2019._01._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ChartOfAccountGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/", "ChartOfAccountGetResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChartOfAccountGetResult }
     * 
     */
    public ChartOfAccountGetResult createChartOfAccountGetResult() {
        return new ChartOfAccountGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChartOfAccountGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/", name = "ChartOfAccountGetResult")
    public JAXBElement<ChartOfAccountGetResult> createChartOfAccountGetResult(ChartOfAccountGetResult value) {
        return new JAXBElement<ChartOfAccountGetResult>(_ChartOfAccountGetResult_QNAME, ChartOfAccountGetResult.class, null, value);
    }

}
