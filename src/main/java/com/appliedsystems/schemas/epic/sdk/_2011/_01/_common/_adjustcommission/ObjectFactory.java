
package com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CommissionSplitItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/", "CommissionSplitItem");
    private final static QName _CommissionSplitItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/", "CommissionSplitItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CommissionSplitItems }
     * 
     */
    public CommissionSplitItems createCommissionSplitItems() {
        return new CommissionSplitItems();
    }

    /**
     * Create an instance of {@link CommissionSplitItem }
     * 
     */
    public CommissionSplitItem createCommissionSplitItem() {
        return new CommissionSplitItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommissionSplitItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/", name = "CommissionSplitItem")
    public JAXBElement<CommissionSplitItem> createCommissionSplitItem(CommissionSplitItem value) {
        return new JAXBElement<CommissionSplitItem>(_CommissionSplitItem_QNAME, CommissionSplitItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommissionSplitItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/", name = "CommissionSplitItems")
    public JAXBElement<CommissionSplitItems> createCommissionSplitItems(CommissionSplitItems value) {
        return new JAXBElement<CommissionSplitItems>(_CommissionSplitItems_QNAME, CommissionSplitItems.class, null, value);
    }

}
