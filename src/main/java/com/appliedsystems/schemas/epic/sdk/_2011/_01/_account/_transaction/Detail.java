
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.DetailItems;


/**
 * <p>Java class for Detail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Detail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DetailItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_detail/}DetailItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Detail", propOrder = {
    "balance",
    "detailItemsValue"
})
public class Detail {

    @XmlElement(name = "Balance")
    protected BigDecimal balance;
    @XmlElement(name = "DetailItemsValue", nillable = true)
    protected DetailItems detailItemsValue;

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the detailItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link DetailItems }
     *     
     */
    public DetailItems getDetailItemsValue() {
        return detailItemsValue;
    }

    /**
     * Sets the value of the detailItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailItems }
     *     
     */
    public void setDetailItemsValue(DetailItems value) {
        this.detailItemsValue = value;
    }

}
