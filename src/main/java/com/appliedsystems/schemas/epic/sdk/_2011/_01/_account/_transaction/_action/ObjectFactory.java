
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VoidPayment_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "VoidPayment");
    private final static QName _MoveTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "MoveTransaction");
    private final static QName _ApplyCreditsToDebits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ApplyCreditsToDebits");
    private final static QName _ArrayOfGenerateTaxFee_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfGenerateTaxFee");
    private final static QName _ArrayOfMoveTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfMoveTransaction");
    private final static QName _ArrayOfReverseTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfReverseTransaction");
    private final static QName _ArrayOfRevisePremium_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfRevisePremium");
    private final static QName _ArrayOfUnapplyCreditsToDebits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfUnapplyCreditsToDebits");
    private final static QName _ReverseTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ReverseTransaction");
    private final static QName _GenerateTaxFee_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "GenerateTaxFee");
    private final static QName _FinanceTransaction_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "FinanceTransaction");
    private final static QName _AccountsReceivableWriteOff_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "AccountsReceivableWriteOff");
    private final static QName _RevisePremium_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "RevisePremium");
    private final static QName _UnapplyCreditsToDebits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "UnapplyCreditsToDebits");
    private final static QName _ArrayOfApplyCreditsToDebits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", "ArrayOfApplyCreditsToDebits");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MoveTransaction }
     * 
     */
    public MoveTransaction createMoveTransaction() {
        return new MoveTransaction();
    }

    /**
     * Create an instance of {@link ApplyCreditsToDebits }
     * 
     */
    public ApplyCreditsToDebits createApplyCreditsToDebits() {
        return new ApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link FinanceTransaction }
     * 
     */
    public FinanceTransaction createFinanceTransaction() {
        return new FinanceTransaction();
    }

    /**
     * Create an instance of {@link VoidPayment }
     * 
     */
    public VoidPayment createVoidPayment() {
        return new VoidPayment();
    }

    /**
     * Create an instance of {@link GenerateTaxFee }
     * 
     */
    public GenerateTaxFee createGenerateTaxFee() {
        return new GenerateTaxFee();
    }

    /**
     * Create an instance of {@link ArrayOfMoveTransaction }
     * 
     */
    public ArrayOfMoveTransaction createArrayOfMoveTransaction() {
        return new ArrayOfMoveTransaction();
    }

    /**
     * Create an instance of {@link UnapplyCreditsToDebits }
     * 
     */
    public UnapplyCreditsToDebits createUnapplyCreditsToDebits() {
        return new UnapplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link RevisePremium }
     * 
     */
    public RevisePremium createRevisePremium() {
        return new RevisePremium();
    }

    /**
     * Create an instance of {@link ReverseTransaction }
     * 
     */
    public ReverseTransaction createReverseTransaction() {
        return new ReverseTransaction();
    }

    /**
     * Create an instance of {@link ArrayOfUnapplyCreditsToDebits }
     * 
     */
    public ArrayOfUnapplyCreditsToDebits createArrayOfUnapplyCreditsToDebits() {
        return new ArrayOfUnapplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link ArrayOfReverseTransaction }
     * 
     */
    public ArrayOfReverseTransaction createArrayOfReverseTransaction() {
        return new ArrayOfReverseTransaction();
    }

    /**
     * Create an instance of {@link ArrayOfGenerateTaxFee }
     * 
     */
    public ArrayOfGenerateTaxFee createArrayOfGenerateTaxFee() {
        return new ArrayOfGenerateTaxFee();
    }

    /**
     * Create an instance of {@link ArrayOfRevisePremium }
     * 
     */
    public ArrayOfRevisePremium createArrayOfRevisePremium() {
        return new ArrayOfRevisePremium();
    }

    /**
     * Create an instance of {@link ArrayOfApplyCreditsToDebits }
     * 
     */
    public ArrayOfApplyCreditsToDebits createArrayOfApplyCreditsToDebits() {
        return new ArrayOfApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link AccountsReceivableWriteOff }
     * 
     */
    public AccountsReceivableWriteOff createAccountsReceivableWriteOff() {
        return new AccountsReceivableWriteOff();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoidPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "VoidPayment")
    public JAXBElement<VoidPayment> createVoidPayment(VoidPayment value) {
        return new JAXBElement<VoidPayment>(_VoidPayment_QNAME, VoidPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "MoveTransaction")
    public JAXBElement<MoveTransaction> createMoveTransaction(MoveTransaction value) {
        return new JAXBElement<MoveTransaction>(_MoveTransaction_QNAME, MoveTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplyCreditsToDebits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ApplyCreditsToDebits")
    public JAXBElement<ApplyCreditsToDebits> createApplyCreditsToDebits(ApplyCreditsToDebits value) {
        return new JAXBElement<ApplyCreditsToDebits>(_ApplyCreditsToDebits_QNAME, ApplyCreditsToDebits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGenerateTaxFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfGenerateTaxFee")
    public JAXBElement<ArrayOfGenerateTaxFee> createArrayOfGenerateTaxFee(ArrayOfGenerateTaxFee value) {
        return new JAXBElement<ArrayOfGenerateTaxFee>(_ArrayOfGenerateTaxFee_QNAME, ArrayOfGenerateTaxFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMoveTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfMoveTransaction")
    public JAXBElement<ArrayOfMoveTransaction> createArrayOfMoveTransaction(ArrayOfMoveTransaction value) {
        return new JAXBElement<ArrayOfMoveTransaction>(_ArrayOfMoveTransaction_QNAME, ArrayOfMoveTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReverseTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfReverseTransaction")
    public JAXBElement<ArrayOfReverseTransaction> createArrayOfReverseTransaction(ArrayOfReverseTransaction value) {
        return new JAXBElement<ArrayOfReverseTransaction>(_ArrayOfReverseTransaction_QNAME, ArrayOfReverseTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRevisePremium }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfRevisePremium")
    public JAXBElement<ArrayOfRevisePremium> createArrayOfRevisePremium(ArrayOfRevisePremium value) {
        return new JAXBElement<ArrayOfRevisePremium>(_ArrayOfRevisePremium_QNAME, ArrayOfRevisePremium.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfUnapplyCreditsToDebits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfUnapplyCreditsToDebits")
    public JAXBElement<ArrayOfUnapplyCreditsToDebits> createArrayOfUnapplyCreditsToDebits(ArrayOfUnapplyCreditsToDebits value) {
        return new JAXBElement<ArrayOfUnapplyCreditsToDebits>(_ArrayOfUnapplyCreditsToDebits_QNAME, ArrayOfUnapplyCreditsToDebits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReverseTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ReverseTransaction")
    public JAXBElement<ReverseTransaction> createReverseTransaction(ReverseTransaction value) {
        return new JAXBElement<ReverseTransaction>(_ReverseTransaction_QNAME, ReverseTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateTaxFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "GenerateTaxFee")
    public JAXBElement<GenerateTaxFee> createGenerateTaxFee(GenerateTaxFee value) {
        return new JAXBElement<GenerateTaxFee>(_GenerateTaxFee_QNAME, GenerateTaxFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinanceTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "FinanceTransaction")
    public JAXBElement<FinanceTransaction> createFinanceTransaction(FinanceTransaction value) {
        return new JAXBElement<FinanceTransaction>(_FinanceTransaction_QNAME, FinanceTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountsReceivableWriteOff }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "AccountsReceivableWriteOff")
    public JAXBElement<AccountsReceivableWriteOff> createAccountsReceivableWriteOff(AccountsReceivableWriteOff value) {
        return new JAXBElement<AccountsReceivableWriteOff>(_AccountsReceivableWriteOff_QNAME, AccountsReceivableWriteOff.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevisePremium }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "RevisePremium")
    public JAXBElement<RevisePremium> createRevisePremium(RevisePremium value) {
        return new JAXBElement<RevisePremium>(_RevisePremium_QNAME, RevisePremium.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnapplyCreditsToDebits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "UnapplyCreditsToDebits")
    public JAXBElement<UnapplyCreditsToDebits> createUnapplyCreditsToDebits(UnapplyCreditsToDebits value) {
        return new JAXBElement<UnapplyCreditsToDebits>(_UnapplyCreditsToDebits_QNAME, UnapplyCreditsToDebits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfApplyCreditsToDebits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/", name = "ArrayOfApplyCreditsToDebits")
    public JAXBElement<ArrayOfApplyCreditsToDebits> createArrayOfApplyCreditsToDebits(ArrayOfApplyCreditsToDebits value) {
        return new JAXBElement<ArrayOfApplyCreditsToDebits>(_ArrayOfApplyCreditsToDebits_QNAME, ArrayOfApplyCreditsToDebits.class, null, value);
    }

}
