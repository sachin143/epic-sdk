
package com.appliedsystems.schemas.epic.sdk._2011._01._account;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttachedWithinType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttachedWithinType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Last30Days"/>
 *     &lt;enumeration value="Last60Days"/>
 *     &lt;enumeration value="Last90Days"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AttachedWithinType")
@XmlEnum
public enum AttachedWithinType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Last30Days")
    LAST_30_DAYS("Last30Days"),
    @XmlEnumValue("Last60Days")
    LAST_60_DAYS("Last60Days"),
    @XmlEnumValue("Last90Days")
    LAST_90_DAYS("Last90Days");
    private final String value;

    AttachedWithinType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AttachedWithinType fromValue(String v) {
        for (AttachedWithinType c: AttachedWithinType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
