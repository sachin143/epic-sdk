
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.LineItems;


/**
 * <p>Java class for Cancel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cancel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancelOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="CancellationEffectiveDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CancellationID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CancellationReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CancellationReasonCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CancellationReasonEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CancellationReasonIfOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CancellationReasonPolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LinesOfBusiness" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_cancel/}LineItems" minOccurs="0"/>
 *         &lt;element name="MethodOfCancellationFullTermPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MethodOfCancellationMethodCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodOfCancellationPremiumCancellationSubjectToAudit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MethodOfCancellationReturnPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MethodOfCancellationUnearnedFactor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PolicyEndTermDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyStartTermDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PolicyTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cancel", propOrder = {
    "cancelOption",
    "cancellationEffectiveDateTime",
    "cancellationID",
    "cancellationReasonCode",
    "cancellationReasonCompany",
    "cancellationReasonEffectiveDate",
    "cancellationReasonIfOther",
    "cancellationReasonPolicyNumber",
    "description",
    "isReadOnly",
    "linesOfBusiness",
    "methodOfCancellationFullTermPremium",
    "methodOfCancellationMethodCode",
    "methodOfCancellationPremiumCancellationSubjectToAudit",
    "methodOfCancellationReturnPremium",
    "methodOfCancellationUnearnedFactor",
    "policyEndTermDate",
    "policyID",
    "policyNumber",
    "policyStartTermDate",
    "policyTypeCode",
    "serviceSummaryID",
    "timestamp"
})
public class Cancel {

    @XmlElement(name = "CancelOption", nillable = true)
    protected OptionType cancelOption;
    @XmlElement(name = "CancellationEffectiveDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar cancellationEffectiveDateTime;
    @XmlElement(name = "CancellationID")
    protected Integer cancellationID;
    @XmlElement(name = "CancellationReasonCode", nillable = true)
    protected String cancellationReasonCode;
    @XmlElement(name = "CancellationReasonCompany", nillable = true)
    protected String cancellationReasonCompany;
    @XmlElement(name = "CancellationReasonEffectiveDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar cancellationReasonEffectiveDate;
    @XmlElement(name = "CancellationReasonIfOther", nillable = true)
    protected String cancellationReasonIfOther;
    @XmlElement(name = "CancellationReasonPolicyNumber", nillable = true)
    protected String cancellationReasonPolicyNumber;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "LinesOfBusiness", nillable = true)
    protected LineItems linesOfBusiness;
    @XmlElement(name = "MethodOfCancellationFullTermPremium", nillable = true)
    protected BigDecimal methodOfCancellationFullTermPremium;
    @XmlElement(name = "MethodOfCancellationMethodCode", nillable = true)
    protected String methodOfCancellationMethodCode;
    @XmlElement(name = "MethodOfCancellationPremiumCancellationSubjectToAudit")
    protected Boolean methodOfCancellationPremiumCancellationSubjectToAudit;
    @XmlElement(name = "MethodOfCancellationReturnPremium", nillable = true)
    protected BigDecimal methodOfCancellationReturnPremium;
    @XmlElement(name = "MethodOfCancellationUnearnedFactor", nillable = true)
    protected BigDecimal methodOfCancellationUnearnedFactor;
    @XmlElement(name = "PolicyEndTermDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar policyEndTermDate;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "PolicyNumber", nillable = true)
    protected String policyNumber;
    @XmlElement(name = "PolicyStartTermDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar policyStartTermDate;
    @XmlElement(name = "PolicyTypeCode", nillable = true)
    protected String policyTypeCode;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the cancelOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCancelOption() {
        return cancelOption;
    }

    /**
     * Sets the value of the cancelOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCancelOption(OptionType value) {
        this.cancelOption = value;
    }

    /**
     * Gets the value of the cancellationEffectiveDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCancellationEffectiveDateTime() {
        return cancellationEffectiveDateTime;
    }

    /**
     * Sets the value of the cancellationEffectiveDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCancellationEffectiveDateTime(XMLGregorianCalendar value) {
        this.cancellationEffectiveDateTime = value;
    }

    /**
     * Gets the value of the cancellationID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCancellationID() {
        return cancellationID;
    }

    /**
     * Sets the value of the cancellationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCancellationID(Integer value) {
        this.cancellationID = value;
    }

    /**
     * Gets the value of the cancellationReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationReasonCode() {
        return cancellationReasonCode;
    }

    /**
     * Sets the value of the cancellationReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationReasonCode(String value) {
        this.cancellationReasonCode = value;
    }

    /**
     * Gets the value of the cancellationReasonCompany property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationReasonCompany() {
        return cancellationReasonCompany;
    }

    /**
     * Sets the value of the cancellationReasonCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationReasonCompany(String value) {
        this.cancellationReasonCompany = value;
    }

    /**
     * Gets the value of the cancellationReasonEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCancellationReasonEffectiveDate() {
        return cancellationReasonEffectiveDate;
    }

    /**
     * Sets the value of the cancellationReasonEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCancellationReasonEffectiveDate(XMLGregorianCalendar value) {
        this.cancellationReasonEffectiveDate = value;
    }

    /**
     * Gets the value of the cancellationReasonIfOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationReasonIfOther() {
        return cancellationReasonIfOther;
    }

    /**
     * Sets the value of the cancellationReasonIfOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationReasonIfOther(String value) {
        this.cancellationReasonIfOther = value;
    }

    /**
     * Gets the value of the cancellationReasonPolicyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationReasonPolicyNumber() {
        return cancellationReasonPolicyNumber;
    }

    /**
     * Sets the value of the cancellationReasonPolicyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationReasonPolicyNumber(String value) {
        this.cancellationReasonPolicyNumber = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the linesOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link LineItems }
     *     
     */
    public LineItems getLinesOfBusiness() {
        return linesOfBusiness;
    }

    /**
     * Sets the value of the linesOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItems }
     *     
     */
    public void setLinesOfBusiness(LineItems value) {
        this.linesOfBusiness = value;
    }

    /**
     * Gets the value of the methodOfCancellationFullTermPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMethodOfCancellationFullTermPremium() {
        return methodOfCancellationFullTermPremium;
    }

    /**
     * Sets the value of the methodOfCancellationFullTermPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMethodOfCancellationFullTermPremium(BigDecimal value) {
        this.methodOfCancellationFullTermPremium = value;
    }

    /**
     * Gets the value of the methodOfCancellationMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodOfCancellationMethodCode() {
        return methodOfCancellationMethodCode;
    }

    /**
     * Sets the value of the methodOfCancellationMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodOfCancellationMethodCode(String value) {
        this.methodOfCancellationMethodCode = value;
    }

    /**
     * Gets the value of the methodOfCancellationPremiumCancellationSubjectToAudit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMethodOfCancellationPremiumCancellationSubjectToAudit() {
        return methodOfCancellationPremiumCancellationSubjectToAudit;
    }

    /**
     * Sets the value of the methodOfCancellationPremiumCancellationSubjectToAudit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMethodOfCancellationPremiumCancellationSubjectToAudit(Boolean value) {
        this.methodOfCancellationPremiumCancellationSubjectToAudit = value;
    }

    /**
     * Gets the value of the methodOfCancellationReturnPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMethodOfCancellationReturnPremium() {
        return methodOfCancellationReturnPremium;
    }

    /**
     * Sets the value of the methodOfCancellationReturnPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMethodOfCancellationReturnPremium(BigDecimal value) {
        this.methodOfCancellationReturnPremium = value;
    }

    /**
     * Gets the value of the methodOfCancellationUnearnedFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMethodOfCancellationUnearnedFactor() {
        return methodOfCancellationUnearnedFactor;
    }

    /**
     * Sets the value of the methodOfCancellationUnearnedFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMethodOfCancellationUnearnedFactor(BigDecimal value) {
        this.methodOfCancellationUnearnedFactor = value;
    }

    /**
     * Gets the value of the policyEndTermDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyEndTermDate() {
        return policyEndTermDate;
    }

    /**
     * Sets the value of the policyEndTermDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyEndTermDate(XMLGregorianCalendar value) {
        this.policyEndTermDate = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the policyStartTermDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyStartTermDate() {
        return policyStartTermDate;
    }

    /**
     * Sets the value of the policyStartTermDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyStartTermDate(XMLGregorianCalendar value) {
        this.policyStartTermDate = value;
    }

    /**
     * Gets the value of the policyTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyTypeCode() {
        return policyTypeCode;
    }

    /**
     * Sets the value of the policyTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyTypeCode(String value) {
        this.policyTypeCode = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
