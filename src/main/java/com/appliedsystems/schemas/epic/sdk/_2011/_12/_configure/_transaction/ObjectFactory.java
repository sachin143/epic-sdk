
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfTransactionCode_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", "ArrayOfTransactionCode");
    private final static QName _TransactionCodeGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", "TransactionCodeGetType");
    private final static QName _TransactionCode_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", "TransactionCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfTransactionCode }
     * 
     */
    public ArrayOfTransactionCode createArrayOfTransactionCode() {
        return new ArrayOfTransactionCode();
    }

    /**
     * Create an instance of {@link TransactionCode }
     * 
     */
    public TransactionCode createTransactionCode() {
        return new TransactionCode();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", name = "ArrayOfTransactionCode")
    public JAXBElement<ArrayOfTransactionCode> createArrayOfTransactionCode(ArrayOfTransactionCode value) {
        return new JAXBElement<ArrayOfTransactionCode>(_ArrayOfTransactionCode_QNAME, ArrayOfTransactionCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionCodeGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", name = "TransactionCodeGetType")
    public JAXBElement<TransactionCodeGetType> createTransactionCodeGetType(TransactionCodeGetType value) {
        return new JAXBElement<TransactionCodeGetType>(_TransactionCodeGetType_QNAME, TransactionCodeGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/", name = "TransactionCode")
    public JAXBElement<TransactionCode> createTransactionCode(TransactionCode value) {
        return new JAXBElement<TransactionCode>(_TransactionCode_QNAME, TransactionCode.class, null, value);
    }

}
