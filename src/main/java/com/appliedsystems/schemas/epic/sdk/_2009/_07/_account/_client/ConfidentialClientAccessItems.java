
package com.appliedsystems.schemas.epic.sdk._2009._07._account._client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConfidentialClientAccessItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfidentialClientAccessItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConfidentialClientAccessItem" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}ConfidentialClientAccessItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfidentialClientAccessItems", propOrder = {
    "confidentialClientAccessItems"
})
public class ConfidentialClientAccessItems {

    @XmlElement(name = "ConfidentialClientAccessItem", nillable = true)
    protected List<ConfidentialClientAccessItem> confidentialClientAccessItems;

    /**
     * Gets the value of the confidentialClientAccessItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the confidentialClientAccessItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConfidentialClientAccessItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConfidentialClientAccessItem }
     * 
     * 
     */
    public List<ConfidentialClientAccessItem> getConfidentialClientAccessItems() {
        if (confidentialClientAccessItems == null) {
            confidentialClientAccessItems = new ArrayList<ConfidentialClientAccessItem>();
        }
        return this.confidentialClientAccessItems;
    }

}
