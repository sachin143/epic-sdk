
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.Flags;


/**
 * <p>Java class for ProducerBrokerScheduleItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProducerBrokerScheduleItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/}CommissionItems" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/_producerbrokerscheduleitem/}Flags" minOccurs="0"/>
 *         &lt;element name="LineBrokerProducerScheduleID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProducerBrokerScheduleItem", propOrder = {
    "commissionItems",
    "description",
    "effectiveDate",
    "expirationDate",
    "flag",
    "lineBrokerProducerScheduleID"
})
public class ProducerBrokerScheduleItem {

    @XmlElement(name = "CommissionItems", nillable = true)
    protected CommissionItems commissionItems;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EffectiveDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "ExpirationDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "LineBrokerProducerScheduleID")
    protected Integer lineBrokerProducerScheduleID;

    /**
     * Gets the value of the commissionItems property.
     * 
     * @return
     *     possible object is
     *     {@link CommissionItems }
     *     
     */
    public CommissionItems getCommissionItems() {
        return commissionItems;
    }

    /**
     * Sets the value of the commissionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionItems }
     *     
     */
    public void setCommissionItems(CommissionItems value) {
        this.commissionItems = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the lineBrokerProducerScheduleID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineBrokerProducerScheduleID() {
        return lineBrokerProducerScheduleID;
    }

    /**
     * Sets the value of the lineBrokerProducerScheduleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineBrokerProducerScheduleID(Integer value) {
        this.lineBrokerProducerScheduleID = value;
    }

}
