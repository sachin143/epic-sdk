
package com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfUpdateStageToSubmitted complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfUpdateStageToSubmitted">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateStageToSubmitted" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/}UpdateStageToSubmitted" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfUpdateStageToSubmitted", propOrder = {
    "updateStageToSubmitteds"
})
public class ArrayOfUpdateStageToSubmitted {

    @XmlElement(name = "UpdateStageToSubmitted", nillable = true)
    protected List<UpdateStageToSubmitted> updateStageToSubmitteds;

    /**
     * Gets the value of the updateStageToSubmitteds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateStageToSubmitteds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateStageToSubmitteds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateStageToSubmitted }
     * 
     * 
     */
    public List<UpdateStageToSubmitted> getUpdateStageToSubmitteds() {
        if (updateStageToSubmitteds == null) {
            updateStageToSubmitteds = new ArrayList<UpdateStageToSubmitted>();
        }
        return this.updateStageToSubmitteds;
    }

}
