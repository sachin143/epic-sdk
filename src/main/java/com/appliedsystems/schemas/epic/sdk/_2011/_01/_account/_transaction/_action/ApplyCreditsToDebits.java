
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.CreditItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.DebitItems;


/**
 * <p>Java class for ApplyCreditsToDebits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplyCreditsToDebits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Credits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/}CreditItems" minOccurs="0"/>
 *         &lt;element name="Debits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/}DebitItems" minOccurs="0"/>
 *         &lt;element name="MonthOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyCreditsToDebits", propOrder = {
    "accountingMonth",
    "agencyCode",
    "associatedAccountID",
    "associatedAccountTypeCode",
    "credits",
    "debits",
    "monthOption"
})
public class ApplyCreditsToDebits {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "Credits", nillable = true)
    protected CreditItems credits;
    @XmlElement(name = "Debits", nillable = true)
    protected DebitItems debits;
    @XmlElement(name = "MonthOption", nillable = true)
    protected OptionType monthOption;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the credits property.
     * 
     * @return
     *     possible object is
     *     {@link CreditItems }
     *     
     */
    public CreditItems getCredits() {
        return credits;
    }

    /**
     * Sets the value of the credits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditItems }
     *     
     */
    public void setCredits(CreditItems value) {
        this.credits = value;
    }

    /**
     * Gets the value of the debits property.
     * 
     * @return
     *     possible object is
     *     {@link DebitItems }
     *     
     */
    public DebitItems getDebits() {
        return debits;
    }

    /**
     * Sets the value of the debits property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitItems }
     *     
     */
    public void setDebits(DebitItems value) {
        this.debits = value;
    }

    /**
     * Gets the value of the monthOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getMonthOption() {
        return monthOption;
    }

    /**
     * Sets the value of the monthOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setMonthOption(OptionType value) {
        this.monthOption = value;
    }

}
