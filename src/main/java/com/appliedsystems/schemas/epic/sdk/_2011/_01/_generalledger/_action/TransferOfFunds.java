
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for TransferOfFunds complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferOfFunds">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FromAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromPrintCheck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FromTypeOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="FromVendorLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToTypeOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="TransferAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransferAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransferDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransferEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TransferGeneralLedgerScheduleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferOfFunds", propOrder = {
    "fromAgencyCode",
    "fromBankAccountNumberCode",
    "fromBankSubAccountNumberCode",
    "fromPrintCheck",
    "fromTypeOption",
    "fromVendorLookupCode",
    "toAgencyCode",
    "toBankAccountNumberCode",
    "toBankSubAccountNumberCode",
    "toTypeOption",
    "transferAccountingMonth",
    "transferAmount",
    "transferDescription",
    "transferEffectiveDate",
    "transferGeneralLedgerScheduleCode"
})
public class TransferOfFunds {

    @XmlElement(name = "FromAgencyCode", nillable = true)
    protected String fromAgencyCode;
    @XmlElement(name = "FromBankAccountNumberCode", nillable = true)
    protected String fromBankAccountNumberCode;
    @XmlElement(name = "FromBankSubAccountNumberCode", nillable = true)
    protected String fromBankSubAccountNumberCode;
    @XmlElement(name = "FromPrintCheck")
    protected Boolean fromPrintCheck;
    @XmlElement(name = "FromTypeOption", nillable = true)
    protected OptionType fromTypeOption;
    @XmlElement(name = "FromVendorLookupCode", nillable = true)
    protected String fromVendorLookupCode;
    @XmlElement(name = "ToAgencyCode", nillable = true)
    protected String toAgencyCode;
    @XmlElement(name = "ToBankAccountNumberCode", nillable = true)
    protected String toBankAccountNumberCode;
    @XmlElement(name = "ToBankSubAccountNumberCode", nillable = true)
    protected String toBankSubAccountNumberCode;
    @XmlElement(name = "ToTypeOption", nillable = true)
    protected OptionType toTypeOption;
    @XmlElement(name = "TransferAccountingMonth", nillable = true)
    protected String transferAccountingMonth;
    @XmlElement(name = "TransferAmount")
    protected BigDecimal transferAmount;
    @XmlElement(name = "TransferDescription", nillable = true)
    protected String transferDescription;
    @XmlElement(name = "TransferEffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transferEffectiveDate;
    @XmlElement(name = "TransferGeneralLedgerScheduleCode", nillable = true)
    protected String transferGeneralLedgerScheduleCode;

    /**
     * Gets the value of the fromAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromAgencyCode() {
        return fromAgencyCode;
    }

    /**
     * Sets the value of the fromAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromAgencyCode(String value) {
        this.fromAgencyCode = value;
    }

    /**
     * Gets the value of the fromBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromBankAccountNumberCode() {
        return fromBankAccountNumberCode;
    }

    /**
     * Sets the value of the fromBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromBankAccountNumberCode(String value) {
        this.fromBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the fromBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromBankSubAccountNumberCode() {
        return fromBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the fromBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromBankSubAccountNumberCode(String value) {
        this.fromBankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the fromPrintCheck property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFromPrintCheck() {
        return fromPrintCheck;
    }

    /**
     * Sets the value of the fromPrintCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFromPrintCheck(Boolean value) {
        this.fromPrintCheck = value;
    }

    /**
     * Gets the value of the fromTypeOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getFromTypeOption() {
        return fromTypeOption;
    }

    /**
     * Sets the value of the fromTypeOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setFromTypeOption(OptionType value) {
        this.fromTypeOption = value;
    }

    /**
     * Gets the value of the fromVendorLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromVendorLookupCode() {
        return fromVendorLookupCode;
    }

    /**
     * Sets the value of the fromVendorLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromVendorLookupCode(String value) {
        this.fromVendorLookupCode = value;
    }

    /**
     * Gets the value of the toAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToAgencyCode() {
        return toAgencyCode;
    }

    /**
     * Sets the value of the toAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToAgencyCode(String value) {
        this.toAgencyCode = value;
    }

    /**
     * Gets the value of the toBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToBankAccountNumberCode() {
        return toBankAccountNumberCode;
    }

    /**
     * Sets the value of the toBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToBankAccountNumberCode(String value) {
        this.toBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the toBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToBankSubAccountNumberCode() {
        return toBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the toBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToBankSubAccountNumberCode(String value) {
        this.toBankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the toTypeOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getToTypeOption() {
        return toTypeOption;
    }

    /**
     * Sets the value of the toTypeOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setToTypeOption(OptionType value) {
        this.toTypeOption = value;
    }

    /**
     * Gets the value of the transferAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferAccountingMonth() {
        return transferAccountingMonth;
    }

    /**
     * Sets the value of the transferAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferAccountingMonth(String value) {
        this.transferAccountingMonth = value;
    }

    /**
     * Gets the value of the transferAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    /**
     * Sets the value of the transferAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransferAmount(BigDecimal value) {
        this.transferAmount = value;
    }

    /**
     * Gets the value of the transferDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferDescription() {
        return transferDescription;
    }

    /**
     * Sets the value of the transferDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferDescription(String value) {
        this.transferDescription = value;
    }

    /**
     * Gets the value of the transferEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransferEffectiveDate() {
        return transferEffectiveDate;
    }

    /**
     * Sets the value of the transferEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransferEffectiveDate(XMLGregorianCalendar value) {
        this.transferEffectiveDate = value;
    }

    /**
     * Gets the value of the transferGeneralLedgerScheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferGeneralLedgerScheduleCode() {
        return transferGeneralLedgerScheduleCode;
    }

    /**
     * Sets the value of the transferGeneralLedgerScheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferGeneralLedgerScheduleCode(String value) {
        this.transferGeneralLedgerScheduleCode = value;
    }

}
