
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.SendInvoiceToItems;


/**
 * <p>Java class for SplitReceivable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitReceivable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendInvoiceTo" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_splitreceivable/}SendInvoiceToItems" minOccurs="0"/>
 *         &lt;element name="TotalBillingSummaryAgencyIncome" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalBillingSummaryCompanyNetPremium" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TotalBillingSummaryProducerBrokerExpense" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitReceivable", propOrder = {
    "sendInvoiceTo",
    "totalBillingSummaryAgencyIncome",
    "totalBillingSummaryCompanyNetPremium",
    "totalBillingSummaryProducerBrokerExpense"
})
public class SplitReceivable {

    @XmlElement(name = "SendInvoiceTo", nillable = true)
    protected SendInvoiceToItems sendInvoiceTo;
    @XmlElement(name = "TotalBillingSummaryAgencyIncome")
    protected BigDecimal totalBillingSummaryAgencyIncome;
    @XmlElement(name = "TotalBillingSummaryCompanyNetPremium")
    protected BigDecimal totalBillingSummaryCompanyNetPremium;
    @XmlElement(name = "TotalBillingSummaryProducerBrokerExpense")
    protected BigDecimal totalBillingSummaryProducerBrokerExpense;

    /**
     * Gets the value of the sendInvoiceTo property.
     * 
     * @return
     *     possible object is
     *     {@link SendInvoiceToItems }
     *     
     */
    public SendInvoiceToItems getSendInvoiceTo() {
        return sendInvoiceTo;
    }

    /**
     * Sets the value of the sendInvoiceTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendInvoiceToItems }
     *     
     */
    public void setSendInvoiceTo(SendInvoiceToItems value) {
        this.sendInvoiceTo = value;
    }

    /**
     * Gets the value of the totalBillingSummaryAgencyIncome property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBillingSummaryAgencyIncome() {
        return totalBillingSummaryAgencyIncome;
    }

    /**
     * Sets the value of the totalBillingSummaryAgencyIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBillingSummaryAgencyIncome(BigDecimal value) {
        this.totalBillingSummaryAgencyIncome = value;
    }

    /**
     * Gets the value of the totalBillingSummaryCompanyNetPremium property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBillingSummaryCompanyNetPremium() {
        return totalBillingSummaryCompanyNetPremium;
    }

    /**
     * Sets the value of the totalBillingSummaryCompanyNetPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBillingSummaryCompanyNetPremium(BigDecimal value) {
        this.totalBillingSummaryCompanyNetPremium = value;
    }

    /**
     * Gets the value of the totalBillingSummaryProducerBrokerExpense property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBillingSummaryProducerBrokerExpense() {
        return totalBillingSummaryProducerBrokerExpense;
    }

    /**
     * Sets the value of the totalBillingSummaryProducerBrokerExpense property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBillingSummaryProducerBrokerExpense(BigDecimal value) {
        this.totalBillingSummaryProducerBrokerExpense = value;
    }

}
