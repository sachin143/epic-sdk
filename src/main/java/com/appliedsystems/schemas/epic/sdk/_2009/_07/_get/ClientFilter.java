
package com.appliedsystems.schemas.epic.sdk._2009._07._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.Status;
import com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.Type;


/**
 * <p>Java class for ClientFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClientFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaimsAdditionalPartiesInvolvement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaimsAdditionalPartiesName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaimsAdditionalPartiesPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_clientfilter/}Status" minOccurs="0"/>
 *         &lt;element name="ClientType" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_clientfilter/}Type" minOccurs="0"/>
 *         &lt;element name="CompanyClaimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateOfLossBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateOfLossEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriorAccountID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelationshipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicingRoleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicingRoleEmployeeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StateProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubmissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ZipPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyDefinedCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineInformationLineID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SanctionSearchReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientFilter", propOrder = {
    "agencyCode",
    "branchCode",
    "city",
    "claimsAdditionalPartiesInvolvement",
    "claimsAdditionalPartiesName",
    "claimsAdditionalPartiesPhoneNumber",
    "clientID",
    "clientName",
    "clientStatus",
    "clientType",
    "companyClaimNumber",
    "dateOfLossBegins",
    "dateOfLossEnds",
    "firstName",
    "invoiceNumber",
    "lastName",
    "loanNumber",
    "lookupCode",
    "phoneNumber",
    "policyNumber",
    "priorAccountID",
    "relationshipCode",
    "relationshipName",
    "servicingRoleCode",
    "servicingRoleEmployeeCode",
    "stateProvinceCode",
    "streetAddress",
    "submissionID",
    "zipPostalCode",
    "agencyDefinedCategory",
    "emailAddress",
    "lineInformationLineID",
    "sanctionSearchReferenceNumber",
    "vehicleRegistrationNumber"
})
public class ClientFilter {

    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "BranchCode", nillable = true)
    protected String branchCode;
    @XmlElement(name = "City", nillable = true)
    protected String city;
    @XmlElement(name = "ClaimsAdditionalPartiesInvolvement", nillable = true)
    protected String claimsAdditionalPartiesInvolvement;
    @XmlElement(name = "ClaimsAdditionalPartiesName", nillable = true)
    protected String claimsAdditionalPartiesName;
    @XmlElement(name = "ClaimsAdditionalPartiesPhoneNumber", nillable = true)
    protected String claimsAdditionalPartiesPhoneNumber;
    @XmlElement(name = "ClientID", nillable = true)
    protected Integer clientID;
    @XmlElement(name = "ClientName", nillable = true)
    protected String clientName;
    @XmlElement(name = "ClientStatus")
    @XmlSchemaType(name = "string")
    protected Status clientStatus;
    @XmlElement(name = "ClientType")
    @XmlSchemaType(name = "string")
    protected Type clientType;
    @XmlElement(name = "CompanyClaimNumber", nillable = true)
    protected String companyClaimNumber;
    @XmlElement(name = "DateOfLossBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfLossBegins;
    @XmlElement(name = "DateOfLossEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfLossEnds;
    @XmlElement(name = "FirstName", nillable = true)
    protected String firstName;
    @XmlElement(name = "InvoiceNumber", nillable = true)
    protected Integer invoiceNumber;
    @XmlElement(name = "LastName", nillable = true)
    protected String lastName;
    @XmlElement(name = "LoanNumber", nillable = true)
    protected String loanNumber;
    @XmlElement(name = "LookupCode", nillable = true)
    protected String lookupCode;
    @XmlElement(name = "PhoneNumber", nillable = true)
    protected String phoneNumber;
    @XmlElement(name = "PolicyNumber", nillable = true)
    protected String policyNumber;
    @XmlElement(name = "PriorAccountID", nillable = true)
    protected String priorAccountID;
    @XmlElement(name = "RelationshipCode", nillable = true)
    protected String relationshipCode;
    @XmlElement(name = "RelationshipName", nillable = true)
    protected String relationshipName;
    @XmlElement(name = "ServicingRoleCode", nillable = true)
    protected String servicingRoleCode;
    @XmlElement(name = "ServicingRoleEmployeeCode", nillable = true)
    protected String servicingRoleEmployeeCode;
    @XmlElement(name = "StateProvinceCode", nillable = true)
    protected String stateProvinceCode;
    @XmlElement(name = "StreetAddress", nillable = true)
    protected String streetAddress;
    @XmlElement(name = "SubmissionID", nillable = true)
    protected Integer submissionID;
    @XmlElement(name = "ZipPostalCode", nillable = true)
    protected String zipPostalCode;
    @XmlElement(name = "AgencyDefinedCategory", nillable = true)
    protected String agencyDefinedCategory;
    @XmlElement(name = "EmailAddress", nillable = true)
    protected String emailAddress;
    @XmlElement(name = "LineInformationLineID", nillable = true)
    protected String lineInformationLineID;
    @XmlElement(name = "SanctionSearchReferenceNumber", nillable = true)
    protected String sanctionSearchReferenceNumber;
    @XmlElement(name = "VehicleRegistrationNumber", nillable = true)
    protected String vehicleRegistrationNumber;

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the claimsAdditionalPartiesInvolvement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimsAdditionalPartiesInvolvement() {
        return claimsAdditionalPartiesInvolvement;
    }

    /**
     * Sets the value of the claimsAdditionalPartiesInvolvement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimsAdditionalPartiesInvolvement(String value) {
        this.claimsAdditionalPartiesInvolvement = value;
    }

    /**
     * Gets the value of the claimsAdditionalPartiesName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimsAdditionalPartiesName() {
        return claimsAdditionalPartiesName;
    }

    /**
     * Sets the value of the claimsAdditionalPartiesName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimsAdditionalPartiesName(String value) {
        this.claimsAdditionalPartiesName = value;
    }

    /**
     * Gets the value of the claimsAdditionalPartiesPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimsAdditionalPartiesPhoneNumber() {
        return claimsAdditionalPartiesPhoneNumber;
    }

    /**
     * Sets the value of the claimsAdditionalPartiesPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimsAdditionalPartiesPhoneNumber(String value) {
        this.claimsAdditionalPartiesPhoneNumber = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the clientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Sets the value of the clientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientName(String value) {
        this.clientName = value;
    }

    /**
     * Gets the value of the clientStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getClientStatus() {
        return clientStatus;
    }

    /**
     * Sets the value of the clientStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setClientStatus(Status value) {
        this.clientStatus = value;
    }

    /**
     * Gets the value of the clientType property.
     * 
     * @return
     *     possible object is
     *     {@link Type }
     *     
     */
    public Type getClientType() {
        return clientType;
    }

    /**
     * Sets the value of the clientType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Type }
     *     
     */
    public void setClientType(Type value) {
        this.clientType = value;
    }

    /**
     * Gets the value of the companyClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyClaimNumber() {
        return companyClaimNumber;
    }

    /**
     * Sets the value of the companyClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyClaimNumber(String value) {
        this.companyClaimNumber = value;
    }

    /**
     * Gets the value of the dateOfLossBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfLossBegins() {
        return dateOfLossBegins;
    }

    /**
     * Sets the value of the dateOfLossBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfLossBegins(XMLGregorianCalendar value) {
        this.dateOfLossBegins = value;
    }

    /**
     * Gets the value of the dateOfLossEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfLossEnds() {
        return dateOfLossEnds;
    }

    /**
     * Sets the value of the dateOfLossEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfLossEnds(XMLGregorianCalendar value) {
        this.dateOfLossEnds = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceNumber(Integer value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the loanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNumber() {
        return loanNumber;
    }

    /**
     * Sets the value of the loanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNumber(String value) {
        this.loanNumber = value;
    }

    /**
     * Gets the value of the lookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookupCode() {
        return lookupCode;
    }

    /**
     * Sets the value of the lookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookupCode(String value) {
        this.lookupCode = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the priorAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorAccountID() {
        return priorAccountID;
    }

    /**
     * Sets the value of the priorAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorAccountID(String value) {
        this.priorAccountID = value;
    }

    /**
     * Gets the value of the relationshipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationshipCode() {
        return relationshipCode;
    }

    /**
     * Sets the value of the relationshipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationshipCode(String value) {
        this.relationshipCode = value;
    }

    /**
     * Gets the value of the relationshipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationshipName() {
        return relationshipName;
    }

    /**
     * Sets the value of the relationshipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationshipName(String value) {
        this.relationshipName = value;
    }

    /**
     * Gets the value of the servicingRoleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingRoleCode() {
        return servicingRoleCode;
    }

    /**
     * Sets the value of the servicingRoleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingRoleCode(String value) {
        this.servicingRoleCode = value;
    }

    /**
     * Gets the value of the servicingRoleEmployeeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingRoleEmployeeCode() {
        return servicingRoleEmployeeCode;
    }

    /**
     * Sets the value of the servicingRoleEmployeeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingRoleEmployeeCode(String value) {
        this.servicingRoleEmployeeCode = value;
    }

    /**
     * Gets the value of the stateProvinceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    /**
     * Sets the value of the stateProvinceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateProvinceCode(String value) {
        this.stateProvinceCode = value;
    }

    /**
     * Gets the value of the streetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     * Sets the value of the streetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetAddress(String value) {
        this.streetAddress = value;
    }

    /**
     * Gets the value of the submissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubmissionID() {
        return submissionID;
    }

    /**
     * Sets the value of the submissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubmissionID(Integer value) {
        this.submissionID = value;
    }

    /**
     * Gets the value of the zipPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZipPostalCode() {
        return zipPostalCode;
    }

    /**
     * Sets the value of the zipPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZipPostalCode(String value) {
        this.zipPostalCode = value;
    }

    /**
     * Gets the value of the agencyDefinedCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyDefinedCategory() {
        return agencyDefinedCategory;
    }

    /**
     * Sets the value of the agencyDefinedCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyDefinedCategory(String value) {
        this.agencyDefinedCategory = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the lineInformationLineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineInformationLineID() {
        return lineInformationLineID;
    }

    /**
     * Sets the value of the lineInformationLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineInformationLineID(String value) {
        this.lineInformationLineID = value;
    }

    /**
     * Gets the value of the sanctionSearchReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSanctionSearchReferenceNumber() {
        return sanctionSearchReferenceNumber;
    }

    /**
     * Sets the value of the sanctionSearchReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSanctionSearchReferenceNumber(String value) {
        this.sanctionSearchReferenceNumber = value;
    }

    /**
     * Gets the value of the vehicleRegistrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    /**
     * Sets the value of the vehicleRegistrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRegistrationNumber(String value) {
        this.vehicleRegistrationNumber = value;
    }

}
