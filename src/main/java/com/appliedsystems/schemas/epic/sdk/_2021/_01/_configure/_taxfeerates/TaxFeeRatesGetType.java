
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxFeeRatesGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxFeeRatesGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Current"/>
 *     &lt;enumeration value="Expired"/>
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="TaxFeeRateCodeID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TaxFeeRatesGetType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerates/")
@XmlEnum
public enum TaxFeeRatesGetType {

    @XmlEnumValue("Current")
    CURRENT("Current"),
    @XmlEnumValue("Expired")
    EXPIRED("Expired"),
    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("TaxFeeRateCodeID")
    TAX_FEE_RATE_CODE_ID("TaxFeeRateCodeID");
    private final String value;

    TaxFeeRatesGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TaxFeeRatesGetType fromValue(String v) {
        for (TaxFeeRatesGetType c: TaxFeeRatesGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
