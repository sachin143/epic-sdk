
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for Litigation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Litigation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Court" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateAnswered" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateServed" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DefenseAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="DefenseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefensePhoneExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefensePhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefenseRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaintiffAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="PlaintiffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaintiffPhoneExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaintiffPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaintiffRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Venue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="DefensePhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaintiffPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Litigation", propOrder = {
    "caseNumber",
    "claimID",
    "court",
    "dateAnswered",
    "dateServed",
    "defenseAddress",
    "defenseName",
    "defensePhoneExtension",
    "defensePhoneNumber",
    "defenseRemarks",
    "plaintiffAddress",
    "plaintiffName",
    "plaintiffPhoneExtension",
    "plaintiffPhoneNumber",
    "plaintiffRemarks",
    "timestamp",
    "venue",
    "defensePhoneCountryCode",
    "plaintiffPhoneCountryCode"
})
public class Litigation {

    @XmlElement(name = "CaseNumber", nillable = true)
    protected String caseNumber;
    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "Court", nillable = true)
    protected String court;
    @XmlElement(name = "DateAnswered", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateAnswered;
    @XmlElement(name = "DateServed", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateServed;
    @XmlElement(name = "DefenseAddress", nillable = true)
    protected Address defenseAddress;
    @XmlElement(name = "DefenseName", nillable = true)
    protected String defenseName;
    @XmlElement(name = "DefensePhoneExtension", nillable = true)
    protected String defensePhoneExtension;
    @XmlElement(name = "DefensePhoneNumber", nillable = true)
    protected String defensePhoneNumber;
    @XmlElement(name = "DefenseRemarks", nillable = true)
    protected String defenseRemarks;
    @XmlElement(name = "PlaintiffAddress", nillable = true)
    protected Address plaintiffAddress;
    @XmlElement(name = "PlaintiffName", nillable = true)
    protected String plaintiffName;
    @XmlElement(name = "PlaintiffPhoneExtension", nillable = true)
    protected String plaintiffPhoneExtension;
    @XmlElement(name = "PlaintiffPhoneNumber", nillable = true)
    protected String plaintiffPhoneNumber;
    @XmlElement(name = "PlaintiffRemarks", nillable = true)
    protected String plaintiffRemarks;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "Venue", nillable = true)
    protected Address venue;
    @XmlElement(name = "DefensePhoneCountryCode", nillable = true)
    protected String defensePhoneCountryCode;
    @XmlElement(name = "PlaintiffPhoneCountryCode", nillable = true)
    protected String plaintiffPhoneCountryCode;

    /**
     * Gets the value of the caseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNumber() {
        return caseNumber;
    }

    /**
     * Sets the value of the caseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNumber(String value) {
        this.caseNumber = value;
    }

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the court property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCourt() {
        return court;
    }

    /**
     * Sets the value of the court property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCourt(String value) {
        this.court = value;
    }

    /**
     * Gets the value of the dateAnswered property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateAnswered() {
        return dateAnswered;
    }

    /**
     * Sets the value of the dateAnswered property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateAnswered(XMLGregorianCalendar value) {
        this.dateAnswered = value;
    }

    /**
     * Gets the value of the dateServed property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateServed() {
        return dateServed;
    }

    /**
     * Sets the value of the dateServed property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateServed(XMLGregorianCalendar value) {
        this.dateServed = value;
    }

    /**
     * Gets the value of the defenseAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getDefenseAddress() {
        return defenseAddress;
    }

    /**
     * Sets the value of the defenseAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setDefenseAddress(Address value) {
        this.defenseAddress = value;
    }

    /**
     * Gets the value of the defenseName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefenseName() {
        return defenseName;
    }

    /**
     * Sets the value of the defenseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefenseName(String value) {
        this.defenseName = value;
    }

    /**
     * Gets the value of the defensePhoneExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefensePhoneExtension() {
        return defensePhoneExtension;
    }

    /**
     * Sets the value of the defensePhoneExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefensePhoneExtension(String value) {
        this.defensePhoneExtension = value;
    }

    /**
     * Gets the value of the defensePhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefensePhoneNumber() {
        return defensePhoneNumber;
    }

    /**
     * Sets the value of the defensePhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefensePhoneNumber(String value) {
        this.defensePhoneNumber = value;
    }

    /**
     * Gets the value of the defenseRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefenseRemarks() {
        return defenseRemarks;
    }

    /**
     * Sets the value of the defenseRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefenseRemarks(String value) {
        this.defenseRemarks = value;
    }

    /**
     * Gets the value of the plaintiffAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPlaintiffAddress() {
        return plaintiffAddress;
    }

    /**
     * Sets the value of the plaintiffAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPlaintiffAddress(Address value) {
        this.plaintiffAddress = value;
    }

    /**
     * Gets the value of the plaintiffName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaintiffName() {
        return plaintiffName;
    }

    /**
     * Sets the value of the plaintiffName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaintiffName(String value) {
        this.plaintiffName = value;
    }

    /**
     * Gets the value of the plaintiffPhoneExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaintiffPhoneExtension() {
        return plaintiffPhoneExtension;
    }

    /**
     * Sets the value of the plaintiffPhoneExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaintiffPhoneExtension(String value) {
        this.plaintiffPhoneExtension = value;
    }

    /**
     * Gets the value of the plaintiffPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaintiffPhoneNumber() {
        return plaintiffPhoneNumber;
    }

    /**
     * Sets the value of the plaintiffPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaintiffPhoneNumber(String value) {
        this.plaintiffPhoneNumber = value;
    }

    /**
     * Gets the value of the plaintiffRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaintiffRemarks() {
        return plaintiffRemarks;
    }

    /**
     * Sets the value of the plaintiffRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaintiffRemarks(String value) {
        this.plaintiffRemarks = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the venue property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getVenue() {
        return venue;
    }

    /**
     * Sets the value of the venue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setVenue(Address value) {
        this.venue = value;
    }

    /**
     * Gets the value of the defensePhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefensePhoneCountryCode() {
        return defensePhoneCountryCode;
    }

    /**
     * Sets the value of the defensePhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefensePhoneCountryCode(String value) {
        this.defensePhoneCountryCode = value;
    }

    /**
     * Gets the value of the plaintiffPhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaintiffPhoneCountryCode() {
        return plaintiffPhoneCountryCode;
    }

    /**
     * Sets the value of the plaintiffPhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaintiffPhoneCountryCode(String value) {
        this.plaintiffPhoneCountryCode = value;
    }

}
