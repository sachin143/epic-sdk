
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicationDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodeID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanYear" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PolicyCanBeLineOfBusiness" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TermDefault" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TypeOfBusinessCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="APSection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultSystemDateAsEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ClaimType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultIssuingLocation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludePolicyInformationSection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LossNoticeFilterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumberOfDaysPriorToExpirationShowInRenewalsManager" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TaxOptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnnualizedCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncludeAdditionalCoverages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeAdditionalInterests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeFormsEndorsements" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeRemarks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IvansProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyLineType", propOrder = {
    "applicationDetail",
    "classificationCode",
    "code",
    "codeID",
    "departmentCode",
    "description",
    "planYear",
    "policyCanBeLineOfBusiness",
    "profitCenterCode",
    "termDefault",
    "typeOfBusinessCode",
    "apSection",
    "defaultSystemDateAsEffectiveDate",
    "claimType",
    "defaultIssuingLocation",
    "includePolicyInformationSection",
    "isActive",
    "lossNoticeFilterCode",
    "numberOfDaysPriorToExpirationShowInRenewalsManager",
    "taxOptionCode",
    "annualizedCode",
    "includeAdditionalCoverages",
    "includeAdditionalInterests",
    "includeFormsEndorsements",
    "includeRemarks",
    "ivansProductCode"
})
public class PolicyLineType {

    @XmlElement(name = "ApplicationDetail", nillable = true)
    protected String applicationDetail;
    @XmlElement(name = "ClassificationCode", nillable = true)
    protected String classificationCode;
    @XmlElement(name = "Code", nillable = true)
    protected String code;
    @XmlElement(name = "CodeID")
    protected Integer codeID;
    @XmlElement(name = "DepartmentCode", nillable = true)
    protected String departmentCode;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "PlanYear", nillable = true)
    protected Boolean planYear;
    @XmlElement(name = "PolicyCanBeLineOfBusiness")
    protected Boolean policyCanBeLineOfBusiness;
    @XmlElement(name = "ProfitCenterCode", nillable = true)
    protected String profitCenterCode;
    @XmlElement(name = "TermDefault", nillable = true)
    protected String termDefault;
    @XmlElement(name = "TypeOfBusinessCode", nillable = true)
    protected String typeOfBusinessCode;
    @XmlElement(name = "APSection", nillable = true)
    protected String apSection;
    @XmlElement(name = "DefaultSystemDateAsEffectiveDate")
    protected Boolean defaultSystemDateAsEffectiveDate;
    @XmlElement(name = "ClaimType", nillable = true)
    protected String claimType;
    @XmlElement(name = "DefaultIssuingLocation")
    protected Boolean defaultIssuingLocation;
    @XmlElement(name = "IncludePolicyInformationSection")
    protected Boolean includePolicyInformationSection;
    @XmlElement(name = "IsActive")
    protected Boolean isActive;
    @XmlElement(name = "LossNoticeFilterCode", nillable = true)
    protected String lossNoticeFilterCode;
    @XmlElement(name = "NumberOfDaysPriorToExpirationShowInRenewalsManager", nillable = true)
    protected Integer numberOfDaysPriorToExpirationShowInRenewalsManager;
    @XmlElement(name = "TaxOptionCode", nillable = true)
    protected String taxOptionCode;
    @XmlElement(name = "AnnualizedCode", nillable = true)
    protected String annualizedCode;
    @XmlElement(name = "IncludeAdditionalCoverages")
    protected Boolean includeAdditionalCoverages;
    @XmlElement(name = "IncludeAdditionalInterests")
    protected Boolean includeAdditionalInterests;
    @XmlElement(name = "IncludeFormsEndorsements")
    protected Boolean includeFormsEndorsements;
    @XmlElement(name = "IncludeRemarks")
    protected Boolean includeRemarks;
    @XmlElement(name = "IvansProductCode", nillable = true)
    protected String ivansProductCode;

    /**
     * Gets the value of the applicationDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationDetail() {
        return applicationDetail;
    }

    /**
     * Sets the value of the applicationDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationDetail(String value) {
        this.applicationDetail = value;
    }

    /**
     * Gets the value of the classificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Sets the value of the classificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the codeID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodeID() {
        return codeID;
    }

    /**
     * Sets the value of the codeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodeID(Integer value) {
        this.codeID = value;
    }

    /**
     * Gets the value of the departmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * Sets the value of the departmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentCode(String value) {
        this.departmentCode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the planYear property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlanYear() {
        return planYear;
    }

    /**
     * Sets the value of the planYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlanYear(Boolean value) {
        this.planYear = value;
    }

    /**
     * Gets the value of the policyCanBeLineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPolicyCanBeLineOfBusiness() {
        return policyCanBeLineOfBusiness;
    }

    /**
     * Sets the value of the policyCanBeLineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPolicyCanBeLineOfBusiness(Boolean value) {
        this.policyCanBeLineOfBusiness = value;
    }

    /**
     * Gets the value of the profitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCenterCode() {
        return profitCenterCode;
    }

    /**
     * Sets the value of the profitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCenterCode(String value) {
        this.profitCenterCode = value;
    }

    /**
     * Gets the value of the termDefault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermDefault() {
        return termDefault;
    }

    /**
     * Sets the value of the termDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermDefault(String value) {
        this.termDefault = value;
    }

    /**
     * Gets the value of the typeOfBusinessCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOfBusinessCode() {
        return typeOfBusinessCode;
    }

    /**
     * Sets the value of the typeOfBusinessCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOfBusinessCode(String value) {
        this.typeOfBusinessCode = value;
    }

    /**
     * Gets the value of the apSection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPSection() {
        return apSection;
    }

    /**
     * Sets the value of the apSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPSection(String value) {
        this.apSection = value;
    }

    /**
     * Gets the value of the defaultSystemDateAsEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultSystemDateAsEffectiveDate() {
        return defaultSystemDateAsEffectiveDate;
    }

    /**
     * Sets the value of the defaultSystemDateAsEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultSystemDateAsEffectiveDate(Boolean value) {
        this.defaultSystemDateAsEffectiveDate = value;
    }

    /**
     * Gets the value of the claimType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimType() {
        return claimType;
    }

    /**
     * Sets the value of the claimType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimType(String value) {
        this.claimType = value;
    }

    /**
     * Gets the value of the defaultIssuingLocation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultIssuingLocation() {
        return defaultIssuingLocation;
    }

    /**
     * Sets the value of the defaultIssuingLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultIssuingLocation(Boolean value) {
        this.defaultIssuingLocation = value;
    }

    /**
     * Gets the value of the includePolicyInformationSection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludePolicyInformationSection() {
        return includePolicyInformationSection;
    }

    /**
     * Sets the value of the includePolicyInformationSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludePolicyInformationSection(Boolean value) {
        this.includePolicyInformationSection = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the lossNoticeFilterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLossNoticeFilterCode() {
        return lossNoticeFilterCode;
    }

    /**
     * Sets the value of the lossNoticeFilterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLossNoticeFilterCode(String value) {
        this.lossNoticeFilterCode = value;
    }

    /**
     * Gets the value of the numberOfDaysPriorToExpirationShowInRenewalsManager property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDaysPriorToExpirationShowInRenewalsManager() {
        return numberOfDaysPriorToExpirationShowInRenewalsManager;
    }

    /**
     * Sets the value of the numberOfDaysPriorToExpirationShowInRenewalsManager property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDaysPriorToExpirationShowInRenewalsManager(Integer value) {
        this.numberOfDaysPriorToExpirationShowInRenewalsManager = value;
    }

    /**
     * Gets the value of the taxOptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxOptionCode() {
        return taxOptionCode;
    }

    /**
     * Sets the value of the taxOptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxOptionCode(String value) {
        this.taxOptionCode = value;
    }

    /**
     * Gets the value of the annualizedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualizedCode() {
        return annualizedCode;
    }

    /**
     * Sets the value of the annualizedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualizedCode(String value) {
        this.annualizedCode = value;
    }

    /**
     * Gets the value of the includeAdditionalCoverages property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeAdditionalCoverages() {
        return includeAdditionalCoverages;
    }

    /**
     * Sets the value of the includeAdditionalCoverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeAdditionalCoverages(Boolean value) {
        this.includeAdditionalCoverages = value;
    }

    /**
     * Gets the value of the includeAdditionalInterests property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeAdditionalInterests() {
        return includeAdditionalInterests;
    }

    /**
     * Sets the value of the includeAdditionalInterests property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeAdditionalInterests(Boolean value) {
        this.includeAdditionalInterests = value;
    }

    /**
     * Gets the value of the includeFormsEndorsements property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeFormsEndorsements() {
        return includeFormsEndorsements;
    }

    /**
     * Sets the value of the includeFormsEndorsements property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeFormsEndorsements(Boolean value) {
        this.includeFormsEndorsements = value;
    }

    /**
     * Gets the value of the includeRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeRemarks() {
        return includeRemarks;
    }

    /**
     * Sets the value of the includeRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeRemarks(Boolean value) {
        this.includeRemarks = value;
    }

    /**
     * Gets the value of the ivansProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIvansProductCode() {
        return ivansProductCode;
    }

    /**
     * Sets the value of the ivansProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIvansProductCode(String value) {
        this.ivansProductCode = value;
    }

}
