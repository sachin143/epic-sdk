
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Broker"/>
 *     &lt;enumeration value="Client"/>
 *     &lt;enumeration value="Company"/>
 *     &lt;enumeration value="Employee"/>
 *     &lt;enumeration value="FinanceCompany"/>
 *     &lt;enumeration value="OtherInterest"/>
 *     &lt;enumeration value="Vendor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/_journalentry/")
@XmlEnum
public enum AssociationType {

    @XmlEnumValue("Broker")
    BROKER("Broker"),
    @XmlEnumValue("Client")
    CLIENT("Client"),
    @XmlEnumValue("Company")
    COMPANY("Company"),
    @XmlEnumValue("Employee")
    EMPLOYEE("Employee"),
    @XmlEnumValue("FinanceCompany")
    FINANCE_COMPANY("FinanceCompany"),
    @XmlEnumValue("OtherInterest")
    OTHER_INTEREST("OtherInterest"),
    @XmlEnumValue("Vendor")
    VENDOR("Vendor");
    private final String value;

    AssociationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociationType fromValue(String v) {
        for (AssociationType c: AssociationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
