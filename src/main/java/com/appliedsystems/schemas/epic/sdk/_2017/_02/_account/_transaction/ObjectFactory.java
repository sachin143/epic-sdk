
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaymentInstallmentItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", "PaymentInstallmentItems");
    private final static QName _PaymentInstallmentItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", "PaymentInstallmentItem");
    private final static QName _Receivable_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", "Receivable");
    private final static QName _ArrayOfReceivable_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", "ArrayOfReceivable");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentInstallmentItems }
     * 
     */
    public PaymentInstallmentItems createPaymentInstallmentItems() {
        return new PaymentInstallmentItems();
    }

    /**
     * Create an instance of {@link ArrayOfReceivable }
     * 
     */
    public ArrayOfReceivable createArrayOfReceivable() {
        return new ArrayOfReceivable();
    }

    /**
     * Create an instance of {@link Receivable }
     * 
     */
    public Receivable createReceivable() {
        return new Receivable();
    }

    /**
     * Create an instance of {@link PaymentInstallmentItem }
     * 
     */
    public PaymentInstallmentItem createPaymentInstallmentItem() {
        return new PaymentInstallmentItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentInstallmentItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", name = "PaymentInstallmentItems")
    public JAXBElement<PaymentInstallmentItems> createPaymentInstallmentItems(PaymentInstallmentItems value) {
        return new JAXBElement<PaymentInstallmentItems>(_PaymentInstallmentItems_QNAME, PaymentInstallmentItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentInstallmentItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", name = "PaymentInstallmentItem")
    public JAXBElement<PaymentInstallmentItem> createPaymentInstallmentItem(PaymentInstallmentItem value) {
        return new JAXBElement<PaymentInstallmentItem>(_PaymentInstallmentItem_QNAME, PaymentInstallmentItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Receivable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", name = "Receivable")
    public JAXBElement<Receivable> createReceivable(Receivable value) {
        return new JAXBElement<Receivable>(_Receivable_QNAME, Receivable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReceivable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/", name = "ArrayOfReceivable")
    public JAXBElement<ArrayOfReceivable> createArrayOfReceivable(ArrayOfReceivable value) {
        return new JAXBElement<ArrayOfReceivable>(_ArrayOfReceivable_QNAME, ArrayOfReceivable.class, null, value);
    }

}
