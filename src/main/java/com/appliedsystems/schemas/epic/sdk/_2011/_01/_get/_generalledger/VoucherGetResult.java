
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ArrayOfVoucher;


/**
 * <p>Java class for VoucherGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Vouchers" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ArrayOfVoucher" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherGetResult", propOrder = {
    "totalPages",
    "vouchers"
})
public class VoucherGetResult {

    @XmlElement(name = "TotalPages")
    protected Integer totalPages;
    @XmlElement(name = "Vouchers", nillable = true)
    protected ArrayOfVoucher vouchers;

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

    /**
     * Gets the value of the vouchers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVoucher }
     *     
     */
    public ArrayOfVoucher getVouchers() {
        return vouchers;
    }

    /**
     * Sets the value of the vouchers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVoucher }
     *     
     */
    public void setVouchers(ArrayOfVoucher value) {
        this.vouchers = value;
    }

}
