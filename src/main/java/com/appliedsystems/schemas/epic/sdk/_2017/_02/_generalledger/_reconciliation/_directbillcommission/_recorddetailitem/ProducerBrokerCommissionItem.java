
package com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ProducerBrokerCommissionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProducerBrokerCommissionItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DefaultProducerBrokerCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayableDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ProductionCreditPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenuePercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueReceivingAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProducerBrokerCommissionItem", propOrder = {
    "commissionAmount",
    "commissionPercentage",
    "commissionTypeCode",
    "contractID",
    "defaultProducerBrokerCommissionID",
    "orderNumber",
    "payableDueDate",
    "producerBrokerAccountLookupCode",
    "producerBrokerCode",
    "producerBrokerCommissionID",
    "productionCreditPercentage",
    "shareRevenueAgencyCode",
    "shareRevenueAgencyID",
    "shareRevenueAgencyName",
    "shareRevenueBranchCode",
    "shareRevenueBranchID",
    "shareRevenueBranchName",
    "shareRevenueDepartmentCode",
    "shareRevenueDepartmentID",
    "shareRevenueDepartmentName",
    "shareRevenuePercentage",
    "shareRevenueProfitCenterCode",
    "shareRevenueProfitCenterID",
    "shareRevenueProfitCenterName",
    "shareRevenueReceivingAmount"
})
public class ProducerBrokerCommissionItem {

    @XmlElement(name = "CommissionAmount", nillable = true)
    protected BigDecimal commissionAmount;
    @XmlElement(name = "CommissionPercentage", nillable = true)
    protected BigDecimal commissionPercentage;
    @XmlElement(name = "CommissionTypeCode", nillable = true)
    protected String commissionTypeCode;
    @XmlElement(name = "ContractID", nillable = true)
    protected Integer contractID;
    @XmlElement(name = "DefaultProducerBrokerCommissionID", nillable = true)
    protected Integer defaultProducerBrokerCommissionID;
    @XmlElement(name = "OrderNumber", nillable = true)
    protected Integer orderNumber;
    @XmlElement(name = "PayableDueDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payableDueDate;
    @XmlElement(name = "ProducerBrokerAccountLookupCode", nillable = true)
    protected String producerBrokerAccountLookupCode;
    @XmlElement(name = "ProducerBrokerCode", nillable = true)
    protected String producerBrokerCode;
    @XmlElement(name = "ProducerBrokerCommissionID")
    protected Integer producerBrokerCommissionID;
    @XmlElement(name = "ProductionCreditPercentage")
    protected BigDecimal productionCreditPercentage;
    @XmlElement(name = "ShareRevenueAgencyCode", nillable = true)
    protected String shareRevenueAgencyCode;
    @XmlElement(name = "ShareRevenueAgencyID")
    protected Integer shareRevenueAgencyID;
    @XmlElement(name = "ShareRevenueAgencyName", nillable = true)
    protected String shareRevenueAgencyName;
    @XmlElement(name = "ShareRevenueBranchCode", nillable = true)
    protected String shareRevenueBranchCode;
    @XmlElement(name = "ShareRevenueBranchID")
    protected Integer shareRevenueBranchID;
    @XmlElement(name = "ShareRevenueBranchName", nillable = true)
    protected String shareRevenueBranchName;
    @XmlElement(name = "ShareRevenueDepartmentCode", nillable = true)
    protected String shareRevenueDepartmentCode;
    @XmlElement(name = "ShareRevenueDepartmentID")
    protected Integer shareRevenueDepartmentID;
    @XmlElement(name = "ShareRevenueDepartmentName", nillable = true)
    protected String shareRevenueDepartmentName;
    @XmlElement(name = "ShareRevenuePercentage", nillable = true)
    protected BigDecimal shareRevenuePercentage;
    @XmlElement(name = "ShareRevenueProfitCenterCode", nillable = true)
    protected String shareRevenueProfitCenterCode;
    @XmlElement(name = "ShareRevenueProfitCenterID")
    protected Integer shareRevenueProfitCenterID;
    @XmlElement(name = "ShareRevenueProfitCenterName", nillable = true)
    protected String shareRevenueProfitCenterName;
    @XmlElement(name = "ShareRevenueReceivingAmount", nillable = true)
    protected BigDecimal shareRevenueReceivingAmount;

    /**
     * Gets the value of the commissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionAmount() {
        return commissionAmount;
    }

    /**
     * Sets the value of the commissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionAmount(BigDecimal value) {
        this.commissionAmount = value;
    }

    /**
     * Gets the value of the commissionPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionPercentage() {
        return commissionPercentage;
    }

    /**
     * Sets the value of the commissionPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionPercentage(BigDecimal value) {
        this.commissionPercentage = value;
    }

    /**
     * Gets the value of the commissionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionTypeCode() {
        return commissionTypeCode;
    }

    /**
     * Sets the value of the commissionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionTypeCode(String value) {
        this.commissionTypeCode = value;
    }

    /**
     * Gets the value of the contractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContractID() {
        return contractID;
    }

    /**
     * Sets the value of the contractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContractID(Integer value) {
        this.contractID = value;
    }

    /**
     * Gets the value of the defaultProducerBrokerCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultProducerBrokerCommissionID() {
        return defaultProducerBrokerCommissionID;
    }

    /**
     * Sets the value of the defaultProducerBrokerCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultProducerBrokerCommissionID(Integer value) {
        this.defaultProducerBrokerCommissionID = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the payableDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayableDueDate() {
        return payableDueDate;
    }

    /**
     * Sets the value of the payableDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayableDueDate(XMLGregorianCalendar value) {
        this.payableDueDate = value;
    }

    /**
     * Gets the value of the producerBrokerAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerBrokerAccountLookupCode() {
        return producerBrokerAccountLookupCode;
    }

    /**
     * Sets the value of the producerBrokerAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerBrokerAccountLookupCode(String value) {
        this.producerBrokerAccountLookupCode = value;
    }

    /**
     * Gets the value of the producerBrokerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerBrokerCode() {
        return producerBrokerCode;
    }

    /**
     * Sets the value of the producerBrokerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerBrokerCode(String value) {
        this.producerBrokerCode = value;
    }

    /**
     * Gets the value of the producerBrokerCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProducerBrokerCommissionID() {
        return producerBrokerCommissionID;
    }

    /**
     * Sets the value of the producerBrokerCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProducerBrokerCommissionID(Integer value) {
        this.producerBrokerCommissionID = value;
    }

    /**
     * Gets the value of the productionCreditPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductionCreditPercentage() {
        return productionCreditPercentage;
    }

    /**
     * Sets the value of the productionCreditPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductionCreditPercentage(BigDecimal value) {
        this.productionCreditPercentage = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyCode() {
        return shareRevenueAgencyCode;
    }

    /**
     * Sets the value of the shareRevenueAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyCode(String value) {
        this.shareRevenueAgencyCode = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueAgencyID() {
        return shareRevenueAgencyID;
    }

    /**
     * Sets the value of the shareRevenueAgencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueAgencyID(Integer value) {
        this.shareRevenueAgencyID = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyName() {
        return shareRevenueAgencyName;
    }

    /**
     * Sets the value of the shareRevenueAgencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyName(String value) {
        this.shareRevenueAgencyName = value;
    }

    /**
     * Gets the value of the shareRevenueBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchCode() {
        return shareRevenueBranchCode;
    }

    /**
     * Sets the value of the shareRevenueBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchCode(String value) {
        this.shareRevenueBranchCode = value;
    }

    /**
     * Gets the value of the shareRevenueBranchID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueBranchID() {
        return shareRevenueBranchID;
    }

    /**
     * Sets the value of the shareRevenueBranchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueBranchID(Integer value) {
        this.shareRevenueBranchID = value;
    }

    /**
     * Gets the value of the shareRevenueBranchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchName() {
        return shareRevenueBranchName;
    }

    /**
     * Sets the value of the shareRevenueBranchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchName(String value) {
        this.shareRevenueBranchName = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentCode() {
        return shareRevenueDepartmentCode;
    }

    /**
     * Sets the value of the shareRevenueDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentCode(String value) {
        this.shareRevenueDepartmentCode = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueDepartmentID() {
        return shareRevenueDepartmentID;
    }

    /**
     * Sets the value of the shareRevenueDepartmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueDepartmentID(Integer value) {
        this.shareRevenueDepartmentID = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentName() {
        return shareRevenueDepartmentName;
    }

    /**
     * Sets the value of the shareRevenueDepartmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentName(String value) {
        this.shareRevenueDepartmentName = value;
    }

    /**
     * Gets the value of the shareRevenuePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShareRevenuePercentage() {
        return shareRevenuePercentage;
    }

    /**
     * Sets the value of the shareRevenuePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShareRevenuePercentage(BigDecimal value) {
        this.shareRevenuePercentage = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterCode() {
        return shareRevenueProfitCenterCode;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterCode(String value) {
        this.shareRevenueProfitCenterCode = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueProfitCenterID() {
        return shareRevenueProfitCenterID;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueProfitCenterID(Integer value) {
        this.shareRevenueProfitCenterID = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterName() {
        return shareRevenueProfitCenterName;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterName(String value) {
        this.shareRevenueProfitCenterName = value;
    }

    /**
     * Gets the value of the shareRevenueReceivingAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShareRevenueReceivingAmount() {
        return shareRevenueReceivingAmount;
    }

    /**
     * Sets the value of the shareRevenueReceivingAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShareRevenueReceivingAmount(BigDecimal value) {
        this.shareRevenueReceivingAmount = value;
    }

}
