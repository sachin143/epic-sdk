
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ClaimCodeItems;


/**
 * <p>Java class for ReportingHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportingHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimCodes" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_reportinghistory/}ClaimCodeItems" minOccurs="0"/>
 *         &lt;element name="ClosedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClosedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingHistory", propOrder = {
    "claimCodes",
    "closedBy",
    "closedDate",
    "comments"
})
public class ReportingHistory {

    @XmlElement(name = "ClaimCodes", nillable = true)
    protected ClaimCodeItems claimCodes;
    @XmlElement(name = "ClosedBy", nillable = true)
    protected String closedBy;
    @XmlElement(name = "ClosedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar closedDate;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;

    /**
     * Gets the value of the claimCodes property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimCodeItems }
     *     
     */
    public ClaimCodeItems getClaimCodes() {
        return claimCodes;
    }

    /**
     * Sets the value of the claimCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimCodeItems }
     *     
     */
    public void setClaimCodes(ClaimCodeItems value) {
        this.claimCodes = value;
    }

    /**
     * Gets the value of the closedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClosedBy() {
        return closedBy;
    }

    /**
     * Sets the value of the closedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClosedBy(String value) {
        this.closedBy = value;
    }

    /**
     * Gets the value of the closedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClosedDate() {
        return closedDate;
    }

    /**
     * Sets the value of the closedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClosedDate(XMLGregorianCalendar value) {
        this.closedDate = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

}
