
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _JournalEntryFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "JournalEntryFilter");
    private final static QName _DirectBillCommissionGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "DirectBillCommissionGetResult");
    private final static QName _DisbursementGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "DisbursementGetResult");
    private final static QName _JournalEntryGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "JournalEntryGetResult");
    private final static QName _VoucherGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "VoucherGetResult");
    private final static QName _ReceiptGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", "ReceiptGetResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReceiptGetResult }
     * 
     */
    public ReceiptGetResult createReceiptGetResult() {
        return new ReceiptGetResult();
    }

    /**
     * Create an instance of {@link DisbursementGetResult }
     * 
     */
    public DisbursementGetResult createDisbursementGetResult() {
        return new DisbursementGetResult();
    }

    /**
     * Create an instance of {@link JournalEntryFilter }
     * 
     */
    public JournalEntryFilter createJournalEntryFilter() {
        return new JournalEntryFilter();
    }

    /**
     * Create an instance of {@link DirectBillCommissionGetResult }
     * 
     */
    public DirectBillCommissionGetResult createDirectBillCommissionGetResult() {
        return new DirectBillCommissionGetResult();
    }

    /**
     * Create an instance of {@link VoucherGetResult }
     * 
     */
    public VoucherGetResult createVoucherGetResult() {
        return new VoucherGetResult();
    }

    /**
     * Create an instance of {@link JournalEntryGetResult }
     * 
     */
    public JournalEntryGetResult createJournalEntryGetResult() {
        return new JournalEntryGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JournalEntryFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "JournalEntryFilter")
    public JAXBElement<JournalEntryFilter> createJournalEntryFilter(JournalEntryFilter value) {
        return new JAXBElement<JournalEntryFilter>(_JournalEntryFilter_QNAME, JournalEntryFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectBillCommissionGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "DirectBillCommissionGetResult")
    public JAXBElement<DirectBillCommissionGetResult> createDirectBillCommissionGetResult(DirectBillCommissionGetResult value) {
        return new JAXBElement<DirectBillCommissionGetResult>(_DirectBillCommissionGetResult_QNAME, DirectBillCommissionGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "DisbursementGetResult")
    public JAXBElement<DisbursementGetResult> createDisbursementGetResult(DisbursementGetResult value) {
        return new JAXBElement<DisbursementGetResult>(_DisbursementGetResult_QNAME, DisbursementGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JournalEntryGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "JournalEntryGetResult")
    public JAXBElement<JournalEntryGetResult> createJournalEntryGetResult(JournalEntryGetResult value) {
        return new JAXBElement<JournalEntryGetResult>(_JournalEntryGetResult_QNAME, JournalEntryGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "VoucherGetResult")
    public JAXBElement<VoucherGetResult> createVoucherGetResult(VoucherGetResult value) {
        return new JAXBElement<VoucherGetResult>(_VoucherGetResult_QNAME, VoucherGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/", name = "ReceiptGetResult")
    public JAXBElement<ReceiptGetResult> createReceiptGetResult(ReceiptGetResult value) {
        return new JAXBElement<ReceiptGetResult>(_ReceiptGetResult_QNAME, ReceiptGetResult.class, null, value);
    }

}
