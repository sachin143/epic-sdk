
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiptFilterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReceiptFilterType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="AccountLookup"/>
 *     &lt;enumeration value="AccountingMonth"/>
 *     &lt;enumeration value="BankAccount"/>
 *     &lt;enumeration value="DateEntered"/>
 *     &lt;enumeration value="EffectiveDate"/>
 *     &lt;enumeration value="GeneralLedgerSchedule"/>
 *     &lt;enumeration value="PaymentID"/>
 *     &lt;enumeration value="ReferNumber"/>
 *     &lt;enumeration value="ExportBatchNumber"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReceiptFilterType")
@XmlEnum
public enum ReceiptFilterType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("AccountLookup")
    ACCOUNT_LOOKUP("AccountLookup"),
    @XmlEnumValue("AccountingMonth")
    ACCOUNTING_MONTH("AccountingMonth"),
    @XmlEnumValue("BankAccount")
    BANK_ACCOUNT("BankAccount"),
    @XmlEnumValue("DateEntered")
    DATE_ENTERED("DateEntered"),
    @XmlEnumValue("EffectiveDate")
    EFFECTIVE_DATE("EffectiveDate"),
    @XmlEnumValue("GeneralLedgerSchedule")
    GENERAL_LEDGER_SCHEDULE("GeneralLedgerSchedule"),
    @XmlEnumValue("PaymentID")
    PAYMENT_ID("PaymentID"),
    @XmlEnumValue("ReferNumber")
    REFER_NUMBER("ReferNumber"),
    @XmlEnumValue("ExportBatchNumber")
    EXPORT_BATCH_NUMBER("ExportBatchNumber");
    private final String value;

    ReceiptFilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReceiptFilterType fromValue(String v) {
        for (ReceiptFilterType c: ReceiptFilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
