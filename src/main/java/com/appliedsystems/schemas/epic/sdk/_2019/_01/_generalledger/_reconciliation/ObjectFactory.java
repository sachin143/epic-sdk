
package com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BankDetail_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", "BankDetail");
    private final static QName _BankDetailItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", "BankDetailItem");
    private final static QName _BankDetailItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", "BankDetailItems");
    private final static QName _ArrayOfBank_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", "ArrayOfBank");
    private final static QName _Bank_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", "Bank");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Bank }
     * 
     */
    public Bank createBank() {
        return new Bank();
    }

    /**
     * Create an instance of {@link ArrayOfBank }
     * 
     */
    public ArrayOfBank createArrayOfBank() {
        return new ArrayOfBank();
    }

    /**
     * Create an instance of {@link BankDetail }
     * 
     */
    public BankDetail createBankDetail() {
        return new BankDetail();
    }

    /**
     * Create an instance of {@link BankDetailItem }
     * 
     */
    public BankDetailItem createBankDetailItem() {
        return new BankDetailItem();
    }

    /**
     * Create an instance of {@link BankDetailItems }
     * 
     */
    public BankDetailItems createBankDetailItems() {
        return new BankDetailItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", name = "BankDetail")
    public JAXBElement<BankDetail> createBankDetail(BankDetail value) {
        return new JAXBElement<BankDetail>(_BankDetail_QNAME, BankDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetailItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", name = "BankDetailItem")
    public JAXBElement<BankDetailItem> createBankDetailItem(BankDetailItem value) {
        return new JAXBElement<BankDetailItem>(_BankDetailItem_QNAME, BankDetailItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetailItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", name = "BankDetailItems")
    public JAXBElement<BankDetailItems> createBankDetailItems(BankDetailItems value) {
        return new JAXBElement<BankDetailItems>(_BankDetailItems_QNAME, BankDetailItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfBank }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", name = "ArrayOfBank")
    public JAXBElement<ArrayOfBank> createArrayOfBank(ArrayOfBank value) {
        return new JAXBElement<ArrayOfBank>(_ArrayOfBank_QNAME, ArrayOfBank.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Bank }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/", name = "Bank")
    public JAXBElement<Bank> createBank(Bank value) {
        return new JAXBElement<Bank>(_Bank_QNAME, Bank.class, null, value);
    }

}
