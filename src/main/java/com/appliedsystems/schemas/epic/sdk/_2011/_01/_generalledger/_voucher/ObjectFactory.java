
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Check_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", "Check");
    private final static QName _Detail_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", "Detail");
    private final static QName _Discount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", "Discount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Discount }
     * 
     */
    public Discount createDiscount() {
        return new Discount();
    }

    /**
     * Create an instance of {@link Check }
     * 
     */
    public Check createCheck() {
        return new Check();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Check }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", name = "Check")
    public JAXBElement<Check> createCheck(Check value) {
        return new JAXBElement<Check>(_Check_QNAME, Check.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Detail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", name = "Detail")
    public JAXBElement<Detail> createDetail(Detail value) {
        return new JAXBElement<Detail>(_Detail_QNAME, Detail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Discount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/", name = "Discount")
    public JAXBElement<Discount> createDiscount(Discount value) {
        return new JAXBElement<Discount>(_Discount_QNAME, Discount.class, null, value);
    }

}
