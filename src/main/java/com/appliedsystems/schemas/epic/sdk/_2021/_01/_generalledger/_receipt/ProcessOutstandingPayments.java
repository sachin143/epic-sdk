
package com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.OutstandingPayments;


/**
 * <p>Java class for ProcessOutstandingPayments complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcessOutstandingPayments">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OutstandingPaymentValue" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments}OutstandingPayments" minOccurs="0"/>
 *         &lt;element name="PaymentCreateTransmissionFile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PaymentNoTransmissionFile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReceiptForPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UpdateAccountingMonthToMatchReceipt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessOutstandingPayments", propOrder = {
    "outstandingPaymentValue",
    "paymentCreateTransmissionFile",
    "paymentNoTransmissionFile",
    "receiptForPayment",
    "updateAccountingMonthToMatchReceipt"
})
public class ProcessOutstandingPayments {

    @XmlElement(name = "OutstandingPaymentValue", nillable = true)
    protected OutstandingPayments outstandingPaymentValue;
    @XmlElement(name = "PaymentCreateTransmissionFile")
    protected Boolean paymentCreateTransmissionFile;
    @XmlElement(name = "PaymentNoTransmissionFile")
    protected Boolean paymentNoTransmissionFile;
    @XmlElement(name = "ReceiptForPayment")
    protected Boolean receiptForPayment;
    @XmlElement(name = "UpdateAccountingMonthToMatchReceipt")
    protected Boolean updateAccountingMonthToMatchReceipt;

    /**
     * Gets the value of the outstandingPaymentValue property.
     * 
     * @return
     *     possible object is
     *     {@link OutstandingPayments }
     *     
     */
    public OutstandingPayments getOutstandingPaymentValue() {
        return outstandingPaymentValue;
    }

    /**
     * Sets the value of the outstandingPaymentValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutstandingPayments }
     *     
     */
    public void setOutstandingPaymentValue(OutstandingPayments value) {
        this.outstandingPaymentValue = value;
    }

    /**
     * Gets the value of the paymentCreateTransmissionFile property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPaymentCreateTransmissionFile() {
        return paymentCreateTransmissionFile;
    }

    /**
     * Sets the value of the paymentCreateTransmissionFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaymentCreateTransmissionFile(Boolean value) {
        this.paymentCreateTransmissionFile = value;
    }

    /**
     * Gets the value of the paymentNoTransmissionFile property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPaymentNoTransmissionFile() {
        return paymentNoTransmissionFile;
    }

    /**
     * Sets the value of the paymentNoTransmissionFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaymentNoTransmissionFile(Boolean value) {
        this.paymentNoTransmissionFile = value;
    }

    /**
     * Gets the value of the receiptForPayment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReceiptForPayment() {
        return receiptForPayment;
    }

    /**
     * Sets the value of the receiptForPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReceiptForPayment(Boolean value) {
        this.receiptForPayment = value;
    }

    /**
     * Gets the value of the updateAccountingMonthToMatchReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUpdateAccountingMonthToMatchReceipt() {
        return updateAccountingMonthToMatchReceipt;
    }

    /**
     * Sets the value of the updateAccountingMonthToMatchReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpdateAccountingMonthToMatchReceipt(Boolean value) {
        this.updateAccountingMonthToMatchReceipt = value;
    }

}
