
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIssueNotIssuePolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIssueNotIssuePolicy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueNotIssuePolicy" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}IssueNotIssuePolicy" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIssueNotIssuePolicy", propOrder = {
    "issueNotIssuePolicies"
})
public class ArrayOfIssueNotIssuePolicy {

    @XmlElement(name = "IssueNotIssuePolicy", nillable = true)
    protected List<IssueNotIssuePolicy> issueNotIssuePolicies;

    /**
     * Gets the value of the issueNotIssuePolicies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the issueNotIssuePolicies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIssueNotIssuePolicies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IssueNotIssuePolicy }
     * 
     * 
     */
    public List<IssueNotIssuePolicy> getIssueNotIssuePolicies() {
        if (issueNotIssuePolicies == null) {
            issueNotIssuePolicies = new ArrayList<IssueNotIssuePolicy>();
        }
        return this.issueNotIssuePolicies;
    }

}
