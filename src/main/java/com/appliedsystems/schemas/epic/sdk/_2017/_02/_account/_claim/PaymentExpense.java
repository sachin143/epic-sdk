
package com.appliedsystems.schemas.epic.sdk._2017._02._account._claim;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.PaymentExpenseItems;


/**
 * <p>Java class for PaymentExpense complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentExpense">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ExpensesPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ExpensesReserved" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IndemnityPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IndemnityReserved" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PaymentExpenseItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/_paymentexpense/}PaymentExpenseItems" minOccurs="0"/>
 *         &lt;element name="ReceivedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ReceivedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SalvageRecoveryPaid" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentExpense", propOrder = {
    "claimID",
    "expensesPaid",
    "expensesReserved",
    "indemnityPaid",
    "indemnityReserved",
    "paymentExpenseItems",
    "receivedAmount",
    "receivedDate",
    "salvageRecoveryPaid",
    "sentAmount",
    "sentDate",
    "timestamp"
})
public class PaymentExpense {

    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "ExpensesPaid", nillable = true)
    protected BigDecimal expensesPaid;
    @XmlElement(name = "ExpensesReserved", nillable = true)
    protected BigDecimal expensesReserved;
    @XmlElement(name = "IndemnityPaid", nillable = true)
    protected BigDecimal indemnityPaid;
    @XmlElement(name = "IndemnityReserved", nillable = true)
    protected BigDecimal indemnityReserved;
    @XmlElement(name = "PaymentExpenseItems", nillable = true)
    protected PaymentExpenseItems paymentExpenseItems;
    @XmlElement(name = "ReceivedAmount", nillable = true)
    protected BigDecimal receivedAmount;
    @XmlElement(name = "ReceivedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receivedDate;
    @XmlElement(name = "SalvageRecoveryPaid", nillable = true)
    protected BigDecimal salvageRecoveryPaid;
    @XmlElement(name = "SentAmount", nillable = true)
    protected BigDecimal sentAmount;
    @XmlElement(name = "SentDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sentDate;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the expensesPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpensesPaid() {
        return expensesPaid;
    }

    /**
     * Sets the value of the expensesPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpensesPaid(BigDecimal value) {
        this.expensesPaid = value;
    }

    /**
     * Gets the value of the expensesReserved property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExpensesReserved() {
        return expensesReserved;
    }

    /**
     * Sets the value of the expensesReserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExpensesReserved(BigDecimal value) {
        this.expensesReserved = value;
    }

    /**
     * Gets the value of the indemnityPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIndemnityPaid() {
        return indemnityPaid;
    }

    /**
     * Sets the value of the indemnityPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIndemnityPaid(BigDecimal value) {
        this.indemnityPaid = value;
    }

    /**
     * Gets the value of the indemnityReserved property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIndemnityReserved() {
        return indemnityReserved;
    }

    /**
     * Sets the value of the indemnityReserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIndemnityReserved(BigDecimal value) {
        this.indemnityReserved = value;
    }

    /**
     * Gets the value of the paymentExpenseItems property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentExpenseItems }
     *     
     */
    public PaymentExpenseItems getPaymentExpenseItems() {
        return paymentExpenseItems;
    }

    /**
     * Sets the value of the paymentExpenseItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentExpenseItems }
     *     
     */
    public void setPaymentExpenseItems(PaymentExpenseItems value) {
        this.paymentExpenseItems = value;
    }

    /**
     * Gets the value of the receivedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReceivedAmount() {
        return receivedAmount;
    }

    /**
     * Sets the value of the receivedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReceivedAmount(BigDecimal value) {
        this.receivedAmount = value;
    }

    /**
     * Gets the value of the receivedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceivedDate() {
        return receivedDate;
    }

    /**
     * Sets the value of the receivedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceivedDate(XMLGregorianCalendar value) {
        this.receivedDate = value;
    }

    /**
     * Gets the value of the salvageRecoveryPaid property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalvageRecoveryPaid() {
        return salvageRecoveryPaid;
    }

    /**
     * Sets the value of the salvageRecoveryPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalvageRecoveryPaid(BigDecimal value) {
        this.salvageRecoveryPaid = value;
    }

    /**
     * Gets the value of the sentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSentAmount() {
        return sentAmount;
    }

    /**
     * Sets the value of the sentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSentAmount(BigDecimal value) {
        this.sentAmount = value;
    }

    /**
     * Gets the value of the sentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSentDate() {
        return sentDate;
    }

    /**
     * Sets the value of the sentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSentDate(XMLGregorianCalendar value) {
        this.sentDate = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
