
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.LineItems;


/**
 * <p>Java class for EndorseReviseAddLineMidterm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EndorseReviseAddLineMidterm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDateOfChange" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LinesOfBusiness" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/_action/_endorsereviseaddlinemidterm/}LineItems" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EndorseReviseAddLineMidterm", propOrder = {
    "description",
    "effectiveDateOfChange",
    "linesOfBusiness",
    "policyID"
})
public class EndorseReviseAddLineMidterm {

    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EffectiveDateOfChange")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDateOfChange;
    @XmlElement(name = "LinesOfBusiness", nillable = true)
    protected LineItems linesOfBusiness;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effectiveDateOfChange property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDateOfChange() {
        return effectiveDateOfChange;
    }

    /**
     * Sets the value of the effectiveDateOfChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDateOfChange(XMLGregorianCalendar value) {
        this.effectiveDateOfChange = value;
    }

    /**
     * Gets the value of the linesOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link LineItems }
     *     
     */
    public LineItems getLinesOfBusiness() {
        return linesOfBusiness;
    }

    /**
     * Sets the value of the linesOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItems }
     *     
     */
    public void setLinesOfBusiness(LineItems value) {
        this.linesOfBusiness = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

}
