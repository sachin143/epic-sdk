
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaidTransactionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/_common/", "PaidTransactionItems");
    private final static QName _PaidTransactionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/_common/", "PaidTransactionItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaidTransactionItem }
     * 
     */
    public PaidTransactionItem createPaidTransactionItem() {
        return new PaidTransactionItem();
    }

    /**
     * Create an instance of {@link PaidTransactionItems }
     * 
     */
    public PaidTransactionItems createPaidTransactionItems() {
        return new PaidTransactionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidTransactionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/_common/", name = "PaidTransactionItems")
    public JAXBElement<PaidTransactionItems> createPaidTransactionItems(PaidTransactionItems value) {
        return new JAXBElement<PaidTransactionItems>(_PaidTransactionItems_QNAME, PaidTransactionItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidTransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/_common/", name = "PaidTransactionItem")
    public JAXBElement<PaidTransactionItem> createPaidTransactionItem(PaidTransactionItem value) {
        return new JAXBElement<PaidTransactionItem>(_PaidTransactionItem_QNAME, PaidTransactionItem.class, null, value);
    }

}
