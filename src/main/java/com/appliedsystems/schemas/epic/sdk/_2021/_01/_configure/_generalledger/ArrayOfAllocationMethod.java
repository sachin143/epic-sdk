
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAllocationMethod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAllocationMethod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationMethod" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}AllocationMethod" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAllocationMethod", propOrder = {
    "allocationMethods"
})
public class ArrayOfAllocationMethod {

    @XmlElement(name = "AllocationMethod", nillable = true)
    protected List<AllocationMethod> allocationMethods;

    /**
     * Gets the value of the allocationMethods property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocationMethods property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocationMethods().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocationMethod }
     * 
     * 
     */
    public List<AllocationMethod> getAllocationMethods() {
        if (allocationMethods == null) {
            allocationMethods = new ArrayList<AllocationMethod>();
        }
        return this.allocationMethods;
    }

}
