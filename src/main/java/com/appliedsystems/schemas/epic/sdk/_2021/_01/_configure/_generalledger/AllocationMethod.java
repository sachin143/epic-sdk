
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for AllocationMethod complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationMethod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationMethodDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocationMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AllocationStructureCombination" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}StructureCombinations" minOccurs="0"/>
 *         &lt;element name="AutomaticBreakdown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AutomaticBreakdownOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ValueAsPercentage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationMethod", propOrder = {
    "allocationMethodDescription",
    "allocationMethodID",
    "allocationStructureCombination",
    "automaticBreakdown",
    "automaticBreakdownOption",
    "timestamp",
    "valueAsPercentage"
})
public class AllocationMethod {

    @XmlElement(name = "AllocationMethodDescription", nillable = true)
    protected String allocationMethodDescription;
    @XmlElement(name = "AllocationMethodID")
    protected Integer allocationMethodID;
    @XmlElement(name = "AllocationStructureCombination", nillable = true)
    protected StructureCombinations allocationStructureCombination;
    @XmlElement(name = "AutomaticBreakdown")
    protected Boolean automaticBreakdown;
    @XmlElement(name = "AutomaticBreakdownOption", nillable = true)
    protected OptionType automaticBreakdownOption;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "ValueAsPercentage", nillable = true)
    protected Boolean valueAsPercentage;

    /**
     * Gets the value of the allocationMethodDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocationMethodDescription() {
        return allocationMethodDescription;
    }

    /**
     * Sets the value of the allocationMethodDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocationMethodDescription(String value) {
        this.allocationMethodDescription = value;
    }

    /**
     * Gets the value of the allocationMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAllocationMethodID() {
        return allocationMethodID;
    }

    /**
     * Sets the value of the allocationMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAllocationMethodID(Integer value) {
        this.allocationMethodID = value;
    }

    /**
     * Gets the value of the allocationStructureCombination property.
     * 
     * @return
     *     possible object is
     *     {@link StructureCombinations }
     *     
     */
    public StructureCombinations getAllocationStructureCombination() {
        return allocationStructureCombination;
    }

    /**
     * Sets the value of the allocationStructureCombination property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureCombinations }
     *     
     */
    public void setAllocationStructureCombination(StructureCombinations value) {
        this.allocationStructureCombination = value;
    }

    /**
     * Gets the value of the automaticBreakdown property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutomaticBreakdown() {
        return automaticBreakdown;
    }

    /**
     * Sets the value of the automaticBreakdown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticBreakdown(Boolean value) {
        this.automaticBreakdown = value;
    }

    /**
     * Gets the value of the automaticBreakdownOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAutomaticBreakdownOption() {
        return automaticBreakdownOption;
    }

    /**
     * Sets the value of the automaticBreakdownOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAutomaticBreakdownOption(OptionType value) {
        this.automaticBreakdownOption = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the valueAsPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValueAsPercentage() {
        return valueAsPercentage;
    }

    /**
     * Sets the value of the valueAsPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValueAsPercentage(Boolean value) {
        this.valueAsPercentage = value;
    }

}
