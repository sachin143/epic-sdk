
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.Flags;


/**
 * <p>Java class for DetailItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DetailItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DebitCreditOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailItemID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_detail/_detailitem/}Flags" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerScheduleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocationMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AllocationStructureGroupingID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetailItem", propOrder = {
    "amount",
    "debitCreditOption",
    "description",
    "detailItemID",
    "flag",
    "generalLedgerAccountNumberCode",
    "generalLedgerScheduleCode",
    "generalLedgerSubAccountNumberCode",
    "structureAgencyCode",
    "structureBranchCode",
    "structureDepartmentCode",
    "structureProfitCenterCode",
    "allocationMethodID",
    "allocationStructureGroupingID"
})
public class DetailItem {

    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "DebitCreditOption", nillable = true)
    protected OptionType debitCreditOption;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DetailItemID")
    protected Integer detailItemID;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "GeneralLedgerAccountNumberCode", nillable = true)
    protected String generalLedgerAccountNumberCode;
    @XmlElement(name = "GeneralLedgerScheduleCode", nillable = true)
    protected String generalLedgerScheduleCode;
    @XmlElement(name = "GeneralLedgerSubAccountNumberCode", nillable = true)
    protected String generalLedgerSubAccountNumberCode;
    @XmlElement(name = "StructureAgencyCode", nillable = true)
    protected String structureAgencyCode;
    @XmlElement(name = "StructureBranchCode", nillable = true)
    protected String structureBranchCode;
    @XmlElement(name = "StructureDepartmentCode", nillable = true)
    protected String structureDepartmentCode;
    @XmlElement(name = "StructureProfitCenterCode", nillable = true)
    protected String structureProfitCenterCode;
    @XmlElement(name = "AllocationMethodID")
    protected Integer allocationMethodID;
    @XmlElement(name = "AllocationStructureGroupingID")
    protected Integer allocationStructureGroupingID;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the debitCreditOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getDebitCreditOption() {
        return debitCreditOption;
    }

    /**
     * Sets the value of the debitCreditOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setDebitCreditOption(OptionType value) {
        this.debitCreditOption = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailItemID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDetailItemID() {
        return detailItemID;
    }

    /**
     * Sets the value of the detailItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDetailItemID(Integer value) {
        this.detailItemID = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the generalLedgerAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerAccountNumberCode() {
        return generalLedgerAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerAccountNumberCode(String value) {
        this.generalLedgerAccountNumberCode = value;
    }

    /**
     * Gets the value of the generalLedgerScheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerScheduleCode() {
        return generalLedgerScheduleCode;
    }

    /**
     * Sets the value of the generalLedgerScheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerScheduleCode(String value) {
        this.generalLedgerScheduleCode = value;
    }

    /**
     * Gets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerSubAccountNumberCode() {
        return generalLedgerSubAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerSubAccountNumberCode(String value) {
        this.generalLedgerSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the structureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureAgencyCode() {
        return structureAgencyCode;
    }

    /**
     * Sets the value of the structureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureAgencyCode(String value) {
        this.structureAgencyCode = value;
    }

    /**
     * Gets the value of the structureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureBranchCode() {
        return structureBranchCode;
    }

    /**
     * Sets the value of the structureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureBranchCode(String value) {
        this.structureBranchCode = value;
    }

    /**
     * Gets the value of the structureDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureDepartmentCode() {
        return structureDepartmentCode;
    }

    /**
     * Sets the value of the structureDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureDepartmentCode(String value) {
        this.structureDepartmentCode = value;
    }

    /**
     * Gets the value of the structureProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureProfitCenterCode() {
        return structureProfitCenterCode;
    }

    /**
     * Sets the value of the structureProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureProfitCenterCode(String value) {
        this.structureProfitCenterCode = value;
    }

    /**
     * Gets the value of the allocationMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAllocationMethodID() {
        return allocationMethodID;
    }

    /**
     * Sets the value of the allocationMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAllocationMethodID(Integer value) {
        this.allocationMethodID = value;
    }

    /**
     * Gets the value of the allocationStructureGroupingID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAllocationStructureGroupingID() {
        return allocationStructureGroupingID;
    }

    /**
     * Sets the value of the allocationStructureGroupingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAllocationStructureGroupingID(Integer value) {
        this.allocationStructureGroupingID = value;
    }

}
