
package com.appliedsystems.schemas.epic.sdk._2009._07._account._client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.AgencyDefinedCodeItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.RelationshipItems;


/**
 * <p>Java class for CategoriesHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoriesHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyDefinedCategories" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}AgencyDefinedCodeItems" minOccurs="0"/>
 *         &lt;element name="DateConverted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PriorAccountID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Relationships" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}RelationshipItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoriesHistory", propOrder = {
    "agencyDefinedCategories",
    "dateConverted",
    "priorAccountID",
    "relationships"
})
public class CategoriesHistory {

    @XmlElement(name = "AgencyDefinedCategories", nillable = true)
    protected AgencyDefinedCodeItems agencyDefinedCategories;
    @XmlElement(name = "DateConverted", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateConverted;
    @XmlElement(name = "PriorAccountID", nillable = true)
    protected String priorAccountID;
    @XmlElement(name = "Relationships", nillable = true)
    protected RelationshipItems relationships;

    /**
     * Gets the value of the agencyDefinedCategories property.
     * 
     * @return
     *     possible object is
     *     {@link AgencyDefinedCodeItems }
     *     
     */
    public AgencyDefinedCodeItems getAgencyDefinedCategories() {
        return agencyDefinedCategories;
    }

    /**
     * Sets the value of the agencyDefinedCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgencyDefinedCodeItems }
     *     
     */
    public void setAgencyDefinedCategories(AgencyDefinedCodeItems value) {
        this.agencyDefinedCategories = value;
    }

    /**
     * Gets the value of the dateConverted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateConverted() {
        return dateConverted;
    }

    /**
     * Sets the value of the dateConverted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateConverted(XMLGregorianCalendar value) {
        this.dateConverted = value;
    }

    /**
     * Gets the value of the priorAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorAccountID() {
        return priorAccountID;
    }

    /**
     * Sets the value of the priorAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorAccountID(String value) {
        this.priorAccountID = value;
    }

    /**
     * Gets the value of the relationships property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipItems }
     *     
     */
    public RelationshipItems getRelationships() {
        return relationships;
    }

    /**
     * Sets the value of the relationships property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipItems }
     *     
     */
    public void setRelationships(RelationshipItems value) {
        this.relationships = value;
    }

}
