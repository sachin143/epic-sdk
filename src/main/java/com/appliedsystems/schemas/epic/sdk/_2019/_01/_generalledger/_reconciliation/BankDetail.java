
package com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BankDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckAndWithdrawalItems" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}BankDetailItems" minOccurs="0"/>
 *         &lt;element name="ChecksAndWithdrawalsTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepositAndAdditionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}BankDetailItems" minOccurs="0"/>
 *         &lt;element name="DepositsAndAdditionsTotal" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SummaryBankStatementEndingBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SummaryBeginningBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SummaryDifference" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SummaryNetChange" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SummaryReconciliationEndingBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankDetail", propOrder = {
    "checkAndWithdrawalItems",
    "checksAndWithdrawalsTotal",
    "depositAndAdditionItems",
    "depositsAndAdditionsTotal",
    "summaryBankStatementEndingBalance",
    "summaryBeginningBalance",
    "summaryDifference",
    "summaryNetChange",
    "summaryReconciliationEndingBalance"
})
public class BankDetail {

    @XmlElement(name = "CheckAndWithdrawalItems", nillable = true)
    protected BankDetailItems checkAndWithdrawalItems;
    @XmlElement(name = "ChecksAndWithdrawalsTotal", nillable = true)
    protected BigDecimal checksAndWithdrawalsTotal;
    @XmlElement(name = "DepositAndAdditionItems", nillable = true)
    protected BankDetailItems depositAndAdditionItems;
    @XmlElement(name = "DepositsAndAdditionsTotal", nillable = true)
    protected BigDecimal depositsAndAdditionsTotal;
    @XmlElement(name = "SummaryBankStatementEndingBalance", nillable = true)
    protected BigDecimal summaryBankStatementEndingBalance;
    @XmlElement(name = "SummaryBeginningBalance", nillable = true)
    protected BigDecimal summaryBeginningBalance;
    @XmlElement(name = "SummaryDifference", nillable = true)
    protected BigDecimal summaryDifference;
    @XmlElement(name = "SummaryNetChange", nillable = true)
    protected BigDecimal summaryNetChange;
    @XmlElement(name = "SummaryReconciliationEndingBalance", nillable = true)
    protected BigDecimal summaryReconciliationEndingBalance;

    /**
     * Gets the value of the checkAndWithdrawalItems property.
     * 
     * @return
     *     possible object is
     *     {@link BankDetailItems }
     *     
     */
    public BankDetailItems getCheckAndWithdrawalItems() {
        return checkAndWithdrawalItems;
    }

    /**
     * Sets the value of the checkAndWithdrawalItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankDetailItems }
     *     
     */
    public void setCheckAndWithdrawalItems(BankDetailItems value) {
        this.checkAndWithdrawalItems = value;
    }

    /**
     * Gets the value of the checksAndWithdrawalsTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChecksAndWithdrawalsTotal() {
        return checksAndWithdrawalsTotal;
    }

    /**
     * Sets the value of the checksAndWithdrawalsTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChecksAndWithdrawalsTotal(BigDecimal value) {
        this.checksAndWithdrawalsTotal = value;
    }

    /**
     * Gets the value of the depositAndAdditionItems property.
     * 
     * @return
     *     possible object is
     *     {@link BankDetailItems }
     *     
     */
    public BankDetailItems getDepositAndAdditionItems() {
        return depositAndAdditionItems;
    }

    /**
     * Sets the value of the depositAndAdditionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankDetailItems }
     *     
     */
    public void setDepositAndAdditionItems(BankDetailItems value) {
        this.depositAndAdditionItems = value;
    }

    /**
     * Gets the value of the depositsAndAdditionsTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepositsAndAdditionsTotal() {
        return depositsAndAdditionsTotal;
    }

    /**
     * Sets the value of the depositsAndAdditionsTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepositsAndAdditionsTotal(BigDecimal value) {
        this.depositsAndAdditionsTotal = value;
    }

    /**
     * Gets the value of the summaryBankStatementEndingBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummaryBankStatementEndingBalance() {
        return summaryBankStatementEndingBalance;
    }

    /**
     * Sets the value of the summaryBankStatementEndingBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummaryBankStatementEndingBalance(BigDecimal value) {
        this.summaryBankStatementEndingBalance = value;
    }

    /**
     * Gets the value of the summaryBeginningBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummaryBeginningBalance() {
        return summaryBeginningBalance;
    }

    /**
     * Sets the value of the summaryBeginningBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummaryBeginningBalance(BigDecimal value) {
        this.summaryBeginningBalance = value;
    }

    /**
     * Gets the value of the summaryDifference property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummaryDifference() {
        return summaryDifference;
    }

    /**
     * Sets the value of the summaryDifference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummaryDifference(BigDecimal value) {
        this.summaryDifference = value;
    }

    /**
     * Gets the value of the summaryNetChange property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummaryNetChange() {
        return summaryNetChange;
    }

    /**
     * Sets the value of the summaryNetChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummaryNetChange(BigDecimal value) {
        this.summaryNetChange = value;
    }

    /**
     * Gets the value of the summaryReconciliationEndingBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummaryReconciliationEndingBalance() {
        return summaryReconciliationEndingBalance;
    }

    /**
     * Sets the value of the summaryReconciliationEndingBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummaryReconciliationEndingBalance(BigDecimal value) {
        this.summaryReconciliationEndingBalance = value;
    }

}
