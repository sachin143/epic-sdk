
package com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfUpdateRenewalStage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfUpdateRenewalStage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateRenewalStage" type="{http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/}UpdateRenewalStage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfUpdateRenewalStage", propOrder = {
    "updateRenewalStages"
})
public class ArrayOfUpdateRenewalStage {

    @XmlElement(name = "UpdateRenewalStage", nillable = true)
    protected List<UpdateRenewalStage> updateRenewalStages;

    /**
     * Gets the value of the updateRenewalStages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the updateRenewalStages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdateRenewalStages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateRenewalStage }
     * 
     * 
     */
    public List<UpdateRenewalStage> getUpdateRenewalStages() {
        if (updateRenewalStages == null) {
            updateRenewalStages = new ArrayList<UpdateRenewalStage>();
        }
        return this.updateRenewalStages;
    }

}
