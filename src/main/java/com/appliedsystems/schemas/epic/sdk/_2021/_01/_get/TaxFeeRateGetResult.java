
package com.appliedsystems.schemas.epic.sdk._2021._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ArrayOfTaxFeeRate;


/**
 * <p>Java class for TaxFeeRateGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxFeeRateGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxFeeRates" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/}ArrayOfTaxFeeRate" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFeeRateGetResult", propOrder = {
    "taxFeeRates",
    "totalPages"
})
public class TaxFeeRateGetResult {

    @XmlElement(name = "TaxFeeRates", nillable = true)
    protected ArrayOfTaxFeeRate taxFeeRates;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the taxFeeRates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTaxFeeRate }
     *     
     */
    public ArrayOfTaxFeeRate getTaxFeeRates() {
        return taxFeeRates;
    }

    /**
     * Sets the value of the taxFeeRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTaxFeeRate }
     *     
     */
    public void setTaxFeeRates(ArrayOfTaxFeeRate value) {
        this.taxFeeRates = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
