
package com.appliedsystems.schemas.epic.sdk._2011._01._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfActivity_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", "ArrayOfActivity");
    private final static QName _ArrayOfAdjustCommission_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", "ArrayOfAdjustCommission");
    private final static QName _Activity_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", "Activity");
    private final static QName _AdjustCommission_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", "AdjustCommission");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link ArrayOfActivity }
     * 
     */
    public ArrayOfActivity createArrayOfActivity() {
        return new ArrayOfActivity();
    }

    /**
     * Create an instance of {@link ArrayOfAdjustCommission }
     * 
     */
    public ArrayOfAdjustCommission createArrayOfAdjustCommission() {
        return new ArrayOfAdjustCommission();
    }

    /**
     * Create an instance of {@link AdjustCommission }
     * 
     */
    public AdjustCommission createAdjustCommission() {
        return new AdjustCommission();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfActivity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", name = "ArrayOfActivity")
    public JAXBElement<ArrayOfActivity> createArrayOfActivity(ArrayOfActivity value) {
        return new JAXBElement<ArrayOfActivity>(_ArrayOfActivity_QNAME, ArrayOfActivity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAdjustCommission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", name = "ArrayOfAdjustCommission")
    public JAXBElement<ArrayOfAdjustCommission> createArrayOfAdjustCommission(ArrayOfAdjustCommission value) {
        return new JAXBElement<ArrayOfAdjustCommission>(_ArrayOfAdjustCommission_QNAME, ArrayOfAdjustCommission.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Activity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", name = "Activity")
    public JAXBElement<Activity> createActivity(Activity value) {
        return new JAXBElement<Activity>(_Activity_QNAME, Activity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjustCommission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/", name = "AdjustCommission")
    public JAXBElement<AdjustCommission> createAdjustCommission(AdjustCommission value) {
        return new JAXBElement<AdjustCommission>(_AdjustCommission_QNAME, AdjustCommission.class, null, value);
    }

}
