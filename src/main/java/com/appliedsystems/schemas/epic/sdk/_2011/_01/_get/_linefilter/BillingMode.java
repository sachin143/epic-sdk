
package com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillingMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Agency"/>
 *     &lt;enumeration value="Direct"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillingMode", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_linefilter/")
@XmlEnum
public enum BillingMode {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Agency")
    AGENCY("Agency"),
    @XmlEnumValue("Direct")
    DIRECT("Direct");
    private final String value;

    BillingMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BillingMode fromValue(String v) {
        for (BillingMode c: BillingMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
