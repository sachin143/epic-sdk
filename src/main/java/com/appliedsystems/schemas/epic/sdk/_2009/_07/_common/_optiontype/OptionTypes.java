
package com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OptionTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OptionTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ClientFormatOptions"/>
 *     &lt;enumeration value="ClientInActivateOptions"/>
 *     &lt;enumeration value="ContactCategoryOptions"/>
 *     &lt;enumeration value="ContactBusinessIndividualOptions"/>
 *     &lt;enumeration value="PolicyBillingModeOptions"/>
 *     &lt;enumeration value="LineBillingModeOptions"/>
 *     &lt;enumeration value="BusinessOwnersApplicantPolicyOptions"/>
 *     &lt;enumeration value="BusinessOwnersPremisesBuildingImprovementsCommunitySpecifiedOptions"/>
 *     &lt;enumeration value="CommercialApplicationLossHistoryOpenClosedOptions"/>
 *     &lt;enumeration value="CommercialApplicationPriorCarrierGeneralLiabilityClaimsMadeOccurrenceOptions"/>
 *     &lt;enumeration value="CommercialUmbrellaPolicyInformationAndEmployeeBenefitsUmbrellaExcessOptions"/>
 *     &lt;enumeration value="CommercialUmbrellaPolicyInformationAndEmployeeBenefitsNewRenewalOptions"/>
 *     &lt;enumeration value="CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceOptions"/>
 *     &lt;enumeration value="CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceVoluntaryOptions"/>
 *     &lt;enumeration value="GeneralLiabilityCoveragesClaimsMadeOccurrenceOptions"/>
 *     &lt;enumeration value="PersonalApplicationApplicantNewRenewalOptions"/>
 *     &lt;enumeration value="PersonalAutoPolicyInformationResidenceIsOwnedRentedOptions"/>
 *     &lt;enumeration value="PersonalAutoVehicleNewUsedVehicleOptions"/>
 *     &lt;enumeration value="PropertyBusinessIncomeExtraExpenseRentalValueCoveragesPremiseInformationOptions"/>
 *     &lt;enumeration value="PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyCoveragesDependentPropertyFormsOptions"/>
 *     &lt;enumeration value="PropertySubjectOfInsuranceInflationGuardPercentagePerOptions"/>
 *     &lt;enumeration value="PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyNameAndAddressForOptions"/>
 *     &lt;enumeration value="PropertyStatementOfValuesRateRequestedOptions"/>
 *     &lt;enumeration value="ResidentialSolidFuelQuestionnaireStovePipeQuestion3Options"/>
 *     &lt;enumeration value="ActivityStatusOptions"/>
 *     &lt;enumeration value="ClaimSummaryPolicyNoticeOfOptions"/>
 *     &lt;enumeration value="TransactionAttachToOptions"/>
 *     &lt;enumeration value="TransactionBillingModeOptions"/>
 *     &lt;enumeration value="TransactionBillNumberOptions"/>
 *     &lt;enumeration value="TransactionSendInvoiceToOptions"/>
 *     &lt;enumeration value="MultiCarrierSchedulePolicyLineOptions"/>
 *     &lt;enumeration value="PolicyActionCancelOptions"/>
 *     &lt;enumeration value="PolicyActionIssueNotIssuePolicyOptions"/>
 *     &lt;enumeration value="PolicyActionIssueNotIssueEndorsementOptions"/>
 *     &lt;enumeration value="GeneralLedgerActionTransferOfFundsFromTypeOptions"/>
 *     &lt;enumeration value="GeneralLedgerActionTransferOfFundsToTypeOptions"/>
 *     &lt;enumeration value="GeneralLedgerNewDefaultEntryOptions"/>
 *     &lt;enumeration value="ReceiptDetailItemDebitCreditOptions"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToSelectedItemsApplyCreditsToDebitsApplyRemainingBalanceToOptions"/>
 *     &lt;enumeration value="DisbursementVoidRelatedVouchersOptions"/>
 *     &lt;enumeration value="DisbursementDetailItemDebitCreditOptions"/>
 *     &lt;enumeration value="VoucherDetailItemDebitCreditOptions"/>
 *     &lt;enumeration value="JournalEntryDetailItemDebitCreditOptions"/>
 *     &lt;enumeration value="CustomFieldDataType"/>
 *     &lt;enumeration value="TransactionActionFinanceTransactionPercentageFlatAmountOptions"/>
 *     &lt;enumeration value="TransactionActionApplyCreditsToDebitsMonthOptions"/>
 *     &lt;enumeration value="PersonalApplicationApplicantCurrentAddressOwnedRentedOptions"/>
 *     &lt;enumeration value="AccountAllStructuresSelectedOptions"/>
 *     &lt;enumeration value="BrokerAccountingInternalAgencyExternalBrokerOptions"/>
 *     &lt;enumeration value="EmployeePersonnelFullTimePartTimeOptions"/>
 *     &lt;enumeration value="CertificateClaimsMadeOccurrenceOptions"/>
 *     &lt;enumeration value="CertificateExcessUmbrellaLiabilityOptions"/>
 *     &lt;enumeration value="CertificateDeductibleRetentionOptions"/>
 *     &lt;enumeration value="CertificateAnyProprietorPartnerExecutiveOfficerMemberExcludedOptions"/>
 *     &lt;enumeration value="CommissionCriteriaAllSelectedOptions"/>
 *     &lt;enumeration value="PremiumIndividualPolicyBookOfBusinessOptions"/>
 *     &lt;enumeration value="CommissionCompanyTypeBillingIssuingBothOptions"/>
 *     &lt;enumeration value="CommissionBillModeAgencyDirectBothOptions"/>
 *     &lt;enumeration value="WorkersCompensationParticipatingNonParticipatingOptions"/>
 *     &lt;enumeration value="WorkersCompensationIncludedExcludedOptions"/>
 *     &lt;enumeration value="WatercraftDayEveningOptions"/>
 *     &lt;enumeration value="WatercraftSummerWinterOptions"/>
 *     &lt;enumeration value="WatercraftDryAfloatOptions"/>
 *     &lt;enumeration value="TransactionBasicInstallmentOptions"/>
 *     &lt;enumeration value="TransactionUpdateInstallmentsOptions"/>
 *     &lt;enumeration value="TransactionActionRevisePremiumInstallmentsOptions"/>
 *     &lt;enumeration value="TransactionActionRevisePremiumReallocateOptions"/>
 *     &lt;enumeration value="CompanyBillingBillModeOptions"/>
 *     &lt;enumeration value="EquipmentFloaterCoverageBlanketAmountScheduledOptions"/>
 *     &lt;enumeration value="EquipmentFloaterCoverageAllRisksNamedPerilsNamedPerilsIncludingTheftOptions"/>
 *     &lt;enumeration value="EquipmentFloaterCoverageActualCashValueReplacementCostOptions"/>
 *     &lt;enumeration value="EquipmentFloaterEquipmentNewUsedOptions"/>
 *     &lt;enumeration value="GarageAndDealersBusinessAndDealersPhysicalDamageOptions"/>
 *     &lt;enumeration value="GarageAndDealersBusinessAndDealersFranchisedOptions"/>
 *     &lt;enumeration value="GarageAndDealersCoverageAntitheftAppliesOptions"/>
 *     &lt;enumeration value="GarageAndDealersPhysicalDamageGarageKeepersOptions"/>
 *     &lt;enumeration value="CanadianEnglishFrenchLanguageOptions"/>
 *     &lt;enumeration value="PaymentAuthorizationNewRequestChangeExistingInfoOptions"/>
 *     &lt;enumeration value="PaymentAuthorizationMethodOfPaymentOptions"/>
 *     &lt;enumeration value="CanadianHabitationalLocationAreaSquareFeetSquareMeterOptions"/>
 *     &lt;enumeration value="CanadianHabitationalHeightFeetMeterOptions"/>
 *     &lt;enumeration value="VehicleOwnOrLeasedOptions"/>
 *     &lt;enumeration value="CanadianHabitationalMetalChimneyClearanceInchesCentimetresOption"/>
 *     &lt;enumeration value="CanadianHabitationalClearanceMeasurementInchesCentimetresOption"/>
 *     &lt;enumeration value="CanadianHeatingUnitPrimaryAuxiliaryOption"/>
 *     &lt;enumeration value="ReconciliationRecordReconcileCommissionsOptions"/>
 *     &lt;enumeration value="ReconciliationCompanyBrokerOptions"/>
 *     &lt;enumeration value="ReconciliationAllSelectedOptions"/>
 *     &lt;enumeration value="ReconciliationBrokerExternalInternalBothOptions"/>
 *     &lt;enumeration value="MarketedPolicyOpenClosedOptions"/>
 *     &lt;enumeration value="MarketedPolicyUpdateMarketingAttachmentAddReplaceRemoveOptions"/>
 *     &lt;enumeration value="MarketedPolicyCreateCarrierSubmissionSubmitToICOorPPEOptions"/>
 *     &lt;enumeration value="MarketedPolicyMoveMarketedLinesToCurrentPolicyAddRenewUpdatePolicyOptions"/>
 *     &lt;enumeration value="MarketedPolicyMoveMarketedLinesToCurrentPolicyUpdateStageToOptions"/>
 *     &lt;enumeration value="MarketedPolicyMoveMarketedLinesToCurrentPolicyLineAddDoNotAddToPolicyOptions"/>
 *     &lt;enumeration value="ActivitiesTasksViewOptions"/>
 *     &lt;enumeration value="BoilerAndMachineryStatementOfValuesRateRequestedOptions"/>
 *     &lt;enumeration value="ElectronicDataProcessingScheduleOfEquipmentOwnedLeasedOptions"/>
 *     &lt;enumeration value="TransportationInterestTypeOptions"/>
 *     &lt;enumeration value="IdentificationNumberStatusActiveInactiveOptions"/>
 *     &lt;enumeration value="RisksInsuredTotalEligibleOptions"/>
 *     &lt;enumeration value="CertificatePolicyFormIsISOOtherOption"/>
 *     &lt;enumeration value="MilesKilometersOptions"/>
 *     &lt;enumeration value="ClientContractsServiceAttachToOptions"/>
 *     &lt;enumeration value="ConfidentialClientAccessAllSelectedOptions"/>
 *     &lt;enumeration value="OpportunityBusinessTypeOption"/>
 *     &lt;enumeration value="CanadianHabitationalLossHistoryStatusOptions"/>
 *     &lt;enumeration value="ReconciliationRecordCommissionPremiumOptions"/>
 *     &lt;enumeration value="ReconciliationRecordPolicyLineOptions"/>
 *     &lt;enumeration value="TransactionInvoicePaymentOptions"/>
 *     &lt;enumeration value="ViewOthersOpportunityTypeOptions"/>
 *     &lt;enumeration value="ReceiptDetailItemCashOnAccountOptions"/>
 *     &lt;enumeration value="CheckingSavingsOptions"/>
 *     &lt;enumeration value="PercentageAmountOptions"/>
 *     &lt;enumeration value="ChartOfAccountsLevelOptions"/>
 *     &lt;enumeration value="ActivityWhoOwnerOptions"/>
 *     &lt;enumeration value="CertificateHoldersContactTypeOptions"/>
 *     &lt;enumeration value="CertificateHoldersEmailAddressOptions"/>
 *     &lt;enumeration value="AdjustCommissionUpdateInstallmentsOptions"/>
 *     &lt;enumeration value="DateBasedCommissionYearsMonthsOptions"/>
 *     &lt;enumeration value="DefaultActivityAsOptions"/>
 *     &lt;enumeration value="RevenueIndividualPolicyBookOfBusinessClientAccountOptions"/>
 *     &lt;enumeration value="AllocationMethodAutomaticBreakdownOptions"/>
 *     &lt;enumeration value="ProducerBrokerCommissionTermOptions"/>
 *     &lt;enumeration value="TruckersStateFederalFilingActionLiabilityOptions"/>
 *     &lt;enumeration value="TruckersStateFederalFilingActionCargoOptions"/>
 *     &lt;enumeration value="TaxFeeRatesAllSelectedOptions"/>
 *     &lt;enumeration value="TaxFeeRatesIncludeExcludeOptions"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OptionTypes", namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_optiontype/")
@XmlEnum
public enum OptionTypes {

    @XmlEnumValue("ClientFormatOptions")
    CLIENT_FORMAT_OPTIONS("ClientFormatOptions"),
    @XmlEnumValue("ClientInActivateOptions")
    CLIENT_IN_ACTIVATE_OPTIONS("ClientInActivateOptions"),
    @XmlEnumValue("ContactCategoryOptions")
    CONTACT_CATEGORY_OPTIONS("ContactCategoryOptions"),
    @XmlEnumValue("ContactBusinessIndividualOptions")
    CONTACT_BUSINESS_INDIVIDUAL_OPTIONS("ContactBusinessIndividualOptions"),
    @XmlEnumValue("PolicyBillingModeOptions")
    POLICY_BILLING_MODE_OPTIONS("PolicyBillingModeOptions"),
    @XmlEnumValue("LineBillingModeOptions")
    LINE_BILLING_MODE_OPTIONS("LineBillingModeOptions"),
    @XmlEnumValue("BusinessOwnersApplicantPolicyOptions")
    BUSINESS_OWNERS_APPLICANT_POLICY_OPTIONS("BusinessOwnersApplicantPolicyOptions"),
    @XmlEnumValue("BusinessOwnersPremisesBuildingImprovementsCommunitySpecifiedOptions")
    BUSINESS_OWNERS_PREMISES_BUILDING_IMPROVEMENTS_COMMUNITY_SPECIFIED_OPTIONS("BusinessOwnersPremisesBuildingImprovementsCommunitySpecifiedOptions"),
    @XmlEnumValue("CommercialApplicationLossHistoryOpenClosedOptions")
    COMMERCIAL_APPLICATION_LOSS_HISTORY_OPEN_CLOSED_OPTIONS("CommercialApplicationLossHistoryOpenClosedOptions"),
    @XmlEnumValue("CommercialApplicationPriorCarrierGeneralLiabilityClaimsMadeOccurrenceOptions")
    COMMERCIAL_APPLICATION_PRIOR_CARRIER_GENERAL_LIABILITY_CLAIMS_MADE_OCCURRENCE_OPTIONS("CommercialApplicationPriorCarrierGeneralLiabilityClaimsMadeOccurrenceOptions"),
    @XmlEnumValue("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsUmbrellaExcessOptions")
    COMMERCIAL_UMBRELLA_POLICY_INFORMATION_AND_EMPLOYEE_BENEFITS_UMBRELLA_EXCESS_OPTIONS("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsUmbrellaExcessOptions"),
    @XmlEnumValue("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsNewRenewalOptions")
    COMMERCIAL_UMBRELLA_POLICY_INFORMATION_AND_EMPLOYEE_BENEFITS_NEW_RENEWAL_OPTIONS("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsNewRenewalOptions"),
    @XmlEnumValue("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceOptions")
    COMMERCIAL_UMBRELLA_POLICY_INFORMATION_AND_EMPLOYEE_BENEFITS_CLAIMS_MADE_OCCURRENCE_OPTIONS("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceOptions"),
    @XmlEnumValue("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceVoluntaryOptions")
    COMMERCIAL_UMBRELLA_POLICY_INFORMATION_AND_EMPLOYEE_BENEFITS_CLAIMS_MADE_OCCURRENCE_VOLUNTARY_OPTIONS("CommercialUmbrellaPolicyInformationAndEmployeeBenefitsClaimsMadeOccurrenceVoluntaryOptions"),
    @XmlEnumValue("GeneralLiabilityCoveragesClaimsMadeOccurrenceOptions")
    GENERAL_LIABILITY_COVERAGES_CLAIMS_MADE_OCCURRENCE_OPTIONS("GeneralLiabilityCoveragesClaimsMadeOccurrenceOptions"),
    @XmlEnumValue("PersonalApplicationApplicantNewRenewalOptions")
    PERSONAL_APPLICATION_APPLICANT_NEW_RENEWAL_OPTIONS("PersonalApplicationApplicantNewRenewalOptions"),
    @XmlEnumValue("PersonalAutoPolicyInformationResidenceIsOwnedRentedOptions")
    PERSONAL_AUTO_POLICY_INFORMATION_RESIDENCE_IS_OWNED_RENTED_OPTIONS("PersonalAutoPolicyInformationResidenceIsOwnedRentedOptions"),
    @XmlEnumValue("PersonalAutoVehicleNewUsedVehicleOptions")
    PERSONAL_AUTO_VEHICLE_NEW_USED_VEHICLE_OPTIONS("PersonalAutoVehicleNewUsedVehicleOptions"),
    @XmlEnumValue("PropertyBusinessIncomeExtraExpenseRentalValueCoveragesPremiseInformationOptions")
    PROPERTY_BUSINESS_INCOME_EXTRA_EXPENSE_RENTAL_VALUE_COVERAGES_PREMISE_INFORMATION_OPTIONS("PropertyBusinessIncomeExtraExpenseRentalValueCoveragesPremiseInformationOptions"),
    @XmlEnumValue("PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyCoveragesDependentPropertyFormsOptions")
    PROPERTY_BUSINESS_INCOME_EXTRA_EXPENSE_RENTAL_VALUE_OFF_PREMISE_POWER_DEPENDENT_PROPERTY_COVERAGES_DEPENDENT_PROPERTY_FORMS_OPTIONS("PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyCoveragesDependentPropertyFormsOptions"),
    @XmlEnumValue("PropertySubjectOfInsuranceInflationGuardPercentagePerOptions")
    PROPERTY_SUBJECT_OF_INSURANCE_INFLATION_GUARD_PERCENTAGE_PER_OPTIONS("PropertySubjectOfInsuranceInflationGuardPercentagePerOptions"),
    @XmlEnumValue("PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyNameAndAddressForOptions")
    PROPERTY_BUSINESS_INCOME_EXTRA_EXPENSE_RENTAL_VALUE_OFF_PREMISE_POWER_DEPENDENT_PROPERTY_NAME_AND_ADDRESS_FOR_OPTIONS("PropertyBusinessIncomeExtraExpenseRentalValueOffPremisePowerDependentPropertyNameAndAddressForOptions"),
    @XmlEnumValue("PropertyStatementOfValuesRateRequestedOptions")
    PROPERTY_STATEMENT_OF_VALUES_RATE_REQUESTED_OPTIONS("PropertyStatementOfValuesRateRequestedOptions"),
    @XmlEnumValue("ResidentialSolidFuelQuestionnaireStovePipeQuestion3Options")
    RESIDENTIAL_SOLID_FUEL_QUESTIONNAIRE_STOVE_PIPE_QUESTION_3_OPTIONS("ResidentialSolidFuelQuestionnaireStovePipeQuestion3Options"),
    @XmlEnumValue("ActivityStatusOptions")
    ACTIVITY_STATUS_OPTIONS("ActivityStatusOptions"),
    @XmlEnumValue("ClaimSummaryPolicyNoticeOfOptions")
    CLAIM_SUMMARY_POLICY_NOTICE_OF_OPTIONS("ClaimSummaryPolicyNoticeOfOptions"),
    @XmlEnumValue("TransactionAttachToOptions")
    TRANSACTION_ATTACH_TO_OPTIONS("TransactionAttachToOptions"),
    @XmlEnumValue("TransactionBillingModeOptions")
    TRANSACTION_BILLING_MODE_OPTIONS("TransactionBillingModeOptions"),
    @XmlEnumValue("TransactionBillNumberOptions")
    TRANSACTION_BILL_NUMBER_OPTIONS("TransactionBillNumberOptions"),
    @XmlEnumValue("TransactionSendInvoiceToOptions")
    TRANSACTION_SEND_INVOICE_TO_OPTIONS("TransactionSendInvoiceToOptions"),
    @XmlEnumValue("MultiCarrierSchedulePolicyLineOptions")
    MULTI_CARRIER_SCHEDULE_POLICY_LINE_OPTIONS("MultiCarrierSchedulePolicyLineOptions"),
    @XmlEnumValue("PolicyActionCancelOptions")
    POLICY_ACTION_CANCEL_OPTIONS("PolicyActionCancelOptions"),
    @XmlEnumValue("PolicyActionIssueNotIssuePolicyOptions")
    POLICY_ACTION_ISSUE_NOT_ISSUE_POLICY_OPTIONS("PolicyActionIssueNotIssuePolicyOptions"),
    @XmlEnumValue("PolicyActionIssueNotIssueEndorsementOptions")
    POLICY_ACTION_ISSUE_NOT_ISSUE_ENDORSEMENT_OPTIONS("PolicyActionIssueNotIssueEndorsementOptions"),
    @XmlEnumValue("GeneralLedgerActionTransferOfFundsFromTypeOptions")
    GENERAL_LEDGER_ACTION_TRANSFER_OF_FUNDS_FROM_TYPE_OPTIONS("GeneralLedgerActionTransferOfFundsFromTypeOptions"),
    @XmlEnumValue("GeneralLedgerActionTransferOfFundsToTypeOptions")
    GENERAL_LEDGER_ACTION_TRANSFER_OF_FUNDS_TO_TYPE_OPTIONS("GeneralLedgerActionTransferOfFundsToTypeOptions"),
    @XmlEnumValue("GeneralLedgerNewDefaultEntryOptions")
    GENERAL_LEDGER_NEW_DEFAULT_ENTRY_OPTIONS("GeneralLedgerNewDefaultEntryOptions"),
    @XmlEnumValue("ReceiptDetailItemDebitCreditOptions")
    RECEIPT_DETAIL_ITEM_DEBIT_CREDIT_OPTIONS("ReceiptDetailItemDebitCreditOptions"),
    @XmlEnumValue("ReceiptDetailItemApplyToSelectedItemsApplyCreditsToDebitsApplyRemainingBalanceToOptions")
    RECEIPT_DETAIL_ITEM_APPLY_TO_SELECTED_ITEMS_APPLY_CREDITS_TO_DEBITS_APPLY_REMAINING_BALANCE_TO_OPTIONS("ReceiptDetailItemApplyToSelectedItemsApplyCreditsToDebitsApplyRemainingBalanceToOptions"),
    @XmlEnumValue("DisbursementVoidRelatedVouchersOptions")
    DISBURSEMENT_VOID_RELATED_VOUCHERS_OPTIONS("DisbursementVoidRelatedVouchersOptions"),
    @XmlEnumValue("DisbursementDetailItemDebitCreditOptions")
    DISBURSEMENT_DETAIL_ITEM_DEBIT_CREDIT_OPTIONS("DisbursementDetailItemDebitCreditOptions"),
    @XmlEnumValue("VoucherDetailItemDebitCreditOptions")
    VOUCHER_DETAIL_ITEM_DEBIT_CREDIT_OPTIONS("VoucherDetailItemDebitCreditOptions"),
    @XmlEnumValue("JournalEntryDetailItemDebitCreditOptions")
    JOURNAL_ENTRY_DETAIL_ITEM_DEBIT_CREDIT_OPTIONS("JournalEntryDetailItemDebitCreditOptions"),
    @XmlEnumValue("CustomFieldDataType")
    CUSTOM_FIELD_DATA_TYPE("CustomFieldDataType"),
    @XmlEnumValue("TransactionActionFinanceTransactionPercentageFlatAmountOptions")
    TRANSACTION_ACTION_FINANCE_TRANSACTION_PERCENTAGE_FLAT_AMOUNT_OPTIONS("TransactionActionFinanceTransactionPercentageFlatAmountOptions"),
    @XmlEnumValue("TransactionActionApplyCreditsToDebitsMonthOptions")
    TRANSACTION_ACTION_APPLY_CREDITS_TO_DEBITS_MONTH_OPTIONS("TransactionActionApplyCreditsToDebitsMonthOptions"),
    @XmlEnumValue("PersonalApplicationApplicantCurrentAddressOwnedRentedOptions")
    PERSONAL_APPLICATION_APPLICANT_CURRENT_ADDRESS_OWNED_RENTED_OPTIONS("PersonalApplicationApplicantCurrentAddressOwnedRentedOptions"),
    @XmlEnumValue("AccountAllStructuresSelectedOptions")
    ACCOUNT_ALL_STRUCTURES_SELECTED_OPTIONS("AccountAllStructuresSelectedOptions"),
    @XmlEnumValue("BrokerAccountingInternalAgencyExternalBrokerOptions")
    BROKER_ACCOUNTING_INTERNAL_AGENCY_EXTERNAL_BROKER_OPTIONS("BrokerAccountingInternalAgencyExternalBrokerOptions"),
    @XmlEnumValue("EmployeePersonnelFullTimePartTimeOptions")
    EMPLOYEE_PERSONNEL_FULL_TIME_PART_TIME_OPTIONS("EmployeePersonnelFullTimePartTimeOptions"),
    @XmlEnumValue("CertificateClaimsMadeOccurrenceOptions")
    CERTIFICATE_CLAIMS_MADE_OCCURRENCE_OPTIONS("CertificateClaimsMadeOccurrenceOptions"),
    @XmlEnumValue("CertificateExcessUmbrellaLiabilityOptions")
    CERTIFICATE_EXCESS_UMBRELLA_LIABILITY_OPTIONS("CertificateExcessUmbrellaLiabilityOptions"),
    @XmlEnumValue("CertificateDeductibleRetentionOptions")
    CERTIFICATE_DEDUCTIBLE_RETENTION_OPTIONS("CertificateDeductibleRetentionOptions"),
    @XmlEnumValue("CertificateAnyProprietorPartnerExecutiveOfficerMemberExcludedOptions")
    CERTIFICATE_ANY_PROPRIETOR_PARTNER_EXECUTIVE_OFFICER_MEMBER_EXCLUDED_OPTIONS("CertificateAnyProprietorPartnerExecutiveOfficerMemberExcludedOptions"),
    @XmlEnumValue("CommissionCriteriaAllSelectedOptions")
    COMMISSION_CRITERIA_ALL_SELECTED_OPTIONS("CommissionCriteriaAllSelectedOptions"),
    @XmlEnumValue("PremiumIndividualPolicyBookOfBusinessOptions")
    PREMIUM_INDIVIDUAL_POLICY_BOOK_OF_BUSINESS_OPTIONS("PremiumIndividualPolicyBookOfBusinessOptions"),
    @XmlEnumValue("CommissionCompanyTypeBillingIssuingBothOptions")
    COMMISSION_COMPANY_TYPE_BILLING_ISSUING_BOTH_OPTIONS("CommissionCompanyTypeBillingIssuingBothOptions"),
    @XmlEnumValue("CommissionBillModeAgencyDirectBothOptions")
    COMMISSION_BILL_MODE_AGENCY_DIRECT_BOTH_OPTIONS("CommissionBillModeAgencyDirectBothOptions"),
    @XmlEnumValue("WorkersCompensationParticipatingNonParticipatingOptions")
    WORKERS_COMPENSATION_PARTICIPATING_NON_PARTICIPATING_OPTIONS("WorkersCompensationParticipatingNonParticipatingOptions"),
    @XmlEnumValue("WorkersCompensationIncludedExcludedOptions")
    WORKERS_COMPENSATION_INCLUDED_EXCLUDED_OPTIONS("WorkersCompensationIncludedExcludedOptions"),
    @XmlEnumValue("WatercraftDayEveningOptions")
    WATERCRAFT_DAY_EVENING_OPTIONS("WatercraftDayEveningOptions"),
    @XmlEnumValue("WatercraftSummerWinterOptions")
    WATERCRAFT_SUMMER_WINTER_OPTIONS("WatercraftSummerWinterOptions"),
    @XmlEnumValue("WatercraftDryAfloatOptions")
    WATERCRAFT_DRY_AFLOAT_OPTIONS("WatercraftDryAfloatOptions"),
    @XmlEnumValue("TransactionBasicInstallmentOptions")
    TRANSACTION_BASIC_INSTALLMENT_OPTIONS("TransactionBasicInstallmentOptions"),
    @XmlEnumValue("TransactionUpdateInstallmentsOptions")
    TRANSACTION_UPDATE_INSTALLMENTS_OPTIONS("TransactionUpdateInstallmentsOptions"),
    @XmlEnumValue("TransactionActionRevisePremiumInstallmentsOptions")
    TRANSACTION_ACTION_REVISE_PREMIUM_INSTALLMENTS_OPTIONS("TransactionActionRevisePremiumInstallmentsOptions"),
    @XmlEnumValue("TransactionActionRevisePremiumReallocateOptions")
    TRANSACTION_ACTION_REVISE_PREMIUM_REALLOCATE_OPTIONS("TransactionActionRevisePremiumReallocateOptions"),
    @XmlEnumValue("CompanyBillingBillModeOptions")
    COMPANY_BILLING_BILL_MODE_OPTIONS("CompanyBillingBillModeOptions"),
    @XmlEnumValue("EquipmentFloaterCoverageBlanketAmountScheduledOptions")
    EQUIPMENT_FLOATER_COVERAGE_BLANKET_AMOUNT_SCHEDULED_OPTIONS("EquipmentFloaterCoverageBlanketAmountScheduledOptions"),
    @XmlEnumValue("EquipmentFloaterCoverageAllRisksNamedPerilsNamedPerilsIncludingTheftOptions")
    EQUIPMENT_FLOATER_COVERAGE_ALL_RISKS_NAMED_PERILS_NAMED_PERILS_INCLUDING_THEFT_OPTIONS("EquipmentFloaterCoverageAllRisksNamedPerilsNamedPerilsIncludingTheftOptions"),
    @XmlEnumValue("EquipmentFloaterCoverageActualCashValueReplacementCostOptions")
    EQUIPMENT_FLOATER_COVERAGE_ACTUAL_CASH_VALUE_REPLACEMENT_COST_OPTIONS("EquipmentFloaterCoverageActualCashValueReplacementCostOptions"),
    @XmlEnumValue("EquipmentFloaterEquipmentNewUsedOptions")
    EQUIPMENT_FLOATER_EQUIPMENT_NEW_USED_OPTIONS("EquipmentFloaterEquipmentNewUsedOptions"),
    @XmlEnumValue("GarageAndDealersBusinessAndDealersPhysicalDamageOptions")
    GARAGE_AND_DEALERS_BUSINESS_AND_DEALERS_PHYSICAL_DAMAGE_OPTIONS("GarageAndDealersBusinessAndDealersPhysicalDamageOptions"),
    @XmlEnumValue("GarageAndDealersBusinessAndDealersFranchisedOptions")
    GARAGE_AND_DEALERS_BUSINESS_AND_DEALERS_FRANCHISED_OPTIONS("GarageAndDealersBusinessAndDealersFranchisedOptions"),
    @XmlEnumValue("GarageAndDealersCoverageAntitheftAppliesOptions")
    GARAGE_AND_DEALERS_COVERAGE_ANTITHEFT_APPLIES_OPTIONS("GarageAndDealersCoverageAntitheftAppliesOptions"),
    @XmlEnumValue("GarageAndDealersPhysicalDamageGarageKeepersOptions")
    GARAGE_AND_DEALERS_PHYSICAL_DAMAGE_GARAGE_KEEPERS_OPTIONS("GarageAndDealersPhysicalDamageGarageKeepersOptions"),
    @XmlEnumValue("CanadianEnglishFrenchLanguageOptions")
    CANADIAN_ENGLISH_FRENCH_LANGUAGE_OPTIONS("CanadianEnglishFrenchLanguageOptions"),
    @XmlEnumValue("PaymentAuthorizationNewRequestChangeExistingInfoOptions")
    PAYMENT_AUTHORIZATION_NEW_REQUEST_CHANGE_EXISTING_INFO_OPTIONS("PaymentAuthorizationNewRequestChangeExistingInfoOptions"),
    @XmlEnumValue("PaymentAuthorizationMethodOfPaymentOptions")
    PAYMENT_AUTHORIZATION_METHOD_OF_PAYMENT_OPTIONS("PaymentAuthorizationMethodOfPaymentOptions"),
    @XmlEnumValue("CanadianHabitationalLocationAreaSquareFeetSquareMeterOptions")
    CANADIAN_HABITATIONAL_LOCATION_AREA_SQUARE_FEET_SQUARE_METER_OPTIONS("CanadianHabitationalLocationAreaSquareFeetSquareMeterOptions"),
    @XmlEnumValue("CanadianHabitationalHeightFeetMeterOptions")
    CANADIAN_HABITATIONAL_HEIGHT_FEET_METER_OPTIONS("CanadianHabitationalHeightFeetMeterOptions"),
    @XmlEnumValue("VehicleOwnOrLeasedOptions")
    VEHICLE_OWN_OR_LEASED_OPTIONS("VehicleOwnOrLeasedOptions"),
    @XmlEnumValue("CanadianHabitationalMetalChimneyClearanceInchesCentimetresOption")
    CANADIAN_HABITATIONAL_METAL_CHIMNEY_CLEARANCE_INCHES_CENTIMETRES_OPTION("CanadianHabitationalMetalChimneyClearanceInchesCentimetresOption"),
    @XmlEnumValue("CanadianHabitationalClearanceMeasurementInchesCentimetresOption")
    CANADIAN_HABITATIONAL_CLEARANCE_MEASUREMENT_INCHES_CENTIMETRES_OPTION("CanadianHabitationalClearanceMeasurementInchesCentimetresOption"),
    @XmlEnumValue("CanadianHeatingUnitPrimaryAuxiliaryOption")
    CANADIAN_HEATING_UNIT_PRIMARY_AUXILIARY_OPTION("CanadianHeatingUnitPrimaryAuxiliaryOption"),
    @XmlEnumValue("ReconciliationRecordReconcileCommissionsOptions")
    RECONCILIATION_RECORD_RECONCILE_COMMISSIONS_OPTIONS("ReconciliationRecordReconcileCommissionsOptions"),
    @XmlEnumValue("ReconciliationCompanyBrokerOptions")
    RECONCILIATION_COMPANY_BROKER_OPTIONS("ReconciliationCompanyBrokerOptions"),
    @XmlEnumValue("ReconciliationAllSelectedOptions")
    RECONCILIATION_ALL_SELECTED_OPTIONS("ReconciliationAllSelectedOptions"),
    @XmlEnumValue("ReconciliationBrokerExternalInternalBothOptions")
    RECONCILIATION_BROKER_EXTERNAL_INTERNAL_BOTH_OPTIONS("ReconciliationBrokerExternalInternalBothOptions"),
    @XmlEnumValue("MarketedPolicyOpenClosedOptions")
    MARKETED_POLICY_OPEN_CLOSED_OPTIONS("MarketedPolicyOpenClosedOptions"),
    @XmlEnumValue("MarketedPolicyUpdateMarketingAttachmentAddReplaceRemoveOptions")
    MARKETED_POLICY_UPDATE_MARKETING_ATTACHMENT_ADD_REPLACE_REMOVE_OPTIONS("MarketedPolicyUpdateMarketingAttachmentAddReplaceRemoveOptions"),
    @XmlEnumValue("MarketedPolicyCreateCarrierSubmissionSubmitToICOorPPEOptions")
    MARKETED_POLICY_CREATE_CARRIER_SUBMISSION_SUBMIT_TO_IC_OOR_PPE_OPTIONS("MarketedPolicyCreateCarrierSubmissionSubmitToICOorPPEOptions"),
    @XmlEnumValue("MarketedPolicyMoveMarketedLinesToCurrentPolicyAddRenewUpdatePolicyOptions")
    MARKETED_POLICY_MOVE_MARKETED_LINES_TO_CURRENT_POLICY_ADD_RENEW_UPDATE_POLICY_OPTIONS("MarketedPolicyMoveMarketedLinesToCurrentPolicyAddRenewUpdatePolicyOptions"),
    @XmlEnumValue("MarketedPolicyMoveMarketedLinesToCurrentPolicyUpdateStageToOptions")
    MARKETED_POLICY_MOVE_MARKETED_LINES_TO_CURRENT_POLICY_UPDATE_STAGE_TO_OPTIONS("MarketedPolicyMoveMarketedLinesToCurrentPolicyUpdateStageToOptions"),
    @XmlEnumValue("MarketedPolicyMoveMarketedLinesToCurrentPolicyLineAddDoNotAddToPolicyOptions")
    MARKETED_POLICY_MOVE_MARKETED_LINES_TO_CURRENT_POLICY_LINE_ADD_DO_NOT_ADD_TO_POLICY_OPTIONS("MarketedPolicyMoveMarketedLinesToCurrentPolicyLineAddDoNotAddToPolicyOptions"),
    @XmlEnumValue("ActivitiesTasksViewOptions")
    ACTIVITIES_TASKS_VIEW_OPTIONS("ActivitiesTasksViewOptions"),
    @XmlEnumValue("BoilerAndMachineryStatementOfValuesRateRequestedOptions")
    BOILER_AND_MACHINERY_STATEMENT_OF_VALUES_RATE_REQUESTED_OPTIONS("BoilerAndMachineryStatementOfValuesRateRequestedOptions"),
    @XmlEnumValue("ElectronicDataProcessingScheduleOfEquipmentOwnedLeasedOptions")
    ELECTRONIC_DATA_PROCESSING_SCHEDULE_OF_EQUIPMENT_OWNED_LEASED_OPTIONS("ElectronicDataProcessingScheduleOfEquipmentOwnedLeasedOptions"),
    @XmlEnumValue("TransportationInterestTypeOptions")
    TRANSPORTATION_INTEREST_TYPE_OPTIONS("TransportationInterestTypeOptions"),
    @XmlEnumValue("IdentificationNumberStatusActiveInactiveOptions")
    IDENTIFICATION_NUMBER_STATUS_ACTIVE_INACTIVE_OPTIONS("IdentificationNumberStatusActiveInactiveOptions"),
    @XmlEnumValue("RisksInsuredTotalEligibleOptions")
    RISKS_INSURED_TOTAL_ELIGIBLE_OPTIONS("RisksInsuredTotalEligibleOptions"),
    @XmlEnumValue("CertificatePolicyFormIsISOOtherOption")
    CERTIFICATE_POLICY_FORM_IS_ISO_OTHER_OPTION("CertificatePolicyFormIsISOOtherOption"),
    @XmlEnumValue("MilesKilometersOptions")
    MILES_KILOMETERS_OPTIONS("MilesKilometersOptions"),
    @XmlEnumValue("ClientContractsServiceAttachToOptions")
    CLIENT_CONTRACTS_SERVICE_ATTACH_TO_OPTIONS("ClientContractsServiceAttachToOptions"),
    @XmlEnumValue("ConfidentialClientAccessAllSelectedOptions")
    CONFIDENTIAL_CLIENT_ACCESS_ALL_SELECTED_OPTIONS("ConfidentialClientAccessAllSelectedOptions"),
    @XmlEnumValue("OpportunityBusinessTypeOption")
    OPPORTUNITY_BUSINESS_TYPE_OPTION("OpportunityBusinessTypeOption"),
    @XmlEnumValue("CanadianHabitationalLossHistoryStatusOptions")
    CANADIAN_HABITATIONAL_LOSS_HISTORY_STATUS_OPTIONS("CanadianHabitationalLossHistoryStatusOptions"),
    @XmlEnumValue("ReconciliationRecordCommissionPremiumOptions")
    RECONCILIATION_RECORD_COMMISSION_PREMIUM_OPTIONS("ReconciliationRecordCommissionPremiumOptions"),
    @XmlEnumValue("ReconciliationRecordPolicyLineOptions")
    RECONCILIATION_RECORD_POLICY_LINE_OPTIONS("ReconciliationRecordPolicyLineOptions"),
    @XmlEnumValue("TransactionInvoicePaymentOptions")
    TRANSACTION_INVOICE_PAYMENT_OPTIONS("TransactionInvoicePaymentOptions"),
    @XmlEnumValue("ViewOthersOpportunityTypeOptions")
    VIEW_OTHERS_OPPORTUNITY_TYPE_OPTIONS("ViewOthersOpportunityTypeOptions"),
    @XmlEnumValue("ReceiptDetailItemCashOnAccountOptions")
    RECEIPT_DETAIL_ITEM_CASH_ON_ACCOUNT_OPTIONS("ReceiptDetailItemCashOnAccountOptions"),
    @XmlEnumValue("CheckingSavingsOptions")
    CHECKING_SAVINGS_OPTIONS("CheckingSavingsOptions"),
    @XmlEnumValue("PercentageAmountOptions")
    PERCENTAGE_AMOUNT_OPTIONS("PercentageAmountOptions"),
    @XmlEnumValue("ChartOfAccountsLevelOptions")
    CHART_OF_ACCOUNTS_LEVEL_OPTIONS("ChartOfAccountsLevelOptions"),
    @XmlEnumValue("ActivityWhoOwnerOptions")
    ACTIVITY_WHO_OWNER_OPTIONS("ActivityWhoOwnerOptions"),
    @XmlEnumValue("CertificateHoldersContactTypeOptions")
    CERTIFICATE_HOLDERS_CONTACT_TYPE_OPTIONS("CertificateHoldersContactTypeOptions"),
    @XmlEnumValue("CertificateHoldersEmailAddressOptions")
    CERTIFICATE_HOLDERS_EMAIL_ADDRESS_OPTIONS("CertificateHoldersEmailAddressOptions"),
    @XmlEnumValue("AdjustCommissionUpdateInstallmentsOptions")
    ADJUST_COMMISSION_UPDATE_INSTALLMENTS_OPTIONS("AdjustCommissionUpdateInstallmentsOptions"),
    @XmlEnumValue("DateBasedCommissionYearsMonthsOptions")
    DATE_BASED_COMMISSION_YEARS_MONTHS_OPTIONS("DateBasedCommissionYearsMonthsOptions"),
    @XmlEnumValue("DefaultActivityAsOptions")
    DEFAULT_ACTIVITY_AS_OPTIONS("DefaultActivityAsOptions"),
    @XmlEnumValue("RevenueIndividualPolicyBookOfBusinessClientAccountOptions")
    REVENUE_INDIVIDUAL_POLICY_BOOK_OF_BUSINESS_CLIENT_ACCOUNT_OPTIONS("RevenueIndividualPolicyBookOfBusinessClientAccountOptions"),
    @XmlEnumValue("AllocationMethodAutomaticBreakdownOptions")
    ALLOCATION_METHOD_AUTOMATIC_BREAKDOWN_OPTIONS("AllocationMethodAutomaticBreakdownOptions"),
    @XmlEnumValue("ProducerBrokerCommissionTermOptions")
    PRODUCER_BROKER_COMMISSION_TERM_OPTIONS("ProducerBrokerCommissionTermOptions"),
    @XmlEnumValue("TruckersStateFederalFilingActionLiabilityOptions")
    TRUCKERS_STATE_FEDERAL_FILING_ACTION_LIABILITY_OPTIONS("TruckersStateFederalFilingActionLiabilityOptions"),
    @XmlEnumValue("TruckersStateFederalFilingActionCargoOptions")
    TRUCKERS_STATE_FEDERAL_FILING_ACTION_CARGO_OPTIONS("TruckersStateFederalFilingActionCargoOptions"),
    @XmlEnumValue("TaxFeeRatesAllSelectedOptions")
    TAX_FEE_RATES_ALL_SELECTED_OPTIONS("TaxFeeRatesAllSelectedOptions"),
    @XmlEnumValue("TaxFeeRatesIncludeExcludeOptions")
    TAX_FEE_RATES_INCLUDE_EXCLUDE_OPTIONS("TaxFeeRatesIncludeExcludeOptions");
    private final String value;

    OptionTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OptionTypes fromValue(String v) {
        for (OptionTypes c: OptionTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
