
package com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.Flags;


/**
 * <p>Java class for AttachedToItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttachedToItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachedToID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AttachedToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/_attachedtoitem/}Flags" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachedToItem", propOrder = {
    "attachedToID",
    "attachedToType",
    "description",
    "flag"
})
public class AttachedToItem {

    @XmlElement(name = "AttachedToID")
    protected Integer attachedToID;
    @XmlElement(name = "AttachedToType", nillable = true)
    protected String attachedToType;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;

    /**
     * Gets the value of the attachedToID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttachedToID() {
        return attachedToID;
    }

    /**
     * Sets the value of the attachedToID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttachedToID(Integer value) {
        this.attachedToID = value;
    }

    /**
     * Gets the value of the attachedToType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachedToType() {
        return attachedToType;
    }

    /**
     * Sets the value of the attachedToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachedToType(String value) {
        this.attachedToType = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

}
