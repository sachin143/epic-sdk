
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Commissions_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "Commissions");
    private final static QName _InstallmentItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "InstallmentItem");
    private final static QName _InstallmentItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "InstallmentItems");
    private final static QName _Payment_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "Payment");
    private final static QName _Detail_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "Detail");
    private final static QName _Billing_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "Billing");
    private final static QName _Invoice_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "Invoice");
    private final static QName _PaymentMethod_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", "PaymentMethod");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link Invoice }
     * 
     */
    public Invoice createInvoice() {
        return new Invoice();
    }

    /**
     * Create an instance of {@link Billing }
     * 
     */
    public Billing createBilling() {
        return new Billing();
    }

    /**
     * Create an instance of {@link InstallmentItem }
     * 
     */
    public InstallmentItem createInstallmentItem() {
        return new InstallmentItem();
    }

    /**
     * Create an instance of {@link PaymentMethod }
     * 
     */
    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    /**
     * Create an instance of {@link InstallmentItems }
     * 
     */
    public InstallmentItems createInstallmentItems() {
        return new InstallmentItems();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link Commissions }
     * 
     */
    public Commissions createCommissions() {
        return new Commissions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Commissions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "Commissions")
    public JAXBElement<Commissions> createCommissions(Commissions value) {
        return new JAXBElement<Commissions>(_Commissions_QNAME, Commissions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstallmentItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "InstallmentItem")
    public JAXBElement<InstallmentItem> createInstallmentItem(InstallmentItem value) {
        return new JAXBElement<InstallmentItem>(_InstallmentItem_QNAME, InstallmentItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstallmentItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "InstallmentItems")
    public JAXBElement<InstallmentItems> createInstallmentItems(InstallmentItems value) {
        return new JAXBElement<InstallmentItems>(_InstallmentItems_QNAME, InstallmentItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Payment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "Payment")
    public JAXBElement<Payment> createPayment(Payment value) {
        return new JAXBElement<Payment>(_Payment_QNAME, Payment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Detail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "Detail")
    public JAXBElement<Detail> createDetail(Detail value) {
        return new JAXBElement<Detail>(_Detail_QNAME, Detail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Billing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "Billing")
    public JAXBElement<Billing> createBilling(Billing value) {
        return new JAXBElement<Billing>(_Billing_QNAME, Billing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Invoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "Invoice")
    public JAXBElement<Invoice> createInvoice(Invoice value) {
        return new JAXBElement<Invoice>(_Invoice_QNAME, Invoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/", name = "PaymentMethod")
    public JAXBElement<PaymentMethod> createPaymentMethod(PaymentMethod value) {
        return new JAXBElement<PaymentMethod>(_PaymentMethod_QNAME, PaymentMethod.class, null, value);
    }

}
