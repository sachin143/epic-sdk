
package com.appliedsystems.schemas.epic.sdk._2009._07._account;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfAttachment_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", "ArrayOfAttachment");
    private final static QName _Attachment_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", "Attachment");
    private final static QName _ArrayOfClient_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", "ArrayOfClient");
    private final static QName _Client_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", "Client");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link ArrayOfAttachment }
     * 
     */
    public ArrayOfAttachment createArrayOfAttachment() {
        return new ArrayOfAttachment();
    }

    /**
     * Create an instance of {@link ArrayOfClient }
     * 
     */
    public ArrayOfClient createArrayOfClient() {
        return new ArrayOfClient();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAttachment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", name = "ArrayOfAttachment")
    public JAXBElement<ArrayOfAttachment> createArrayOfAttachment(ArrayOfAttachment value) {
        return new JAXBElement<ArrayOfAttachment>(_ArrayOfAttachment_QNAME, ArrayOfAttachment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Attachment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", name = "Attachment")
    public JAXBElement<Attachment> createAttachment(Attachment value) {
        return new JAXBElement<Attachment>(_Attachment_QNAME, Attachment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfClient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", name = "ArrayOfClient")
    public JAXBElement<ArrayOfClient> createArrayOfClient(ArrayOfClient value) {
        return new JAXBElement<ArrayOfClient>(_ArrayOfClient_QNAME, ArrayOfClient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Client }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/", name = "Client")
    public JAXBElement<Client> createClient(Client value) {
        return new JAXBElement<Client>(_Client_QNAME, Client.class, null, value);
    }

}
