
package com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BankDetailItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankDetailItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankDetailItem" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}BankDetailItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankDetailItems", propOrder = {
    "bankDetailItems"
})
public class BankDetailItems {

    @XmlElement(name = "BankDetailItem", nillable = true)
    protected List<BankDetailItem> bankDetailItems;

    /**
     * Gets the value of the bankDetailItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bankDetailItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBankDetailItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BankDetailItem }
     * 
     * 
     */
    public List<BankDetailItem> getBankDetailItems() {
        if (bankDetailItems == null) {
            bankDetailItems = new ArrayList<BankDetailItem>();
        }
        return this.bankDetailItems;
    }

}
