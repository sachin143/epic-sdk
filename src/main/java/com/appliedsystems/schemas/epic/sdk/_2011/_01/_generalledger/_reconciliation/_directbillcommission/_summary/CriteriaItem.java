
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for CriteriaItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CriteriaItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllSelectedOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Inactive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SelectedItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}SelectedItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CriteriaItem", propOrder = {
    "allSelectedOption",
    "inactive",
    "selectedItems"
})
public class CriteriaItem {

    @XmlElement(name = "AllSelectedOption", nillable = true)
    protected OptionType allSelectedOption;
    @XmlElement(name = "Inactive")
    protected Boolean inactive;
    @XmlElement(name = "SelectedItems", nillable = true)
    protected SelectedItems selectedItems;

    /**
     * Gets the value of the allSelectedOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAllSelectedOption() {
        return allSelectedOption;
    }

    /**
     * Sets the value of the allSelectedOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAllSelectedOption(OptionType value) {
        this.allSelectedOption = value;
    }

    /**
     * Gets the value of the inactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInactive() {
        return inactive;
    }

    /**
     * Sets the value of the inactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInactive(Boolean value) {
        this.inactive = value;
    }

    /**
     * Gets the value of the selectedItems property.
     * 
     * @return
     *     possible object is
     *     {@link SelectedItems }
     *     
     */
    public SelectedItems getSelectedItems() {
        return selectedItems;
    }

    /**
     * Sets the value of the selectedItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectedItems }
     *     
     */
    public void setSelectedItems(SelectedItems value) {
        this.selectedItems = value;
    }

}
