
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.InvoiceItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.TaxFee;


/**
 * <p>Java class for GenerateTaxFeeItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateTaxFeeItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/}InvoiceItems" minOccurs="0"/>
 *         &lt;element name="TaxFeeValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/_generatetaxfeeitem/}TaxFee" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateTaxFeeItem", propOrder = {
    "invoiceItems",
    "taxFeeValue"
})
public class GenerateTaxFeeItem {

    @XmlElement(name = "InvoiceItems", nillable = true)
    protected InvoiceItems invoiceItems;
    @XmlElement(name = "TaxFeeValue", nillable = true)
    protected TaxFee taxFeeValue;

    /**
     * Gets the value of the invoiceItems property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceItems }
     *     
     */
    public InvoiceItems getInvoiceItems() {
        return invoiceItems;
    }

    /**
     * Sets the value of the invoiceItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceItems }
     *     
     */
    public void setInvoiceItems(InvoiceItems value) {
        this.invoiceItems = value;
    }

    /**
     * Gets the value of the taxFeeValue property.
     * 
     * @return
     *     possible object is
     *     {@link TaxFee }
     *     
     */
    public TaxFee getTaxFeeValue() {
        return taxFeeValue;
    }

    /**
     * Sets the value of the taxFeeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxFee }
     *     
     */
    public void setTaxFeeValue(TaxFee value) {
        this.taxFeeValue = value;
    }

}
