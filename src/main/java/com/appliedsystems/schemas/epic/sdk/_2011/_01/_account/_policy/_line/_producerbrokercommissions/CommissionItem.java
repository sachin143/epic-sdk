
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.Flags;


/**
 * <p>Java class for CommissionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CommissionPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/_commissionitem/}Flags" minOccurs="0"/>
 *         &lt;element name="LookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductionCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CommissionAgreementID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ContractName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueAgencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueBranchName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueDepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenuePercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ShareRevenueProfitCenterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrBrLineCommissionAnnualized" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PrBrLineCommissionEstimated" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="OverrideCommissionAgreementPercentageOrAmount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UseCommissionAgreement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionItem", propOrder = {
    "commissionAmount",
    "commissionID",
    "commissionPercent",
    "commissionType",
    "flag",
    "lookupCode",
    "orderNumber",
    "producerBrokerCode",
    "productionCredit",
    "commissionAgreementID",
    "contractID",
    "contractName",
    "shareRevenueAgencyCode",
    "shareRevenueAgencyID",
    "shareRevenueAgencyName",
    "shareRevenueBranchCode",
    "shareRevenueBranchID",
    "shareRevenueBranchName",
    "shareRevenueDepartmentCode",
    "shareRevenueDepartmentID",
    "shareRevenueDepartmentName",
    "shareRevenuePercent",
    "shareRevenueProfitCenterCode",
    "shareRevenueProfitCenterID",
    "shareRevenueProfitCenterName",
    "prBrLineCommissionAnnualized",
    "prBrLineCommissionEstimated",
    "overrideCommissionAgreementPercentageOrAmount",
    "useCommissionAgreement"
})
public class CommissionItem {

    @XmlElement(name = "CommissionAmount", nillable = true)
    protected BigDecimal commissionAmount;
    @XmlElement(name = "CommissionID")
    protected Integer commissionID;
    @XmlElement(name = "CommissionPercent", nillable = true)
    protected BigDecimal commissionPercent;
    @XmlElement(name = "CommissionType", nillable = true)
    protected String commissionType;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "LookupCode", nillable = true)
    protected String lookupCode;
    @XmlElement(name = "OrderNumber")
    protected Integer orderNumber;
    @XmlElement(name = "ProducerBrokerCode", nillable = true)
    protected String producerBrokerCode;
    @XmlElement(name = "ProductionCredit")
    protected BigDecimal productionCredit;
    @XmlElement(name = "CommissionAgreementID")
    protected Integer commissionAgreementID;
    @XmlElement(name = "ContractID", nillable = true)
    protected Integer contractID;
    @XmlElement(name = "ContractName", nillable = true)
    protected String contractName;
    @XmlElement(name = "ShareRevenueAgencyCode", nillable = true)
    protected String shareRevenueAgencyCode;
    @XmlElement(name = "ShareRevenueAgencyID")
    protected Integer shareRevenueAgencyID;
    @XmlElement(name = "ShareRevenueAgencyName", nillable = true)
    protected String shareRevenueAgencyName;
    @XmlElement(name = "ShareRevenueBranchCode", nillable = true)
    protected String shareRevenueBranchCode;
    @XmlElement(name = "ShareRevenueBranchID")
    protected Integer shareRevenueBranchID;
    @XmlElement(name = "ShareRevenueBranchName", nillable = true)
    protected String shareRevenueBranchName;
    @XmlElement(name = "ShareRevenueDepartmentCode", nillable = true)
    protected String shareRevenueDepartmentCode;
    @XmlElement(name = "ShareRevenueDepartmentID")
    protected Integer shareRevenueDepartmentID;
    @XmlElement(name = "ShareRevenueDepartmentName", nillable = true)
    protected String shareRevenueDepartmentName;
    @XmlElement(name = "ShareRevenuePercent", nillable = true)
    protected BigDecimal shareRevenuePercent;
    @XmlElement(name = "ShareRevenueProfitCenterCode", nillable = true)
    protected String shareRevenueProfitCenterCode;
    @XmlElement(name = "ShareRevenueProfitCenterID")
    protected Integer shareRevenueProfitCenterID;
    @XmlElement(name = "ShareRevenueProfitCenterName", nillable = true)
    protected String shareRevenueProfitCenterName;
    @XmlElement(name = "PrBrLineCommissionAnnualized", nillable = true)
    protected BigDecimal prBrLineCommissionAnnualized;
    @XmlElement(name = "PrBrLineCommissionEstimated", nillable = true)
    protected BigDecimal prBrLineCommissionEstimated;
    @XmlElement(name = "OverrideCommissionAgreementPercentageOrAmount")
    protected Boolean overrideCommissionAgreementPercentageOrAmount;
    @XmlElement(name = "UseCommissionAgreement")
    protected Boolean useCommissionAgreement;

    /**
     * Gets the value of the commissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionAmount() {
        return commissionAmount;
    }

    /**
     * Sets the value of the commissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionAmount(BigDecimal value) {
        this.commissionAmount = value;
    }

    /**
     * Gets the value of the commissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCommissionID() {
        return commissionID;
    }

    /**
     * Sets the value of the commissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCommissionID(Integer value) {
        this.commissionID = value;
    }

    /**
     * Gets the value of the commissionPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCommissionPercent() {
        return commissionPercent;
    }

    /**
     * Sets the value of the commissionPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCommissionPercent(BigDecimal value) {
        this.commissionPercent = value;
    }

    /**
     * Gets the value of the commissionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionType() {
        return commissionType;
    }

    /**
     * Sets the value of the commissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionType(String value) {
        this.commissionType = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the lookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookupCode() {
        return lookupCode;
    }

    /**
     * Sets the value of the lookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookupCode(String value) {
        this.lookupCode = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOrderNumber(Integer value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the producerBrokerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerBrokerCode() {
        return producerBrokerCode;
    }

    /**
     * Sets the value of the producerBrokerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerBrokerCode(String value) {
        this.producerBrokerCode = value;
    }

    /**
     * Gets the value of the productionCredit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProductionCredit() {
        return productionCredit;
    }

    /**
     * Sets the value of the productionCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProductionCredit(BigDecimal value) {
        this.productionCredit = value;
    }

    /**
     * Gets the value of the commissionAgreementID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCommissionAgreementID() {
        return commissionAgreementID;
    }

    /**
     * Sets the value of the commissionAgreementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCommissionAgreementID(Integer value) {
        this.commissionAgreementID = value;
    }

    /**
     * Gets the value of the contractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContractID() {
        return contractID;
    }

    /**
     * Sets the value of the contractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContractID(Integer value) {
        this.contractID = value;
    }

    /**
     * Gets the value of the contractName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractName() {
        return contractName;
    }

    /**
     * Sets the value of the contractName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractName(String value) {
        this.contractName = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyCode() {
        return shareRevenueAgencyCode;
    }

    /**
     * Sets the value of the shareRevenueAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyCode(String value) {
        this.shareRevenueAgencyCode = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueAgencyID() {
        return shareRevenueAgencyID;
    }

    /**
     * Sets the value of the shareRevenueAgencyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueAgencyID(Integer value) {
        this.shareRevenueAgencyID = value;
    }

    /**
     * Gets the value of the shareRevenueAgencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueAgencyName() {
        return shareRevenueAgencyName;
    }

    /**
     * Sets the value of the shareRevenueAgencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueAgencyName(String value) {
        this.shareRevenueAgencyName = value;
    }

    /**
     * Gets the value of the shareRevenueBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchCode() {
        return shareRevenueBranchCode;
    }

    /**
     * Sets the value of the shareRevenueBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchCode(String value) {
        this.shareRevenueBranchCode = value;
    }

    /**
     * Gets the value of the shareRevenueBranchID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueBranchID() {
        return shareRevenueBranchID;
    }

    /**
     * Sets the value of the shareRevenueBranchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueBranchID(Integer value) {
        this.shareRevenueBranchID = value;
    }

    /**
     * Gets the value of the shareRevenueBranchName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueBranchName() {
        return shareRevenueBranchName;
    }

    /**
     * Sets the value of the shareRevenueBranchName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueBranchName(String value) {
        this.shareRevenueBranchName = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentCode() {
        return shareRevenueDepartmentCode;
    }

    /**
     * Sets the value of the shareRevenueDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentCode(String value) {
        this.shareRevenueDepartmentCode = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueDepartmentID() {
        return shareRevenueDepartmentID;
    }

    /**
     * Sets the value of the shareRevenueDepartmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueDepartmentID(Integer value) {
        this.shareRevenueDepartmentID = value;
    }

    /**
     * Gets the value of the shareRevenueDepartmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueDepartmentName() {
        return shareRevenueDepartmentName;
    }

    /**
     * Sets the value of the shareRevenueDepartmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueDepartmentName(String value) {
        this.shareRevenueDepartmentName = value;
    }

    /**
     * Gets the value of the shareRevenuePercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getShareRevenuePercent() {
        return shareRevenuePercent;
    }

    /**
     * Sets the value of the shareRevenuePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setShareRevenuePercent(BigDecimal value) {
        this.shareRevenuePercent = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterCode() {
        return shareRevenueProfitCenterCode;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterCode(String value) {
        this.shareRevenueProfitCenterCode = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShareRevenueProfitCenterID() {
        return shareRevenueProfitCenterID;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShareRevenueProfitCenterID(Integer value) {
        this.shareRevenueProfitCenterID = value;
    }

    /**
     * Gets the value of the shareRevenueProfitCenterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRevenueProfitCenterName() {
        return shareRevenueProfitCenterName;
    }

    /**
     * Sets the value of the shareRevenueProfitCenterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRevenueProfitCenterName(String value) {
        this.shareRevenueProfitCenterName = value;
    }

    /**
     * Gets the value of the prBrLineCommissionAnnualized property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrBrLineCommissionAnnualized() {
        return prBrLineCommissionAnnualized;
    }

    /**
     * Sets the value of the prBrLineCommissionAnnualized property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrBrLineCommissionAnnualized(BigDecimal value) {
        this.prBrLineCommissionAnnualized = value;
    }

    /**
     * Gets the value of the prBrLineCommissionEstimated property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrBrLineCommissionEstimated() {
        return prBrLineCommissionEstimated;
    }

    /**
     * Sets the value of the prBrLineCommissionEstimated property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrBrLineCommissionEstimated(BigDecimal value) {
        this.prBrLineCommissionEstimated = value;
    }

    /**
     * Gets the value of the overrideCommissionAgreementPercentageOrAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideCommissionAgreementPercentageOrAmount() {
        return overrideCommissionAgreementPercentageOrAmount;
    }

    /**
     * Sets the value of the overrideCommissionAgreementPercentageOrAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCommissionAgreementPercentageOrAmount(Boolean value) {
        this.overrideCommissionAgreementPercentageOrAmount = value;
    }

    /**
     * Gets the value of the useCommissionAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCommissionAgreement() {
        return useCommissionAgreement;
    }

    /**
     * Sets the value of the useCommissionAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCommissionAgreement(Boolean value) {
        this.useCommissionAgreement = value;
    }

}
