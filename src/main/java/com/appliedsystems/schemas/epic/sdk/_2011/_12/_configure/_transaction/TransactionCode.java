
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Bill" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodeID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GLAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GLSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARDueDateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARDueDateDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ARDueDatePlusMinusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AmountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AnnualizedCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BrokerCommissionable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PolicyEstimatedPremium" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProducerCommissionable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ServiceFee" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DisplayOnAcceptQuoteScreen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionCode", propOrder = {
    "bill",
    "classDescription",
    "code",
    "codeID",
    "description",
    "glAccountNumberCode",
    "glSubAccountNumberCode",
    "arDueDateCode",
    "arDueDateDays",
    "arDueDatePlusMinusCode",
    "agencyCommissionable",
    "amountCode",
    "annualizedCode",
    "billedCode",
    "brokerCommissionable",
    "policyEstimatedPremium",
    "producerCommissionable",
    "serviceFee",
    "displayOnAcceptQuoteScreen"
})
public class TransactionCode {

    @XmlElement(name = "Bill", nillable = true)
    protected String bill;
    @XmlElement(name = "ClassDescription", nillable = true)
    protected String classDescription;
    @XmlElement(name = "Code", nillable = true)
    protected String code;
    @XmlElement(name = "CodeID")
    protected Integer codeID;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "GLAccountNumberCode", nillable = true)
    protected String glAccountNumberCode;
    @XmlElement(name = "GLSubAccountNumberCode", nillable = true)
    protected String glSubAccountNumberCode;
    @XmlElement(name = "ARDueDateCode", nillable = true)
    protected String arDueDateCode;
    @XmlElement(name = "ARDueDateDays", nillable = true)
    protected Integer arDueDateDays;
    @XmlElement(name = "ARDueDatePlusMinusCode", nillable = true)
    protected String arDueDatePlusMinusCode;
    @XmlElement(name = "AgencyCommissionable")
    protected Boolean agencyCommissionable;
    @XmlElement(name = "AmountCode", nillable = true)
    protected String amountCode;
    @XmlElement(name = "AnnualizedCode", nillable = true)
    protected String annualizedCode;
    @XmlElement(name = "BilledCode", nillable = true)
    protected String billedCode;
    @XmlElement(name = "BrokerCommissionable")
    protected Boolean brokerCommissionable;
    @XmlElement(name = "PolicyEstimatedPremium")
    protected Boolean policyEstimatedPremium;
    @XmlElement(name = "ProducerCommissionable")
    protected Boolean producerCommissionable;
    @XmlElement(name = "ServiceFee")
    protected Boolean serviceFee;
    @XmlElement(name = "DisplayOnAcceptQuoteScreen", nillable = true)
    protected Boolean displayOnAcceptQuoteScreen;

    /**
     * Gets the value of the bill property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBill() {
        return bill;
    }

    /**
     * Sets the value of the bill property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBill(String value) {
        this.bill = value;
    }

    /**
     * Gets the value of the classDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassDescription() {
        return classDescription;
    }

    /**
     * Sets the value of the classDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassDescription(String value) {
        this.classDescription = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the codeID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodeID() {
        return codeID;
    }

    /**
     * Sets the value of the codeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodeID(Integer value) {
        this.codeID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the glAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLAccountNumberCode() {
        return glAccountNumberCode;
    }

    /**
     * Sets the value of the glAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLAccountNumberCode(String value) {
        this.glAccountNumberCode = value;
    }

    /**
     * Gets the value of the glSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLSubAccountNumberCode() {
        return glSubAccountNumberCode;
    }

    /**
     * Sets the value of the glSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLSubAccountNumberCode(String value) {
        this.glSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the arDueDateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARDueDateCode() {
        return arDueDateCode;
    }

    /**
     * Sets the value of the arDueDateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARDueDateCode(String value) {
        this.arDueDateCode = value;
    }

    /**
     * Gets the value of the arDueDateDays property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getARDueDateDays() {
        return arDueDateDays;
    }

    /**
     * Sets the value of the arDueDateDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setARDueDateDays(Integer value) {
        this.arDueDateDays = value;
    }

    /**
     * Gets the value of the arDueDatePlusMinusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARDueDatePlusMinusCode() {
        return arDueDatePlusMinusCode;
    }

    /**
     * Sets the value of the arDueDatePlusMinusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARDueDatePlusMinusCode(String value) {
        this.arDueDatePlusMinusCode = value;
    }

    /**
     * Gets the value of the agencyCommissionable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAgencyCommissionable() {
        return agencyCommissionable;
    }

    /**
     * Sets the value of the agencyCommissionable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAgencyCommissionable(Boolean value) {
        this.agencyCommissionable = value;
    }

    /**
     * Gets the value of the amountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountCode() {
        return amountCode;
    }

    /**
     * Sets the value of the amountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountCode(String value) {
        this.amountCode = value;
    }

    /**
     * Gets the value of the annualizedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualizedCode() {
        return annualizedCode;
    }

    /**
     * Sets the value of the annualizedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualizedCode(String value) {
        this.annualizedCode = value;
    }

    /**
     * Gets the value of the billedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledCode() {
        return billedCode;
    }

    /**
     * Sets the value of the billedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledCode(String value) {
        this.billedCode = value;
    }

    /**
     * Gets the value of the brokerCommissionable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBrokerCommissionable() {
        return brokerCommissionable;
    }

    /**
     * Sets the value of the brokerCommissionable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBrokerCommissionable(Boolean value) {
        this.brokerCommissionable = value;
    }

    /**
     * Gets the value of the policyEstimatedPremium property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPolicyEstimatedPremium() {
        return policyEstimatedPremium;
    }

    /**
     * Sets the value of the policyEstimatedPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPolicyEstimatedPremium(Boolean value) {
        this.policyEstimatedPremium = value;
    }

    /**
     * Gets the value of the producerCommissionable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProducerCommissionable() {
        return producerCommissionable;
    }

    /**
     * Sets the value of the producerCommissionable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProducerCommissionable(Boolean value) {
        this.producerCommissionable = value;
    }

    /**
     * Gets the value of the serviceFee property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceFee() {
        return serviceFee;
    }

    /**
     * Sets the value of the serviceFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceFee(Boolean value) {
        this.serviceFee = value;
    }

    /**
     * Gets the value of the displayOnAcceptQuoteScreen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayOnAcceptQuoteScreen() {
        return displayOnAcceptQuoteScreen;
    }

    /**
     * Sets the value of the displayOnAcceptQuoteScreen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayOnAcceptQuoteScreen(Boolean value) {
        this.displayOnAcceptQuoteScreen = value;
    }

}
