
package com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelPolicyAdditionalInterestGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "CancelPolicyAdditionalInterestGetType");
    private final static QName _ArrayOfRemark_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "ArrayOfRemark");
    private final static QName _Remark_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "Remark");
    private final static QName _CancelPolicyRemarksGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "CancelPolicyRemarksGetType");
    private final static QName _AdditionalInterest_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "AdditionalInterest");
    private final static QName _ArrayOfAdditionalInterest_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", "ArrayOfAdditionalInterest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfRemark }
     * 
     */
    public ArrayOfRemark createArrayOfRemark() {
        return new ArrayOfRemark();
    }

    /**
     * Create an instance of {@link AdditionalInterest }
     * 
     */
    public AdditionalInterest createAdditionalInterest() {
        return new AdditionalInterest();
    }

    /**
     * Create an instance of {@link Remark }
     * 
     */
    public Remark createRemark() {
        return new Remark();
    }

    /**
     * Create an instance of {@link ArrayOfAdditionalInterest }
     * 
     */
    public ArrayOfAdditionalInterest createArrayOfAdditionalInterest() {
        return new ArrayOfAdditionalInterest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelPolicyAdditionalInterestGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "CancelPolicyAdditionalInterestGetType")
    public JAXBElement<CancelPolicyAdditionalInterestGetType> createCancelPolicyAdditionalInterestGetType(CancelPolicyAdditionalInterestGetType value) {
        return new JAXBElement<CancelPolicyAdditionalInterestGetType>(_CancelPolicyAdditionalInterestGetType_QNAME, CancelPolicyAdditionalInterestGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRemark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "ArrayOfRemark")
    public JAXBElement<ArrayOfRemark> createArrayOfRemark(ArrayOfRemark value) {
        return new JAXBElement<ArrayOfRemark>(_ArrayOfRemark_QNAME, ArrayOfRemark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Remark }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "Remark")
    public JAXBElement<Remark> createRemark(Remark value) {
        return new JAXBElement<Remark>(_Remark_QNAME, Remark.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelPolicyRemarksGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "CancelPolicyRemarksGetType")
    public JAXBElement<CancelPolicyRemarksGetType> createCancelPolicyRemarksGetType(CancelPolicyRemarksGetType value) {
        return new JAXBElement<CancelPolicyRemarksGetType>(_CancelPolicyRemarksGetType_QNAME, CancelPolicyRemarksGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalInterest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "AdditionalInterest")
    public JAXBElement<AdditionalInterest> createAdditionalInterest(AdditionalInterest value) {
        return new JAXBElement<AdditionalInterest>(_AdditionalInterest_QNAME, AdditionalInterest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAdditionalInterest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/", name = "ArrayOfAdditionalInterest")
    public JAXBElement<ArrayOfAdditionalInterest> createArrayOfAdditionalInterest(ArrayOfAdditionalInterest value) {
        return new JAXBElement<ArrayOfAdditionalInterest>(_ArrayOfAdditionalInterest_QNAME, ArrayOfAdditionalInterest.class, null, value);
    }

}
