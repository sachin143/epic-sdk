
package com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociatedToType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociatedToType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="PolicyID"/>
 *     &lt;enumeration value="LineID"/>
 *     &lt;enumeration value="TransactionID"/>
 *     &lt;enumeration value="ClaimID"/>
 *     &lt;enumeration value="ReceiptDetailItemID"/>
 *     &lt;enumeration value="MasterMarketingSubmissionID"/>
 *     &lt;enumeration value="ServiceID"/>
 *     &lt;enumeration value="OpportunityID"/>
 *     &lt;enumeration value="QuoteGUID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociatedToType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/")
@XmlEnum
public enum AssociatedToType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("PolicyID")
    POLICY_ID("PolicyID"),
    @XmlEnumValue("LineID")
    LINE_ID("LineID"),
    @XmlEnumValue("TransactionID")
    TRANSACTION_ID("TransactionID"),
    @XmlEnumValue("ClaimID")
    CLAIM_ID("ClaimID"),
    @XmlEnumValue("ReceiptDetailItemID")
    RECEIPT_DETAIL_ITEM_ID("ReceiptDetailItemID"),
    @XmlEnumValue("MasterMarketingSubmissionID")
    MASTER_MARKETING_SUBMISSION_ID("MasterMarketingSubmissionID"),
    @XmlEnumValue("ServiceID")
    SERVICE_ID("ServiceID"),
    @XmlEnumValue("OpportunityID")
    OPPORTUNITY_ID("OpportunityID"),
    @XmlEnumValue("QuoteGUID")
    QUOTE_GUID("QuoteGUID");
    private final String value;

    AssociatedToType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociatedToType fromValue(String v) {
        for (AssociatedToType c: AssociatedToType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
