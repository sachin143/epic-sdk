
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InstallmentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstallmentItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="GenerateInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GenerateInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InstallmentNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InstallmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceGrouping" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceGroupingExistingInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InvoiceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductionMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Transaction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstallmentItem", propOrder = {
    "arDueDate",
    "accountingMonth",
    "amount",
    "description",
    "effectiveDate",
    "generateInvoice",
    "generateInvoiceDate",
    "installmentNumber",
    "installmentType",
    "invoiceGrouping",
    "invoiceGroupingExistingInvoiceNumber",
    "invoiceMessage",
    "productionMonth",
    "transaction"
})
public class InstallmentItem {

    @XmlElement(name = "ARDueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arDueDate;
    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "GenerateInvoice")
    protected Boolean generateInvoice;
    @XmlElement(name = "GenerateInvoiceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar generateInvoiceDate;
    @XmlElement(name = "InstallmentNumber")
    protected Integer installmentNumber;
    @XmlElement(name = "InstallmentType", nillable = true)
    protected String installmentType;
    @XmlElement(name = "InvoiceGrouping", nillable = true)
    protected String invoiceGrouping;
    @XmlElement(name = "InvoiceGroupingExistingInvoiceNumber", nillable = true)
    protected Integer invoiceGroupingExistingInvoiceNumber;
    @XmlElement(name = "InvoiceMessage", nillable = true)
    protected String invoiceMessage;
    @XmlElement(name = "ProductionMonth", nillable = true)
    protected String productionMonth;
    @XmlElement(name = "Transaction", nillable = true)
    protected String transaction;

    /**
     * Gets the value of the arDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getARDueDate() {
        return arDueDate;
    }

    /**
     * Sets the value of the arDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setARDueDate(XMLGregorianCalendar value) {
        this.arDueDate = value;
    }

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the generateInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenerateInvoice() {
        return generateInvoice;
    }

    /**
     * Sets the value of the generateInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateInvoice(Boolean value) {
        this.generateInvoice = value;
    }

    /**
     * Gets the value of the generateInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGenerateInvoiceDate() {
        return generateInvoiceDate;
    }

    /**
     * Sets the value of the generateInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGenerateInvoiceDate(XMLGregorianCalendar value) {
        this.generateInvoiceDate = value;
    }

    /**
     * Gets the value of the installmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    /**
     * Sets the value of the installmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallmentNumber(Integer value) {
        this.installmentNumber = value;
    }

    /**
     * Gets the value of the installmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallmentType() {
        return installmentType;
    }

    /**
     * Sets the value of the installmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallmentType(String value) {
        this.installmentType = value;
    }

    /**
     * Gets the value of the invoiceGrouping property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceGrouping() {
        return invoiceGrouping;
    }

    /**
     * Sets the value of the invoiceGrouping property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceGrouping(String value) {
        this.invoiceGrouping = value;
    }

    /**
     * Gets the value of the invoiceGroupingExistingInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceGroupingExistingInvoiceNumber() {
        return invoiceGroupingExistingInvoiceNumber;
    }

    /**
     * Sets the value of the invoiceGroupingExistingInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceGroupingExistingInvoiceNumber(Integer value) {
        this.invoiceGroupingExistingInvoiceNumber = value;
    }

    /**
     * Gets the value of the invoiceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceMessage() {
        return invoiceMessage;
    }

    /**
     * Sets the value of the invoiceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceMessage(String value) {
        this.invoiceMessage = value;
    }

    /**
     * Gets the value of the productionMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionMonth() {
        return productionMonth;
    }

    /**
     * Sets the value of the productionMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionMonth(String value) {
        this.productionMonth = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransaction(String value) {
        this.transaction = value;
    }

}
