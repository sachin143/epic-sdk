
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._policy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Line_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/", "Line");
    private final static QName _ArrayOfLine_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/", "ArrayOfLine");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._policy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Line }
     * 
     */
    public Line createLine() {
        return new Line();
    }

    /**
     * Create an instance of {@link ArrayOfLine }
     * 
     */
    public ArrayOfLine createArrayOfLine() {
        return new ArrayOfLine();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Line }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/", name = "Line")
    public JAXBElement<Line> createLine(Line value) {
        return new JAXBElement<Line>(_Line_QNAME, Line.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/", name = "ArrayOfLine")
    public JAXBElement<ArrayOfLine> createArrayOfLine(ArrayOfLine value) {
        return new JAXBElement<ArrayOfLine>(_ArrayOfLine_QNAME, ArrayOfLine.class, null, value);
    }

}
