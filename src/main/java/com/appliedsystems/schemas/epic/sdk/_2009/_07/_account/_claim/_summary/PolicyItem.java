
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for PolicyItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimSummaryPolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClaimType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/_policyitem/}Flags" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NoticeOfOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RiskDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RiskID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RiskNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RiskType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubRiskDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubRiskID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SubRiskNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SubRiskType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyItem", propOrder = {
    "claimSummaryPolicyID",
    "claimType",
    "flag",
    "lineID",
    "noticeOfOption",
    "policyID",
    "riskDescription",
    "riskID",
    "riskNumber",
    "riskType",
    "subRiskDescription",
    "subRiskID",
    "subRiskNumber",
    "subRiskType",
    "serviceSummaryID"
})
public class PolicyItem {

    @XmlElement(name = "ClaimSummaryPolicyID")
    protected Integer claimSummaryPolicyID;
    @XmlElement(name = "ClaimType", nillable = true)
    protected String claimType;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "LineID")
    protected Integer lineID;
    @XmlElement(name = "NoticeOfOption", nillable = true)
    protected OptionType noticeOfOption;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "RiskDescription", nillable = true)
    protected String riskDescription;
    @XmlElement(name = "RiskID", nillable = true)
    protected Integer riskID;
    @XmlElement(name = "RiskNumber", nillable = true)
    protected Integer riskNumber;
    @XmlElement(name = "RiskType", nillable = true)
    protected String riskType;
    @XmlElement(name = "SubRiskDescription", nillable = true)
    protected String subRiskDescription;
    @XmlElement(name = "SubRiskID", nillable = true)
    protected Integer subRiskID;
    @XmlElement(name = "SubRiskNumber", nillable = true)
    protected Integer subRiskNumber;
    @XmlElement(name = "SubRiskType", nillable = true)
    protected String subRiskType;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;

    /**
     * Gets the value of the claimSummaryPolicyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimSummaryPolicyID() {
        return claimSummaryPolicyID;
    }

    /**
     * Sets the value of the claimSummaryPolicyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimSummaryPolicyID(Integer value) {
        this.claimSummaryPolicyID = value;
    }

    /**
     * Gets the value of the claimType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimType() {
        return claimType;
    }

    /**
     * Sets the value of the claimType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimType(String value) {
        this.claimType = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the noticeOfOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getNoticeOfOption() {
        return noticeOfOption;
    }

    /**
     * Sets the value of the noticeOfOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setNoticeOfOption(OptionType value) {
        this.noticeOfOption = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the riskDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskDescription() {
        return riskDescription;
    }

    /**
     * Sets the value of the riskDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskDescription(String value) {
        this.riskDescription = value;
    }

    /**
     * Gets the value of the riskID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRiskID() {
        return riskID;
    }

    /**
     * Sets the value of the riskID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRiskID(Integer value) {
        this.riskID = value;
    }

    /**
     * Gets the value of the riskNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRiskNumber() {
        return riskNumber;
    }

    /**
     * Sets the value of the riskNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRiskNumber(Integer value) {
        this.riskNumber = value;
    }

    /**
     * Gets the value of the riskType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskType() {
        return riskType;
    }

    /**
     * Sets the value of the riskType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskType(String value) {
        this.riskType = value;
    }

    /**
     * Gets the value of the subRiskDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubRiskDescription() {
        return subRiskDescription;
    }

    /**
     * Sets the value of the subRiskDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubRiskDescription(String value) {
        this.subRiskDescription = value;
    }

    /**
     * Gets the value of the subRiskID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubRiskID() {
        return subRiskID;
    }

    /**
     * Sets the value of the subRiskID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubRiskID(Integer value) {
        this.subRiskID = value;
    }

    /**
     * Gets the value of the subRiskNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubRiskNumber() {
        return subRiskNumber;
    }

    /**
     * Sets the value of the subRiskNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubRiskNumber(Integer value) {
        this.subRiskNumber = value;
    }

    /**
     * Gets the value of the subRiskType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubRiskType() {
        return subRiskType;
    }

    /**
     * Sets the value of the subRiskType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubRiskType(String value) {
        this.subRiskType = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

}
