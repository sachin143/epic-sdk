
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for ModifyRevenueDeferralSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModifyRevenueDeferralSchedule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceFeeRevenueSchedule" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ModifyRevenueDeferralScheduleItems" minOccurs="0"/>
 *         &lt;element name="TransactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModifyRevenueDeferralSchedule", propOrder = {
    "serviceFeeRevenueSchedule",
    "transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions",
    "transactionID"
})
public class ModifyRevenueDeferralSchedule {

    @XmlElement(name = "ServiceFeeRevenueSchedule", nillable = true)
    protected ModifyRevenueDeferralScheduleItems serviceFeeRevenueSchedule;
    @XmlElement(name = "TransactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions", nillable = true)
    protected OptionType transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;

    /**
     * Gets the value of the serviceFeeRevenueSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link ModifyRevenueDeferralScheduleItems }
     *     
     */
    public ModifyRevenueDeferralScheduleItems getServiceFeeRevenueSchedule() {
        return serviceFeeRevenueSchedule;
    }

    /**
     * Sets the value of the serviceFeeRevenueSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModifyRevenueDeferralScheduleItems }
     *     
     */
    public void setServiceFeeRevenueSchedule(ModifyRevenueDeferralScheduleItems value) {
        this.serviceFeeRevenueSchedule = value;
    }

    /**
     * Gets the value of the transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getTransactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions() {
        return transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions;
    }

    /**
     * Sets the value of the transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setTransactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions(OptionType value) {
        this.transactionActionModifyRevenueDeferralScheduleUpdateInstallmentsOptions = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

}
