
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ReconcileDetailItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.Summary;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;


/**
 * <p>Java class for DirectBillCommission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DirectBillCommission">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DirectBillCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PaidStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReconcileDetailItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/}ReconcileDetailItems" minOccurs="0"/>
 *         &lt;element name="StatementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SummaryValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/}Summary" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="RecordDetailItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/}RecordDetailItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DirectBillCommission", propOrder = {
    "directBillCommissionID",
    "isReadOnly",
    "paidStatus",
    "reconcileDetailItems",
    "statementStatus",
    "summaryValue",
    "timestamp",
    "recordDetailItems"
})
public class DirectBillCommission {

    @XmlElement(name = "DirectBillCommissionID")
    protected Integer directBillCommissionID;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "PaidStatus", nillable = true)
    protected String paidStatus;
    @XmlElement(name = "ReconcileDetailItems", nillable = true)
    protected ReconcileDetailItems reconcileDetailItems;
    @XmlElement(name = "StatementStatus", nillable = true)
    protected String statementStatus;
    @XmlElement(name = "SummaryValue", nillable = true)
    protected Summary summaryValue;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "RecordDetailItems", nillable = true)
    protected RecordDetailItems recordDetailItems;

    /**
     * Gets the value of the directBillCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDirectBillCommissionID() {
        return directBillCommissionID;
    }

    /**
     * Sets the value of the directBillCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDirectBillCommissionID(Integer value) {
        this.directBillCommissionID = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the paidStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaidStatus() {
        return paidStatus;
    }

    /**
     * Sets the value of the paidStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaidStatus(String value) {
        this.paidStatus = value;
    }

    /**
     * Gets the value of the reconcileDetailItems property.
     * 
     * @return
     *     possible object is
     *     {@link ReconcileDetailItems }
     *     
     */
    public ReconcileDetailItems getReconcileDetailItems() {
        return reconcileDetailItems;
    }

    /**
     * Sets the value of the reconcileDetailItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReconcileDetailItems }
     *     
     */
    public void setReconcileDetailItems(ReconcileDetailItems value) {
        this.reconcileDetailItems = value;
    }

    /**
     * Gets the value of the statementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementStatus() {
        return statementStatus;
    }

    /**
     * Sets the value of the statementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementStatus(String value) {
        this.statementStatus = value;
    }

    /**
     * Gets the value of the summaryValue property.
     * 
     * @return
     *     possible object is
     *     {@link Summary }
     *     
     */
    public Summary getSummaryValue() {
        return summaryValue;
    }

    /**
     * Sets the value of the summaryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Summary }
     *     
     */
    public void setSummaryValue(Summary value) {
        this.summaryValue = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the recordDetailItems property.
     * 
     * @return
     *     possible object is
     *     {@link RecordDetailItems }
     *     
     */
    public RecordDetailItems getRecordDetailItems() {
        return recordDetailItems;
    }

    /**
     * Sets the value of the recordDetailItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordDetailItems }
     *     
     */
    public void setRecordDetailItems(RecordDetailItems value) {
        this.recordDetailItems = value;
    }

}
