
package com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RecordRisks complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecordRisks">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LineRisksInsured" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LineTotalEligible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionRisksInsured" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionTotalEligible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecordRisks", propOrder = {
    "lineRisksInsured",
    "lineTotalEligible",
    "transactionRisksInsured",
    "transactionTotalEligible"
})
public class RecordRisks {

    @XmlElement(name = "LineRisksInsured", nillable = true)
    protected Integer lineRisksInsured;
    @XmlElement(name = "LineTotalEligible", nillable = true)
    protected Integer lineTotalEligible;
    @XmlElement(name = "TransactionRisksInsured", nillable = true)
    protected Integer transactionRisksInsured;
    @XmlElement(name = "TransactionTotalEligible", nillable = true)
    protected Integer transactionTotalEligible;

    /**
     * Gets the value of the lineRisksInsured property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineRisksInsured() {
        return lineRisksInsured;
    }

    /**
     * Sets the value of the lineRisksInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineRisksInsured(Integer value) {
        this.lineRisksInsured = value;
    }

    /**
     * Gets the value of the lineTotalEligible property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineTotalEligible() {
        return lineTotalEligible;
    }

    /**
     * Sets the value of the lineTotalEligible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineTotalEligible(Integer value) {
        this.lineTotalEligible = value;
    }

    /**
     * Gets the value of the transactionRisksInsured property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionRisksInsured() {
        return transactionRisksInsured;
    }

    /**
     * Sets the value of the transactionRisksInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionRisksInsured(Integer value) {
        this.transactionRisksInsured = value;
    }

    /**
     * Gets the value of the transactionTotalEligible property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionTotalEligible() {
        return transactionTotalEligible;
    }

    /**
     * Sets the value of the transactionTotalEligible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionTotalEligible(Integer value) {
        this.transactionTotalEligible = value;
    }

}
