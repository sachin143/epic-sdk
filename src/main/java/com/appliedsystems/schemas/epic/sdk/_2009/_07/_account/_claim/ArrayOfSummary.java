
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Summary" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}Summary" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSummary", propOrder = {
    "summaries"
})
public class ArrayOfSummary {

    @XmlElement(name = "Summary", nillable = true)
    protected List<Summary> summaries;

    /**
     * Gets the value of the summaries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the summaries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSummaries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Summary }
     * 
     * 
     */
    public List<Summary> getSummaries() {
        if (summaries == null) {
            summaries = new ArrayList<Summary>();
        }
        return this.summaries;
    }

}
