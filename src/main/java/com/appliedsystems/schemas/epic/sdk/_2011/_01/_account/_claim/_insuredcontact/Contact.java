
package com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Address" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="BusinessPhoneExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactInsured" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ContactWhen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobilePhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidencePhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondaryEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobilePhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidencePhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelationshipToInsuredCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = {
    "address",
    "businessPhoneExtension",
    "businessPhoneNumber",
    "comments",
    "contactInsured",
    "contactWhen",
    "mobilePhoneNumber",
    "name",
    "primaryEmail",
    "residencePhoneNumber",
    "secondaryEmail",
    "siteID",
    "businessPhoneCountryCode",
    "mobilePhoneCountryCode",
    "residencePhoneCountryCode",
    "faxCountryCode",
    "faxExtension",
    "faxNumber",
    "relationshipToInsuredCode"
})
public class Contact {

    @XmlElement(name = "Address", nillable = true)
    protected Address address;
    @XmlElement(name = "BusinessPhoneExtension", nillable = true)
    protected String businessPhoneExtension;
    @XmlElement(name = "BusinessPhoneNumber", nillable = true)
    protected String businessPhoneNumber;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "ContactInsured")
    protected Boolean contactInsured;
    @XmlElement(name = "ContactWhen", nillable = true)
    protected String contactWhen;
    @XmlElement(name = "MobilePhoneNumber", nillable = true)
    protected String mobilePhoneNumber;
    @XmlElement(name = "Name", nillable = true)
    protected String name;
    @XmlElement(name = "PrimaryEmail", nillable = true)
    protected String primaryEmail;
    @XmlElement(name = "ResidencePhoneNumber", nillable = true)
    protected String residencePhoneNumber;
    @XmlElement(name = "SecondaryEmail", nillable = true)
    protected String secondaryEmail;
    @XmlElement(name = "SiteID", nillable = true)
    protected String siteID;
    @XmlElement(name = "BusinessPhoneCountryCode", nillable = true)
    protected String businessPhoneCountryCode;
    @XmlElement(name = "MobilePhoneCountryCode", nillable = true)
    protected String mobilePhoneCountryCode;
    @XmlElement(name = "ResidencePhoneCountryCode", nillable = true)
    protected String residencePhoneCountryCode;
    @XmlElement(name = "FaxCountryCode", nillable = true)
    protected String faxCountryCode;
    @XmlElement(name = "FaxExtension", nillable = true)
    protected String faxExtension;
    @XmlElement(name = "FaxNumber", nillable = true)
    protected String faxNumber;
    @XmlElement(name = "RelationshipToInsuredCode", nillable = true)
    protected String relationshipToInsuredCode;

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the businessPhoneExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPhoneExtension() {
        return businessPhoneExtension;
    }

    /**
     * Sets the value of the businessPhoneExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPhoneExtension(String value) {
        this.businessPhoneExtension = value;
    }

    /**
     * Gets the value of the businessPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPhoneNumber() {
        return businessPhoneNumber;
    }

    /**
     * Sets the value of the businessPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPhoneNumber(String value) {
        this.businessPhoneNumber = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the contactInsured property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactInsured() {
        return contactInsured;
    }

    /**
     * Sets the value of the contactInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactInsured(Boolean value) {
        this.contactInsured = value;
    }

    /**
     * Gets the value of the contactWhen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactWhen() {
        return contactWhen;
    }

    /**
     * Sets the value of the contactWhen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactWhen(String value) {
        this.contactWhen = value;
    }

    /**
     * Gets the value of the mobilePhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    /**
     * Sets the value of the mobilePhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneNumber(String value) {
        this.mobilePhoneNumber = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the primaryEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryEmail() {
        return primaryEmail;
    }

    /**
     * Sets the value of the primaryEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryEmail(String value) {
        this.primaryEmail = value;
    }

    /**
     * Gets the value of the residencePhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePhoneNumber() {
        return residencePhoneNumber;
    }

    /**
     * Sets the value of the residencePhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePhoneNumber(String value) {
        this.residencePhoneNumber = value;
    }

    /**
     * Gets the value of the secondaryEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    /**
     * Sets the value of the secondaryEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryEmail(String value) {
        this.secondaryEmail = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the businessPhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPhoneCountryCode() {
        return businessPhoneCountryCode;
    }

    /**
     * Sets the value of the businessPhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPhoneCountryCode(String value) {
        this.businessPhoneCountryCode = value;
    }

    /**
     * Gets the value of the mobilePhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneCountryCode() {
        return mobilePhoneCountryCode;
    }

    /**
     * Sets the value of the mobilePhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneCountryCode(String value) {
        this.mobilePhoneCountryCode = value;
    }

    /**
     * Gets the value of the residencePhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePhoneCountryCode() {
        return residencePhoneCountryCode;
    }

    /**
     * Sets the value of the residencePhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePhoneCountryCode(String value) {
        this.residencePhoneCountryCode = value;
    }

    /**
     * Gets the value of the faxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxCountryCode() {
        return faxCountryCode;
    }

    /**
     * Sets the value of the faxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxCountryCode(String value) {
        this.faxCountryCode = value;
    }

    /**
     * Gets the value of the faxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxExtension() {
        return faxExtension;
    }

    /**
     * Sets the value of the faxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxExtension(String value) {
        this.faxExtension = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the relationshipToInsuredCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationshipToInsuredCode() {
        return relationshipToInsuredCode;
    }

    /**
     * Sets the value of the relationshipToInsuredCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationshipToInsuredCode(String value) {
        this.relationshipToInsuredCode = value;
    }

}
