
package com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BasicSettings_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/", "BasicSettings");
    private final static QName _ActivityCodeGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/", "ActivityCodeGetType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BasicSettings }
     * 
     */
    public BasicSettings createBasicSettings() {
        return new BasicSettings();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BasicSettings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/", name = "BasicSettings")
    public JAXBElement<BasicSettings> createBasicSettings(BasicSettings value) {
        return new JAXBElement<BasicSettings>(_BasicSettings_QNAME, BasicSettings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityCodeGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/", name = "ActivityCodeGetType")
    public JAXBElement<ActivityCodeGetType> createActivityCodeGetType(ActivityCodeGetType value) {
        return new JAXBElement<ActivityCodeGetType>(_ActivityCodeGetType_QNAME, ActivityCodeGetType.class, null, value);
    }

}
