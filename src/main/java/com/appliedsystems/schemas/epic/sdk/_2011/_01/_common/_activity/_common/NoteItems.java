
package com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NoteItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NoteItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoteItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/}NoteItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoteItems", propOrder = {
    "noteItems"
})
public class NoteItems {

    @XmlElement(name = "NoteItem", nillable = true)
    protected List<NoteItem> noteItems;

    /**
     * Gets the value of the noteItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the noteItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNoteItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteItem }
     * 
     * 
     */
    public List<NoteItem> getNoteItems() {
        if (noteItems == null) {
            noteItems = new ArrayList<NoteItem>();
        }
        return this.noteItems;
    }

}
