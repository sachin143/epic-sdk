
package com.appliedsystems.schemas.epic.sdk._2009._07._account._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.Flags;


/**
 * <p>Java class for RelationshipItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/_relationshipitem/}Flags" minOccurs="0"/>
 *         &lt;element name="IfRelatedAccountTypeIsClientIsProspect" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RelatedAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelatedAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelationshipID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RelationshipRole" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipItem", propOrder = {
    "flag",
    "ifRelatedAccountTypeIsClientIsProspect",
    "relatedAccountLookupCode",
    "relatedAccountType",
    "relationshipID",
    "relationshipRole",
    "relationshipType"
})
public class RelationshipItem {

    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "IfRelatedAccountTypeIsClientIsProspect")
    protected Boolean ifRelatedAccountTypeIsClientIsProspect;
    @XmlElement(name = "RelatedAccountLookupCode", nillable = true)
    protected String relatedAccountLookupCode;
    @XmlElement(name = "RelatedAccountType", nillable = true)
    protected String relatedAccountType;
    @XmlElement(name = "RelationshipID")
    protected Integer relationshipID;
    @XmlElement(name = "RelationshipRole", nillable = true)
    protected String relationshipRole;
    @XmlElement(name = "RelationshipType", nillable = true)
    protected String relationshipType;

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the ifRelatedAccountTypeIsClientIsProspect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIfRelatedAccountTypeIsClientIsProspect() {
        return ifRelatedAccountTypeIsClientIsProspect;
    }

    /**
     * Sets the value of the ifRelatedAccountTypeIsClientIsProspect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIfRelatedAccountTypeIsClientIsProspect(Boolean value) {
        this.ifRelatedAccountTypeIsClientIsProspect = value;
    }

    /**
     * Gets the value of the relatedAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedAccountLookupCode() {
        return relatedAccountLookupCode;
    }

    /**
     * Sets the value of the relatedAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedAccountLookupCode(String value) {
        this.relatedAccountLookupCode = value;
    }

    /**
     * Gets the value of the relatedAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedAccountType() {
        return relatedAccountType;
    }

    /**
     * Sets the value of the relatedAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedAccountType(String value) {
        this.relatedAccountType = value;
    }

    /**
     * Gets the value of the relationshipID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRelationshipID() {
        return relationshipID;
    }

    /**
     * Sets the value of the relationshipID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRelationshipID(Integer value) {
        this.relationshipID = value;
    }

    /**
     * Gets the value of the relationshipRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationshipRole() {
        return relationshipRole;
    }

    /**
     * Sets the value of the relationshipRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationshipRole(String value) {
        this.relationshipRole = value;
    }

    /**
     * Gets the value of the relationshipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets the value of the relationshipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationshipType(String value) {
        this.relationshipType = value;
    }

}
