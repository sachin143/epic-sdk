
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ActivityID"/>
 *     &lt;enumeration value="ReceiptID"/>
 *     &lt;enumeration value="DisbursementID"/>
 *     &lt;enumeration value="VoucherID"/>
 *     &lt;enumeration value="JournalEntryID"/>
 *     &lt;enumeration value="Filtered"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityGetType")
@XmlEnum
public enum ActivityGetType {

    @XmlEnumValue("ActivityID")
    ACTIVITY_ID("ActivityID"),
    @XmlEnumValue("ReceiptID")
    RECEIPT_ID("ReceiptID"),
    @XmlEnumValue("DisbursementID")
    DISBURSEMENT_ID("DisbursementID"),
    @XmlEnumValue("VoucherID")
    VOUCHER_ID("VoucherID"),
    @XmlEnumValue("JournalEntryID")
    JOURNAL_ENTRY_ID("JournalEntryID"),
    @XmlEnumValue("Filtered")
    FILTERED("Filtered");
    private final String value;

    ActivityGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityGetType fromValue(String v) {
        for (ActivityGetType c: ActivityGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
