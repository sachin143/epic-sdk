
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.PaidItemsAdvancePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.PaidItemsReturnCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.PaidItemsReturnPremium;


/**
 * <p>Java class for DetailItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DetailItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdvancePremium" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ApplyTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToBrokerLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToCashOnAccountPaidItemsReturnCommission" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/}PaidItemsReturnCommission" minOccurs="0"/>
 *         &lt;element name="ApplyToClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToFinanceCompanyLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToOtherInterestLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyIncludeHistory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyLineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ApplyToSelectedItemsPaidItemsAdvancePremium" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/}PaidItemsAdvancePremium" minOccurs="0"/>
 *         &lt;element name="ApplyToSelectedItemsPaidItemsReturnPremium" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/}PaidItemsReturnPremium" minOccurs="0"/>
 *         &lt;element name="DebitCreditOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailItemID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/}Flags" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerScheduleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBankAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReturnPremium" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetailItem", propOrder = {
    "advancePremium",
    "amount",
    "applyTo",
    "applyToBrokerLookupCode",
    "applyToCashOnAccountPaidItemsReturnCommission",
    "applyToClientLookupCode",
    "applyToFinanceCompanyLookupCode",
    "applyToOtherInterestLookupCode",
    "applyToPolicyClientLookupCode",
    "applyToPolicyIncludeHistory",
    "applyToPolicyLineID",
    "applyToSelectedItemsPaidItemsAdvancePremium",
    "applyToSelectedItemsPaidItemsReturnPremium",
    "debitCreditOption",
    "description",
    "detailItemID",
    "flag",
    "generalLedgerAccountNumberCode",
    "generalLedgerScheduleCode",
    "generalLedgerSubAccountNumberCode",
    "invoiceDate",
    "invoiceNumber",
    "isBankAccount",
    "purchaseOrderNumber",
    "returnPremium",
    "structureAgencyCode",
    "structureBranchCode",
    "structureDepartmentCode",
    "structureProfitCenterCode"
})
public class DetailItem {

    @XmlElement(name = "AdvancePremium")
    protected Boolean advancePremium;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "ApplyTo", nillable = true)
    protected String applyTo;
    @XmlElement(name = "ApplyToBrokerLookupCode", nillable = true)
    protected String applyToBrokerLookupCode;
    @XmlElement(name = "ApplyToCashOnAccountPaidItemsReturnCommission", nillable = true)
    protected PaidItemsReturnCommission applyToCashOnAccountPaidItemsReturnCommission;
    @XmlElement(name = "ApplyToClientLookupCode", nillable = true)
    protected String applyToClientLookupCode;
    @XmlElement(name = "ApplyToFinanceCompanyLookupCode", nillable = true)
    protected String applyToFinanceCompanyLookupCode;
    @XmlElement(name = "ApplyToOtherInterestLookupCode", nillable = true)
    protected String applyToOtherInterestLookupCode;
    @XmlElement(name = "ApplyToPolicyClientLookupCode", nillable = true)
    protected String applyToPolicyClientLookupCode;
    @XmlElement(name = "ApplyToPolicyIncludeHistory")
    protected Boolean applyToPolicyIncludeHistory;
    @XmlElement(name = "ApplyToPolicyLineID")
    protected Integer applyToPolicyLineID;
    @XmlElement(name = "ApplyToSelectedItemsPaidItemsAdvancePremium", nillable = true)
    protected PaidItemsAdvancePremium applyToSelectedItemsPaidItemsAdvancePremium;
    @XmlElement(name = "ApplyToSelectedItemsPaidItemsReturnPremium", nillable = true)
    protected PaidItemsReturnPremium applyToSelectedItemsPaidItemsReturnPremium;
    @XmlElement(name = "DebitCreditOption", nillable = true)
    protected OptionType debitCreditOption;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DetailItemID")
    protected Integer detailItemID;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "GeneralLedgerAccountNumberCode", nillable = true)
    protected String generalLedgerAccountNumberCode;
    @XmlElement(name = "GeneralLedgerScheduleCode", nillable = true)
    protected String generalLedgerScheduleCode;
    @XmlElement(name = "GeneralLedgerSubAccountNumberCode", nillable = true)
    protected String generalLedgerSubAccountNumberCode;
    @XmlElement(name = "InvoiceDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceDate;
    @XmlElement(name = "InvoiceNumber", nillable = true)
    protected String invoiceNumber;
    @XmlElement(name = "IsBankAccount")
    protected Boolean isBankAccount;
    @XmlElement(name = "PurchaseOrderNumber", nillable = true)
    protected String purchaseOrderNumber;
    @XmlElement(name = "ReturnPremium")
    protected Boolean returnPremium;
    @XmlElement(name = "StructureAgencyCode", nillable = true)
    protected String structureAgencyCode;
    @XmlElement(name = "StructureBranchCode", nillable = true)
    protected String structureBranchCode;
    @XmlElement(name = "StructureDepartmentCode", nillable = true)
    protected String structureDepartmentCode;
    @XmlElement(name = "StructureProfitCenterCode", nillable = true)
    protected String structureProfitCenterCode;

    /**
     * Gets the value of the advancePremium property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdvancePremium() {
        return advancePremium;
    }

    /**
     * Sets the value of the advancePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdvancePremium(Boolean value) {
        this.advancePremium = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the applyTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyTo() {
        return applyTo;
    }

    /**
     * Sets the value of the applyTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyTo(String value) {
        this.applyTo = value;
    }

    /**
     * Gets the value of the applyToBrokerLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToBrokerLookupCode() {
        return applyToBrokerLookupCode;
    }

    /**
     * Sets the value of the applyToBrokerLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToBrokerLookupCode(String value) {
        this.applyToBrokerLookupCode = value;
    }

    /**
     * Gets the value of the applyToCashOnAccountPaidItemsReturnCommission property.
     * 
     * @return
     *     possible object is
     *     {@link PaidItemsReturnCommission }
     *     
     */
    public PaidItemsReturnCommission getApplyToCashOnAccountPaidItemsReturnCommission() {
        return applyToCashOnAccountPaidItemsReturnCommission;
    }

    /**
     * Sets the value of the applyToCashOnAccountPaidItemsReturnCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidItemsReturnCommission }
     *     
     */
    public void setApplyToCashOnAccountPaidItemsReturnCommission(PaidItemsReturnCommission value) {
        this.applyToCashOnAccountPaidItemsReturnCommission = value;
    }

    /**
     * Gets the value of the applyToClientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToClientLookupCode() {
        return applyToClientLookupCode;
    }

    /**
     * Sets the value of the applyToClientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToClientLookupCode(String value) {
        this.applyToClientLookupCode = value;
    }

    /**
     * Gets the value of the applyToFinanceCompanyLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToFinanceCompanyLookupCode() {
        return applyToFinanceCompanyLookupCode;
    }

    /**
     * Sets the value of the applyToFinanceCompanyLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToFinanceCompanyLookupCode(String value) {
        this.applyToFinanceCompanyLookupCode = value;
    }

    /**
     * Gets the value of the applyToOtherInterestLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToOtherInterestLookupCode() {
        return applyToOtherInterestLookupCode;
    }

    /**
     * Sets the value of the applyToOtherInterestLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToOtherInterestLookupCode(String value) {
        this.applyToOtherInterestLookupCode = value;
    }

    /**
     * Gets the value of the applyToPolicyClientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToPolicyClientLookupCode() {
        return applyToPolicyClientLookupCode;
    }

    /**
     * Sets the value of the applyToPolicyClientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToPolicyClientLookupCode(String value) {
        this.applyToPolicyClientLookupCode = value;
    }

    /**
     * Gets the value of the applyToPolicyIncludeHistory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyToPolicyIncludeHistory() {
        return applyToPolicyIncludeHistory;
    }

    /**
     * Sets the value of the applyToPolicyIncludeHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyToPolicyIncludeHistory(Boolean value) {
        this.applyToPolicyIncludeHistory = value;
    }

    /**
     * Gets the value of the applyToPolicyLineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getApplyToPolicyLineID() {
        return applyToPolicyLineID;
    }

    /**
     * Sets the value of the applyToPolicyLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setApplyToPolicyLineID(Integer value) {
        this.applyToPolicyLineID = value;
    }

    /**
     * Gets the value of the applyToSelectedItemsPaidItemsAdvancePremium property.
     * 
     * @return
     *     possible object is
     *     {@link PaidItemsAdvancePremium }
     *     
     */
    public PaidItemsAdvancePremium getApplyToSelectedItemsPaidItemsAdvancePremium() {
        return applyToSelectedItemsPaidItemsAdvancePremium;
    }

    /**
     * Sets the value of the applyToSelectedItemsPaidItemsAdvancePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidItemsAdvancePremium }
     *     
     */
    public void setApplyToSelectedItemsPaidItemsAdvancePremium(PaidItemsAdvancePremium value) {
        this.applyToSelectedItemsPaidItemsAdvancePremium = value;
    }

    /**
     * Gets the value of the applyToSelectedItemsPaidItemsReturnPremium property.
     * 
     * @return
     *     possible object is
     *     {@link PaidItemsReturnPremium }
     *     
     */
    public PaidItemsReturnPremium getApplyToSelectedItemsPaidItemsReturnPremium() {
        return applyToSelectedItemsPaidItemsReturnPremium;
    }

    /**
     * Sets the value of the applyToSelectedItemsPaidItemsReturnPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidItemsReturnPremium }
     *     
     */
    public void setApplyToSelectedItemsPaidItemsReturnPremium(PaidItemsReturnPremium value) {
        this.applyToSelectedItemsPaidItemsReturnPremium = value;
    }

    /**
     * Gets the value of the debitCreditOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getDebitCreditOption() {
        return debitCreditOption;
    }

    /**
     * Sets the value of the debitCreditOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setDebitCreditOption(OptionType value) {
        this.debitCreditOption = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailItemID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDetailItemID() {
        return detailItemID;
    }

    /**
     * Sets the value of the detailItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDetailItemID(Integer value) {
        this.detailItemID = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the generalLedgerAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerAccountNumberCode() {
        return generalLedgerAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerAccountNumberCode(String value) {
        this.generalLedgerAccountNumberCode = value;
    }

    /**
     * Gets the value of the generalLedgerScheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerScheduleCode() {
        return generalLedgerScheduleCode;
    }

    /**
     * Sets the value of the generalLedgerScheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerScheduleCode(String value) {
        this.generalLedgerScheduleCode = value;
    }

    /**
     * Gets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerSubAccountNumberCode() {
        return generalLedgerSubAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerSubAccountNumberCode(String value) {
        this.generalLedgerSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceDate(XMLGregorianCalendar value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the isBankAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBankAccount() {
        return isBankAccount;
    }

    /**
     * Sets the value of the isBankAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBankAccount(Boolean value) {
        this.isBankAccount = value;
    }

    /**
     * Gets the value of the purchaseOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * Sets the value of the purchaseOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderNumber(String value) {
        this.purchaseOrderNumber = value;
    }

    /**
     * Gets the value of the returnPremium property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnPremium() {
        return returnPremium;
    }

    /**
     * Sets the value of the returnPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnPremium(Boolean value) {
        this.returnPremium = value;
    }

    /**
     * Gets the value of the structureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureAgencyCode() {
        return structureAgencyCode;
    }

    /**
     * Sets the value of the structureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureAgencyCode(String value) {
        this.structureAgencyCode = value;
    }

    /**
     * Gets the value of the structureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureBranchCode() {
        return structureBranchCode;
    }

    /**
     * Sets the value of the structureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureBranchCode(String value) {
        this.structureBranchCode = value;
    }

    /**
     * Gets the value of the structureDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureDepartmentCode() {
        return structureDepartmentCode;
    }

    /**
     * Sets the value of the structureDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureDepartmentCode(String value) {
        this.structureDepartmentCode = value;
    }

    /**
     * Gets the value of the structureProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureProfitCenterCode() {
        return structureProfitCenterCode;
    }

    /**
     * Sets the value of the structureProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureProfitCenterCode(String value) {
        this.structureProfitCenterCode = value;
    }

}
