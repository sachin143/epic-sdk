
package com.appliedsystems.schemas.epic.sdk._2018._01._account;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.SplitReceivableItems;


/**
 * <p>Java class for SplitReceivableTemplate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitReceivableTemplate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ParentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ParentPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SplitByAccountContactOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="SplitReceivableItems" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/}SplitReceivableItems" minOccurs="0"/>
 *         &lt;element name="SplitReceivableTemplateID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SplitValuePercentageAmountOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitReceivableTemplate", propOrder = {
    "clientID",
    "description",
    "isActive",
    "parentAmount",
    "parentPercentage",
    "splitByAccountContactOption",
    "splitReceivableItems",
    "splitReceivableTemplateID",
    "splitValuePercentageAmountOption",
    "timestamp",
    "totalAmount"
})
public class SplitReceivableTemplate {

    @XmlElement(name = "ClientID")
    protected Integer clientID;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "IsActive", nillable = true)
    protected Boolean isActive;
    @XmlElement(name = "ParentAmount", nillable = true)
    protected BigDecimal parentAmount;
    @XmlElement(name = "ParentPercentage", nillable = true)
    protected BigDecimal parentPercentage;
    @XmlElement(name = "SplitByAccountContactOption", nillable = true)
    protected OptionType splitByAccountContactOption;
    @XmlElement(name = "SplitReceivableItems", nillable = true)
    protected SplitReceivableItems splitReceivableItems;
    @XmlElement(name = "SplitReceivableTemplateID")
    protected Integer splitReceivableTemplateID;
    @XmlElement(name = "SplitValuePercentageAmountOption", nillable = true)
    protected OptionType splitValuePercentageAmountOption;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "TotalAmount", nillable = true)
    protected BigDecimal totalAmount;

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the parentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParentAmount() {
        return parentAmount;
    }

    /**
     * Sets the value of the parentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParentAmount(BigDecimal value) {
        this.parentAmount = value;
    }

    /**
     * Gets the value of the parentPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParentPercentage() {
        return parentPercentage;
    }

    /**
     * Sets the value of the parentPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParentPercentage(BigDecimal value) {
        this.parentPercentage = value;
    }

    /**
     * Gets the value of the splitByAccountContactOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getSplitByAccountContactOption() {
        return splitByAccountContactOption;
    }

    /**
     * Sets the value of the splitByAccountContactOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setSplitByAccountContactOption(OptionType value) {
        this.splitByAccountContactOption = value;
    }

    /**
     * Gets the value of the splitReceivableItems property.
     * 
     * @return
     *     possible object is
     *     {@link SplitReceivableItems }
     *     
     */
    public SplitReceivableItems getSplitReceivableItems() {
        return splitReceivableItems;
    }

    /**
     * Sets the value of the splitReceivableItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitReceivableItems }
     *     
     */
    public void setSplitReceivableItems(SplitReceivableItems value) {
        this.splitReceivableItems = value;
    }

    /**
     * Gets the value of the splitReceivableTemplateID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSplitReceivableTemplateID() {
        return splitReceivableTemplateID;
    }

    /**
     * Sets the value of the splitReceivableTemplateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSplitReceivableTemplateID(Integer value) {
        this.splitReceivableTemplateID = value;
    }

    /**
     * Gets the value of the splitValuePercentageAmountOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getSplitValuePercentageAmountOption() {
        return splitValuePercentageAmountOption;
    }

    /**
     * Sets the value of the splitValuePercentageAmountOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setSplitValuePercentageAmountOption(OptionType value) {
        this.splitValuePercentageAmountOption = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

}
