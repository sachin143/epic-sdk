
package com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SearchStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="Unpaid"/>
 *     &lt;enumeration value="Search"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SearchStatus", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/reconciliation/_directbillcommission/")
@XmlEnum
public enum SearchStatus {

    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("Unpaid")
    UNPAID("Unpaid"),
    @XmlEnumValue("Search")
    SEARCH("Search");
    private final String value;

    SearchStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchStatus fromValue(String v) {
        for (SearchStatus c: SearchStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
