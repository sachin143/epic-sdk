
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdjustorGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AdjustorGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AdjustorID"/>
 *     &lt;enumeration value="ClaimID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AdjustorGetType")
@XmlEnum
public enum AdjustorGetType {

    @XmlEnumValue("AdjustorID")
    ADJUSTOR_ID("AdjustorID"),
    @XmlEnumValue("ClaimID")
    CLAIM_ID("ClaimID");
    private final String value;

    AdjustorGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdjustorGetType fromValue(String v) {
        for (AdjustorGetType c: AdjustorGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
