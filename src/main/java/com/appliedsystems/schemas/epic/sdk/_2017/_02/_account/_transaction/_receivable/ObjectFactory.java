
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReceivableItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", "ReceivableItems");
    private final static QName _ReceivableBalanceItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", "ReceivableBalanceItems");
    private final static QName _ReceivableItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", "ReceivableItem");
    private final static QName _ReceivableBalanceItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", "ReceivableBalanceItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReceivableBalanceItems }
     * 
     */
    public ReceivableBalanceItems createReceivableBalanceItems() {
        return new ReceivableBalanceItems();
    }

    /**
     * Create an instance of {@link ReceivableBalanceItem }
     * 
     */
    public ReceivableBalanceItem createReceivableBalanceItem() {
        return new ReceivableBalanceItem();
    }

    /**
     * Create an instance of {@link ReceivableItem }
     * 
     */
    public ReceivableItem createReceivableItem() {
        return new ReceivableItem();
    }

    /**
     * Create an instance of {@link ReceivableItems }
     * 
     */
    public ReceivableItems createReceivableItems() {
        return new ReceivableItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", name = "ReceivableItems")
    public JAXBElement<ReceivableItems> createReceivableItems(ReceivableItems value) {
        return new JAXBElement<ReceivableItems>(_ReceivableItems_QNAME, ReceivableItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableBalanceItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", name = "ReceivableBalanceItems")
    public JAXBElement<ReceivableBalanceItems> createReceivableBalanceItems(ReceivableBalanceItems value) {
        return new JAXBElement<ReceivableBalanceItems>(_ReceivableBalanceItems_QNAME, ReceivableBalanceItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", name = "ReceivableItem")
    public JAXBElement<ReceivableItem> createReceivableItem(ReceivableItem value) {
        return new JAXBElement<ReceivableItem>(_ReceivableItem_QNAME, ReceivableItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableBalanceItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/", name = "ReceivableBalanceItem")
    public JAXBElement<ReceivableBalanceItem> createReceivableBalanceItem(ReceivableBalanceItem value) {
        return new JAXBElement<ReceivableBalanceItem>(_ReceivableBalanceItem_QNAME, ReceivableBalanceItem.class, null, value);
    }

}
