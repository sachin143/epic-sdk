
package com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/", "ComparisonType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_get/_contactfilter/", name = "ComparisonType")
    public JAXBElement<ComparisonType> createComparisonType(ComparisonType value) {
        return new JAXBElement<ComparisonType>(_ComparisonType_QNAME, ComparisonType.class, null, value);
    }

}
