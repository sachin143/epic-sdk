
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisbursementGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DisbursementGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DisbursementID"/>
 *     &lt;enumeration value="Filtered"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DisbursementGetType")
@XmlEnum
public enum DisbursementGetType {

    @XmlEnumValue("DisbursementID")
    DISBURSEMENT_ID("DisbursementID"),
    @XmlEnumValue("Filtered")
    FILTERED("Filtered");
    private final String value;

    DisbursementGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DisbursementGetType fromValue(String v) {
        for (DisbursementGetType c: DisbursementGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
