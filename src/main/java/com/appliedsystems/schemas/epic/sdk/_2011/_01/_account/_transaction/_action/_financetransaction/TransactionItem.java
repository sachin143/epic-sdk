
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.Flags;


/**
 * <p>Java class for TransactionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinanceDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FinanceFlatAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FinanceGenerateInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FinancePercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FinancePercentageFlatAmountOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_financetransaction/_transactionitem/}Flags" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToDeliveryMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToFaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToFaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendInvoiceToLoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionItem", propOrder = {
    "financeDescription",
    "financeFlatAmount",
    "financeGenerateInvoice",
    "financePercentage",
    "financePercentageFlatAmountOption",
    "flag",
    "sendInvoiceToAccountLookupCode",
    "sendInvoiceToAddress",
    "sendInvoiceToContact",
    "sendInvoiceToContactID",
    "sendInvoiceToDeliveryMethod",
    "sendInvoiceToEmail",
    "sendInvoiceToFaxCountryCode",
    "sendInvoiceToFaxExtension",
    "sendInvoiceToFaxNumber",
    "sendInvoiceToLoanNumber",
    "transactionID"
})
public class TransactionItem {

    @XmlElement(name = "FinanceDescription", nillable = true)
    protected String financeDescription;
    @XmlElement(name = "FinanceFlatAmount")
    protected BigDecimal financeFlatAmount;
    @XmlElement(name = "FinanceGenerateInvoice")
    protected Boolean financeGenerateInvoice;
    @XmlElement(name = "FinancePercentage")
    protected BigDecimal financePercentage;
    @XmlElement(name = "FinancePercentageFlatAmountOption", nillable = true)
    protected OptionType financePercentageFlatAmountOption;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "SendInvoiceToAccountLookupCode", nillable = true)
    protected String sendInvoiceToAccountLookupCode;
    @XmlElement(name = "SendInvoiceToAddress", nillable = true)
    protected Address sendInvoiceToAddress;
    @XmlElement(name = "SendInvoiceToContact", nillable = true)
    protected String sendInvoiceToContact;
    @XmlElement(name = "SendInvoiceToContactID")
    protected Integer sendInvoiceToContactID;
    @XmlElement(name = "SendInvoiceToDeliveryMethod", nillable = true)
    protected String sendInvoiceToDeliveryMethod;
    @XmlElement(name = "SendInvoiceToEmail", nillable = true)
    protected String sendInvoiceToEmail;
    @XmlElement(name = "SendInvoiceToFaxCountryCode", nillable = true)
    protected String sendInvoiceToFaxCountryCode;
    @XmlElement(name = "SendInvoiceToFaxExtension", nillable = true)
    protected String sendInvoiceToFaxExtension;
    @XmlElement(name = "SendInvoiceToFaxNumber", nillable = true)
    protected String sendInvoiceToFaxNumber;
    @XmlElement(name = "SendInvoiceToLoanNumber", nillable = true)
    protected String sendInvoiceToLoanNumber;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;

    /**
     * Gets the value of the financeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinanceDescription() {
        return financeDescription;
    }

    /**
     * Sets the value of the financeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinanceDescription(String value) {
        this.financeDescription = value;
    }

    /**
     * Gets the value of the financeFlatAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFinanceFlatAmount() {
        return financeFlatAmount;
    }

    /**
     * Sets the value of the financeFlatAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFinanceFlatAmount(BigDecimal value) {
        this.financeFlatAmount = value;
    }

    /**
     * Gets the value of the financeGenerateInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinanceGenerateInvoice() {
        return financeGenerateInvoice;
    }

    /**
     * Sets the value of the financeGenerateInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinanceGenerateInvoice(Boolean value) {
        this.financeGenerateInvoice = value;
    }

    /**
     * Gets the value of the financePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFinancePercentage() {
        return financePercentage;
    }

    /**
     * Sets the value of the financePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFinancePercentage(BigDecimal value) {
        this.financePercentage = value;
    }

    /**
     * Gets the value of the financePercentageFlatAmountOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getFinancePercentageFlatAmountOption() {
        return financePercentageFlatAmountOption;
    }

    /**
     * Sets the value of the financePercentageFlatAmountOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setFinancePercentageFlatAmountOption(OptionType value) {
        this.financePercentageFlatAmountOption = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the sendInvoiceToAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToAccountLookupCode() {
        return sendInvoiceToAccountLookupCode;
    }

    /**
     * Sets the value of the sendInvoiceToAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToAccountLookupCode(String value) {
        this.sendInvoiceToAccountLookupCode = value;
    }

    /**
     * Gets the value of the sendInvoiceToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getSendInvoiceToAddress() {
        return sendInvoiceToAddress;
    }

    /**
     * Sets the value of the sendInvoiceToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setSendInvoiceToAddress(Address value) {
        this.sendInvoiceToAddress = value;
    }

    /**
     * Gets the value of the sendInvoiceToContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToContact() {
        return sendInvoiceToContact;
    }

    /**
     * Sets the value of the sendInvoiceToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToContact(String value) {
        this.sendInvoiceToContact = value;
    }

    /**
     * Gets the value of the sendInvoiceToContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSendInvoiceToContactID() {
        return sendInvoiceToContactID;
    }

    /**
     * Sets the value of the sendInvoiceToContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSendInvoiceToContactID(Integer value) {
        this.sendInvoiceToContactID = value;
    }

    /**
     * Gets the value of the sendInvoiceToDeliveryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToDeliveryMethod() {
        return sendInvoiceToDeliveryMethod;
    }

    /**
     * Sets the value of the sendInvoiceToDeliveryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToDeliveryMethod(String value) {
        this.sendInvoiceToDeliveryMethod = value;
    }

    /**
     * Gets the value of the sendInvoiceToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToEmail() {
        return sendInvoiceToEmail;
    }

    /**
     * Sets the value of the sendInvoiceToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToEmail(String value) {
        this.sendInvoiceToEmail = value;
    }

    /**
     * Gets the value of the sendInvoiceToFaxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToFaxCountryCode() {
        return sendInvoiceToFaxCountryCode;
    }

    /**
     * Sets the value of the sendInvoiceToFaxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToFaxCountryCode(String value) {
        this.sendInvoiceToFaxCountryCode = value;
    }

    /**
     * Gets the value of the sendInvoiceToFaxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToFaxExtension() {
        return sendInvoiceToFaxExtension;
    }

    /**
     * Sets the value of the sendInvoiceToFaxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToFaxExtension(String value) {
        this.sendInvoiceToFaxExtension = value;
    }

    /**
     * Gets the value of the sendInvoiceToFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToFaxNumber() {
        return sendInvoiceToFaxNumber;
    }

    /**
     * Sets the value of the sendInvoiceToFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToFaxNumber(String value) {
        this.sendInvoiceToFaxNumber = value;
    }

    /**
     * Gets the value of the sendInvoiceToLoanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendInvoiceToLoanNumber() {
        return sendInvoiceToLoanNumber;
    }

    /**
     * Sets the value of the sendInvoiceToLoanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendInvoiceToLoanNumber(String value) {
        this.sendInvoiceToLoanNumber = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

}
