
package com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.LineItems;


/**
 * <p>Java class for UpdateStageToSubmitted complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateStageToSubmitted">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LineItems" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/_updatestagetosubmitted/}LineItems" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateStageToSubmitted", propOrder = {
    "lineItems",
    "policyID"
})
public class UpdateStageToSubmitted {

    @XmlElement(name = "LineItems", nillable = true)
    protected LineItems lineItems;
    @XmlElement(name = "PolicyID")
    protected Integer policyID;

    /**
     * Gets the value of the lineItems property.
     * 
     * @return
     *     possible object is
     *     {@link LineItems }
     *     
     */
    public LineItems getLineItems() {
        return lineItems;
    }

    /**
     * Sets the value of the lineItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItems }
     *     
     */
    public void setLineItems(LineItems value) {
        this.lineItems = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

}
