
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyLineTypeGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyLineTypeGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PolicyLineTypeID"/>
 *     &lt;enumeration value="PolicyLineTypeCode"/>
 *     &lt;enumeration value="All"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PolicyLineTypeGetType")
@XmlEnum
public enum PolicyLineTypeGetType {

    @XmlEnumValue("PolicyLineTypeID")
    POLICY_LINE_TYPE_ID("PolicyLineTypeID"),
    @XmlEnumValue("PolicyLineTypeCode")
    POLICY_LINE_TYPE_CODE("PolicyLineTypeCode"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    PolicyLineTypeGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyLineTypeGetType fromValue(String v) {
        for (PolicyLineTypeGetType c: PolicyLineTypeGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
