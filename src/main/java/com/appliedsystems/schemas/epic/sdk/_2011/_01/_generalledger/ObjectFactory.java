
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ActivityGetLimitType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ActivityGetLimitType");
    private final static QName _JournalEntry_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "JournalEntry");
    private final static QName _VoucherComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "VoucherComparisonType");
    private final static QName _ReceiptComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ReceiptComparisonType");
    private final static QName _DisbursementGetLimitType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "DisbursementGetLimitType");
    private final static QName _ActivityComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ActivityComparisonType");
    private final static QName _Receipt_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "Receipt");
    private final static QName _ArrayOfJournalEntry_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ArrayOfJournalEntry");
    private final static QName _ReceiptGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ReceiptGetType");
    private final static QName _DisbursementFilterType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "DisbursementFilterType");
    private final static QName _VoucherGetLimitType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "VoucherGetLimitType");
    private final static QName _DisbursementComparisonType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "DisbursementComparisonType");
    private final static QName _ArrayOfReceipt_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ArrayOfReceipt");
    private final static QName _VoucherGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "VoucherGetType");
    private final static QName _DisbursementGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "DisbursementGetType");
    private final static QName _ArrayOfDisbursement_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ArrayOfDisbursement");
    private final static QName _ReceiptFilterType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ReceiptFilterType");
    private final static QName _Disbursement_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "Disbursement");
    private final static QName _VoucherFilterType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "VoucherFilterType");
    private final static QName _ReceiptGetLimitType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ReceiptGetLimitType");
    private final static QName _Voucher_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "Voucher");
    private final static QName _ArrayOfVoucher_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ArrayOfVoucher");
    private final static QName _ActivityGetFilterType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ActivityGetFilterType");
    private final static QName _ActivityGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", "ActivityGetType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Receipt }
     * 
     */
    public Receipt createReceipt() {
        return new Receipt();
    }

    /**
     * Create an instance of {@link ArrayOfVoucher }
     * 
     */
    public ArrayOfVoucher createArrayOfVoucher() {
        return new ArrayOfVoucher();
    }

    /**
     * Create an instance of {@link ArrayOfDisbursement }
     * 
     */
    public ArrayOfDisbursement createArrayOfDisbursement() {
        return new ArrayOfDisbursement();
    }

    /**
     * Create an instance of {@link JournalEntry }
     * 
     */
    public JournalEntry createJournalEntry() {
        return new JournalEntry();
    }

    /**
     * Create an instance of {@link Voucher }
     * 
     */
    public Voucher createVoucher() {
        return new Voucher();
    }

    /**
     * Create an instance of {@link ArrayOfReceipt }
     * 
     */
    public ArrayOfReceipt createArrayOfReceipt() {
        return new ArrayOfReceipt();
    }

    /**
     * Create an instance of {@link Disbursement }
     * 
     */
    public Disbursement createDisbursement() {
        return new Disbursement();
    }

    /**
     * Create an instance of {@link ArrayOfJournalEntry }
     * 
     */
    public ArrayOfJournalEntry createArrayOfJournalEntry() {
        return new ArrayOfJournalEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityGetLimitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ActivityGetLimitType")
    public JAXBElement<ActivityGetLimitType> createActivityGetLimitType(ActivityGetLimitType value) {
        return new JAXBElement<ActivityGetLimitType>(_ActivityGetLimitType_QNAME, ActivityGetLimitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JournalEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "JournalEntry")
    public JAXBElement<JournalEntry> createJournalEntry(JournalEntry value) {
        return new JAXBElement<JournalEntry>(_JournalEntry_QNAME, JournalEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "VoucherComparisonType")
    public JAXBElement<VoucherComparisonType> createVoucherComparisonType(VoucherComparisonType value) {
        return new JAXBElement<VoucherComparisonType>(_VoucherComparisonType_QNAME, VoucherComparisonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ReceiptComparisonType")
    public JAXBElement<ReceiptComparisonType> createReceiptComparisonType(ReceiptComparisonType value) {
        return new JAXBElement<ReceiptComparisonType>(_ReceiptComparisonType_QNAME, ReceiptComparisonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementGetLimitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "DisbursementGetLimitType")
    public JAXBElement<DisbursementGetLimitType> createDisbursementGetLimitType(DisbursementGetLimitType value) {
        return new JAXBElement<DisbursementGetLimitType>(_DisbursementGetLimitType_QNAME, DisbursementGetLimitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ActivityComparisonType")
    public JAXBElement<ActivityComparisonType> createActivityComparisonType(ActivityComparisonType value) {
        return new JAXBElement<ActivityComparisonType>(_ActivityComparisonType_QNAME, ActivityComparisonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Receipt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "Receipt")
    public JAXBElement<Receipt> createReceipt(Receipt value) {
        return new JAXBElement<Receipt>(_Receipt_QNAME, Receipt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfJournalEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ArrayOfJournalEntry")
    public JAXBElement<ArrayOfJournalEntry> createArrayOfJournalEntry(ArrayOfJournalEntry value) {
        return new JAXBElement<ArrayOfJournalEntry>(_ArrayOfJournalEntry_QNAME, ArrayOfJournalEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ReceiptGetType")
    public JAXBElement<ReceiptGetType> createReceiptGetType(ReceiptGetType value) {
        return new JAXBElement<ReceiptGetType>(_ReceiptGetType_QNAME, ReceiptGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "DisbursementFilterType")
    public JAXBElement<DisbursementFilterType> createDisbursementFilterType(DisbursementFilterType value) {
        return new JAXBElement<DisbursementFilterType>(_DisbursementFilterType_QNAME, DisbursementFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherGetLimitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "VoucherGetLimitType")
    public JAXBElement<VoucherGetLimitType> createVoucherGetLimitType(VoucherGetLimitType value) {
        return new JAXBElement<VoucherGetLimitType>(_VoucherGetLimitType_QNAME, VoucherGetLimitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementComparisonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "DisbursementComparisonType")
    public JAXBElement<DisbursementComparisonType> createDisbursementComparisonType(DisbursementComparisonType value) {
        return new JAXBElement<DisbursementComparisonType>(_DisbursementComparisonType_QNAME, DisbursementComparisonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReceipt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ArrayOfReceipt")
    public JAXBElement<ArrayOfReceipt> createArrayOfReceipt(ArrayOfReceipt value) {
        return new JAXBElement<ArrayOfReceipt>(_ArrayOfReceipt_QNAME, ArrayOfReceipt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "VoucherGetType")
    public JAXBElement<VoucherGetType> createVoucherGetType(VoucherGetType value) {
        return new JAXBElement<VoucherGetType>(_VoucherGetType_QNAME, VoucherGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "DisbursementGetType")
    public JAXBElement<DisbursementGetType> createDisbursementGetType(DisbursementGetType value) {
        return new JAXBElement<DisbursementGetType>(_DisbursementGetType_QNAME, DisbursementGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDisbursement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ArrayOfDisbursement")
    public JAXBElement<ArrayOfDisbursement> createArrayOfDisbursement(ArrayOfDisbursement value) {
        return new JAXBElement<ArrayOfDisbursement>(_ArrayOfDisbursement_QNAME, ArrayOfDisbursement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ReceiptFilterType")
    public JAXBElement<ReceiptFilterType> createReceiptFilterType(ReceiptFilterType value) {
        return new JAXBElement<ReceiptFilterType>(_ReceiptFilterType_QNAME, ReceiptFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Disbursement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "Disbursement")
    public JAXBElement<Disbursement> createDisbursement(Disbursement value) {
        return new JAXBElement<Disbursement>(_Disbursement_QNAME, Disbursement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "VoucherFilterType")
    public JAXBElement<VoucherFilterType> createVoucherFilterType(VoucherFilterType value) {
        return new JAXBElement<VoucherFilterType>(_VoucherFilterType_QNAME, VoucherFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptGetLimitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ReceiptGetLimitType")
    public JAXBElement<ReceiptGetLimitType> createReceiptGetLimitType(ReceiptGetLimitType value) {
        return new JAXBElement<ReceiptGetLimitType>(_ReceiptGetLimitType_QNAME, ReceiptGetLimitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Voucher }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "Voucher")
    public JAXBElement<Voucher> createVoucher(Voucher value) {
        return new JAXBElement<Voucher>(_Voucher_QNAME, Voucher.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVoucher }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ArrayOfVoucher")
    public JAXBElement<ArrayOfVoucher> createArrayOfVoucher(ArrayOfVoucher value) {
        return new JAXBElement<ArrayOfVoucher>(_ArrayOfVoucher_QNAME, ArrayOfVoucher.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityGetFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ActivityGetFilterType")
    public JAXBElement<ActivityGetFilterType> createActivityGetFilterType(ActivityGetFilterType value) {
        return new JAXBElement<ActivityGetFilterType>(_ActivityGetFilterType_QNAME, ActivityGetFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/", name = "ActivityGetType")
    public JAXBElement<ActivityGetType> createActivityGetType(ActivityGetType value) {
        return new JAXBElement<ActivityGetType>(_ActivityGetType_QNAME, ActivityGetType.class, null, value);
    }

}
