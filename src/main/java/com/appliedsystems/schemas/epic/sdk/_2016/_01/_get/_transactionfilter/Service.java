
package com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Service.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Service">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Include"/>
 *     &lt;enumeration value="Only"/>
 *     &lt;enumeration value="Exclude"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Service", namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/")
@XmlEnum
public enum Service {

    @XmlEnumValue("Include")
    INCLUDE("Include"),
    @XmlEnumValue("Only")
    ONLY("Only"),
    @XmlEnumValue("Exclude")
    EXCLUDE("Exclude");
    private final String value;

    Service(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Service fromValue(String v) {
        for (Service c: Service.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
