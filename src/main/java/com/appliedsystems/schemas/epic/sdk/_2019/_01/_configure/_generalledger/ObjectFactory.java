
package com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfChartOfAccount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", "ArrayOfChartOfAccount");
    private final static QName _ChartOfAccount_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", "ChartOfAccount");
    private final static QName _ChartOfAccountGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", "ChartOfAccountGetType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ChartOfAccount }
     * 
     */
    public ChartOfAccount createChartOfAccount() {
        return new ChartOfAccount();
    }

    /**
     * Create an instance of {@link ArrayOfChartOfAccount }
     * 
     */
    public ArrayOfChartOfAccount createArrayOfChartOfAccount() {
        return new ArrayOfChartOfAccount();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChartOfAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", name = "ArrayOfChartOfAccount")
    public JAXBElement<ArrayOfChartOfAccount> createArrayOfChartOfAccount(ArrayOfChartOfAccount value) {
        return new JAXBElement<ArrayOfChartOfAccount>(_ArrayOfChartOfAccount_QNAME, ArrayOfChartOfAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChartOfAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", name = "ChartOfAccount")
    public JAXBElement<ChartOfAccount> createChartOfAccount(ChartOfAccount value) {
        return new JAXBElement<ChartOfAccount>(_ChartOfAccount_QNAME, ChartOfAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChartOfAccountGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/", name = "ChartOfAccountGetType")
    public JAXBElement<ChartOfAccountGetType> createChartOfAccountGetType(ChartOfAccountGetType value) {
        return new JAXBElement<ChartOfAccountGetType>(_ChartOfAccountGetType_QNAME, ChartOfAccountGetType.class, null, value);
    }

}
