
package com.appliedsystems.schemas.epic.sdk._2021._01._account._client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._account._client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EmployeeClassItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", "EmployeeClassItem");
    private final static QName _EmployeeBenefits_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", "EmployeeBenefits");
    private final static QName _EmployeeClassItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", "EmployeeClassItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._account._client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EmployeeBenefits }
     * 
     */
    public EmployeeBenefits createEmployeeBenefits() {
        return new EmployeeBenefits();
    }

    /**
     * Create an instance of {@link EmployeeClassItem }
     * 
     */
    public EmployeeClassItem createEmployeeClassItem() {
        return new EmployeeClassItem();
    }

    /**
     * Create an instance of {@link EmployeeClassItems }
     * 
     */
    public EmployeeClassItems createEmployeeClassItems() {
        return new EmployeeClassItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeClassItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", name = "EmployeeClassItem")
    public JAXBElement<EmployeeClassItem> createEmployeeClassItem(EmployeeClassItem value) {
        return new JAXBElement<EmployeeClassItem>(_EmployeeClassItem_QNAME, EmployeeClassItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeBenefits }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", name = "EmployeeBenefits")
    public JAXBElement<EmployeeBenefits> createEmployeeBenefits(EmployeeBenefits value) {
        return new JAXBElement<EmployeeBenefits>(_EmployeeBenefits_QNAME, EmployeeBenefits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeClassItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/", name = "EmployeeClassItems")
    public JAXBElement<EmployeeClassItems> createEmployeeClassItems(EmployeeClassItems value) {
        return new JAXBElement<EmployeeClassItems>(_EmployeeClassItems_QNAME, EmployeeClassItems.class, null, value);
    }

}
