
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.AttachmentSortType;


/**
 * <p>Java class for AttachmentSorting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttachmentSorting">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SortOrder" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}SortOrder" minOccurs="0"/>
 *         &lt;element name="SortType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_attachment/}AttachmentSortType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentSorting", propOrder = {
    "sortOrder",
    "sortType"
})
public class AttachmentSorting {

    @XmlElement(name = "SortOrder")
    @XmlSchemaType(name = "string")
    protected SortOrder sortOrder;
    @XmlElement(name = "SortType")
    @XmlSchemaType(name = "string")
    protected AttachmentSortType sortType;

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link SortOrder }
     *     
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortOrder }
     *     
     */
    public void setSortOrder(SortOrder value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the sortType property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentSortType }
     *     
     */
    public AttachmentSortType getSortType() {
        return sortType;
    }

    /**
     * Sets the value of the sortType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentSortType }
     *     
     */
    public void setSortType(AttachmentSortType value) {
        this.sortType = value;
    }

}
