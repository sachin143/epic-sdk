
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for VoidPayment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoidPayment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailChargeNSFFee" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DetailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NSFTransactionBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionGenerateInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NSFTransactionInvoiceGrouping" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionInvoiceGroupingExistingInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NSFTransactionInvoiceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToDeliveryMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToFaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToFaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NSFTransactionSendInvoiceToFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsForMissedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoidPayment", propOrder = {
    "associatedAccountID",
    "associatedAccountTypeCode",
    "detailAccountingMonth",
    "detailChargeNSFFee",
    "detailDescription",
    "nsfTransactionAgencyCode",
    "nsfTransactionAmount",
    "nsfTransactionBranchCode",
    "nsfTransactionCode",
    "nsfTransactionDepartmentCode",
    "nsfTransactionDescription",
    "nsfTransactionGenerateInvoice",
    "nsfTransactionInvoiceGrouping",
    "nsfTransactionInvoiceGroupingExistingInvoiceNumber",
    "nsfTransactionInvoiceMessage",
    "nsfTransactionProfitCenterCode",
    "nsfTransactionSendInvoiceToAddress",
    "nsfTransactionSendInvoiceToContact",
    "nsfTransactionSendInvoiceToContactID",
    "nsfTransactionSendInvoiceToDeliveryMethod",
    "nsfTransactionSendInvoiceToEmail",
    "nsfTransactionSendInvoiceToFaxCountryCode",
    "nsfTransactionSendInvoiceToFaxExtension",
    "nsfTransactionSendInvoiceToFaxNumber",
    "reason",
    "reasonDetails",
    "transactionID",
    "isForMissedPayment"
})
public class VoidPayment {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "DetailAccountingMonth", nillable = true)
    protected String detailAccountingMonth;
    @XmlElement(name = "DetailChargeNSFFee")
    protected Boolean detailChargeNSFFee;
    @XmlElement(name = "DetailDescription", nillable = true)
    protected String detailDescription;
    @XmlElement(name = "NSFTransactionAgencyCode", nillable = true)
    protected String nsfTransactionAgencyCode;
    @XmlElement(name = "NSFTransactionAmount", nillable = true)
    protected BigDecimal nsfTransactionAmount;
    @XmlElement(name = "NSFTransactionBranchCode", nillable = true)
    protected String nsfTransactionBranchCode;
    @XmlElement(name = "NSFTransactionCode", nillable = true)
    protected String nsfTransactionCode;
    @XmlElement(name = "NSFTransactionDepartmentCode", nillable = true)
    protected String nsfTransactionDepartmentCode;
    @XmlElement(name = "NSFTransactionDescription", nillable = true)
    protected String nsfTransactionDescription;
    @XmlElement(name = "NSFTransactionGenerateInvoice")
    protected Boolean nsfTransactionGenerateInvoice;
    @XmlElement(name = "NSFTransactionInvoiceGrouping", nillable = true)
    protected String nsfTransactionInvoiceGrouping;
    @XmlElement(name = "NSFTransactionInvoiceGroupingExistingInvoiceNumber", nillable = true)
    protected Integer nsfTransactionInvoiceGroupingExistingInvoiceNumber;
    @XmlElement(name = "NSFTransactionInvoiceMessage", nillable = true)
    protected String nsfTransactionInvoiceMessage;
    @XmlElement(name = "NSFTransactionProfitCenterCode", nillable = true)
    protected String nsfTransactionProfitCenterCode;
    @XmlElement(name = "NSFTransactionSendInvoiceToAddress", nillable = true)
    protected Address nsfTransactionSendInvoiceToAddress;
    @XmlElement(name = "NSFTransactionSendInvoiceToContact", nillable = true)
    protected String nsfTransactionSendInvoiceToContact;
    @XmlElement(name = "NSFTransactionSendInvoiceToContactID")
    protected Integer nsfTransactionSendInvoiceToContactID;
    @XmlElement(name = "NSFTransactionSendInvoiceToDeliveryMethod", nillable = true)
    protected String nsfTransactionSendInvoiceToDeliveryMethod;
    @XmlElement(name = "NSFTransactionSendInvoiceToEmail", nillable = true)
    protected String nsfTransactionSendInvoiceToEmail;
    @XmlElement(name = "NSFTransactionSendInvoiceToFaxCountryCode", nillable = true)
    protected String nsfTransactionSendInvoiceToFaxCountryCode;
    @XmlElement(name = "NSFTransactionSendInvoiceToFaxExtension", nillable = true)
    protected String nsfTransactionSendInvoiceToFaxExtension;
    @XmlElement(name = "NSFTransactionSendInvoiceToFaxNumber", nillable = true)
    protected String nsfTransactionSendInvoiceToFaxNumber;
    @XmlElement(name = "Reason", nillable = true)
    protected String reason;
    @XmlElement(name = "ReasonDetails", nillable = true)
    protected String reasonDetails;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "IsForMissedPayment", nillable = true)
    protected Boolean isForMissedPayment;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the detailAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailAccountingMonth() {
        return detailAccountingMonth;
    }

    /**
     * Sets the value of the detailAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailAccountingMonth(String value) {
        this.detailAccountingMonth = value;
    }

    /**
     * Gets the value of the detailChargeNSFFee property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDetailChargeNSFFee() {
        return detailChargeNSFFee;
    }

    /**
     * Sets the value of the detailChargeNSFFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDetailChargeNSFFee(Boolean value) {
        this.detailChargeNSFFee = value;
    }

    /**
     * Gets the value of the detailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescription() {
        return detailDescription;
    }

    /**
     * Sets the value of the detailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescription(String value) {
        this.detailDescription = value;
    }

    /**
     * Gets the value of the nsfTransactionAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionAgencyCode() {
        return nsfTransactionAgencyCode;
    }

    /**
     * Sets the value of the nsfTransactionAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionAgencyCode(String value) {
        this.nsfTransactionAgencyCode = value;
    }

    /**
     * Gets the value of the nsfTransactionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNSFTransactionAmount() {
        return nsfTransactionAmount;
    }

    /**
     * Sets the value of the nsfTransactionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNSFTransactionAmount(BigDecimal value) {
        this.nsfTransactionAmount = value;
    }

    /**
     * Gets the value of the nsfTransactionBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionBranchCode() {
        return nsfTransactionBranchCode;
    }

    /**
     * Sets the value of the nsfTransactionBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionBranchCode(String value) {
        this.nsfTransactionBranchCode = value;
    }

    /**
     * Gets the value of the nsfTransactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionCode() {
        return nsfTransactionCode;
    }

    /**
     * Sets the value of the nsfTransactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionCode(String value) {
        this.nsfTransactionCode = value;
    }

    /**
     * Gets the value of the nsfTransactionDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionDepartmentCode() {
        return nsfTransactionDepartmentCode;
    }

    /**
     * Sets the value of the nsfTransactionDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionDepartmentCode(String value) {
        this.nsfTransactionDepartmentCode = value;
    }

    /**
     * Gets the value of the nsfTransactionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionDescription() {
        return nsfTransactionDescription;
    }

    /**
     * Sets the value of the nsfTransactionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionDescription(String value) {
        this.nsfTransactionDescription = value;
    }

    /**
     * Gets the value of the nsfTransactionGenerateInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNSFTransactionGenerateInvoice() {
        return nsfTransactionGenerateInvoice;
    }

    /**
     * Sets the value of the nsfTransactionGenerateInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNSFTransactionGenerateInvoice(Boolean value) {
        this.nsfTransactionGenerateInvoice = value;
    }

    /**
     * Gets the value of the nsfTransactionInvoiceGrouping property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionInvoiceGrouping() {
        return nsfTransactionInvoiceGrouping;
    }

    /**
     * Sets the value of the nsfTransactionInvoiceGrouping property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionInvoiceGrouping(String value) {
        this.nsfTransactionInvoiceGrouping = value;
    }

    /**
     * Gets the value of the nsfTransactionInvoiceGroupingExistingInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNSFTransactionInvoiceGroupingExistingInvoiceNumber() {
        return nsfTransactionInvoiceGroupingExistingInvoiceNumber;
    }

    /**
     * Sets the value of the nsfTransactionInvoiceGroupingExistingInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNSFTransactionInvoiceGroupingExistingInvoiceNumber(Integer value) {
        this.nsfTransactionInvoiceGroupingExistingInvoiceNumber = value;
    }

    /**
     * Gets the value of the nsfTransactionInvoiceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionInvoiceMessage() {
        return nsfTransactionInvoiceMessage;
    }

    /**
     * Sets the value of the nsfTransactionInvoiceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionInvoiceMessage(String value) {
        this.nsfTransactionInvoiceMessage = value;
    }

    /**
     * Gets the value of the nsfTransactionProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionProfitCenterCode() {
        return nsfTransactionProfitCenterCode;
    }

    /**
     * Sets the value of the nsfTransactionProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionProfitCenterCode(String value) {
        this.nsfTransactionProfitCenterCode = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getNSFTransactionSendInvoiceToAddress() {
        return nsfTransactionSendInvoiceToAddress;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setNSFTransactionSendInvoiceToAddress(Address value) {
        this.nsfTransactionSendInvoiceToAddress = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToContact() {
        return nsfTransactionSendInvoiceToContact;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToContact(String value) {
        this.nsfTransactionSendInvoiceToContact = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNSFTransactionSendInvoiceToContactID() {
        return nsfTransactionSendInvoiceToContactID;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNSFTransactionSendInvoiceToContactID(Integer value) {
        this.nsfTransactionSendInvoiceToContactID = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToDeliveryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToDeliveryMethod() {
        return nsfTransactionSendInvoiceToDeliveryMethod;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToDeliveryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToDeliveryMethod(String value) {
        this.nsfTransactionSendInvoiceToDeliveryMethod = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToEmail() {
        return nsfTransactionSendInvoiceToEmail;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToEmail(String value) {
        this.nsfTransactionSendInvoiceToEmail = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToFaxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToFaxCountryCode() {
        return nsfTransactionSendInvoiceToFaxCountryCode;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToFaxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToFaxCountryCode(String value) {
        this.nsfTransactionSendInvoiceToFaxCountryCode = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToFaxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToFaxExtension() {
        return nsfTransactionSendInvoiceToFaxExtension;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToFaxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToFaxExtension(String value) {
        this.nsfTransactionSendInvoiceToFaxExtension = value;
    }

    /**
     * Gets the value of the nsfTransactionSendInvoiceToFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNSFTransactionSendInvoiceToFaxNumber() {
        return nsfTransactionSendInvoiceToFaxNumber;
    }

    /**
     * Sets the value of the nsfTransactionSendInvoiceToFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNSFTransactionSendInvoiceToFaxNumber(String value) {
        this.nsfTransactionSendInvoiceToFaxNumber = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the reasonDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonDetails() {
        return reasonDetails;
    }

    /**
     * Sets the value of the reasonDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonDetails(String value) {
        this.reasonDetails = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the isForMissedPayment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsForMissedPayment() {
        return isForMissedPayment;
    }

    /**
     * Sets the value of the isForMissedPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsForMissedPayment(Boolean value) {
        this.isForMissedPayment = value;
    }

}
