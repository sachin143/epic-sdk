
package com.appliedsystems.schemas.epic.sdk._2021._01._common._activity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._common._activity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IndioSubmissionDetail_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_common/_activity/", "IndioSubmissionDetail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._common._activity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IndioSubmissionDetail }
     * 
     */
    public IndioSubmissionDetail createIndioSubmissionDetail() {
        return new IndioSubmissionDetail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndioSubmissionDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_common/_activity/", name = "IndioSubmissionDetail")
    public JAXBElement<IndioSubmissionDetail> createIndioSubmissionDetail(IndioSubmissionDetail value) {
        return new JAXBElement<IndioSubmissionDetail>(_IndioSubmissionDetail_QNAME, IndioSubmissionDetail.class, null, value);
    }

}
