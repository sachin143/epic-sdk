
package com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Status">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="CurrentFuture"/>
 *     &lt;enumeration value="Expired"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Status", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_policyfilter/")
@XmlEnum
public enum Status {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("CurrentFuture")
    CURRENT_FUTURE("CurrentFuture"),
    @XmlEnumValue("Expired")
    EXPIRED("Expired");
    private final String value;

    Status(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Status fromValue(String v) {
        for (Status c: Status.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
