
package com.appliedsystems.schemas.epic.sdk._2021._01._account._client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EmployeeBenefits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployeeBenefits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CobraAdministration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERISAEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ERISAExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EmployeeBenefitsEmployeeClassItems" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/}EmployeeClassItems" minOccurs="0"/>
 *         &lt;element name="Form5500Required" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FullTimeEquivalent" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FullTimeEquivalentAsOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="HIPAASelected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="HIPAASignedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NumberEligible" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NumberEligibleAsOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NumberOfEmployees" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NumberOfEmployeesAsOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NumberOfRetirees" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NumberOfRetireesAsOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="VerifiedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="WrapDocument" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployeeBenefits", propOrder = {
    "cobraAdministration",
    "erisaEffectiveDate",
    "erisaExpirationDate",
    "employeeBenefitsEmployeeClassItems",
    "form5500Required",
    "fullTimeEquivalent",
    "fullTimeEquivalentAsOf",
    "hipaaSelected",
    "hipaaSignedDate",
    "numberEligible",
    "numberEligibleAsOf",
    "numberOfEmployees",
    "numberOfEmployeesAsOf",
    "numberOfRetirees",
    "numberOfRetireesAsOf",
    "verifiedDate",
    "wrapDocument"
})
public class EmployeeBenefits {

    @XmlElement(name = "CobraAdministration", nillable = true)
    protected String cobraAdministration;
    @XmlElement(name = "ERISAEffectiveDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar erisaEffectiveDate;
    @XmlElement(name = "ERISAExpirationDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar erisaExpirationDate;
    @XmlElement(name = "EmployeeBenefitsEmployeeClassItems", nillable = true)
    protected EmployeeClassItems employeeBenefitsEmployeeClassItems;
    @XmlElement(name = "Form5500Required", nillable = true)
    protected Boolean form5500Required;
    @XmlElement(name = "FullTimeEquivalent", nillable = true)
    protected Integer fullTimeEquivalent;
    @XmlElement(name = "FullTimeEquivalentAsOf", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fullTimeEquivalentAsOf;
    @XmlElement(name = "HIPAASelected", nillable = true)
    protected Boolean hipaaSelected;
    @XmlElement(name = "HIPAASignedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar hipaaSignedDate;
    @XmlElement(name = "NumberEligible", nillable = true)
    protected Integer numberEligible;
    @XmlElement(name = "NumberEligibleAsOf", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar numberEligibleAsOf;
    @XmlElement(name = "NumberOfEmployees", nillable = true)
    protected Integer numberOfEmployees;
    @XmlElement(name = "NumberOfEmployeesAsOf", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar numberOfEmployeesAsOf;
    @XmlElement(name = "NumberOfRetirees", nillable = true)
    protected Integer numberOfRetirees;
    @XmlElement(name = "NumberOfRetireesAsOf", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar numberOfRetireesAsOf;
    @XmlElement(name = "VerifiedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar verifiedDate;
    @XmlElement(name = "WrapDocument", nillable = true)
    protected Boolean wrapDocument;

    /**
     * Gets the value of the cobraAdministration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCobraAdministration() {
        return cobraAdministration;
    }

    /**
     * Sets the value of the cobraAdministration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCobraAdministration(String value) {
        this.cobraAdministration = value;
    }

    /**
     * Gets the value of the erisaEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getERISAEffectiveDate() {
        return erisaEffectiveDate;
    }

    /**
     * Sets the value of the erisaEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setERISAEffectiveDate(XMLGregorianCalendar value) {
        this.erisaEffectiveDate = value;
    }

    /**
     * Gets the value of the erisaExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getERISAExpirationDate() {
        return erisaExpirationDate;
    }

    /**
     * Sets the value of the erisaExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setERISAExpirationDate(XMLGregorianCalendar value) {
        this.erisaExpirationDate = value;
    }

    /**
     * Gets the value of the employeeBenefitsEmployeeClassItems property.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeClassItems }
     *     
     */
    public EmployeeClassItems getEmployeeBenefitsEmployeeClassItems() {
        return employeeBenefitsEmployeeClassItems;
    }

    /**
     * Sets the value of the employeeBenefitsEmployeeClassItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeClassItems }
     *     
     */
    public void setEmployeeBenefitsEmployeeClassItems(EmployeeClassItems value) {
        this.employeeBenefitsEmployeeClassItems = value;
    }

    /**
     * Gets the value of the form5500Required property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForm5500Required() {
        return form5500Required;
    }

    /**
     * Sets the value of the form5500Required property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForm5500Required(Boolean value) {
        this.form5500Required = value;
    }

    /**
     * Gets the value of the fullTimeEquivalent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFullTimeEquivalent() {
        return fullTimeEquivalent;
    }

    /**
     * Sets the value of the fullTimeEquivalent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFullTimeEquivalent(Integer value) {
        this.fullTimeEquivalent = value;
    }

    /**
     * Gets the value of the fullTimeEquivalentAsOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFullTimeEquivalentAsOf() {
        return fullTimeEquivalentAsOf;
    }

    /**
     * Sets the value of the fullTimeEquivalentAsOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFullTimeEquivalentAsOf(XMLGregorianCalendar value) {
        this.fullTimeEquivalentAsOf = value;
    }

    /**
     * Gets the value of the hipaaSelected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHIPAASelected() {
        return hipaaSelected;
    }

    /**
     * Sets the value of the hipaaSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHIPAASelected(Boolean value) {
        this.hipaaSelected = value;
    }

    /**
     * Gets the value of the hipaaSignedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHIPAASignedDate() {
        return hipaaSignedDate;
    }

    /**
     * Sets the value of the hipaaSignedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHIPAASignedDate(XMLGregorianCalendar value) {
        this.hipaaSignedDate = value;
    }

    /**
     * Gets the value of the numberEligible property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberEligible() {
        return numberEligible;
    }

    /**
     * Sets the value of the numberEligible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberEligible(Integer value) {
        this.numberEligible = value;
    }

    /**
     * Gets the value of the numberEligibleAsOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNumberEligibleAsOf() {
        return numberEligibleAsOf;
    }

    /**
     * Sets the value of the numberEligibleAsOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNumberEligibleAsOf(XMLGregorianCalendar value) {
        this.numberEligibleAsOf = value;
    }

    /**
     * Gets the value of the numberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * Sets the value of the numberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfEmployees(Integer value) {
        this.numberOfEmployees = value;
    }

    /**
     * Gets the value of the numberOfEmployeesAsOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNumberOfEmployeesAsOf() {
        return numberOfEmployeesAsOf;
    }

    /**
     * Sets the value of the numberOfEmployeesAsOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNumberOfEmployeesAsOf(XMLGregorianCalendar value) {
        this.numberOfEmployeesAsOf = value;
    }

    /**
     * Gets the value of the numberOfRetirees property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRetirees() {
        return numberOfRetirees;
    }

    /**
     * Sets the value of the numberOfRetirees property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRetirees(Integer value) {
        this.numberOfRetirees = value;
    }

    /**
     * Gets the value of the numberOfRetireesAsOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNumberOfRetireesAsOf() {
        return numberOfRetireesAsOf;
    }

    /**
     * Sets the value of the numberOfRetireesAsOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNumberOfRetireesAsOf(XMLGregorianCalendar value) {
        this.numberOfRetireesAsOf = value;
    }

    /**
     * Gets the value of the verifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerifiedDate() {
        return verifiedDate;
    }

    /**
     * Sets the value of the verifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerifiedDate(XMLGregorianCalendar value) {
        this.verifiedDate = value;
    }

    /**
     * Gets the value of the wrapDocument property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWrapDocument() {
        return wrapDocument;
    }

    /**
     * Sets the value of the wrapDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWrapDocument(Boolean value) {
        this.wrapDocument = value;
    }

}
