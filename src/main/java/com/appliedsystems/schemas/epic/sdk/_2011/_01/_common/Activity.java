
package com.appliedsystems.schemas.epic.sdk._2011._01._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.CloseDetail;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.Detail;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.TaskItems;
import com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.AccountServicingContactItems;
import com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.IndioSubmissionDetail;


/**
 * <p>Java class for Activity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Activity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssociatedToID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CloseDetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/}CloseDetail" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/}Detail" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Tasks" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/}TaskItems" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="WhoOwnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerItemIDForAssociatedToID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EnteredDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AttachmentsYesNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ServicingContacts" type="{http://schemas.appliedsystems.com/epic/sdk/2017/01/_common/_activity/}AccountServicingContactItems" minOccurs="0"/>
 *         &lt;element name="WhoOwnerOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="AssociatedToGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IndioSubmissionValue" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_common/_activity/}IndioSubmissionDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Activity", propOrder = {
    "accountID",
    "accountTypeCode",
    "activityCode",
    "activityID",
    "agencyCode",
    "associatedToID",
    "associatedToType",
    "branchCode",
    "closeDetailValue",
    "description",
    "detailValue",
    "priority",
    "statusOption",
    "tasks",
    "timestamp",
    "whoOwnerCode",
    "categoryID",
    "generalLedgerItemIDForAssociatedToID",
    "enteredDate",
    "attachmentsYesNo",
    "lastUpdatedDate",
    "servicingContacts",
    "whoOwnerOption",
    "associatedToGUID",
    "lineID",
    "sms",
    "indioSubmissionValue"
})
public class Activity {

    @XmlElement(name = "AccountID")
    protected Integer accountID;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "ActivityCode", nillable = true)
    protected String activityCode;
    @XmlElement(name = "ActivityID")
    protected Integer activityID;
    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "AssociatedToID")
    protected Integer associatedToID;
    @XmlElement(name = "AssociatedToType", nillable = true)
    protected String associatedToType;
    @XmlElement(name = "BranchCode", nillable = true)
    protected String branchCode;
    @XmlElement(name = "CloseDetailValue", nillable = true)
    protected CloseDetail closeDetailValue;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "Priority", nillable = true)
    protected String priority;
    @XmlElement(name = "StatusOption", nillable = true)
    protected OptionType statusOption;
    @XmlElement(name = "Tasks", nillable = true)
    protected TaskItems tasks;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "WhoOwnerCode", nillable = true)
    protected String whoOwnerCode;
    @XmlElement(name = "CategoryID")
    protected Integer categoryID;
    @XmlElement(name = "GeneralLedgerItemIDForAssociatedToID")
    protected Integer generalLedgerItemIDForAssociatedToID;
    @XmlElement(name = "EnteredDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enteredDate;
    @XmlElement(name = "AttachmentsYesNo", nillable = true)
    protected String attachmentsYesNo;
    @XmlElement(name = "LastUpdatedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUpdatedDate;
    @XmlElement(name = "ServicingContacts", nillable = true)
    protected AccountServicingContactItems servicingContacts;
    @XmlElement(name = "WhoOwnerOption", nillable = true)
    protected OptionType whoOwnerOption;
    @XmlElement(name = "AssociatedToGUID", nillable = true)
    protected String associatedToGUID;
    @XmlElement(name = "LineID", nillable = true)
    protected String lineID;
    @XmlElement(name = "SMS", nillable = true)
    protected String sms;
    @XmlElement(name = "IndioSubmissionValue", nillable = true)
    protected IndioSubmissionDetail indioSubmissionValue;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the activityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * Sets the value of the activityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivityCode(String value) {
        this.activityCode = value;
    }

    /**
     * Gets the value of the activityID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivityID() {
        return activityID;
    }

    /**
     * Sets the value of the activityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivityID(Integer value) {
        this.activityID = value;
    }

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the associatedToID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedToID() {
        return associatedToID;
    }

    /**
     * Sets the value of the associatedToID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedToID(Integer value) {
        this.associatedToID = value;
    }

    /**
     * Gets the value of the associatedToType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedToType() {
        return associatedToType;
    }

    /**
     * Sets the value of the associatedToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedToType(String value) {
        this.associatedToType = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the closeDetailValue property.
     * 
     * @return
     *     possible object is
     *     {@link CloseDetail }
     *     
     */
    public CloseDetail getCloseDetailValue() {
        return closeDetailValue;
    }

    /**
     * Sets the value of the closeDetailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CloseDetail }
     *     
     */
    public void setCloseDetailValue(CloseDetail value) {
        this.closeDetailValue = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the statusOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getStatusOption() {
        return statusOption;
    }

    /**
     * Sets the value of the statusOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setStatusOption(OptionType value) {
        this.statusOption = value;
    }

    /**
     * Gets the value of the tasks property.
     * 
     * @return
     *     possible object is
     *     {@link TaskItems }
     *     
     */
    public TaskItems getTasks() {
        return tasks;
    }

    /**
     * Sets the value of the tasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaskItems }
     *     
     */
    public void setTasks(TaskItems value) {
        this.tasks = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the whoOwnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhoOwnerCode() {
        return whoOwnerCode;
    }

    /**
     * Sets the value of the whoOwnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhoOwnerCode(String value) {
        this.whoOwnerCode = value;
    }

    /**
     * Gets the value of the categoryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCategoryID() {
        return categoryID;
    }

    /**
     * Sets the value of the categoryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCategoryID(Integer value) {
        this.categoryID = value;
    }

    /**
     * Gets the value of the generalLedgerItemIDForAssociatedToID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGeneralLedgerItemIDForAssociatedToID() {
        return generalLedgerItemIDForAssociatedToID;
    }

    /**
     * Sets the value of the generalLedgerItemIDForAssociatedToID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGeneralLedgerItemIDForAssociatedToID(Integer value) {
        this.generalLedgerItemIDForAssociatedToID = value;
    }

    /**
     * Gets the value of the enteredDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEnteredDate() {
        return enteredDate;
    }

    /**
     * Sets the value of the enteredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEnteredDate(XMLGregorianCalendar value) {
        this.enteredDate = value;
    }

    /**
     * Gets the value of the attachmentsYesNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentsYesNo() {
        return attachmentsYesNo;
    }

    /**
     * Sets the value of the attachmentsYesNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentsYesNo(String value) {
        this.attachmentsYesNo = value;
    }

    /**
     * Gets the value of the lastUpdatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * Sets the value of the lastUpdatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdatedDate(XMLGregorianCalendar value) {
        this.lastUpdatedDate = value;
    }

    /**
     * Gets the value of the servicingContacts property.
     * 
     * @return
     *     possible object is
     *     {@link AccountServicingContactItems }
     *     
     */
    public AccountServicingContactItems getServicingContacts() {
        return servicingContacts;
    }

    /**
     * Sets the value of the servicingContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountServicingContactItems }
     *     
     */
    public void setServicingContacts(AccountServicingContactItems value) {
        this.servicingContacts = value;
    }

    /**
     * Gets the value of the whoOwnerOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getWhoOwnerOption() {
        return whoOwnerOption;
    }

    /**
     * Sets the value of the whoOwnerOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setWhoOwnerOption(OptionType value) {
        this.whoOwnerOption = value;
    }

    /**
     * Gets the value of the associatedToGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedToGUID() {
        return associatedToGUID;
    }

    /**
     * Sets the value of the associatedToGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedToGUID(String value) {
        this.associatedToGUID = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineID(String value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the sms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSMS() {
        return sms;
    }

    /**
     * Sets the value of the sms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSMS(String value) {
        this.sms = value;
    }

    /**
     * Gets the value of the indioSubmissionValue property.
     * 
     * @return
     *     possible object is
     *     {@link IndioSubmissionDetail }
     *     
     */
    public IndioSubmissionDetail getIndioSubmissionValue() {
        return indioSubmissionValue;
    }

    /**
     * Sets the value of the indioSubmissionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndioSubmissionDetail }
     *     
     */
    public void setIndioSubmissionValue(IndioSubmissionDetail value) {
        this.indioSubmissionValue = value;
    }

}
