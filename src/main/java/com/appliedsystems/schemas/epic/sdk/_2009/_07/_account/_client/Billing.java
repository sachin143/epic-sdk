
package com.appliedsystems.schemas.epic.sdk._2009._07._account._client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Billing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Billing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BrokerLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceChargeFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InvoiceLayout" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoicePageBreak" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementLayout" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementPageBreak" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillBrokerNet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CPAAuthorized" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Billing", propOrder = {
    "brokerLookupCode",
    "serviceChargeFlag",
    "invoiceLayout",
    "invoicePageBreak",
    "statementLayout",
    "statementPageBreak",
    "billBrokerNet",
    "cpaAuthorized"
})
public class Billing {

    @XmlElement(name = "BrokerLookupCode", nillable = true)
    protected String brokerLookupCode;
    @XmlElement(name = "ServiceChargeFlag")
    protected Boolean serviceChargeFlag;
    @XmlElement(name = "InvoiceLayout", nillable = true)
    protected String invoiceLayout;
    @XmlElement(name = "InvoicePageBreak", nillable = true)
    protected String invoicePageBreak;
    @XmlElement(name = "StatementLayout", nillable = true)
    protected String statementLayout;
    @XmlElement(name = "StatementPageBreak", nillable = true)
    protected String statementPageBreak;
    @XmlElement(name = "BillBrokerNet")
    protected Boolean billBrokerNet;
    @XmlElement(name = "CPAAuthorized", nillable = true)
    protected String cpaAuthorized;

    /**
     * Gets the value of the brokerLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrokerLookupCode() {
        return brokerLookupCode;
    }

    /**
     * Sets the value of the brokerLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrokerLookupCode(String value) {
        this.brokerLookupCode = value;
    }

    /**
     * Gets the value of the serviceChargeFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isServiceChargeFlag() {
        return serviceChargeFlag;
    }

    /**
     * Sets the value of the serviceChargeFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setServiceChargeFlag(Boolean value) {
        this.serviceChargeFlag = value;
    }

    /**
     * Gets the value of the invoiceLayout property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceLayout() {
        return invoiceLayout;
    }

    /**
     * Sets the value of the invoiceLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceLayout(String value) {
        this.invoiceLayout = value;
    }

    /**
     * Gets the value of the invoicePageBreak property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicePageBreak() {
        return invoicePageBreak;
    }

    /**
     * Sets the value of the invoicePageBreak property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicePageBreak(String value) {
        this.invoicePageBreak = value;
    }

    /**
     * Gets the value of the statementLayout property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementLayout() {
        return statementLayout;
    }

    /**
     * Sets the value of the statementLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementLayout(String value) {
        this.statementLayout = value;
    }

    /**
     * Gets the value of the statementPageBreak property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementPageBreak() {
        return statementPageBreak;
    }

    /**
     * Sets the value of the statementPageBreak property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementPageBreak(String value) {
        this.statementPageBreak = value;
    }

    /**
     * Gets the value of the billBrokerNet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBillBrokerNet() {
        return billBrokerNet;
    }

    /**
     * Sets the value of the billBrokerNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBillBrokerNet(Boolean value) {
        this.billBrokerNet = value;
    }

    /**
     * Gets the value of the cpaAuthorized property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCPAAuthorized() {
        return cpaAuthorized;
    }

    /**
     * Sets the value of the cpaAuthorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCPAAuthorized(String value) {
        this.cpaAuthorized = value;
    }

}
