
package com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttachedToItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttachedToItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachedToItem" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/}AttachedToItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachedToItems", propOrder = {
    "attachedToItems"
})
public class AttachedToItems {

    @XmlElement(name = "AttachedToItem", nillable = true)
    protected List<AttachedToItem> attachedToItems;

    /**
     * Gets the value of the attachedToItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachedToItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachedToItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttachedToItem }
     * 
     * 
     */
    public List<AttachedToItem> getAttachedToItems() {
        if (attachedToItems == null) {
            attachedToItems = new ArrayList<AttachedToItem>();
        }
        return this.attachedToItems;
    }

}
