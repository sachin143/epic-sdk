
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ServicingRolesItems;


/**
 * <p>Java class for ServicingContacts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServicingContacts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServicingRolesValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}ServicingRolesItems" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServicingContacts", propOrder = {
    "claimID",
    "servicingRolesValue",
    "timestamp"
})
public class ServicingContacts {

    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "ServicingRolesValue", nillable = true)
    protected ServicingRolesItems servicingRolesValue;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the servicingRolesValue property.
     * 
     * @return
     *     possible object is
     *     {@link ServicingRolesItems }
     *     
     */
    public ServicingRolesItems getServicingRolesValue() {
        return servicingRolesValue;
    }

    /**
     * Sets the value of the servicingRolesValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicingRolesItems }
     *     
     */
    public void setServicingRolesValue(ServicingRolesItems value) {
        this.servicingRolesValue = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
