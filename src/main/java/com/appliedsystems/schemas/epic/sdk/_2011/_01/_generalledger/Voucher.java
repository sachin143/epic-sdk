
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.Check;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.Detail;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.Discount;


/**
 * <p>Java class for Voucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Voucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/}Check" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/}Detail" minOccurs="0"/>
 *         &lt;element name="DiscountValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/}Discount" minOccurs="0"/>
 *         &lt;element name="IgnoreDuplicatePayToTheOrderOfInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressSiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfPayee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfPayeeContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="VoidDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidVoided" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VoucherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="VoucherID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VoucherRecurringEntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreAccountingMonthVerification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Voucher", propOrder = {
    "checkValue",
    "detailValue",
    "discountValue",
    "ignoreDuplicatePayToTheOrderOfInvoiceNumber",
    "isReadOnly",
    "payToTheOrderOfAccountLookupCode",
    "payToTheOrderOfAccountNumber",
    "payToTheOrderOfAccountType",
    "payToTheOrderOfDueDate",
    "payToTheOrderOfInvoiceDate",
    "payToTheOrderOfInvoiceNumber",
    "payToTheOrderOfMailingAddress",
    "payToTheOrderOfMailingAddressContact",
    "payToTheOrderOfMailingAddressContactID",
    "payToTheOrderOfMailingAddressSiteID",
    "payToTheOrderOfPayee",
    "payToTheOrderOfPayeeContactID",
    "timestamp",
    "voidDetails",
    "voidReason",
    "voidReferNumber",
    "voidVoided",
    "voucherAccountingMonth",
    "voucherBankAccountNumberCode",
    "voucherBankSubAccountNumberCode",
    "voucherDefaultEntryID",
    "voucherDescription",
    "voucherEffectiveDate",
    "voucherID",
    "voucherRecurringEntry",
    "voucherReferNumber",
    "voucherStatus",
    "ignoreAccountingMonthVerification"
})
public class Voucher {

    @XmlElement(name = "CheckValue", nillable = true)
    protected Check checkValue;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "DiscountValue", nillable = true)
    protected Discount discountValue;
    @XmlElement(name = "IgnoreDuplicatePayToTheOrderOfInvoiceNumber")
    protected Boolean ignoreDuplicatePayToTheOrderOfInvoiceNumber;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "PayToTheOrderOfAccountLookupCode", nillable = true)
    protected String payToTheOrderOfAccountLookupCode;
    @XmlElement(name = "PayToTheOrderOfAccountNumber", nillable = true)
    protected String payToTheOrderOfAccountNumber;
    @XmlElement(name = "PayToTheOrderOfAccountType", nillable = true)
    protected String payToTheOrderOfAccountType;
    @XmlElement(name = "PayToTheOrderOfDueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payToTheOrderOfDueDate;
    @XmlElement(name = "PayToTheOrderOfInvoiceDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payToTheOrderOfInvoiceDate;
    @XmlElement(name = "PayToTheOrderOfInvoiceNumber", nillable = true)
    protected String payToTheOrderOfInvoiceNumber;
    @XmlElement(name = "PayToTheOrderOfMailingAddress", nillable = true)
    protected Address payToTheOrderOfMailingAddress;
    @XmlElement(name = "PayToTheOrderOfMailingAddressContact", nillable = true)
    protected String payToTheOrderOfMailingAddressContact;
    @XmlElement(name = "PayToTheOrderOfMailingAddressContactID")
    protected Integer payToTheOrderOfMailingAddressContactID;
    @XmlElement(name = "PayToTheOrderOfMailingAddressSiteID", nillable = true)
    protected String payToTheOrderOfMailingAddressSiteID;
    @XmlElement(name = "PayToTheOrderOfPayee", nillable = true)
    protected String payToTheOrderOfPayee;
    @XmlElement(name = "PayToTheOrderOfPayeeContactID")
    protected Integer payToTheOrderOfPayeeContactID;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "VoidDetails", nillable = true)
    protected String voidDetails;
    @XmlElement(name = "VoidReason", nillable = true)
    protected String voidReason;
    @XmlElement(name = "VoidReferNumber", nillable = true)
    protected String voidReferNumber;
    @XmlElement(name = "VoidVoided", nillable = true)
    protected String voidVoided;
    @XmlElement(name = "VoucherAccountingMonth", nillable = true)
    protected String voucherAccountingMonth;
    @XmlElement(name = "VoucherBankAccountNumberCode", nillable = true)
    protected String voucherBankAccountNumberCode;
    @XmlElement(name = "VoucherBankSubAccountNumberCode", nillable = true)
    protected String voucherBankSubAccountNumberCode;
    @XmlElement(name = "VoucherDefaultEntryID")
    protected Integer voucherDefaultEntryID;
    @XmlElement(name = "VoucherDescription", nillable = true)
    protected String voucherDescription;
    @XmlElement(name = "VoucherEffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar voucherEffectiveDate;
    @XmlElement(name = "VoucherID")
    protected Integer voucherID;
    @XmlElement(name = "VoucherRecurringEntry", nillable = true)
    protected String voucherRecurringEntry;
    @XmlElement(name = "VoucherReferNumber", nillable = true)
    protected String voucherReferNumber;
    @XmlElement(name = "VoucherStatus", nillable = true)
    protected String voucherStatus;
    @XmlElement(name = "IgnoreAccountingMonthVerification", nillable = true)
    protected Boolean ignoreAccountingMonthVerification;

    /**
     * Gets the value of the checkValue property.
     * 
     * @return
     *     possible object is
     *     {@link Check }
     *     
     */
    public Check getCheckValue() {
        return checkValue;
    }

    /**
     * Sets the value of the checkValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Check }
     *     
     */
    public void setCheckValue(Check value) {
        this.checkValue = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the discountValue property.
     * 
     * @return
     *     possible object is
     *     {@link Discount }
     *     
     */
    public Discount getDiscountValue() {
        return discountValue;
    }

    /**
     * Sets the value of the discountValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Discount }
     *     
     */
    public void setDiscountValue(Discount value) {
        this.discountValue = value;
    }

    /**
     * Gets the value of the ignoreDuplicatePayToTheOrderOfInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreDuplicatePayToTheOrderOfInvoiceNumber() {
        return ignoreDuplicatePayToTheOrderOfInvoiceNumber;
    }

    /**
     * Sets the value of the ignoreDuplicatePayToTheOrderOfInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreDuplicatePayToTheOrderOfInvoiceNumber(Boolean value) {
        this.ignoreDuplicatePayToTheOrderOfInvoiceNumber = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountLookupCode() {
        return payToTheOrderOfAccountLookupCode;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountLookupCode(String value) {
        this.payToTheOrderOfAccountLookupCode = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountNumber() {
        return payToTheOrderOfAccountNumber;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountNumber(String value) {
        this.payToTheOrderOfAccountNumber = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountType() {
        return payToTheOrderOfAccountType;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountType(String value) {
        this.payToTheOrderOfAccountType = value;
    }

    /**
     * Gets the value of the payToTheOrderOfDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayToTheOrderOfDueDate() {
        return payToTheOrderOfDueDate;
    }

    /**
     * Sets the value of the payToTheOrderOfDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayToTheOrderOfDueDate(XMLGregorianCalendar value) {
        this.payToTheOrderOfDueDate = value;
    }

    /**
     * Gets the value of the payToTheOrderOfInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayToTheOrderOfInvoiceDate() {
        return payToTheOrderOfInvoiceDate;
    }

    /**
     * Sets the value of the payToTheOrderOfInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayToTheOrderOfInvoiceDate(XMLGregorianCalendar value) {
        this.payToTheOrderOfInvoiceDate = value;
    }

    /**
     * Gets the value of the payToTheOrderOfInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfInvoiceNumber() {
        return payToTheOrderOfInvoiceNumber;
    }

    /**
     * Sets the value of the payToTheOrderOfInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfInvoiceNumber(String value) {
        this.payToTheOrderOfInvoiceNumber = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPayToTheOrderOfMailingAddress() {
        return payToTheOrderOfMailingAddress;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPayToTheOrderOfMailingAddress(Address value) {
        this.payToTheOrderOfMailingAddress = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfMailingAddressContact() {
        return payToTheOrderOfMailingAddressContact;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfMailingAddressContact(String value) {
        this.payToTheOrderOfMailingAddressContact = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayToTheOrderOfMailingAddressContactID() {
        return payToTheOrderOfMailingAddressContactID;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayToTheOrderOfMailingAddressContactID(Integer value) {
        this.payToTheOrderOfMailingAddressContactID = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressSiteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfMailingAddressSiteID() {
        return payToTheOrderOfMailingAddressSiteID;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressSiteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfMailingAddressSiteID(String value) {
        this.payToTheOrderOfMailingAddressSiteID = value;
    }

    /**
     * Gets the value of the payToTheOrderOfPayee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfPayee() {
        return payToTheOrderOfPayee;
    }

    /**
     * Sets the value of the payToTheOrderOfPayee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfPayee(String value) {
        this.payToTheOrderOfPayee = value;
    }

    /**
     * Gets the value of the payToTheOrderOfPayeeContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayToTheOrderOfPayeeContactID() {
        return payToTheOrderOfPayeeContactID;
    }

    /**
     * Sets the value of the payToTheOrderOfPayeeContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayToTheOrderOfPayeeContactID(Integer value) {
        this.payToTheOrderOfPayeeContactID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the voidDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidDetails() {
        return voidDetails;
    }

    /**
     * Sets the value of the voidDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidDetails(String value) {
        this.voidDetails = value;
    }

    /**
     * Gets the value of the voidReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReason() {
        return voidReason;
    }

    /**
     * Sets the value of the voidReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReason(String value) {
        this.voidReason = value;
    }

    /**
     * Gets the value of the voidReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReferNumber() {
        return voidReferNumber;
    }

    /**
     * Sets the value of the voidReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReferNumber(String value) {
        this.voidReferNumber = value;
    }

    /**
     * Gets the value of the voidVoided property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidVoided() {
        return voidVoided;
    }

    /**
     * Sets the value of the voidVoided property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidVoided(String value) {
        this.voidVoided = value;
    }

    /**
     * Gets the value of the voucherAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherAccountingMonth() {
        return voucherAccountingMonth;
    }

    /**
     * Sets the value of the voucherAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherAccountingMonth(String value) {
        this.voucherAccountingMonth = value;
    }

    /**
     * Gets the value of the voucherBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherBankAccountNumberCode() {
        return voucherBankAccountNumberCode;
    }

    /**
     * Sets the value of the voucherBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherBankAccountNumberCode(String value) {
        this.voucherBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the voucherBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherBankSubAccountNumberCode() {
        return voucherBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the voucherBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherBankSubAccountNumberCode(String value) {
        this.voucherBankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the voucherDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherDefaultEntryID() {
        return voucherDefaultEntryID;
    }

    /**
     * Sets the value of the voucherDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherDefaultEntryID(Integer value) {
        this.voucherDefaultEntryID = value;
    }

    /**
     * Gets the value of the voucherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherDescription() {
        return voucherDescription;
    }

    /**
     * Sets the value of the voucherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherDescription(String value) {
        this.voucherDescription = value;
    }

    /**
     * Gets the value of the voucherEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVoucherEffectiveDate() {
        return voucherEffectiveDate;
    }

    /**
     * Sets the value of the voucherEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVoucherEffectiveDate(XMLGregorianCalendar value) {
        this.voucherEffectiveDate = value;
    }

    /**
     * Gets the value of the voucherID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherID() {
        return voucherID;
    }

    /**
     * Sets the value of the voucherID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherID(Integer value) {
        this.voucherID = value;
    }

    /**
     * Gets the value of the voucherRecurringEntry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherRecurringEntry() {
        return voucherRecurringEntry;
    }

    /**
     * Sets the value of the voucherRecurringEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherRecurringEntry(String value) {
        this.voucherRecurringEntry = value;
    }

    /**
     * Gets the value of the voucherReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherReferNumber() {
        return voucherReferNumber;
    }

    /**
     * Sets the value of the voucherReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherReferNumber(String value) {
        this.voucherReferNumber = value;
    }

    /**
     * Gets the value of the voucherStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherStatus() {
        return voucherStatus;
    }

    /**
     * Sets the value of the voucherStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherStatus(String value) {
        this.voucherStatus = value;
    }

    /**
     * Gets the value of the ignoreAccountingMonthVerification property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAccountingMonthVerification() {
        return ignoreAccountingMonthVerification;
    }

    /**
     * Sets the value of the ignoreAccountingMonthVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAccountingMonthVerification(Boolean value) {
        this.ignoreAccountingMonthVerification = value;
    }

}
