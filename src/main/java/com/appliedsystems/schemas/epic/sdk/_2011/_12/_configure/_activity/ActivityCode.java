
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.BasicSettings;


/**
 * <p>Java class for ActivityCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BasicSetting" type="{http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/}BasicSettings" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityCode", propOrder = {
    "code",
    "description",
    "basicSetting",
    "isActive"
})
public class ActivityCode {

    @XmlElement(name = "Code", nillable = true)
    protected String code;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "BasicSetting", nillable = true)
    protected BasicSettings basicSetting;
    @XmlElement(name = "IsActive")
    protected Boolean isActive;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the basicSetting property.
     * 
     * @return
     *     possible object is
     *     {@link BasicSettings }
     *     
     */
    public BasicSettings getBasicSetting() {
        return basicSetting;
    }

    /**
     * Sets the value of the basicSetting property.
     * 
     * @param value
     *     allowed object is
     *     {@link BasicSettings }
     *     
     */
    public void setBasicSetting(BasicSettings value) {
        this.basicSetting = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

}
