
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReverseBalanceTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReverseBalanceTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetailAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ReverseTransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReverseBalanceTransfer", propOrder = {
    "detailAccountingMonth",
    "detailDescription",
    "reasonDetails",
    "reasonID",
    "reverseTransactionID"
})
public class ReverseBalanceTransfer {

    @XmlElement(name = "DetailAccountingMonth", nillable = true)
    protected String detailAccountingMonth;
    @XmlElement(name = "DetailDescription", nillable = true)
    protected String detailDescription;
    @XmlElement(name = "ReasonDetails", nillable = true)
    protected String reasonDetails;
    @XmlElement(name = "ReasonID", nillable = true)
    protected Integer reasonID;
    @XmlElement(name = "ReverseTransactionID")
    protected Integer reverseTransactionID;

    /**
     * Gets the value of the detailAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailAccountingMonth() {
        return detailAccountingMonth;
    }

    /**
     * Sets the value of the detailAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailAccountingMonth(String value) {
        this.detailAccountingMonth = value;
    }

    /**
     * Gets the value of the detailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescription() {
        return detailDescription;
    }

    /**
     * Sets the value of the detailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescription(String value) {
        this.detailDescription = value;
    }

    /**
     * Gets the value of the reasonDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonDetails() {
        return reasonDetails;
    }

    /**
     * Sets the value of the reasonDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonDetails(String value) {
        this.reasonDetails = value;
    }

    /**
     * Gets the value of the reasonID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReasonID() {
        return reasonID;
    }

    /**
     * Sets the value of the reasonID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReasonID(Integer value) {
        this.reasonID = value;
    }

    /**
     * Gets the value of the reverseTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReverseTransactionID() {
        return reverseTransactionID;
    }

    /**
     * Sets the value of the reverseTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReverseTransactionID(Integer value) {
        this.reverseTransactionID = value;
    }

}
