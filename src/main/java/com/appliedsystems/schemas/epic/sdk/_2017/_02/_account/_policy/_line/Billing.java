
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for Billing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Billing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillBrokerNet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BillingPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FinancedFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InvoiceToAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="InvoiceToAddressDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InvoiceToContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToDeliveryMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToFaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToFaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToSiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxOptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Billing", propOrder = {
    "billBrokerNet",
    "billingPlan",
    "financedFlag",
    "invoiceToAccountLookupCode",
    "invoiceToAddress",
    "invoiceToAddressDescription",
    "invoiceToContactID",
    "invoiceToContactName",
    "invoiceToDeliveryMethod",
    "invoiceToEmail",
    "invoiceToFaxCountryCode",
    "invoiceToFaxExtension",
    "invoiceToFaxNumber",
    "invoiceToSiteID",
    "invoiceToType",
    "loanNumber",
    "taxOptionCode"
})
public class Billing {

    @XmlElement(name = "BillBrokerNet")
    protected Boolean billBrokerNet;
    @XmlElement(name = "BillingPlan", nillable = true)
    protected String billingPlan;
    @XmlElement(name = "FinancedFlag")
    protected Boolean financedFlag;
    @XmlElement(name = "InvoiceToAccountLookupCode", nillable = true)
    protected String invoiceToAccountLookupCode;
    @XmlElement(name = "InvoiceToAddress", nillable = true)
    protected Address invoiceToAddress;
    @XmlElement(name = "InvoiceToAddressDescription", nillable = true)
    protected String invoiceToAddressDescription;
    @XmlElement(name = "InvoiceToContactID", nillable = true)
    protected Integer invoiceToContactID;
    @XmlElement(name = "InvoiceToContactName", nillable = true)
    protected String invoiceToContactName;
    @XmlElement(name = "InvoiceToDeliveryMethod", nillable = true)
    protected String invoiceToDeliveryMethod;
    @XmlElement(name = "InvoiceToEmail", nillable = true)
    protected String invoiceToEmail;
    @XmlElement(name = "InvoiceToFaxCountryCode", nillable = true)
    protected String invoiceToFaxCountryCode;
    @XmlElement(name = "InvoiceToFaxExtension", nillable = true)
    protected String invoiceToFaxExtension;
    @XmlElement(name = "InvoiceToFaxNumber", nillable = true)
    protected String invoiceToFaxNumber;
    @XmlElement(name = "InvoiceToSiteID", nillable = true)
    protected String invoiceToSiteID;
    @XmlElement(name = "InvoiceToType", nillable = true)
    protected String invoiceToType;
    @XmlElement(name = "LoanNumber", nillable = true)
    protected String loanNumber;
    @XmlElement(name = "TaxOptionCode", nillable = true)
    protected String taxOptionCode;

    /**
     * Gets the value of the billBrokerNet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBillBrokerNet() {
        return billBrokerNet;
    }

    /**
     * Sets the value of the billBrokerNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBillBrokerNet(Boolean value) {
        this.billBrokerNet = value;
    }

    /**
     * Gets the value of the billingPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingPlan() {
        return billingPlan;
    }

    /**
     * Sets the value of the billingPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingPlan(String value) {
        this.billingPlan = value;
    }

    /**
     * Gets the value of the financedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinancedFlag() {
        return financedFlag;
    }

    /**
     * Sets the value of the financedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinancedFlag(Boolean value) {
        this.financedFlag = value;
    }

    /**
     * Gets the value of the invoiceToAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToAccountLookupCode() {
        return invoiceToAccountLookupCode;
    }

    /**
     * Sets the value of the invoiceToAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToAccountLookupCode(String value) {
        this.invoiceToAccountLookupCode = value;
    }

    /**
     * Gets the value of the invoiceToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getInvoiceToAddress() {
        return invoiceToAddress;
    }

    /**
     * Sets the value of the invoiceToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setInvoiceToAddress(Address value) {
        this.invoiceToAddress = value;
    }

    /**
     * Gets the value of the invoiceToAddressDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToAddressDescription() {
        return invoiceToAddressDescription;
    }

    /**
     * Sets the value of the invoiceToAddressDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToAddressDescription(String value) {
        this.invoiceToAddressDescription = value;
    }

    /**
     * Gets the value of the invoiceToContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceToContactID() {
        return invoiceToContactID;
    }

    /**
     * Sets the value of the invoiceToContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceToContactID(Integer value) {
        this.invoiceToContactID = value;
    }

    /**
     * Gets the value of the invoiceToContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToContactName() {
        return invoiceToContactName;
    }

    /**
     * Sets the value of the invoiceToContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToContactName(String value) {
        this.invoiceToContactName = value;
    }

    /**
     * Gets the value of the invoiceToDeliveryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToDeliveryMethod() {
        return invoiceToDeliveryMethod;
    }

    /**
     * Sets the value of the invoiceToDeliveryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToDeliveryMethod(String value) {
        this.invoiceToDeliveryMethod = value;
    }

    /**
     * Gets the value of the invoiceToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToEmail() {
        return invoiceToEmail;
    }

    /**
     * Sets the value of the invoiceToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToEmail(String value) {
        this.invoiceToEmail = value;
    }

    /**
     * Gets the value of the invoiceToFaxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToFaxCountryCode() {
        return invoiceToFaxCountryCode;
    }

    /**
     * Sets the value of the invoiceToFaxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToFaxCountryCode(String value) {
        this.invoiceToFaxCountryCode = value;
    }

    /**
     * Gets the value of the invoiceToFaxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToFaxExtension() {
        return invoiceToFaxExtension;
    }

    /**
     * Sets the value of the invoiceToFaxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToFaxExtension(String value) {
        this.invoiceToFaxExtension = value;
    }

    /**
     * Gets the value of the invoiceToFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToFaxNumber() {
        return invoiceToFaxNumber;
    }

    /**
     * Sets the value of the invoiceToFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToFaxNumber(String value) {
        this.invoiceToFaxNumber = value;
    }

    /**
     * Gets the value of the invoiceToSiteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToSiteID() {
        return invoiceToSiteID;
    }

    /**
     * Sets the value of the invoiceToSiteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToSiteID(String value) {
        this.invoiceToSiteID = value;
    }

    /**
     * Gets the value of the invoiceToType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToType() {
        return invoiceToType;
    }

    /**
     * Sets the value of the invoiceToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToType(String value) {
        this.invoiceToType = value;
    }

    /**
     * Gets the value of the loanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNumber() {
        return loanNumber;
    }

    /**
     * Sets the value of the loanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNumber(String value) {
        this.loanNumber = value;
    }

    /**
     * Gets the value of the taxOptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxOptionCode() {
        return taxOptionCode;
    }

    /**
     * Sets the value of the taxOptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxOptionCode(String value) {
        this.taxOptionCode = value;
    }

}
