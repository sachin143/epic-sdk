
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstallmentSummaryItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstallmentSummaryItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InstallmentSummaryItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/}InstallmentSummaryItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstallmentSummaryItems", propOrder = {
    "installmentSummaryItems"
})
public class InstallmentSummaryItems {

    @XmlElement(name = "InstallmentSummaryItem", nillable = true)
    protected List<InstallmentSummaryItem> installmentSummaryItems;

    /**
     * Gets the value of the installmentSummaryItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the installmentSummaryItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstallmentSummaryItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstallmentSummaryItem }
     * 
     * 
     */
    public List<InstallmentSummaryItem> getInstallmentSummaryItems() {
        if (installmentSummaryItems == null) {
            installmentSummaryItems = new ArrayList<InstallmentSummaryItem>();
        }
        return this.installmentSummaryItems;
    }

}
