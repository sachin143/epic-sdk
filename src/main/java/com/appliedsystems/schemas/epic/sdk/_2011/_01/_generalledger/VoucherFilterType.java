
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherFilterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherFilterType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="AccountCode"/>
 *     &lt;enumeration value="AccountName"/>
 *     &lt;enumeration value="AccountingMonth"/>
 *     &lt;enumeration value="BankAccount"/>
 *     &lt;enumeration value="DateEntered"/>
 *     &lt;enumeration value="DueDate"/>
 *     &lt;enumeration value="GeneralLedgerSchedule"/>
 *     &lt;enumeration value="Payee"/>
 *     &lt;enumeration value="ReferNumber"/>
 *     &lt;enumeration value="SiteID"/>
 *     &lt;enumeration value="ExportBatchNumber"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VoucherFilterType")
@XmlEnum
public enum VoucherFilterType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("AccountCode")
    ACCOUNT_CODE("AccountCode"),
    @XmlEnumValue("AccountName")
    ACCOUNT_NAME("AccountName"),
    @XmlEnumValue("AccountingMonth")
    ACCOUNTING_MONTH("AccountingMonth"),
    @XmlEnumValue("BankAccount")
    BANK_ACCOUNT("BankAccount"),
    @XmlEnumValue("DateEntered")
    DATE_ENTERED("DateEntered"),
    @XmlEnumValue("DueDate")
    DUE_DATE("DueDate"),
    @XmlEnumValue("GeneralLedgerSchedule")
    GENERAL_LEDGER_SCHEDULE("GeneralLedgerSchedule"),
    @XmlEnumValue("Payee")
    PAYEE("Payee"),
    @XmlEnumValue("ReferNumber")
    REFER_NUMBER("ReferNumber"),
    @XmlEnumValue("SiteID")
    SITE_ID("SiteID"),
    @XmlEnumValue("ExportBatchNumber")
    EXPORT_BATCH_NUMBER("ExportBatchNumber");
    private final String value;

    VoucherFilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoucherFilterType fromValue(String v) {
        for (VoucherFilterType c: VoucherFilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
