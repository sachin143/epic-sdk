
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;


/**
 * <p>Java class for SendInvoiceToItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SendInvoiceToItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="AddressDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DeliveryMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GenerateInvoice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InvoiceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InvoiceToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoanNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SplitReceivableAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SplitReceivablePercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceGrouping" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceGroupingExistingInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BillBrokerNet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendInvoiceToItem", propOrder = {
    "accountLookupCode",
    "address",
    "addressDescription",
    "contact",
    "contactID",
    "deliveryMethod",
    "email",
    "faxCountryCode",
    "faxExtension",
    "faxNumber",
    "generateInvoice",
    "invoiceMessage",
    "invoiceNumber",
    "invoiceToType",
    "loanNumber",
    "siteID",
    "splitReceivableAmount",
    "splitReceivablePercent",
    "invoiceGrouping",
    "invoiceGroupingExistingInvoiceNumber",
    "billBrokerNet"
})
public class SendInvoiceToItem {

    @XmlElement(name = "AccountLookupCode", nillable = true)
    protected String accountLookupCode;
    @XmlElement(name = "Address", nillable = true)
    protected Address address;
    @XmlElement(name = "AddressDescription", nillable = true)
    protected String addressDescription;
    @XmlElement(name = "Contact", nillable = true)
    protected String contact;
    @XmlElement(name = "ContactID", nillable = true)
    protected Integer contactID;
    @XmlElement(name = "DeliveryMethod", nillable = true)
    protected String deliveryMethod;
    @XmlElement(name = "Email", nillable = true)
    protected String email;
    @XmlElement(name = "FaxCountryCode", nillable = true)
    protected String faxCountryCode;
    @XmlElement(name = "FaxExtension", nillable = true)
    protected String faxExtension;
    @XmlElement(name = "FaxNumber", nillable = true)
    protected String faxNumber;
    @XmlElement(name = "GenerateInvoice")
    protected Boolean generateInvoice;
    @XmlElement(name = "InvoiceMessage", nillable = true)
    protected String invoiceMessage;
    @XmlElement(name = "InvoiceNumber", nillable = true)
    protected Integer invoiceNumber;
    @XmlElement(name = "InvoiceToType", nillable = true)
    protected String invoiceToType;
    @XmlElement(name = "LoanNumber", nillable = true)
    protected String loanNumber;
    @XmlElement(name = "SiteID", nillable = true)
    protected String siteID;
    @XmlElement(name = "SplitReceivableAmount", nillable = true)
    protected BigDecimal splitReceivableAmount;
    @XmlElement(name = "SplitReceivablePercent", nillable = true)
    protected BigDecimal splitReceivablePercent;
    @XmlElement(name = "InvoiceGrouping", nillable = true)
    protected String invoiceGrouping;
    @XmlElement(name = "InvoiceGroupingExistingInvoiceNumber", nillable = true)
    protected Integer invoiceGroupingExistingInvoiceNumber;
    @XmlElement(name = "BillBrokerNet", nillable = true)
    protected Boolean billBrokerNet;

    /**
     * Gets the value of the accountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountLookupCode() {
        return accountLookupCode;
    }

    /**
     * Sets the value of the accountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountLookupCode(String value) {
        this.accountLookupCode = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the addressDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressDescription() {
        return addressDescription;
    }

    /**
     * Sets the value of the addressDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressDescription(String value) {
        this.addressDescription = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContactID(Integer value) {
        this.contactID = value;
    }

    /**
     * Gets the value of the deliveryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    /**
     * Sets the value of the deliveryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryMethod(String value) {
        this.deliveryMethod = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the faxCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxCountryCode() {
        return faxCountryCode;
    }

    /**
     * Sets the value of the faxCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxCountryCode(String value) {
        this.faxCountryCode = value;
    }

    /**
     * Gets the value of the faxExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxExtension() {
        return faxExtension;
    }

    /**
     * Sets the value of the faxExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxExtension(String value) {
        this.faxExtension = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the generateInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenerateInvoice() {
        return generateInvoice;
    }

    /**
     * Sets the value of the generateInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateInvoice(Boolean value) {
        this.generateInvoice = value;
    }

    /**
     * Gets the value of the invoiceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceMessage() {
        return invoiceMessage;
    }

    /**
     * Sets the value of the invoiceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceMessage(String value) {
        this.invoiceMessage = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceNumber(Integer value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the invoiceToType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceToType() {
        return invoiceToType;
    }

    /**
     * Sets the value of the invoiceToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceToType(String value) {
        this.invoiceToType = value;
    }

    /**
     * Gets the value of the loanNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNumber() {
        return loanNumber;
    }

    /**
     * Sets the value of the loanNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNumber(String value) {
        this.loanNumber = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the splitReceivableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSplitReceivableAmount() {
        return splitReceivableAmount;
    }

    /**
     * Sets the value of the splitReceivableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSplitReceivableAmount(BigDecimal value) {
        this.splitReceivableAmount = value;
    }

    /**
     * Gets the value of the splitReceivablePercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSplitReceivablePercent() {
        return splitReceivablePercent;
    }

    /**
     * Sets the value of the splitReceivablePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSplitReceivablePercent(BigDecimal value) {
        this.splitReceivablePercent = value;
    }

    /**
     * Gets the value of the invoiceGrouping property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceGrouping() {
        return invoiceGrouping;
    }

    /**
     * Sets the value of the invoiceGrouping property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceGrouping(String value) {
        this.invoiceGrouping = value;
    }

    /**
     * Gets the value of the invoiceGroupingExistingInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceGroupingExistingInvoiceNumber() {
        return invoiceGroupingExistingInvoiceNumber;
    }

    /**
     * Sets the value of the invoiceGroupingExistingInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceGroupingExistingInvoiceNumber(Integer value) {
        this.invoiceGroupingExistingInvoiceNumber = value;
    }

    /**
     * Gets the value of the billBrokerNet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBillBrokerNet() {
        return billBrokerNet;
    }

    /**
     * Sets the value of the billBrokerNet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBillBrokerNet(Boolean value) {
        this.billBrokerNet = value;
    }

}
