
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionCodeGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionCodeGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TransactionCodeID"/>
 *     &lt;enumeration value="TransactionCode"/>
 *     &lt;enumeration value="All"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionCodeGetType")
@XmlEnum
public enum TransactionCodeGetType {

    @XmlEnumValue("TransactionCodeID")
    TRANSACTION_CODE_ID("TransactionCodeID"),
    @XmlEnumValue("TransactionCode")
    TRANSACTION_CODE("TransactionCode"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    TransactionCodeGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionCodeGetType fromValue(String v) {
        for (TransactionCodeGetType c: TransactionCodeGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
