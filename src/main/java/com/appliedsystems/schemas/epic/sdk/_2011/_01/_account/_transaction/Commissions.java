
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.RevenueScheduleItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.SplitItems;


/**
 * <p>Java class for Commissions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Commissions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RevenueDeferralSchedule" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/}RevenueScheduleItems" minOccurs="0"/>
 *         &lt;element name="Splits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/}SplitItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Commissions", propOrder = {
    "revenueDeferralSchedule",
    "splits"
})
public class Commissions {

    @XmlElement(name = "RevenueDeferralSchedule", nillable = true)
    protected RevenueScheduleItems revenueDeferralSchedule;
    @XmlElement(name = "Splits", nillable = true)
    protected SplitItems splits;

    /**
     * Gets the value of the revenueDeferralSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link RevenueScheduleItems }
     *     
     */
    public RevenueScheduleItems getRevenueDeferralSchedule() {
        return revenueDeferralSchedule;
    }

    /**
     * Sets the value of the revenueDeferralSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link RevenueScheduleItems }
     *     
     */
    public void setRevenueDeferralSchedule(RevenueScheduleItems value) {
        this.revenueDeferralSchedule = value;
    }

    /**
     * Gets the value of the splits property.
     * 
     * @return
     *     possible object is
     *     {@link SplitItems }
     *     
     */
    public SplitItems getSplits() {
        return splits;
    }

    /**
     * Sets the value of the splits property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitItems }
     *     
     */
    public void setSplits(SplitItems value) {
        this.splits = value;
    }

}
