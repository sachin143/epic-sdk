
package com.appliedsystems.schemas.epic.sdk._2017._02._account._contact;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._contact package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IdentificationNumberItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", "IdentificationNumberItems");
    private final static QName _IdentificationNumberItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", "IdentificationNumberItem");
    private final static QName _BusinessInformation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", "BusinessInformation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._contact
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusinessInformation }
     * 
     */
    public BusinessInformation createBusinessInformation() {
        return new BusinessInformation();
    }

    /**
     * Create an instance of {@link IdentificationNumberItems }
     * 
     */
    public IdentificationNumberItems createIdentificationNumberItems() {
        return new IdentificationNumberItems();
    }

    /**
     * Create an instance of {@link IdentificationNumberItem }
     * 
     */
    public IdentificationNumberItem createIdentificationNumberItem() {
        return new IdentificationNumberItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationNumberItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", name = "IdentificationNumberItems")
    public JAXBElement<IdentificationNumberItems> createIdentificationNumberItems(IdentificationNumberItems value) {
        return new JAXBElement<IdentificationNumberItems>(_IdentificationNumberItems_QNAME, IdentificationNumberItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationNumberItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", name = "IdentificationNumberItem")
    public JAXBElement<IdentificationNumberItem> createIdentificationNumberItem(IdentificationNumberItem value) {
        return new JAXBElement<IdentificationNumberItem>(_IdentificationNumberItem_QNAME, IdentificationNumberItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/", name = "BusinessInformation")
    public JAXBElement<BusinessInformation> createBusinessInformation(BusinessInformation value) {
        return new JAXBElement<BusinessInformation>(_BusinessInformation_QNAME, BusinessInformation.class, null, value);
    }

}
