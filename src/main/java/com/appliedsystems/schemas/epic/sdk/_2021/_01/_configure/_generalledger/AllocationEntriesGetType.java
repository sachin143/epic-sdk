
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllocationEntriesGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllocationEntriesGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AllocationMethodID"/>
 *     &lt;enumeration value="All"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AllocationEntriesGetType")
@XmlEnum
public enum AllocationEntriesGetType {

    @XmlEnumValue("AllocationMethodID")
    ALLOCATION_METHOD_ID("AllocationMethodID"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    AllocationEntriesGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AllocationEntriesGetType fromValue(String v) {
        for (AllocationEntriesGetType c: AllocationEntriesGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
