
package com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LookupTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LookupTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Prefix"/>
 *     &lt;enumeration value="Suffix"/>
 *     &lt;enumeration value="PhoneNumberType"/>
 *     &lt;enumeration value="Agency"/>
 *     &lt;enumeration value="Branch"/>
 *     &lt;enumeration value="ServicingRoles"/>
 *     &lt;enumeration value="EmployeesForServicingRoles"/>
 *     &lt;enumeration value="Broker"/>
 *     &lt;enumeration value="ADCCategories"/>
 *     &lt;enumeration value="ADC"/>
 *     &lt;enumeration value="InActivateReason"/>
 *     &lt;enumeration value="InvoicePageBreak"/>
 *     &lt;enumeration value="InvoiceLayout"/>
 *     &lt;enumeration value="StatementPageBreak"/>
 *     &lt;enumeration value="StatementLayout"/>
 *     &lt;enumeration value="ContactVia"/>
 *     &lt;enumeration value="ContactMethod"/>
 *     &lt;enumeration value="DeliveryMethod"/>
 *     &lt;enumeration value="MarketingMethod"/>
 *     &lt;enumeration value="CallPermission"/>
 *     &lt;enumeration value="Language"/>
 *     &lt;enumeration value="DriverType"/>
 *     &lt;enumeration value="GoodStudent"/>
 *     &lt;enumeration value="DriverTraining"/>
 *     &lt;enumeration value="Gender"/>
 *     &lt;enumeration value="MaritalStatus"/>
 *     &lt;enumeration value="Occupation"/>
 *     &lt;enumeration value="Classification"/>
 *     &lt;enumeration value="NAICSCategory"/>
 *     &lt;enumeration value="NAICS"/>
 *     &lt;enumeration value="SICCategory"/>
 *     &lt;enumeration value="SIC"/>
 *     &lt;enumeration value="CreditBureau"/>
 *     &lt;enumeration value="BusinessType"/>
 *     &lt;enumeration value="NatureOfBusiness"/>
 *     &lt;enumeration value="ContactDescription"/>
 *     &lt;enumeration value="AddressDescription"/>
 *     &lt;enumeration value="ActivityCode"/>
 *     &lt;enumeration value="ClosedReason"/>
 *     &lt;enumeration value="AccessLevel"/>
 *     &lt;enumeration value="TaskStatus"/>
 *     &lt;enumeration value="Priority"/>
 *     &lt;enumeration value="WhoOwnerCode"/>
 *     &lt;enumeration value="TaskOwnerType"/>
 *     &lt;enumeration value="ActivityAssociatedToType"/>
 *     &lt;enumeration value="ClosedStatus"/>
 *     &lt;enumeration value="PolicyType"/>
 *     &lt;enumeration value="LineStatus"/>
 *     &lt;enumeration value="Department"/>
 *     &lt;enumeration value="StateProvince"/>
 *     &lt;enumeration value="Country"/>
 *     &lt;enumeration value="IssuingCompany"/>
 *     &lt;enumeration value="BillingCompany"/>
 *     &lt;enumeration value="ProfitCenter"/>
 *     &lt;enumeration value="AgencyCommissionType"/>
 *     &lt;enumeration value="BrokerCommissionType"/>
 *     &lt;enumeration value="ProducerCommissionType"/>
 *     &lt;enumeration value="AgencyForClientID"/>
 *     &lt;enumeration value="BranchForClientID"/>
 *     &lt;enumeration value="BillingPlan"/>
 *     &lt;enumeration value="PolicySource"/>
 *     &lt;enumeration value="LineType"/>
 *     &lt;enumeration value="CommissionAgreement"/>
 *     &lt;enumeration value="ProducerBrokerCode"/>
 *     &lt;enumeration value="BrokerCommissionAgreement"/>
 *     &lt;enumeration value="ProducerCommissionAgreement"/>
 *     &lt;enumeration value="TransactionCode"/>
 *     &lt;enumeration value="InvoiceToType"/>
 *     &lt;enumeration value="AccountType"/>
 *     &lt;enumeration value="Producer"/>
 *     &lt;enumeration value="MessageGallery"/>
 *     &lt;enumeration value="FormAndEndorsementNumber"/>
 *     &lt;enumeration value="CopyrightType"/>
 *     &lt;enumeration value="BuildingInterest"/>
 *     &lt;enumeration value="PaymentPlan"/>
 *     &lt;enumeration value="Audit"/>
 *     &lt;enumeration value="Carrier"/>
 *     &lt;enumeration value="PersonalApplicationBillingCode"/>
 *     &lt;enumeration value="YesNo"/>
 *     &lt;enumeration value="PersonalApplicationLossHistoryEnteredBy"/>
 *     &lt;enumeration value="VehicleMakeModel"/>
 *     &lt;enumeration value="VehicleBodyType"/>
 *     &lt;enumeration value="PaymentMethod"/>
 *     &lt;enumeration value="PersonalApplicationMailPolicyTo"/>
 *     &lt;enumeration value="PayorCode"/>
 *     &lt;enumeration value="EmployeeType"/>
 *     &lt;enumeration value="CreditCardCompany"/>
 *     &lt;enumeration value="FinanceCompany"/>
 *     &lt;enumeration value="RelationToApplicant"/>
 *     &lt;enumeration value="SymbolAgeGroup"/>
 *     &lt;enumeration value="Territory"/>
 *     &lt;enumeration value="Usage"/>
 *     &lt;enumeration value="Performance"/>
 *     &lt;enumeration value="MultiCarCredit"/>
 *     &lt;enumeration value="Garaged"/>
 *     &lt;enumeration value="AntiTheftDevices"/>
 *     &lt;enumeration value="Interest"/>
 *     &lt;enumeration value="AccidentTypeCode"/>
 *     &lt;enumeration value="DrivingRecordCode"/>
 *     &lt;enumeration value="MilitaryBranch"/>
 *     &lt;enumeration value="MedicationTreatment"/>
 *     &lt;enumeration value="LimitAppliesTo"/>
 *     &lt;enumeration value="PhoneType"/>
 *     &lt;enumeration value="Jurisdiction"/>
 *     &lt;enumeration value="PaymentType"/>
 *     &lt;enumeration value="LossType"/>
 *     &lt;enumeration value="ClaimCode"/>
 *     &lt;enumeration value="ClaimType"/>
 *     &lt;enumeration value="AvailableRiskType"/>
 *     &lt;enumeration value="Risk"/>
 *     &lt;enumeration value="PayeeType"/>
 *     &lt;enumeration value="AvailableRiskTypeForCustomForm"/>
 *     &lt;enumeration value="RiskForCustomForm"/>
 *     &lt;enumeration value="CoverageGeneralAggregateAppliesTo"/>
 *     &lt;enumeration value="CoveragesDeductiblesBasis"/>
 *     &lt;enumeration value="HazardClass"/>
 *     &lt;enumeration value="PremiumBasis"/>
 *     &lt;enumeration value="AdditionalCoverageCode"/>
 *     &lt;enumeration value="InsuranceRequestedFormNumber"/>
 *     &lt;enumeration value="InsuranceRequestedFormType"/>
 *     &lt;enumeration value="ResidentialRatingUnderwritingUsage"/>
 *     &lt;enumeration value="Occupancy"/>
 *     &lt;enumeration value="Sprinkler"/>
 *     &lt;enumeration value="HousekeepingCondition"/>
 *     &lt;enumeration value="DistanceToTidalWater"/>
 *     &lt;enumeration value="DoorLock"/>
 *     &lt;enumeration value="CourseOfConstruction"/>
 *     &lt;enumeration value="SmokeProtectionDevice"/>
 *     &lt;enumeration value="TemperatureFireProtectionDevice"/>
 *     &lt;enumeration value="BurglarProtectionDevice"/>
 *     &lt;enumeration value="Wiring"/>
 *     &lt;enumeration value="HeatType"/>
 *     &lt;enumeration value="Rating"/>
 *     &lt;enumeration value="HomeownerDwellingFireFoundation"/>
 *     &lt;enumeration value="FuelStorageTankLocation"/>
 *     &lt;enumeration value="FuelStorageLineLocation"/>
 *     &lt;enumeration value="ResidenceType"/>
 *     &lt;enumeration value="DwellingLocation"/>
 *     &lt;enumeration value="SwimmingPoolIs"/>
 *     &lt;enumeration value="PlumbingCondition"/>
 *     &lt;enumeration value="RoofCondition"/>
 *     &lt;enumeration value="RoofType"/>
 *     &lt;enumeration value="WindClass"/>
 *     &lt;enumeration value="StormShutters"/>
 *     &lt;enumeration value="Renovation"/>
 *     &lt;enumeration value="AnimalType"/>
 *     &lt;enumeration value="AnimalBreed"/>
 *     &lt;enumeration value="MaterialUnattached"/>
 *     &lt;enumeration value="EvaluationMethod"/>
 *     &lt;enumeration value="EvaluationSystem"/>
 *     &lt;enumeration value="ResidenceCondition"/>
 *     &lt;enumeration value="AirConditioning"/>
 *     &lt;enumeration value="Breezeway"/>
 *     &lt;enumeration value="Basement"/>
 *     &lt;enumeration value="PorchType"/>
 *     &lt;enumeration value="GarageType"/>
 *     &lt;enumeration value="RecreationalVehicleType"/>
 *     &lt;enumeration value="Fuel"/>
 *     &lt;enumeration value="StoveType"/>
 *     &lt;enumeration value="TestingLaboratoryLabel"/>
 *     &lt;enumeration value="UnitType"/>
 *     &lt;enumeration value="SolidFuelDeviceConstruction"/>
 *     &lt;enumeration value="LocationOfSolidFuelDevice"/>
 *     &lt;enumeration value="InstallationInspectedBy"/>
 *     &lt;enumeration value="HeatingUse"/>
 *     &lt;enumeration value="InstallationDoneBy"/>
 *     &lt;enumeration value="ChimneyConstruction"/>
 *     &lt;enumeration value="ChimneyAttached"/>
 *     &lt;enumeration value="StovePipeType"/>
 *     &lt;enumeration value="VentStyle"/>
 *     &lt;enumeration value="ProtectiveMaterial"/>
 *     &lt;enumeration value="FrequencyCleanedInspected"/>
 *     &lt;enumeration value="ResidentialBusinessType"/>
 *     &lt;enumeration value="ResidentialTypeOfBusiness"/>
 *     &lt;enumeration value="ResidentialPropertyCoverageValuation"/>
 *     &lt;enumeration value="DeductibleBasis"/>
 *     &lt;enumeration value="OtherDeductibleType"/>
 *     &lt;enumeration value="TieDown"/>
 *     &lt;enumeration value="CookingLocation"/>
 *     &lt;enumeration value="MobileHomeFoundation"/>
 *     &lt;enumeration value="StrapsCablesType"/>
 *     &lt;enumeration value="AnchorTypeForTieDowns"/>
 *     &lt;enumeration value="OtherOptionalCoverageCode"/>
 *     &lt;enumeration value="OtherOptionalCoverageDeductibleType"/>
 *     &lt;enumeration value="VehicleRatingFarthestTerminal"/>
 *     &lt;enumeration value="VehicleRatingDriveToWorkSchool"/>
 *     &lt;enumeration value="VehicleRatingUse"/>
 *     &lt;enumeration value="VehicleRatingSpecialUse"/>
 *     &lt;enumeration value="VehicleRatingPurchased"/>
 *     &lt;enumeration value="VehicleRatingLeased"/>
 *     &lt;enumeration value="ValuationType"/>
 *     &lt;enumeration value="ConstructionType"/>
 *     &lt;enumeration value="BurglarAlarm"/>
 *     &lt;enumeration value="ExtentOfProtection"/>
 *     &lt;enumeration value="ClockHourly"/>
 *     &lt;enumeration value="TypeOfFireProtection"/>
 *     &lt;enumeration value="PropertyPremiseCoveragesOptions"/>
 *     &lt;enumeration value="SubjectOfInsurance"/>
 *     &lt;enumeration value="Valuation"/>
 *     &lt;enumeration value="CauseOfLoss"/>
 *     &lt;enumeration value="PropertyBusinessIncomeCoverageTypeOfBusiness"/>
 *     &lt;enumeration value="CoinsurancePercentage"/>
 *     &lt;enumeration value="ApplicableCausesOfLoss"/>
 *     &lt;enumeration value="LimitLossPayCode"/>
 *     &lt;enumeration value="PropertyType"/>
 *     &lt;enumeration value="MediaUsed"/>
 *     &lt;enumeration value="UnderlyingInsuranceGeneralLiabilityType"/>
 *     &lt;enumeration value="AlarmType"/>
 *     &lt;enumeration value="AlarmDescription"/>
 *     &lt;enumeration value="SafeVault"/>
 *     &lt;enumeration value="PremisesAlarm"/>
 *     &lt;enumeration value="Label"/>
 *     &lt;enumeration value="FrequencyOfDeposits"/>
 *     &lt;enumeration value="DeadboltCylinderDoorLocks"/>
 *     &lt;enumeration value="CoveragesPropertyValuation"/>
 *     &lt;enumeration value="AdditionalLiabilityCoverageCode"/>
 *     &lt;enumeration value="OtherAdditionalCoverageCode"/>
 *     &lt;enumeration value="GLCode"/>
 *     &lt;enumeration value="InsideCityLimits"/>
 *     &lt;enumeration value="ClassCode"/>
 *     &lt;enumeration value="AttachmentFolder"/>
 *     &lt;enumeration value="AttachmentAccessLevel"/>
 *     &lt;enumeration value="AttachmentAttachTo"/>
 *     &lt;enumeration value="PolicyTypesOfSameBusinessType"/>
 *     &lt;enumeration value="PremiumPayableType"/>
 *     &lt;enumeration value="Prefill"/>
 *     &lt;enumeration value="TransactionInvoiceToType"/>
 *     &lt;enumeration value="CustomFormOptions"/>
 *     &lt;enumeration value="CancellationReason"/>
 *     &lt;enumeration value="MethodOfCancellationMethod"/>
 *     &lt;enumeration value="VehicleCompOTCSym"/>
 *     &lt;enumeration value="VehicleCollSym"/>
 *     &lt;enumeration value="Vendor"/>
 *     &lt;enumeration value="GeneralLedgerSchedule"/>
 *     &lt;enumeration value="GeneralLedgerTransferOfFundsFromBankAccount"/>
 *     &lt;enumeration value="GeneralLedgerTransferOfFundsToBankAccount"/>
 *     &lt;enumeration value="GeneralLedgerTransferOfFundsFromAgency"/>
 *     &lt;enumeration value="GeneralLedgerTransferOfFundsToAgency"/>
 *     &lt;enumeration value="GeneralLedgerPayVouchersBankAccount"/>
 *     &lt;enumeration value="GeneralLedgerPayVouchersAgency"/>
 *     &lt;enumeration value="ReceiptBankAccount"/>
 *     &lt;enumeration value="ReceiptDetailItemType"/>
 *     &lt;enumeration value="ReceiptDetailItemAccount"/>
 *     &lt;enumeration value="ReceiptDetailItemMethod"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyTo"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToClient"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToPolicyClient"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToPolicyLine"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToSelectedItemsAgency"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToAccountStructureAgencyBranch"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyClient"/>
 *     &lt;enumeration value="ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyLine"/>
 *     &lt;enumeration value="ReceiptDetailItemDefaultGeneralLedgerAccount"/>
 *     &lt;enumeration value="ReceiptDetailItemGeneralLedgerAccount"/>
 *     &lt;enumeration value="ReceiptDetailItemStructureAgency"/>
 *     &lt;enumeration value="ReceiptDetailItemStructureBranch"/>
 *     &lt;enumeration value="ReceiptDetailItemStructureDepartment"/>
 *     &lt;enumeration value="ReceiptDetailItemStructureProfitCenter"/>
 *     &lt;enumeration value="DisbursementBankAccount"/>
 *     &lt;enumeration value="DisbursementDefaultEntry"/>
 *     &lt;enumeration value="DisbursementPayToTheOrderOfAccountType"/>
 *     &lt;enumeration value="DisbursementPayToTheOrderOfAccount"/>
 *     &lt;enumeration value="DisbursementDetailDefaultEntry"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyTo"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToBroker"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToClient"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToFinanceCompany"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToOtherInterest"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToPolicyClient"/>
 *     &lt;enumeration value="DisbursementDetailItemApplyToPolicyLine"/>
 *     &lt;enumeration value="DisbursementDetailItemGeneralLedgerAccount"/>
 *     &lt;enumeration value="DisbursementDetailItemStructureAgency"/>
 *     &lt;enumeration value="DisbursementDetailItemStructureBranch"/>
 *     &lt;enumeration value="DisbursementDetailItemStructureDepartment"/>
 *     &lt;enumeration value="DisbursementDetailItemStructureProfitCenter"/>
 *     &lt;enumeration value="DisbursementVoidReason"/>
 *     &lt;enumeration value="VoucherBankAccount"/>
 *     &lt;enumeration value="VoucherDefaultEntry"/>
 *     &lt;enumeration value="VoucherPayToTheOrderOfAccountType"/>
 *     &lt;enumeration value="VoucherPayToTheOrderOfAccount"/>
 *     &lt;enumeration value="VoucherDiscountType"/>
 *     &lt;enumeration value="VoucherDetailDefaultEntry"/>
 *     &lt;enumeration value="VoucherDetailItemApplyTo"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToBroker"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToClient"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToFinanceCompany"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToOtherInterest"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToPolicyClient"/>
 *     &lt;enumeration value="VoucherDetailItemApplyToPolicyLine"/>
 *     &lt;enumeration value="VoucherDetailItemGeneralLedgerAccount"/>
 *     &lt;enumeration value="VoucherDetailItemStructureAgency"/>
 *     &lt;enumeration value="VoucherDetailItemStructureBranch"/>
 *     &lt;enumeration value="VoucherDetailItemStructureDepartment"/>
 *     &lt;enumeration value="VoucherDetailItemStructureProfitCenter"/>
 *     &lt;enumeration value="VoucherVoidReason"/>
 *     &lt;enumeration value="JournalEntryDefaultEntry"/>
 *     &lt;enumeration value="JournalEntryAssociateToAccountType"/>
 *     &lt;enumeration value="JournalEntryAssociateToAccount"/>
 *     &lt;enumeration value="JournalEntryAutomaticallyReverseReason"/>
 *     &lt;enumeration value="JournalEntryVoidReason"/>
 *     &lt;enumeration value="JournalEntryDetailDefaultEntry"/>
 *     &lt;enumeration value="JournalEntryDetailItemGeneralLedgerAccount"/>
 *     &lt;enumeration value="JournalEntryDetailItemStructureAgency"/>
 *     &lt;enumeration value="JournalEntryDetailItemStructureBranch"/>
 *     &lt;enumeration value="JournalEntryDetailItemStructureDepartment"/>
 *     &lt;enumeration value="JournalEntryDetailItemStructureProfitCenter"/>
 *     &lt;enumeration value="VehicleCoverageStacking"/>
 *     &lt;enumeration value="LinePremiumPayableContract"/>
 *     &lt;enumeration value="TransactionPremiumPayableContract"/>
 *     &lt;enumeration value="TransactionPremiumPayable"/>
 *     &lt;enumeration value="MultiCarrierSchedulePremiumPayableContract"/>
 *     &lt;enumeration value="TransactionApplyCreditsToDebitsAgency"/>
 *     &lt;enumeration value="TransactionAccountsReceivableWriteOffTransactionCode"/>
 *     &lt;enumeration value="TransactionFinanceCompany"/>
 *     &lt;enumeration value="TransactionFinanceDeliveryMethod"/>
 *     &lt;enumeration value="TransactionMove"/>
 *     &lt;enumeration value="TransactionMoveTo"/>
 *     &lt;enumeration value="TransactionReverseReason"/>
 *     &lt;enumeration value="TransactionVoidPaymentReason"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionCode"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionAgency"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionBranch"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionDepartment"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionProfitCenter"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionDeliveryMethod"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionInvoiceGrouping"/>
 *     &lt;enumeration value="TransactionVoidPaymentNSFTransactionInvoiceGroupingExistingInvoice"/>
 *     &lt;enumeration value="ActivityCategory"/>
 *     &lt;enumeration value="ActivityIssuingCompany"/>
 *     &lt;enumeration value="ActivityPremiumPayableType"/>
 *     &lt;enumeration value="ActivityPremiumPayable"/>
 *     &lt;enumeration value="SmokeDetectors"/>
 *     &lt;enumeration value="CoverageAppliesTo"/>
 *     &lt;enumeration value="TransactionInvoiceGrouping"/>
 *     &lt;enumeration value="TransactionInvoiceGroupingExisitingInvoice"/>
 *     &lt;enumeration value="TransactionRevenueDeferralScheduleType"/>
 *     &lt;enumeration value="BondTerm"/>
 *     &lt;enumeration value="RenewalMethod"/>
 *     &lt;enumeration value="CountersigningAgent"/>
 *     &lt;enumeration value="SuretyTermsType"/>
 *     &lt;enumeration value="ChargeType"/>
 *     &lt;enumeration value="Currency"/>
 *     &lt;enumeration value="ObligeeLookupCode"/>
 *     &lt;enumeration value="NameType"/>
 *     &lt;enumeration value="SuretyBillingType"/>
 *     &lt;enumeration value="MethodCode"/>
 *     &lt;enumeration value="PremiumPaymentSupplementFinanceCompany"/>
 *     &lt;enumeration value="Company"/>
 *     &lt;enumeration value="AccountStatus"/>
 *     &lt;enumeration value="AccountNonClientAgency"/>
 *     &lt;enumeration value="AccountNonClientBranch"/>
 *     &lt;enumeration value="AccountNonClientDepartment"/>
 *     &lt;enumeration value="AccountNonClientProfitCenter"/>
 *     &lt;enumeration value="AgencyDefinedCategory"/>
 *     &lt;enumeration value="AgencyDefinedCategoryOption"/>
 *     &lt;enumeration value="DefinedRelationships"/>
 *     &lt;enumeration value="BatchPaymentMethod"/>
 *     &lt;enumeration value="ContractType"/>
 *     &lt;enumeration value="PaymentDueDate"/>
 *     &lt;enumeration value="PaymentDueDateAddSubtract"/>
 *     &lt;enumeration value="PayableContractStructureAgency"/>
 *     &lt;enumeration value="PayableContractStructureBranch"/>
 *     &lt;enumeration value="AccountPayableSubaccount"/>
 *     &lt;enumeration value="LocateAdditionalPartyBy"/>
 *     &lt;enumeration value="AllServicingRoles"/>
 *     &lt;enumeration value="ClaimAdjustorID"/>
 *     &lt;enumeration value="Statuses"/>
 *     &lt;enumeration value="TypeOfBusiness"/>
 *     &lt;enumeration value="TermDefault"/>
 *     &lt;enumeration value="ApplicationDetail"/>
 *     &lt;enumeration value="BillingMode"/>
 *     &lt;enumeration value="TransactionClass"/>
 *     &lt;enumeration value="AvailableDescriptionVariables"/>
 *     &lt;enumeration value="AvailableGeneralLedgerAccounts"/>
 *     &lt;enumeration value="PolicyTypeClassification"/>
 *     &lt;enumeration value="ManualClosedStatus"/>
 *     &lt;enumeration value="OutlookIntegration"/>
 *     &lt;enumeration value="ReconciliationMethod"/>
 *     &lt;enumeration value="ParentCompany"/>
 *     &lt;enumeration value="CertificateType"/>
 *     &lt;enumeration value="CertificateOtherLineOfBusiness"/>
 *     &lt;enumeration value="CertificateGeneralLiabilityLineOfBusiness"/>
 *     &lt;enumeration value="CertificateAutomobileLineOfBusiness"/>
 *     &lt;enumeration value="CertificateExcessUmbrellaLineOfBusiness"/>
 *     &lt;enumeration value="CertificateWorkersCompensationEmployersLineOfBusiness"/>
 *     &lt;enumeration value="CertificatePropertyLineOfBusiness"/>
 *     &lt;enumeration value="CertificateInlandMarineLineOfBusiness"/>
 *     &lt;enumeration value="CertificateCrimeLineOfBusiness"/>
 *     &lt;enumeration value="CertificateBoilerAndMachineryLineOfBusiness"/>
 *     &lt;enumeration value="CertificateCargoLineOfBusiness"/>
 *     &lt;enumeration value="CertificateTrailerInterchangeLineOfBusiness"/>
 *     &lt;enumeration value="CertificateLineServiceSummary"/>
 *     &lt;enumeration value="ProofsOfInsuranceDistributeVia"/>
 *     &lt;enumeration value="ProofsOfInsuranceInactivationReason"/>
 *     &lt;enumeration value="IncludedExcluded"/>
 *     &lt;enumeration value="SubmissionMethod"/>
 *     &lt;enumeration value="InstallmentPremiumPayment"/>
 *     &lt;enumeration value="Deposit"/>
 *     &lt;enumeration value="WorkersCompensationEmployersLiability"/>
 *     &lt;enumeration value="PremiumPaymentOption"/>
 *     &lt;enumeration value="PortableAccessoryEquipmentType"/>
 *     &lt;enumeration value="EngineFuelType"/>
 *     &lt;enumeration value="HullType"/>
 *     &lt;enumeration value="PowerType"/>
 *     &lt;enumeration value="HullMaterial"/>
 *     &lt;enumeration value="HullDesign"/>
 *     &lt;enumeration value="FuelTank"/>
 *     &lt;enumeration value="SparMaterial"/>
 *     &lt;enumeration value="WatersNavigated"/>
 *     &lt;enumeration value="Commission"/>
 *     &lt;enumeration value="CommissionType"/>
 *     &lt;enumeration value="CommissionLineOfBusiness"/>
 *     &lt;enumeration value="CommissionCompany"/>
 *     &lt;enumeration value="OtherInterestCode"/>
 *     &lt;enumeration value="CommissionBranch"/>
 *     &lt;enumeration value="CommissionDepartment"/>
 *     &lt;enumeration value="CommissionProfitCenter"/>
 *     &lt;enumeration value="InstallmentType"/>
 *     &lt;enumeration value="InstallmentPlan"/>
 *     &lt;enumeration value="DistributeVia"/>
 *     &lt;enumeration value="CompensationType"/>
 *     &lt;enumeration value="EmployeeAuthorizedSignatureUserSecurityGroup"/>
 *     &lt;enumeration value="EmployeeAuthorizedSignatureUserIndividualEmployee"/>
 *     &lt;enumeration value="EmployeeShareRevenuePrimaryOfficeAgency"/>
 *     &lt;enumeration value="EmployeeShareRevenuePrimaryOfficeBranch"/>
 *     &lt;enumeration value="EmployeeShareRevenuePrimaryOfficeDepartment"/>
 *     &lt;enumeration value="EmployeeShareRevenuePrimaryOfficeProfitCenter"/>
 *     &lt;enumeration value="EmployeeShareRevenueStructureRegion"/>
 *     &lt;enumeration value="EmployeeShareRevenueStructureAgency"/>
 *     &lt;enumeration value="EmployeeShareRevenueStructureBranch"/>
 *     &lt;enumeration value="EmployeeShareRevenueStructureDepartment"/>
 *     &lt;enumeration value="EmployeeShareRevenueStructureProfitCenter"/>
 *     &lt;enumeration value="TransactionCommissionProducer"/>
 *     &lt;enumeration value="TransactionProducerBrokerCommissionContract"/>
 *     &lt;enumeration value="LineProducerBrokerCommissionContract"/>
 *     &lt;enumeration value="BrokerReceivableCommissionType"/>
 *     &lt;enumeration value="ShareRevenueIsEnabledSystemSetting"/>
 *     &lt;enumeration value="ShareRevenueProducer"/>
 *     &lt;enumeration value="EmployeeRevenueAgency"/>
 *     &lt;enumeration value="EmployeeRevenueBranch"/>
 *     &lt;enumeration value="EmployeeRevenueDepartment"/>
 *     &lt;enumeration value="EmployeeRevenueProfitCenter"/>
 *     &lt;enumeration value="AvailableSupplementalScreens"/>
 *     &lt;enumeration value="EquipmentFloaterCoverageDeductibleType"/>
 *     &lt;enumeration value="GarageAndDealersPremises"/>
 *     &lt;enumeration value="GarageAndDealersLocationNumber"/>
 *     &lt;enumeration value="PersonalUmbrellaAvailablePolicyLines"/>
 *     &lt;enumeration value="ActiveLanguages"/>
 *     &lt;enumeration value="ReceiptDetailItemPayablesCommissionsItemsAccountLocateBy"/>
 *     &lt;enumeration value="ReceiptDetailItemPayablesCommissionsItemsToPay"/>
 *     &lt;enumeration value="ReceiptDetailItemPayablesCommissionsAvailableStatements"/>
 *     &lt;enumeration value="ReceiptDetailItemPayablesCommissionsPayableType"/>
 *     &lt;enumeration value="ReceiptDetailItemPayablesCommissionsCashOnAccountApplyTo"/>
 *     &lt;enumeration value="PrimaryCountry"/>
 *     &lt;enumeration value="CountryOfOperation"/>
 *     &lt;enumeration value="InternationalDialingCodes"/>
 *     &lt;enumeration value="CanadaBillingType"/>
 *     &lt;enumeration value="CanadaWhoSendsDocuments"/>
 *     &lt;enumeration value="RefusalType"/>
 *     &lt;enumeration value="RefusalReason"/>
 *     &lt;enumeration value="LineOfBusinessType"/>
 *     &lt;enumeration value="CanadaPhoneType"/>
 *     &lt;enumeration value="LegalEntity"/>
 *     &lt;enumeration value="CanadaCauseOfLoss"/>
 *     &lt;enumeration value="CanadaInterestType"/>
 *     &lt;enumeration value="Attachment"/>
 *     &lt;enumeration value="LocationAccess"/>
 *     &lt;enumeration value="EvaluatorProduct"/>
 *     &lt;enumeration value="NumberOfStoreys"/>
 *     &lt;enumeration value="CanadaRelationToApplicant"/>
 *     &lt;enumeration value="InspectionSourceReport"/>
 *     &lt;enumeration value="CanadaOccupancy"/>
 *     &lt;enumeration value="CanadaStructureType"/>
 *     &lt;enumeration value="FoundationType"/>
 *     &lt;enumeration value="CanadaConstruction"/>
 *     &lt;enumeration value="CanadaSwimmingPoolType"/>
 *     &lt;enumeration value="GarageTypeCode"/>
 *     &lt;enumeration value="HeatingApparatus"/>
 *     &lt;enumeration value="CanadaFuelType"/>
 *     &lt;enumeration value="HeatingLocation"/>
 *     &lt;enumeration value="OilTankLocation"/>
 *     &lt;enumeration value="MetersToHydrant"/>
 *     &lt;enumeration value="KilometersToFirehall"/>
 *     &lt;enumeration value="DetectorType"/>
 *     &lt;enumeration value="SecurityType"/>
 *     &lt;enumeration value="CanadaRoofingType"/>
 *     &lt;enumeration value="Amps"/>
 *     &lt;enumeration value="CanadaWiring"/>
 *     &lt;enumeration value="Service"/>
 *     &lt;enumeration value="InteriorWallConstructionType"/>
 *     &lt;enumeration value="AdditionalInteriorQuality"/>
 *     &lt;enumeration value="DeductibleType"/>
 *     &lt;enumeration value="DeductibleType1"/>
 *     &lt;enumeration value="DeductibleType2"/>
 *     &lt;enumeration value="DeductibleType3"/>
 *     &lt;enumeration value="DeductibleType4"/>
 *     &lt;enumeration value="Discount"/>
 *     &lt;enumeration value="PackageForm"/>
 *     &lt;enumeration value="RatingPlan"/>
 *     &lt;enumeration value="CoveragesDeductibleType"/>
 *     &lt;enumeration value="LiabilityExtension"/>
 *     &lt;enumeration value="DetachedOutbuildingStructureType"/>
 *     &lt;enumeration value="WatercraftClass"/>
 *     &lt;enumeration value="WatercraftStyle"/>
 *     &lt;enumeration value="WatercraftType"/>
 *     &lt;enumeration value="Watercraftconstruction"/>
 *     &lt;enumeration value="WatercraftUse"/>
 *     &lt;enumeration value="WatercraftNavigated"/>
 *     &lt;enumeration value="CoverageDiscountType"/>
 *     &lt;enumeration value="DiscountType"/>
 *     &lt;enumeration value="Mooring"/>
 *     &lt;enumeration value="PropertyCoverage"/>
 *     &lt;enumeration value="PropertyClass"/>
 *     &lt;enumeration value="CertificationLabel"/>
 *     &lt;enumeration value="FuelHeatingLocation"/>
 *     &lt;enumeration value="HeatingType"/>
 *     &lt;enumeration value="ChimneyType"/>
 *     &lt;enumeration value="LiningType"/>
 *     &lt;enumeration value="CleaningByWhom"/>
 *     &lt;enumeration value="ChimneyLocation"/>
 *     &lt;enumeration value="ChimneyBuiltFrom"/>
 *     &lt;enumeration value="Construction"/>
 *     &lt;enumeration value="StovePipeConstruction"/>
 *     &lt;enumeration value="WatercraftTrailerStorage"/>
 *     &lt;enumeration value="CanadaTypeOfCharges"/>
 *     &lt;enumeration value="CanadaCreditCardCompany"/>
 *     &lt;enumeration value="CanadaPaymentPlan"/>
 *     &lt;enumeration value="CanadaPremiumAppearsCompany"/>
 *     &lt;enumeration value="CanadaDriverConviction"/>
 *     &lt;enumeration value="CanadaDriverLicenseCancellationReason"/>
 *     &lt;enumeration value="CanadaClaimPaidBack"/>
 *     &lt;enumeration value="CanadaClaimPaidUnder"/>
 *     &lt;enumeration value="CanadaVehicleFuelType"/>
 *     &lt;enumeration value="CanadaVehicleUsage"/>
 *     &lt;enumeration value="CanadaVehicleBodyType"/>
 *     &lt;enumeration value="CanadaNewUsed"/>
 *     &lt;enumeration value="CanadaFacilityTiers"/>
 *     &lt;enumeration value="MotorVehicleLiabilityInsuranceCardIssued"/>
 *     &lt;enumeration value="SignedBy"/>
 *     &lt;enumeration value="CanadaDriverGender"/>
 *     &lt;enumeration value="CanadaDriverMaritalStatus"/>
 *     &lt;enumeration value="CanadaDriverTraining"/>
 *     &lt;enumeration value="CanadaCarPoolFrequency"/>
 *     &lt;enumeration value="CanadaSecurityDeviceType"/>
 *     &lt;enumeration value="CanadaSecurityProductCode"/>
 *     &lt;enumeration value="CanadaSecurityCharacteristics"/>
 *     &lt;enumeration value="CanadaVolatileToxicMaterialType"/>
 *     &lt;enumeration value="CanadaDeliveryType"/>
 *     &lt;enumeration value="CanadaDeliveryFrequency"/>
 *     &lt;enumeration value="CanadaVehicleIncomeReplacement"/>
 *     &lt;enumeration value="CanadaDiscount"/>
 *     &lt;enumeration value="CanadaSurcharge"/>
 *     &lt;enumeration value="CanadaPolicyChangeFormOPCF"/>
 *     &lt;enumeration value="CanadaPolicyChangeFormSEF"/>
 *     &lt;enumeration value="CanadaPolicyChangeFormNBEF"/>
 *     &lt;enumeration value="CanadaPolicyChangeFormQEF"/>
 *     &lt;enumeration value="TransactionGenerateTaxFeeTransactionCode"/>
 *     &lt;enumeration value="TaxFeeRateTypeCode"/>
 *     &lt;enumeration value="ActiveGovernmentPayableVendors"/>
 *     &lt;enumeration value="TaxableTransactionCode"/>
 *     &lt;enumeration value="ContractNumber"/>
 *     &lt;enumeration value="TransactionCommissionBroker"/>
 *     &lt;enumeration value="AdditionalPartyField"/>
 *     &lt;enumeration value="AdditionalPartyFieldDescription"/>
 *     &lt;enumeration value="CanadaDelivery"/>
 *     &lt;enumeration value="CanadaMerchandiseType"/>
 *     &lt;enumeration value="CanadaFrequency"/>
 *     &lt;enumeration value="CanadaTrailerTractorTraining"/>
 *     &lt;enumeration value="CanadaMachineryEquipment"/>
 *     &lt;enumeration value="CanadaSeasonalUse"/>
 *     &lt;enumeration value="CanadaVehicleSpecialUse"/>
 *     &lt;enumeration value="CanadaFilingType"/>
 *     &lt;enumeration value="CanadaRoadCourse"/>
 *     &lt;enumeration value="CanadaAmbulanceUsage"/>
 *     &lt;enumeration value="CanadaBusType"/>
 *     &lt;enumeration value="CanadaPrivateBusUsage"/>
 *     &lt;enumeration value="InsertedArea"/>
 *     &lt;enumeration value="EntityCodeType"/>
 *     &lt;enumeration value="PaidArea"/>
 *     &lt;enumeration value="ReconciliationPaidStatus"/>
 *     &lt;enumeration value="ReconciliationGeneralLedgerType"/>
 *     &lt;enumeration value="ReconciliationPremiumPayableEntity"/>
 *     &lt;enumeration value="ReconciliationPremiumPayableEntityAgency"/>
 *     &lt;enumeration value="ReconciliationPremiumPayableEntityBranch"/>
 *     &lt;enumeration value="ReconciliationPremiumPayableEntityDepartment"/>
 *     &lt;enumeration value="ReconciliationPremiumPayableEntityProfitCenter"/>
 *     &lt;enumeration value="LineOfBusiness"/>
 *     &lt;enumeration value="TransactionBinderBillStatus"/>
 *     &lt;enumeration value="AgencyDefinedCategoryType"/>
 *     &lt;enumeration value="MarketingSubmissionStatus"/>
 *     &lt;enumeration value="CarrierResponse"/>
 *     &lt;enumeration value="CarrierResponseDescription"/>
 *     &lt;enumeration value="PremiumPayable"/>
 *     &lt;enumeration value="IssuingCompanyByEntityStructure"/>
 *     &lt;enumeration value="LineTypeForTypeOfBusiness"/>
 *     &lt;enumeration value="CarrierLineServiceSummary"/>
 *     &lt;enumeration value="LineCommissionAgreement"/>
 *     &lt;enumeration value="UnderwritingValidOptions"/>
 *     &lt;enumeration value="SoftwareBackupStorageType"/>
 *     &lt;enumeration value="OnPremiseStorageType"/>
 *     &lt;enumeration value="LineTypeForPolicy"/>
 *     &lt;enumeration value="BusinessAutoVehicleBodyType"/>
 *     &lt;enumeration value="VehicleType"/>
 *     &lt;enumeration value="CanadaLicenceClassON"/>
 *     &lt;enumeration value="BrokerGeneralLedgerPayType"/>
 *     &lt;enumeration value="CompanyByIssuingBillingPolicyLine"/>
 *     &lt;enumeration value="OtherDeductible"/>
 *     &lt;enumeration value="PriorCarrierPolicyType"/>
 *     &lt;enumeration value="CommAPAttachmentType"/>
 *     &lt;enumeration value="IdentificationNumberType"/>
 *     &lt;enumeration value="FATCAComplianceStatusCode"/>
 *     &lt;enumeration value="ACORDLineOfBusinessRouting"/>
 *     &lt;enumeration value="CertificatePolicyPrimaryAndNonContributory"/>
 *     &lt;enumeration value="ActiveAmountQualifier"/>
 *     &lt;enumeration value="OpportunityStage"/>
 *     &lt;enumeration value="OpportunityOwnerMemberType"/>
 *     &lt;enumeration value="OpportunityClosedStatus"/>
 *     &lt;enumeration value="OpportunityLostReasons"/>
 *     &lt;enumeration value="ServiceCode"/>
 *     &lt;enumeration value="ServiceTerm"/>
 *     &lt;enumeration value="ServiceType"/>
 *     &lt;enumeration value="CommonProducerBrokerCommissionAccountLookupCode"/>
 *     &lt;enumeration value="CommonProducerBrokerCommissionContract"/>
 *     &lt;enumeration value="EmployeeShareRevenueAgencyFromLine"/>
 *     &lt;enumeration value="EmployeeShareRevenueBranchFromLine"/>
 *     &lt;enumeration value="EmployeeShareRevenueDepartmentFromLine"/>
 *     &lt;enumeration value="EmployeeShareRevenueProfitCenterFromLine"/>
 *     &lt;enumeration value="ConfidentialClientAccessGroup"/>
 *     &lt;enumeration value="ConfidentialClientAccessType"/>
 *     &lt;enumeration value="RequestedDeclined"/>
 *     &lt;enumeration value="WaterHeaterApparatusType"/>
 *     &lt;enumeration value="SumpPumpType"/>
 *     &lt;enumeration value="AuxiliaryPowerType"/>
 *     &lt;enumeration value="ValveType"/>
 *     &lt;enumeration value="MainWaterValveShutOffType"/>
 *     &lt;enumeration value="HydrantType"/>
 *     &lt;enumeration value="Involvement"/>
 *     &lt;enumeration value="PolicyLines"/>
 *     &lt;enumeration value="PlanName"/>
 *     &lt;enumeration value="PlanType"/>
 *     &lt;enumeration value="GroupSize"/>
 *     &lt;enumeration value="WaysOfInsuring"/>
 *     &lt;enumeration value="MarketType"/>
 *     &lt;enumeration value="MarketSize"/>
 *     &lt;enumeration value="PayrollCycle"/>
 *     &lt;enumeration value="LocationType"/>
 *     &lt;enumeration value="EmployeeClass"/>
 *     &lt;enumeration value="EmployeeStatus"/>
 *     &lt;enumeration value="Eligibility"/>
 *     &lt;enumeration value="CobraAdministration"/>
 *     &lt;enumeration value="AvailableEmployeeClass"/>
 *     &lt;enumeration value="Beginning"/>
 *     &lt;enumeration value="WaitingPeriod"/>
 *     &lt;enumeration value="PhysicalDamageReportingPeriod"/>
 *     &lt;enumeration value="InsuranceCompany"/>
 *     &lt;enumeration value="ActivityUpdate"/>
 *     &lt;enumeration value="OpportunityStageGroup"/>
 *     &lt;enumeration value="SalesTargetType"/>
 *     &lt;enumeration value="ReconciliationRecordGetLinesByClientOrPolicyNumber"/>
 *     &lt;enumeration value="DescriptionType"/>
 *     &lt;enumeration value="CompareByType"/>
 *     &lt;enumeration value="TransactionPaymentOption"/>
 *     &lt;enumeration value="AgencyForNonClient"/>
 *     &lt;enumeration value="BranchForNonClient"/>
 *     &lt;enumeration value="StopLossContractType"/>
 *     &lt;enumeration value="StopLossIssuingCompany"/>
 *     &lt;enumeration value="RateTier"/>
 *     &lt;enumeration value="TPAServices"/>
 *     &lt;enumeration value="BenefitTermination"/>
 *     &lt;enumeration value="PolicyLineTaxOption"/>
 *     &lt;enumeration value="TransactionAgingCategory"/>
 *     &lt;enumeration value="ELTOEmployerTypeCode"/>
 *     &lt;enumeration value="Amount"/>
 *     &lt;enumeration value="ARDueDate"/>
 *     &lt;enumeration value="UpdatePolicyBilled"/>
 *     &lt;enumeration value="UpdatePolicyBilledAnnualized"/>
 *     &lt;enumeration value="AccountSource"/>
 *     &lt;enumeration value="CommercialApplicationNameType"/>
 *     &lt;enumeration value="BuildingPropertyDeductibleType"/>
 *     &lt;enumeration value="PersonalPropertyDeductibleType"/>
 *     &lt;enumeration value="UKPaymentType"/>
 *     &lt;enumeration value="DirectDepositMethodForAccount"/>
 *     &lt;enumeration value="DirectDepositMethodForContact"/>
 *     &lt;enumeration value="DirectDeposit"/>
 *     &lt;enumeration value="PaymentMethodCode"/>
 *     &lt;enumeration value="ServiceClassCode"/>
 *     &lt;enumeration value="DirectWithdrawalAuthorizationStatus"/>
 *     &lt;enumeration value="DurationDescription"/>
 *     &lt;enumeration value="DepartmentForNonClient"/>
 *     &lt;enumeration value="ProfitCenterForNonClient"/>
 *     &lt;enumeration value="ClientResponse"/>
 *     &lt;enumeration value="InvoiceGrouping"/>
 *     &lt;enumeration value="APSection"/>
 *     &lt;enumeration value="LossNoticeFilter"/>
 *     &lt;enumeration value="TaxOption"/>
 *     &lt;enumeration value="PolicyLineClaimType"/>
 *     &lt;enumeration value="ActivityEvent"/>
 *     &lt;enumeration value="ActivityCodeByEvent"/>
 *     &lt;enumeration value="ReconciliationBankDetailItemGeneralLedgerAccount"/>
 *     &lt;enumeration value="ReconciliationBankDetailItemAgency"/>
 *     &lt;enumeration value="ReconciliationBankDetailItemBranch"/>
 *     &lt;enumeration value="ReconciliationBankDetailItemDepartment"/>
 *     &lt;enumeration value="ReconciliationBankDetailItemProfitCenter"/>
 *     &lt;enumeration value="ReconciliationBankBankAccount"/>
 *     &lt;enumeration value="RoofMaterial"/>
 *     &lt;enumeration value="GLType"/>
 *     &lt;enumeration value="ChartOfAccountGroup"/>
 *     &lt;enumeration value="ChartOfAccountAvailableStructure"/>
 *     &lt;enumeration value="AgencyForBankAccount"/>
 *     &lt;enumeration value="ServiceClassCodeForBank"/>
 *     &lt;enumeration value="CheckLayout"/>
 *     &lt;enumeration value="TransmissionFileType"/>
 *     &lt;enumeration value="MappingFile"/>
 *     &lt;enumeration value="ContactsSummary"/>
 *     &lt;enumeration value="AttachmentFolderByEntity"/>
 *     &lt;enumeration value="Culture"/>
 *     &lt;enumeration value="PolicyAnnualized"/>
 *     &lt;enumeration value="ServerTimeZone"/>
 *     &lt;enumeration value="RatePerCode"/>
 *     &lt;enumeration value="OpportunityProduct"/>
 *     &lt;enumeration value="EmployeeClassTimeWorkedPer"/>
 *     &lt;enumeration value="AllocationEntriesStructureCombinations"/>
 *     &lt;enumeration value="AllocationStructureGroupingDefaultMethod"/>
 *     &lt;enumeration value="TruckersTypeOfChange"/>
 *     &lt;enumeration value="ViewGovernmentTaxFeeRates"/>
 *     &lt;enumeration value="FormEdition"/>
 *     &lt;enumeration value="ActiveRenewalStages"/>
 *     &lt;enumeration value="IvansProduct"/>
 *     &lt;enumeration value="TransactionBalanceTransferAccount"/>
 *     &lt;enumeration value="TransactionBalanceTransferAgency"/>
 *     &lt;enumeration value="TransactionBalanceTransferBranch"/>
 *     &lt;enumeration value="DiscountTransactionCodes"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LookupTypes", namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_lookup/")
@XmlEnum
public enum LookupTypes {

    @XmlEnumValue("Prefix")
    PREFIX("Prefix"),
    @XmlEnumValue("Suffix")
    SUFFIX("Suffix"),
    @XmlEnumValue("PhoneNumberType")
    PHONE_NUMBER_TYPE("PhoneNumberType"),
    @XmlEnumValue("Agency")
    AGENCY("Agency"),
    @XmlEnumValue("Branch")
    BRANCH("Branch"),
    @XmlEnumValue("ServicingRoles")
    SERVICING_ROLES("ServicingRoles"),
    @XmlEnumValue("EmployeesForServicingRoles")
    EMPLOYEES_FOR_SERVICING_ROLES("EmployeesForServicingRoles"),
    @XmlEnumValue("Broker")
    BROKER("Broker"),
    @XmlEnumValue("ADCCategories")
    ADC_CATEGORIES("ADCCategories"),
    ADC("ADC"),
    @XmlEnumValue("InActivateReason")
    IN_ACTIVATE_REASON("InActivateReason"),
    @XmlEnumValue("InvoicePageBreak")
    INVOICE_PAGE_BREAK("InvoicePageBreak"),
    @XmlEnumValue("InvoiceLayout")
    INVOICE_LAYOUT("InvoiceLayout"),
    @XmlEnumValue("StatementPageBreak")
    STATEMENT_PAGE_BREAK("StatementPageBreak"),
    @XmlEnumValue("StatementLayout")
    STATEMENT_LAYOUT("StatementLayout"),
    @XmlEnumValue("ContactVia")
    CONTACT_VIA("ContactVia"),
    @XmlEnumValue("ContactMethod")
    CONTACT_METHOD("ContactMethod"),
    @XmlEnumValue("DeliveryMethod")
    DELIVERY_METHOD("DeliveryMethod"),
    @XmlEnumValue("MarketingMethod")
    MARKETING_METHOD("MarketingMethod"),
    @XmlEnumValue("CallPermission")
    CALL_PERMISSION("CallPermission"),
    @XmlEnumValue("Language")
    LANGUAGE("Language"),
    @XmlEnumValue("DriverType")
    DRIVER_TYPE("DriverType"),
    @XmlEnumValue("GoodStudent")
    GOOD_STUDENT("GoodStudent"),
    @XmlEnumValue("DriverTraining")
    DRIVER_TRAINING("DriverTraining"),
    @XmlEnumValue("Gender")
    GENDER("Gender"),
    @XmlEnumValue("MaritalStatus")
    MARITAL_STATUS("MaritalStatus"),
    @XmlEnumValue("Occupation")
    OCCUPATION("Occupation"),
    @XmlEnumValue("Classification")
    CLASSIFICATION("Classification"),
    @XmlEnumValue("NAICSCategory")
    NAICS_CATEGORY("NAICSCategory"),
    NAICS("NAICS"),
    @XmlEnumValue("SICCategory")
    SIC_CATEGORY("SICCategory"),
    SIC("SIC"),
    @XmlEnumValue("CreditBureau")
    CREDIT_BUREAU("CreditBureau"),
    @XmlEnumValue("BusinessType")
    BUSINESS_TYPE("BusinessType"),
    @XmlEnumValue("NatureOfBusiness")
    NATURE_OF_BUSINESS("NatureOfBusiness"),
    @XmlEnumValue("ContactDescription")
    CONTACT_DESCRIPTION("ContactDescription"),
    @XmlEnumValue("AddressDescription")
    ADDRESS_DESCRIPTION("AddressDescription"),
    @XmlEnumValue("ActivityCode")
    ACTIVITY_CODE("ActivityCode"),
    @XmlEnumValue("ClosedReason")
    CLOSED_REASON("ClosedReason"),
    @XmlEnumValue("AccessLevel")
    ACCESS_LEVEL("AccessLevel"),
    @XmlEnumValue("TaskStatus")
    TASK_STATUS("TaskStatus"),
    @XmlEnumValue("Priority")
    PRIORITY("Priority"),
    @XmlEnumValue("WhoOwnerCode")
    WHO_OWNER_CODE("WhoOwnerCode"),
    @XmlEnumValue("TaskOwnerType")
    TASK_OWNER_TYPE("TaskOwnerType"),
    @XmlEnumValue("ActivityAssociatedToType")
    ACTIVITY_ASSOCIATED_TO_TYPE("ActivityAssociatedToType"),
    @XmlEnumValue("ClosedStatus")
    CLOSED_STATUS("ClosedStatus"),
    @XmlEnumValue("PolicyType")
    POLICY_TYPE("PolicyType"),
    @XmlEnumValue("LineStatus")
    LINE_STATUS("LineStatus"),
    @XmlEnumValue("Department")
    DEPARTMENT("Department"),
    @XmlEnumValue("StateProvince")
    STATE_PROVINCE("StateProvince"),
    @XmlEnumValue("Country")
    COUNTRY("Country"),
    @XmlEnumValue("IssuingCompany")
    ISSUING_COMPANY("IssuingCompany"),
    @XmlEnumValue("BillingCompany")
    BILLING_COMPANY("BillingCompany"),
    @XmlEnumValue("ProfitCenter")
    PROFIT_CENTER("ProfitCenter"),
    @XmlEnumValue("AgencyCommissionType")
    AGENCY_COMMISSION_TYPE("AgencyCommissionType"),
    @XmlEnumValue("BrokerCommissionType")
    BROKER_COMMISSION_TYPE("BrokerCommissionType"),
    @XmlEnumValue("ProducerCommissionType")
    PRODUCER_COMMISSION_TYPE("ProducerCommissionType"),
    @XmlEnumValue("AgencyForClientID")
    AGENCY_FOR_CLIENT_ID("AgencyForClientID"),
    @XmlEnumValue("BranchForClientID")
    BRANCH_FOR_CLIENT_ID("BranchForClientID"),
    @XmlEnumValue("BillingPlan")
    BILLING_PLAN("BillingPlan"),
    @XmlEnumValue("PolicySource")
    POLICY_SOURCE("PolicySource"),
    @XmlEnumValue("LineType")
    LINE_TYPE("LineType"),
    @XmlEnumValue("CommissionAgreement")
    COMMISSION_AGREEMENT("CommissionAgreement"),
    @XmlEnumValue("ProducerBrokerCode")
    PRODUCER_BROKER_CODE("ProducerBrokerCode"),
    @XmlEnumValue("BrokerCommissionAgreement")
    BROKER_COMMISSION_AGREEMENT("BrokerCommissionAgreement"),
    @XmlEnumValue("ProducerCommissionAgreement")
    PRODUCER_COMMISSION_AGREEMENT("ProducerCommissionAgreement"),
    @XmlEnumValue("TransactionCode")
    TRANSACTION_CODE("TransactionCode"),
    @XmlEnumValue("InvoiceToType")
    INVOICE_TO_TYPE("InvoiceToType"),
    @XmlEnumValue("AccountType")
    ACCOUNT_TYPE("AccountType"),
    @XmlEnumValue("Producer")
    PRODUCER("Producer"),
    @XmlEnumValue("MessageGallery")
    MESSAGE_GALLERY("MessageGallery"),
    @XmlEnumValue("FormAndEndorsementNumber")
    FORM_AND_ENDORSEMENT_NUMBER("FormAndEndorsementNumber"),
    @XmlEnumValue("CopyrightType")
    COPYRIGHT_TYPE("CopyrightType"),
    @XmlEnumValue("BuildingInterest")
    BUILDING_INTEREST("BuildingInterest"),
    @XmlEnumValue("PaymentPlan")
    PAYMENT_PLAN("PaymentPlan"),
    @XmlEnumValue("Audit")
    AUDIT("Audit"),
    @XmlEnumValue("Carrier")
    CARRIER("Carrier"),
    @XmlEnumValue("PersonalApplicationBillingCode")
    PERSONAL_APPLICATION_BILLING_CODE("PersonalApplicationBillingCode"),
    @XmlEnumValue("YesNo")
    YES_NO("YesNo"),
    @XmlEnumValue("PersonalApplicationLossHistoryEnteredBy")
    PERSONAL_APPLICATION_LOSS_HISTORY_ENTERED_BY("PersonalApplicationLossHistoryEnteredBy"),
    @XmlEnumValue("VehicleMakeModel")
    VEHICLE_MAKE_MODEL("VehicleMakeModel"),
    @XmlEnumValue("VehicleBodyType")
    VEHICLE_BODY_TYPE("VehicleBodyType"),
    @XmlEnumValue("PaymentMethod")
    PAYMENT_METHOD("PaymentMethod"),
    @XmlEnumValue("PersonalApplicationMailPolicyTo")
    PERSONAL_APPLICATION_MAIL_POLICY_TO("PersonalApplicationMailPolicyTo"),
    @XmlEnumValue("PayorCode")
    PAYOR_CODE("PayorCode"),
    @XmlEnumValue("EmployeeType")
    EMPLOYEE_TYPE("EmployeeType"),
    @XmlEnumValue("CreditCardCompany")
    CREDIT_CARD_COMPANY("CreditCardCompany"),
    @XmlEnumValue("FinanceCompany")
    FINANCE_COMPANY("FinanceCompany"),
    @XmlEnumValue("RelationToApplicant")
    RELATION_TO_APPLICANT("RelationToApplicant"),
    @XmlEnumValue("SymbolAgeGroup")
    SYMBOL_AGE_GROUP("SymbolAgeGroup"),
    @XmlEnumValue("Territory")
    TERRITORY("Territory"),
    @XmlEnumValue("Usage")
    USAGE("Usage"),
    @XmlEnumValue("Performance")
    PERFORMANCE("Performance"),
    @XmlEnumValue("MultiCarCredit")
    MULTI_CAR_CREDIT("MultiCarCredit"),
    @XmlEnumValue("Garaged")
    GARAGED("Garaged"),
    @XmlEnumValue("AntiTheftDevices")
    ANTI_THEFT_DEVICES("AntiTheftDevices"),
    @XmlEnumValue("Interest")
    INTEREST("Interest"),
    @XmlEnumValue("AccidentTypeCode")
    ACCIDENT_TYPE_CODE("AccidentTypeCode"),
    @XmlEnumValue("DrivingRecordCode")
    DRIVING_RECORD_CODE("DrivingRecordCode"),
    @XmlEnumValue("MilitaryBranch")
    MILITARY_BRANCH("MilitaryBranch"),
    @XmlEnumValue("MedicationTreatment")
    MEDICATION_TREATMENT("MedicationTreatment"),
    @XmlEnumValue("LimitAppliesTo")
    LIMIT_APPLIES_TO("LimitAppliesTo"),
    @XmlEnumValue("PhoneType")
    PHONE_TYPE("PhoneType"),
    @XmlEnumValue("Jurisdiction")
    JURISDICTION("Jurisdiction"),
    @XmlEnumValue("PaymentType")
    PAYMENT_TYPE("PaymentType"),
    @XmlEnumValue("LossType")
    LOSS_TYPE("LossType"),
    @XmlEnumValue("ClaimCode")
    CLAIM_CODE("ClaimCode"),
    @XmlEnumValue("ClaimType")
    CLAIM_TYPE("ClaimType"),
    @XmlEnumValue("AvailableRiskType")
    AVAILABLE_RISK_TYPE("AvailableRiskType"),
    @XmlEnumValue("Risk")
    RISK("Risk"),
    @XmlEnumValue("PayeeType")
    PAYEE_TYPE("PayeeType"),
    @XmlEnumValue("AvailableRiskTypeForCustomForm")
    AVAILABLE_RISK_TYPE_FOR_CUSTOM_FORM("AvailableRiskTypeForCustomForm"),
    @XmlEnumValue("RiskForCustomForm")
    RISK_FOR_CUSTOM_FORM("RiskForCustomForm"),
    @XmlEnumValue("CoverageGeneralAggregateAppliesTo")
    COVERAGE_GENERAL_AGGREGATE_APPLIES_TO("CoverageGeneralAggregateAppliesTo"),
    @XmlEnumValue("CoveragesDeductiblesBasis")
    COVERAGES_DEDUCTIBLES_BASIS("CoveragesDeductiblesBasis"),
    @XmlEnumValue("HazardClass")
    HAZARD_CLASS("HazardClass"),
    @XmlEnumValue("PremiumBasis")
    PREMIUM_BASIS("PremiumBasis"),
    @XmlEnumValue("AdditionalCoverageCode")
    ADDITIONAL_COVERAGE_CODE("AdditionalCoverageCode"),
    @XmlEnumValue("InsuranceRequestedFormNumber")
    INSURANCE_REQUESTED_FORM_NUMBER("InsuranceRequestedFormNumber"),
    @XmlEnumValue("InsuranceRequestedFormType")
    INSURANCE_REQUESTED_FORM_TYPE("InsuranceRequestedFormType"),
    @XmlEnumValue("ResidentialRatingUnderwritingUsage")
    RESIDENTIAL_RATING_UNDERWRITING_USAGE("ResidentialRatingUnderwritingUsage"),
    @XmlEnumValue("Occupancy")
    OCCUPANCY("Occupancy"),
    @XmlEnumValue("Sprinkler")
    SPRINKLER("Sprinkler"),
    @XmlEnumValue("HousekeepingCondition")
    HOUSEKEEPING_CONDITION("HousekeepingCondition"),
    @XmlEnumValue("DistanceToTidalWater")
    DISTANCE_TO_TIDAL_WATER("DistanceToTidalWater"),
    @XmlEnumValue("DoorLock")
    DOOR_LOCK("DoorLock"),
    @XmlEnumValue("CourseOfConstruction")
    COURSE_OF_CONSTRUCTION("CourseOfConstruction"),
    @XmlEnumValue("SmokeProtectionDevice")
    SMOKE_PROTECTION_DEVICE("SmokeProtectionDevice"),
    @XmlEnumValue("TemperatureFireProtectionDevice")
    TEMPERATURE_FIRE_PROTECTION_DEVICE("TemperatureFireProtectionDevice"),
    @XmlEnumValue("BurglarProtectionDevice")
    BURGLAR_PROTECTION_DEVICE("BurglarProtectionDevice"),
    @XmlEnumValue("Wiring")
    WIRING("Wiring"),
    @XmlEnumValue("HeatType")
    HEAT_TYPE("HeatType"),
    @XmlEnumValue("Rating")
    RATING("Rating"),
    @XmlEnumValue("HomeownerDwellingFireFoundation")
    HOMEOWNER_DWELLING_FIRE_FOUNDATION("HomeownerDwellingFireFoundation"),
    @XmlEnumValue("FuelStorageTankLocation")
    FUEL_STORAGE_TANK_LOCATION("FuelStorageTankLocation"),
    @XmlEnumValue("FuelStorageLineLocation")
    FUEL_STORAGE_LINE_LOCATION("FuelStorageLineLocation"),
    @XmlEnumValue("ResidenceType")
    RESIDENCE_TYPE("ResidenceType"),
    @XmlEnumValue("DwellingLocation")
    DWELLING_LOCATION("DwellingLocation"),
    @XmlEnumValue("SwimmingPoolIs")
    SWIMMING_POOL_IS("SwimmingPoolIs"),
    @XmlEnumValue("PlumbingCondition")
    PLUMBING_CONDITION("PlumbingCondition"),
    @XmlEnumValue("RoofCondition")
    ROOF_CONDITION("RoofCondition"),
    @XmlEnumValue("RoofType")
    ROOF_TYPE("RoofType"),
    @XmlEnumValue("WindClass")
    WIND_CLASS("WindClass"),
    @XmlEnumValue("StormShutters")
    STORM_SHUTTERS("StormShutters"),
    @XmlEnumValue("Renovation")
    RENOVATION("Renovation"),
    @XmlEnumValue("AnimalType")
    ANIMAL_TYPE("AnimalType"),
    @XmlEnumValue("AnimalBreed")
    ANIMAL_BREED("AnimalBreed"),
    @XmlEnumValue("MaterialUnattached")
    MATERIAL_UNATTACHED("MaterialUnattached"),
    @XmlEnumValue("EvaluationMethod")
    EVALUATION_METHOD("EvaluationMethod"),
    @XmlEnumValue("EvaluationSystem")
    EVALUATION_SYSTEM("EvaluationSystem"),
    @XmlEnumValue("ResidenceCondition")
    RESIDENCE_CONDITION("ResidenceCondition"),
    @XmlEnumValue("AirConditioning")
    AIR_CONDITIONING("AirConditioning"),
    @XmlEnumValue("Breezeway")
    BREEZEWAY("Breezeway"),
    @XmlEnumValue("Basement")
    BASEMENT("Basement"),
    @XmlEnumValue("PorchType")
    PORCH_TYPE("PorchType"),
    @XmlEnumValue("GarageType")
    GARAGE_TYPE("GarageType"),
    @XmlEnumValue("RecreationalVehicleType")
    RECREATIONAL_VEHICLE_TYPE("RecreationalVehicleType"),
    @XmlEnumValue("Fuel")
    FUEL("Fuel"),
    @XmlEnumValue("StoveType")
    STOVE_TYPE("StoveType"),
    @XmlEnumValue("TestingLaboratoryLabel")
    TESTING_LABORATORY_LABEL("TestingLaboratoryLabel"),
    @XmlEnumValue("UnitType")
    UNIT_TYPE("UnitType"),
    @XmlEnumValue("SolidFuelDeviceConstruction")
    SOLID_FUEL_DEVICE_CONSTRUCTION("SolidFuelDeviceConstruction"),
    @XmlEnumValue("LocationOfSolidFuelDevice")
    LOCATION_OF_SOLID_FUEL_DEVICE("LocationOfSolidFuelDevice"),
    @XmlEnumValue("InstallationInspectedBy")
    INSTALLATION_INSPECTED_BY("InstallationInspectedBy"),
    @XmlEnumValue("HeatingUse")
    HEATING_USE("HeatingUse"),
    @XmlEnumValue("InstallationDoneBy")
    INSTALLATION_DONE_BY("InstallationDoneBy"),
    @XmlEnumValue("ChimneyConstruction")
    CHIMNEY_CONSTRUCTION("ChimneyConstruction"),
    @XmlEnumValue("ChimneyAttached")
    CHIMNEY_ATTACHED("ChimneyAttached"),
    @XmlEnumValue("StovePipeType")
    STOVE_PIPE_TYPE("StovePipeType"),
    @XmlEnumValue("VentStyle")
    VENT_STYLE("VentStyle"),
    @XmlEnumValue("ProtectiveMaterial")
    PROTECTIVE_MATERIAL("ProtectiveMaterial"),
    @XmlEnumValue("FrequencyCleanedInspected")
    FREQUENCY_CLEANED_INSPECTED("FrequencyCleanedInspected"),
    @XmlEnumValue("ResidentialBusinessType")
    RESIDENTIAL_BUSINESS_TYPE("ResidentialBusinessType"),
    @XmlEnumValue("ResidentialTypeOfBusiness")
    RESIDENTIAL_TYPE_OF_BUSINESS("ResidentialTypeOfBusiness"),
    @XmlEnumValue("ResidentialPropertyCoverageValuation")
    RESIDENTIAL_PROPERTY_COVERAGE_VALUATION("ResidentialPropertyCoverageValuation"),
    @XmlEnumValue("DeductibleBasis")
    DEDUCTIBLE_BASIS("DeductibleBasis"),
    @XmlEnumValue("OtherDeductibleType")
    OTHER_DEDUCTIBLE_TYPE("OtherDeductibleType"),
    @XmlEnumValue("TieDown")
    TIE_DOWN("TieDown"),
    @XmlEnumValue("CookingLocation")
    COOKING_LOCATION("CookingLocation"),
    @XmlEnumValue("MobileHomeFoundation")
    MOBILE_HOME_FOUNDATION("MobileHomeFoundation"),
    @XmlEnumValue("StrapsCablesType")
    STRAPS_CABLES_TYPE("StrapsCablesType"),
    @XmlEnumValue("AnchorTypeForTieDowns")
    ANCHOR_TYPE_FOR_TIE_DOWNS("AnchorTypeForTieDowns"),
    @XmlEnumValue("OtherOptionalCoverageCode")
    OTHER_OPTIONAL_COVERAGE_CODE("OtherOptionalCoverageCode"),
    @XmlEnumValue("OtherOptionalCoverageDeductibleType")
    OTHER_OPTIONAL_COVERAGE_DEDUCTIBLE_TYPE("OtherOptionalCoverageDeductibleType"),
    @XmlEnumValue("VehicleRatingFarthestTerminal")
    VEHICLE_RATING_FARTHEST_TERMINAL("VehicleRatingFarthestTerminal"),
    @XmlEnumValue("VehicleRatingDriveToWorkSchool")
    VEHICLE_RATING_DRIVE_TO_WORK_SCHOOL("VehicleRatingDriveToWorkSchool"),
    @XmlEnumValue("VehicleRatingUse")
    VEHICLE_RATING_USE("VehicleRatingUse"),
    @XmlEnumValue("VehicleRatingSpecialUse")
    VEHICLE_RATING_SPECIAL_USE("VehicleRatingSpecialUse"),
    @XmlEnumValue("VehicleRatingPurchased")
    VEHICLE_RATING_PURCHASED("VehicleRatingPurchased"),
    @XmlEnumValue("VehicleRatingLeased")
    VEHICLE_RATING_LEASED("VehicleRatingLeased"),
    @XmlEnumValue("ValuationType")
    VALUATION_TYPE("ValuationType"),
    @XmlEnumValue("ConstructionType")
    CONSTRUCTION_TYPE("ConstructionType"),
    @XmlEnumValue("BurglarAlarm")
    BURGLAR_ALARM("BurglarAlarm"),
    @XmlEnumValue("ExtentOfProtection")
    EXTENT_OF_PROTECTION("ExtentOfProtection"),
    @XmlEnumValue("ClockHourly")
    CLOCK_HOURLY("ClockHourly"),
    @XmlEnumValue("TypeOfFireProtection")
    TYPE_OF_FIRE_PROTECTION("TypeOfFireProtection"),
    @XmlEnumValue("PropertyPremiseCoveragesOptions")
    PROPERTY_PREMISE_COVERAGES_OPTIONS("PropertyPremiseCoveragesOptions"),
    @XmlEnumValue("SubjectOfInsurance")
    SUBJECT_OF_INSURANCE("SubjectOfInsurance"),
    @XmlEnumValue("Valuation")
    VALUATION("Valuation"),
    @XmlEnumValue("CauseOfLoss")
    CAUSE_OF_LOSS("CauseOfLoss"),
    @XmlEnumValue("PropertyBusinessIncomeCoverageTypeOfBusiness")
    PROPERTY_BUSINESS_INCOME_COVERAGE_TYPE_OF_BUSINESS("PropertyBusinessIncomeCoverageTypeOfBusiness"),
    @XmlEnumValue("CoinsurancePercentage")
    COINSURANCE_PERCENTAGE("CoinsurancePercentage"),
    @XmlEnumValue("ApplicableCausesOfLoss")
    APPLICABLE_CAUSES_OF_LOSS("ApplicableCausesOfLoss"),
    @XmlEnumValue("LimitLossPayCode")
    LIMIT_LOSS_PAY_CODE("LimitLossPayCode"),
    @XmlEnumValue("PropertyType")
    PROPERTY_TYPE("PropertyType"),
    @XmlEnumValue("MediaUsed")
    MEDIA_USED("MediaUsed"),
    @XmlEnumValue("UnderlyingInsuranceGeneralLiabilityType")
    UNDERLYING_INSURANCE_GENERAL_LIABILITY_TYPE("UnderlyingInsuranceGeneralLiabilityType"),
    @XmlEnumValue("AlarmType")
    ALARM_TYPE("AlarmType"),
    @XmlEnumValue("AlarmDescription")
    ALARM_DESCRIPTION("AlarmDescription"),
    @XmlEnumValue("SafeVault")
    SAFE_VAULT("SafeVault"),
    @XmlEnumValue("PremisesAlarm")
    PREMISES_ALARM("PremisesAlarm"),
    @XmlEnumValue("Label")
    LABEL("Label"),
    @XmlEnumValue("FrequencyOfDeposits")
    FREQUENCY_OF_DEPOSITS("FrequencyOfDeposits"),
    @XmlEnumValue("DeadboltCylinderDoorLocks")
    DEADBOLT_CYLINDER_DOOR_LOCKS("DeadboltCylinderDoorLocks"),
    @XmlEnumValue("CoveragesPropertyValuation")
    COVERAGES_PROPERTY_VALUATION("CoveragesPropertyValuation"),
    @XmlEnumValue("AdditionalLiabilityCoverageCode")
    ADDITIONAL_LIABILITY_COVERAGE_CODE("AdditionalLiabilityCoverageCode"),
    @XmlEnumValue("OtherAdditionalCoverageCode")
    OTHER_ADDITIONAL_COVERAGE_CODE("OtherAdditionalCoverageCode"),
    @XmlEnumValue("GLCode")
    GL_CODE("GLCode"),
    @XmlEnumValue("InsideCityLimits")
    INSIDE_CITY_LIMITS("InsideCityLimits"),
    @XmlEnumValue("ClassCode")
    CLASS_CODE("ClassCode"),
    @XmlEnumValue("AttachmentFolder")
    ATTACHMENT_FOLDER("AttachmentFolder"),
    @XmlEnumValue("AttachmentAccessLevel")
    ATTACHMENT_ACCESS_LEVEL("AttachmentAccessLevel"),
    @XmlEnumValue("AttachmentAttachTo")
    ATTACHMENT_ATTACH_TO("AttachmentAttachTo"),
    @XmlEnumValue("PolicyTypesOfSameBusinessType")
    POLICY_TYPES_OF_SAME_BUSINESS_TYPE("PolicyTypesOfSameBusinessType"),
    @XmlEnumValue("PremiumPayableType")
    PREMIUM_PAYABLE_TYPE("PremiumPayableType"),
    @XmlEnumValue("Prefill")
    PREFILL("Prefill"),
    @XmlEnumValue("TransactionInvoiceToType")
    TRANSACTION_INVOICE_TO_TYPE("TransactionInvoiceToType"),
    @XmlEnumValue("CustomFormOptions")
    CUSTOM_FORM_OPTIONS("CustomFormOptions"),
    @XmlEnumValue("CancellationReason")
    CANCELLATION_REASON("CancellationReason"),
    @XmlEnumValue("MethodOfCancellationMethod")
    METHOD_OF_CANCELLATION_METHOD("MethodOfCancellationMethod"),
    @XmlEnumValue("VehicleCompOTCSym")
    VEHICLE_COMP_OTC_SYM("VehicleCompOTCSym"),
    @XmlEnumValue("VehicleCollSym")
    VEHICLE_COLL_SYM("VehicleCollSym"),
    @XmlEnumValue("Vendor")
    VENDOR("Vendor"),
    @XmlEnumValue("GeneralLedgerSchedule")
    GENERAL_LEDGER_SCHEDULE("GeneralLedgerSchedule"),
    @XmlEnumValue("GeneralLedgerTransferOfFundsFromBankAccount")
    GENERAL_LEDGER_TRANSFER_OF_FUNDS_FROM_BANK_ACCOUNT("GeneralLedgerTransferOfFundsFromBankAccount"),
    @XmlEnumValue("GeneralLedgerTransferOfFundsToBankAccount")
    GENERAL_LEDGER_TRANSFER_OF_FUNDS_TO_BANK_ACCOUNT("GeneralLedgerTransferOfFundsToBankAccount"),
    @XmlEnumValue("GeneralLedgerTransferOfFundsFromAgency")
    GENERAL_LEDGER_TRANSFER_OF_FUNDS_FROM_AGENCY("GeneralLedgerTransferOfFundsFromAgency"),
    @XmlEnumValue("GeneralLedgerTransferOfFundsToAgency")
    GENERAL_LEDGER_TRANSFER_OF_FUNDS_TO_AGENCY("GeneralLedgerTransferOfFundsToAgency"),
    @XmlEnumValue("GeneralLedgerPayVouchersBankAccount")
    GENERAL_LEDGER_PAY_VOUCHERS_BANK_ACCOUNT("GeneralLedgerPayVouchersBankAccount"),
    @XmlEnumValue("GeneralLedgerPayVouchersAgency")
    GENERAL_LEDGER_PAY_VOUCHERS_AGENCY("GeneralLedgerPayVouchersAgency"),
    @XmlEnumValue("ReceiptBankAccount")
    RECEIPT_BANK_ACCOUNT("ReceiptBankAccount"),
    @XmlEnumValue("ReceiptDetailItemType")
    RECEIPT_DETAIL_ITEM_TYPE("ReceiptDetailItemType"),
    @XmlEnumValue("ReceiptDetailItemAccount")
    RECEIPT_DETAIL_ITEM_ACCOUNT("ReceiptDetailItemAccount"),
    @XmlEnumValue("ReceiptDetailItemMethod")
    RECEIPT_DETAIL_ITEM_METHOD("ReceiptDetailItemMethod"),
    @XmlEnumValue("ReceiptDetailItemApplyTo")
    RECEIPT_DETAIL_ITEM_APPLY_TO("ReceiptDetailItemApplyTo"),
    @XmlEnumValue("ReceiptDetailItemApplyToClient")
    RECEIPT_DETAIL_ITEM_APPLY_TO_CLIENT("ReceiptDetailItemApplyToClient"),
    @XmlEnumValue("ReceiptDetailItemApplyToPolicyClient")
    RECEIPT_DETAIL_ITEM_APPLY_TO_POLICY_CLIENT("ReceiptDetailItemApplyToPolicyClient"),
    @XmlEnumValue("ReceiptDetailItemApplyToPolicyLine")
    RECEIPT_DETAIL_ITEM_APPLY_TO_POLICY_LINE("ReceiptDetailItemApplyToPolicyLine"),
    @XmlEnumValue("ReceiptDetailItemApplyToSelectedItemsAgency")
    RECEIPT_DETAIL_ITEM_APPLY_TO_SELECTED_ITEMS_AGENCY("ReceiptDetailItemApplyToSelectedItemsAgency"),
    @XmlEnumValue("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToAccountStructureAgencyBranch")
    RECEIPT_DETAIL_ITEM_APPLY_TO_SELECTED_ITEMS_APPLY_REMAINING_BALANCE_TO_ACCOUNT_STRUCTURE_AGENCY_BRANCH("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToAccountStructureAgencyBranch"),
    @XmlEnumValue("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyClient")
    RECEIPT_DETAIL_ITEM_APPLY_TO_SELECTED_ITEMS_APPLY_REMAINING_BALANCE_TO_POLICY_CLIENT("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyClient"),
    @XmlEnumValue("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyLine")
    RECEIPT_DETAIL_ITEM_APPLY_TO_SELECTED_ITEMS_APPLY_REMAINING_BALANCE_TO_POLICY_LINE("ReceiptDetailItemApplyToSelectedItemsApplyRemainingBalanceToPolicyLine"),
    @XmlEnumValue("ReceiptDetailItemDefaultGeneralLedgerAccount")
    RECEIPT_DETAIL_ITEM_DEFAULT_GENERAL_LEDGER_ACCOUNT("ReceiptDetailItemDefaultGeneralLedgerAccount"),
    @XmlEnumValue("ReceiptDetailItemGeneralLedgerAccount")
    RECEIPT_DETAIL_ITEM_GENERAL_LEDGER_ACCOUNT("ReceiptDetailItemGeneralLedgerAccount"),
    @XmlEnumValue("ReceiptDetailItemStructureAgency")
    RECEIPT_DETAIL_ITEM_STRUCTURE_AGENCY("ReceiptDetailItemStructureAgency"),
    @XmlEnumValue("ReceiptDetailItemStructureBranch")
    RECEIPT_DETAIL_ITEM_STRUCTURE_BRANCH("ReceiptDetailItemStructureBranch"),
    @XmlEnumValue("ReceiptDetailItemStructureDepartment")
    RECEIPT_DETAIL_ITEM_STRUCTURE_DEPARTMENT("ReceiptDetailItemStructureDepartment"),
    @XmlEnumValue("ReceiptDetailItemStructureProfitCenter")
    RECEIPT_DETAIL_ITEM_STRUCTURE_PROFIT_CENTER("ReceiptDetailItemStructureProfitCenter"),
    @XmlEnumValue("DisbursementBankAccount")
    DISBURSEMENT_BANK_ACCOUNT("DisbursementBankAccount"),
    @XmlEnumValue("DisbursementDefaultEntry")
    DISBURSEMENT_DEFAULT_ENTRY("DisbursementDefaultEntry"),
    @XmlEnumValue("DisbursementPayToTheOrderOfAccountType")
    DISBURSEMENT_PAY_TO_THE_ORDER_OF_ACCOUNT_TYPE("DisbursementPayToTheOrderOfAccountType"),
    @XmlEnumValue("DisbursementPayToTheOrderOfAccount")
    DISBURSEMENT_PAY_TO_THE_ORDER_OF_ACCOUNT("DisbursementPayToTheOrderOfAccount"),
    @XmlEnumValue("DisbursementDetailDefaultEntry")
    DISBURSEMENT_DETAIL_DEFAULT_ENTRY("DisbursementDetailDefaultEntry"),
    @XmlEnumValue("DisbursementDetailItemApplyTo")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO("DisbursementDetailItemApplyTo"),
    @XmlEnumValue("DisbursementDetailItemApplyToBroker")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_BROKER("DisbursementDetailItemApplyToBroker"),
    @XmlEnumValue("DisbursementDetailItemApplyToClient")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_CLIENT("DisbursementDetailItemApplyToClient"),
    @XmlEnumValue("DisbursementDetailItemApplyToFinanceCompany")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_FINANCE_COMPANY("DisbursementDetailItemApplyToFinanceCompany"),
    @XmlEnumValue("DisbursementDetailItemApplyToOtherInterest")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_OTHER_INTEREST("DisbursementDetailItemApplyToOtherInterest"),
    @XmlEnumValue("DisbursementDetailItemApplyToPolicyClient")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_POLICY_CLIENT("DisbursementDetailItemApplyToPolicyClient"),
    @XmlEnumValue("DisbursementDetailItemApplyToPolicyLine")
    DISBURSEMENT_DETAIL_ITEM_APPLY_TO_POLICY_LINE("DisbursementDetailItemApplyToPolicyLine"),
    @XmlEnumValue("DisbursementDetailItemGeneralLedgerAccount")
    DISBURSEMENT_DETAIL_ITEM_GENERAL_LEDGER_ACCOUNT("DisbursementDetailItemGeneralLedgerAccount"),
    @XmlEnumValue("DisbursementDetailItemStructureAgency")
    DISBURSEMENT_DETAIL_ITEM_STRUCTURE_AGENCY("DisbursementDetailItemStructureAgency"),
    @XmlEnumValue("DisbursementDetailItemStructureBranch")
    DISBURSEMENT_DETAIL_ITEM_STRUCTURE_BRANCH("DisbursementDetailItemStructureBranch"),
    @XmlEnumValue("DisbursementDetailItemStructureDepartment")
    DISBURSEMENT_DETAIL_ITEM_STRUCTURE_DEPARTMENT("DisbursementDetailItemStructureDepartment"),
    @XmlEnumValue("DisbursementDetailItemStructureProfitCenter")
    DISBURSEMENT_DETAIL_ITEM_STRUCTURE_PROFIT_CENTER("DisbursementDetailItemStructureProfitCenter"),
    @XmlEnumValue("DisbursementVoidReason")
    DISBURSEMENT_VOID_REASON("DisbursementVoidReason"),
    @XmlEnumValue("VoucherBankAccount")
    VOUCHER_BANK_ACCOUNT("VoucherBankAccount"),
    @XmlEnumValue("VoucherDefaultEntry")
    VOUCHER_DEFAULT_ENTRY("VoucherDefaultEntry"),
    @XmlEnumValue("VoucherPayToTheOrderOfAccountType")
    VOUCHER_PAY_TO_THE_ORDER_OF_ACCOUNT_TYPE("VoucherPayToTheOrderOfAccountType"),
    @XmlEnumValue("VoucherPayToTheOrderOfAccount")
    VOUCHER_PAY_TO_THE_ORDER_OF_ACCOUNT("VoucherPayToTheOrderOfAccount"),
    @XmlEnumValue("VoucherDiscountType")
    VOUCHER_DISCOUNT_TYPE("VoucherDiscountType"),
    @XmlEnumValue("VoucherDetailDefaultEntry")
    VOUCHER_DETAIL_DEFAULT_ENTRY("VoucherDetailDefaultEntry"),
    @XmlEnumValue("VoucherDetailItemApplyTo")
    VOUCHER_DETAIL_ITEM_APPLY_TO("VoucherDetailItemApplyTo"),
    @XmlEnumValue("VoucherDetailItemApplyToBroker")
    VOUCHER_DETAIL_ITEM_APPLY_TO_BROKER("VoucherDetailItemApplyToBroker"),
    @XmlEnumValue("VoucherDetailItemApplyToClient")
    VOUCHER_DETAIL_ITEM_APPLY_TO_CLIENT("VoucherDetailItemApplyToClient"),
    @XmlEnumValue("VoucherDetailItemApplyToFinanceCompany")
    VOUCHER_DETAIL_ITEM_APPLY_TO_FINANCE_COMPANY("VoucherDetailItemApplyToFinanceCompany"),
    @XmlEnumValue("VoucherDetailItemApplyToOtherInterest")
    VOUCHER_DETAIL_ITEM_APPLY_TO_OTHER_INTEREST("VoucherDetailItemApplyToOtherInterest"),
    @XmlEnumValue("VoucherDetailItemApplyToPolicyClient")
    VOUCHER_DETAIL_ITEM_APPLY_TO_POLICY_CLIENT("VoucherDetailItemApplyToPolicyClient"),
    @XmlEnumValue("VoucherDetailItemApplyToPolicyLine")
    VOUCHER_DETAIL_ITEM_APPLY_TO_POLICY_LINE("VoucherDetailItemApplyToPolicyLine"),
    @XmlEnumValue("VoucherDetailItemGeneralLedgerAccount")
    VOUCHER_DETAIL_ITEM_GENERAL_LEDGER_ACCOUNT("VoucherDetailItemGeneralLedgerAccount"),
    @XmlEnumValue("VoucherDetailItemStructureAgency")
    VOUCHER_DETAIL_ITEM_STRUCTURE_AGENCY("VoucherDetailItemStructureAgency"),
    @XmlEnumValue("VoucherDetailItemStructureBranch")
    VOUCHER_DETAIL_ITEM_STRUCTURE_BRANCH("VoucherDetailItemStructureBranch"),
    @XmlEnumValue("VoucherDetailItemStructureDepartment")
    VOUCHER_DETAIL_ITEM_STRUCTURE_DEPARTMENT("VoucherDetailItemStructureDepartment"),
    @XmlEnumValue("VoucherDetailItemStructureProfitCenter")
    VOUCHER_DETAIL_ITEM_STRUCTURE_PROFIT_CENTER("VoucherDetailItemStructureProfitCenter"),
    @XmlEnumValue("VoucherVoidReason")
    VOUCHER_VOID_REASON("VoucherVoidReason"),
    @XmlEnumValue("JournalEntryDefaultEntry")
    JOURNAL_ENTRY_DEFAULT_ENTRY("JournalEntryDefaultEntry"),
    @XmlEnumValue("JournalEntryAssociateToAccountType")
    JOURNAL_ENTRY_ASSOCIATE_TO_ACCOUNT_TYPE("JournalEntryAssociateToAccountType"),
    @XmlEnumValue("JournalEntryAssociateToAccount")
    JOURNAL_ENTRY_ASSOCIATE_TO_ACCOUNT("JournalEntryAssociateToAccount"),
    @XmlEnumValue("JournalEntryAutomaticallyReverseReason")
    JOURNAL_ENTRY_AUTOMATICALLY_REVERSE_REASON("JournalEntryAutomaticallyReverseReason"),
    @XmlEnumValue("JournalEntryVoidReason")
    JOURNAL_ENTRY_VOID_REASON("JournalEntryVoidReason"),
    @XmlEnumValue("JournalEntryDetailDefaultEntry")
    JOURNAL_ENTRY_DETAIL_DEFAULT_ENTRY("JournalEntryDetailDefaultEntry"),
    @XmlEnumValue("JournalEntryDetailItemGeneralLedgerAccount")
    JOURNAL_ENTRY_DETAIL_ITEM_GENERAL_LEDGER_ACCOUNT("JournalEntryDetailItemGeneralLedgerAccount"),
    @XmlEnumValue("JournalEntryDetailItemStructureAgency")
    JOURNAL_ENTRY_DETAIL_ITEM_STRUCTURE_AGENCY("JournalEntryDetailItemStructureAgency"),
    @XmlEnumValue("JournalEntryDetailItemStructureBranch")
    JOURNAL_ENTRY_DETAIL_ITEM_STRUCTURE_BRANCH("JournalEntryDetailItemStructureBranch"),
    @XmlEnumValue("JournalEntryDetailItemStructureDepartment")
    JOURNAL_ENTRY_DETAIL_ITEM_STRUCTURE_DEPARTMENT("JournalEntryDetailItemStructureDepartment"),
    @XmlEnumValue("JournalEntryDetailItemStructureProfitCenter")
    JOURNAL_ENTRY_DETAIL_ITEM_STRUCTURE_PROFIT_CENTER("JournalEntryDetailItemStructureProfitCenter"),
    @XmlEnumValue("VehicleCoverageStacking")
    VEHICLE_COVERAGE_STACKING("VehicleCoverageStacking"),
    @XmlEnumValue("LinePremiumPayableContract")
    LINE_PREMIUM_PAYABLE_CONTRACT("LinePremiumPayableContract"),
    @XmlEnumValue("TransactionPremiumPayableContract")
    TRANSACTION_PREMIUM_PAYABLE_CONTRACT("TransactionPremiumPayableContract"),
    @XmlEnumValue("TransactionPremiumPayable")
    TRANSACTION_PREMIUM_PAYABLE("TransactionPremiumPayable"),
    @XmlEnumValue("MultiCarrierSchedulePremiumPayableContract")
    MULTI_CARRIER_SCHEDULE_PREMIUM_PAYABLE_CONTRACT("MultiCarrierSchedulePremiumPayableContract"),
    @XmlEnumValue("TransactionApplyCreditsToDebitsAgency")
    TRANSACTION_APPLY_CREDITS_TO_DEBITS_AGENCY("TransactionApplyCreditsToDebitsAgency"),
    @XmlEnumValue("TransactionAccountsReceivableWriteOffTransactionCode")
    TRANSACTION_ACCOUNTS_RECEIVABLE_WRITE_OFF_TRANSACTION_CODE("TransactionAccountsReceivableWriteOffTransactionCode"),
    @XmlEnumValue("TransactionFinanceCompany")
    TRANSACTION_FINANCE_COMPANY("TransactionFinanceCompany"),
    @XmlEnumValue("TransactionFinanceDeliveryMethod")
    TRANSACTION_FINANCE_DELIVERY_METHOD("TransactionFinanceDeliveryMethod"),
    @XmlEnumValue("TransactionMove")
    TRANSACTION_MOVE("TransactionMove"),
    @XmlEnumValue("TransactionMoveTo")
    TRANSACTION_MOVE_TO("TransactionMoveTo"),
    @XmlEnumValue("TransactionReverseReason")
    TRANSACTION_REVERSE_REASON("TransactionReverseReason"),
    @XmlEnumValue("TransactionVoidPaymentReason")
    TRANSACTION_VOID_PAYMENT_REASON("TransactionVoidPaymentReason"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionCode")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_CODE("TransactionVoidPaymentNSFTransactionCode"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionAgency")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_AGENCY("TransactionVoidPaymentNSFTransactionAgency"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionBranch")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_BRANCH("TransactionVoidPaymentNSFTransactionBranch"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionDepartment")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_DEPARTMENT("TransactionVoidPaymentNSFTransactionDepartment"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionProfitCenter")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_PROFIT_CENTER("TransactionVoidPaymentNSFTransactionProfitCenter"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionDeliveryMethod")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_DELIVERY_METHOD("TransactionVoidPaymentNSFTransactionDeliveryMethod"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionInvoiceGrouping")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_INVOICE_GROUPING("TransactionVoidPaymentNSFTransactionInvoiceGrouping"),
    @XmlEnumValue("TransactionVoidPaymentNSFTransactionInvoiceGroupingExistingInvoice")
    TRANSACTION_VOID_PAYMENT_NSF_TRANSACTION_INVOICE_GROUPING_EXISTING_INVOICE("TransactionVoidPaymentNSFTransactionInvoiceGroupingExistingInvoice"),
    @XmlEnumValue("ActivityCategory")
    ACTIVITY_CATEGORY("ActivityCategory"),
    @XmlEnumValue("ActivityIssuingCompany")
    ACTIVITY_ISSUING_COMPANY("ActivityIssuingCompany"),
    @XmlEnumValue("ActivityPremiumPayableType")
    ACTIVITY_PREMIUM_PAYABLE_TYPE("ActivityPremiumPayableType"),
    @XmlEnumValue("ActivityPremiumPayable")
    ACTIVITY_PREMIUM_PAYABLE("ActivityPremiumPayable"),
    @XmlEnumValue("SmokeDetectors")
    SMOKE_DETECTORS("SmokeDetectors"),
    @XmlEnumValue("CoverageAppliesTo")
    COVERAGE_APPLIES_TO("CoverageAppliesTo"),
    @XmlEnumValue("TransactionInvoiceGrouping")
    TRANSACTION_INVOICE_GROUPING("TransactionInvoiceGrouping"),
    @XmlEnumValue("TransactionInvoiceGroupingExisitingInvoice")
    TRANSACTION_INVOICE_GROUPING_EXISITING_INVOICE("TransactionInvoiceGroupingExisitingInvoice"),
    @XmlEnumValue("TransactionRevenueDeferralScheduleType")
    TRANSACTION_REVENUE_DEFERRAL_SCHEDULE_TYPE("TransactionRevenueDeferralScheduleType"),
    @XmlEnumValue("BondTerm")
    BOND_TERM("BondTerm"),
    @XmlEnumValue("RenewalMethod")
    RENEWAL_METHOD("RenewalMethod"),
    @XmlEnumValue("CountersigningAgent")
    COUNTERSIGNING_AGENT("CountersigningAgent"),
    @XmlEnumValue("SuretyTermsType")
    SURETY_TERMS_TYPE("SuretyTermsType"),
    @XmlEnumValue("ChargeType")
    CHARGE_TYPE("ChargeType"),
    @XmlEnumValue("Currency")
    CURRENCY("Currency"),
    @XmlEnumValue("ObligeeLookupCode")
    OBLIGEE_LOOKUP_CODE("ObligeeLookupCode"),
    @XmlEnumValue("NameType")
    NAME_TYPE("NameType"),
    @XmlEnumValue("SuretyBillingType")
    SURETY_BILLING_TYPE("SuretyBillingType"),
    @XmlEnumValue("MethodCode")
    METHOD_CODE("MethodCode"),
    @XmlEnumValue("PremiumPaymentSupplementFinanceCompany")
    PREMIUM_PAYMENT_SUPPLEMENT_FINANCE_COMPANY("PremiumPaymentSupplementFinanceCompany"),
    @XmlEnumValue("Company")
    COMPANY("Company"),
    @XmlEnumValue("AccountStatus")
    ACCOUNT_STATUS("AccountStatus"),
    @XmlEnumValue("AccountNonClientAgency")
    ACCOUNT_NON_CLIENT_AGENCY("AccountNonClientAgency"),
    @XmlEnumValue("AccountNonClientBranch")
    ACCOUNT_NON_CLIENT_BRANCH("AccountNonClientBranch"),
    @XmlEnumValue("AccountNonClientDepartment")
    ACCOUNT_NON_CLIENT_DEPARTMENT("AccountNonClientDepartment"),
    @XmlEnumValue("AccountNonClientProfitCenter")
    ACCOUNT_NON_CLIENT_PROFIT_CENTER("AccountNonClientProfitCenter"),
    @XmlEnumValue("AgencyDefinedCategory")
    AGENCY_DEFINED_CATEGORY("AgencyDefinedCategory"),
    @XmlEnumValue("AgencyDefinedCategoryOption")
    AGENCY_DEFINED_CATEGORY_OPTION("AgencyDefinedCategoryOption"),
    @XmlEnumValue("DefinedRelationships")
    DEFINED_RELATIONSHIPS("DefinedRelationships"),
    @XmlEnumValue("BatchPaymentMethod")
    BATCH_PAYMENT_METHOD("BatchPaymentMethod"),
    @XmlEnumValue("ContractType")
    CONTRACT_TYPE("ContractType"),
    @XmlEnumValue("PaymentDueDate")
    PAYMENT_DUE_DATE("PaymentDueDate"),
    @XmlEnumValue("PaymentDueDateAddSubtract")
    PAYMENT_DUE_DATE_ADD_SUBTRACT("PaymentDueDateAddSubtract"),
    @XmlEnumValue("PayableContractStructureAgency")
    PAYABLE_CONTRACT_STRUCTURE_AGENCY("PayableContractStructureAgency"),
    @XmlEnumValue("PayableContractStructureBranch")
    PAYABLE_CONTRACT_STRUCTURE_BRANCH("PayableContractStructureBranch"),
    @XmlEnumValue("AccountPayableSubaccount")
    ACCOUNT_PAYABLE_SUBACCOUNT("AccountPayableSubaccount"),
    @XmlEnumValue("LocateAdditionalPartyBy")
    LOCATE_ADDITIONAL_PARTY_BY("LocateAdditionalPartyBy"),
    @XmlEnumValue("AllServicingRoles")
    ALL_SERVICING_ROLES("AllServicingRoles"),
    @XmlEnumValue("ClaimAdjustorID")
    CLAIM_ADJUSTOR_ID("ClaimAdjustorID"),
    @XmlEnumValue("Statuses")
    STATUSES("Statuses"),
    @XmlEnumValue("TypeOfBusiness")
    TYPE_OF_BUSINESS("TypeOfBusiness"),
    @XmlEnumValue("TermDefault")
    TERM_DEFAULT("TermDefault"),
    @XmlEnumValue("ApplicationDetail")
    APPLICATION_DETAIL("ApplicationDetail"),
    @XmlEnumValue("BillingMode")
    BILLING_MODE("BillingMode"),
    @XmlEnumValue("TransactionClass")
    TRANSACTION_CLASS("TransactionClass"),
    @XmlEnumValue("AvailableDescriptionVariables")
    AVAILABLE_DESCRIPTION_VARIABLES("AvailableDescriptionVariables"),
    @XmlEnumValue("AvailableGeneralLedgerAccounts")
    AVAILABLE_GENERAL_LEDGER_ACCOUNTS("AvailableGeneralLedgerAccounts"),
    @XmlEnumValue("PolicyTypeClassification")
    POLICY_TYPE_CLASSIFICATION("PolicyTypeClassification"),
    @XmlEnumValue("ManualClosedStatus")
    MANUAL_CLOSED_STATUS("ManualClosedStatus"),
    @XmlEnumValue("OutlookIntegration")
    OUTLOOK_INTEGRATION("OutlookIntegration"),
    @XmlEnumValue("ReconciliationMethod")
    RECONCILIATION_METHOD("ReconciliationMethod"),
    @XmlEnumValue("ParentCompany")
    PARENT_COMPANY("ParentCompany"),
    @XmlEnumValue("CertificateType")
    CERTIFICATE_TYPE("CertificateType"),
    @XmlEnumValue("CertificateOtherLineOfBusiness")
    CERTIFICATE_OTHER_LINE_OF_BUSINESS("CertificateOtherLineOfBusiness"),
    @XmlEnumValue("CertificateGeneralLiabilityLineOfBusiness")
    CERTIFICATE_GENERAL_LIABILITY_LINE_OF_BUSINESS("CertificateGeneralLiabilityLineOfBusiness"),
    @XmlEnumValue("CertificateAutomobileLineOfBusiness")
    CERTIFICATE_AUTOMOBILE_LINE_OF_BUSINESS("CertificateAutomobileLineOfBusiness"),
    @XmlEnumValue("CertificateExcessUmbrellaLineOfBusiness")
    CERTIFICATE_EXCESS_UMBRELLA_LINE_OF_BUSINESS("CertificateExcessUmbrellaLineOfBusiness"),
    @XmlEnumValue("CertificateWorkersCompensationEmployersLineOfBusiness")
    CERTIFICATE_WORKERS_COMPENSATION_EMPLOYERS_LINE_OF_BUSINESS("CertificateWorkersCompensationEmployersLineOfBusiness"),
    @XmlEnumValue("CertificatePropertyLineOfBusiness")
    CERTIFICATE_PROPERTY_LINE_OF_BUSINESS("CertificatePropertyLineOfBusiness"),
    @XmlEnumValue("CertificateInlandMarineLineOfBusiness")
    CERTIFICATE_INLAND_MARINE_LINE_OF_BUSINESS("CertificateInlandMarineLineOfBusiness"),
    @XmlEnumValue("CertificateCrimeLineOfBusiness")
    CERTIFICATE_CRIME_LINE_OF_BUSINESS("CertificateCrimeLineOfBusiness"),
    @XmlEnumValue("CertificateBoilerAndMachineryLineOfBusiness")
    CERTIFICATE_BOILER_AND_MACHINERY_LINE_OF_BUSINESS("CertificateBoilerAndMachineryLineOfBusiness"),
    @XmlEnumValue("CertificateCargoLineOfBusiness")
    CERTIFICATE_CARGO_LINE_OF_BUSINESS("CertificateCargoLineOfBusiness"),
    @XmlEnumValue("CertificateTrailerInterchangeLineOfBusiness")
    CERTIFICATE_TRAILER_INTERCHANGE_LINE_OF_BUSINESS("CertificateTrailerInterchangeLineOfBusiness"),
    @XmlEnumValue("CertificateLineServiceSummary")
    CERTIFICATE_LINE_SERVICE_SUMMARY("CertificateLineServiceSummary"),
    @XmlEnumValue("ProofsOfInsuranceDistributeVia")
    PROOFS_OF_INSURANCE_DISTRIBUTE_VIA("ProofsOfInsuranceDistributeVia"),
    @XmlEnumValue("ProofsOfInsuranceInactivationReason")
    PROOFS_OF_INSURANCE_INACTIVATION_REASON("ProofsOfInsuranceInactivationReason"),
    @XmlEnumValue("IncludedExcluded")
    INCLUDED_EXCLUDED("IncludedExcluded"),
    @XmlEnumValue("SubmissionMethod")
    SUBMISSION_METHOD("SubmissionMethod"),
    @XmlEnumValue("InstallmentPremiumPayment")
    INSTALLMENT_PREMIUM_PAYMENT("InstallmentPremiumPayment"),
    @XmlEnumValue("Deposit")
    DEPOSIT("Deposit"),
    @XmlEnumValue("WorkersCompensationEmployersLiability")
    WORKERS_COMPENSATION_EMPLOYERS_LIABILITY("WorkersCompensationEmployersLiability"),
    @XmlEnumValue("PremiumPaymentOption")
    PREMIUM_PAYMENT_OPTION("PremiumPaymentOption"),
    @XmlEnumValue("PortableAccessoryEquipmentType")
    PORTABLE_ACCESSORY_EQUIPMENT_TYPE("PortableAccessoryEquipmentType"),
    @XmlEnumValue("EngineFuelType")
    ENGINE_FUEL_TYPE("EngineFuelType"),
    @XmlEnumValue("HullType")
    HULL_TYPE("HullType"),
    @XmlEnumValue("PowerType")
    POWER_TYPE("PowerType"),
    @XmlEnumValue("HullMaterial")
    HULL_MATERIAL("HullMaterial"),
    @XmlEnumValue("HullDesign")
    HULL_DESIGN("HullDesign"),
    @XmlEnumValue("FuelTank")
    FUEL_TANK("FuelTank"),
    @XmlEnumValue("SparMaterial")
    SPAR_MATERIAL("SparMaterial"),
    @XmlEnumValue("WatersNavigated")
    WATERS_NAVIGATED("WatersNavigated"),
    @XmlEnumValue("Commission")
    COMMISSION("Commission"),
    @XmlEnumValue("CommissionType")
    COMMISSION_TYPE("CommissionType"),
    @XmlEnumValue("CommissionLineOfBusiness")
    COMMISSION_LINE_OF_BUSINESS("CommissionLineOfBusiness"),
    @XmlEnumValue("CommissionCompany")
    COMMISSION_COMPANY("CommissionCompany"),
    @XmlEnumValue("OtherInterestCode")
    OTHER_INTEREST_CODE("OtherInterestCode"),
    @XmlEnumValue("CommissionBranch")
    COMMISSION_BRANCH("CommissionBranch"),
    @XmlEnumValue("CommissionDepartment")
    COMMISSION_DEPARTMENT("CommissionDepartment"),
    @XmlEnumValue("CommissionProfitCenter")
    COMMISSION_PROFIT_CENTER("CommissionProfitCenter"),
    @XmlEnumValue("InstallmentType")
    INSTALLMENT_TYPE("InstallmentType"),
    @XmlEnumValue("InstallmentPlan")
    INSTALLMENT_PLAN("InstallmentPlan"),
    @XmlEnumValue("DistributeVia")
    DISTRIBUTE_VIA("DistributeVia"),
    @XmlEnumValue("CompensationType")
    COMPENSATION_TYPE("CompensationType"),
    @XmlEnumValue("EmployeeAuthorizedSignatureUserSecurityGroup")
    EMPLOYEE_AUTHORIZED_SIGNATURE_USER_SECURITY_GROUP("EmployeeAuthorizedSignatureUserSecurityGroup"),
    @XmlEnumValue("EmployeeAuthorizedSignatureUserIndividualEmployee")
    EMPLOYEE_AUTHORIZED_SIGNATURE_USER_INDIVIDUAL_EMPLOYEE("EmployeeAuthorizedSignatureUserIndividualEmployee"),
    @XmlEnumValue("EmployeeShareRevenuePrimaryOfficeAgency")
    EMPLOYEE_SHARE_REVENUE_PRIMARY_OFFICE_AGENCY("EmployeeShareRevenuePrimaryOfficeAgency"),
    @XmlEnumValue("EmployeeShareRevenuePrimaryOfficeBranch")
    EMPLOYEE_SHARE_REVENUE_PRIMARY_OFFICE_BRANCH("EmployeeShareRevenuePrimaryOfficeBranch"),
    @XmlEnumValue("EmployeeShareRevenuePrimaryOfficeDepartment")
    EMPLOYEE_SHARE_REVENUE_PRIMARY_OFFICE_DEPARTMENT("EmployeeShareRevenuePrimaryOfficeDepartment"),
    @XmlEnumValue("EmployeeShareRevenuePrimaryOfficeProfitCenter")
    EMPLOYEE_SHARE_REVENUE_PRIMARY_OFFICE_PROFIT_CENTER("EmployeeShareRevenuePrimaryOfficeProfitCenter"),
    @XmlEnumValue("EmployeeShareRevenueStructureRegion")
    EMPLOYEE_SHARE_REVENUE_STRUCTURE_REGION("EmployeeShareRevenueStructureRegion"),
    @XmlEnumValue("EmployeeShareRevenueStructureAgency")
    EMPLOYEE_SHARE_REVENUE_STRUCTURE_AGENCY("EmployeeShareRevenueStructureAgency"),
    @XmlEnumValue("EmployeeShareRevenueStructureBranch")
    EMPLOYEE_SHARE_REVENUE_STRUCTURE_BRANCH("EmployeeShareRevenueStructureBranch"),
    @XmlEnumValue("EmployeeShareRevenueStructureDepartment")
    EMPLOYEE_SHARE_REVENUE_STRUCTURE_DEPARTMENT("EmployeeShareRevenueStructureDepartment"),
    @XmlEnumValue("EmployeeShareRevenueStructureProfitCenter")
    EMPLOYEE_SHARE_REVENUE_STRUCTURE_PROFIT_CENTER("EmployeeShareRevenueStructureProfitCenter"),
    @XmlEnumValue("TransactionCommissionProducer")
    TRANSACTION_COMMISSION_PRODUCER("TransactionCommissionProducer"),
    @XmlEnumValue("TransactionProducerBrokerCommissionContract")
    TRANSACTION_PRODUCER_BROKER_COMMISSION_CONTRACT("TransactionProducerBrokerCommissionContract"),
    @XmlEnumValue("LineProducerBrokerCommissionContract")
    LINE_PRODUCER_BROKER_COMMISSION_CONTRACT("LineProducerBrokerCommissionContract"),
    @XmlEnumValue("BrokerReceivableCommissionType")
    BROKER_RECEIVABLE_COMMISSION_TYPE("BrokerReceivableCommissionType"),
    @XmlEnumValue("ShareRevenueIsEnabledSystemSetting")
    SHARE_REVENUE_IS_ENABLED_SYSTEM_SETTING("ShareRevenueIsEnabledSystemSetting"),
    @XmlEnumValue("ShareRevenueProducer")
    SHARE_REVENUE_PRODUCER("ShareRevenueProducer"),
    @XmlEnumValue("EmployeeRevenueAgency")
    EMPLOYEE_REVENUE_AGENCY("EmployeeRevenueAgency"),
    @XmlEnumValue("EmployeeRevenueBranch")
    EMPLOYEE_REVENUE_BRANCH("EmployeeRevenueBranch"),
    @XmlEnumValue("EmployeeRevenueDepartment")
    EMPLOYEE_REVENUE_DEPARTMENT("EmployeeRevenueDepartment"),
    @XmlEnumValue("EmployeeRevenueProfitCenter")
    EMPLOYEE_REVENUE_PROFIT_CENTER("EmployeeRevenueProfitCenter"),
    @XmlEnumValue("AvailableSupplementalScreens")
    AVAILABLE_SUPPLEMENTAL_SCREENS("AvailableSupplementalScreens"),
    @XmlEnumValue("EquipmentFloaterCoverageDeductibleType")
    EQUIPMENT_FLOATER_COVERAGE_DEDUCTIBLE_TYPE("EquipmentFloaterCoverageDeductibleType"),
    @XmlEnumValue("GarageAndDealersPremises")
    GARAGE_AND_DEALERS_PREMISES("GarageAndDealersPremises"),
    @XmlEnumValue("GarageAndDealersLocationNumber")
    GARAGE_AND_DEALERS_LOCATION_NUMBER("GarageAndDealersLocationNumber"),
    @XmlEnumValue("PersonalUmbrellaAvailablePolicyLines")
    PERSONAL_UMBRELLA_AVAILABLE_POLICY_LINES("PersonalUmbrellaAvailablePolicyLines"),
    @XmlEnumValue("ActiveLanguages")
    ACTIVE_LANGUAGES("ActiveLanguages"),
    @XmlEnumValue("ReceiptDetailItemPayablesCommissionsItemsAccountLocateBy")
    RECEIPT_DETAIL_ITEM_PAYABLES_COMMISSIONS_ITEMS_ACCOUNT_LOCATE_BY("ReceiptDetailItemPayablesCommissionsItemsAccountLocateBy"),
    @XmlEnumValue("ReceiptDetailItemPayablesCommissionsItemsToPay")
    RECEIPT_DETAIL_ITEM_PAYABLES_COMMISSIONS_ITEMS_TO_PAY("ReceiptDetailItemPayablesCommissionsItemsToPay"),
    @XmlEnumValue("ReceiptDetailItemPayablesCommissionsAvailableStatements")
    RECEIPT_DETAIL_ITEM_PAYABLES_COMMISSIONS_AVAILABLE_STATEMENTS("ReceiptDetailItemPayablesCommissionsAvailableStatements"),
    @XmlEnumValue("ReceiptDetailItemPayablesCommissionsPayableType")
    RECEIPT_DETAIL_ITEM_PAYABLES_COMMISSIONS_PAYABLE_TYPE("ReceiptDetailItemPayablesCommissionsPayableType"),
    @XmlEnumValue("ReceiptDetailItemPayablesCommissionsCashOnAccountApplyTo")
    RECEIPT_DETAIL_ITEM_PAYABLES_COMMISSIONS_CASH_ON_ACCOUNT_APPLY_TO("ReceiptDetailItemPayablesCommissionsCashOnAccountApplyTo"),
    @XmlEnumValue("PrimaryCountry")
    PRIMARY_COUNTRY("PrimaryCountry"),
    @XmlEnumValue("CountryOfOperation")
    COUNTRY_OF_OPERATION("CountryOfOperation"),
    @XmlEnumValue("InternationalDialingCodes")
    INTERNATIONAL_DIALING_CODES("InternationalDialingCodes"),
    @XmlEnumValue("CanadaBillingType")
    CANADA_BILLING_TYPE("CanadaBillingType"),
    @XmlEnumValue("CanadaWhoSendsDocuments")
    CANADA_WHO_SENDS_DOCUMENTS("CanadaWhoSendsDocuments"),
    @XmlEnumValue("RefusalType")
    REFUSAL_TYPE("RefusalType"),
    @XmlEnumValue("RefusalReason")
    REFUSAL_REASON("RefusalReason"),
    @XmlEnumValue("LineOfBusinessType")
    LINE_OF_BUSINESS_TYPE("LineOfBusinessType"),
    @XmlEnumValue("CanadaPhoneType")
    CANADA_PHONE_TYPE("CanadaPhoneType"),
    @XmlEnumValue("LegalEntity")
    LEGAL_ENTITY("LegalEntity"),
    @XmlEnumValue("CanadaCauseOfLoss")
    CANADA_CAUSE_OF_LOSS("CanadaCauseOfLoss"),
    @XmlEnumValue("CanadaInterestType")
    CANADA_INTEREST_TYPE("CanadaInterestType"),
    @XmlEnumValue("Attachment")
    ATTACHMENT("Attachment"),
    @XmlEnumValue("LocationAccess")
    LOCATION_ACCESS("LocationAccess"),
    @XmlEnumValue("EvaluatorProduct")
    EVALUATOR_PRODUCT("EvaluatorProduct"),
    @XmlEnumValue("NumberOfStoreys")
    NUMBER_OF_STOREYS("NumberOfStoreys"),
    @XmlEnumValue("CanadaRelationToApplicant")
    CANADA_RELATION_TO_APPLICANT("CanadaRelationToApplicant"),
    @XmlEnumValue("InspectionSourceReport")
    INSPECTION_SOURCE_REPORT("InspectionSourceReport"),
    @XmlEnumValue("CanadaOccupancy")
    CANADA_OCCUPANCY("CanadaOccupancy"),
    @XmlEnumValue("CanadaStructureType")
    CANADA_STRUCTURE_TYPE("CanadaStructureType"),
    @XmlEnumValue("FoundationType")
    FOUNDATION_TYPE("FoundationType"),
    @XmlEnumValue("CanadaConstruction")
    CANADA_CONSTRUCTION("CanadaConstruction"),
    @XmlEnumValue("CanadaSwimmingPoolType")
    CANADA_SWIMMING_POOL_TYPE("CanadaSwimmingPoolType"),
    @XmlEnumValue("GarageTypeCode")
    GARAGE_TYPE_CODE("GarageTypeCode"),
    @XmlEnumValue("HeatingApparatus")
    HEATING_APPARATUS("HeatingApparatus"),
    @XmlEnumValue("CanadaFuelType")
    CANADA_FUEL_TYPE("CanadaFuelType"),
    @XmlEnumValue("HeatingLocation")
    HEATING_LOCATION("HeatingLocation"),
    @XmlEnumValue("OilTankLocation")
    OIL_TANK_LOCATION("OilTankLocation"),
    @XmlEnumValue("MetersToHydrant")
    METERS_TO_HYDRANT("MetersToHydrant"),
    @XmlEnumValue("KilometersToFirehall")
    KILOMETERS_TO_FIREHALL("KilometersToFirehall"),
    @XmlEnumValue("DetectorType")
    DETECTOR_TYPE("DetectorType"),
    @XmlEnumValue("SecurityType")
    SECURITY_TYPE("SecurityType"),
    @XmlEnumValue("CanadaRoofingType")
    CANADA_ROOFING_TYPE("CanadaRoofingType"),
    @XmlEnumValue("Amps")
    AMPS("Amps"),
    @XmlEnumValue("CanadaWiring")
    CANADA_WIRING("CanadaWiring"),
    @XmlEnumValue("Service")
    SERVICE("Service"),
    @XmlEnumValue("InteriorWallConstructionType")
    INTERIOR_WALL_CONSTRUCTION_TYPE("InteriorWallConstructionType"),
    @XmlEnumValue("AdditionalInteriorQuality")
    ADDITIONAL_INTERIOR_QUALITY("AdditionalInteriorQuality"),
    @XmlEnumValue("DeductibleType")
    DEDUCTIBLE_TYPE("DeductibleType"),
    @XmlEnumValue("DeductibleType1")
    DEDUCTIBLE_TYPE_1("DeductibleType1"),
    @XmlEnumValue("DeductibleType2")
    DEDUCTIBLE_TYPE_2("DeductibleType2"),
    @XmlEnumValue("DeductibleType3")
    DEDUCTIBLE_TYPE_3("DeductibleType3"),
    @XmlEnumValue("DeductibleType4")
    DEDUCTIBLE_TYPE_4("DeductibleType4"),
    @XmlEnumValue("Discount")
    DISCOUNT("Discount"),
    @XmlEnumValue("PackageForm")
    PACKAGE_FORM("PackageForm"),
    @XmlEnumValue("RatingPlan")
    RATING_PLAN("RatingPlan"),
    @XmlEnumValue("CoveragesDeductibleType")
    COVERAGES_DEDUCTIBLE_TYPE("CoveragesDeductibleType"),
    @XmlEnumValue("LiabilityExtension")
    LIABILITY_EXTENSION("LiabilityExtension"),
    @XmlEnumValue("DetachedOutbuildingStructureType")
    DETACHED_OUTBUILDING_STRUCTURE_TYPE("DetachedOutbuildingStructureType"),
    @XmlEnumValue("WatercraftClass")
    WATERCRAFT_CLASS("WatercraftClass"),
    @XmlEnumValue("WatercraftStyle")
    WATERCRAFT_STYLE("WatercraftStyle"),
    @XmlEnumValue("WatercraftType")
    WATERCRAFT_TYPE("WatercraftType"),
    @XmlEnumValue("Watercraftconstruction")
    WATERCRAFTCONSTRUCTION("Watercraftconstruction"),
    @XmlEnumValue("WatercraftUse")
    WATERCRAFT_USE("WatercraftUse"),
    @XmlEnumValue("WatercraftNavigated")
    WATERCRAFT_NAVIGATED("WatercraftNavigated"),
    @XmlEnumValue("CoverageDiscountType")
    COVERAGE_DISCOUNT_TYPE("CoverageDiscountType"),
    @XmlEnumValue("DiscountType")
    DISCOUNT_TYPE("DiscountType"),
    @XmlEnumValue("Mooring")
    MOORING("Mooring"),
    @XmlEnumValue("PropertyCoverage")
    PROPERTY_COVERAGE("PropertyCoverage"),
    @XmlEnumValue("PropertyClass")
    PROPERTY_CLASS("PropertyClass"),
    @XmlEnumValue("CertificationLabel")
    CERTIFICATION_LABEL("CertificationLabel"),
    @XmlEnumValue("FuelHeatingLocation")
    FUEL_HEATING_LOCATION("FuelHeatingLocation"),
    @XmlEnumValue("HeatingType")
    HEATING_TYPE("HeatingType"),
    @XmlEnumValue("ChimneyType")
    CHIMNEY_TYPE("ChimneyType"),
    @XmlEnumValue("LiningType")
    LINING_TYPE("LiningType"),
    @XmlEnumValue("CleaningByWhom")
    CLEANING_BY_WHOM("CleaningByWhom"),
    @XmlEnumValue("ChimneyLocation")
    CHIMNEY_LOCATION("ChimneyLocation"),
    @XmlEnumValue("ChimneyBuiltFrom")
    CHIMNEY_BUILT_FROM("ChimneyBuiltFrom"),
    @XmlEnumValue("Construction")
    CONSTRUCTION("Construction"),
    @XmlEnumValue("StovePipeConstruction")
    STOVE_PIPE_CONSTRUCTION("StovePipeConstruction"),
    @XmlEnumValue("WatercraftTrailerStorage")
    WATERCRAFT_TRAILER_STORAGE("WatercraftTrailerStorage"),
    @XmlEnumValue("CanadaTypeOfCharges")
    CANADA_TYPE_OF_CHARGES("CanadaTypeOfCharges"),
    @XmlEnumValue("CanadaCreditCardCompany")
    CANADA_CREDIT_CARD_COMPANY("CanadaCreditCardCompany"),
    @XmlEnumValue("CanadaPaymentPlan")
    CANADA_PAYMENT_PLAN("CanadaPaymentPlan"),
    @XmlEnumValue("CanadaPremiumAppearsCompany")
    CANADA_PREMIUM_APPEARS_COMPANY("CanadaPremiumAppearsCompany"),
    @XmlEnumValue("CanadaDriverConviction")
    CANADA_DRIVER_CONVICTION("CanadaDriverConviction"),
    @XmlEnumValue("CanadaDriverLicenseCancellationReason")
    CANADA_DRIVER_LICENSE_CANCELLATION_REASON("CanadaDriverLicenseCancellationReason"),
    @XmlEnumValue("CanadaClaimPaidBack")
    CANADA_CLAIM_PAID_BACK("CanadaClaimPaidBack"),
    @XmlEnumValue("CanadaClaimPaidUnder")
    CANADA_CLAIM_PAID_UNDER("CanadaClaimPaidUnder"),
    @XmlEnumValue("CanadaVehicleFuelType")
    CANADA_VEHICLE_FUEL_TYPE("CanadaVehicleFuelType"),
    @XmlEnumValue("CanadaVehicleUsage")
    CANADA_VEHICLE_USAGE("CanadaVehicleUsage"),
    @XmlEnumValue("CanadaVehicleBodyType")
    CANADA_VEHICLE_BODY_TYPE("CanadaVehicleBodyType"),
    @XmlEnumValue("CanadaNewUsed")
    CANADA_NEW_USED("CanadaNewUsed"),
    @XmlEnumValue("CanadaFacilityTiers")
    CANADA_FACILITY_TIERS("CanadaFacilityTiers"),
    @XmlEnumValue("MotorVehicleLiabilityInsuranceCardIssued")
    MOTOR_VEHICLE_LIABILITY_INSURANCE_CARD_ISSUED("MotorVehicleLiabilityInsuranceCardIssued"),
    @XmlEnumValue("SignedBy")
    SIGNED_BY("SignedBy"),
    @XmlEnumValue("CanadaDriverGender")
    CANADA_DRIVER_GENDER("CanadaDriverGender"),
    @XmlEnumValue("CanadaDriverMaritalStatus")
    CANADA_DRIVER_MARITAL_STATUS("CanadaDriverMaritalStatus"),
    @XmlEnumValue("CanadaDriverTraining")
    CANADA_DRIVER_TRAINING("CanadaDriverTraining"),
    @XmlEnumValue("CanadaCarPoolFrequency")
    CANADA_CAR_POOL_FREQUENCY("CanadaCarPoolFrequency"),
    @XmlEnumValue("CanadaSecurityDeviceType")
    CANADA_SECURITY_DEVICE_TYPE("CanadaSecurityDeviceType"),
    @XmlEnumValue("CanadaSecurityProductCode")
    CANADA_SECURITY_PRODUCT_CODE("CanadaSecurityProductCode"),
    @XmlEnumValue("CanadaSecurityCharacteristics")
    CANADA_SECURITY_CHARACTERISTICS("CanadaSecurityCharacteristics"),
    @XmlEnumValue("CanadaVolatileToxicMaterialType")
    CANADA_VOLATILE_TOXIC_MATERIAL_TYPE("CanadaVolatileToxicMaterialType"),
    @XmlEnumValue("CanadaDeliveryType")
    CANADA_DELIVERY_TYPE("CanadaDeliveryType"),
    @XmlEnumValue("CanadaDeliveryFrequency")
    CANADA_DELIVERY_FREQUENCY("CanadaDeliveryFrequency"),
    @XmlEnumValue("CanadaVehicleIncomeReplacement")
    CANADA_VEHICLE_INCOME_REPLACEMENT("CanadaVehicleIncomeReplacement"),
    @XmlEnumValue("CanadaDiscount")
    CANADA_DISCOUNT("CanadaDiscount"),
    @XmlEnumValue("CanadaSurcharge")
    CANADA_SURCHARGE("CanadaSurcharge"),
    @XmlEnumValue("CanadaPolicyChangeFormOPCF")
    CANADA_POLICY_CHANGE_FORM_OPCF("CanadaPolicyChangeFormOPCF"),
    @XmlEnumValue("CanadaPolicyChangeFormSEF")
    CANADA_POLICY_CHANGE_FORM_SEF("CanadaPolicyChangeFormSEF"),
    @XmlEnumValue("CanadaPolicyChangeFormNBEF")
    CANADA_POLICY_CHANGE_FORM_NBEF("CanadaPolicyChangeFormNBEF"),
    @XmlEnumValue("CanadaPolicyChangeFormQEF")
    CANADA_POLICY_CHANGE_FORM_QEF("CanadaPolicyChangeFormQEF"),
    @XmlEnumValue("TransactionGenerateTaxFeeTransactionCode")
    TRANSACTION_GENERATE_TAX_FEE_TRANSACTION_CODE("TransactionGenerateTaxFeeTransactionCode"),
    @XmlEnumValue("TaxFeeRateTypeCode")
    TAX_FEE_RATE_TYPE_CODE("TaxFeeRateTypeCode"),
    @XmlEnumValue("ActiveGovernmentPayableVendors")
    ACTIVE_GOVERNMENT_PAYABLE_VENDORS("ActiveGovernmentPayableVendors"),
    @XmlEnumValue("TaxableTransactionCode")
    TAXABLE_TRANSACTION_CODE("TaxableTransactionCode"),
    @XmlEnumValue("ContractNumber")
    CONTRACT_NUMBER("ContractNumber"),
    @XmlEnumValue("TransactionCommissionBroker")
    TRANSACTION_COMMISSION_BROKER("TransactionCommissionBroker"),
    @XmlEnumValue("AdditionalPartyField")
    ADDITIONAL_PARTY_FIELD("AdditionalPartyField"),
    @XmlEnumValue("AdditionalPartyFieldDescription")
    ADDITIONAL_PARTY_FIELD_DESCRIPTION("AdditionalPartyFieldDescription"),
    @XmlEnumValue("CanadaDelivery")
    CANADA_DELIVERY("CanadaDelivery"),
    @XmlEnumValue("CanadaMerchandiseType")
    CANADA_MERCHANDISE_TYPE("CanadaMerchandiseType"),
    @XmlEnumValue("CanadaFrequency")
    CANADA_FREQUENCY("CanadaFrequency"),
    @XmlEnumValue("CanadaTrailerTractorTraining")
    CANADA_TRAILER_TRACTOR_TRAINING("CanadaTrailerTractorTraining"),
    @XmlEnumValue("CanadaMachineryEquipment")
    CANADA_MACHINERY_EQUIPMENT("CanadaMachineryEquipment"),
    @XmlEnumValue("CanadaSeasonalUse")
    CANADA_SEASONAL_USE("CanadaSeasonalUse"),
    @XmlEnumValue("CanadaVehicleSpecialUse")
    CANADA_VEHICLE_SPECIAL_USE("CanadaVehicleSpecialUse"),
    @XmlEnumValue("CanadaFilingType")
    CANADA_FILING_TYPE("CanadaFilingType"),
    @XmlEnumValue("CanadaRoadCourse")
    CANADA_ROAD_COURSE("CanadaRoadCourse"),
    @XmlEnumValue("CanadaAmbulanceUsage")
    CANADA_AMBULANCE_USAGE("CanadaAmbulanceUsage"),
    @XmlEnumValue("CanadaBusType")
    CANADA_BUS_TYPE("CanadaBusType"),
    @XmlEnumValue("CanadaPrivateBusUsage")
    CANADA_PRIVATE_BUS_USAGE("CanadaPrivateBusUsage"),
    @XmlEnumValue("InsertedArea")
    INSERTED_AREA("InsertedArea"),
    @XmlEnumValue("EntityCodeType")
    ENTITY_CODE_TYPE("EntityCodeType"),
    @XmlEnumValue("PaidArea")
    PAID_AREA("PaidArea"),
    @XmlEnumValue("ReconciliationPaidStatus")
    RECONCILIATION_PAID_STATUS("ReconciliationPaidStatus"),
    @XmlEnumValue("ReconciliationGeneralLedgerType")
    RECONCILIATION_GENERAL_LEDGER_TYPE("ReconciliationGeneralLedgerType"),
    @XmlEnumValue("ReconciliationPremiumPayableEntity")
    RECONCILIATION_PREMIUM_PAYABLE_ENTITY("ReconciliationPremiumPayableEntity"),
    @XmlEnumValue("ReconciliationPremiumPayableEntityAgency")
    RECONCILIATION_PREMIUM_PAYABLE_ENTITY_AGENCY("ReconciliationPremiumPayableEntityAgency"),
    @XmlEnumValue("ReconciliationPremiumPayableEntityBranch")
    RECONCILIATION_PREMIUM_PAYABLE_ENTITY_BRANCH("ReconciliationPremiumPayableEntityBranch"),
    @XmlEnumValue("ReconciliationPremiumPayableEntityDepartment")
    RECONCILIATION_PREMIUM_PAYABLE_ENTITY_DEPARTMENT("ReconciliationPremiumPayableEntityDepartment"),
    @XmlEnumValue("ReconciliationPremiumPayableEntityProfitCenter")
    RECONCILIATION_PREMIUM_PAYABLE_ENTITY_PROFIT_CENTER("ReconciliationPremiumPayableEntityProfitCenter"),
    @XmlEnumValue("LineOfBusiness")
    LINE_OF_BUSINESS("LineOfBusiness"),
    @XmlEnumValue("TransactionBinderBillStatus")
    TRANSACTION_BINDER_BILL_STATUS("TransactionBinderBillStatus"),
    @XmlEnumValue("AgencyDefinedCategoryType")
    AGENCY_DEFINED_CATEGORY_TYPE("AgencyDefinedCategoryType"),
    @XmlEnumValue("MarketingSubmissionStatus")
    MARKETING_SUBMISSION_STATUS("MarketingSubmissionStatus"),
    @XmlEnumValue("CarrierResponse")
    CARRIER_RESPONSE("CarrierResponse"),
    @XmlEnumValue("CarrierResponseDescription")
    CARRIER_RESPONSE_DESCRIPTION("CarrierResponseDescription"),
    @XmlEnumValue("PremiumPayable")
    PREMIUM_PAYABLE("PremiumPayable"),
    @XmlEnumValue("IssuingCompanyByEntityStructure")
    ISSUING_COMPANY_BY_ENTITY_STRUCTURE("IssuingCompanyByEntityStructure"),
    @XmlEnumValue("LineTypeForTypeOfBusiness")
    LINE_TYPE_FOR_TYPE_OF_BUSINESS("LineTypeForTypeOfBusiness"),
    @XmlEnumValue("CarrierLineServiceSummary")
    CARRIER_LINE_SERVICE_SUMMARY("CarrierLineServiceSummary"),
    @XmlEnumValue("LineCommissionAgreement")
    LINE_COMMISSION_AGREEMENT("LineCommissionAgreement"),
    @XmlEnumValue("UnderwritingValidOptions")
    UNDERWRITING_VALID_OPTIONS("UnderwritingValidOptions"),
    @XmlEnumValue("SoftwareBackupStorageType")
    SOFTWARE_BACKUP_STORAGE_TYPE("SoftwareBackupStorageType"),
    @XmlEnumValue("OnPremiseStorageType")
    ON_PREMISE_STORAGE_TYPE("OnPremiseStorageType"),
    @XmlEnumValue("LineTypeForPolicy")
    LINE_TYPE_FOR_POLICY("LineTypeForPolicy"),
    @XmlEnumValue("BusinessAutoVehicleBodyType")
    BUSINESS_AUTO_VEHICLE_BODY_TYPE("BusinessAutoVehicleBodyType"),
    @XmlEnumValue("VehicleType")
    VEHICLE_TYPE("VehicleType"),
    @XmlEnumValue("CanadaLicenceClassON")
    CANADA_LICENCE_CLASS_ON("CanadaLicenceClassON"),
    @XmlEnumValue("BrokerGeneralLedgerPayType")
    BROKER_GENERAL_LEDGER_PAY_TYPE("BrokerGeneralLedgerPayType"),
    @XmlEnumValue("CompanyByIssuingBillingPolicyLine")
    COMPANY_BY_ISSUING_BILLING_POLICY_LINE("CompanyByIssuingBillingPolicyLine"),
    @XmlEnumValue("OtherDeductible")
    OTHER_DEDUCTIBLE("OtherDeductible"),
    @XmlEnumValue("PriorCarrierPolicyType")
    PRIOR_CARRIER_POLICY_TYPE("PriorCarrierPolicyType"),
    @XmlEnumValue("CommAPAttachmentType")
    COMM_AP_ATTACHMENT_TYPE("CommAPAttachmentType"),
    @XmlEnumValue("IdentificationNumberType")
    IDENTIFICATION_NUMBER_TYPE("IdentificationNumberType"),
    @XmlEnumValue("FATCAComplianceStatusCode")
    FATCA_COMPLIANCE_STATUS_CODE("FATCAComplianceStatusCode"),
    @XmlEnumValue("ACORDLineOfBusinessRouting")
    ACORD_LINE_OF_BUSINESS_ROUTING("ACORDLineOfBusinessRouting"),
    @XmlEnumValue("CertificatePolicyPrimaryAndNonContributory")
    CERTIFICATE_POLICY_PRIMARY_AND_NON_CONTRIBUTORY("CertificatePolicyPrimaryAndNonContributory"),
    @XmlEnumValue("ActiveAmountQualifier")
    ACTIVE_AMOUNT_QUALIFIER("ActiveAmountQualifier"),
    @XmlEnumValue("OpportunityStage")
    OPPORTUNITY_STAGE("OpportunityStage"),
    @XmlEnumValue("OpportunityOwnerMemberType")
    OPPORTUNITY_OWNER_MEMBER_TYPE("OpportunityOwnerMemberType"),
    @XmlEnumValue("OpportunityClosedStatus")
    OPPORTUNITY_CLOSED_STATUS("OpportunityClosedStatus"),
    @XmlEnumValue("OpportunityLostReasons")
    OPPORTUNITY_LOST_REASONS("OpportunityLostReasons"),
    @XmlEnumValue("ServiceCode")
    SERVICE_CODE("ServiceCode"),
    @XmlEnumValue("ServiceTerm")
    SERVICE_TERM("ServiceTerm"),
    @XmlEnumValue("ServiceType")
    SERVICE_TYPE("ServiceType"),
    @XmlEnumValue("CommonProducerBrokerCommissionAccountLookupCode")
    COMMON_PRODUCER_BROKER_COMMISSION_ACCOUNT_LOOKUP_CODE("CommonProducerBrokerCommissionAccountLookupCode"),
    @XmlEnumValue("CommonProducerBrokerCommissionContract")
    COMMON_PRODUCER_BROKER_COMMISSION_CONTRACT("CommonProducerBrokerCommissionContract"),
    @XmlEnumValue("EmployeeShareRevenueAgencyFromLine")
    EMPLOYEE_SHARE_REVENUE_AGENCY_FROM_LINE("EmployeeShareRevenueAgencyFromLine"),
    @XmlEnumValue("EmployeeShareRevenueBranchFromLine")
    EMPLOYEE_SHARE_REVENUE_BRANCH_FROM_LINE("EmployeeShareRevenueBranchFromLine"),
    @XmlEnumValue("EmployeeShareRevenueDepartmentFromLine")
    EMPLOYEE_SHARE_REVENUE_DEPARTMENT_FROM_LINE("EmployeeShareRevenueDepartmentFromLine"),
    @XmlEnumValue("EmployeeShareRevenueProfitCenterFromLine")
    EMPLOYEE_SHARE_REVENUE_PROFIT_CENTER_FROM_LINE("EmployeeShareRevenueProfitCenterFromLine"),
    @XmlEnumValue("ConfidentialClientAccessGroup")
    CONFIDENTIAL_CLIENT_ACCESS_GROUP("ConfidentialClientAccessGroup"),
    @XmlEnumValue("ConfidentialClientAccessType")
    CONFIDENTIAL_CLIENT_ACCESS_TYPE("ConfidentialClientAccessType"),
    @XmlEnumValue("RequestedDeclined")
    REQUESTED_DECLINED("RequestedDeclined"),
    @XmlEnumValue("WaterHeaterApparatusType")
    WATER_HEATER_APPARATUS_TYPE("WaterHeaterApparatusType"),
    @XmlEnumValue("SumpPumpType")
    SUMP_PUMP_TYPE("SumpPumpType"),
    @XmlEnumValue("AuxiliaryPowerType")
    AUXILIARY_POWER_TYPE("AuxiliaryPowerType"),
    @XmlEnumValue("ValveType")
    VALVE_TYPE("ValveType"),
    @XmlEnumValue("MainWaterValveShutOffType")
    MAIN_WATER_VALVE_SHUT_OFF_TYPE("MainWaterValveShutOffType"),
    @XmlEnumValue("HydrantType")
    HYDRANT_TYPE("HydrantType"),
    @XmlEnumValue("Involvement")
    INVOLVEMENT("Involvement"),
    @XmlEnumValue("PolicyLines")
    POLICY_LINES("PolicyLines"),
    @XmlEnumValue("PlanName")
    PLAN_NAME("PlanName"),
    @XmlEnumValue("PlanType")
    PLAN_TYPE("PlanType"),
    @XmlEnumValue("GroupSize")
    GROUP_SIZE("GroupSize"),
    @XmlEnumValue("WaysOfInsuring")
    WAYS_OF_INSURING("WaysOfInsuring"),
    @XmlEnumValue("MarketType")
    MARKET_TYPE("MarketType"),
    @XmlEnumValue("MarketSize")
    MARKET_SIZE("MarketSize"),
    @XmlEnumValue("PayrollCycle")
    PAYROLL_CYCLE("PayrollCycle"),
    @XmlEnumValue("LocationType")
    LOCATION_TYPE("LocationType"),
    @XmlEnumValue("EmployeeClass")
    EMPLOYEE_CLASS("EmployeeClass"),
    @XmlEnumValue("EmployeeStatus")
    EMPLOYEE_STATUS("EmployeeStatus"),
    @XmlEnumValue("Eligibility")
    ELIGIBILITY("Eligibility"),
    @XmlEnumValue("CobraAdministration")
    COBRA_ADMINISTRATION("CobraAdministration"),
    @XmlEnumValue("AvailableEmployeeClass")
    AVAILABLE_EMPLOYEE_CLASS("AvailableEmployeeClass"),
    @XmlEnumValue("Beginning")
    BEGINNING("Beginning"),
    @XmlEnumValue("WaitingPeriod")
    WAITING_PERIOD("WaitingPeriod"),
    @XmlEnumValue("PhysicalDamageReportingPeriod")
    PHYSICAL_DAMAGE_REPORTING_PERIOD("PhysicalDamageReportingPeriod"),
    @XmlEnumValue("InsuranceCompany")
    INSURANCE_COMPANY("InsuranceCompany"),
    @XmlEnumValue("ActivityUpdate")
    ACTIVITY_UPDATE("ActivityUpdate"),
    @XmlEnumValue("OpportunityStageGroup")
    OPPORTUNITY_STAGE_GROUP("OpportunityStageGroup"),
    @XmlEnumValue("SalesTargetType")
    SALES_TARGET_TYPE("SalesTargetType"),
    @XmlEnumValue("ReconciliationRecordGetLinesByClientOrPolicyNumber")
    RECONCILIATION_RECORD_GET_LINES_BY_CLIENT_OR_POLICY_NUMBER("ReconciliationRecordGetLinesByClientOrPolicyNumber"),
    @XmlEnumValue("DescriptionType")
    DESCRIPTION_TYPE("DescriptionType"),
    @XmlEnumValue("CompareByType")
    COMPARE_BY_TYPE("CompareByType"),
    @XmlEnumValue("TransactionPaymentOption")
    TRANSACTION_PAYMENT_OPTION("TransactionPaymentOption"),
    @XmlEnumValue("AgencyForNonClient")
    AGENCY_FOR_NON_CLIENT("AgencyForNonClient"),
    @XmlEnumValue("BranchForNonClient")
    BRANCH_FOR_NON_CLIENT("BranchForNonClient"),
    @XmlEnumValue("StopLossContractType")
    STOP_LOSS_CONTRACT_TYPE("StopLossContractType"),
    @XmlEnumValue("StopLossIssuingCompany")
    STOP_LOSS_ISSUING_COMPANY("StopLossIssuingCompany"),
    @XmlEnumValue("RateTier")
    RATE_TIER("RateTier"),
    @XmlEnumValue("TPAServices")
    TPA_SERVICES("TPAServices"),
    @XmlEnumValue("BenefitTermination")
    BENEFIT_TERMINATION("BenefitTermination"),
    @XmlEnumValue("PolicyLineTaxOption")
    POLICY_LINE_TAX_OPTION("PolicyLineTaxOption"),
    @XmlEnumValue("TransactionAgingCategory")
    TRANSACTION_AGING_CATEGORY("TransactionAgingCategory"),
    @XmlEnumValue("ELTOEmployerTypeCode")
    ELTO_EMPLOYER_TYPE_CODE("ELTOEmployerTypeCode"),
    @XmlEnumValue("Amount")
    AMOUNT("Amount"),
    @XmlEnumValue("ARDueDate")
    AR_DUE_DATE("ARDueDate"),
    @XmlEnumValue("UpdatePolicyBilled")
    UPDATE_POLICY_BILLED("UpdatePolicyBilled"),
    @XmlEnumValue("UpdatePolicyBilledAnnualized")
    UPDATE_POLICY_BILLED_ANNUALIZED("UpdatePolicyBilledAnnualized"),
    @XmlEnumValue("AccountSource")
    ACCOUNT_SOURCE("AccountSource"),
    @XmlEnumValue("CommercialApplicationNameType")
    COMMERCIAL_APPLICATION_NAME_TYPE("CommercialApplicationNameType"),
    @XmlEnumValue("BuildingPropertyDeductibleType")
    BUILDING_PROPERTY_DEDUCTIBLE_TYPE("BuildingPropertyDeductibleType"),
    @XmlEnumValue("PersonalPropertyDeductibleType")
    PERSONAL_PROPERTY_DEDUCTIBLE_TYPE("PersonalPropertyDeductibleType"),
    @XmlEnumValue("UKPaymentType")
    UK_PAYMENT_TYPE("UKPaymentType"),
    @XmlEnumValue("DirectDepositMethodForAccount")
    DIRECT_DEPOSIT_METHOD_FOR_ACCOUNT("DirectDepositMethodForAccount"),
    @XmlEnumValue("DirectDepositMethodForContact")
    DIRECT_DEPOSIT_METHOD_FOR_CONTACT("DirectDepositMethodForContact"),
    @XmlEnumValue("DirectDeposit")
    DIRECT_DEPOSIT("DirectDeposit"),
    @XmlEnumValue("PaymentMethodCode")
    PAYMENT_METHOD_CODE("PaymentMethodCode"),
    @XmlEnumValue("ServiceClassCode")
    SERVICE_CLASS_CODE("ServiceClassCode"),
    @XmlEnumValue("DirectWithdrawalAuthorizationStatus")
    DIRECT_WITHDRAWAL_AUTHORIZATION_STATUS("DirectWithdrawalAuthorizationStatus"),
    @XmlEnumValue("DurationDescription")
    DURATION_DESCRIPTION("DurationDescription"),
    @XmlEnumValue("DepartmentForNonClient")
    DEPARTMENT_FOR_NON_CLIENT("DepartmentForNonClient"),
    @XmlEnumValue("ProfitCenterForNonClient")
    PROFIT_CENTER_FOR_NON_CLIENT("ProfitCenterForNonClient"),
    @XmlEnumValue("ClientResponse")
    CLIENT_RESPONSE("ClientResponse"),
    @XmlEnumValue("InvoiceGrouping")
    INVOICE_GROUPING("InvoiceGrouping"),
    @XmlEnumValue("APSection")
    AP_SECTION("APSection"),
    @XmlEnumValue("LossNoticeFilter")
    LOSS_NOTICE_FILTER("LossNoticeFilter"),
    @XmlEnumValue("TaxOption")
    TAX_OPTION("TaxOption"),
    @XmlEnumValue("PolicyLineClaimType")
    POLICY_LINE_CLAIM_TYPE("PolicyLineClaimType"),
    @XmlEnumValue("ActivityEvent")
    ACTIVITY_EVENT("ActivityEvent"),
    @XmlEnumValue("ActivityCodeByEvent")
    ACTIVITY_CODE_BY_EVENT("ActivityCodeByEvent"),
    @XmlEnumValue("ReconciliationBankDetailItemGeneralLedgerAccount")
    RECONCILIATION_BANK_DETAIL_ITEM_GENERAL_LEDGER_ACCOUNT("ReconciliationBankDetailItemGeneralLedgerAccount"),
    @XmlEnumValue("ReconciliationBankDetailItemAgency")
    RECONCILIATION_BANK_DETAIL_ITEM_AGENCY("ReconciliationBankDetailItemAgency"),
    @XmlEnumValue("ReconciliationBankDetailItemBranch")
    RECONCILIATION_BANK_DETAIL_ITEM_BRANCH("ReconciliationBankDetailItemBranch"),
    @XmlEnumValue("ReconciliationBankDetailItemDepartment")
    RECONCILIATION_BANK_DETAIL_ITEM_DEPARTMENT("ReconciliationBankDetailItemDepartment"),
    @XmlEnumValue("ReconciliationBankDetailItemProfitCenter")
    RECONCILIATION_BANK_DETAIL_ITEM_PROFIT_CENTER("ReconciliationBankDetailItemProfitCenter"),
    @XmlEnumValue("ReconciliationBankBankAccount")
    RECONCILIATION_BANK_BANK_ACCOUNT("ReconciliationBankBankAccount"),
    @XmlEnumValue("RoofMaterial")
    ROOF_MATERIAL("RoofMaterial"),
    @XmlEnumValue("GLType")
    GL_TYPE("GLType"),
    @XmlEnumValue("ChartOfAccountGroup")
    CHART_OF_ACCOUNT_GROUP("ChartOfAccountGroup"),
    @XmlEnumValue("ChartOfAccountAvailableStructure")
    CHART_OF_ACCOUNT_AVAILABLE_STRUCTURE("ChartOfAccountAvailableStructure"),
    @XmlEnumValue("AgencyForBankAccount")
    AGENCY_FOR_BANK_ACCOUNT("AgencyForBankAccount"),
    @XmlEnumValue("ServiceClassCodeForBank")
    SERVICE_CLASS_CODE_FOR_BANK("ServiceClassCodeForBank"),
    @XmlEnumValue("CheckLayout")
    CHECK_LAYOUT("CheckLayout"),
    @XmlEnumValue("TransmissionFileType")
    TRANSMISSION_FILE_TYPE("TransmissionFileType"),
    @XmlEnumValue("MappingFile")
    MAPPING_FILE("MappingFile"),
    @XmlEnumValue("ContactsSummary")
    CONTACTS_SUMMARY("ContactsSummary"),
    @XmlEnumValue("AttachmentFolderByEntity")
    ATTACHMENT_FOLDER_BY_ENTITY("AttachmentFolderByEntity"),
    @XmlEnumValue("Culture")
    CULTURE("Culture"),
    @XmlEnumValue("PolicyAnnualized")
    POLICY_ANNUALIZED("PolicyAnnualized"),
    @XmlEnumValue("ServerTimeZone")
    SERVER_TIME_ZONE("ServerTimeZone"),
    @XmlEnumValue("RatePerCode")
    RATE_PER_CODE("RatePerCode"),
    @XmlEnumValue("OpportunityProduct")
    OPPORTUNITY_PRODUCT("OpportunityProduct"),
    @XmlEnumValue("EmployeeClassTimeWorkedPer")
    EMPLOYEE_CLASS_TIME_WORKED_PER("EmployeeClassTimeWorkedPer"),
    @XmlEnumValue("AllocationEntriesStructureCombinations")
    ALLOCATION_ENTRIES_STRUCTURE_COMBINATIONS("AllocationEntriesStructureCombinations"),
    @XmlEnumValue("AllocationStructureGroupingDefaultMethod")
    ALLOCATION_STRUCTURE_GROUPING_DEFAULT_METHOD("AllocationStructureGroupingDefaultMethod"),
    @XmlEnumValue("TruckersTypeOfChange")
    TRUCKERS_TYPE_OF_CHANGE("TruckersTypeOfChange"),
    @XmlEnumValue("ViewGovernmentTaxFeeRates")
    VIEW_GOVERNMENT_TAX_FEE_RATES("ViewGovernmentTaxFeeRates"),
    @XmlEnumValue("FormEdition")
    FORM_EDITION("FormEdition"),
    @XmlEnumValue("ActiveRenewalStages")
    ACTIVE_RENEWAL_STAGES("ActiveRenewalStages"),
    @XmlEnumValue("IvansProduct")
    IVANS_PRODUCT("IvansProduct"),
    @XmlEnumValue("TransactionBalanceTransferAccount")
    TRANSACTION_BALANCE_TRANSFER_ACCOUNT("TransactionBalanceTransferAccount"),
    @XmlEnumValue("TransactionBalanceTransferAgency")
    TRANSACTION_BALANCE_TRANSFER_AGENCY("TransactionBalanceTransferAgency"),
    @XmlEnumValue("TransactionBalanceTransferBranch")
    TRANSACTION_BALANCE_TRANSFER_BRANCH("TransactionBalanceTransferBranch"),
    @XmlEnumValue("DiscountTransactionCodes")
    DISCOUNT_TRANSACTION_CODES("DiscountTransactionCodes");
    private final String value;

    LookupTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LookupTypes fromValue(String v) {
        for (LookupTypes c: LookupTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
