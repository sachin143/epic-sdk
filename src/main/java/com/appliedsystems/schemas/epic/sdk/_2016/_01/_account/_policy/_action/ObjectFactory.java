
package com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfUpdateStageToSubmitted_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/", "ArrayOfUpdateStageToSubmitted");
    private final static QName _UpdateStageToSubmitted_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/", "UpdateStageToSubmitted");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfUpdateStageToSubmitted }
     * 
     */
    public ArrayOfUpdateStageToSubmitted createArrayOfUpdateStageToSubmitted() {
        return new ArrayOfUpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link UpdateStageToSubmitted }
     * 
     */
    public UpdateStageToSubmitted createUpdateStageToSubmitted() {
        return new UpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfUpdateStageToSubmitted }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/", name = "ArrayOfUpdateStageToSubmitted")
    public JAXBElement<ArrayOfUpdateStageToSubmitted> createArrayOfUpdateStageToSubmitted(ArrayOfUpdateStageToSubmitted value) {
        return new JAXBElement<ArrayOfUpdateStageToSubmitted>(_ArrayOfUpdateStageToSubmitted_QNAME, ArrayOfUpdateStageToSubmitted.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStageToSubmitted }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/", name = "UpdateStageToSubmitted")
    public JAXBElement<UpdateStageToSubmitted> createUpdateStageToSubmitted(UpdateStageToSubmitted value) {
        return new JAXBElement<UpdateStageToSubmitted>(_UpdateStageToSubmitted_QNAME, UpdateStageToSubmitted.class, null, value);
    }

}
