
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplyToDebitTransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FullPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PartialPaymentAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentItem", propOrder = {
    "applyToDebitTransactionID",
    "fullPayment",
    "partialPaymentAmount"
})
public class PaymentItem {

    @XmlElement(name = "ApplyToDebitTransactionID")
    protected Integer applyToDebitTransactionID;
    @XmlElement(name = "FullPayment")
    protected Boolean fullPayment;
    @XmlElement(name = "PartialPaymentAmount", nillable = true)
    protected BigDecimal partialPaymentAmount;

    /**
     * Gets the value of the applyToDebitTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getApplyToDebitTransactionID() {
        return applyToDebitTransactionID;
    }

    /**
     * Sets the value of the applyToDebitTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setApplyToDebitTransactionID(Integer value) {
        this.applyToDebitTransactionID = value;
    }

    /**
     * Gets the value of the fullPayment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFullPayment() {
        return fullPayment;
    }

    /**
     * Sets the value of the fullPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFullPayment(Boolean value) {
        this.fullPayment = value;
    }

    /**
     * Gets the value of the partialPaymentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPartialPaymentAmount() {
        return partialPaymentAmount;
    }

    /**
     * Sets the value of the partialPaymentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPartialPaymentAmount(BigDecimal value) {
        this.partialPaymentAmount = value;
    }

}
