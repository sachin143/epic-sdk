
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DisbursementVoid_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_action/", "DisbursementVoid");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DisbursementVoid }
     * 
     */
    public DisbursementVoid createDisbursementVoid() {
        return new DisbursementVoid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisbursementVoid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_action/", name = "DisbursementVoid")
    public JAXBElement<DisbursementVoid> createDisbursementVoid(DisbursementVoid value) {
        return new JAXBElement<DisbursementVoid>(_DisbursementVoid_QNAME, DisbursementVoid.class, null, value);
    }

}
