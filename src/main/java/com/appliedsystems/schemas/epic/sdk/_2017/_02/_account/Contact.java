
package com.appliedsystems.schemas.epic.sdk._2017._02._account;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ContactInformation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.Driver;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.Employer;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.PersonalClassifications;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.BusinessInformation;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.IdentificationNumberItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._common.PaymentMethodItems;


/**
 * <p>Java class for Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountAddressFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="AddressDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BusinessIndividualOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="BusinessInfo" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/}BusinessInformation" minOccurs="0"/>
 *         &lt;element name="BusinessName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ContactInfo" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/}ContactInformation" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/}Driver" minOccurs="0"/>
 *         &lt;element name="EmployerValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/}Employer" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FormalHeading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentificationNumberItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/}IdentificationNumberItems" minOccurs="0"/>
 *         &lt;element name="InFormalHeading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MailingFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MainBusinessContact" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonalClassificationsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/}PersonalClassifications" minOccurs="0"/>
 *         &lt;element name="Prefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContact" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Suffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentMethodItems" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/}PaymentMethodItems" minOccurs="0"/>
 *         &lt;element name="PersonGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = {
    "accountAddressFlag",
    "accountID",
    "accountType",
    "address",
    "addressDescription",
    "billingFlag",
    "businessIndividualOption",
    "businessInfo",
    "businessName",
    "categoryOption",
    "comments",
    "contactID",
    "contactInfo",
    "description",
    "driverValue",
    "employerValue",
    "firstName",
    "formalHeading",
    "identificationNumberItems",
    "inFormalHeading",
    "lastName",
    "mailingFlag",
    "mainBusinessContact",
    "middleName",
    "personalClassificationsValue",
    "prefix",
    "primaryContact",
    "siteID",
    "suffix",
    "timestamp",
    "paymentMethodItems",
    "personGUID"
})
public class Contact {

    @XmlElement(name = "AccountAddressFlag")
    protected Boolean accountAddressFlag;
    @XmlElement(name = "AccountID")
    protected Integer accountID;
    @XmlElement(name = "AccountType", nillable = true)
    protected String accountType;
    @XmlElement(name = "Address", nillable = true)
    protected Address address;
    @XmlElement(name = "AddressDescription", nillable = true)
    protected String addressDescription;
    @XmlElement(name = "BillingFlag")
    protected Boolean billingFlag;
    @XmlElement(name = "BusinessIndividualOption", nillable = true)
    protected OptionType businessIndividualOption;
    @XmlElement(name = "BusinessInfo", nillable = true)
    protected BusinessInformation businessInfo;
    @XmlElement(name = "BusinessName", nillable = true)
    protected String businessName;
    @XmlElement(name = "CategoryOption", nillable = true)
    protected OptionType categoryOption;
    @XmlElement(name = "Comments", nillable = true)
    protected String comments;
    @XmlElement(name = "ContactID")
    protected Integer contactID;
    @XmlElement(name = "ContactInfo", nillable = true)
    protected ContactInformation contactInfo;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DriverValue", nillable = true)
    protected Driver driverValue;
    @XmlElement(name = "EmployerValue", nillable = true)
    protected Employer employerValue;
    @XmlElement(name = "FirstName", nillable = true)
    protected String firstName;
    @XmlElement(name = "FormalHeading", nillable = true)
    protected String formalHeading;
    @XmlElement(name = "IdentificationNumberItems", nillable = true)
    protected IdentificationNumberItems identificationNumberItems;
    @XmlElement(name = "InFormalHeading", nillable = true)
    protected String inFormalHeading;
    @XmlElement(name = "LastName", nillable = true)
    protected String lastName;
    @XmlElement(name = "MailingFlag")
    protected Boolean mailingFlag;
    @XmlElement(name = "MainBusinessContact")
    protected Boolean mainBusinessContact;
    @XmlElement(name = "MiddleName", nillable = true)
    protected String middleName;
    @XmlElement(name = "PersonalClassificationsValue", nillable = true)
    protected PersonalClassifications personalClassificationsValue;
    @XmlElement(name = "Prefix", nillable = true)
    protected String prefix;
    @XmlElement(name = "PrimaryContact")
    protected Boolean primaryContact;
    @XmlElement(name = "SiteID", nillable = true)
    protected String siteID;
    @XmlElement(name = "Suffix", nillable = true)
    protected String suffix;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "PaymentMethodItems", nillable = true)
    protected PaymentMethodItems paymentMethodItems;
    @XmlElement(name = "PersonGUID", nillable = true)
    protected String personGUID;

    /**
     * Gets the value of the accountAddressFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccountAddressFlag() {
        return accountAddressFlag;
    }

    /**
     * Sets the value of the accountAddressFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccountAddressFlag(Boolean value) {
        this.accountAddressFlag = value;
    }

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

    /**
     * Gets the value of the addressDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressDescription() {
        return addressDescription;
    }

    /**
     * Sets the value of the addressDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressDescription(String value) {
        this.addressDescription = value;
    }

    /**
     * Gets the value of the billingFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBillingFlag() {
        return billingFlag;
    }

    /**
     * Sets the value of the billingFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBillingFlag(Boolean value) {
        this.billingFlag = value;
    }

    /**
     * Gets the value of the businessIndividualOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBusinessIndividualOption() {
        return businessIndividualOption;
    }

    /**
     * Sets the value of the businessIndividualOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBusinessIndividualOption(OptionType value) {
        this.businessIndividualOption = value;
    }

    /**
     * Gets the value of the businessInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessInformation }
     *     
     */
    public BusinessInformation getBusinessInfo() {
        return businessInfo;
    }

    /**
     * Sets the value of the businessInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessInformation }
     *     
     */
    public void setBusinessInfo(BusinessInformation value) {
        this.businessInfo = value;
    }

    /**
     * Gets the value of the businessName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * Sets the value of the businessName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessName(String value) {
        this.businessName = value;
    }

    /**
     * Gets the value of the categoryOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCategoryOption() {
        return categoryOption;
    }

    /**
     * Sets the value of the categoryOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCategoryOption(OptionType value) {
        this.categoryOption = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContactID(Integer value) {
        this.contactID = value;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ContactInformation }
     *     
     */
    public ContactInformation getContactInfo() {
        return contactInfo;
    }

    /**
     * Sets the value of the contactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInformation }
     *     
     */
    public void setContactInfo(ContactInformation value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the driverValue property.
     * 
     * @return
     *     possible object is
     *     {@link Driver }
     *     
     */
    public Driver getDriverValue() {
        return driverValue;
    }

    /**
     * Sets the value of the driverValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Driver }
     *     
     */
    public void setDriverValue(Driver value) {
        this.driverValue = value;
    }

    /**
     * Gets the value of the employerValue property.
     * 
     * @return
     *     possible object is
     *     {@link Employer }
     *     
     */
    public Employer getEmployerValue() {
        return employerValue;
    }

    /**
     * Sets the value of the employerValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Employer }
     *     
     */
    public void setEmployerValue(Employer value) {
        this.employerValue = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the formalHeading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormalHeading() {
        return formalHeading;
    }

    /**
     * Sets the value of the formalHeading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormalHeading(String value) {
        this.formalHeading = value;
    }

    /**
     * Gets the value of the identificationNumberItems property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationNumberItems }
     *     
     */
    public IdentificationNumberItems getIdentificationNumberItems() {
        return identificationNumberItems;
    }

    /**
     * Sets the value of the identificationNumberItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationNumberItems }
     *     
     */
    public void setIdentificationNumberItems(IdentificationNumberItems value) {
        this.identificationNumberItems = value;
    }

    /**
     * Gets the value of the inFormalHeading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInFormalHeading() {
        return inFormalHeading;
    }

    /**
     * Sets the value of the inFormalHeading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInFormalHeading(String value) {
        this.inFormalHeading = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the mailingFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMailingFlag() {
        return mailingFlag;
    }

    /**
     * Sets the value of the mailingFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMailingFlag(Boolean value) {
        this.mailingFlag = value;
    }

    /**
     * Gets the value of the mainBusinessContact property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMainBusinessContact() {
        return mainBusinessContact;
    }

    /**
     * Sets the value of the mainBusinessContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMainBusinessContact(Boolean value) {
        this.mainBusinessContact = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the personalClassificationsValue property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalClassifications }
     *     
     */
    public PersonalClassifications getPersonalClassificationsValue() {
        return personalClassificationsValue;
    }

    /**
     * Sets the value of the personalClassificationsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalClassifications }
     *     
     */
    public void setPersonalClassificationsValue(PersonalClassifications value) {
        this.personalClassificationsValue = value;
    }

    /**
     * Gets the value of the prefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Sets the value of the prefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefix(String value) {
        this.prefix = value;
    }

    /**
     * Gets the value of the primaryContact property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimaryContact() {
        return primaryContact;
    }

    /**
     * Sets the value of the primaryContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryContact(Boolean value) {
        this.primaryContact = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the suffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Sets the value of the suffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuffix(String value) {
        this.suffix = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the paymentMethodItems property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodItems }
     *     
     */
    public PaymentMethodItems getPaymentMethodItems() {
        return paymentMethodItems;
    }

    /**
     * Sets the value of the paymentMethodItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodItems }
     *     
     */
    public void setPaymentMethodItems(PaymentMethodItems value) {
        this.paymentMethodItems = value;
    }

    /**
     * Gets the value of the personGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonGUID() {
        return personGUID;
    }

    /**
     * Sets the value of the personGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonGUID(String value) {
        this.personGUID = value;
    }

}
