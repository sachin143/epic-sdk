
package com.appliedsystems.schemas.epic.sdk._2021._01._account._client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.Flags;


/**
 * <p>Java class for EmployeeClassItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployeeClassItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CoverageEnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoverageStart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Eligibility" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployeeClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployeeClassID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/_employeeclassitem/}Flags" minOccurs="0"/>
 *         &lt;element name="PayrollCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TimeWorked" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TimeWorkedPerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TotalCountAsOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="WaitingPeriod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployeeClassItem", propOrder = {
    "coverageEnd",
    "coverageStart",
    "eligibility",
    "employeeClass",
    "employeeClassID",
    "flag",
    "payrollCycle",
    "status",
    "timeWorked",
    "timeWorkedPerCode",
    "totalCount",
    "totalCountAsOf",
    "waitingPeriod"
})
public class EmployeeClassItem {

    @XmlElement(name = "CoverageEnd", nillable = true)
    protected String coverageEnd;
    @XmlElement(name = "CoverageStart", nillable = true)
    protected String coverageStart;
    @XmlElement(name = "Eligibility", nillable = true)
    protected String eligibility;
    @XmlElement(name = "EmployeeClass", nillable = true)
    protected String employeeClass;
    @XmlElement(name = "EmployeeClassID")
    protected Integer employeeClassID;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "PayrollCycle", nillable = true)
    protected String payrollCycle;
    @XmlElement(name = "Status", nillable = true)
    protected String status;
    @XmlElement(name = "TimeWorked", nillable = true)
    protected Integer timeWorked;
    @XmlElement(name = "TimeWorkedPerCode", nillable = true)
    protected String timeWorkedPerCode;
    @XmlElement(name = "TotalCount", nillable = true)
    protected Integer totalCount;
    @XmlElement(name = "TotalCountAsOf", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar totalCountAsOf;
    @XmlElement(name = "WaitingPeriod", nillable = true)
    protected String waitingPeriod;

    /**
     * Gets the value of the coverageEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverageEnd() {
        return coverageEnd;
    }

    /**
     * Sets the value of the coverageEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageEnd(String value) {
        this.coverageEnd = value;
    }

    /**
     * Gets the value of the coverageStart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverageStart() {
        return coverageStart;
    }

    /**
     * Sets the value of the coverageStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageStart(String value) {
        this.coverageStart = value;
    }

    /**
     * Gets the value of the eligibility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligibility() {
        return eligibility;
    }

    /**
     * Sets the value of the eligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligibility(String value) {
        this.eligibility = value;
    }

    /**
     * Gets the value of the employeeClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeClass() {
        return employeeClass;
    }

    /**
     * Sets the value of the employeeClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeClass(String value) {
        this.employeeClass = value;
    }

    /**
     * Gets the value of the employeeClassID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmployeeClassID() {
        return employeeClassID;
    }

    /**
     * Sets the value of the employeeClassID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmployeeClassID(Integer value) {
        this.employeeClassID = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the payrollCycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayrollCycle() {
        return payrollCycle;
    }

    /**
     * Sets the value of the payrollCycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayrollCycle(String value) {
        this.payrollCycle = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the timeWorked property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeWorked() {
        return timeWorked;
    }

    /**
     * Sets the value of the timeWorked property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeWorked(Integer value) {
        this.timeWorked = value;
    }

    /**
     * Gets the value of the timeWorkedPerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeWorkedPerCode() {
        return timeWorkedPerCode;
    }

    /**
     * Sets the value of the timeWorkedPerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeWorkedPerCode(String value) {
        this.timeWorkedPerCode = value;
    }

    /**
     * Gets the value of the totalCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * Sets the value of the totalCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalCount(Integer value) {
        this.totalCount = value;
    }

    /**
     * Gets the value of the totalCountAsOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTotalCountAsOf() {
        return totalCountAsOf;
    }

    /**
     * Sets the value of the totalCountAsOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTotalCountAsOf(XMLGregorianCalendar value) {
        this.totalCountAsOf = value;
    }

    /**
     * Gets the value of the waitingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaitingPeriod() {
        return waitingPeriod;
    }

    /**
     * Sets the value of the waitingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaitingPeriod(String value) {
        this.waitingPeriod = value;
    }

}
