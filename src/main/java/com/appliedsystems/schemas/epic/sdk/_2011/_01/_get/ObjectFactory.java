
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AttachmentSorting_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "AttachmentSorting");
    private final static QName _LineFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "LineFilter");
    private final static QName _AttachmentFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "AttachmentFilter");
    private final static QName _AttachmentGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "AttachmentGetResult");
    private final static QName _DirectBillCommissionFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "DirectBillCommissionFilter");
    private final static QName _SortOrder_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "SortOrder");
    private final static QName _ActivityGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "ActivityGetResult");
    private final static QName _ActivityFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "ActivityFilter");
    private final static QName _TransactionGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "TransactionGetResult");
    private final static QName _PolicyGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "PolicyGetResult");
    private final static QName _PolicyFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", "PolicyFilter");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivityGetResult }
     * 
     */
    public ActivityGetResult createActivityGetResult() {
        return new ActivityGetResult();
    }

    /**
     * Create an instance of {@link ActivityFilter }
     * 
     */
    public ActivityFilter createActivityFilter() {
        return new ActivityFilter();
    }

    /**
     * Create an instance of {@link PolicyFilter }
     * 
     */
    public PolicyFilter createPolicyFilter() {
        return new PolicyFilter();
    }

    /**
     * Create an instance of {@link LineFilter }
     * 
     */
    public LineFilter createLineFilter() {
        return new LineFilter();
    }

    /**
     * Create an instance of {@link PolicyGetResult }
     * 
     */
    public PolicyGetResult createPolicyGetResult() {
        return new PolicyGetResult();
    }

    /**
     * Create an instance of {@link DirectBillCommissionFilter }
     * 
     */
    public DirectBillCommissionFilter createDirectBillCommissionFilter() {
        return new DirectBillCommissionFilter();
    }

    /**
     * Create an instance of {@link AttachmentGetResult }
     * 
     */
    public AttachmentGetResult createAttachmentGetResult() {
        return new AttachmentGetResult();
    }

    /**
     * Create an instance of {@link TransactionGetResult }
     * 
     */
    public TransactionGetResult createTransactionGetResult() {
        return new TransactionGetResult();
    }

    /**
     * Create an instance of {@link AttachmentFilter }
     * 
     */
    public AttachmentFilter createAttachmentFilter() {
        return new AttachmentFilter();
    }

    /**
     * Create an instance of {@link AttachmentSorting }
     * 
     */
    public AttachmentSorting createAttachmentSorting() {
        return new AttachmentSorting();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentSorting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "AttachmentSorting")
    public JAXBElement<AttachmentSorting> createAttachmentSorting(AttachmentSorting value) {
        return new JAXBElement<AttachmentSorting>(_AttachmentSorting_QNAME, AttachmentSorting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "LineFilter")
    public JAXBElement<LineFilter> createLineFilter(LineFilter value) {
        return new JAXBElement<LineFilter>(_LineFilter_QNAME, LineFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "AttachmentFilter")
    public JAXBElement<AttachmentFilter> createAttachmentFilter(AttachmentFilter value) {
        return new JAXBElement<AttachmentFilter>(_AttachmentFilter_QNAME, AttachmentFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "AttachmentGetResult")
    public JAXBElement<AttachmentGetResult> createAttachmentGetResult(AttachmentGetResult value) {
        return new JAXBElement<AttachmentGetResult>(_AttachmentGetResult_QNAME, AttachmentGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectBillCommissionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "DirectBillCommissionFilter")
    public JAXBElement<DirectBillCommissionFilter> createDirectBillCommissionFilter(DirectBillCommissionFilter value) {
        return new JAXBElement<DirectBillCommissionFilter>(_DirectBillCommissionFilter_QNAME, DirectBillCommissionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SortOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "SortOrder")
    public JAXBElement<SortOrder> createSortOrder(SortOrder value) {
        return new JAXBElement<SortOrder>(_SortOrder_QNAME, SortOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "ActivityGetResult")
    public JAXBElement<ActivityGetResult> createActivityGetResult(ActivityGetResult value) {
        return new JAXBElement<ActivityGetResult>(_ActivityGetResult_QNAME, ActivityGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "ActivityFilter")
    public JAXBElement<ActivityFilter> createActivityFilter(ActivityFilter value) {
        return new JAXBElement<ActivityFilter>(_ActivityFilter_QNAME, ActivityFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "TransactionGetResult")
    public JAXBElement<TransactionGetResult> createTransactionGetResult(TransactionGetResult value) {
        return new JAXBElement<TransactionGetResult>(_TransactionGetResult_QNAME, TransactionGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "PolicyGetResult")
    public JAXBElement<PolicyGetResult> createPolicyGetResult(PolicyGetResult value) {
        return new JAXBElement<PolicyGetResult>(_PolicyGetResult_QNAME, PolicyGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/", name = "PolicyFilter")
    public JAXBElement<PolicyFilter> createPolicyFilter(PolicyFilter value) {
        return new JAXBElement<PolicyFilter>(_PolicyFilter_QNAME, PolicyFilter.class, null, value);
    }

}
