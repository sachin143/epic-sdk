
package com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NoteItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/", "NoteItems");
    private final static QName _NoteItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/", "NoteItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NoteItem }
     * 
     */
    public NoteItem createNoteItem() {
        return new NoteItem();
    }

    /**
     * Create an instance of {@link NoteItems }
     * 
     */
    public NoteItems createNoteItems() {
        return new NoteItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoteItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/", name = "NoteItems")
    public JAXBElement<NoteItems> createNoteItems(NoteItems value) {
        return new JAXBElement<NoteItems>(_NoteItems_QNAME, NoteItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoteItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/", name = "NoteItem")
    public JAXBElement<NoteItem> createNoteItem(NoteItem value) {
        return new JAXBElement<NoteItem>(_NoteItem_QNAME, NoteItem.class, null, value);
    }

}
