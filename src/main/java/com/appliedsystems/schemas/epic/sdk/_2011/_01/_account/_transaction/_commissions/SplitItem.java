
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.Flags;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ProducerBrokerCommissionItems;


/**
 * <p>Java class for SplitItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionPercentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencySplitAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CompanyPayableDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissions/_splititem/}Flags" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MultiCarrierScheduleID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PremiumPayableContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PremiumPayableLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PremiumPayableTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCommissions" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_commissionsplititem/}ProducerBrokerCommissionItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitItem", propOrder = {
    "agencyCommissionAmount",
    "agencyCommissionPercentage",
    "agencyCommissionTypeCode",
    "agencySplitAmount",
    "companyPayableDueDate",
    "flag",
    "issuingCompanyLookupCode",
    "lineID",
    "multiCarrierScheduleID",
    "premiumPayableContractID",
    "premiumPayableLookupCode",
    "premiumPayableTypeCode",
    "producerBrokerCommissions"
})
public class SplitItem {

    @XmlElement(name = "AgencyCommissionAmount")
    protected BigDecimal agencyCommissionAmount;
    @XmlElement(name = "AgencyCommissionPercentage")
    protected BigDecimal agencyCommissionPercentage;
    @XmlElement(name = "AgencyCommissionTypeCode", nillable = true)
    protected String agencyCommissionTypeCode;
    @XmlElement(name = "AgencySplitAmount")
    protected BigDecimal agencySplitAmount;
    @XmlElement(name = "CompanyPayableDueDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar companyPayableDueDate;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "IssuingCompanyLookupCode", nillable = true)
    protected String issuingCompanyLookupCode;
    @XmlElement(name = "LineID")
    protected Integer lineID;
    @XmlElement(name = "MultiCarrierScheduleID")
    protected Integer multiCarrierScheduleID;
    @XmlElement(name = "PremiumPayableContractID")
    protected Integer premiumPayableContractID;
    @XmlElement(name = "PremiumPayableLookupCode", nillable = true)
    protected String premiumPayableLookupCode;
    @XmlElement(name = "PremiumPayableTypeCode", nillable = true)
    protected String premiumPayableTypeCode;
    @XmlElement(name = "ProducerBrokerCommissions", nillable = true)
    protected ProducerBrokerCommissionItems producerBrokerCommissions;

    /**
     * Gets the value of the agencyCommissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionAmount() {
        return agencyCommissionAmount;
    }

    /**
     * Sets the value of the agencyCommissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionAmount(BigDecimal value) {
        this.agencyCommissionAmount = value;
    }

    /**
     * Gets the value of the agencyCommissionPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionPercentage() {
        return agencyCommissionPercentage;
    }

    /**
     * Sets the value of the agencyCommissionPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionPercentage(BigDecimal value) {
        this.agencyCommissionPercentage = value;
    }

    /**
     * Gets the value of the agencyCommissionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCommissionTypeCode() {
        return agencyCommissionTypeCode;
    }

    /**
     * Sets the value of the agencyCommissionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCommissionTypeCode(String value) {
        this.agencyCommissionTypeCode = value;
    }

    /**
     * Gets the value of the agencySplitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencySplitAmount() {
        return agencySplitAmount;
    }

    /**
     * Sets the value of the agencySplitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencySplitAmount(BigDecimal value) {
        this.agencySplitAmount = value;
    }

    /**
     * Gets the value of the companyPayableDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCompanyPayableDueDate() {
        return companyPayableDueDate;
    }

    /**
     * Sets the value of the companyPayableDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCompanyPayableDueDate(XMLGregorianCalendar value) {
        this.companyPayableDueDate = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the issuingCompanyLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyLookupCode() {
        return issuingCompanyLookupCode;
    }

    /**
     * Sets the value of the issuingCompanyLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyLookupCode(String value) {
        this.issuingCompanyLookupCode = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the multiCarrierScheduleID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMultiCarrierScheduleID() {
        return multiCarrierScheduleID;
    }

    /**
     * Sets the value of the multiCarrierScheduleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMultiCarrierScheduleID(Integer value) {
        this.multiCarrierScheduleID = value;
    }

    /**
     * Gets the value of the premiumPayableContractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPremiumPayableContractID() {
        return premiumPayableContractID;
    }

    /**
     * Sets the value of the premiumPayableContractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPremiumPayableContractID(Integer value) {
        this.premiumPayableContractID = value;
    }

    /**
     * Gets the value of the premiumPayableLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableLookupCode() {
        return premiumPayableLookupCode;
    }

    /**
     * Sets the value of the premiumPayableLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableLookupCode(String value) {
        this.premiumPayableLookupCode = value;
    }

    /**
     * Gets the value of the premiumPayableTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableTypeCode() {
        return premiumPayableTypeCode;
    }

    /**
     * Sets the value of the premiumPayableTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableTypeCode(String value) {
        this.premiumPayableTypeCode = value;
    }

    /**
     * Gets the value of the producerBrokerCommissions property.
     * 
     * @return
     *     possible object is
     *     {@link ProducerBrokerCommissionItems }
     *     
     */
    public ProducerBrokerCommissionItems getProducerBrokerCommissions() {
        return producerBrokerCommissions;
    }

    /**
     * Sets the value of the producerBrokerCommissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProducerBrokerCommissionItems }
     *     
     */
    public void setProducerBrokerCommissions(ProducerBrokerCommissionItems value) {
        this.producerBrokerCommissions = value;
    }

}
