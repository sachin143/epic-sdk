
package com.appliedsystems.schemas.epic.sdk._2011._01._common._activity;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.NoteItems;


/**
 * <p>Java class for Detail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Detail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactNumberEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactVia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FollowUpEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FollowUpEndTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FollowUpStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FollowUpStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_activity/_common/}NoteItems" minOccurs="0"/>
 *         &lt;element name="PremiumPayableLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PremiumPayableTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReminderDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReminderTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ContactPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AmountQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Update" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Detail", propOrder = {
    "contactName",
    "contactNumberEmail",
    "contactVia",
    "followUpEndDate",
    "followUpEndTime",
    "followUpStartDate",
    "followUpStartTime",
    "issuingCompanyLookupCode",
    "notes",
    "premiumPayableLookupCode",
    "premiumPayableTypeCode",
    "reminderDate",
    "reminderTime",
    "contactPhoneCountryCode",
    "amount",
    "amountQualifier",
    "update",
    "contactID"
})
public class Detail {

    @XmlElement(name = "ContactName", nillable = true)
    protected String contactName;
    @XmlElement(name = "ContactNumberEmail", nillable = true)
    protected String contactNumberEmail;
    @XmlElement(name = "ContactVia", nillable = true)
    protected String contactVia;
    @XmlElement(name = "FollowUpEndDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpEndDate;
    @XmlElement(name = "FollowUpEndTime", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpEndTime;
    @XmlElement(name = "FollowUpStartDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpStartDate;
    @XmlElement(name = "FollowUpStartTime", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar followUpStartTime;
    @XmlElement(name = "IssuingCompanyLookupCode", nillable = true)
    protected String issuingCompanyLookupCode;
    @XmlElement(name = "Notes", nillable = true)
    protected NoteItems notes;
    @XmlElement(name = "PremiumPayableLookupCode", nillable = true)
    protected String premiumPayableLookupCode;
    @XmlElement(name = "PremiumPayableTypeCode", nillable = true)
    protected String premiumPayableTypeCode;
    @XmlElement(name = "ReminderDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reminderDate;
    @XmlElement(name = "ReminderTime", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reminderTime;
    @XmlElement(name = "ContactPhoneCountryCode", nillable = true)
    protected String contactPhoneCountryCode;
    @XmlElement(name = "Amount", nillable = true)
    protected BigDecimal amount;
    @XmlElement(name = "AmountQualifier", nillable = true)
    protected String amountQualifier;
    @XmlElement(name = "Update", nillable = true)
    protected String update;
    @XmlElement(name = "ContactID", nillable = true)
    protected Integer contactID;

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the contactNumberEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactNumberEmail() {
        return contactNumberEmail;
    }

    /**
     * Sets the value of the contactNumberEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactNumberEmail(String value) {
        this.contactNumberEmail = value;
    }

    /**
     * Gets the value of the contactVia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactVia() {
        return contactVia;
    }

    /**
     * Sets the value of the contactVia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactVia(String value) {
        this.contactVia = value;
    }

    /**
     * Gets the value of the followUpEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpEndDate() {
        return followUpEndDate;
    }

    /**
     * Sets the value of the followUpEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpEndDate(XMLGregorianCalendar value) {
        this.followUpEndDate = value;
    }

    /**
     * Gets the value of the followUpEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpEndTime() {
        return followUpEndTime;
    }

    /**
     * Sets the value of the followUpEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpEndTime(XMLGregorianCalendar value) {
        this.followUpEndTime = value;
    }

    /**
     * Gets the value of the followUpStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpStartDate() {
        return followUpStartDate;
    }

    /**
     * Sets the value of the followUpStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpStartDate(XMLGregorianCalendar value) {
        this.followUpStartDate = value;
    }

    /**
     * Gets the value of the followUpStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFollowUpStartTime() {
        return followUpStartTime;
    }

    /**
     * Sets the value of the followUpStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFollowUpStartTime(XMLGregorianCalendar value) {
        this.followUpStartTime = value;
    }

    /**
     * Gets the value of the issuingCompanyLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyLookupCode() {
        return issuingCompanyLookupCode;
    }

    /**
     * Sets the value of the issuingCompanyLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyLookupCode(String value) {
        this.issuingCompanyLookupCode = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link NoteItems }
     *     
     */
    public NoteItems getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link NoteItems }
     *     
     */
    public void setNotes(NoteItems value) {
        this.notes = value;
    }

    /**
     * Gets the value of the premiumPayableLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableLookupCode() {
        return premiumPayableLookupCode;
    }

    /**
     * Sets the value of the premiumPayableLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableLookupCode(String value) {
        this.premiumPayableLookupCode = value;
    }

    /**
     * Gets the value of the premiumPayableTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableTypeCode() {
        return premiumPayableTypeCode;
    }

    /**
     * Sets the value of the premiumPayableTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableTypeCode(String value) {
        this.premiumPayableTypeCode = value;
    }

    /**
     * Gets the value of the reminderDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReminderDate() {
        return reminderDate;
    }

    /**
     * Sets the value of the reminderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReminderDate(XMLGregorianCalendar value) {
        this.reminderDate = value;
    }

    /**
     * Gets the value of the reminderTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReminderTime() {
        return reminderTime;
    }

    /**
     * Sets the value of the reminderTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReminderTime(XMLGregorianCalendar value) {
        this.reminderTime = value;
    }

    /**
     * Gets the value of the contactPhoneCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPhoneCountryCode() {
        return contactPhoneCountryCode;
    }

    /**
     * Sets the value of the contactPhoneCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPhoneCountryCode(String value) {
        this.contactPhoneCountryCode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the amountQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountQualifier() {
        return amountQualifier;
    }

    /**
     * Sets the value of the amountQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountQualifier(String value) {
        this.amountQualifier = value;
    }

    /**
     * Gets the value of the update property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdate() {
        return update;
    }

    /**
     * Sets the value of the update property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdate(String value) {
        this.update = value;
    }

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContactID(Integer value) {
        this.contactID = value;
    }

}
