
package com.appliedsystems.schemas.epic.sdk._2009._07._account._contact;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._contact package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ContactInformation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", "ContactInformation");
    private final static QName _PersonalClassifications_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", "PersonalClassifications");
    private final static QName _Driver_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", "Driver");
    private final static QName _Employer_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", "Employer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._contact
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PersonalClassifications }
     * 
     */
    public PersonalClassifications createPersonalClassifications() {
        return new PersonalClassifications();
    }

    /**
     * Create an instance of {@link Employer }
     * 
     */
    public Employer createEmployer() {
        return new Employer();
    }

    /**
     * Create an instance of {@link Driver }
     * 
     */
    public Driver createDriver() {
        return new Driver();
    }

    /**
     * Create an instance of {@link ContactInformation }
     * 
     */
    public ContactInformation createContactInformation() {
        return new ContactInformation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", name = "ContactInformation")
    public JAXBElement<ContactInformation> createContactInformation(ContactInformation value) {
        return new JAXBElement<ContactInformation>(_ContactInformation_QNAME, ContactInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonalClassifications }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", name = "PersonalClassifications")
    public JAXBElement<PersonalClassifications> createPersonalClassifications(PersonalClassifications value) {
        return new JAXBElement<PersonalClassifications>(_PersonalClassifications_QNAME, PersonalClassifications.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Driver }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", name = "Driver")
    public JAXBElement<Driver> createDriver(Driver value) {
        return new JAXBElement<Driver>(_Driver_QNAME, Driver.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Employer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/", name = "Employer")
    public JAXBElement<Employer> createEmployer(Employer value) {
        return new JAXBElement<Employer>(_Employer_QNAME, Employer.class, null, value);
    }

}
