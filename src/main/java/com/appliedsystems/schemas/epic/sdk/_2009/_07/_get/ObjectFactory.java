
package com.appliedsystems.schemas.epic.sdk._2009._07._get;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._get package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClientFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", "ClientFilter");
    private final static QName _ContactFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", "ContactFilter");
    private final static QName _ClaimSummaryGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", "ClaimSummaryGetResult");
    private final static QName _ClientGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", "ClientGetResult");
    private final static QName _ClaimSummaryFilter_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", "ClaimSummaryFilter");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._get
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClientFilter }
     * 
     */
    public ClientFilter createClientFilter() {
        return new ClientFilter();
    }

    /**
     * Create an instance of {@link ClientGetResult }
     * 
     */
    public ClientGetResult createClientGetResult() {
        return new ClientGetResult();
    }

    /**
     * Create an instance of {@link ContactFilter }
     * 
     */
    public ContactFilter createContactFilter() {
        return new ContactFilter();
    }

    /**
     * Create an instance of {@link ClaimSummaryFilter }
     * 
     */
    public ClaimSummaryFilter createClaimSummaryFilter() {
        return new ClaimSummaryFilter();
    }

    /**
     * Create an instance of {@link ClaimSummaryGetResult }
     * 
     */
    public ClaimSummaryGetResult createClaimSummaryGetResult() {
        return new ClaimSummaryGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", name = "ClientFilter")
    public JAXBElement<ClientFilter> createClientFilter(ClientFilter value) {
        return new JAXBElement<ClientFilter>(_ClientFilter_QNAME, ClientFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", name = "ContactFilter")
    public JAXBElement<ContactFilter> createContactFilter(ContactFilter value) {
        return new JAXBElement<ContactFilter>(_ContactFilter_QNAME, ContactFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClaimSummaryGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", name = "ClaimSummaryGetResult")
    public JAXBElement<ClaimSummaryGetResult> createClaimSummaryGetResult(ClaimSummaryGetResult value) {
        return new JAXBElement<ClaimSummaryGetResult>(_ClaimSummaryGetResult_QNAME, ClaimSummaryGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", name = "ClientGetResult")
    public JAXBElement<ClientGetResult> createClientGetResult(ClientGetResult value) {
        return new JAXBElement<ClientGetResult>(_ClientGetResult_QNAME, ClientGetResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClaimSummaryFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/", name = "ClaimSummaryFilter")
    public JAXBElement<ClaimSummaryFilter> createClaimSummaryFilter(ClaimSummaryFilter value) {
        return new JAXBElement<ClaimSummaryFilter>(_ClaimSummaryFilter_QNAME, ClaimSummaryFilter.class, null, value);
    }

}
