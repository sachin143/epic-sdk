
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.PaidItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.PaidStatementItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.PayablesCommissions;


/**
 * <p>Java class for DetailItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DetailItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ApplyTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyIncludeHistory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ApplyToPolicyLineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ApplyToSelectedItemsApplyCreditsToDebits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}ApplyCreditsToDebits" minOccurs="0"/>
 *         &lt;element name="DebitCreditOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailItemAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailItemID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DetailItemType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}Flags" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerScheduleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBankAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Method" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StructureProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyToPayablesCommissionsSelectPayablesCommissions" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}PayablesCommissions" minOccurs="0"/>
 *         &lt;element name="PaidItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}PaidItems" minOccurs="0"/>
 *         &lt;element name="PaidStatements" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/}PaidStatementItems" minOccurs="0"/>
 *         &lt;element name="CashOnAccountOptions" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetailItem", propOrder = {
    "amount",
    "applyTo",
    "applyToClientLookupCode",
    "applyToPolicyClientLookupCode",
    "applyToPolicyIncludeHistory",
    "applyToPolicyLineID",
    "applyToSelectedItemsApplyCreditsToDebits",
    "debitCreditOption",
    "description",
    "detailItemAccountLookupCode",
    "detailItemID",
    "detailItemType",
    "flag",
    "generalLedgerAccountNumberCode",
    "generalLedgerScheduleCode",
    "generalLedgerSubAccountNumberCode",
    "isBankAccount",
    "method",
    "paymentDate",
    "paymentID",
    "routingNumber",
    "structureAgencyCode",
    "structureBranchCode",
    "structureDepartmentCode",
    "structureProfitCenterCode",
    "applyToPayablesCommissionsSelectPayablesCommissions",
    "paidItems",
    "paidStatements",
    "cashOnAccountOptions"
})
public class DetailItem {

    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "ApplyTo", nillable = true)
    protected String applyTo;
    @XmlElement(name = "ApplyToClientLookupCode", nillable = true)
    protected String applyToClientLookupCode;
    @XmlElement(name = "ApplyToPolicyClientLookupCode", nillable = true)
    protected String applyToPolicyClientLookupCode;
    @XmlElement(name = "ApplyToPolicyIncludeHistory")
    protected Boolean applyToPolicyIncludeHistory;
    @XmlElement(name = "ApplyToPolicyLineID")
    protected Integer applyToPolicyLineID;
    @XmlElement(name = "ApplyToSelectedItemsApplyCreditsToDebits", nillable = true)
    protected ApplyCreditsToDebits applyToSelectedItemsApplyCreditsToDebits;
    @XmlElement(name = "DebitCreditOption", nillable = true)
    protected OptionType debitCreditOption;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DetailItemAccountLookupCode", nillable = true)
    protected String detailItemAccountLookupCode;
    @XmlElement(name = "DetailItemID")
    protected Integer detailItemID;
    @XmlElement(name = "DetailItemType", nillable = true)
    protected String detailItemType;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "GeneralLedgerAccountNumberCode", nillable = true)
    protected String generalLedgerAccountNumberCode;
    @XmlElement(name = "GeneralLedgerScheduleCode", nillable = true)
    protected String generalLedgerScheduleCode;
    @XmlElement(name = "GeneralLedgerSubAccountNumberCode", nillable = true)
    protected String generalLedgerSubAccountNumberCode;
    @XmlElement(name = "IsBankAccount")
    protected Boolean isBankAccount;
    @XmlElement(name = "Method", nillable = true)
    protected String method;
    @XmlElement(name = "PaymentDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentDate;
    @XmlElement(name = "PaymentID", nillable = true)
    protected String paymentID;
    @XmlElement(name = "RoutingNumber", nillable = true)
    protected String routingNumber;
    @XmlElement(name = "StructureAgencyCode", nillable = true)
    protected String structureAgencyCode;
    @XmlElement(name = "StructureBranchCode", nillable = true)
    protected String structureBranchCode;
    @XmlElement(name = "StructureDepartmentCode", nillable = true)
    protected String structureDepartmentCode;
    @XmlElement(name = "StructureProfitCenterCode", nillable = true)
    protected String structureProfitCenterCode;
    @XmlElement(name = "ApplyToPayablesCommissionsSelectPayablesCommissions", nillable = true)
    protected PayablesCommissions applyToPayablesCommissionsSelectPayablesCommissions;
    @XmlElement(name = "PaidItems", nillable = true)
    protected PaidItems paidItems;
    @XmlElement(name = "PaidStatements", nillable = true)
    protected PaidStatementItems paidStatements;
    @XmlElement(name = "CashOnAccountOptions", nillable = true)
    protected OptionType cashOnAccountOptions;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the applyTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyTo() {
        return applyTo;
    }

    /**
     * Sets the value of the applyTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyTo(String value) {
        this.applyTo = value;
    }

    /**
     * Gets the value of the applyToClientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToClientLookupCode() {
        return applyToClientLookupCode;
    }

    /**
     * Sets the value of the applyToClientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToClientLookupCode(String value) {
        this.applyToClientLookupCode = value;
    }

    /**
     * Gets the value of the applyToPolicyClientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyToPolicyClientLookupCode() {
        return applyToPolicyClientLookupCode;
    }

    /**
     * Sets the value of the applyToPolicyClientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyToPolicyClientLookupCode(String value) {
        this.applyToPolicyClientLookupCode = value;
    }

    /**
     * Gets the value of the applyToPolicyIncludeHistory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyToPolicyIncludeHistory() {
        return applyToPolicyIncludeHistory;
    }

    /**
     * Sets the value of the applyToPolicyIncludeHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyToPolicyIncludeHistory(Boolean value) {
        this.applyToPolicyIncludeHistory = value;
    }

    /**
     * Gets the value of the applyToPolicyLineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getApplyToPolicyLineID() {
        return applyToPolicyLineID;
    }

    /**
     * Sets the value of the applyToPolicyLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setApplyToPolicyLineID(Integer value) {
        this.applyToPolicyLineID = value;
    }

    /**
     * Gets the value of the applyToSelectedItemsApplyCreditsToDebits property.
     * 
     * @return
     *     possible object is
     *     {@link ApplyCreditsToDebits }
     *     
     */
    public ApplyCreditsToDebits getApplyToSelectedItemsApplyCreditsToDebits() {
        return applyToSelectedItemsApplyCreditsToDebits;
    }

    /**
     * Sets the value of the applyToSelectedItemsApplyCreditsToDebits property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyCreditsToDebits }
     *     
     */
    public void setApplyToSelectedItemsApplyCreditsToDebits(ApplyCreditsToDebits value) {
        this.applyToSelectedItemsApplyCreditsToDebits = value;
    }

    /**
     * Gets the value of the debitCreditOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getDebitCreditOption() {
        return debitCreditOption;
    }

    /**
     * Sets the value of the debitCreditOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setDebitCreditOption(OptionType value) {
        this.debitCreditOption = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailItemAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailItemAccountLookupCode() {
        return detailItemAccountLookupCode;
    }

    /**
     * Sets the value of the detailItemAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailItemAccountLookupCode(String value) {
        this.detailItemAccountLookupCode = value;
    }

    /**
     * Gets the value of the detailItemID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDetailItemID() {
        return detailItemID;
    }

    /**
     * Sets the value of the detailItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDetailItemID(Integer value) {
        this.detailItemID = value;
    }

    /**
     * Gets the value of the detailItemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailItemType() {
        return detailItemType;
    }

    /**
     * Sets the value of the detailItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailItemType(String value) {
        this.detailItemType = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the generalLedgerAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerAccountNumberCode() {
        return generalLedgerAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerAccountNumberCode(String value) {
        this.generalLedgerAccountNumberCode = value;
    }

    /**
     * Gets the value of the generalLedgerScheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerScheduleCode() {
        return generalLedgerScheduleCode;
    }

    /**
     * Sets the value of the generalLedgerScheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerScheduleCode(String value) {
        this.generalLedgerScheduleCode = value;
    }

    /**
     * Gets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerSubAccountNumberCode() {
        return generalLedgerSubAccountNumberCode;
    }

    /**
     * Sets the value of the generalLedgerSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerSubAccountNumberCode(String value) {
        this.generalLedgerSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the isBankAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBankAccount() {
        return isBankAccount;
    }

    /**
     * Sets the value of the isBankAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBankAccount(Boolean value) {
        this.isBankAccount = value;
    }

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethod(String value) {
        this.method = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDate(XMLGregorianCalendar value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the paymentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentID() {
        return paymentID;
    }

    /**
     * Sets the value of the paymentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentID(String value) {
        this.paymentID = value;
    }

    /**
     * Gets the value of the routingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Sets the value of the routingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoutingNumber(String value) {
        this.routingNumber = value;
    }

    /**
     * Gets the value of the structureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureAgencyCode() {
        return structureAgencyCode;
    }

    /**
     * Sets the value of the structureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureAgencyCode(String value) {
        this.structureAgencyCode = value;
    }

    /**
     * Gets the value of the structureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureBranchCode() {
        return structureBranchCode;
    }

    /**
     * Sets the value of the structureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureBranchCode(String value) {
        this.structureBranchCode = value;
    }

    /**
     * Gets the value of the structureDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureDepartmentCode() {
        return structureDepartmentCode;
    }

    /**
     * Sets the value of the structureDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureDepartmentCode(String value) {
        this.structureDepartmentCode = value;
    }

    /**
     * Gets the value of the structureProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStructureProfitCenterCode() {
        return structureProfitCenterCode;
    }

    /**
     * Sets the value of the structureProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStructureProfitCenterCode(String value) {
        this.structureProfitCenterCode = value;
    }

    /**
     * Gets the value of the applyToPayablesCommissionsSelectPayablesCommissions property.
     * 
     * @return
     *     possible object is
     *     {@link PayablesCommissions }
     *     
     */
    public PayablesCommissions getApplyToPayablesCommissionsSelectPayablesCommissions() {
        return applyToPayablesCommissionsSelectPayablesCommissions;
    }

    /**
     * Sets the value of the applyToPayablesCommissionsSelectPayablesCommissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayablesCommissions }
     *     
     */
    public void setApplyToPayablesCommissionsSelectPayablesCommissions(PayablesCommissions value) {
        this.applyToPayablesCommissionsSelectPayablesCommissions = value;
    }

    /**
     * Gets the value of the paidItems property.
     * 
     * @return
     *     possible object is
     *     {@link PaidItems }
     *     
     */
    public PaidItems getPaidItems() {
        return paidItems;
    }

    /**
     * Sets the value of the paidItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidItems }
     *     
     */
    public void setPaidItems(PaidItems value) {
        this.paidItems = value;
    }

    /**
     * Gets the value of the paidStatements property.
     * 
     * @return
     *     possible object is
     *     {@link PaidStatementItems }
     *     
     */
    public PaidStatementItems getPaidStatements() {
        return paidStatements;
    }

    /**
     * Sets the value of the paidStatements property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaidStatementItems }
     *     
     */
    public void setPaidStatements(PaidStatementItems value) {
        this.paidStatements = value;
    }

    /**
     * Gets the value of the cashOnAccountOptions property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCashOnAccountOptions() {
        return cashOnAccountOptions;
    }

    /**
     * Sets the value of the cashOnAccountOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCashOnAccountOptions(OptionType value) {
        this.cashOnAccountOptions = value;
    }

}
