
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;


/**
 * <p>Java class for LineItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCommissionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionPercent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AgencyCommissionTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgreementID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BillingModeOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="DefaultCommissionAgreement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssuingLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayableContractID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PrefillID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PremiumPayableLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PremiumPayableTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxOptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItem", propOrder = {
    "agencyCommissionAmount",
    "agencyCommissionPercent",
    "agencyCommissionTypeCode",
    "agreementID",
    "billingModeOption",
    "defaultCommissionAgreement",
    "issuingCompanyLookupCode",
    "issuingLocationCode",
    "lineTypeCode",
    "payableContractID",
    "prefillID",
    "premiumPayableLookupCode",
    "premiumPayableTypeCode",
    "profitCenterCode",
    "statusCode",
    "taxOptionCode"
})
public class LineItem {

    @XmlElement(name = "AgencyCommissionAmount", nillable = true)
    protected BigDecimal agencyCommissionAmount;
    @XmlElement(name = "AgencyCommissionPercent", nillable = true)
    protected BigDecimal agencyCommissionPercent;
    @XmlElement(name = "AgencyCommissionTypeCode", nillable = true)
    protected String agencyCommissionTypeCode;
    @XmlElement(name = "AgreementID")
    protected Integer agreementID;
    @XmlElement(name = "BillingModeOption", nillable = true)
    protected OptionType billingModeOption;
    @XmlElement(name = "DefaultCommissionAgreement")
    protected Boolean defaultCommissionAgreement;
    @XmlElement(name = "IssuingCompanyLookupCode", nillable = true)
    protected String issuingCompanyLookupCode;
    @XmlElement(name = "IssuingLocationCode", nillable = true)
    protected String issuingLocationCode;
    @XmlElement(name = "LineTypeCode", nillable = true)
    protected String lineTypeCode;
    @XmlElement(name = "PayableContractID")
    protected Integer payableContractID;
    @XmlElement(name = "PrefillID", nillable = true)
    protected Integer prefillID;
    @XmlElement(name = "PremiumPayableLookupCode", nillable = true)
    protected String premiumPayableLookupCode;
    @XmlElement(name = "PremiumPayableTypeCode", nillable = true)
    protected String premiumPayableTypeCode;
    @XmlElement(name = "ProfitCenterCode", nillable = true)
    protected String profitCenterCode;
    @XmlElement(name = "StatusCode", nillable = true)
    protected String statusCode;
    @XmlElement(name = "TaxOptionCode", nillable = true)
    protected String taxOptionCode;

    /**
     * Gets the value of the agencyCommissionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionAmount() {
        return agencyCommissionAmount;
    }

    /**
     * Sets the value of the agencyCommissionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionAmount(BigDecimal value) {
        this.agencyCommissionAmount = value;
    }

    /**
     * Gets the value of the agencyCommissionPercent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAgencyCommissionPercent() {
        return agencyCommissionPercent;
    }

    /**
     * Sets the value of the agencyCommissionPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAgencyCommissionPercent(BigDecimal value) {
        this.agencyCommissionPercent = value;
    }

    /**
     * Gets the value of the agencyCommissionTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCommissionTypeCode() {
        return agencyCommissionTypeCode;
    }

    /**
     * Sets the value of the agencyCommissionTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCommissionTypeCode(String value) {
        this.agencyCommissionTypeCode = value;
    }

    /**
     * Gets the value of the agreementID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgreementID() {
        return agreementID;
    }

    /**
     * Sets the value of the agreementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgreementID(Integer value) {
        this.agreementID = value;
    }

    /**
     * Gets the value of the billingModeOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBillingModeOption() {
        return billingModeOption;
    }

    /**
     * Sets the value of the billingModeOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBillingModeOption(OptionType value) {
        this.billingModeOption = value;
    }

    /**
     * Gets the value of the defaultCommissionAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultCommissionAgreement() {
        return defaultCommissionAgreement;
    }

    /**
     * Sets the value of the defaultCommissionAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultCommissionAgreement(Boolean value) {
        this.defaultCommissionAgreement = value;
    }

    /**
     * Gets the value of the issuingCompanyLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyLookupCode() {
        return issuingCompanyLookupCode;
    }

    /**
     * Sets the value of the issuingCompanyLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyLookupCode(String value) {
        this.issuingCompanyLookupCode = value;
    }

    /**
     * Gets the value of the issuingLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingLocationCode() {
        return issuingLocationCode;
    }

    /**
     * Sets the value of the issuingLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingLocationCode(String value) {
        this.issuingLocationCode = value;
    }

    /**
     * Gets the value of the lineTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineTypeCode() {
        return lineTypeCode;
    }

    /**
     * Sets the value of the lineTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineTypeCode(String value) {
        this.lineTypeCode = value;
    }

    /**
     * Gets the value of the payableContractID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayableContractID() {
        return payableContractID;
    }

    /**
     * Sets the value of the payableContractID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayableContractID(Integer value) {
        this.payableContractID = value;
    }

    /**
     * Gets the value of the prefillID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrefillID() {
        return prefillID;
    }

    /**
     * Sets the value of the prefillID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrefillID(Integer value) {
        this.prefillID = value;
    }

    /**
     * Gets the value of the premiumPayableLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableLookupCode() {
        return premiumPayableLookupCode;
    }

    /**
     * Sets the value of the premiumPayableLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableLookupCode(String value) {
        this.premiumPayableLookupCode = value;
    }

    /**
     * Gets the value of the premiumPayableTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayableTypeCode() {
        return premiumPayableTypeCode;
    }

    /**
     * Sets the value of the premiumPayableTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayableTypeCode(String value) {
        this.premiumPayableTypeCode = value;
    }

    /**
     * Gets the value of the profitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCenterCode() {
        return profitCenterCode;
    }

    /**
     * Sets the value of the profitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCenterCode(String value) {
        this.profitCenterCode = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the taxOptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxOptionCode() {
        return taxOptionCode;
    }

    /**
     * Sets the value of the taxOptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxOptionCode(String value) {
        this.taxOptionCode = value;
    }

}
