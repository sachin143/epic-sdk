
package com.appliedsystems.schemas.epic.sdk._2011._01._common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.CommissionSplitItems;


/**
 * <p>Java class for AdjustCommission complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdjustCommission">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommissionSplits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/}CommissionSplitItems" minOccurs="0"/>
 *         &lt;element name="IgnoreAgencyCommissionLessThanProducerBroker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IgnoreFlatCommission" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IgnoreInstallmentsThatCannotBeUpdated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UpdateInstallmentsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="IgnoreAccountingMonthVerification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdjustCommission", propOrder = {
    "associatedAccountID",
    "associatedAccountTypeCode",
    "commissionSplits",
    "ignoreAgencyCommissionLessThanProducerBroker",
    "ignoreFlatCommission",
    "transactionID",
    "ignoreInstallmentsThatCannotBeUpdated",
    "updateInstallmentsOption",
    "ignoreAccountingMonthVerification"
})
public class AdjustCommission {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "CommissionSplits", nillable = true)
    protected CommissionSplitItems commissionSplits;
    @XmlElement(name = "IgnoreAgencyCommissionLessThanProducerBroker")
    protected Boolean ignoreAgencyCommissionLessThanProducerBroker;
    @XmlElement(name = "IgnoreFlatCommission")
    protected Boolean ignoreFlatCommission;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "IgnoreInstallmentsThatCannotBeUpdated")
    protected Boolean ignoreInstallmentsThatCannotBeUpdated;
    @XmlElement(name = "UpdateInstallmentsOption", nillable = true)
    protected OptionType updateInstallmentsOption;
    @XmlElement(name = "IgnoreAccountingMonthVerification")
    protected Boolean ignoreAccountingMonthVerification;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the commissionSplits property.
     * 
     * @return
     *     possible object is
     *     {@link CommissionSplitItems }
     *     
     */
    public CommissionSplitItems getCommissionSplits() {
        return commissionSplits;
    }

    /**
     * Sets the value of the commissionSplits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionSplitItems }
     *     
     */
    public void setCommissionSplits(CommissionSplitItems value) {
        this.commissionSplits = value;
    }

    /**
     * Gets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAgencyCommissionLessThanProducerBroker() {
        return ignoreAgencyCommissionLessThanProducerBroker;
    }

    /**
     * Sets the value of the ignoreAgencyCommissionLessThanProducerBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAgencyCommissionLessThanProducerBroker(Boolean value) {
        this.ignoreAgencyCommissionLessThanProducerBroker = value;
    }

    /**
     * Gets the value of the ignoreFlatCommission property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreFlatCommission() {
        return ignoreFlatCommission;
    }

    /**
     * Sets the value of the ignoreFlatCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreFlatCommission(Boolean value) {
        this.ignoreFlatCommission = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the ignoreInstallmentsThatCannotBeUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreInstallmentsThatCannotBeUpdated() {
        return ignoreInstallmentsThatCannotBeUpdated;
    }

    /**
     * Sets the value of the ignoreInstallmentsThatCannotBeUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreInstallmentsThatCannotBeUpdated(Boolean value) {
        this.ignoreInstallmentsThatCannotBeUpdated = value;
    }

    /**
     * Gets the value of the updateInstallmentsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getUpdateInstallmentsOption() {
        return updateInstallmentsOption;
    }

    /**
     * Sets the value of the updateInstallmentsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setUpdateInstallmentsOption(OptionType value) {
        this.updateInstallmentsOption = value;
    }

    /**
     * Gets the value of the ignoreAccountingMonthVerification property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAccountingMonthVerification() {
        return ignoreAccountingMonthVerification;
    }

    /**
     * Sets the value of the ignoreAccountingMonthVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAccountingMonthVerification(Boolean value) {
        this.ignoreAccountingMonthVerification = value;
    }

}
