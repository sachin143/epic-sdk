
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityGetLimitType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityGetLimitType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Open"/>
 *     &lt;enumeration value="LastSixMonths"/>
 *     &lt;enumeration value="Search"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityGetLimitType")
@XmlEnum
public enum ActivityGetLimitType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("LastSixMonths")
    LAST_SIX_MONTHS("LastSixMonths"),
    @XmlEnumValue("Search")
    SEARCH("Search");
    private final String value;

    ActivityGetLimitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityGetLimitType fromValue(String v) {
        for (ActivityGetLimitType c: ActivityGetLimitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
