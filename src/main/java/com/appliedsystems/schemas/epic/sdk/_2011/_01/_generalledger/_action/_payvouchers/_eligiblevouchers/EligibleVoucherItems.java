
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EligibleVoucherItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EligibleVoucherItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EligibleVoucherItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/_payvouchers/_eligiblevouchers/}EligibleVoucherItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EligibleVoucherItems", propOrder = {
    "eligibleVoucherItems"
})
public class EligibleVoucherItems {

    @XmlElement(name = "EligibleVoucherItem", nillable = true)
    protected List<EligibleVoucherItem> eligibleVoucherItems;

    /**
     * Gets the value of the eligibleVoucherItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eligibleVoucherItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEligibleVoucherItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EligibleVoucherItem }
     * 
     * 
     */
    public List<EligibleVoucherItem> getEligibleVoucherItems() {
        if (eligibleVoucherItems == null) {
            eligibleVoucherItems = new ArrayList<EligibleVoucherItem>();
        }
        return this.eligibleVoucherItems;
    }

}
