
package com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SortType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SortType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DefaultOrder"/>
 *     &lt;enumeration value="AccountLookupCode"/>
 *     &lt;enumeration value="ActivityCode"/>
 *     &lt;enumeration value="EnteredDate"/>
 *     &lt;enumeration value="FollowUpStartDate"/>
 *     &lt;enumeration value="WhoOwnerCode"/>
 *     &lt;enumeration value="ActivityStatus"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SortType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_activityfilter/")
@XmlEnum
public enum SortType {

    @XmlEnumValue("DefaultOrder")
    DEFAULT_ORDER("DefaultOrder"),
    @XmlEnumValue("AccountLookupCode")
    ACCOUNT_LOOKUP_CODE("AccountLookupCode"),
    @XmlEnumValue("ActivityCode")
    ACTIVITY_CODE("ActivityCode"),
    @XmlEnumValue("EnteredDate")
    ENTERED_DATE("EnteredDate"),
    @XmlEnumValue("FollowUpStartDate")
    FOLLOW_UP_START_DATE("FollowUpStartDate"),
    @XmlEnumValue("WhoOwnerCode")
    WHO_OWNER_CODE("WhoOwnerCode"),
    @XmlEnumValue("ActivityStatus")
    ACTIVITY_STATUS("ActivityStatus");
    private final String value;

    SortType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SortType fromValue(String v) {
        for (SortType c: SortType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
