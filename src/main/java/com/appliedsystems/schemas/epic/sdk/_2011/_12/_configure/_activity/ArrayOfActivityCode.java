
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfActivityCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfActivityCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivityCode" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/}ActivityCode" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfActivityCode", propOrder = {
    "activityCodes"
})
public class ArrayOfActivityCode {

    @XmlElement(name = "ActivityCode", nillable = true)
    protected List<ActivityCode> activityCodes;

    /**
     * Gets the value of the activityCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activityCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivityCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivityCode }
     * 
     * 
     */
    public List<ActivityCode> getActivityCodes() {
        if (activityCodes == null) {
            activityCodes = new ArrayList<ActivityCode>();
        }
        return this.activityCodes;
    }

}
