
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.CriteriaItem;


/**
 * <p>Java class for Summary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Summary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountingMonthCriteria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="BranchCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="BrokerExternalInternalBothCriteriaOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="CompanyBrokerOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="CreateSeparateStatements" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DefaultAsTransactionDescription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DepartmentCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="DirectBillCommissionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntityCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="LineOfBusinessCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="MasterStatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitCenterCriteriaValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/}CriteriaItem" minOccurs="0"/>
 *         &lt;element name="RecordReconcileCommissionsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="StatementDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementOwnerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Summary", propOrder = {
    "accountingMonth",
    "accountingMonthCriteria",
    "agencyCriteriaValue",
    "branchCriteriaValue",
    "brokerExternalInternalBothCriteriaOption",
    "companyBrokerOption",
    "createSeparateStatements",
    "defaultAsTransactionDescription",
    "departmentCriteriaValue",
    "directBillCommissionDescription",
    "entityCriteriaValue",
    "issuingCompanyCriteriaValue",
    "lineOfBusinessCriteriaValue",
    "masterStatementNumber",
    "profitCenterCriteriaValue",
    "recordReconcileCommissionsOption",
    "statementDescription",
    "statementNumber",
    "statementOwnerCode"
})
public class Summary {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "AccountingMonthCriteria", nillable = true)
    protected String accountingMonthCriteria;
    @XmlElement(name = "AgencyCriteriaValue", nillable = true)
    protected CriteriaItem agencyCriteriaValue;
    @XmlElement(name = "BranchCriteriaValue", nillable = true)
    protected CriteriaItem branchCriteriaValue;
    @XmlElement(name = "BrokerExternalInternalBothCriteriaOption", nillable = true)
    protected OptionType brokerExternalInternalBothCriteriaOption;
    @XmlElement(name = "CompanyBrokerOption", nillable = true)
    protected OptionType companyBrokerOption;
    @XmlElement(name = "CreateSeparateStatements")
    protected Boolean createSeparateStatements;
    @XmlElement(name = "DefaultAsTransactionDescription")
    protected Boolean defaultAsTransactionDescription;
    @XmlElement(name = "DepartmentCriteriaValue", nillable = true)
    protected CriteriaItem departmentCriteriaValue;
    @XmlElement(name = "DirectBillCommissionDescription", nillable = true)
    protected String directBillCommissionDescription;
    @XmlElement(name = "EntityCriteriaValue", nillable = true)
    protected CriteriaItem entityCriteriaValue;
    @XmlElement(name = "IssuingCompanyCriteriaValue", nillable = true)
    protected CriteriaItem issuingCompanyCriteriaValue;
    @XmlElement(name = "LineOfBusinessCriteriaValue", nillable = true)
    protected CriteriaItem lineOfBusinessCriteriaValue;
    @XmlElement(name = "MasterStatementNumber", nillable = true)
    protected String masterStatementNumber;
    @XmlElement(name = "ProfitCenterCriteriaValue", nillable = true)
    protected CriteriaItem profitCenterCriteriaValue;
    @XmlElement(name = "RecordReconcileCommissionsOption", nillable = true)
    protected OptionType recordReconcileCommissionsOption;
    @XmlElement(name = "StatementDescription", nillable = true)
    protected String statementDescription;
    @XmlElement(name = "StatementNumber", nillable = true)
    protected String statementNumber;
    @XmlElement(name = "StatementOwnerCode", nillable = true)
    protected String statementOwnerCode;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the accountingMonthCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonthCriteria() {
        return accountingMonthCriteria;
    }

    /**
     * Sets the value of the accountingMonthCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonthCriteria(String value) {
        this.accountingMonthCriteria = value;
    }

    /**
     * Gets the value of the agencyCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getAgencyCriteriaValue() {
        return agencyCriteriaValue;
    }

    /**
     * Sets the value of the agencyCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setAgencyCriteriaValue(CriteriaItem value) {
        this.agencyCriteriaValue = value;
    }

    /**
     * Gets the value of the branchCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getBranchCriteriaValue() {
        return branchCriteriaValue;
    }

    /**
     * Sets the value of the branchCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setBranchCriteriaValue(CriteriaItem value) {
        this.branchCriteriaValue = value;
    }

    /**
     * Gets the value of the brokerExternalInternalBothCriteriaOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getBrokerExternalInternalBothCriteriaOption() {
        return brokerExternalInternalBothCriteriaOption;
    }

    /**
     * Sets the value of the brokerExternalInternalBothCriteriaOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setBrokerExternalInternalBothCriteriaOption(OptionType value) {
        this.brokerExternalInternalBothCriteriaOption = value;
    }

    /**
     * Gets the value of the companyBrokerOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getCompanyBrokerOption() {
        return companyBrokerOption;
    }

    /**
     * Sets the value of the companyBrokerOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setCompanyBrokerOption(OptionType value) {
        this.companyBrokerOption = value;
    }

    /**
     * Gets the value of the createSeparateStatements property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCreateSeparateStatements() {
        return createSeparateStatements;
    }

    /**
     * Sets the value of the createSeparateStatements property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreateSeparateStatements(Boolean value) {
        this.createSeparateStatements = value;
    }

    /**
     * Gets the value of the defaultAsTransactionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultAsTransactionDescription() {
        return defaultAsTransactionDescription;
    }

    /**
     * Sets the value of the defaultAsTransactionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultAsTransactionDescription(Boolean value) {
        this.defaultAsTransactionDescription = value;
    }

    /**
     * Gets the value of the departmentCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getDepartmentCriteriaValue() {
        return departmentCriteriaValue;
    }

    /**
     * Sets the value of the departmentCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setDepartmentCriteriaValue(CriteriaItem value) {
        this.departmentCriteriaValue = value;
    }

    /**
     * Gets the value of the directBillCommissionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectBillCommissionDescription() {
        return directBillCommissionDescription;
    }

    /**
     * Sets the value of the directBillCommissionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectBillCommissionDescription(String value) {
        this.directBillCommissionDescription = value;
    }

    /**
     * Gets the value of the entityCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getEntityCriteriaValue() {
        return entityCriteriaValue;
    }

    /**
     * Sets the value of the entityCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setEntityCriteriaValue(CriteriaItem value) {
        this.entityCriteriaValue = value;
    }

    /**
     * Gets the value of the issuingCompanyCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getIssuingCompanyCriteriaValue() {
        return issuingCompanyCriteriaValue;
    }

    /**
     * Sets the value of the issuingCompanyCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setIssuingCompanyCriteriaValue(CriteriaItem value) {
        this.issuingCompanyCriteriaValue = value;
    }

    /**
     * Gets the value of the lineOfBusinessCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getLineOfBusinessCriteriaValue() {
        return lineOfBusinessCriteriaValue;
    }

    /**
     * Sets the value of the lineOfBusinessCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setLineOfBusinessCriteriaValue(CriteriaItem value) {
        this.lineOfBusinessCriteriaValue = value;
    }

    /**
     * Gets the value of the masterStatementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterStatementNumber() {
        return masterStatementNumber;
    }

    /**
     * Sets the value of the masterStatementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterStatementNumber(String value) {
        this.masterStatementNumber = value;
    }

    /**
     * Gets the value of the profitCenterCriteriaValue property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaItem }
     *     
     */
    public CriteriaItem getProfitCenterCriteriaValue() {
        return profitCenterCriteriaValue;
    }

    /**
     * Sets the value of the profitCenterCriteriaValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaItem }
     *     
     */
    public void setProfitCenterCriteriaValue(CriteriaItem value) {
        this.profitCenterCriteriaValue = value;
    }

    /**
     * Gets the value of the recordReconcileCommissionsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getRecordReconcileCommissionsOption() {
        return recordReconcileCommissionsOption;
    }

    /**
     * Sets the value of the recordReconcileCommissionsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setRecordReconcileCommissionsOption(OptionType value) {
        this.recordReconcileCommissionsOption = value;
    }

    /**
     * Gets the value of the statementDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementDescription() {
        return statementDescription;
    }

    /**
     * Sets the value of the statementDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementDescription(String value) {
        this.statementDescription = value;
    }

    /**
     * Gets the value of the statementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementNumber() {
        return statementNumber;
    }

    /**
     * Sets the value of the statementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementNumber(String value) {
        this.statementNumber = value;
    }

    /**
     * Gets the value of the statementOwnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementOwnerCode() {
        return statementOwnerCode;
    }

    /**
     * Sets the value of the statementOwnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementOwnerCode(String value) {
        this.statementOwnerCode = value;
    }

}
