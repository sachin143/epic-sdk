
package com.appliedsystems.schemas.epic.sdk._2009._07._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfSummary;


/**
 * <p>Java class for ClaimSummaryGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimSummaryGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimSummaries" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}ArrayOfSummary" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimSummaryGetResult", propOrder = {
    "claimSummaries",
    "totalPages"
})
public class ClaimSummaryGetResult {

    @XmlElement(name = "ClaimSummaries", nillable = true)
    protected ArrayOfSummary claimSummaries;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the claimSummaries property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSummary }
     *     
     */
    public ArrayOfSummary getClaimSummaries() {
        return claimSummaries;
    }

    /**
     * Sets the value of the claimSummaries property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSummary }
     *     
     */
    public void setClaimSummaries(ArrayOfSummary value) {
        this.claimSummaries = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
