
package com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgencyStructureItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "AgencyStructureItem");
    private final static QName _FileDetailItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "FileDetailItem");
    private final static QName _FileDetailItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "FileDetailItems");
    private final static QName _AttachedToItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "AttachedToItem");
    private final static QName _AgencyStructureItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "AgencyStructureItems");
    private final static QName _AttachedToItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", "AttachedToItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AttachedToItems }
     * 
     */
    public AttachedToItems createAttachedToItems() {
        return new AttachedToItems();
    }

    /**
     * Create an instance of {@link AgencyStructureItems }
     * 
     */
    public AgencyStructureItems createAgencyStructureItems() {
        return new AgencyStructureItems();
    }

    /**
     * Create an instance of {@link FileDetailItems }
     * 
     */
    public FileDetailItems createFileDetailItems() {
        return new FileDetailItems();
    }

    /**
     * Create an instance of {@link AttachedToItem }
     * 
     */
    public AttachedToItem createAttachedToItem() {
        return new AttachedToItem();
    }

    /**
     * Create an instance of {@link FileDetailItem }
     * 
     */
    public FileDetailItem createFileDetailItem() {
        return new FileDetailItem();
    }

    /**
     * Create an instance of {@link AgencyStructureItem }
     * 
     */
    public AgencyStructureItem createAgencyStructureItem() {
        return new AgencyStructureItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyStructureItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "AgencyStructureItem")
    public JAXBElement<AgencyStructureItem> createAgencyStructureItem(AgencyStructureItem value) {
        return new JAXBElement<AgencyStructureItem>(_AgencyStructureItem_QNAME, AgencyStructureItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileDetailItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "FileDetailItem")
    public JAXBElement<FileDetailItem> createFileDetailItem(FileDetailItem value) {
        return new JAXBElement<FileDetailItem>(_FileDetailItem_QNAME, FileDetailItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileDetailItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "FileDetailItems")
    public JAXBElement<FileDetailItems> createFileDetailItems(FileDetailItems value) {
        return new JAXBElement<FileDetailItems>(_FileDetailItems_QNAME, FileDetailItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachedToItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "AttachedToItem")
    public JAXBElement<AttachedToItem> createAttachedToItem(AttachedToItem value) {
        return new JAXBElement<AttachedToItem>(_AttachedToItem_QNAME, AttachedToItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyStructureItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "AgencyStructureItems")
    public JAXBElement<AgencyStructureItems> createAgencyStructureItems(AgencyStructureItems value) {
        return new JAXBElement<AgencyStructureItems>(_AgencyStructureItems_QNAME, AgencyStructureItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachedToItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_attachment/", name = "AttachedToItems")
    public JAXBElement<AttachedToItems> createAttachedToItems(AttachedToItems value) {
        return new JAXBElement<AttachedToItems>(_AttachedToItems_QNAME, AttachedToItems.class, null, value);
    }

}
