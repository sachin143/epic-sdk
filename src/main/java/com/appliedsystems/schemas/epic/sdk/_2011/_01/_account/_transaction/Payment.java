
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerCleared" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PaymentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentReceivedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ProductionMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceivingAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceivingBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReversalVoidItemReversalNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReversalVoidReasonDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReversalVoidReasonID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ReversalVoidReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payment", propOrder = {
    "accountingMonth",
    "agencyCode",
    "branchCode",
    "departmentCode",
    "generalLedgerCleared",
    "generalLedgerReferNumber",
    "methodID",
    "paymentDate",
    "paymentID",
    "paymentReceivedDate",
    "productionMonth",
    "profitCenterCode",
    "receivingAgencyCode",
    "receivingBranchCode",
    "reversalVoidItemReversalNumber",
    "reversalVoidReasonDetails",
    "reversalVoidReasonID",
    "reversalVoidReferNumber"
})
public class Payment {

    @XmlElement(name = "AccountingMonth", nillable = true)
    protected String accountingMonth;
    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "BranchCode", nillable = true)
    protected String branchCode;
    @XmlElement(name = "DepartmentCode", nillable = true)
    protected String departmentCode;
    @XmlElement(name = "GeneralLedgerCleared", nillable = true)
    protected String generalLedgerCleared;
    @XmlElement(name = "GeneralLedgerReferNumber", nillable = true)
    protected String generalLedgerReferNumber;
    @XmlElement(name = "MethodID", nillable = true)
    protected Integer methodID;
    @XmlElement(name = "PaymentDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentDate;
    @XmlElement(name = "PaymentID", nillable = true)
    protected String paymentID;
    @XmlElement(name = "PaymentReceivedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentReceivedDate;
    @XmlElement(name = "ProductionMonth", nillable = true)
    protected String productionMonth;
    @XmlElement(name = "ProfitCenterCode", nillable = true)
    protected String profitCenterCode;
    @XmlElement(name = "ReceivingAgencyCode", nillable = true)
    protected String receivingAgencyCode;
    @XmlElement(name = "ReceivingBranchCode", nillable = true)
    protected String receivingBranchCode;
    @XmlElement(name = "ReversalVoidItemReversalNumber", nillable = true)
    protected String reversalVoidItemReversalNumber;
    @XmlElement(name = "ReversalVoidReasonDetails", nillable = true)
    protected String reversalVoidReasonDetails;
    @XmlElement(name = "ReversalVoidReasonID", nillable = true)
    protected Integer reversalVoidReasonID;
    @XmlElement(name = "ReversalVoidReferNumber", nillable = true)
    protected String reversalVoidReferNumber;

    /**
     * Gets the value of the accountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountingMonth() {
        return accountingMonth;
    }

    /**
     * Sets the value of the accountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountingMonth(String value) {
        this.accountingMonth = value;
    }

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the departmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /**
     * Sets the value of the departmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentCode(String value) {
        this.departmentCode = value;
    }

    /**
     * Gets the value of the generalLedgerCleared property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerCleared() {
        return generalLedgerCleared;
    }

    /**
     * Sets the value of the generalLedgerCleared property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerCleared(String value) {
        this.generalLedgerCleared = value;
    }

    /**
     * Gets the value of the generalLedgerReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralLedgerReferNumber() {
        return generalLedgerReferNumber;
    }

    /**
     * Sets the value of the generalLedgerReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralLedgerReferNumber(String value) {
        this.generalLedgerReferNumber = value;
    }

    /**
     * Gets the value of the methodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMethodID() {
        return methodID;
    }

    /**
     * Sets the value of the methodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMethodID(Integer value) {
        this.methodID = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDate(XMLGregorianCalendar value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the paymentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentID() {
        return paymentID;
    }

    /**
     * Sets the value of the paymentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentID(String value) {
        this.paymentID = value;
    }

    /**
     * Gets the value of the paymentReceivedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentReceivedDate() {
        return paymentReceivedDate;
    }

    /**
     * Sets the value of the paymentReceivedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentReceivedDate(XMLGregorianCalendar value) {
        this.paymentReceivedDate = value;
    }

    /**
     * Gets the value of the productionMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionMonth() {
        return productionMonth;
    }

    /**
     * Sets the value of the productionMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionMonth(String value) {
        this.productionMonth = value;
    }

    /**
     * Gets the value of the profitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCenterCode() {
        return profitCenterCode;
    }

    /**
     * Sets the value of the profitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCenterCode(String value) {
        this.profitCenterCode = value;
    }

    /**
     * Gets the value of the receivingAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingAgencyCode() {
        return receivingAgencyCode;
    }

    /**
     * Sets the value of the receivingAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingAgencyCode(String value) {
        this.receivingAgencyCode = value;
    }

    /**
     * Gets the value of the receivingBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingBranchCode() {
        return receivingBranchCode;
    }

    /**
     * Sets the value of the receivingBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingBranchCode(String value) {
        this.receivingBranchCode = value;
    }

    /**
     * Gets the value of the reversalVoidItemReversalNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReversalVoidItemReversalNumber() {
        return reversalVoidItemReversalNumber;
    }

    /**
     * Sets the value of the reversalVoidItemReversalNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReversalVoidItemReversalNumber(String value) {
        this.reversalVoidItemReversalNumber = value;
    }

    /**
     * Gets the value of the reversalVoidReasonDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReversalVoidReasonDetails() {
        return reversalVoidReasonDetails;
    }

    /**
     * Sets the value of the reversalVoidReasonDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReversalVoidReasonDetails(String value) {
        this.reversalVoidReasonDetails = value;
    }

    /**
     * Gets the value of the reversalVoidReasonID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReversalVoidReasonID() {
        return reversalVoidReasonID;
    }

    /**
     * Sets the value of the reversalVoidReasonID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReversalVoidReasonID(Integer value) {
        this.reversalVoidReasonID = value;
    }

    /**
     * Gets the value of the reversalVoidReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReversalVoidReferNumber() {
        return reversalVoidReferNumber;
    }

    /**
     * Sets the value of the reversalVoidReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReversalVoidReferNumber(String value) {
        this.reversalVoidReferNumber = value;
    }

}
