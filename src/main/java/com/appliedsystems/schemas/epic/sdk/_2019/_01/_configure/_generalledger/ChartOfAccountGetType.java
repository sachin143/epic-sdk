
package com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChartOfAccountGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChartOfAccountGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ChartOfAccountID"/>
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Asset"/>
 *     &lt;enumeration value="Liability"/>
 *     &lt;enumeration value="Equity"/>
 *     &lt;enumeration value="Income"/>
 *     &lt;enumeration value="Expense"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChartOfAccountGetType")
@XmlEnum
public enum ChartOfAccountGetType {

    @XmlEnumValue("ChartOfAccountID")
    CHART_OF_ACCOUNT_ID("ChartOfAccountID"),
    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Asset")
    ASSET("Asset"),
    @XmlEnumValue("Liability")
    LIABILITY("Liability"),
    @XmlEnumValue("Equity")
    EQUITY("Equity"),
    @XmlEnumValue("Income")
    INCOME("Income"),
    @XmlEnumValue("Expense")
    EXPENSE("Expense");
    private final String value;

    ChartOfAccountGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChartOfAccountGetType fromValue(String v) {
        for (ChartOfAccountGetType c: ChartOfAccountGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
