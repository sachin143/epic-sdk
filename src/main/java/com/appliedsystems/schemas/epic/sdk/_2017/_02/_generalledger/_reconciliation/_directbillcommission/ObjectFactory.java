
package com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RecordDetailItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/", "RecordDetailItems");
    private final static QName _RecordDetailItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/", "RecordDetailItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecordDetailItems }
     * 
     */
    public RecordDetailItems createRecordDetailItems() {
        return new RecordDetailItems();
    }

    /**
     * Create an instance of {@link RecordDetailItem }
     * 
     */
    public RecordDetailItem createRecordDetailItem() {
        return new RecordDetailItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecordDetailItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/", name = "RecordDetailItems")
    public JAXBElement<RecordDetailItems> createRecordDetailItems(RecordDetailItems value) {
        return new JAXBElement<RecordDetailItems>(_RecordDetailItems_QNAME, RecordDetailItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecordDetailItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/", name = "RecordDetailItem")
    public JAXBElement<RecordDetailItem> createRecordDetailItem(RecordDetailItem value) {
        return new JAXBElement<RecordDetailItem>(_RecordDetailItem_QNAME, RecordDetailItem.class, null, value);
    }

}
