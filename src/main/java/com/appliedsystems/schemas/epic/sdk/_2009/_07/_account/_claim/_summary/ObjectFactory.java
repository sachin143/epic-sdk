
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReportingHistory_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", "ReportingHistory");
    private final static QName _PolicyItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", "PolicyItem");
    private final static QName _PolicyItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", "PolicyItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReportingHistory }
     * 
     */
    public ReportingHistory createReportingHistory() {
        return new ReportingHistory();
    }

    /**
     * Create an instance of {@link PolicyItems }
     * 
     */
    public PolicyItems createPolicyItems() {
        return new PolicyItems();
    }

    /**
     * Create an instance of {@link PolicyItem }
     * 
     */
    public PolicyItem createPolicyItem() {
        return new PolicyItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReportingHistory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", name = "ReportingHistory")
    public JAXBElement<ReportingHistory> createReportingHistory(ReportingHistory value) {
        return new JAXBElement<ReportingHistory>(_ReportingHistory_QNAME, ReportingHistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", name = "PolicyItem")
    public JAXBElement<PolicyItem> createPolicyItem(PolicyItem value) {
        return new JAXBElement<PolicyItem>(_PolicyItem_QNAME, PolicyItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/", name = "PolicyItems")
    public JAXBElement<PolicyItems> createPolicyItems(PolicyItems value) {
        return new JAXBElement<PolicyItems>(_PolicyItems_QNAME, PolicyItems.class, null, value);
    }

}
