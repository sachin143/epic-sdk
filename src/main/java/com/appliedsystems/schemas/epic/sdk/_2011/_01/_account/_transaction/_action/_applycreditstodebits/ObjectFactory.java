
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DebitItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", "DebitItems");
    private final static QName _CreditItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", "CreditItems");
    private final static QName _CreditItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", "CreditItem");
    private final static QName _DebitItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", "DebitItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DebitItems }
     * 
     */
    public DebitItems createDebitItems() {
        return new DebitItems();
    }

    /**
     * Create an instance of {@link CreditItem }
     * 
     */
    public CreditItem createCreditItem() {
        return new CreditItem();
    }

    /**
     * Create an instance of {@link DebitItem }
     * 
     */
    public DebitItem createDebitItem() {
        return new DebitItem();
    }

    /**
     * Create an instance of {@link CreditItems }
     * 
     */
    public CreditItems createCreditItems() {
        return new CreditItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DebitItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", name = "DebitItems")
    public JAXBElement<DebitItems> createDebitItems(DebitItems value) {
        return new JAXBElement<DebitItems>(_DebitItems_QNAME, DebitItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", name = "CreditItems")
    public JAXBElement<CreditItems> createCreditItems(CreditItems value) {
        return new JAXBElement<CreditItems>(_CreditItems_QNAME, CreditItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", name = "CreditItem")
    public JAXBElement<CreditItem> createCreditItem(CreditItem value) {
        return new JAXBElement<CreditItem>(_CreditItem_QNAME, CreditItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DebitItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/", name = "DebitItem")
    public JAXBElement<DebitItem> createDebitItem(DebitItem value) {
        return new JAXBElement<DebitItem>(_DebitItem_QNAME, DebitItem.class, null, value);
    }

}
