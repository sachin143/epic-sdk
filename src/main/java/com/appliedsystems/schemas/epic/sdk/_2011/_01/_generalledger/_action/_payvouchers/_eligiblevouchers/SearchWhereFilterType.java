
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchWhereFilterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SearchWhereFilterType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AccountLookup"/>
 *     &lt;enumeration value="AccountingMonth"/>
 *     &lt;enumeration value="DiscountDate"/>
 *     &lt;enumeration value="DueDate"/>
 *     &lt;enumeration value="InvoiceDate"/>
 *     &lt;enumeration value="Payee"/>
 *     &lt;enumeration value="ReferNumber"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SearchWhereFilterType")
@XmlEnum
public enum SearchWhereFilterType {

    @XmlEnumValue("AccountLookup")
    ACCOUNT_LOOKUP("AccountLookup"),
    @XmlEnumValue("AccountingMonth")
    ACCOUNTING_MONTH("AccountingMonth"),
    @XmlEnumValue("DiscountDate")
    DISCOUNT_DATE("DiscountDate"),
    @XmlEnumValue("DueDate")
    DUE_DATE("DueDate"),
    @XmlEnumValue("InvoiceDate")
    INVOICE_DATE("InvoiceDate"),
    @XmlEnumValue("Payee")
    PAYEE("Payee"),
    @XmlEnumValue("ReferNumber")
    REFER_NUMBER("ReferNumber");
    private final String value;

    SearchWhereFilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchWhereFilterType fromValue(String v) {
        for (SearchWhereFilterType c: SearchWhereFilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
