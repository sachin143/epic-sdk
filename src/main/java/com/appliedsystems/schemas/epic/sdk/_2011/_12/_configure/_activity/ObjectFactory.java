
package com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfActivityCode_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/", "ArrayOfActivityCode");
    private final static QName _ActivityCode_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/", "ActivityCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivityCode }
     * 
     */
    public ActivityCode createActivityCode() {
        return new ActivityCode();
    }

    /**
     * Create an instance of {@link ArrayOfActivityCode }
     * 
     */
    public ArrayOfActivityCode createArrayOfActivityCode() {
        return new ArrayOfActivityCode();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfActivityCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/", name = "ArrayOfActivityCode")
    public JAXBElement<ArrayOfActivityCode> createArrayOfActivityCode(ArrayOfActivityCode value) {
        return new JAXBElement<ArrayOfActivityCode>(_ArrayOfActivityCode_QNAME, ArrayOfActivityCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/", name = "ActivityCode")
    public JAXBElement<ActivityCode> createActivityCode(ActivityCode value) {
        return new JAXBElement<ActivityCode>(_ActivityCode_QNAME, ActivityCode.class, null, value);
    }

}
