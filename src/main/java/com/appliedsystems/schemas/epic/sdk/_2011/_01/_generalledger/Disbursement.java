
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.Detail;


/**
 * <p>Java class for Disbursement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Disbursement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckLastPrintedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CheckMemo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CheckPreviousReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckPrint" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CheckRemittance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/}Detail" minOccurs="0"/>
 *         &lt;element name="DisbursementAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DisbursementDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DisbursementID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DisbursementRecurringEntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreDuplicatePayToTheOrderOfInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddress" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfMailingAddressSiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfPayee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayToTheOrderOfPayeeContactID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="VoidDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoidVoided" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckIncludeCheckStubDetail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CheckRouting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DirectDepositMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Disbursement", propOrder = {
    "checkLastPrintedDate",
    "checkMemo",
    "checkNumber",
    "checkPreviousReferNumber",
    "checkPrint",
    "checkRemittance",
    "detailValue",
    "disbursementAccountingMonth",
    "disbursementBankAccountNumberCode",
    "disbursementBankSubAccountNumberCode",
    "disbursementDefaultEntryID",
    "disbursementDescription",
    "disbursementEffectiveDate",
    "disbursementID",
    "disbursementRecurringEntry",
    "disbursementReferNumber",
    "ignoreDuplicatePayToTheOrderOfInvoiceNumber",
    "isReadOnly",
    "payToTheOrderOfAccountLookupCode",
    "payToTheOrderOfAccountNumber",
    "payToTheOrderOfAccountType",
    "payToTheOrderOfInvoiceDate",
    "payToTheOrderOfInvoiceNumber",
    "payToTheOrderOfMailingAddress",
    "payToTheOrderOfMailingAddressContact",
    "payToTheOrderOfMailingAddressContactID",
    "payToTheOrderOfMailingAddressSiteID",
    "payToTheOrderOfPayee",
    "payToTheOrderOfPayeeContactID",
    "timestamp",
    "voidDetails",
    "voidReason",
    "voidReferNumber",
    "voidVoided",
    "checkComments",
    "checkIncludeCheckStubDetail",
    "checkRouting",
    "directDepositMethodID"
})
public class Disbursement {

    @XmlElement(name = "CheckLastPrintedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar checkLastPrintedDate;
    @XmlElement(name = "CheckMemo", nillable = true)
    protected String checkMemo;
    @XmlElement(name = "CheckNumber", nillable = true)
    protected Integer checkNumber;
    @XmlElement(name = "CheckPreviousReferNumber", nillable = true)
    protected String checkPreviousReferNumber;
    @XmlElement(name = "CheckPrint")
    protected Boolean checkPrint;
    @XmlElement(name = "CheckRemittance", nillable = true)
    protected String checkRemittance;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "DisbursementAccountingMonth", nillable = true)
    protected String disbursementAccountingMonth;
    @XmlElement(name = "DisbursementBankAccountNumberCode", nillable = true)
    protected String disbursementBankAccountNumberCode;
    @XmlElement(name = "DisbursementBankSubAccountNumberCode", nillable = true)
    protected String disbursementBankSubAccountNumberCode;
    @XmlElement(name = "DisbursementDefaultEntryID")
    protected Integer disbursementDefaultEntryID;
    @XmlElement(name = "DisbursementDescription", nillable = true)
    protected String disbursementDescription;
    @XmlElement(name = "DisbursementEffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar disbursementEffectiveDate;
    @XmlElement(name = "DisbursementID")
    protected Integer disbursementID;
    @XmlElement(name = "DisbursementRecurringEntry", nillable = true)
    protected String disbursementRecurringEntry;
    @XmlElement(name = "DisbursementReferNumber", nillable = true)
    protected String disbursementReferNumber;
    @XmlElement(name = "IgnoreDuplicatePayToTheOrderOfInvoiceNumber")
    protected Boolean ignoreDuplicatePayToTheOrderOfInvoiceNumber;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "PayToTheOrderOfAccountLookupCode", nillable = true)
    protected String payToTheOrderOfAccountLookupCode;
    @XmlElement(name = "PayToTheOrderOfAccountNumber", nillable = true)
    protected String payToTheOrderOfAccountNumber;
    @XmlElement(name = "PayToTheOrderOfAccountType", nillable = true)
    protected String payToTheOrderOfAccountType;
    @XmlElement(name = "PayToTheOrderOfInvoiceDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payToTheOrderOfInvoiceDate;
    @XmlElement(name = "PayToTheOrderOfInvoiceNumber", nillable = true)
    protected String payToTheOrderOfInvoiceNumber;
    @XmlElement(name = "PayToTheOrderOfMailingAddress", nillable = true)
    protected Address payToTheOrderOfMailingAddress;
    @XmlElement(name = "PayToTheOrderOfMailingAddressContact", nillable = true)
    protected String payToTheOrderOfMailingAddressContact;
    @XmlElement(name = "PayToTheOrderOfMailingAddressContactID")
    protected Integer payToTheOrderOfMailingAddressContactID;
    @XmlElement(name = "PayToTheOrderOfMailingAddressSiteID", nillable = true)
    protected String payToTheOrderOfMailingAddressSiteID;
    @XmlElement(name = "PayToTheOrderOfPayee", nillable = true)
    protected String payToTheOrderOfPayee;
    @XmlElement(name = "PayToTheOrderOfPayeeContactID")
    protected Integer payToTheOrderOfPayeeContactID;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "VoidDetails", nillable = true)
    protected String voidDetails;
    @XmlElement(name = "VoidReason", nillable = true)
    protected String voidReason;
    @XmlElement(name = "VoidReferNumber", nillable = true)
    protected String voidReferNumber;
    @XmlElement(name = "VoidVoided", nillable = true)
    protected String voidVoided;
    @XmlElement(name = "CheckComments", nillable = true)
    protected String checkComments;
    @XmlElement(name = "CheckIncludeCheckStubDetail")
    protected Boolean checkIncludeCheckStubDetail;
    @XmlElement(name = "CheckRouting", nillable = true)
    protected String checkRouting;
    @XmlElement(name = "DirectDepositMethodID", nillable = true)
    protected Integer directDepositMethodID;

    /**
     * Gets the value of the checkLastPrintedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCheckLastPrintedDate() {
        return checkLastPrintedDate;
    }

    /**
     * Sets the value of the checkLastPrintedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCheckLastPrintedDate(XMLGregorianCalendar value) {
        this.checkLastPrintedDate = value;
    }

    /**
     * Gets the value of the checkMemo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckMemo() {
        return checkMemo;
    }

    /**
     * Sets the value of the checkMemo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckMemo(String value) {
        this.checkMemo = value;
    }

    /**
     * Gets the value of the checkNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCheckNumber() {
        return checkNumber;
    }

    /**
     * Sets the value of the checkNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCheckNumber(Integer value) {
        this.checkNumber = value;
    }

    /**
     * Gets the value of the checkPreviousReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckPreviousReferNumber() {
        return checkPreviousReferNumber;
    }

    /**
     * Sets the value of the checkPreviousReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckPreviousReferNumber(String value) {
        this.checkPreviousReferNumber = value;
    }

    /**
     * Gets the value of the checkPrint property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCheckPrint() {
        return checkPrint;
    }

    /**
     * Sets the value of the checkPrint property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCheckPrint(Boolean value) {
        this.checkPrint = value;
    }

    /**
     * Gets the value of the checkRemittance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckRemittance() {
        return checkRemittance;
    }

    /**
     * Sets the value of the checkRemittance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckRemittance(String value) {
        this.checkRemittance = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the disbursementAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementAccountingMonth() {
        return disbursementAccountingMonth;
    }

    /**
     * Sets the value of the disbursementAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementAccountingMonth(String value) {
        this.disbursementAccountingMonth = value;
    }

    /**
     * Gets the value of the disbursementBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementBankAccountNumberCode() {
        return disbursementBankAccountNumberCode;
    }

    /**
     * Sets the value of the disbursementBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementBankAccountNumberCode(String value) {
        this.disbursementBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the disbursementBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementBankSubAccountNumberCode() {
        return disbursementBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the disbursementBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementBankSubAccountNumberCode(String value) {
        this.disbursementBankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the disbursementDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisbursementDefaultEntryID() {
        return disbursementDefaultEntryID;
    }

    /**
     * Sets the value of the disbursementDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisbursementDefaultEntryID(Integer value) {
        this.disbursementDefaultEntryID = value;
    }

    /**
     * Gets the value of the disbursementDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementDescription() {
        return disbursementDescription;
    }

    /**
     * Sets the value of the disbursementDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementDescription(String value) {
        this.disbursementDescription = value;
    }

    /**
     * Gets the value of the disbursementEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDisbursementEffectiveDate() {
        return disbursementEffectiveDate;
    }

    /**
     * Sets the value of the disbursementEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDisbursementEffectiveDate(XMLGregorianCalendar value) {
        this.disbursementEffectiveDate = value;
    }

    /**
     * Gets the value of the disbursementID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisbursementID() {
        return disbursementID;
    }

    /**
     * Sets the value of the disbursementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisbursementID(Integer value) {
        this.disbursementID = value;
    }

    /**
     * Gets the value of the disbursementRecurringEntry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementRecurringEntry() {
        return disbursementRecurringEntry;
    }

    /**
     * Sets the value of the disbursementRecurringEntry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementRecurringEntry(String value) {
        this.disbursementRecurringEntry = value;
    }

    /**
     * Gets the value of the disbursementReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementReferNumber() {
        return disbursementReferNumber;
    }

    /**
     * Sets the value of the disbursementReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementReferNumber(String value) {
        this.disbursementReferNumber = value;
    }

    /**
     * Gets the value of the ignoreDuplicatePayToTheOrderOfInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreDuplicatePayToTheOrderOfInvoiceNumber() {
        return ignoreDuplicatePayToTheOrderOfInvoiceNumber;
    }

    /**
     * Sets the value of the ignoreDuplicatePayToTheOrderOfInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreDuplicatePayToTheOrderOfInvoiceNumber(Boolean value) {
        this.ignoreDuplicatePayToTheOrderOfInvoiceNumber = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountLookupCode() {
        return payToTheOrderOfAccountLookupCode;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountLookupCode(String value) {
        this.payToTheOrderOfAccountLookupCode = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountNumber() {
        return payToTheOrderOfAccountNumber;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountNumber(String value) {
        this.payToTheOrderOfAccountNumber = value;
    }

    /**
     * Gets the value of the payToTheOrderOfAccountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfAccountType() {
        return payToTheOrderOfAccountType;
    }

    /**
     * Sets the value of the payToTheOrderOfAccountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfAccountType(String value) {
        this.payToTheOrderOfAccountType = value;
    }

    /**
     * Gets the value of the payToTheOrderOfInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayToTheOrderOfInvoiceDate() {
        return payToTheOrderOfInvoiceDate;
    }

    /**
     * Sets the value of the payToTheOrderOfInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayToTheOrderOfInvoiceDate(XMLGregorianCalendar value) {
        this.payToTheOrderOfInvoiceDate = value;
    }

    /**
     * Gets the value of the payToTheOrderOfInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfInvoiceNumber() {
        return payToTheOrderOfInvoiceNumber;
    }

    /**
     * Sets the value of the payToTheOrderOfInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfInvoiceNumber(String value) {
        this.payToTheOrderOfInvoiceNumber = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPayToTheOrderOfMailingAddress() {
        return payToTheOrderOfMailingAddress;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPayToTheOrderOfMailingAddress(Address value) {
        this.payToTheOrderOfMailingAddress = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfMailingAddressContact() {
        return payToTheOrderOfMailingAddressContact;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfMailingAddressContact(String value) {
        this.payToTheOrderOfMailingAddressContact = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayToTheOrderOfMailingAddressContactID() {
        return payToTheOrderOfMailingAddressContactID;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayToTheOrderOfMailingAddressContactID(Integer value) {
        this.payToTheOrderOfMailingAddressContactID = value;
    }

    /**
     * Gets the value of the payToTheOrderOfMailingAddressSiteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfMailingAddressSiteID() {
        return payToTheOrderOfMailingAddressSiteID;
    }

    /**
     * Sets the value of the payToTheOrderOfMailingAddressSiteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfMailingAddressSiteID(String value) {
        this.payToTheOrderOfMailingAddressSiteID = value;
    }

    /**
     * Gets the value of the payToTheOrderOfPayee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayToTheOrderOfPayee() {
        return payToTheOrderOfPayee;
    }

    /**
     * Sets the value of the payToTheOrderOfPayee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayToTheOrderOfPayee(String value) {
        this.payToTheOrderOfPayee = value;
    }

    /**
     * Gets the value of the payToTheOrderOfPayeeContactID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayToTheOrderOfPayeeContactID() {
        return payToTheOrderOfPayeeContactID;
    }

    /**
     * Sets the value of the payToTheOrderOfPayeeContactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayToTheOrderOfPayeeContactID(Integer value) {
        this.payToTheOrderOfPayeeContactID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the voidDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidDetails() {
        return voidDetails;
    }

    /**
     * Sets the value of the voidDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidDetails(String value) {
        this.voidDetails = value;
    }

    /**
     * Gets the value of the voidReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReason() {
        return voidReason;
    }

    /**
     * Sets the value of the voidReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReason(String value) {
        this.voidReason = value;
    }

    /**
     * Gets the value of the voidReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidReferNumber() {
        return voidReferNumber;
    }

    /**
     * Sets the value of the voidReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidReferNumber(String value) {
        this.voidReferNumber = value;
    }

    /**
     * Gets the value of the voidVoided property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoidVoided() {
        return voidVoided;
    }

    /**
     * Sets the value of the voidVoided property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoidVoided(String value) {
        this.voidVoided = value;
    }

    /**
     * Gets the value of the checkComments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckComments() {
        return checkComments;
    }

    /**
     * Sets the value of the checkComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckComments(String value) {
        this.checkComments = value;
    }

    /**
     * Gets the value of the checkIncludeCheckStubDetail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCheckIncludeCheckStubDetail() {
        return checkIncludeCheckStubDetail;
    }

    /**
     * Sets the value of the checkIncludeCheckStubDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCheckIncludeCheckStubDetail(Boolean value) {
        this.checkIncludeCheckStubDetail = value;
    }

    /**
     * Gets the value of the checkRouting property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckRouting() {
        return checkRouting;
    }

    /**
     * Sets the value of the checkRouting property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckRouting(String value) {
        this.checkRouting = value;
    }

    /**
     * Gets the value of the directDepositMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDirectDepositMethodID() {
        return directDepositMethodID;
    }

    /**
     * Sets the value of the directDepositMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDirectDepositMethodID(Integer value) {
        this.directDepositMethodID = value;
    }

}
