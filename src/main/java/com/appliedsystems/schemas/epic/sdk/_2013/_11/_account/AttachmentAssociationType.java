
package com.appliedsystems.schemas.epic.sdk._2013._11._account;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttachmentAssociationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttachmentAssociationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AccountID"/>
 *     &lt;enumeration value="ActivityID"/>
 *     &lt;enumeration value="ClaimID"/>
 *     &lt;enumeration value="PolicyID"/>
 *     &lt;enumeration value="LineID"/>
 *     &lt;enumeration value="CertificateID"/>
 *     &lt;enumeration value="BinderID"/>
 *     &lt;enumeration value="EvidenceID"/>
 *     &lt;enumeration value="MarketedPolicyID"/>
 *     &lt;enumeration value="CarrierSubmissionID"/>
 *     &lt;enumeration value="I207ID"/>
 *     &lt;enumeration value="ServiceID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AttachmentAssociationType", namespace = "http://schemas.appliedsystems.com/epic/sdk/2013/11/_account/")
@XmlEnum
public enum AttachmentAssociationType {

    @XmlEnumValue("AccountID")
    ACCOUNT_ID("AccountID"),
    @XmlEnumValue("ActivityID")
    ACTIVITY_ID("ActivityID"),
    @XmlEnumValue("ClaimID")
    CLAIM_ID("ClaimID"),
    @XmlEnumValue("PolicyID")
    POLICY_ID("PolicyID"),
    @XmlEnumValue("LineID")
    LINE_ID("LineID"),
    @XmlEnumValue("CertificateID")
    CERTIFICATE_ID("CertificateID"),
    @XmlEnumValue("BinderID")
    BINDER_ID("BinderID"),
    @XmlEnumValue("EvidenceID")
    EVIDENCE_ID("EvidenceID"),
    @XmlEnumValue("MarketedPolicyID")
    MARKETED_POLICY_ID("MarketedPolicyID"),
    @XmlEnumValue("CarrierSubmissionID")
    CARRIER_SUBMISSION_ID("CarrierSubmissionID"),
    @XmlEnumValue("I207ID")
    I_207_ID("I207ID"),
    @XmlEnumValue("ServiceID")
    SERVICE_ID("ServiceID");
    private final String value;

    AttachmentAssociationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AttachmentAssociationType fromValue(String v) {
        for (AttachmentAssociationType c: AttachmentAssociationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
