
package com.appliedsystems.schemas.epic.sdk._2009._07._account._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgencyStructureItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgencyStructureItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyStructureItem" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}AgencyStructureItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgencyStructureItems", propOrder = {
    "agencyStructureItems"
})
public class AgencyStructureItems {

    @XmlElement(name = "AgencyStructureItem", nillable = true)
    protected List<AgencyStructureItem> agencyStructureItems;

    /**
     * Gets the value of the agencyStructureItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agencyStructureItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgencyStructureItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgencyStructureItem }
     * 
     * 
     */
    public List<AgencyStructureItem> getAgencyStructureItems() {
        if (agencyStructureItems == null) {
            agencyStructureItems = new ArrayList<AgencyStructureItem>();
        }
        return this.agencyStructureItems;
    }

}
