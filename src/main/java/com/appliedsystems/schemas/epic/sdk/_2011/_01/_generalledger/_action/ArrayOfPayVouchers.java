
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPayVouchers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPayVouchers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PayVouchers" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/}PayVouchers" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPayVouchers", propOrder = {
    "payVouchers"
})
public class ArrayOfPayVouchers {

    @XmlElement(name = "PayVouchers", nillable = true)
    protected List<PayVouchers> payVouchers;

    /**
     * Gets the value of the payVouchers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payVouchers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPayVouchers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PayVouchers }
     * 
     * 
     */
    public List<PayVouchers> getPayVouchers() {
        if (payVouchers == null) {
            payVouchers = new ArrayList<PayVouchers>();
        }
        return this.payVouchers;
    }

}
