
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeServiceSummaryDescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeServiceSummaryDescription">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeDescriptionTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeServiceSummaryDescription", propOrder = {
    "changeDescriptionTo",
    "serviceSummaryID"
})
public class ChangeServiceSummaryDescription {

    @XmlElement(name = "ChangeDescriptionTo", nillable = true)
    protected String changeDescriptionTo;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;

    /**
     * Gets the value of the changeDescriptionTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeDescriptionTo() {
        return changeDescriptionTo;
    }

    /**
     * Sets the value of the changeDescriptionTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeDescriptionTo(String value) {
        this.changeDescriptionTo = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

}
