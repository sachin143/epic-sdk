
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.BillingMode;


/**
 * <p>Java class for LineFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingMode" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_linefilter/}BillingMode" minOccurs="0"/>
 *         &lt;element name="IssuingCompanyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LineStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineFilter", propOrder = {
    "billingMode",
    "issuingCompanyCode",
    "lineID",
    "lineStatusCode",
    "policyID"
})
public class LineFilter {

    @XmlElement(name = "BillingMode")
    @XmlSchemaType(name = "string")
    protected BillingMode billingMode;
    @XmlElement(name = "IssuingCompanyCode", nillable = true)
    protected String issuingCompanyCode;
    @XmlElement(name = "LineID", nillable = true)
    protected Integer lineID;
    @XmlElement(name = "LineStatusCode", nillable = true)
    protected String lineStatusCode;
    @XmlElement(name = "PolicyID", nillable = true)
    protected Integer policyID;

    /**
     * Gets the value of the billingMode property.
     * 
     * @return
     *     possible object is
     *     {@link BillingMode }
     *     
     */
    public BillingMode getBillingMode() {
        return billingMode;
    }

    /**
     * Sets the value of the billingMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingMode }
     *     
     */
    public void setBillingMode(BillingMode value) {
        this.billingMode = value;
    }

    /**
     * Gets the value of the issuingCompanyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCompanyCode() {
        return issuingCompanyCode;
    }

    /**
     * Sets the value of the issuingCompanyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCompanyCode(String value) {
        this.issuingCompanyCode = value;
    }

    /**
     * Gets the value of the lineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLineID() {
        return lineID;
    }

    /**
     * Sets the value of the lineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLineID(Integer value) {
        this.lineID = value;
    }

    /**
     * Gets the value of the lineStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineStatusCode() {
        return lineStatusCode;
    }

    /**
     * Sets the value of the lineStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineStatusCode(String value) {
        this.lineStatusCode = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

}
