
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CommissionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", "CommissionItem");
    private final static QName _ProducerBrokerScheduleItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", "ProducerBrokerScheduleItems");
    private final static QName _ProducerBrokerScheduleItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", "ProducerBrokerScheduleItem");
    private final static QName _CommissionItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", "CommissionItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CommissionItem }
     * 
     */
    public CommissionItem createCommissionItem() {
        return new CommissionItem();
    }

    /**
     * Create an instance of {@link ProducerBrokerScheduleItems }
     * 
     */
    public ProducerBrokerScheduleItems createProducerBrokerScheduleItems() {
        return new ProducerBrokerScheduleItems();
    }

    /**
     * Create an instance of {@link ProducerBrokerScheduleItem }
     * 
     */
    public ProducerBrokerScheduleItem createProducerBrokerScheduleItem() {
        return new ProducerBrokerScheduleItem();
    }

    /**
     * Create an instance of {@link CommissionItems }
     * 
     */
    public CommissionItems createCommissionItems() {
        return new CommissionItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommissionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", name = "CommissionItem")
    public JAXBElement<CommissionItem> createCommissionItem(CommissionItem value) {
        return new JAXBElement<CommissionItem>(_CommissionItem_QNAME, CommissionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerScheduleItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", name = "ProducerBrokerScheduleItems")
    public JAXBElement<ProducerBrokerScheduleItems> createProducerBrokerScheduleItems(ProducerBrokerScheduleItems value) {
        return new JAXBElement<ProducerBrokerScheduleItems>(_ProducerBrokerScheduleItems_QNAME, ProducerBrokerScheduleItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerScheduleItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", name = "ProducerBrokerScheduleItem")
    public JAXBElement<ProducerBrokerScheduleItem> createProducerBrokerScheduleItem(ProducerBrokerScheduleItem value) {
        return new JAXBElement<ProducerBrokerScheduleItem>(_ProducerBrokerScheduleItem_QNAME, ProducerBrokerScheduleItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommissionItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/", name = "CommissionItems")
    public JAXBElement<CommissionItems> createCommissionItems(CommissionItems value) {
        return new JAXBElement<CommissionItems>(_CommissionItems_QNAME, CommissionItems.class, null, value);
    }

}
