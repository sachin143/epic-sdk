
package com.appliedsystems.schemas.epic.sdk._2011._01._account._claim;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._claim package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InsuredContact_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/", "InsuredContact");
    private final static QName _ArrayOfInsuredContact_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/", "ArrayOfInsuredContact");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._claim
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsuredContact }
     * 
     */
    public InsuredContact createInsuredContact() {
        return new InsuredContact();
    }

    /**
     * Create an instance of {@link ArrayOfInsuredContact }
     * 
     */
    public ArrayOfInsuredContact createArrayOfInsuredContact() {
        return new ArrayOfInsuredContact();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsuredContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/", name = "InsuredContact")
    public JAXBElement<InsuredContact> createInsuredContact(InsuredContact value) {
        return new JAXBElement<InsuredContact>(_InsuredContact_QNAME, InsuredContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInsuredContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/", name = "ArrayOfInsuredContact")
    public JAXBElement<ArrayOfInsuredContact> createArrayOfInsuredContact(ArrayOfInsuredContact value) {
        return new JAXBElement<ArrayOfInsuredContact>(_ArrayOfInsuredContact_QNAME, ArrayOfInsuredContact.class, null, value);
    }

}
