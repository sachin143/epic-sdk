
package com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClassificationItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/_personalclassifications/", "ClassificationItem");
    private final static QName _ClassificationItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/_personalclassifications/", "ClassificationItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClassificationItem }
     * 
     */
    public ClassificationItem createClassificationItem() {
        return new ClassificationItem();
    }

    /**
     * Create an instance of {@link ClassificationItems }
     * 
     */
    public ClassificationItems createClassificationItems() {
        return new ClassificationItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassificationItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/_personalclassifications/", name = "ClassificationItem")
    public JAXBElement<ClassificationItem> createClassificationItem(ClassificationItem value) {
        return new JAXBElement<ClassificationItem>(_ClassificationItem_QNAME, ClassificationItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassificationItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_contact/_personalclassifications/", name = "ClassificationItems")
    public JAXBElement<ClassificationItems> createClassificationItems(ClassificationItems value) {
        return new JAXBElement<ClassificationItems>(_ClassificationItems_QNAME, ClassificationItems.class, null, value);
    }

}
