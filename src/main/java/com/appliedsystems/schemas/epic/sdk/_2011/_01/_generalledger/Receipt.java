
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.Detail;
import com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ProcessOutstandingPayments;


/**
 * <p>Java class for Receipt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Receipt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/}Detail" minOccurs="0"/>
 *         &lt;element name="FinalizedReceipt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReceiptAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ReceiptID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ReceiptReferNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SuspendedReceipt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IgnoreAccountingMonthVerification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProcessOutstandingPaymentsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/}ProcessOutstandingPayments" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Receipt", propOrder = {
    "bankAccountNumberCode",
    "bankSubAccountNumberCode",
    "detailValue",
    "finalizedReceipt",
    "isReadOnly",
    "receiptAccountingMonth",
    "receiptDescription",
    "receiptEffectiveDate",
    "receiptID",
    "receiptReferNumber",
    "suspendedReceipt",
    "timestamp",
    "ignoreAccountingMonthVerification",
    "processOutstandingPaymentsValue"
})
public class Receipt {

    @XmlElement(name = "BankAccountNumberCode", nillable = true)
    protected String bankAccountNumberCode;
    @XmlElement(name = "BankSubAccountNumberCode", nillable = true)
    protected String bankSubAccountNumberCode;
    @XmlElement(name = "DetailValue", nillable = true)
    protected Detail detailValue;
    @XmlElement(name = "FinalizedReceipt")
    protected Boolean finalizedReceipt;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "ReceiptAccountingMonth", nillable = true)
    protected String receiptAccountingMonth;
    @XmlElement(name = "ReceiptDescription", nillable = true)
    protected String receiptDescription;
    @XmlElement(name = "ReceiptEffectiveDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar receiptEffectiveDate;
    @XmlElement(name = "ReceiptID")
    protected Integer receiptID;
    @XmlElement(name = "ReceiptReferNumber", nillable = true)
    protected String receiptReferNumber;
    @XmlElement(name = "SuspendedReceipt")
    protected Boolean suspendedReceipt;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "IgnoreAccountingMonthVerification", nillable = true)
    protected Boolean ignoreAccountingMonthVerification;
    @XmlElement(name = "ProcessOutstandingPaymentsValue", nillable = true)
    protected ProcessOutstandingPayments processOutstandingPaymentsValue;

    /**
     * Gets the value of the bankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNumberCode() {
        return bankAccountNumberCode;
    }

    /**
     * Sets the value of the bankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNumberCode(String value) {
        this.bankAccountNumberCode = value;
    }

    /**
     * Gets the value of the bankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankSubAccountNumberCode() {
        return bankSubAccountNumberCode;
    }

    /**
     * Sets the value of the bankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankSubAccountNumberCode(String value) {
        this.bankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the detailValue property.
     * 
     * @return
     *     possible object is
     *     {@link Detail }
     *     
     */
    public Detail getDetailValue() {
        return detailValue;
    }

    /**
     * Sets the value of the detailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Detail }
     *     
     */
    public void setDetailValue(Detail value) {
        this.detailValue = value;
    }

    /**
     * Gets the value of the finalizedReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinalizedReceipt() {
        return finalizedReceipt;
    }

    /**
     * Sets the value of the finalizedReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinalizedReceipt(Boolean value) {
        this.finalizedReceipt = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the receiptAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptAccountingMonth() {
        return receiptAccountingMonth;
    }

    /**
     * Sets the value of the receiptAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptAccountingMonth(String value) {
        this.receiptAccountingMonth = value;
    }

    /**
     * Gets the value of the receiptDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptDescription() {
        return receiptDescription;
    }

    /**
     * Sets the value of the receiptDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptDescription(String value) {
        this.receiptDescription = value;
    }

    /**
     * Gets the value of the receiptEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReceiptEffectiveDate() {
        return receiptEffectiveDate;
    }

    /**
     * Sets the value of the receiptEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReceiptEffectiveDate(XMLGregorianCalendar value) {
        this.receiptEffectiveDate = value;
    }

    /**
     * Gets the value of the receiptID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptID() {
        return receiptID;
    }

    /**
     * Sets the value of the receiptID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptID(Integer value) {
        this.receiptID = value;
    }

    /**
     * Gets the value of the receiptReferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptReferNumber() {
        return receiptReferNumber;
    }

    /**
     * Sets the value of the receiptReferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptReferNumber(String value) {
        this.receiptReferNumber = value;
    }

    /**
     * Gets the value of the suspendedReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuspendedReceipt() {
        return suspendedReceipt;
    }

    /**
     * Sets the value of the suspendedReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuspendedReceipt(Boolean value) {
        this.suspendedReceipt = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the ignoreAccountingMonthVerification property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreAccountingMonthVerification() {
        return ignoreAccountingMonthVerification;
    }

    /**
     * Sets the value of the ignoreAccountingMonthVerification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreAccountingMonthVerification(Boolean value) {
        this.ignoreAccountingMonthVerification = value;
    }

    /**
     * Gets the value of the processOutstandingPaymentsValue property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessOutstandingPayments }
     *     
     */
    public ProcessOutstandingPayments getProcessOutstandingPaymentsValue() {
        return processOutstandingPaymentsValue;
    }

    /**
     * Sets the value of the processOutstandingPaymentsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessOutstandingPayments }
     *     
     */
    public void setProcessOutstandingPaymentsValue(ProcessOutstandingPayments value) {
        this.processOutstandingPaymentsValue = value;
    }

}
