
package com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Billing_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/_line/", "Billing");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Billing }
     * 
     */
    public Billing createBilling() {
        return new Billing();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Billing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/_line/", name = "Billing")
    public JAXBElement<Billing> createBilling(Billing value) {
        return new JAXBElement<Billing>(_Billing_QNAME, Billing.class, null, value);
    }

}
