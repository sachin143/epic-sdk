
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.Flags;


/**
 * <p>Java class for TransactionItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetailAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailClosePremiumPayable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DetailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Flag" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_reversetransaction/_transactionitem/}Flags" minOccurs="0"/>
 *         &lt;element name="ParentFinanceTransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ParentTransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DetailCloseGovernmentPayable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DetailClosePrBrPayable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionItem", propOrder = {
    "detailAccountingMonth",
    "detailClosePremiumPayable",
    "detailDescription",
    "flag",
    "parentFinanceTransactionID",
    "parentTransactionID",
    "transactionID",
    "detailCloseGovernmentPayable",
    "detailClosePrBrPayable"
})
public class TransactionItem {

    @XmlElement(name = "DetailAccountingMonth", nillable = true)
    protected String detailAccountingMonth;
    @XmlElement(name = "DetailClosePremiumPayable")
    protected Boolean detailClosePremiumPayable;
    @XmlElement(name = "DetailDescription", nillable = true)
    protected String detailDescription;
    @XmlElement(name = "Flag")
    @XmlSchemaType(name = "string")
    protected Flags flag;
    @XmlElement(name = "ParentFinanceTransactionID")
    protected Integer parentFinanceTransactionID;
    @XmlElement(name = "ParentTransactionID")
    protected Integer parentTransactionID;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "DetailCloseGovernmentPayable")
    protected Boolean detailCloseGovernmentPayable;
    @XmlElement(name = "DetailClosePrBrPayable")
    protected Boolean detailClosePrBrPayable;

    /**
     * Gets the value of the detailAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailAccountingMonth() {
        return detailAccountingMonth;
    }

    /**
     * Sets the value of the detailAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailAccountingMonth(String value) {
        this.detailAccountingMonth = value;
    }

    /**
     * Gets the value of the detailClosePremiumPayable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDetailClosePremiumPayable() {
        return detailClosePremiumPayable;
    }

    /**
     * Sets the value of the detailClosePremiumPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDetailClosePremiumPayable(Boolean value) {
        this.detailClosePremiumPayable = value;
    }

    /**
     * Gets the value of the detailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailDescription() {
        return detailDescription;
    }

    /**
     * Sets the value of the detailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailDescription(String value) {
        this.detailDescription = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link Flags }
     *     
     */
    public Flags getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Flags }
     *     
     */
    public void setFlag(Flags value) {
        this.flag = value;
    }

    /**
     * Gets the value of the parentFinanceTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getParentFinanceTransactionID() {
        return parentFinanceTransactionID;
    }

    /**
     * Sets the value of the parentFinanceTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setParentFinanceTransactionID(Integer value) {
        this.parentFinanceTransactionID = value;
    }

    /**
     * Gets the value of the parentTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getParentTransactionID() {
        return parentTransactionID;
    }

    /**
     * Sets the value of the parentTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setParentTransactionID(Integer value) {
        this.parentTransactionID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the detailCloseGovernmentPayable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDetailCloseGovernmentPayable() {
        return detailCloseGovernmentPayable;
    }

    /**
     * Sets the value of the detailCloseGovernmentPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDetailCloseGovernmentPayable(Boolean value) {
        this.detailCloseGovernmentPayable = value;
    }

    /**
     * Gets the value of the detailClosePrBrPayable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDetailClosePrBrPayable() {
        return detailClosePrBrPayable;
    }

    /**
     * Sets the value of the detailClosePrBrPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDetailClosePrBrPayable(Boolean value) {
        this.detailClosePrBrPayable = value;
    }

}
