
package com.appliedsystems.schemas.epic.sdk._2017._02._account._claim;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._claim package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfPaymentExpense_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/", "ArrayOfPaymentExpense");
    private final static QName _PaymentExpense_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/", "PaymentExpense");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._claim
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentExpense }
     * 
     */
    public PaymentExpense createPaymentExpense() {
        return new PaymentExpense();
    }

    /**
     * Create an instance of {@link ArrayOfPaymentExpense }
     * 
     */
    public ArrayOfPaymentExpense createArrayOfPaymentExpense() {
        return new ArrayOfPaymentExpense();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPaymentExpense }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/", name = "ArrayOfPaymentExpense")
    public JAXBElement<ArrayOfPaymentExpense> createArrayOfPaymentExpense(ArrayOfPaymentExpense value) {
        return new JAXBElement<ArrayOfPaymentExpense>(_ArrayOfPaymentExpense_QNAME, ArrayOfPaymentExpense.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentExpense }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/", name = "PaymentExpense")
    public JAXBElement<PaymentExpense> createPaymentExpense(PaymentExpense value) {
        return new JAXBElement<PaymentExpense>(_PaymentExpense_QNAME, PaymentExpense.class, null, value);
    }

}
