
package com.appliedsystems.schemas.epic.sdk._2011._01._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.AttachedWithinType;
import com.appliedsystems.schemas.epic.sdk._2013._11._account.AttachmentAssociationType;


/**
 * <p>Java class for AttachmentFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttachmentFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AttachedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AttachmentAssociationID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AttachmentAssociationType" type="{http://schemas.appliedsystems.com/epic/sdk/2013/11/_account/}AttachmentAssociationType" minOccurs="0"/>
 *         &lt;element name="AttachmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientAccessible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Folder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FolderEquals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Inactive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeSubfolderAttachments" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubFolder1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder1Equals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubFolder2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder2Equals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SystemGenerated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AccessibleByEmployeeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DescriptionEquals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EditedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="EditedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SubFolder3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder3Equals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubFolder4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder4Equals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SubFolder5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFolder5Equals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AttachedWithin" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/}AttachedWithinType" minOccurs="0"/>
 *         &lt;element name="AccessedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AccessedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="WasAccessed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientAccessExpireOnDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ClientAccessExpireOnDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DoNotPurge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DoNotPurgeExpireOnDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DoNotPurgeExpireOnDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentFilter", propOrder = {
    "attachedDateBegins",
    "attachedDateEnds",
    "attachmentAssociationID",
    "attachmentAssociationType",
    "attachmentID",
    "clientAccessible",
    "folder",
    "folderEquals",
    "inactive",
    "includeSubfolderAttachments",
    "subFolder1",
    "subFolder1Equals",
    "subFolder2",
    "subFolder2Equals",
    "systemGenerated",
    "accessibleByEmployeeCode",
    "description",
    "descriptionEquals",
    "editedDateBegins",
    "editedDateEnds",
    "subFolder3",
    "subFolder3Equals",
    "subFolder4",
    "subFolder4Equals",
    "subFolder5",
    "subFolder5Equals",
    "attachedWithin",
    "accessedDateBegins",
    "accessedDateEnds",
    "wasAccessed",
    "accountTypeCode",
    "clientAccessExpireOnDateBegins",
    "clientAccessExpireOnDateEnds",
    "doNotPurge",
    "doNotPurgeExpireOnDateBegins",
    "doNotPurgeExpireOnDateEnds"
})
public class AttachmentFilter {

    @XmlElement(name = "AttachedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar attachedDateBegins;
    @XmlElement(name = "AttachedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar attachedDateEnds;
    @XmlElement(name = "AttachmentAssociationID", nillable = true)
    protected Integer attachmentAssociationID;
    @XmlElement(name = "AttachmentAssociationType")
    @XmlSchemaType(name = "string")
    protected AttachmentAssociationType attachmentAssociationType;
    @XmlElement(name = "AttachmentID", nillable = true)
    protected Integer attachmentID;
    @XmlElement(name = "ClientAccessible", nillable = true)
    protected Boolean clientAccessible;
    @XmlElement(name = "Folder", nillable = true)
    protected String folder;
    @XmlElement(name = "FolderEquals")
    protected Boolean folderEquals;
    @XmlElement(name = "Inactive", nillable = true)
    protected Boolean inactive;
    @XmlElement(name = "IncludeSubfolderAttachments")
    protected Boolean includeSubfolderAttachments;
    @XmlElement(name = "SubFolder1", nillable = true)
    protected String subFolder1;
    @XmlElement(name = "SubFolder1Equals")
    protected Boolean subFolder1Equals;
    @XmlElement(name = "SubFolder2", nillable = true)
    protected String subFolder2;
    @XmlElement(name = "SubFolder2Equals")
    protected Boolean subFolder2Equals;
    @XmlElement(name = "SystemGenerated", nillable = true)
    protected Boolean systemGenerated;
    @XmlElement(name = "AccessibleByEmployeeCode", nillable = true)
    protected String accessibleByEmployeeCode;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DescriptionEquals")
    protected Boolean descriptionEquals;
    @XmlElement(name = "EditedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar editedDateBegins;
    @XmlElement(name = "EditedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar editedDateEnds;
    @XmlElement(name = "SubFolder3", nillable = true)
    protected String subFolder3;
    @XmlElement(name = "SubFolder3Equals")
    protected Boolean subFolder3Equals;
    @XmlElement(name = "SubFolder4", nillable = true)
    protected String subFolder4;
    @XmlElement(name = "SubFolder4Equals")
    protected Boolean subFolder4Equals;
    @XmlElement(name = "SubFolder5", nillable = true)
    protected String subFolder5;
    @XmlElement(name = "SubFolder5Equals")
    protected Boolean subFolder5Equals;
    @XmlElement(name = "AttachedWithin")
    @XmlSchemaType(name = "string")
    protected AttachedWithinType attachedWithin;
    @XmlElement(name = "AccessedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar accessedDateBegins;
    @XmlElement(name = "AccessedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar accessedDateEnds;
    @XmlElement(name = "WasAccessed", nillable = true)
    protected Boolean wasAccessed;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "ClientAccessExpireOnDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar clientAccessExpireOnDateBegins;
    @XmlElement(name = "ClientAccessExpireOnDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar clientAccessExpireOnDateEnds;
    @XmlElement(name = "DoNotPurge", nillable = true)
    protected Boolean doNotPurge;
    @XmlElement(name = "DoNotPurgeExpireOnDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar doNotPurgeExpireOnDateBegins;
    @XmlElement(name = "DoNotPurgeExpireOnDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar doNotPurgeExpireOnDateEnds;

    /**
     * Gets the value of the attachedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAttachedDateBegins() {
        return attachedDateBegins;
    }

    /**
     * Sets the value of the attachedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAttachedDateBegins(XMLGregorianCalendar value) {
        this.attachedDateBegins = value;
    }

    /**
     * Gets the value of the attachedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAttachedDateEnds() {
        return attachedDateEnds;
    }

    /**
     * Sets the value of the attachedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAttachedDateEnds(XMLGregorianCalendar value) {
        this.attachedDateEnds = value;
    }

    /**
     * Gets the value of the attachmentAssociationID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttachmentAssociationID() {
        return attachmentAssociationID;
    }

    /**
     * Sets the value of the attachmentAssociationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttachmentAssociationID(Integer value) {
        this.attachmentAssociationID = value;
    }

    /**
     * Gets the value of the attachmentAssociationType property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentAssociationType }
     *     
     */
    public AttachmentAssociationType getAttachmentAssociationType() {
        return attachmentAssociationType;
    }

    /**
     * Sets the value of the attachmentAssociationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentAssociationType }
     *     
     */
    public void setAttachmentAssociationType(AttachmentAssociationType value) {
        this.attachmentAssociationType = value;
    }

    /**
     * Gets the value of the attachmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttachmentID() {
        return attachmentID;
    }

    /**
     * Sets the value of the attachmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttachmentID(Integer value) {
        this.attachmentID = value;
    }

    /**
     * Gets the value of the clientAccessible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClientAccessible() {
        return clientAccessible;
    }

    /**
     * Sets the value of the clientAccessible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClientAccessible(Boolean value) {
        this.clientAccessible = value;
    }

    /**
     * Gets the value of the folder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Sets the value of the folder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolder(String value) {
        this.folder = value;
    }

    /**
     * Gets the value of the folderEquals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFolderEquals() {
        return folderEquals;
    }

    /**
     * Sets the value of the folderEquals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFolderEquals(Boolean value) {
        this.folderEquals = value;
    }

    /**
     * Gets the value of the inactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInactive() {
        return inactive;
    }

    /**
     * Sets the value of the inactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInactive(Boolean value) {
        this.inactive = value;
    }

    /**
     * Gets the value of the includeSubfolderAttachments property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSubfolderAttachments() {
        return includeSubfolderAttachments;
    }

    /**
     * Sets the value of the includeSubfolderAttachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSubfolderAttachments(Boolean value) {
        this.includeSubfolderAttachments = value;
    }

    /**
     * Gets the value of the subFolder1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder1() {
        return subFolder1;
    }

    /**
     * Sets the value of the subFolder1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder1(String value) {
        this.subFolder1 = value;
    }

    /**
     * Gets the value of the subFolder1Equals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubFolder1Equals() {
        return subFolder1Equals;
    }

    /**
     * Sets the value of the subFolder1Equals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubFolder1Equals(Boolean value) {
        this.subFolder1Equals = value;
    }

    /**
     * Gets the value of the subFolder2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder2() {
        return subFolder2;
    }

    /**
     * Sets the value of the subFolder2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder2(String value) {
        this.subFolder2 = value;
    }

    /**
     * Gets the value of the subFolder2Equals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubFolder2Equals() {
        return subFolder2Equals;
    }

    /**
     * Sets the value of the subFolder2Equals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubFolder2Equals(Boolean value) {
        this.subFolder2Equals = value;
    }

    /**
     * Gets the value of the systemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSystemGenerated() {
        return systemGenerated;
    }

    /**
     * Sets the value of the systemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSystemGenerated(Boolean value) {
        this.systemGenerated = value;
    }

    /**
     * Gets the value of the accessibleByEmployeeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessibleByEmployeeCode() {
        return accessibleByEmployeeCode;
    }

    /**
     * Sets the value of the accessibleByEmployeeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessibleByEmployeeCode(String value) {
        this.accessibleByEmployeeCode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the descriptionEquals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDescriptionEquals() {
        return descriptionEquals;
    }

    /**
     * Sets the value of the descriptionEquals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDescriptionEquals(Boolean value) {
        this.descriptionEquals = value;
    }

    /**
     * Gets the value of the editedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEditedDateBegins() {
        return editedDateBegins;
    }

    /**
     * Sets the value of the editedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEditedDateBegins(XMLGregorianCalendar value) {
        this.editedDateBegins = value;
    }

    /**
     * Gets the value of the editedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEditedDateEnds() {
        return editedDateEnds;
    }

    /**
     * Sets the value of the editedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEditedDateEnds(XMLGregorianCalendar value) {
        this.editedDateEnds = value;
    }

    /**
     * Gets the value of the subFolder3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder3() {
        return subFolder3;
    }

    /**
     * Sets the value of the subFolder3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder3(String value) {
        this.subFolder3 = value;
    }

    /**
     * Gets the value of the subFolder3Equals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubFolder3Equals() {
        return subFolder3Equals;
    }

    /**
     * Sets the value of the subFolder3Equals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubFolder3Equals(Boolean value) {
        this.subFolder3Equals = value;
    }

    /**
     * Gets the value of the subFolder4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder4() {
        return subFolder4;
    }

    /**
     * Sets the value of the subFolder4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder4(String value) {
        this.subFolder4 = value;
    }

    /**
     * Gets the value of the subFolder4Equals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubFolder4Equals() {
        return subFolder4Equals;
    }

    /**
     * Sets the value of the subFolder4Equals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubFolder4Equals(Boolean value) {
        this.subFolder4Equals = value;
    }

    /**
     * Gets the value of the subFolder5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubFolder5() {
        return subFolder5;
    }

    /**
     * Sets the value of the subFolder5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubFolder5(String value) {
        this.subFolder5 = value;
    }

    /**
     * Gets the value of the subFolder5Equals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubFolder5Equals() {
        return subFolder5Equals;
    }

    /**
     * Sets the value of the subFolder5Equals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubFolder5Equals(Boolean value) {
        this.subFolder5Equals = value;
    }

    /**
     * Gets the value of the attachedWithin property.
     * 
     * @return
     *     possible object is
     *     {@link AttachedWithinType }
     *     
     */
    public AttachedWithinType getAttachedWithin() {
        return attachedWithin;
    }

    /**
     * Sets the value of the attachedWithin property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachedWithinType }
     *     
     */
    public void setAttachedWithin(AttachedWithinType value) {
        this.attachedWithin = value;
    }

    /**
     * Gets the value of the accessedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccessedDateBegins() {
        return accessedDateBegins;
    }

    /**
     * Sets the value of the accessedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccessedDateBegins(XMLGregorianCalendar value) {
        this.accessedDateBegins = value;
    }

    /**
     * Gets the value of the accessedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccessedDateEnds() {
        return accessedDateEnds;
    }

    /**
     * Sets the value of the accessedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccessedDateEnds(XMLGregorianCalendar value) {
        this.accessedDateEnds = value;
    }

    /**
     * Gets the value of the wasAccessed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWasAccessed() {
        return wasAccessed;
    }

    /**
     * Sets the value of the wasAccessed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWasAccessed(Boolean value) {
        this.wasAccessed = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the clientAccessExpireOnDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClientAccessExpireOnDateBegins() {
        return clientAccessExpireOnDateBegins;
    }

    /**
     * Sets the value of the clientAccessExpireOnDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClientAccessExpireOnDateBegins(XMLGregorianCalendar value) {
        this.clientAccessExpireOnDateBegins = value;
    }

    /**
     * Gets the value of the clientAccessExpireOnDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClientAccessExpireOnDateEnds() {
        return clientAccessExpireOnDateEnds;
    }

    /**
     * Sets the value of the clientAccessExpireOnDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClientAccessExpireOnDateEnds(XMLGregorianCalendar value) {
        this.clientAccessExpireOnDateEnds = value;
    }

    /**
     * Gets the value of the doNotPurge property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoNotPurge() {
        return doNotPurge;
    }

    /**
     * Sets the value of the doNotPurge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoNotPurge(Boolean value) {
        this.doNotPurge = value;
    }

    /**
     * Gets the value of the doNotPurgeExpireOnDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDoNotPurgeExpireOnDateBegins() {
        return doNotPurgeExpireOnDateBegins;
    }

    /**
     * Sets the value of the doNotPurgeExpireOnDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDoNotPurgeExpireOnDateBegins(XMLGregorianCalendar value) {
        this.doNotPurgeExpireOnDateBegins = value;
    }

    /**
     * Gets the value of the doNotPurgeExpireOnDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDoNotPurgeExpireOnDateEnds() {
        return doNotPurgeExpireOnDateEnds;
    }

    /**
     * Sets the value of the doNotPurgeExpireOnDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDoNotPurgeExpireOnDateEnds(XMLGregorianCalendar value) {
        this.doNotPurgeExpireOnDateEnds = value;
    }

}
