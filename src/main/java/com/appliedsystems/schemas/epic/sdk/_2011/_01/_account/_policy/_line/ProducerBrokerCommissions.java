
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.CommissionItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ProducerBrokerScheduleItems;


/**
 * <p>Java class for ProducerBrokerCommissions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProducerBrokerCommissions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Commissions" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/}CommissionItems" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerCommissionTermOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="ProducerBrokerScheduleTermItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/_producerbrokercommissions/}ProducerBrokerScheduleItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProducerBrokerCommissions", propOrder = {
    "commissions",
    "producerBrokerCommissionTermOption",
    "producerBrokerScheduleTermItems"
})
public class ProducerBrokerCommissions {

    @XmlElement(name = "Commissions", nillable = true)
    protected CommissionItems commissions;
    @XmlElement(name = "ProducerBrokerCommissionTermOption", nillable = true)
    protected OptionType producerBrokerCommissionTermOption;
    @XmlElement(name = "ProducerBrokerScheduleTermItems", nillable = true)
    protected ProducerBrokerScheduleItems producerBrokerScheduleTermItems;

    /**
     * Gets the value of the commissions property.
     * 
     * @return
     *     possible object is
     *     {@link CommissionItems }
     *     
     */
    public CommissionItems getCommissions() {
        return commissions;
    }

    /**
     * Sets the value of the commissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionItems }
     *     
     */
    public void setCommissions(CommissionItems value) {
        this.commissions = value;
    }

    /**
     * Gets the value of the producerBrokerCommissionTermOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getProducerBrokerCommissionTermOption() {
        return producerBrokerCommissionTermOption;
    }

    /**
     * Sets the value of the producerBrokerCommissionTermOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setProducerBrokerCommissionTermOption(OptionType value) {
        this.producerBrokerCommissionTermOption = value;
    }

    /**
     * Gets the value of the producerBrokerScheduleTermItems property.
     * 
     * @return
     *     possible object is
     *     {@link ProducerBrokerScheduleItems }
     *     
     */
    public ProducerBrokerScheduleItems getProducerBrokerScheduleTermItems() {
        return producerBrokerScheduleTermItems;
    }

    /**
     * Sets the value of the producerBrokerScheduleTermItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProducerBrokerScheduleItems }
     *     
     */
    public void setProducerBrokerScheduleTermItems(ProducerBrokerScheduleItems value) {
        this.producerBrokerScheduleTermItems = value;
    }

}
