
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _JournalEntryVoid_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_action/", "JournalEntryVoid");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JournalEntryVoid }
     * 
     */
    public JournalEntryVoid createJournalEntryVoid() {
        return new JournalEntryVoid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JournalEntryVoid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_action/", name = "JournalEntryVoid")
    public JAXBElement<JournalEntryVoid> createJournalEntryVoid(JournalEntryVoid value) {
        return new JAXBElement<JournalEntryVoid>(_JournalEntryVoid_QNAME, JournalEntryVoid.class, null, value);
    }

}
