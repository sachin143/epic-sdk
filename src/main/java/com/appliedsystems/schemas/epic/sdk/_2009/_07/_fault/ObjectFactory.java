
package com.appliedsystems.schemas.epic.sdk._2009._07._fault;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2009._07._fault package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InputValidationFault_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", "InputValidationFault");
    private final static QName _AuthenticationFault_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", "AuthenticationFault");
    private final static QName _ConcurrencyFault_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", "ConcurrencyFault");
    private final static QName _MethodCallFault_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", "MethodCallFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2009._07._fault
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MethodCallFault }
     * 
     */
    public MethodCallFault createMethodCallFault() {
        return new MethodCallFault();
    }

    /**
     * Create an instance of {@link InputValidationFault }
     * 
     */
    public InputValidationFault createInputValidationFault() {
        return new InputValidationFault();
    }

    /**
     * Create an instance of {@link AuthenticationFault }
     * 
     */
    public AuthenticationFault createAuthenticationFault() {
        return new AuthenticationFault();
    }

    /**
     * Create an instance of {@link ConcurrencyFault }
     * 
     */
    public ConcurrencyFault createConcurrencyFault() {
        return new ConcurrencyFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InputValidationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", name = "InputValidationFault")
    public JAXBElement<InputValidationFault> createInputValidationFault(InputValidationFault value) {
        return new JAXBElement<InputValidationFault>(_InputValidationFault_QNAME, InputValidationFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", name = "AuthenticationFault")
    public JAXBElement<AuthenticationFault> createAuthenticationFault(AuthenticationFault value) {
        return new JAXBElement<AuthenticationFault>(_AuthenticationFault_QNAME, AuthenticationFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConcurrencyFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", name = "ConcurrencyFault")
    public JAXBElement<ConcurrencyFault> createConcurrencyFault(ConcurrencyFault value) {
        return new JAXBElement<ConcurrencyFault>(_ConcurrencyFault_QNAME, ConcurrencyFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MethodCallFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/_fault/", name = "MethodCallFault")
    public JAXBElement<MethodCallFault> createMethodCallFault(MethodCallFault value) {
        return new JAXBElement<MethodCallFault>(_MethodCallFault_QNAME, MethodCallFault.class, null, value);
    }

}
