
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DetailItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_detail/", "DetailItem");
    private final static QName _DetailItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_detail/", "DetailItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DetailItem }
     * 
     */
    public DetailItem createDetailItem() {
        return new DetailItem();
    }

    /**
     * Create an instance of {@link DetailItems }
     * 
     */
    public DetailItems createDetailItems() {
        return new DetailItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DetailItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_detail/", name = "DetailItem")
    public JAXBElement<DetailItem> createDetailItem(DetailItem value) {
        return new JAXBElement<DetailItem>(_DetailItem_QNAME, DetailItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DetailItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_detail/", name = "DetailItems")
    public JAXBElement<DetailItems> createDetailItems(DetailItems value) {
        return new JAXBElement<DetailItems>(_DetailItems_QNAME, DetailItems.class, null, value);
    }

}
