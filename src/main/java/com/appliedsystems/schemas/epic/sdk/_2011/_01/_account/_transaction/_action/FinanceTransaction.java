
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.TransactionItems;


/**
 * <p>Java class for FinanceTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinanceTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionItemsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_financetransaction/}TransactionItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinanceTransaction", propOrder = {
    "associatedAccountID",
    "transactionID",
    "transactionItemsValue"
})
public class FinanceTransaction {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "TransactionItemsValue", nillable = true)
    protected TransactionItems transactionItemsValue;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionItemsValue property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionItems }
     *     
     */
    public TransactionItems getTransactionItemsValue() {
        return transactionItemsValue;
    }

    /**
     * Sets the value of the transactionItemsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionItems }
     *     
     */
    public void setTransactionItemsValue(TransactionItems value) {
        this.transactionItemsValue = value;
    }

}
