
package com.appliedsystems.schemas.epic.sdk._2009._07._account._common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServicingRolesItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServicingRolesItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServicingRoleItem" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}ServicingRoleItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServicingRolesItems", propOrder = {
    "servicingRoleItems"
})
public class ServicingRolesItems {

    @XmlElement(name = "ServicingRoleItem", nillable = true)
    protected List<ServicingRoleItem> servicingRoleItems;

    /**
     * Gets the value of the servicingRoleItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servicingRoleItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicingRoleItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServicingRoleItem }
     * 
     * 
     */
    public List<ServicingRoleItem> getServicingRoleItems() {
        if (servicingRoleItems == null) {
            servicingRoleItems = new ArrayList<ServicingRoleItem>();
        }
        return this.servicingRoleItems;
    }

}
