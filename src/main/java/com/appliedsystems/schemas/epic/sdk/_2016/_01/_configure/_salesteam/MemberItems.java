
package com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MemberItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MemberItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberItem" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/}MemberItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberItems", propOrder = {
    "memberItems"
})
public class MemberItems {

    @XmlElement(name = "MemberItem", nillable = true)
    protected List<MemberItem> memberItems;

    /**
     * Gets the value of the memberItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MemberItem }
     * 
     * 
     */
    public List<MemberItem> getMemberItems() {
        if (memberItems == null) {
            memberItems = new ArrayList<MemberItem>();
        }
        return this.memberItems;
    }

}
