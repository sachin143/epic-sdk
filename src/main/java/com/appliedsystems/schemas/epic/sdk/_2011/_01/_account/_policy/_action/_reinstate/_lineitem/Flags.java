
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Flags.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Flags">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="View"/>
 *     &lt;enumeration value="Reinstate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Flags", namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_reinstate/_lineitem/")
@XmlEnum
public enum Flags {

    @XmlEnumValue("View")
    VIEW("View"),
    @XmlEnumValue("Reinstate")
    REINSTATE("Reinstate");
    private final String value;

    Flags(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Flags fromValue(String v) {
        for (Flags c: Flags.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
