
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReviseInstallmentsBasedOn_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", "ReviseInstallmentsBasedOn");
    private final static QName _SplitReceivableItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", "SplitReceivableItems");
    private final static QName _SplitReceivableItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", "SplitReceivableItem");
    private final static QName _InstallmentSummaryItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", "InstallmentSummaryItem");
    private final static QName _InstallmentSummaryItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", "InstallmentSummaryItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InstallmentSummaryItem }
     * 
     */
    public InstallmentSummaryItem createInstallmentSummaryItem() {
        return new InstallmentSummaryItem();
    }

    /**
     * Create an instance of {@link SplitReceivableItems }
     * 
     */
    public SplitReceivableItems createSplitReceivableItems() {
        return new SplitReceivableItems();
    }

    /**
     * Create an instance of {@link InstallmentSummaryItems }
     * 
     */
    public InstallmentSummaryItems createInstallmentSummaryItems() {
        return new InstallmentSummaryItems();
    }

    /**
     * Create an instance of {@link SplitReceivableItem }
     * 
     */
    public SplitReceivableItem createSplitReceivableItem() {
        return new SplitReceivableItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReviseInstallmentsBasedOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", name = "ReviseInstallmentsBasedOn")
    public JAXBElement<ReviseInstallmentsBasedOn> createReviseInstallmentsBasedOn(ReviseInstallmentsBasedOn value) {
        return new JAXBElement<ReviseInstallmentsBasedOn>(_ReviseInstallmentsBasedOn_QNAME, ReviseInstallmentsBasedOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", name = "SplitReceivableItems")
    public JAXBElement<SplitReceivableItems> createSplitReceivableItems(SplitReceivableItems value) {
        return new JAXBElement<SplitReceivableItems>(_SplitReceivableItems_QNAME, SplitReceivableItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", name = "SplitReceivableItem")
    public JAXBElement<SplitReceivableItem> createSplitReceivableItem(SplitReceivableItem value) {
        return new JAXBElement<SplitReceivableItem>(_SplitReceivableItem_QNAME, SplitReceivableItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstallmentSummaryItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", name = "InstallmentSummaryItem")
    public JAXBElement<InstallmentSummaryItem> createInstallmentSummaryItem(InstallmentSummaryItem value) {
        return new JAXBElement<InstallmentSummaryItem>(_InstallmentSummaryItem_QNAME, InstallmentSummaryItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstallmentSummaryItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/_installments/", name = "InstallmentSummaryItems")
    public JAXBElement<InstallmentSummaryItems> createInstallmentSummaryItems(InstallmentSummaryItems value) {
        return new JAXBElement<InstallmentSummaryItems>(_InstallmentSummaryItems_QNAME, InstallmentSummaryItems.class, null, value);
    }

}
