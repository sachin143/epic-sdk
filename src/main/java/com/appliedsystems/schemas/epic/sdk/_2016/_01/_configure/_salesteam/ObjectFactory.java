
package com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MemberItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/", "MemberItems");
    private final static QName _MemberItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/", "MemberItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MemberItem }
     * 
     */
    public MemberItem createMemberItem() {
        return new MemberItem();
    }

    /**
     * Create an instance of {@link MemberItems }
     * 
     */
    public MemberItems createMemberItems() {
        return new MemberItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/", name = "MemberItems")
    public JAXBElement<MemberItems> createMemberItems(MemberItems value) {
        return new JAXBElement<MemberItems>(_MemberItems_QNAME, MemberItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2016/01/_configure/_salesteam/", name = "MemberItem")
    public JAXBElement<MemberItem> createMemberItem(MemberItem value) {
        return new JAXBElement<MemberItem>(_MemberItem_QNAME, MemberItem.class, null, value);
    }

}
