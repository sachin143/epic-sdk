
package com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BalanceTransfer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BalanceTransfer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransferFromTransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransferToAccountLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransferToAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransferToBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceTransfer", propOrder = {
    "transferAmount",
    "transferFromTransactionID",
    "transferToAccountLookupCode",
    "transferToAgencyCode",
    "transferToBranchCode"
})
public class BalanceTransfer {

    @XmlElement(name = "TransferAmount", nillable = true)
    protected BigDecimal transferAmount;
    @XmlElement(name = "TransferFromTransactionID")
    protected Integer transferFromTransactionID;
    @XmlElement(name = "TransferToAccountLookupCode", nillable = true)
    protected String transferToAccountLookupCode;
    @XmlElement(name = "TransferToAgencyCode", nillable = true)
    protected String transferToAgencyCode;
    @XmlElement(name = "TransferToBranchCode", nillable = true)
    protected String transferToBranchCode;

    /**
     * Gets the value of the transferAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    /**
     * Sets the value of the transferAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransferAmount(BigDecimal value) {
        this.transferAmount = value;
    }

    /**
     * Gets the value of the transferFromTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransferFromTransactionID() {
        return transferFromTransactionID;
    }

    /**
     * Sets the value of the transferFromTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransferFromTransactionID(Integer value) {
        this.transferFromTransactionID = value;
    }

    /**
     * Gets the value of the transferToAccountLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToAccountLookupCode() {
        return transferToAccountLookupCode;
    }

    /**
     * Sets the value of the transferToAccountLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToAccountLookupCode(String value) {
        this.transferToAccountLookupCode = value;
    }

    /**
     * Gets the value of the transferToAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToAgencyCode() {
        return transferToAgencyCode;
    }

    /**
     * Sets the value of the transferToAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToAgencyCode(String value) {
        this.transferToAgencyCode = value;
    }

    /**
     * Gets the value of the transferToBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToBranchCode() {
        return transferToBranchCode;
    }

    /**
     * Sets the value of the transferToBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToBranchCode(String value) {
        this.transferToBranchCode = value;
    }

}
