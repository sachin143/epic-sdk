
package com.appliedsystems.schemas.epic.sdk._2009._07._fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InputValidationFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InputValidationFault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExceptionFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsLengthExceededFault" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MaximumValueorLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InputValidationFault", propOrder = {
    "description",
    "exceptionFile",
    "fieldName",
    "isLengthExceededFault",
    "maximumValueorLength",
    "methodName"
})
public class InputValidationFault {

    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "ExceptionFile", nillable = true)
    protected String exceptionFile;
    @XmlElement(name = "FieldName", nillable = true)
    protected String fieldName;
    @XmlElement(name = "IsLengthExceededFault")
    protected Boolean isLengthExceededFault;
    @XmlElement(name = "MaximumValueorLength", nillable = true)
    protected String maximumValueorLength;
    @XmlElement(name = "MethodName", nillable = true)
    protected String methodName;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the exceptionFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionFile() {
        return exceptionFile;
    }

    /**
     * Sets the value of the exceptionFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionFile(String value) {
        this.exceptionFile = value;
    }

    /**
     * Gets the value of the fieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Sets the value of the fieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldName(String value) {
        this.fieldName = value;
    }

    /**
     * Gets the value of the isLengthExceededFault property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLengthExceededFault() {
        return isLengthExceededFault;
    }

    /**
     * Sets the value of the isLengthExceededFault property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLengthExceededFault(Boolean value) {
        this.isLengthExceededFault = value;
    }

    /**
     * Gets the value of the maximumValueorLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumValueorLength() {
        return maximumValueorLength;
    }

    /**
     * Sets the value of the maximumValueorLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumValueorLength(String value) {
        this.maximumValueorLength = value;
    }

    /**
     * Gets the value of the methodName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Sets the value of the methodName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodName(String value) {
        this.methodName = value;
    }

}
