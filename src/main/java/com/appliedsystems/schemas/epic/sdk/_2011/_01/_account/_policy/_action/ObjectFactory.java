
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EndorseReviseExistingLine_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "EndorseReviseExistingLine");
    private final static QName _Renew_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "Renew");
    private final static QName _ArrayOfIssueCancellation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfIssueCancellation");
    private final static QName _Reinstate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "Reinstate");
    private final static QName _IssueNotIssuePolicyGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "IssueNotIssuePolicyGetType");
    private final static QName _IssueNotIssuePolicy_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "IssueNotIssuePolicy");
    private final static QName _ArrayOfReinstate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfReinstate");
    private final static QName _IssueNotIssueEndorsement_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "IssueNotIssueEndorsement");
    private final static QName _CancellationRequestReasonMethodGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "CancellationRequestReasonMethodGetType");
    private final static QName _ChangeServiceSummaryDescription_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ChangeServiceSummaryDescription");
    private final static QName _IssueNotIssueEndorsementGetType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "IssueNotIssueEndorsementGetType");
    private final static QName _ChangePolicyEffectiveExpirationDates_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ChangePolicyEffectiveExpirationDates");
    private final static QName _ArrayOfCancel_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfCancel");
    private final static QName _Cancel_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "Cancel");
    private final static QName _ArrayOfChangePolicyEffectiveExpirationDates_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfChangePolicyEffectiveExpirationDates");
    private final static QName _ArrayOfIssueNotIssueEndorsement_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfIssueNotIssueEndorsement");
    private final static QName _ArrayOfIssueNotIssuePolicy_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfIssueNotIssuePolicy");
    private final static QName _IssueCancellation_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "IssueCancellation");
    private final static QName _ArrayOfRenew_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", "ArrayOfRenew");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfCancel }
     * 
     */
    public ArrayOfCancel createArrayOfCancel() {
        return new ArrayOfCancel();
    }

    /**
     * Create an instance of {@link IssueNotIssueEndorsement }
     * 
     */
    public IssueNotIssueEndorsement createIssueNotIssueEndorsement() {
        return new IssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link ArrayOfIssueNotIssuePolicy }
     * 
     */
    public ArrayOfIssueNotIssuePolicy createArrayOfIssueNotIssuePolicy() {
        return new ArrayOfIssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link ArrayOfReinstate }
     * 
     */
    public ArrayOfReinstate createArrayOfReinstate() {
        return new ArrayOfReinstate();
    }

    /**
     * Create an instance of {@link IssueCancellation }
     * 
     */
    public IssueCancellation createIssueCancellation() {
        return new IssueCancellation();
    }

    /**
     * Create an instance of {@link ChangePolicyEffectiveExpirationDates }
     * 
     */
    public ChangePolicyEffectiveExpirationDates createChangePolicyEffectiveExpirationDates() {
        return new ChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link ArrayOfIssueCancellation }
     * 
     */
    public ArrayOfIssueCancellation createArrayOfIssueCancellation() {
        return new ArrayOfIssueCancellation();
    }

    /**
     * Create an instance of {@link ArrayOfChangePolicyEffectiveExpirationDates }
     * 
     */
    public ArrayOfChangePolicyEffectiveExpirationDates createArrayOfChangePolicyEffectiveExpirationDates() {
        return new ArrayOfChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link Renew }
     * 
     */
    public Renew createRenew() {
        return new Renew();
    }

    /**
     * Create an instance of {@link ArrayOfIssueNotIssueEndorsement }
     * 
     */
    public ArrayOfIssueNotIssueEndorsement createArrayOfIssueNotIssueEndorsement() {
        return new ArrayOfIssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link IssueNotIssuePolicy }
     * 
     */
    public IssueNotIssuePolicy createIssueNotIssuePolicy() {
        return new IssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link Cancel }
     * 
     */
    public Cancel createCancel() {
        return new Cancel();
    }

    /**
     * Create an instance of {@link EndorseReviseExistingLine }
     * 
     */
    public EndorseReviseExistingLine createEndorseReviseExistingLine() {
        return new EndorseReviseExistingLine();
    }

    /**
     * Create an instance of {@link ChangeServiceSummaryDescription }
     * 
     */
    public ChangeServiceSummaryDescription createChangeServiceSummaryDescription() {
        return new ChangeServiceSummaryDescription();
    }

    /**
     * Create an instance of {@link ArrayOfRenew }
     * 
     */
    public ArrayOfRenew createArrayOfRenew() {
        return new ArrayOfRenew();
    }

    /**
     * Create an instance of {@link Reinstate }
     * 
     */
    public Reinstate createReinstate() {
        return new Reinstate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EndorseReviseExistingLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "EndorseReviseExistingLine")
    public JAXBElement<EndorseReviseExistingLine> createEndorseReviseExistingLine(EndorseReviseExistingLine value) {
        return new JAXBElement<EndorseReviseExistingLine>(_EndorseReviseExistingLine_QNAME, EndorseReviseExistingLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Renew }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "Renew")
    public JAXBElement<Renew> createRenew(Renew value) {
        return new JAXBElement<Renew>(_Renew_QNAME, Renew.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIssueCancellation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfIssueCancellation")
    public JAXBElement<ArrayOfIssueCancellation> createArrayOfIssueCancellation(ArrayOfIssueCancellation value) {
        return new JAXBElement<ArrayOfIssueCancellation>(_ArrayOfIssueCancellation_QNAME, ArrayOfIssueCancellation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reinstate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "Reinstate")
    public JAXBElement<Reinstate> createReinstate(Reinstate value) {
        return new JAXBElement<Reinstate>(_Reinstate_QNAME, Reinstate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueNotIssuePolicyGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "IssueNotIssuePolicyGetType")
    public JAXBElement<IssueNotIssuePolicyGetType> createIssueNotIssuePolicyGetType(IssueNotIssuePolicyGetType value) {
        return new JAXBElement<IssueNotIssuePolicyGetType>(_IssueNotIssuePolicyGetType_QNAME, IssueNotIssuePolicyGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueNotIssuePolicy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "IssueNotIssuePolicy")
    public JAXBElement<IssueNotIssuePolicy> createIssueNotIssuePolicy(IssueNotIssuePolicy value) {
        return new JAXBElement<IssueNotIssuePolicy>(_IssueNotIssuePolicy_QNAME, IssueNotIssuePolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReinstate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfReinstate")
    public JAXBElement<ArrayOfReinstate> createArrayOfReinstate(ArrayOfReinstate value) {
        return new JAXBElement<ArrayOfReinstate>(_ArrayOfReinstate_QNAME, ArrayOfReinstate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueNotIssueEndorsement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "IssueNotIssueEndorsement")
    public JAXBElement<IssueNotIssueEndorsement> createIssueNotIssueEndorsement(IssueNotIssueEndorsement value) {
        return new JAXBElement<IssueNotIssueEndorsement>(_IssueNotIssueEndorsement_QNAME, IssueNotIssueEndorsement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancellationRequestReasonMethodGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "CancellationRequestReasonMethodGetType")
    public JAXBElement<CancellationRequestReasonMethodGetType> createCancellationRequestReasonMethodGetType(CancellationRequestReasonMethodGetType value) {
        return new JAXBElement<CancellationRequestReasonMethodGetType>(_CancellationRequestReasonMethodGetType_QNAME, CancellationRequestReasonMethodGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeServiceSummaryDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ChangeServiceSummaryDescription")
    public JAXBElement<ChangeServiceSummaryDescription> createChangeServiceSummaryDescription(ChangeServiceSummaryDescription value) {
        return new JAXBElement<ChangeServiceSummaryDescription>(_ChangeServiceSummaryDescription_QNAME, ChangeServiceSummaryDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueNotIssueEndorsementGetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "IssueNotIssueEndorsementGetType")
    public JAXBElement<IssueNotIssueEndorsementGetType> createIssueNotIssueEndorsementGetType(IssueNotIssueEndorsementGetType value) {
        return new JAXBElement<IssueNotIssueEndorsementGetType>(_IssueNotIssueEndorsementGetType_QNAME, IssueNotIssueEndorsementGetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePolicyEffectiveExpirationDates }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ChangePolicyEffectiveExpirationDates")
    public JAXBElement<ChangePolicyEffectiveExpirationDates> createChangePolicyEffectiveExpirationDates(ChangePolicyEffectiveExpirationDates value) {
        return new JAXBElement<ChangePolicyEffectiveExpirationDates>(_ChangePolicyEffectiveExpirationDates_QNAME, ChangePolicyEffectiveExpirationDates.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCancel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfCancel")
    public JAXBElement<ArrayOfCancel> createArrayOfCancel(ArrayOfCancel value) {
        return new JAXBElement<ArrayOfCancel>(_ArrayOfCancel_QNAME, ArrayOfCancel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cancel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "Cancel")
    public JAXBElement<Cancel> createCancel(Cancel value) {
        return new JAXBElement<Cancel>(_Cancel_QNAME, Cancel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChangePolicyEffectiveExpirationDates }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfChangePolicyEffectiveExpirationDates")
    public JAXBElement<ArrayOfChangePolicyEffectiveExpirationDates> createArrayOfChangePolicyEffectiveExpirationDates(ArrayOfChangePolicyEffectiveExpirationDates value) {
        return new JAXBElement<ArrayOfChangePolicyEffectiveExpirationDates>(_ArrayOfChangePolicyEffectiveExpirationDates_QNAME, ArrayOfChangePolicyEffectiveExpirationDates.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIssueNotIssueEndorsement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfIssueNotIssueEndorsement")
    public JAXBElement<ArrayOfIssueNotIssueEndorsement> createArrayOfIssueNotIssueEndorsement(ArrayOfIssueNotIssueEndorsement value) {
        return new JAXBElement<ArrayOfIssueNotIssueEndorsement>(_ArrayOfIssueNotIssueEndorsement_QNAME, ArrayOfIssueNotIssueEndorsement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIssueNotIssuePolicy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfIssueNotIssuePolicy")
    public JAXBElement<ArrayOfIssueNotIssuePolicy> createArrayOfIssueNotIssuePolicy(ArrayOfIssueNotIssuePolicy value) {
        return new JAXBElement<ArrayOfIssueNotIssuePolicy>(_ArrayOfIssueNotIssuePolicy_QNAME, ArrayOfIssueNotIssuePolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueCancellation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "IssueCancellation")
    public JAXBElement<IssueCancellation> createIssueCancellation(IssueCancellation value) {
        return new JAXBElement<IssueCancellation>(_IssueCancellation_QNAME, IssueCancellation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRenew }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/", name = "ArrayOfRenew")
    public JAXBElement<ArrayOfRenew> createArrayOfRenew(ArrayOfRenew value) {
        return new JAXBElement<ArrayOfRenew>(_ArrayOfRenew_QNAME, ArrayOfRenew.class, null, value);
    }

}
