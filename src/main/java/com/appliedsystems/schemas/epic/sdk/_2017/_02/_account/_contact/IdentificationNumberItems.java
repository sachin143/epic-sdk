
package com.appliedsystems.schemas.epic.sdk._2017._02._account._contact;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationNumberItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationNumberItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificationNumberItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_contact/}IdentificationNumberItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationNumberItems", propOrder = {
    "identificationNumberItems"
})
public class IdentificationNumberItems {

    @XmlElement(name = "IdentificationNumberItem", nillable = true)
    protected List<IdentificationNumberItem> identificationNumberItems;

    /**
     * Gets the value of the identificationNumberItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identificationNumberItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentificationNumberItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificationNumberItem }
     * 
     * 
     */
    public List<IdentificationNumberItem> getIdentificationNumberItems() {
        if (identificationNumberItems == null) {
            identificationNumberItems = new ArrayList<IdentificationNumberItem>();
        }
        return this.identificationNumberItems;
    }

}
