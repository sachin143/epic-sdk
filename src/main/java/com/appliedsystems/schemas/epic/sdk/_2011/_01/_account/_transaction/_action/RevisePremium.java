
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.Installments;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.SplitReceivable;


/**
 * <p>Java class for RevisePremium complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RevisePremium">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyReverseSplitReceivablesInstallments" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DetailARDueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DetailAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DetailGenerateInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DetailProductionMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreFlatCommission" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IgnorePayableContractPayableDueDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IgnorePayableDueDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="InstallmentsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="InstallmentsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/}Installments" minOccurs="0"/>
 *         &lt;element name="IsInstallment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsSplitReceivable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SplitReceivableValue" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_revisepremium/}SplitReceivable" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TransactionAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionReallocateOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RevisePremium", propOrder = {
    "associatedAccountID",
    "associatedAccountTypeCode",
    "automaticallyReverseSplitReceivablesInstallments",
    "detailARDueDate",
    "detailAccountingMonth",
    "detailGenerateInvoiceDate",
    "detailProductionMonth",
    "ignoreFlatCommission",
    "ignorePayableContractPayableDueDate",
    "ignorePayableDueDate",
    "installmentsOption",
    "installmentsValue",
    "isInstallment",
    "isSplitReceivable",
    "splitReceivableValue",
    "timestamp",
    "transactionAmount",
    "transactionCode",
    "transactionDescription",
    "transactionID",
    "transactionReallocateOption"
})
public class RevisePremium {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "AutomaticallyReverseSplitReceivablesInstallments")
    protected Boolean automaticallyReverseSplitReceivablesInstallments;
    @XmlElement(name = "DetailARDueDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar detailARDueDate;
    @XmlElement(name = "DetailAccountingMonth", nillable = true)
    protected String detailAccountingMonth;
    @XmlElement(name = "DetailGenerateInvoiceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar detailGenerateInvoiceDate;
    @XmlElement(name = "DetailProductionMonth", nillable = true)
    protected String detailProductionMonth;
    @XmlElement(name = "IgnoreFlatCommission")
    protected Boolean ignoreFlatCommission;
    @XmlElement(name = "IgnorePayableContractPayableDueDate")
    protected Boolean ignorePayableContractPayableDueDate;
    @XmlElement(name = "IgnorePayableDueDate")
    protected Boolean ignorePayableDueDate;
    @XmlElement(name = "InstallmentsOption", nillable = true)
    protected OptionType installmentsOption;
    @XmlElement(name = "InstallmentsValue", nillable = true)
    protected Installments installmentsValue;
    @XmlElement(name = "IsInstallment")
    protected Boolean isInstallment;
    @XmlElement(name = "IsSplitReceivable")
    protected Boolean isSplitReceivable;
    @XmlElement(name = "SplitReceivableValue", nillable = true)
    protected SplitReceivable splitReceivableValue;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "TransactionAmount")
    protected BigDecimal transactionAmount;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionDescription", nillable = true)
    protected String transactionDescription;
    @XmlElement(name = "TransactionID")
    protected Integer transactionID;
    @XmlElement(name = "TransactionReallocateOption", nillable = true)
    protected OptionType transactionReallocateOption;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the automaticallyReverseSplitReceivablesInstallments property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutomaticallyReverseSplitReceivablesInstallments() {
        return automaticallyReverseSplitReceivablesInstallments;
    }

    /**
     * Sets the value of the automaticallyReverseSplitReceivablesInstallments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticallyReverseSplitReceivablesInstallments(Boolean value) {
        this.automaticallyReverseSplitReceivablesInstallments = value;
    }

    /**
     * Gets the value of the detailARDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDetailARDueDate() {
        return detailARDueDate;
    }

    /**
     * Sets the value of the detailARDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDetailARDueDate(XMLGregorianCalendar value) {
        this.detailARDueDate = value;
    }

    /**
     * Gets the value of the detailAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailAccountingMonth() {
        return detailAccountingMonth;
    }

    /**
     * Sets the value of the detailAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailAccountingMonth(String value) {
        this.detailAccountingMonth = value;
    }

    /**
     * Gets the value of the detailGenerateInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDetailGenerateInvoiceDate() {
        return detailGenerateInvoiceDate;
    }

    /**
     * Sets the value of the detailGenerateInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDetailGenerateInvoiceDate(XMLGregorianCalendar value) {
        this.detailGenerateInvoiceDate = value;
    }

    /**
     * Gets the value of the detailProductionMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailProductionMonth() {
        return detailProductionMonth;
    }

    /**
     * Sets the value of the detailProductionMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailProductionMonth(String value) {
        this.detailProductionMonth = value;
    }

    /**
     * Gets the value of the ignoreFlatCommission property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreFlatCommission() {
        return ignoreFlatCommission;
    }

    /**
     * Sets the value of the ignoreFlatCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreFlatCommission(Boolean value) {
        this.ignoreFlatCommission = value;
    }

    /**
     * Gets the value of the ignorePayableContractPayableDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnorePayableContractPayableDueDate() {
        return ignorePayableContractPayableDueDate;
    }

    /**
     * Sets the value of the ignorePayableContractPayableDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnorePayableContractPayableDueDate(Boolean value) {
        this.ignorePayableContractPayableDueDate = value;
    }

    /**
     * Gets the value of the ignorePayableDueDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnorePayableDueDate() {
        return ignorePayableDueDate;
    }

    /**
     * Sets the value of the ignorePayableDueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnorePayableDueDate(Boolean value) {
        this.ignorePayableDueDate = value;
    }

    /**
     * Gets the value of the installmentsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getInstallmentsOption() {
        return installmentsOption;
    }

    /**
     * Sets the value of the installmentsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setInstallmentsOption(OptionType value) {
        this.installmentsOption = value;
    }

    /**
     * Gets the value of the installmentsValue property.
     * 
     * @return
     *     possible object is
     *     {@link Installments }
     *     
     */
    public Installments getInstallmentsValue() {
        return installmentsValue;
    }

    /**
     * Sets the value of the installmentsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Installments }
     *     
     */
    public void setInstallmentsValue(Installments value) {
        this.installmentsValue = value;
    }

    /**
     * Gets the value of the isInstallment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInstallment() {
        return isInstallment;
    }

    /**
     * Sets the value of the isInstallment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInstallment(Boolean value) {
        this.isInstallment = value;
    }

    /**
     * Gets the value of the isSplitReceivable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSplitReceivable() {
        return isSplitReceivable;
    }

    /**
     * Sets the value of the isSplitReceivable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSplitReceivable(Boolean value) {
        this.isSplitReceivable = value;
    }

    /**
     * Gets the value of the splitReceivableValue property.
     * 
     * @return
     *     possible object is
     *     {@link SplitReceivable }
     *     
     */
    public SplitReceivable getSplitReceivableValue() {
        return splitReceivableValue;
    }

    /**
     * Sets the value of the splitReceivableValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitReceivable }
     *     
     */
    public void setSplitReceivableValue(SplitReceivable value) {
        this.splitReceivableValue = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the transactionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Sets the value of the transactionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransactionAmount(BigDecimal value) {
        this.transactionAmount = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Sets the value of the transactionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDescription(String value) {
        this.transactionDescription = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionReallocateOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getTransactionReallocateOption() {
        return transactionReallocateOption;
    }

    /**
     * Sets the value of the transactionReallocateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setTransactionReallocateOption(OptionType value) {
        this.transactionReallocateOption = value;
    }

}
