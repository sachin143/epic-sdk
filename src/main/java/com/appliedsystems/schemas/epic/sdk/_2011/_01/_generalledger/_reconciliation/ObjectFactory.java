
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfDirectBillCommission_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/", "ArrayOfDirectBillCommission");
    private final static QName _DirectBillCommission_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/", "DirectBillCommission");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DirectBillCommission }
     * 
     */
    public DirectBillCommission createDirectBillCommission() {
        return new DirectBillCommission();
    }

    /**
     * Create an instance of {@link ArrayOfDirectBillCommission }
     * 
     */
    public ArrayOfDirectBillCommission createArrayOfDirectBillCommission() {
        return new ArrayOfDirectBillCommission();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDirectBillCommission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/", name = "ArrayOfDirectBillCommission")
    public JAXBElement<ArrayOfDirectBillCommission> createArrayOfDirectBillCommission(ArrayOfDirectBillCommission value) {
        return new JAXBElement<ArrayOfDirectBillCommission>(_ArrayOfDirectBillCommission_QNAME, ArrayOfDirectBillCommission.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectBillCommission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/", name = "DirectBillCommission")
    public JAXBElement<DirectBillCommission> createDirectBillCommission(DirectBillCommission value) {
        return new JAXBElement<DirectBillCommission>(_DirectBillCommission_QNAME, DirectBillCommission.class, null, value);
    }

}
