
package com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SplitReceivableItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/", "SplitReceivableItems");
    private final static QName _SplitReceivableItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/", "SplitReceivableItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SplitReceivableItems }
     * 
     */
    public SplitReceivableItems createSplitReceivableItems() {
        return new SplitReceivableItems();
    }

    /**
     * Create an instance of {@link SplitReceivableItem }
     * 
     */
    public SplitReceivableItem createSplitReceivableItem() {
        return new SplitReceivableItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/", name = "SplitReceivableItems")
    public JAXBElement<SplitReceivableItems> createSplitReceivableItems(SplitReceivableItems value) {
        return new JAXBElement<SplitReceivableItems>(_SplitReceivableItems_QNAME, SplitReceivableItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitReceivableItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/_splitreceivabletemplate/", name = "SplitReceivableItem")
    public JAXBElement<SplitReceivableItem> createSplitReceivableItem(SplitReceivableItem value) {
        return new JAXBElement<SplitReceivableItem>(_SplitReceivableItem_QNAME, SplitReceivableItem.class, null, value);
    }

}
