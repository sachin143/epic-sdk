
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisbursementGetLimitType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DisbursementGetLimitType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="LastSixMonths"/>
 *     &lt;enumeration value="Search"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DisbursementGetLimitType")
@XmlEnum
public enum DisbursementGetLimitType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("LastSixMonths")
    LAST_SIX_MONTHS("LastSixMonths"),
    @XmlEnumValue("Search")
    SEARCH("Search");
    private final String value;

    DisbursementGetLimitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DisbursementGetLimitType fromValue(String v) {
        for (DisbursementGetLimitType c: DisbursementGetLimitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
