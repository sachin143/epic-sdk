
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GenerateTaxFeeItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/", "GenerateTaxFeeItems");
    private final static QName _GenerateTaxFeeItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/", "GenerateTaxFeeItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerateTaxFeeItems }
     * 
     */
    public GenerateTaxFeeItems createGenerateTaxFeeItems() {
        return new GenerateTaxFeeItems();
    }

    /**
     * Create an instance of {@link GenerateTaxFeeItem }
     * 
     */
    public GenerateTaxFeeItem createGenerateTaxFeeItem() {
        return new GenerateTaxFeeItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateTaxFeeItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/", name = "GenerateTaxFeeItems")
    public JAXBElement<GenerateTaxFeeItems> createGenerateTaxFeeItems(GenerateTaxFeeItems value) {
        return new JAXBElement<GenerateTaxFeeItems>(_GenerateTaxFeeItems_QNAME, GenerateTaxFeeItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateTaxFeeItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_generatetaxfee/", name = "GenerateTaxFeeItem")
    public JAXBElement<GenerateTaxFeeItem> createGenerateTaxFeeItem(GenerateTaxFeeItem value) {
        return new JAXBElement<GenerateTaxFeeItem>(_GenerateTaxFeeItem_QNAME, GenerateTaxFeeItem.class, null, value);
    }

}
