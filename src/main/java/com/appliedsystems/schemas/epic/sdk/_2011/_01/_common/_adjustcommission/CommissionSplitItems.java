
package com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommissionSplitItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommissionSplitItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommissionSplitItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/_adjustcommission/}CommissionSplitItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommissionSplitItems", propOrder = {
    "commissionSplitItems"
})
public class CommissionSplitItems {

    @XmlElement(name = "CommissionSplitItem", nillable = true)
    protected List<CommissionSplitItem> commissionSplitItems;

    /**
     * Gets the value of the commissionSplitItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commissionSplitItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommissionSplitItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommissionSplitItem }
     * 
     * 
     */
    public List<CommissionSplitItem> getCommissionSplitItems() {
        if (commissionSplitItems == null) {
            commissionSplitItems = new ArrayList<CommissionSplitItem>();
        }
        return this.commissionSplitItems;
    }

}
