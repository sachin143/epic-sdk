
package com.appliedsystems.schemas.epic.sdk._2009._07._account._contact;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingContactMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactVia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email1Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email2Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MarketingContactMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1CallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1NumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2CallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2NumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3CallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3NumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4CallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4NumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5CallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5Extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5NumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicingContactMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Website" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WebsiteDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone3CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone4CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone5CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MarketingOptInEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOptInFax" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOptInMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOptInPhone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOptInSMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Phone1SMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Phone2SMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Phone3SMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Phone4SMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Phone5SMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOptInThirdParty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ConversationalSMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInformation", propOrder = {
    "billingContactMethod",
    "contactVia",
    "email1",
    "email1Description",
    "email2",
    "email2Description",
    "language",
    "marketingContactMethod",
    "phone1CallPermission",
    "phone1Description",
    "phone1Extension",
    "phone1Number",
    "phone1NumberType",
    "phone2CallPermission",
    "phone2Description",
    "phone2Extension",
    "phone2Number",
    "phone2NumberType",
    "phone3CallPermission",
    "phone3Description",
    "phone3Extension",
    "phone3Number",
    "phone3NumberType",
    "phone4CallPermission",
    "phone4Description",
    "phone4Extension",
    "phone4Number",
    "phone4NumberType",
    "phone5CallPermission",
    "phone5Description",
    "phone5Extension",
    "phone5Number",
    "phone5NumberType",
    "servicingContactMethod",
    "website",
    "websiteDescription",
    "phone1CountryCode",
    "phone2CountryCode",
    "phone3CountryCode",
    "phone4CountryCode",
    "phone5CountryCode",
    "marketingOptInEmail",
    "marketingOptInFax",
    "marketingOptInMail",
    "marketingOptInPhone",
    "marketingOptInSMS",
    "phone1SMS",
    "phone2SMS",
    "phone3SMS",
    "phone4SMS",
    "phone5SMS",
    "marketingOptInThirdParty",
    "conversationalSMS"
})
public class ContactInformation {

    @XmlElement(name = "BillingContactMethod", nillable = true)
    protected String billingContactMethod;
    @XmlElement(name = "ContactVia", nillable = true)
    protected String contactVia;
    @XmlElement(name = "Email1", nillable = true)
    protected String email1;
    @XmlElement(name = "Email1Description", nillable = true)
    protected String email1Description;
    @XmlElement(name = "Email2", nillable = true)
    protected String email2;
    @XmlElement(name = "Email2Description", nillable = true)
    protected String email2Description;
    @XmlElement(name = "Language", nillable = true)
    protected String language;
    @XmlElement(name = "MarketingContactMethod", nillable = true)
    protected String marketingContactMethod;
    @XmlElement(name = "Phone1CallPermission", nillable = true)
    protected String phone1CallPermission;
    @XmlElement(name = "Phone1Description", nillable = true)
    protected String phone1Description;
    @XmlElement(name = "Phone1Extension", nillable = true)
    protected String phone1Extension;
    @XmlElement(name = "Phone1Number", nillable = true)
    protected String phone1Number;
    @XmlElement(name = "Phone1NumberType", nillable = true)
    protected String phone1NumberType;
    @XmlElement(name = "Phone2CallPermission", nillable = true)
    protected String phone2CallPermission;
    @XmlElement(name = "Phone2Description", nillable = true)
    protected String phone2Description;
    @XmlElement(name = "Phone2Extension", nillable = true)
    protected String phone2Extension;
    @XmlElement(name = "Phone2Number", nillable = true)
    protected String phone2Number;
    @XmlElement(name = "Phone2NumberType", nillable = true)
    protected String phone2NumberType;
    @XmlElement(name = "Phone3CallPermission", nillable = true)
    protected String phone3CallPermission;
    @XmlElement(name = "Phone3Description", nillable = true)
    protected String phone3Description;
    @XmlElement(name = "Phone3Extension", nillable = true)
    protected String phone3Extension;
    @XmlElement(name = "Phone3Number", nillable = true)
    protected String phone3Number;
    @XmlElement(name = "Phone3NumberType", nillable = true)
    protected String phone3NumberType;
    @XmlElement(name = "Phone4CallPermission", nillable = true)
    protected String phone4CallPermission;
    @XmlElement(name = "Phone4Description", nillable = true)
    protected String phone4Description;
    @XmlElement(name = "Phone4Extension", nillable = true)
    protected String phone4Extension;
    @XmlElement(name = "Phone4Number", nillable = true)
    protected String phone4Number;
    @XmlElement(name = "Phone4NumberType", nillable = true)
    protected String phone4NumberType;
    @XmlElement(name = "Phone5CallPermission", nillable = true)
    protected String phone5CallPermission;
    @XmlElement(name = "Phone5Description", nillable = true)
    protected String phone5Description;
    @XmlElement(name = "Phone5Extension", nillable = true)
    protected String phone5Extension;
    @XmlElement(name = "Phone5Number", nillable = true)
    protected String phone5Number;
    @XmlElement(name = "Phone5NumberType", nillable = true)
    protected String phone5NumberType;
    @XmlElement(name = "ServicingContactMethod", nillable = true)
    protected String servicingContactMethod;
    @XmlElement(name = "Website", nillable = true)
    protected String website;
    @XmlElement(name = "WebsiteDescription", nillable = true)
    protected String websiteDescription;
    @XmlElement(name = "Phone1CountryCode", nillable = true)
    protected String phone1CountryCode;
    @XmlElement(name = "Phone2CountryCode", nillable = true)
    protected String phone2CountryCode;
    @XmlElement(name = "Phone3CountryCode", nillable = true)
    protected String phone3CountryCode;
    @XmlElement(name = "Phone4CountryCode", nillable = true)
    protected String phone4CountryCode;
    @XmlElement(name = "Phone5CountryCode", nillable = true)
    protected String phone5CountryCode;
    @XmlElement(name = "MarketingOptInEmail")
    protected Boolean marketingOptInEmail;
    @XmlElement(name = "MarketingOptInFax")
    protected Boolean marketingOptInFax;
    @XmlElement(name = "MarketingOptInMail")
    protected Boolean marketingOptInMail;
    @XmlElement(name = "MarketingOptInPhone")
    protected Boolean marketingOptInPhone;
    @XmlElement(name = "MarketingOptInSMS")
    protected Boolean marketingOptInSMS;
    @XmlElement(name = "Phone1SMS")
    protected Boolean phone1SMS;
    @XmlElement(name = "Phone2SMS")
    protected Boolean phone2SMS;
    @XmlElement(name = "Phone3SMS")
    protected Boolean phone3SMS;
    @XmlElement(name = "Phone4SMS")
    protected Boolean phone4SMS;
    @XmlElement(name = "Phone5SMS")
    protected Boolean phone5SMS;
    @XmlElement(name = "MarketingOptInThirdParty")
    protected Boolean marketingOptInThirdParty;
    @XmlElement(name = "ConversationalSMS")
    protected Boolean conversationalSMS;

    /**
     * Gets the value of the billingContactMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingContactMethod() {
        return billingContactMethod;
    }

    /**
     * Sets the value of the billingContactMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingContactMethod(String value) {
        this.billingContactMethod = value;
    }

    /**
     * Gets the value of the contactVia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactVia() {
        return contactVia;
    }

    /**
     * Sets the value of the contactVia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactVia(String value) {
        this.contactVia = value;
    }

    /**
     * Gets the value of the email1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail1() {
        return email1;
    }

    /**
     * Sets the value of the email1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail1(String value) {
        this.email1 = value;
    }

    /**
     * Gets the value of the email1Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail1Description() {
        return email1Description;
    }

    /**
     * Sets the value of the email1Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail1Description(String value) {
        this.email1Description = value;
    }

    /**
     * Gets the value of the email2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail2() {
        return email2;
    }

    /**
     * Sets the value of the email2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail2(String value) {
        this.email2 = value;
    }

    /**
     * Gets the value of the email2Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail2Description() {
        return email2Description;
    }

    /**
     * Sets the value of the email2Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail2Description(String value) {
        this.email2Description = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the marketingContactMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingContactMethod() {
        return marketingContactMethod;
    }

    /**
     * Sets the value of the marketingContactMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingContactMethod(String value) {
        this.marketingContactMethod = value;
    }

    /**
     * Gets the value of the phone1CallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1CallPermission() {
        return phone1CallPermission;
    }

    /**
     * Sets the value of the phone1CallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1CallPermission(String value) {
        this.phone1CallPermission = value;
    }

    /**
     * Gets the value of the phone1Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1Description() {
        return phone1Description;
    }

    /**
     * Sets the value of the phone1Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1Description(String value) {
        this.phone1Description = value;
    }

    /**
     * Gets the value of the phone1Extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1Extension() {
        return phone1Extension;
    }

    /**
     * Sets the value of the phone1Extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1Extension(String value) {
        this.phone1Extension = value;
    }

    /**
     * Gets the value of the phone1Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1Number() {
        return phone1Number;
    }

    /**
     * Sets the value of the phone1Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1Number(String value) {
        this.phone1Number = value;
    }

    /**
     * Gets the value of the phone1NumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1NumberType() {
        return phone1NumberType;
    }

    /**
     * Sets the value of the phone1NumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1NumberType(String value) {
        this.phone1NumberType = value;
    }

    /**
     * Gets the value of the phone2CallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2CallPermission() {
        return phone2CallPermission;
    }

    /**
     * Sets the value of the phone2CallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2CallPermission(String value) {
        this.phone2CallPermission = value;
    }

    /**
     * Gets the value of the phone2Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2Description() {
        return phone2Description;
    }

    /**
     * Sets the value of the phone2Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2Description(String value) {
        this.phone2Description = value;
    }

    /**
     * Gets the value of the phone2Extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2Extension() {
        return phone2Extension;
    }

    /**
     * Sets the value of the phone2Extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2Extension(String value) {
        this.phone2Extension = value;
    }

    /**
     * Gets the value of the phone2Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2Number() {
        return phone2Number;
    }

    /**
     * Sets the value of the phone2Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2Number(String value) {
        this.phone2Number = value;
    }

    /**
     * Gets the value of the phone2NumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2NumberType() {
        return phone2NumberType;
    }

    /**
     * Sets the value of the phone2NumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2NumberType(String value) {
        this.phone2NumberType = value;
    }

    /**
     * Gets the value of the phone3CallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3CallPermission() {
        return phone3CallPermission;
    }

    /**
     * Sets the value of the phone3CallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3CallPermission(String value) {
        this.phone3CallPermission = value;
    }

    /**
     * Gets the value of the phone3Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3Description() {
        return phone3Description;
    }

    /**
     * Sets the value of the phone3Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3Description(String value) {
        this.phone3Description = value;
    }

    /**
     * Gets the value of the phone3Extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3Extension() {
        return phone3Extension;
    }

    /**
     * Sets the value of the phone3Extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3Extension(String value) {
        this.phone3Extension = value;
    }

    /**
     * Gets the value of the phone3Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3Number() {
        return phone3Number;
    }

    /**
     * Sets the value of the phone3Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3Number(String value) {
        this.phone3Number = value;
    }

    /**
     * Gets the value of the phone3NumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3NumberType() {
        return phone3NumberType;
    }

    /**
     * Sets the value of the phone3NumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3NumberType(String value) {
        this.phone3NumberType = value;
    }

    /**
     * Gets the value of the phone4CallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4CallPermission() {
        return phone4CallPermission;
    }

    /**
     * Sets the value of the phone4CallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4CallPermission(String value) {
        this.phone4CallPermission = value;
    }

    /**
     * Gets the value of the phone4Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4Description() {
        return phone4Description;
    }

    /**
     * Sets the value of the phone4Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4Description(String value) {
        this.phone4Description = value;
    }

    /**
     * Gets the value of the phone4Extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4Extension() {
        return phone4Extension;
    }

    /**
     * Sets the value of the phone4Extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4Extension(String value) {
        this.phone4Extension = value;
    }

    /**
     * Gets the value of the phone4Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4Number() {
        return phone4Number;
    }

    /**
     * Sets the value of the phone4Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4Number(String value) {
        this.phone4Number = value;
    }

    /**
     * Gets the value of the phone4NumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4NumberType() {
        return phone4NumberType;
    }

    /**
     * Sets the value of the phone4NumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4NumberType(String value) {
        this.phone4NumberType = value;
    }

    /**
     * Gets the value of the phone5CallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5CallPermission() {
        return phone5CallPermission;
    }

    /**
     * Sets the value of the phone5CallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5CallPermission(String value) {
        this.phone5CallPermission = value;
    }

    /**
     * Gets the value of the phone5Description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5Description() {
        return phone5Description;
    }

    /**
     * Sets the value of the phone5Description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5Description(String value) {
        this.phone5Description = value;
    }

    /**
     * Gets the value of the phone5Extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5Extension() {
        return phone5Extension;
    }

    /**
     * Sets the value of the phone5Extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5Extension(String value) {
        this.phone5Extension = value;
    }

    /**
     * Gets the value of the phone5Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5Number() {
        return phone5Number;
    }

    /**
     * Sets the value of the phone5Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5Number(String value) {
        this.phone5Number = value;
    }

    /**
     * Gets the value of the phone5NumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5NumberType() {
        return phone5NumberType;
    }

    /**
     * Sets the value of the phone5NumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5NumberType(String value) {
        this.phone5NumberType = value;
    }

    /**
     * Gets the value of the servicingContactMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingContactMethod() {
        return servicingContactMethod;
    }

    /**
     * Sets the value of the servicingContactMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingContactMethod(String value) {
        this.servicingContactMethod = value;
    }

    /**
     * Gets the value of the website property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Sets the value of the website property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsite(String value) {
        this.website = value;
    }

    /**
     * Gets the value of the websiteDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteDescription() {
        return websiteDescription;
    }

    /**
     * Sets the value of the websiteDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteDescription(String value) {
        this.websiteDescription = value;
    }

    /**
     * Gets the value of the phone1CountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1CountryCode() {
        return phone1CountryCode;
    }

    /**
     * Sets the value of the phone1CountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1CountryCode(String value) {
        this.phone1CountryCode = value;
    }

    /**
     * Gets the value of the phone2CountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2CountryCode() {
        return phone2CountryCode;
    }

    /**
     * Sets the value of the phone2CountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2CountryCode(String value) {
        this.phone2CountryCode = value;
    }

    /**
     * Gets the value of the phone3CountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone3CountryCode() {
        return phone3CountryCode;
    }

    /**
     * Sets the value of the phone3CountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone3CountryCode(String value) {
        this.phone3CountryCode = value;
    }

    /**
     * Gets the value of the phone4CountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone4CountryCode() {
        return phone4CountryCode;
    }

    /**
     * Sets the value of the phone4CountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone4CountryCode(String value) {
        this.phone4CountryCode = value;
    }

    /**
     * Gets the value of the phone5CountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone5CountryCode() {
        return phone5CountryCode;
    }

    /**
     * Sets the value of the phone5CountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone5CountryCode(String value) {
        this.phone5CountryCode = value;
    }

    /**
     * Gets the value of the marketingOptInEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInEmail() {
        return marketingOptInEmail;
    }

    /**
     * Sets the value of the marketingOptInEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInEmail(Boolean value) {
        this.marketingOptInEmail = value;
    }

    /**
     * Gets the value of the marketingOptInFax property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInFax() {
        return marketingOptInFax;
    }

    /**
     * Sets the value of the marketingOptInFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInFax(Boolean value) {
        this.marketingOptInFax = value;
    }

    /**
     * Gets the value of the marketingOptInMail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInMail() {
        return marketingOptInMail;
    }

    /**
     * Sets the value of the marketingOptInMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInMail(Boolean value) {
        this.marketingOptInMail = value;
    }

    /**
     * Gets the value of the marketingOptInPhone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInPhone() {
        return marketingOptInPhone;
    }

    /**
     * Sets the value of the marketingOptInPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInPhone(Boolean value) {
        this.marketingOptInPhone = value;
    }

    /**
     * Gets the value of the marketingOptInSMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInSMS() {
        return marketingOptInSMS;
    }

    /**
     * Sets the value of the marketingOptInSMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInSMS(Boolean value) {
        this.marketingOptInSMS = value;
    }

    /**
     * Gets the value of the phone1SMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPhone1SMS() {
        return phone1SMS;
    }

    /**
     * Sets the value of the phone1SMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhone1SMS(Boolean value) {
        this.phone1SMS = value;
    }

    /**
     * Gets the value of the phone2SMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPhone2SMS() {
        return phone2SMS;
    }

    /**
     * Sets the value of the phone2SMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhone2SMS(Boolean value) {
        this.phone2SMS = value;
    }

    /**
     * Gets the value of the phone3SMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPhone3SMS() {
        return phone3SMS;
    }

    /**
     * Sets the value of the phone3SMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhone3SMS(Boolean value) {
        this.phone3SMS = value;
    }

    /**
     * Gets the value of the phone4SMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPhone4SMS() {
        return phone4SMS;
    }

    /**
     * Sets the value of the phone4SMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhone4SMS(Boolean value) {
        this.phone4SMS = value;
    }

    /**
     * Gets the value of the phone5SMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPhone5SMS() {
        return phone5SMS;
    }

    /**
     * Sets the value of the phone5SMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPhone5SMS(Boolean value) {
        this.phone5SMS = value;
    }

    /**
     * Gets the value of the marketingOptInThirdParty property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOptInThirdParty() {
        return marketingOptInThirdParty;
    }

    /**
     * Sets the value of the marketingOptInThirdParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOptInThirdParty(Boolean value) {
        this.marketingOptInThirdParty = value;
    }

    /**
     * Gets the value of the conversationalSMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConversationalSMS() {
        return conversationalSMS;
    }

    /**
     * Sets the value of the conversationalSMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConversationalSMS(Boolean value) {
        this.conversationalSMS = value;
    }

}
