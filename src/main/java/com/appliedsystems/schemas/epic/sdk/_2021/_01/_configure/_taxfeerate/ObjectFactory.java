
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfTaxFeeRate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", "ArrayOfTaxFeeRate");
    private final static QName _TaxFeeRate_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", "TaxFeeRate");
    private final static QName _PolicyItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", "PolicyItem");
    private final static QName _TransactionItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", "TransactionItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionItem }
     * 
     */
    public TransactionItem createTransactionItem() {
        return new TransactionItem();
    }

    /**
     * Create an instance of {@link ArrayOfTaxFeeRate }
     * 
     */
    public ArrayOfTaxFeeRate createArrayOfTaxFeeRate() {
        return new ArrayOfTaxFeeRate();
    }

    /**
     * Create an instance of {@link TaxFeeRate }
     * 
     */
    public TaxFeeRate createTaxFeeRate() {
        return new TaxFeeRate();
    }

    /**
     * Create an instance of {@link PolicyItem }
     * 
     */
    public PolicyItem createPolicyItem() {
        return new PolicyItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTaxFeeRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", name = "ArrayOfTaxFeeRate")
    public JAXBElement<ArrayOfTaxFeeRate> createArrayOfTaxFeeRate(ArrayOfTaxFeeRate value) {
        return new JAXBElement<ArrayOfTaxFeeRate>(_ArrayOfTaxFeeRate_QNAME, ArrayOfTaxFeeRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFeeRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", name = "TaxFeeRate")
    public JAXBElement<TaxFeeRate> createTaxFeeRate(TaxFeeRate value) {
        return new JAXBElement<TaxFeeRate>(_TaxFeeRate_QNAME, TaxFeeRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", name = "PolicyItem")
    public JAXBElement<PolicyItem> createPolicyItem(PolicyItem value) {
        return new JAXBElement<PolicyItem>(_PolicyItem_QNAME, PolicyItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/", name = "TransactionItem")
    public JAXBElement<TransactionItem> createTransactionItem(TransactionItem value) {
        return new JAXBElement<TransactionItem>(_TransactionItem_QNAME, TransactionItem.class, null, value);
    }

}
