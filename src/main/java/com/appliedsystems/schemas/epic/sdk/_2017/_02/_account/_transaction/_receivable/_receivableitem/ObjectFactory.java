
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReceivableColumnItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem", "ReceivableColumnItem");
    private final static QName _ReceivableColumnItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem", "ReceivableColumnItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReceivableColumnItems }
     * 
     */
    public ReceivableColumnItems createReceivableColumnItems() {
        return new ReceivableColumnItems();
    }

    /**
     * Create an instance of {@link ReceivableColumnItem }
     * 
     */
    public ReceivableColumnItem createReceivableColumnItem() {
        return new ReceivableColumnItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableColumnItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem", name = "ReceivableColumnItem")
    public JAXBElement<ReceivableColumnItem> createReceivableColumnItem(ReceivableColumnItem value) {
        return new JAXBElement<ReceivableColumnItem>(_ReceivableColumnItem_QNAME, ReceivableColumnItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceivableColumnItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/_receivable/_receivableitem", name = "ReceivableColumnItems")
    public JAXBElement<ReceivableColumnItems> createReceivableColumnItems(ReceivableColumnItems value) {
        return new JAXBElement<ReceivableColumnItems>(_ReceivableColumnItems_QNAME, ReceivableColumnItems.class, null, value);
    }

}
