
package com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BankAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ABARoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankNumberPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankVendorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BrokerCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChartOfAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CompanyCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DataCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultDisbursementTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultReceiptTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementMappingID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EFT" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="EmployeeCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FinanceCompanyCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FractionalRoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NextAvailableCheckNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OriginatorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OtherInterestCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PrintChecks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReceiptMappingID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServiceClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TransitNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseSystemGeneratedCheckNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="VendorCheckLayoutID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankAccount", propOrder = {
    "abaRoutingNumber",
    "bankAccountNumber",
    "bankNumber",
    "bankNumberPrefix",
    "bankVendorCode",
    "brokerCheckLayoutID",
    "chartOfAccountID",
    "clientCheckLayoutID",
    "companyCheckLayoutID",
    "dataCenterCode",
    "defaultAgencyCode",
    "defaultDisbursementTypeCode",
    "defaultReceiptTypeCode",
    "disbursementMappingID",
    "eft",
    "employeeCheckLayoutID",
    "financeCompanyCheckLayoutID",
    "fractionalRoutingNumber",
    "nextAvailableCheckNumber",
    "originatorID",
    "otherInterestCheckLayoutID",
    "printChecks",
    "receiptMappingID",
    "sequenceNumber",
    "serviceClassCode",
    "timestamp",
    "transitNumber",
    "useSystemGeneratedCheckNumbers",
    "vendorCheckLayoutID"
})
public class BankAccount {

    @XmlElement(name = "ABARoutingNumber", nillable = true)
    protected String abaRoutingNumber;
    @XmlElement(name = "BankAccountNumber", nillable = true)
    protected String bankAccountNumber;
    @XmlElement(name = "BankNumber", nillable = true)
    protected String bankNumber;
    @XmlElement(name = "BankNumberPrefix", nillable = true)
    protected String bankNumberPrefix;
    @XmlElement(name = "BankVendorCode", nillable = true)
    protected String bankVendorCode;
    @XmlElement(name = "BrokerCheckLayoutID", nillable = true)
    protected Integer brokerCheckLayoutID;
    @XmlElement(name = "ChartOfAccountID")
    protected Integer chartOfAccountID;
    @XmlElement(name = "ClientCheckLayoutID", nillable = true)
    protected Integer clientCheckLayoutID;
    @XmlElement(name = "CompanyCheckLayoutID", nillable = true)
    protected Integer companyCheckLayoutID;
    @XmlElement(name = "DataCenterCode", nillable = true)
    protected String dataCenterCode;
    @XmlElement(name = "DefaultAgencyCode", nillable = true)
    protected String defaultAgencyCode;
    @XmlElement(name = "DefaultDisbursementTypeCode", nillable = true)
    protected String defaultDisbursementTypeCode;
    @XmlElement(name = "DefaultReceiptTypeCode", nillable = true)
    protected String defaultReceiptTypeCode;
    @XmlElement(name = "DisbursementMappingID", nillable = true)
    protected Integer disbursementMappingID;
    @XmlElement(name = "EFT")
    protected Boolean eft;
    @XmlElement(name = "EmployeeCheckLayoutID", nillable = true)
    protected Integer employeeCheckLayoutID;
    @XmlElement(name = "FinanceCompanyCheckLayoutID", nillable = true)
    protected Integer financeCompanyCheckLayoutID;
    @XmlElement(name = "FractionalRoutingNumber", nillable = true)
    protected String fractionalRoutingNumber;
    @XmlElement(name = "NextAvailableCheckNumber", nillable = true)
    protected Integer nextAvailableCheckNumber;
    @XmlElement(name = "OriginatorID", nillable = true)
    protected String originatorID;
    @XmlElement(name = "OtherInterestCheckLayoutID", nillable = true)
    protected Integer otherInterestCheckLayoutID;
    @XmlElement(name = "PrintChecks")
    protected Boolean printChecks;
    @XmlElement(name = "ReceiptMappingID", nillable = true)
    protected Integer receiptMappingID;
    @XmlElement(name = "SequenceNumber")
    protected Integer sequenceNumber;
    @XmlElement(name = "ServiceClassCode", nillable = true)
    protected String serviceClassCode;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "TransitNumber", nillable = true)
    protected String transitNumber;
    @XmlElement(name = "UseSystemGeneratedCheckNumbers")
    protected Boolean useSystemGeneratedCheckNumbers;
    @XmlElement(name = "VendorCheckLayoutID", nillable = true)
    protected Integer vendorCheckLayoutID;

    /**
     * Gets the value of the abaRoutingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getABARoutingNumber() {
        return abaRoutingNumber;
    }

    /**
     * Sets the value of the abaRoutingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setABARoutingNumber(String value) {
        this.abaRoutingNumber = value;
    }

    /**
     * Gets the value of the bankAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    /**
     * Sets the value of the bankAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNumber(String value) {
        this.bankAccountNumber = value;
    }

    /**
     * Gets the value of the bankNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankNumber() {
        return bankNumber;
    }

    /**
     * Sets the value of the bankNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankNumber(String value) {
        this.bankNumber = value;
    }

    /**
     * Gets the value of the bankNumberPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankNumberPrefix() {
        return bankNumberPrefix;
    }

    /**
     * Sets the value of the bankNumberPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankNumberPrefix(String value) {
        this.bankNumberPrefix = value;
    }

    /**
     * Gets the value of the bankVendorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankVendorCode() {
        return bankVendorCode;
    }

    /**
     * Sets the value of the bankVendorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankVendorCode(String value) {
        this.bankVendorCode = value;
    }

    /**
     * Gets the value of the brokerCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBrokerCheckLayoutID() {
        return brokerCheckLayoutID;
    }

    /**
     * Sets the value of the brokerCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBrokerCheckLayoutID(Integer value) {
        this.brokerCheckLayoutID = value;
    }

    /**
     * Gets the value of the chartOfAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChartOfAccountID() {
        return chartOfAccountID;
    }

    /**
     * Sets the value of the chartOfAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChartOfAccountID(Integer value) {
        this.chartOfAccountID = value;
    }

    /**
     * Gets the value of the clientCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientCheckLayoutID() {
        return clientCheckLayoutID;
    }

    /**
     * Sets the value of the clientCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientCheckLayoutID(Integer value) {
        this.clientCheckLayoutID = value;
    }

    /**
     * Gets the value of the companyCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyCheckLayoutID() {
        return companyCheckLayoutID;
    }

    /**
     * Sets the value of the companyCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyCheckLayoutID(Integer value) {
        this.companyCheckLayoutID = value;
    }

    /**
     * Gets the value of the dataCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCenterCode() {
        return dataCenterCode;
    }

    /**
     * Sets the value of the dataCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCenterCode(String value) {
        this.dataCenterCode = value;
    }

    /**
     * Gets the value of the defaultAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgencyCode() {
        return defaultAgencyCode;
    }

    /**
     * Sets the value of the defaultAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgencyCode(String value) {
        this.defaultAgencyCode = value;
    }

    /**
     * Gets the value of the defaultDisbursementTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDisbursementTypeCode() {
        return defaultDisbursementTypeCode;
    }

    /**
     * Sets the value of the defaultDisbursementTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDisbursementTypeCode(String value) {
        this.defaultDisbursementTypeCode = value;
    }

    /**
     * Gets the value of the defaultReceiptTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultReceiptTypeCode() {
        return defaultReceiptTypeCode;
    }

    /**
     * Sets the value of the defaultReceiptTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultReceiptTypeCode(String value) {
        this.defaultReceiptTypeCode = value;
    }

    /**
     * Gets the value of the disbursementMappingID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisbursementMappingID() {
        return disbursementMappingID;
    }

    /**
     * Sets the value of the disbursementMappingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisbursementMappingID(Integer value) {
        this.disbursementMappingID = value;
    }

    /**
     * Gets the value of the eft property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEFT() {
        return eft;
    }

    /**
     * Sets the value of the eft property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEFT(Boolean value) {
        this.eft = value;
    }

    /**
     * Gets the value of the employeeCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmployeeCheckLayoutID() {
        return employeeCheckLayoutID;
    }

    /**
     * Sets the value of the employeeCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmployeeCheckLayoutID(Integer value) {
        this.employeeCheckLayoutID = value;
    }

    /**
     * Gets the value of the financeCompanyCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFinanceCompanyCheckLayoutID() {
        return financeCompanyCheckLayoutID;
    }

    /**
     * Sets the value of the financeCompanyCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFinanceCompanyCheckLayoutID(Integer value) {
        this.financeCompanyCheckLayoutID = value;
    }

    /**
     * Gets the value of the fractionalRoutingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFractionalRoutingNumber() {
        return fractionalRoutingNumber;
    }

    /**
     * Sets the value of the fractionalRoutingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFractionalRoutingNumber(String value) {
        this.fractionalRoutingNumber = value;
    }

    /**
     * Gets the value of the nextAvailableCheckNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNextAvailableCheckNumber() {
        return nextAvailableCheckNumber;
    }

    /**
     * Sets the value of the nextAvailableCheckNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNextAvailableCheckNumber(Integer value) {
        this.nextAvailableCheckNumber = value;
    }

    /**
     * Gets the value of the originatorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorID() {
        return originatorID;
    }

    /**
     * Sets the value of the originatorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorID(String value) {
        this.originatorID = value;
    }

    /**
     * Gets the value of the otherInterestCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOtherInterestCheckLayoutID() {
        return otherInterestCheckLayoutID;
    }

    /**
     * Sets the value of the otherInterestCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOtherInterestCheckLayoutID(Integer value) {
        this.otherInterestCheckLayoutID = value;
    }

    /**
     * Gets the value of the printChecks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrintChecks() {
        return printChecks;
    }

    /**
     * Sets the value of the printChecks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrintChecks(Boolean value) {
        this.printChecks = value;
    }

    /**
     * Gets the value of the receiptMappingID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptMappingID() {
        return receiptMappingID;
    }

    /**
     * Sets the value of the receiptMappingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptMappingID(Integer value) {
        this.receiptMappingID = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequenceNumber(Integer value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the serviceClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceClassCode() {
        return serviceClassCode;
    }

    /**
     * Sets the value of the serviceClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceClassCode(String value) {
        this.serviceClassCode = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the transitNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitNumber() {
        return transitNumber;
    }

    /**
     * Sets the value of the transitNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitNumber(String value) {
        this.transitNumber = value;
    }

    /**
     * Gets the value of the useSystemGeneratedCheckNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemGeneratedCheckNumbers() {
        return useSystemGeneratedCheckNumbers;
    }

    /**
     * Sets the value of the useSystemGeneratedCheckNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemGeneratedCheckNumbers(Boolean value) {
        this.useSystemGeneratedCheckNumbers = value;
    }

    /**
     * Gets the value of the vendorCheckLayoutID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVendorCheckLayoutID() {
        return vendorCheckLayoutID;
    }

    /**
     * Sets the value of the vendorCheckLayoutID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVendorCheckLayoutID(Integer value) {
        this.vendorCheckLayoutID = value;
    }

}
