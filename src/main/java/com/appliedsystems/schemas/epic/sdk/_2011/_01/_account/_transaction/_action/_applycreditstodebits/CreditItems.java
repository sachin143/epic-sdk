
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditItem" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_applycreditstodebits/}CreditItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditItems", propOrder = {
    "creditItems"
})
public class CreditItems {

    @XmlElement(name = "CreditItem", nillable = true)
    protected List<CreditItem> creditItems;

    /**
     * Gets the value of the creditItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditItem }
     * 
     * 
     */
    public List<CreditItem> getCreditItems() {
        if (creditItems == null) {
            creditItems = new ArrayList<CreditItem>();
        }
        return this.creditItems;
    }

}
