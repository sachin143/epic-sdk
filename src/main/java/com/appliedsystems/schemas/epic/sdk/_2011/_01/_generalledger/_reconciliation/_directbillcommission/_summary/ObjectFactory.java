
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SelectedItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", "SelectedItems");
    private final static QName _SelectedItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", "SelectedItem");
    private final static QName _CriteriaItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", "CriteriaItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CriteriaItem }
     * 
     */
    public CriteriaItem createCriteriaItem() {
        return new CriteriaItem();
    }

    /**
     * Create an instance of {@link SelectedItem }
     * 
     */
    public SelectedItem createSelectedItem() {
        return new SelectedItem();
    }

    /**
     * Create an instance of {@link SelectedItems }
     * 
     */
    public SelectedItems createSelectedItems() {
        return new SelectedItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", name = "SelectedItems")
    public JAXBElement<SelectedItems> createSelectedItems(SelectedItems value) {
        return new JAXBElement<SelectedItems>(_SelectedItems_QNAME, SelectedItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SelectedItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", name = "SelectedItem")
    public JAXBElement<SelectedItem> createSelectedItem(SelectedItem value) {
        return new JAXBElement<SelectedItem>(_SelectedItem_QNAME, SelectedItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CriteriaItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/_summary/", name = "CriteriaItem")
    public JAXBElement<CriteriaItem> createCriteriaItem(CriteriaItem value) {
        return new JAXBElement<CriteriaItem>(_CriteriaItem_QNAME, CriteriaItem.class, null, value);
    }

}
