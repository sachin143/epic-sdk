
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReviseInstallmentsBasedOn.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReviseInstallmentsBasedOn">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Summary"/>
 *     &lt;enumeration value="SplitReceivable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReviseInstallmentsBasedOn")
@XmlEnum
public enum ReviseInstallmentsBasedOn {

    @XmlEnumValue("Summary")
    SUMMARY("Summary"),
    @XmlEnumValue("SplitReceivable")
    SPLIT_RECEIVABLE("SplitReceivable");
    private final String value;

    ReviseInstallmentsBasedOn(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReviseInstallmentsBasedOn fromValue(String v) {
        for (ReviseInstallmentsBasedOn c: ReviseInstallmentsBasedOn.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
