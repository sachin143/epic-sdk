
package com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentInstallmentItems complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentInstallmentItems">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentInstallmentItem" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/}PaymentInstallmentItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentInstallmentItems", propOrder = {
    "paymentInstallmentItems"
})
public class PaymentInstallmentItems {

    @XmlElement(name = "PaymentInstallmentItem", nillable = true)
    protected List<PaymentInstallmentItem> paymentInstallmentItems;

    /**
     * Gets the value of the paymentInstallmentItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentInstallmentItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentInstallmentItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentInstallmentItem }
     * 
     * 
     */
    public List<PaymentInstallmentItem> getPaymentInstallmentItems() {
        if (paymentInstallmentItems == null) {
            paymentInstallmentItems = new ArrayList<PaymentInstallmentItem>();
        }
        return this.paymentInstallmentItems;
    }

}
