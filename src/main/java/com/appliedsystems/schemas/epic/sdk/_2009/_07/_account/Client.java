
package com.appliedsystems.schemas.epic.sdk._2009._07._account;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._client.Account;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._client.Billing;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._client.CategoriesHistory;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ConfidentialClientAccess;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ServicingRolesItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2021._01._account._client.EmployeeBenefits;


/**
 * <p>Java class for Client complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Client">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}Account" minOccurs="0"/>
 *         &lt;element name="AdditionalContactFirst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalContactLast" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalContactMiddle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalContactPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalContactSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillingValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}Billing" minOccurs="0"/>
 *         &lt;element name="BusinessName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BusinessSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoriesHistoryValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}CategoriesHistory" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfidentialClientAccessValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_client/}ConfidentialClientAccess" minOccurs="0"/>
 *         &lt;element name="DUNS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FormatOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="InActivateOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="InActivateReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBenefits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsBonds" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsCommercial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsFinancialServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsLifeAndHealth" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsOther" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsPersonal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsProspectFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PrimaryContactCallPermission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactFirst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactLast" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactMiddle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactNumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryContactSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicingContacts" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_common/}ServicingRolesItems" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IsAgriculture" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PrimaryContactCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountSourceID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DoNotPurge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PrimaryContactEmailDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonGUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployeeBenefitsValue" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_account/_client/}EmployeeBenefits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Client", propOrder = {
    "accountName",
    "accountValue",
    "additionalContactFirst",
    "additionalContactLast",
    "additionalContactMiddle",
    "additionalContactPrefix",
    "additionalContactSuffix",
    "billingValue",
    "businessName",
    "businessPrefix",
    "businessSuffix",
    "categoriesHistoryValue",
    "clientID",
    "clientLookupCode",
    "confidentialClientAccessValue",
    "duns",
    "formatOption",
    "inActivateOption",
    "inActivateReason",
    "isBenefits",
    "isBonds",
    "isCommercial",
    "isFinancialServices",
    "isLifeAndHealth",
    "isOther",
    "isPersonal",
    "isProspectFlag",
    "primaryContactCallPermission",
    "primaryContactEmail",
    "primaryContactExtension",
    "primaryContactFirst",
    "primaryContactLast",
    "primaryContactMiddle",
    "primaryContactNumber",
    "primaryContactNumberType",
    "primaryContactPrefix",
    "primaryContactSuffix",
    "servicingContacts",
    "timestamp",
    "isAgriculture",
    "primaryContactCountryCode",
    "accountSourceID",
    "clientGUID",
    "doNotPurge",
    "primaryContactEmailDescription",
    "personGUID",
    "employeeBenefitsValue"
})
public class Client {

    @XmlElement(name = "AccountName", nillable = true)
    protected String accountName;
    @XmlElement(name = "AccountValue", nillable = true)
    protected Account accountValue;
    @XmlElement(name = "AdditionalContactFirst", nillable = true)
    protected String additionalContactFirst;
    @XmlElement(name = "AdditionalContactLast", nillable = true)
    protected String additionalContactLast;
    @XmlElement(name = "AdditionalContactMiddle", nillable = true)
    protected String additionalContactMiddle;
    @XmlElement(name = "AdditionalContactPrefix", nillable = true)
    protected String additionalContactPrefix;
    @XmlElement(name = "AdditionalContactSuffix", nillable = true)
    protected String additionalContactSuffix;
    @XmlElement(name = "BillingValue", nillable = true)
    protected Billing billingValue;
    @XmlElement(name = "BusinessName", nillable = true)
    protected String businessName;
    @XmlElement(name = "BusinessPrefix", nillable = true)
    protected String businessPrefix;
    @XmlElement(name = "BusinessSuffix", nillable = true)
    protected String businessSuffix;
    @XmlElement(name = "CategoriesHistoryValue", nillable = true)
    protected CategoriesHistory categoriesHistoryValue;
    @XmlElement(name = "ClientID")
    protected Integer clientID;
    @XmlElement(name = "ClientLookupCode", nillable = true)
    protected String clientLookupCode;
    @XmlElement(name = "ConfidentialClientAccessValue", nillable = true)
    protected ConfidentialClientAccess confidentialClientAccessValue;
    @XmlElement(name = "DUNS", nillable = true)
    protected String duns;
    @XmlElement(name = "FormatOption", nillable = true)
    protected OptionType formatOption;
    @XmlElement(name = "InActivateOption", nillable = true)
    protected OptionType inActivateOption;
    @XmlElement(name = "InActivateReason", nillable = true)
    protected String inActivateReason;
    @XmlElement(name = "IsBenefits")
    protected Boolean isBenefits;
    @XmlElement(name = "IsBonds")
    protected Boolean isBonds;
    @XmlElement(name = "IsCommercial")
    protected Boolean isCommercial;
    @XmlElement(name = "IsFinancialServices")
    protected Boolean isFinancialServices;
    @XmlElement(name = "IsLifeAndHealth")
    protected Boolean isLifeAndHealth;
    @XmlElement(name = "IsOther")
    protected Boolean isOther;
    @XmlElement(name = "IsPersonal")
    protected Boolean isPersonal;
    @XmlElement(name = "IsProspectFlag")
    protected Boolean isProspectFlag;
    @XmlElement(name = "PrimaryContactCallPermission", nillable = true)
    protected String primaryContactCallPermission;
    @XmlElement(name = "PrimaryContactEmail", nillable = true)
    protected String primaryContactEmail;
    @XmlElement(name = "PrimaryContactExtension", nillable = true)
    protected String primaryContactExtension;
    @XmlElement(name = "PrimaryContactFirst", nillable = true)
    protected String primaryContactFirst;
    @XmlElement(name = "PrimaryContactLast", nillable = true)
    protected String primaryContactLast;
    @XmlElement(name = "PrimaryContactMiddle", nillable = true)
    protected String primaryContactMiddle;
    @XmlElement(name = "PrimaryContactNumber", nillable = true)
    protected String primaryContactNumber;
    @XmlElement(name = "PrimaryContactNumberType", nillable = true)
    protected String primaryContactNumberType;
    @XmlElement(name = "PrimaryContactPrefix", nillable = true)
    protected String primaryContactPrefix;
    @XmlElement(name = "PrimaryContactSuffix", nillable = true)
    protected String primaryContactSuffix;
    @XmlElement(name = "ServicingContacts", nillable = true)
    protected ServicingRolesItems servicingContacts;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "IsAgriculture")
    protected Boolean isAgriculture;
    @XmlElement(name = "PrimaryContactCountryCode", nillable = true)
    protected String primaryContactCountryCode;
    @XmlElement(name = "AccountSourceID", nillable = true)
    protected Integer accountSourceID;
    @XmlElement(name = "ClientGUID", nillable = true)
    protected String clientGUID;
    @XmlElement(name = "DoNotPurge")
    protected Boolean doNotPurge;
    @XmlElement(name = "PrimaryContactEmailDescription", nillable = true)
    protected String primaryContactEmailDescription;
    @XmlElement(name = "PersonGUID", nillable = true)
    protected String personGUID;
    @XmlElement(name = "EmployeeBenefitsValue", nillable = true)
    protected EmployeeBenefits employeeBenefitsValue;

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the accountValue property.
     * 
     * @return
     *     possible object is
     *     {@link Account }
     *     
     */
    public Account getAccountValue() {
        return accountValue;
    }

    /**
     * Sets the value of the accountValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Account }
     *     
     */
    public void setAccountValue(Account value) {
        this.accountValue = value;
    }

    /**
     * Gets the value of the additionalContactFirst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalContactFirst() {
        return additionalContactFirst;
    }

    /**
     * Sets the value of the additionalContactFirst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalContactFirst(String value) {
        this.additionalContactFirst = value;
    }

    /**
     * Gets the value of the additionalContactLast property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalContactLast() {
        return additionalContactLast;
    }

    /**
     * Sets the value of the additionalContactLast property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalContactLast(String value) {
        this.additionalContactLast = value;
    }

    /**
     * Gets the value of the additionalContactMiddle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalContactMiddle() {
        return additionalContactMiddle;
    }

    /**
     * Sets the value of the additionalContactMiddle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalContactMiddle(String value) {
        this.additionalContactMiddle = value;
    }

    /**
     * Gets the value of the additionalContactPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalContactPrefix() {
        return additionalContactPrefix;
    }

    /**
     * Sets the value of the additionalContactPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalContactPrefix(String value) {
        this.additionalContactPrefix = value;
    }

    /**
     * Gets the value of the additionalContactSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalContactSuffix() {
        return additionalContactSuffix;
    }

    /**
     * Sets the value of the additionalContactSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalContactSuffix(String value) {
        this.additionalContactSuffix = value;
    }

    /**
     * Gets the value of the billingValue property.
     * 
     * @return
     *     possible object is
     *     {@link Billing }
     *     
     */
    public Billing getBillingValue() {
        return billingValue;
    }

    /**
     * Sets the value of the billingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Billing }
     *     
     */
    public void setBillingValue(Billing value) {
        this.billingValue = value;
    }

    /**
     * Gets the value of the businessName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * Sets the value of the businessName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessName(String value) {
        this.businessName = value;
    }

    /**
     * Gets the value of the businessPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPrefix() {
        return businessPrefix;
    }

    /**
     * Sets the value of the businessPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPrefix(String value) {
        this.businessPrefix = value;
    }

    /**
     * Gets the value of the businessSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessSuffix() {
        return businessSuffix;
    }

    /**
     * Sets the value of the businessSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessSuffix(String value) {
        this.businessSuffix = value;
    }

    /**
     * Gets the value of the categoriesHistoryValue property.
     * 
     * @return
     *     possible object is
     *     {@link CategoriesHistory }
     *     
     */
    public CategoriesHistory getCategoriesHistoryValue() {
        return categoriesHistoryValue;
    }

    /**
     * Sets the value of the categoriesHistoryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoriesHistory }
     *     
     */
    public void setCategoriesHistoryValue(CategoriesHistory value) {
        this.categoriesHistoryValue = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the clientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientLookupCode() {
        return clientLookupCode;
    }

    /**
     * Sets the value of the clientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientLookupCode(String value) {
        this.clientLookupCode = value;
    }

    /**
     * Gets the value of the confidentialClientAccessValue property.
     * 
     * @return
     *     possible object is
     *     {@link ConfidentialClientAccess }
     *     
     */
    public ConfidentialClientAccess getConfidentialClientAccessValue() {
        return confidentialClientAccessValue;
    }

    /**
     * Sets the value of the confidentialClientAccessValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfidentialClientAccess }
     *     
     */
    public void setConfidentialClientAccessValue(ConfidentialClientAccess value) {
        this.confidentialClientAccessValue = value;
    }

    /**
     * Gets the value of the duns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDUNS() {
        return duns;
    }

    /**
     * Sets the value of the duns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDUNS(String value) {
        this.duns = value;
    }

    /**
     * Gets the value of the formatOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getFormatOption() {
        return formatOption;
    }

    /**
     * Sets the value of the formatOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setFormatOption(OptionType value) {
        this.formatOption = value;
    }

    /**
     * Gets the value of the inActivateOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getInActivateOption() {
        return inActivateOption;
    }

    /**
     * Sets the value of the inActivateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setInActivateOption(OptionType value) {
        this.inActivateOption = value;
    }

    /**
     * Gets the value of the inActivateReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInActivateReason() {
        return inActivateReason;
    }

    /**
     * Sets the value of the inActivateReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInActivateReason(String value) {
        this.inActivateReason = value;
    }

    /**
     * Gets the value of the isBenefits property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBenefits() {
        return isBenefits;
    }

    /**
     * Sets the value of the isBenefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBenefits(Boolean value) {
        this.isBenefits = value;
    }

    /**
     * Gets the value of the isBonds property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBonds() {
        return isBonds;
    }

    /**
     * Sets the value of the isBonds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBonds(Boolean value) {
        this.isBonds = value;
    }

    /**
     * Gets the value of the isCommercial property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCommercial() {
        return isCommercial;
    }

    /**
     * Sets the value of the isCommercial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCommercial(Boolean value) {
        this.isCommercial = value;
    }

    /**
     * Gets the value of the isFinancialServices property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFinancialServices() {
        return isFinancialServices;
    }

    /**
     * Sets the value of the isFinancialServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFinancialServices(Boolean value) {
        this.isFinancialServices = value;
    }

    /**
     * Gets the value of the isLifeAndHealth property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLifeAndHealth() {
        return isLifeAndHealth;
    }

    /**
     * Sets the value of the isLifeAndHealth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLifeAndHealth(Boolean value) {
        this.isLifeAndHealth = value;
    }

    /**
     * Gets the value of the isOther property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsOther() {
        return isOther;
    }

    /**
     * Sets the value of the isOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsOther(Boolean value) {
        this.isOther = value;
    }

    /**
     * Gets the value of the isPersonal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPersonal() {
        return isPersonal;
    }

    /**
     * Sets the value of the isPersonal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPersonal(Boolean value) {
        this.isPersonal = value;
    }

    /**
     * Gets the value of the isProspectFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsProspectFlag() {
        return isProspectFlag;
    }

    /**
     * Sets the value of the isProspectFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsProspectFlag(Boolean value) {
        this.isProspectFlag = value;
    }

    /**
     * Gets the value of the primaryContactCallPermission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactCallPermission() {
        return primaryContactCallPermission;
    }

    /**
     * Sets the value of the primaryContactCallPermission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactCallPermission(String value) {
        this.primaryContactCallPermission = value;
    }

    /**
     * Gets the value of the primaryContactEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactEmail() {
        return primaryContactEmail;
    }

    /**
     * Sets the value of the primaryContactEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactEmail(String value) {
        this.primaryContactEmail = value;
    }

    /**
     * Gets the value of the primaryContactExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactExtension() {
        return primaryContactExtension;
    }

    /**
     * Sets the value of the primaryContactExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactExtension(String value) {
        this.primaryContactExtension = value;
    }

    /**
     * Gets the value of the primaryContactFirst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactFirst() {
        return primaryContactFirst;
    }

    /**
     * Sets the value of the primaryContactFirst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactFirst(String value) {
        this.primaryContactFirst = value;
    }

    /**
     * Gets the value of the primaryContactLast property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactLast() {
        return primaryContactLast;
    }

    /**
     * Sets the value of the primaryContactLast property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactLast(String value) {
        this.primaryContactLast = value;
    }

    /**
     * Gets the value of the primaryContactMiddle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactMiddle() {
        return primaryContactMiddle;
    }

    /**
     * Sets the value of the primaryContactMiddle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactMiddle(String value) {
        this.primaryContactMiddle = value;
    }

    /**
     * Gets the value of the primaryContactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactNumber() {
        return primaryContactNumber;
    }

    /**
     * Sets the value of the primaryContactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactNumber(String value) {
        this.primaryContactNumber = value;
    }

    /**
     * Gets the value of the primaryContactNumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactNumberType() {
        return primaryContactNumberType;
    }

    /**
     * Sets the value of the primaryContactNumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactNumberType(String value) {
        this.primaryContactNumberType = value;
    }

    /**
     * Gets the value of the primaryContactPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactPrefix() {
        return primaryContactPrefix;
    }

    /**
     * Sets the value of the primaryContactPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactPrefix(String value) {
        this.primaryContactPrefix = value;
    }

    /**
     * Gets the value of the primaryContactSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactSuffix() {
        return primaryContactSuffix;
    }

    /**
     * Sets the value of the primaryContactSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactSuffix(String value) {
        this.primaryContactSuffix = value;
    }

    /**
     * Gets the value of the servicingContacts property.
     * 
     * @return
     *     possible object is
     *     {@link ServicingRolesItems }
     *     
     */
    public ServicingRolesItems getServicingContacts() {
        return servicingContacts;
    }

    /**
     * Sets the value of the servicingContacts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicingRolesItems }
     *     
     */
    public void setServicingContacts(ServicingRolesItems value) {
        this.servicingContacts = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the isAgriculture property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAgriculture() {
        return isAgriculture;
    }

    /**
     * Sets the value of the isAgriculture property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAgriculture(Boolean value) {
        this.isAgriculture = value;
    }

    /**
     * Gets the value of the primaryContactCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactCountryCode() {
        return primaryContactCountryCode;
    }

    /**
     * Sets the value of the primaryContactCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactCountryCode(String value) {
        this.primaryContactCountryCode = value;
    }

    /**
     * Gets the value of the accountSourceID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountSourceID() {
        return accountSourceID;
    }

    /**
     * Sets the value of the accountSourceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountSourceID(Integer value) {
        this.accountSourceID = value;
    }

    /**
     * Gets the value of the clientGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientGUID() {
        return clientGUID;
    }

    /**
     * Sets the value of the clientGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientGUID(String value) {
        this.clientGUID = value;
    }

    /**
     * Gets the value of the doNotPurge property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoNotPurge() {
        return doNotPurge;
    }

    /**
     * Sets the value of the doNotPurge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoNotPurge(Boolean value) {
        this.doNotPurge = value;
    }

    /**
     * Gets the value of the primaryContactEmailDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryContactEmailDescription() {
        return primaryContactEmailDescription;
    }

    /**
     * Sets the value of the primaryContactEmailDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryContactEmailDescription(String value) {
        this.primaryContactEmailDescription = value;
    }

    /**
     * Gets the value of the personGUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonGUID() {
        return personGUID;
    }

    /**
     * Sets the value of the personGUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonGUID(String value) {
        this.personGUID = value;
    }

    /**
     * Gets the value of the employeeBenefitsValue property.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeBenefits }
     *     
     */
    public EmployeeBenefits getEmployeeBenefitsValue() {
        return employeeBenefitsValue;
    }

    /**
     * Sets the value of the employeeBenefitsValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeBenefits }
     *     
     */
    public void setEmployeeBenefitsValue(EmployeeBenefits value) {
        this.employeeBenefitsValue = value;
    }

}
