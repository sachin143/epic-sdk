
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllocationStructureGroupingsGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllocationStructureGroupingsGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AllocationStructureGroupingID"/>
 *     &lt;enumeration value="All"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AllocationStructureGroupingsGetType")
@XmlEnum
public enum AllocationStructureGroupingsGetType {

    @XmlEnumValue("AllocationStructureGroupingID")
    ALLOCATION_STRUCTURE_GROUPING_ID("AllocationStructureGroupingID"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    AllocationStructureGroupingsGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AllocationStructureGroupingsGetType fromValue(String v) {
        for (AllocationStructureGroupingsGetType c: AllocationStructureGroupingsGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
