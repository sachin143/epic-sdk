
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.PolicyItems;
import com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.TransactionItems;


/**
 * <p>Java class for TaxFeeRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxFeeRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllSelectedPoliciesOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="AllSelectedTransactionsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Contact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultTax" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Effective" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Entered" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="IncludeExcludePoliciesOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="IncludeExcludeTransactionsOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="IssuingLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastUpdated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PolicyItems" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/}PolicyItems" minOccurs="0"/>
 *         &lt;element name="Rounding" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TaxFeeRateCodeID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_taxfeerate/}TransactionItems" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_shared/}Address" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFeeRate", propOrder = {
    "accountCode",
    "allSelectedPoliciesOption",
    "allSelectedTransactionsOption",
    "amount",
    "contact",
    "defaultTax",
    "description",
    "effective",
    "entered",
    "expiration",
    "includeExcludePoliciesOption",
    "includeExcludeTransactionsOption",
    "issuingLocationCode",
    "lastUpdated",
    "percent",
    "policyItems",
    "rounding",
    "taxFeeRateCodeID",
    "timestamp",
    "transactionCode",
    "transactionItems",
    "typeCode",
    "address"
})
public class TaxFeeRate {

    @XmlElement(name = "AccountCode", nillable = true)
    protected String accountCode;
    @XmlElement(name = "AllSelectedPoliciesOption", nillable = true)
    protected OptionType allSelectedPoliciesOption;
    @XmlElement(name = "AllSelectedTransactionsOption", nillable = true)
    protected OptionType allSelectedTransactionsOption;
    @XmlElement(name = "Amount", nillable = true)
    protected BigDecimal amount;
    @XmlElement(name = "Contact", nillable = true)
    protected String contact;
    @XmlElement(name = "DefaultTax")
    protected Boolean defaultTax;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "Effective", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effective;
    @XmlElement(name = "Entered", nillable = true)
    protected String entered;
    @XmlElement(name = "Expiration", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expiration;
    @XmlElement(name = "IncludeExcludePoliciesOption", nillable = true)
    protected OptionType includeExcludePoliciesOption;
    @XmlElement(name = "IncludeExcludeTransactionsOption", nillable = true)
    protected OptionType includeExcludeTransactionsOption;
    @XmlElement(name = "IssuingLocationCode", nillable = true)
    protected String issuingLocationCode;
    @XmlElement(name = "LastUpdated", nillable = true)
    protected String lastUpdated;
    @XmlElement(name = "Percent", nillable = true)
    protected BigDecimal percent;
    @XmlElement(name = "PolicyItems", nillable = true)
    protected PolicyItems policyItems;
    @XmlElement(name = "Rounding", nillable = true)
    protected String rounding;
    @XmlElement(name = "TaxFeeRateCodeID")
    protected Integer taxFeeRateCodeID;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionItems", nillable = true)
    protected TransactionItems transactionItems;
    @XmlElement(name = "TypeCode", nillable = true)
    protected String typeCode;
    @XmlElement(name = "Address", nillable = true)
    protected Address address;

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Gets the value of the allSelectedPoliciesOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAllSelectedPoliciesOption() {
        return allSelectedPoliciesOption;
    }

    /**
     * Sets the value of the allSelectedPoliciesOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAllSelectedPoliciesOption(OptionType value) {
        this.allSelectedPoliciesOption = value;
    }

    /**
     * Gets the value of the allSelectedTransactionsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAllSelectedTransactionsOption() {
        return allSelectedTransactionsOption;
    }

    /**
     * Sets the value of the allSelectedTransactionsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAllSelectedTransactionsOption(OptionType value) {
        this.allSelectedTransactionsOption = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Gets the value of the defaultTax property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultTax() {
        return defaultTax;
    }

    /**
     * Sets the value of the defaultTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultTax(Boolean value) {
        this.defaultTax = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effective property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffective() {
        return effective;
    }

    /**
     * Sets the value of the effective property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffective(XMLGregorianCalendar value) {
        this.effective = value;
    }

    /**
     * Gets the value of the entered property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntered() {
        return entered;
    }

    /**
     * Sets the value of the entered property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntered(String value) {
        this.entered = value;
    }

    /**
     * Gets the value of the expiration property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiration() {
        return expiration;
    }

    /**
     * Sets the value of the expiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiration(XMLGregorianCalendar value) {
        this.expiration = value;
    }

    /**
     * Gets the value of the includeExcludePoliciesOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getIncludeExcludePoliciesOption() {
        return includeExcludePoliciesOption;
    }

    /**
     * Sets the value of the includeExcludePoliciesOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setIncludeExcludePoliciesOption(OptionType value) {
        this.includeExcludePoliciesOption = value;
    }

    /**
     * Gets the value of the includeExcludeTransactionsOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getIncludeExcludeTransactionsOption() {
        return includeExcludeTransactionsOption;
    }

    /**
     * Sets the value of the includeExcludeTransactionsOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setIncludeExcludeTransactionsOption(OptionType value) {
        this.includeExcludeTransactionsOption = value;
    }

    /**
     * Gets the value of the issuingLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingLocationCode() {
        return issuingLocationCode;
    }

    /**
     * Sets the value of the issuingLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingLocationCode(String value) {
        this.issuingLocationCode = value;
    }

    /**
     * Gets the value of the lastUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastUpdated() {
        return lastUpdated;
    }

    /**
     * Sets the value of the lastUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastUpdated(String value) {
        this.lastUpdated = value;
    }

    /**
     * Gets the value of the percent property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Sets the value of the percent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

    /**
     * Gets the value of the policyItems property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyItems }
     *     
     */
    public PolicyItems getPolicyItems() {
        return policyItems;
    }

    /**
     * Sets the value of the policyItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyItems }
     *     
     */
    public void setPolicyItems(PolicyItems value) {
        this.policyItems = value;
    }

    /**
     * Gets the value of the rounding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRounding() {
        return rounding;
    }

    /**
     * Sets the value of the rounding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRounding(String value) {
        this.rounding = value;
    }

    /**
     * Gets the value of the taxFeeRateCodeID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxFeeRateCodeID() {
        return taxFeeRateCodeID;
    }

    /**
     * Sets the value of the taxFeeRateCodeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxFeeRateCodeID(Integer value) {
        this.taxFeeRateCodeID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionItems property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionItems }
     *     
     */
    public TransactionItems getTransactionItems() {
        return transactionItems;
    }

    /**
     * Sets the value of the transactionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionItems }
     *     
     */
    public void setTransactionItems(TransactionItems value) {
        this.transactionItems = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddress(Address value) {
        this.address = value;
    }

}
