
package com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OutstandingPayment_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments", "OutstandingPayment");
    private final static QName _OutstandingPayments_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments", "OutstandingPayments");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OutstandingPayments }
     * 
     */
    public OutstandingPayments createOutstandingPayments() {
        return new OutstandingPayments();
    }

    /**
     * Create an instance of {@link OutstandingPayment }
     * 
     */
    public OutstandingPayment createOutstandingPayment() {
        return new OutstandingPayment();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutstandingPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments", name = "OutstandingPayment")
    public JAXBElement<OutstandingPayment> createOutstandingPayment(OutstandingPayment value) {
        return new JAXBElement<OutstandingPayment>(_OutstandingPayment_QNAME, OutstandingPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutstandingPayments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2021/01/_generalledger/_receipt/_processoutstandingpayments", name = "OutstandingPayments")
    public JAXBElement<OutstandingPayments> createOutstandingPayments(OutstandingPayments value) {
        return new JAXBElement<OutstandingPayments>(_OutstandingPayments_QNAME, OutstandingPayments.class, null, value);
    }

}
