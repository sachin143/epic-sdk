
package com.appliedsystems.schemas.epic.sdk._2016._01._get;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ComparisonType;
import com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.FutureItems;
import com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.Service;
import com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.TransactionStatus;


/**
 * <p>Java class for TransactionFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ARDueDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ARDueDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BalanceComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="BillModeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BilledFromAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledFromName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledFromNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="BilledReservedCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledReservedName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledReservedNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="BilledToAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledToName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledToNameComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="BinderStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FutureItems" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/}FutureItems" minOccurs="0"/>
 *         &lt;element name="InstallmentGroup" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InvoiceCreatedBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceCreatedEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceLastGeneratedDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceLastGeneratedDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InvoiceStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LateItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PaymentDateBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentDateEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaymentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyNumberComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SiteIDComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_transactionfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="TaxableItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TransactionStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/}TransactionStatus" minOccurs="0"/>
 *         &lt;element name="Service" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/_transactionfilter/}Service" minOccurs="0"/>
 *         &lt;element name="ServiceID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionFilter", propOrder = {
    "arDueDateBegins",
    "arDueDateEnds",
    "accountID",
    "accountTypeCode",
    "balance",
    "balanceComparisonType",
    "billModeCode",
    "billNumber",
    "billedFromAccountCode",
    "billedFromName",
    "billedFromNameComparisonType",
    "billedReservedCode",
    "billedReservedName",
    "billedReservedNameComparisonType",
    "billedToAccountCode",
    "billedToName",
    "billedToNameComparisonType",
    "binderStatus",
    "futureItems",
    "installmentGroup",
    "invoiceCreatedBegins",
    "invoiceCreatedEnds",
    "invoiceLastGeneratedDateBegins",
    "invoiceLastGeneratedDateEnds",
    "invoiceNumber",
    "invoiceStatus",
    "itemNumber",
    "lateItemNumber",
    "paymentDateBegins",
    "paymentDateEnds",
    "paymentID",
    "policyID",
    "policyNumber",
    "policyNumberComparisonType",
    "siteID",
    "siteIDComparisonType",
    "taxableItemNumber",
    "transactionCode",
    "transactionID",
    "transactionStatus",
    "service",
    "serviceID"
})
public class TransactionFilter {

    @XmlElement(name = "ARDueDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arDueDateBegins;
    @XmlElement(name = "ARDueDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar arDueDateEnds;
    @XmlElement(name = "AccountID", nillable = true)
    protected Integer accountID;
    @XmlElement(name = "AccountTypeCode", nillable = true)
    protected String accountTypeCode;
    @XmlElement(name = "Balance", nillable = true)
    protected BigDecimal balance;
    @XmlElement(name = "BalanceComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType balanceComparisonType;
    @XmlElement(name = "BillModeCode", nillable = true)
    protected String billModeCode;
    @XmlElement(name = "BillNumber", nillable = true)
    protected Integer billNumber;
    @XmlElement(name = "BilledFromAccountCode", nillable = true)
    protected String billedFromAccountCode;
    @XmlElement(name = "BilledFromName", nillable = true)
    protected String billedFromName;
    @XmlElement(name = "BilledFromNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType billedFromNameComparisonType;
    @XmlElement(name = "BilledReservedCode", nillable = true)
    protected String billedReservedCode;
    @XmlElement(name = "BilledReservedName", nillable = true)
    protected String billedReservedName;
    @XmlElement(name = "BilledReservedNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType billedReservedNameComparisonType;
    @XmlElement(name = "BilledToAccountCode", nillable = true)
    protected String billedToAccountCode;
    @XmlElement(name = "BilledToName", nillable = true)
    protected String billedToName;
    @XmlElement(name = "BilledToNameComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType billedToNameComparisonType;
    @XmlElement(name = "BinderStatus", nillable = true)
    protected String binderStatus;
    @XmlElement(name = "FutureItems")
    @XmlSchemaType(name = "string")
    protected FutureItems futureItems;
    @XmlElement(name = "InstallmentGroup", nillable = true)
    protected Integer installmentGroup;
    @XmlElement(name = "InvoiceCreatedBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceCreatedBegins;
    @XmlElement(name = "InvoiceCreatedEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceCreatedEnds;
    @XmlElement(name = "InvoiceLastGeneratedDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceLastGeneratedDateBegins;
    @XmlElement(name = "InvoiceLastGeneratedDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceLastGeneratedDateEnds;
    @XmlElement(name = "InvoiceNumber", nillable = true)
    protected Integer invoiceNumber;
    @XmlElement(name = "InvoiceStatus", nillable = true)
    protected String invoiceStatus;
    @XmlElement(name = "ItemNumber", nillable = true)
    protected Integer itemNumber;
    @XmlElement(name = "LateItemNumber", nillable = true)
    protected Integer lateItemNumber;
    @XmlElement(name = "PaymentDateBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentDateBegins;
    @XmlElement(name = "PaymentDateEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paymentDateEnds;
    @XmlElement(name = "PaymentID", nillable = true)
    protected String paymentID;
    @XmlElement(name = "PolicyID", nillable = true)
    protected Integer policyID;
    @XmlElement(name = "PolicyNumber", nillable = true)
    protected String policyNumber;
    @XmlElement(name = "PolicyNumberComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType policyNumberComparisonType;
    @XmlElement(name = "SiteID", nillable = true)
    protected String siteID;
    @XmlElement(name = "SiteIDComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType siteIDComparisonType;
    @XmlElement(name = "TaxableItemNumber", nillable = true)
    protected Integer taxableItemNumber;
    @XmlElement(name = "TransactionCode", nillable = true)
    protected String transactionCode;
    @XmlElement(name = "TransactionID", nillable = true)
    protected Integer transactionID;
    @XmlElement(name = "TransactionStatus")
    @XmlSchemaType(name = "string")
    protected TransactionStatus transactionStatus;
    @XmlElement(name = "Service")
    @XmlSchemaType(name = "string")
    protected Service service;
    @XmlElement(name = "ServiceID", nillable = true)
    protected Integer serviceID;

    /**
     * Gets the value of the arDueDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getARDueDateBegins() {
        return arDueDateBegins;
    }

    /**
     * Sets the value of the arDueDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setARDueDateBegins(XMLGregorianCalendar value) {
        this.arDueDateBegins = value;
    }

    /**
     * Gets the value of the arDueDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getARDueDateEnds() {
        return arDueDateEnds;
    }

    /**
     * Sets the value of the arDueDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setARDueDateEnds(XMLGregorianCalendar value) {
        this.arDueDateEnds = value;
    }

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountID(Integer value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    /**
     * Sets the value of the accountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTypeCode(String value) {
        this.accountTypeCode = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the balanceComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getBalanceComparisonType() {
        return balanceComparisonType;
    }

    /**
     * Sets the value of the balanceComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setBalanceComparisonType(ComparisonType value) {
        this.balanceComparisonType = value;
    }

    /**
     * Gets the value of the billModeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillModeCode() {
        return billModeCode;
    }

    /**
     * Sets the value of the billModeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillModeCode(String value) {
        this.billModeCode = value;
    }

    /**
     * Gets the value of the billNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillNumber() {
        return billNumber;
    }

    /**
     * Sets the value of the billNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillNumber(Integer value) {
        this.billNumber = value;
    }

    /**
     * Gets the value of the billedFromAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledFromAccountCode() {
        return billedFromAccountCode;
    }

    /**
     * Sets the value of the billedFromAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledFromAccountCode(String value) {
        this.billedFromAccountCode = value;
    }

    /**
     * Gets the value of the billedFromName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledFromName() {
        return billedFromName;
    }

    /**
     * Sets the value of the billedFromName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledFromName(String value) {
        this.billedFromName = value;
    }

    /**
     * Gets the value of the billedFromNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getBilledFromNameComparisonType() {
        return billedFromNameComparisonType;
    }

    /**
     * Sets the value of the billedFromNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setBilledFromNameComparisonType(ComparisonType value) {
        this.billedFromNameComparisonType = value;
    }

    /**
     * Gets the value of the billedReservedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledReservedCode() {
        return billedReservedCode;
    }

    /**
     * Sets the value of the billedReservedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledReservedCode(String value) {
        this.billedReservedCode = value;
    }

    /**
     * Gets the value of the billedReservedName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledReservedName() {
        return billedReservedName;
    }

    /**
     * Sets the value of the billedReservedName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledReservedName(String value) {
        this.billedReservedName = value;
    }

    /**
     * Gets the value of the billedReservedNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getBilledReservedNameComparisonType() {
        return billedReservedNameComparisonType;
    }

    /**
     * Sets the value of the billedReservedNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setBilledReservedNameComparisonType(ComparisonType value) {
        this.billedReservedNameComparisonType = value;
    }

    /**
     * Gets the value of the billedToAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledToAccountCode() {
        return billedToAccountCode;
    }

    /**
     * Sets the value of the billedToAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledToAccountCode(String value) {
        this.billedToAccountCode = value;
    }

    /**
     * Gets the value of the billedToName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledToName() {
        return billedToName;
    }

    /**
     * Sets the value of the billedToName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledToName(String value) {
        this.billedToName = value;
    }

    /**
     * Gets the value of the billedToNameComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getBilledToNameComparisonType() {
        return billedToNameComparisonType;
    }

    /**
     * Sets the value of the billedToNameComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setBilledToNameComparisonType(ComparisonType value) {
        this.billedToNameComparisonType = value;
    }

    /**
     * Gets the value of the binderStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBinderStatus() {
        return binderStatus;
    }

    /**
     * Sets the value of the binderStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBinderStatus(String value) {
        this.binderStatus = value;
    }

    /**
     * Gets the value of the futureItems property.
     * 
     * @return
     *     possible object is
     *     {@link FutureItems }
     *     
     */
    public FutureItems getFutureItems() {
        return futureItems;
    }

    /**
     * Sets the value of the futureItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link FutureItems }
     *     
     */
    public void setFutureItems(FutureItems value) {
        this.futureItems = value;
    }

    /**
     * Gets the value of the installmentGroup property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstallmentGroup() {
        return installmentGroup;
    }

    /**
     * Sets the value of the installmentGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstallmentGroup(Integer value) {
        this.installmentGroup = value;
    }

    /**
     * Gets the value of the invoiceCreatedBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceCreatedBegins() {
        return invoiceCreatedBegins;
    }

    /**
     * Sets the value of the invoiceCreatedBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceCreatedBegins(XMLGregorianCalendar value) {
        this.invoiceCreatedBegins = value;
    }

    /**
     * Gets the value of the invoiceCreatedEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceCreatedEnds() {
        return invoiceCreatedEnds;
    }

    /**
     * Sets the value of the invoiceCreatedEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceCreatedEnds(XMLGregorianCalendar value) {
        this.invoiceCreatedEnds = value;
    }

    /**
     * Gets the value of the invoiceLastGeneratedDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceLastGeneratedDateBegins() {
        return invoiceLastGeneratedDateBegins;
    }

    /**
     * Sets the value of the invoiceLastGeneratedDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceLastGeneratedDateBegins(XMLGregorianCalendar value) {
        this.invoiceLastGeneratedDateBegins = value;
    }

    /**
     * Gets the value of the invoiceLastGeneratedDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvoiceLastGeneratedDateEnds() {
        return invoiceLastGeneratedDateEnds;
    }

    /**
     * Sets the value of the invoiceLastGeneratedDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvoiceLastGeneratedDateEnds(XMLGregorianCalendar value) {
        this.invoiceLastGeneratedDateEnds = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvoiceNumber(Integer value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the invoiceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * Sets the value of the invoiceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceStatus(String value) {
        this.invoiceStatus = value;
    }

    /**
     * Gets the value of the itemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getItemNumber() {
        return itemNumber;
    }

    /**
     * Sets the value of the itemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setItemNumber(Integer value) {
        this.itemNumber = value;
    }

    /**
     * Gets the value of the lateItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLateItemNumber() {
        return lateItemNumber;
    }

    /**
     * Sets the value of the lateItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLateItemNumber(Integer value) {
        this.lateItemNumber = value;
    }

    /**
     * Gets the value of the paymentDateBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDateBegins() {
        return paymentDateBegins;
    }

    /**
     * Sets the value of the paymentDateBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDateBegins(XMLGregorianCalendar value) {
        this.paymentDateBegins = value;
    }

    /**
     * Gets the value of the paymentDateEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentDateEnds() {
        return paymentDateEnds;
    }

    /**
     * Sets the value of the paymentDateEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentDateEnds(XMLGregorianCalendar value) {
        this.paymentDateEnds = value;
    }

    /**
     * Gets the value of the paymentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentID() {
        return paymentID;
    }

    /**
     * Sets the value of the paymentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentID(String value) {
        this.paymentID = value;
    }

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the policyNumberComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getPolicyNumberComparisonType() {
        return policyNumberComparisonType;
    }

    /**
     * Sets the value of the policyNumberComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setPolicyNumberComparisonType(ComparisonType value) {
        this.policyNumberComparisonType = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the siteIDComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getSiteIDComparisonType() {
        return siteIDComparisonType;
    }

    /**
     * Sets the value of the siteIDComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setSiteIDComparisonType(ComparisonType value) {
        this.siteIDComparisonType = value;
    }

    /**
     * Gets the value of the taxableItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTaxableItemNumber() {
        return taxableItemNumber;
    }

    /**
     * Sets the value of the taxableItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTaxableItemNumber(Integer value) {
        this.taxableItemNumber = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCode(String value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionID(Integer value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatus }
     *     
     */
    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatus }
     *     
     */
    public void setTransactionStatus(TransactionStatus value) {
        this.transactionStatus = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link Service }
     *     
     */
    public Service getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link Service }
     *     
     */
    public void setService(Service value) {
        this.service = value;
    }

    /**
     * Gets the value of the serviceID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceID() {
        return serviceID;
    }

    /**
     * Sets the value of the serviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceID(Integer value) {
        this.serviceID = value;
    }

}
