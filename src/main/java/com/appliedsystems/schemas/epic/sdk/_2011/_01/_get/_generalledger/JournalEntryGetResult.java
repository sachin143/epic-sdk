
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ArrayOfJournalEntry;


/**
 * <p>Java class for JournalEntryGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JournalEntryGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JournalEntries" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ArrayOfJournalEntry" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JournalEntryGetResult", propOrder = {
    "journalEntries",
    "totalPages"
})
public class JournalEntryGetResult {

    @XmlElement(name = "JournalEntries", nillable = true)
    protected ArrayOfJournalEntry journalEntries;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the journalEntries property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfJournalEntry }
     *     
     */
    public ArrayOfJournalEntry getJournalEntries() {
        return journalEntries;
    }

    /**
     * Sets the value of the journalEntries property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfJournalEntry }
     *     
     */
    public void setJournalEntries(ArrayOfJournalEntry value) {
        this.journalEntries = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
