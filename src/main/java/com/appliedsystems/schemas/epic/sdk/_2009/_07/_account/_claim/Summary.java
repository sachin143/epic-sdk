
package com.appliedsystems.schemas.epic.sdk._2009._07._account._claim;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.PolicyItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ReportingHistory;


/**
 * <p>Java class for Summary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Summary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyClaimNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Chargeable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CompanyClaimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateOfLoss" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateReported" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstimateAmount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Policies" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/}PolicyItems" minOccurs="0"/>
 *         &lt;element name="PreviouslyReported" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="RecordOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReportedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportedTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportingHistoryValue" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/_summary/}ReportingHistory" minOccurs="0"/>
 *         &lt;element name="Subrogation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Suitpending" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TimeOfLoss" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TimeReported" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CannotBeDetermined" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CSIODateReportedToCompany" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CSIOReportedToCompanyBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CSIOTimeReportedToCompany" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateAgencyBroker" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TimeAgencyBroker" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CatastropheCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Summary", propOrder = {
    "agencyClaimNumber",
    "chargeable",
    "claimID",
    "clientID",
    "companyClaimNumber",
    "dateOfLoss",
    "dateReported",
    "description",
    "estimateAmount",
    "policies",
    "previouslyReported",
    "recordOnly",
    "reportedBy",
    "reportedTo",
    "reportingHistoryValue",
    "subrogation",
    "suitpending",
    "timeOfLoss",
    "timeReported",
    "timestamp",
    "cannotBeDetermined",
    "csioDateReportedToCompany",
    "csioReportedToCompanyBy",
    "csioTimeReportedToCompany",
    "dateAgencyBroker",
    "timeAgencyBroker",
    "catastropheCode"
})
public class Summary {

    @XmlElement(name = "AgencyClaimNumber")
    protected Integer agencyClaimNumber;
    @XmlElement(name = "Chargeable")
    protected Boolean chargeable;
    @XmlElement(name = "ClaimID")
    protected Integer claimID;
    @XmlElement(name = "ClientID")
    protected Integer clientID;
    @XmlElement(name = "CompanyClaimNumber", nillable = true)
    protected String companyClaimNumber;
    @XmlElement(name = "DateOfLoss")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfLoss;
    @XmlElement(name = "DateReported")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateReported;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "EstimateAmount", nillable = true)
    protected Integer estimateAmount;
    @XmlElement(name = "Policies", nillable = true)
    protected PolicyItems policies;
    @XmlElement(name = "PreviouslyReported")
    protected Boolean previouslyReported;
    @XmlElement(name = "RecordOnly")
    protected Boolean recordOnly;
    @XmlElement(name = "ReportedBy", nillable = true)
    protected String reportedBy;
    @XmlElement(name = "ReportedTo", nillable = true)
    protected String reportedTo;
    @XmlElement(name = "ReportingHistoryValue", nillable = true)
    protected ReportingHistory reportingHistoryValue;
    @XmlElement(name = "Subrogation")
    protected Boolean subrogation;
    @XmlElement(name = "Suitpending")
    protected Boolean suitpending;
    @XmlElement(name = "TimeOfLoss", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeOfLoss;
    @XmlElement(name = "TimeReported", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeReported;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlElement(name = "CannotBeDetermined")
    protected Boolean cannotBeDetermined;
    @XmlElement(name = "CSIODateReportedToCompany", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar csioDateReportedToCompany;
    @XmlElement(name = "CSIOReportedToCompanyBy", nillable = true)
    protected String csioReportedToCompanyBy;
    @XmlElement(name = "CSIOTimeReportedToCompany", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar csioTimeReportedToCompany;
    @XmlElement(name = "DateAgencyBroker", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateAgencyBroker;
    @XmlElement(name = "TimeAgencyBroker", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeAgencyBroker;
    @XmlElement(name = "CatastropheCode", nillable = true)
    protected String catastropheCode;

    /**
     * Gets the value of the agencyClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgencyClaimNumber() {
        return agencyClaimNumber;
    }

    /**
     * Sets the value of the agencyClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgencyClaimNumber(Integer value) {
        this.agencyClaimNumber = value;
    }

    /**
     * Gets the value of the chargeable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChargeable() {
        return chargeable;
    }

    /**
     * Sets the value of the chargeable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChargeable(Boolean value) {
        this.chargeable = value;
    }

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the companyClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyClaimNumber() {
        return companyClaimNumber;
    }

    /**
     * Sets the value of the companyClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyClaimNumber(String value) {
        this.companyClaimNumber = value;
    }

    /**
     * Gets the value of the dateOfLoss property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfLoss() {
        return dateOfLoss;
    }

    /**
     * Sets the value of the dateOfLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfLoss(XMLGregorianCalendar value) {
        this.dateOfLoss = value;
    }

    /**
     * Gets the value of the dateReported property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateReported() {
        return dateReported;
    }

    /**
     * Sets the value of the dateReported property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateReported(XMLGregorianCalendar value) {
        this.dateReported = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the estimateAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEstimateAmount() {
        return estimateAmount;
    }

    /**
     * Sets the value of the estimateAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEstimateAmount(Integer value) {
        this.estimateAmount = value;
    }

    /**
     * Gets the value of the policies property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyItems }
     *     
     */
    public PolicyItems getPolicies() {
        return policies;
    }

    /**
     * Sets the value of the policies property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyItems }
     *     
     */
    public void setPolicies(PolicyItems value) {
        this.policies = value;
    }

    /**
     * Gets the value of the previouslyReported property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreviouslyReported() {
        return previouslyReported;
    }

    /**
     * Sets the value of the previouslyReported property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreviouslyReported(Boolean value) {
        this.previouslyReported = value;
    }

    /**
     * Gets the value of the recordOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRecordOnly() {
        return recordOnly;
    }

    /**
     * Sets the value of the recordOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRecordOnly(Boolean value) {
        this.recordOnly = value;
    }

    /**
     * Gets the value of the reportedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportedBy() {
        return reportedBy;
    }

    /**
     * Sets the value of the reportedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportedBy(String value) {
        this.reportedBy = value;
    }

    /**
     * Gets the value of the reportedTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportedTo() {
        return reportedTo;
    }

    /**
     * Sets the value of the reportedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportedTo(String value) {
        this.reportedTo = value;
    }

    /**
     * Gets the value of the reportingHistoryValue property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingHistory }
     *     
     */
    public ReportingHistory getReportingHistoryValue() {
        return reportingHistoryValue;
    }

    /**
     * Sets the value of the reportingHistoryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingHistory }
     *     
     */
    public void setReportingHistoryValue(ReportingHistory value) {
        this.reportingHistoryValue = value;
    }

    /**
     * Gets the value of the subrogation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubrogation() {
        return subrogation;
    }

    /**
     * Sets the value of the subrogation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubrogation(Boolean value) {
        this.subrogation = value;
    }

    /**
     * Gets the value of the suitpending property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuitpending() {
        return suitpending;
    }

    /**
     * Sets the value of the suitpending property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuitpending(Boolean value) {
        this.suitpending = value;
    }

    /**
     * Gets the value of the timeOfLoss property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeOfLoss() {
        return timeOfLoss;
    }

    /**
     * Sets the value of the timeOfLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeOfLoss(XMLGregorianCalendar value) {
        this.timeOfLoss = value;
    }

    /**
     * Gets the value of the timeReported property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeReported() {
        return timeReported;
    }

    /**
     * Sets the value of the timeReported property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeReported(XMLGregorianCalendar value) {
        this.timeReported = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * Gets the value of the cannotBeDetermined property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCannotBeDetermined() {
        return cannotBeDetermined;
    }

    /**
     * Sets the value of the cannotBeDetermined property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCannotBeDetermined(Boolean value) {
        this.cannotBeDetermined = value;
    }

    /**
     * Gets the value of the csioDateReportedToCompany property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCSIODateReportedToCompany() {
        return csioDateReportedToCompany;
    }

    /**
     * Sets the value of the csioDateReportedToCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCSIODateReportedToCompany(XMLGregorianCalendar value) {
        this.csioDateReportedToCompany = value;
    }

    /**
     * Gets the value of the csioReportedToCompanyBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSIOReportedToCompanyBy() {
        return csioReportedToCompanyBy;
    }

    /**
     * Sets the value of the csioReportedToCompanyBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSIOReportedToCompanyBy(String value) {
        this.csioReportedToCompanyBy = value;
    }

    /**
     * Gets the value of the csioTimeReportedToCompany property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCSIOTimeReportedToCompany() {
        return csioTimeReportedToCompany;
    }

    /**
     * Sets the value of the csioTimeReportedToCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCSIOTimeReportedToCompany(XMLGregorianCalendar value) {
        this.csioTimeReportedToCompany = value;
    }

    /**
     * Gets the value of the dateAgencyBroker property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateAgencyBroker() {
        return dateAgencyBroker;
    }

    /**
     * Sets the value of the dateAgencyBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateAgencyBroker(XMLGregorianCalendar value) {
        this.dateAgencyBroker = value;
    }

    /**
     * Gets the value of the timeAgencyBroker property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeAgencyBroker() {
        return timeAgencyBroker;
    }

    /**
     * Sets the value of the timeAgencyBroker property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeAgencyBroker(XMLGregorianCalendar value) {
        this.timeAgencyBroker = value;
    }

    /**
     * Gets the value of the catastropheCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatastropheCode() {
        return catastropheCode;
    }

    /**
     * Sets the value of the catastropheCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatastropheCode(String value) {
        this.catastropheCode = value;
    }

}
