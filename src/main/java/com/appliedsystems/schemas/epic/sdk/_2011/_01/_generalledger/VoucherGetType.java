
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VoucherID"/>
 *     &lt;enumeration value="Filtered"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VoucherGetType")
@XmlEnum
public enum VoucherGetType {

    @XmlEnumValue("VoucherID")
    VOUCHER_ID("VoucherID"),
    @XmlEnumValue("Filtered")
    FILTERED("Filtered");
    private final String value;

    VoucherGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoucherGetType fromValue(String v) {
        for (VoucherGetType c: VoucherGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
