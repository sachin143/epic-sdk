
package com.appliedsystems.schemas.epic.sdk._2017._02._common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2017._02._common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaymentMethodItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/", "PaymentMethodItem");
    private final static QName _PaymentMethodItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/", "PaymentMethodItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2017._02._common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentMethodItem }
     * 
     */
    public PaymentMethodItem createPaymentMethodItem() {
        return new PaymentMethodItem();
    }

    /**
     * Create an instance of {@link PaymentMethodItems }
     * 
     */
    public PaymentMethodItems createPaymentMethodItems() {
        return new PaymentMethodItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentMethodItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/", name = "PaymentMethodItem")
    public JAXBElement<PaymentMethodItem> createPaymentMethodItem(PaymentMethodItem value) {
        return new JAXBElement<PaymentMethodItem>(_PaymentMethodItem_QNAME, PaymentMethodItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentMethodItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2017/02/_common/", name = "PaymentMethodItems")
    public JAXBElement<PaymentMethodItems> createPaymentMethodItems(PaymentMethodItems value) {
        return new JAXBElement<PaymentMethodItems>(_PaymentMethodItems_QNAME, PaymentMethodItems.class, null, value);
    }

}
