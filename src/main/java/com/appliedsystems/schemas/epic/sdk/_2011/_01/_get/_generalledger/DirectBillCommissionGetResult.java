
package com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ArrayOfDirectBillCommission;


/**
 * <p>Java class for DirectBillCommissionGetResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DirectBillCommissionGetResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DirectBillCommissions" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/}ArrayOfDirectBillCommission" minOccurs="0"/>
 *         &lt;element name="TotalPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DirectBillCommissionGetResult", propOrder = {
    "directBillCommissions",
    "totalPages"
})
public class DirectBillCommissionGetResult {

    @XmlElement(name = "DirectBillCommissions", nillable = true)
    protected ArrayOfDirectBillCommission directBillCommissions;
    @XmlElement(name = "TotalPages")
    protected Integer totalPages;

    /**
     * Gets the value of the directBillCommissions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDirectBillCommission }
     *     
     */
    public ArrayOfDirectBillCommission getDirectBillCommissions() {
        return directBillCommissions;
    }

    /**
     * Sets the value of the directBillCommissions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDirectBillCommission }
     *     
     */
    public void setDirectBillCommissions(ArrayOfDirectBillCommission value) {
        this.directBillCommissions = value;
    }

    /**
     * Gets the value of the totalPages property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * Sets the value of the totalPages property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPages(Integer value) {
        this.totalPages = value;
    }

}
