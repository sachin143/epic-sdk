
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AllocationStructureGrouping complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationStructureGrouping">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationStructureCombination" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}StructureCombinations" minOccurs="0"/>
 *         &lt;element name="AllocationStructureGroupingDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocationStructureGroupingID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DefaultMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationStructureGrouping", propOrder = {
    "allocationStructureCombination",
    "allocationStructureGroupingDescription",
    "allocationStructureGroupingID",
    "defaultMethodID",
    "timestamp"
})
public class AllocationStructureGrouping {

    @XmlElement(name = "AllocationStructureCombination", nillable = true)
    protected StructureCombinations allocationStructureCombination;
    @XmlElement(name = "AllocationStructureGroupingDescription", nillable = true)
    protected String allocationStructureGroupingDescription;
    @XmlElement(name = "AllocationStructureGroupingID")
    protected Integer allocationStructureGroupingID;
    @XmlElement(name = "DefaultMethodID")
    protected Integer defaultMethodID;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the allocationStructureCombination property.
     * 
     * @return
     *     possible object is
     *     {@link StructureCombinations }
     *     
     */
    public StructureCombinations getAllocationStructureCombination() {
        return allocationStructureCombination;
    }

    /**
     * Sets the value of the allocationStructureCombination property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureCombinations }
     *     
     */
    public void setAllocationStructureCombination(StructureCombinations value) {
        this.allocationStructureCombination = value;
    }

    /**
     * Gets the value of the allocationStructureGroupingDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocationStructureGroupingDescription() {
        return allocationStructureGroupingDescription;
    }

    /**
     * Sets the value of the allocationStructureGroupingDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocationStructureGroupingDescription(String value) {
        this.allocationStructureGroupingDescription = value;
    }

    /**
     * Gets the value of the allocationStructureGroupingID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAllocationStructureGroupingID() {
        return allocationStructureGroupingID;
    }

    /**
     * Sets the value of the allocationStructureGroupingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAllocationStructureGroupingID(Integer value) {
        this.allocationStructureGroupingID = value;
    }

    /**
     * Gets the value of the defaultMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultMethodID() {
        return defaultMethodID;
    }

    /**
     * Sets the value of the defaultMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultMethodID(Integer value) {
        this.defaultMethodID = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
