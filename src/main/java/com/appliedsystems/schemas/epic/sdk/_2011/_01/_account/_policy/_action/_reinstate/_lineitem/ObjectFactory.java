
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Flags_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_reinstate/_lineitem/", "Flags");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Flags }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/_reinstate/_lineitem/", name = "Flags")
    public JAXBElement<Flags> createFlags(Flags value) {
        return new JAXBElement<Flags>(_Flags_QNAME, Flags.class, null, value);
    }

}
