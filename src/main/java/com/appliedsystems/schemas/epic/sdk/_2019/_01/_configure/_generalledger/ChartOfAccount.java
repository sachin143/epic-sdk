
package com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2019._01._common.StructureItems;


/**
 * <p>Java class for ChartOfAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChartOfAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllStructuresOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="AllowOnlyInJournalEntryProgram" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ChartOfAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsBankAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsRequiredAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LevelOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="StructureItems" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_common/}StructureItems" minOccurs="0"/>
 *         &lt;element name="SubaccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChartOfAccount", propOrder = {
    "accountNumberCode",
    "allStructuresOption",
    "allowOnlyInJournalEntryProgram",
    "chartOfAccountID",
    "description",
    "groupCode",
    "isActive",
    "isBankAccount",
    "isRequiredAccount",
    "levelOption",
    "structureItems",
    "subaccountCode",
    "timestamp"
})
public class ChartOfAccount {

    @XmlElement(name = "AccountNumberCode", nillable = true)
    protected String accountNumberCode;
    @XmlElement(name = "AllStructuresOption", nillable = true)
    protected OptionType allStructuresOption;
    @XmlElement(name = "AllowOnlyInJournalEntryProgram", nillable = true)
    protected Boolean allowOnlyInJournalEntryProgram;
    @XmlElement(name = "ChartOfAccountID")
    protected Integer chartOfAccountID;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "GroupCode", nillable = true)
    protected Integer groupCode;
    @XmlElement(name = "IsActive", nillable = true)
    protected Boolean isActive;
    @XmlElement(name = "IsBankAccount", nillable = true)
    protected Boolean isBankAccount;
    @XmlElement(name = "IsRequiredAccount", nillable = true)
    protected Boolean isRequiredAccount;
    @XmlElement(name = "LevelOption", nillable = true)
    protected OptionType levelOption;
    @XmlElement(name = "StructureItems", nillable = true)
    protected StructureItems structureItems;
    @XmlElement(name = "SubaccountCode", nillable = true)
    protected String subaccountCode;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the accountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumberCode() {
        return accountNumberCode;
    }

    /**
     * Sets the value of the accountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumberCode(String value) {
        this.accountNumberCode = value;
    }

    /**
     * Gets the value of the allStructuresOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getAllStructuresOption() {
        return allStructuresOption;
    }

    /**
     * Sets the value of the allStructuresOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setAllStructuresOption(OptionType value) {
        this.allStructuresOption = value;
    }

    /**
     * Gets the value of the allowOnlyInJournalEntryProgram property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowOnlyInJournalEntryProgram() {
        return allowOnlyInJournalEntryProgram;
    }

    /**
     * Sets the value of the allowOnlyInJournalEntryProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowOnlyInJournalEntryProgram(Boolean value) {
        this.allowOnlyInJournalEntryProgram = value;
    }

    /**
     * Gets the value of the chartOfAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChartOfAccountID() {
        return chartOfAccountID;
    }

    /**
     * Sets the value of the chartOfAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChartOfAccountID(Integer value) {
        this.chartOfAccountID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the groupCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the value of the groupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupCode(Integer value) {
        this.groupCode = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the isBankAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBankAccount() {
        return isBankAccount;
    }

    /**
     * Sets the value of the isBankAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBankAccount(Boolean value) {
        this.isBankAccount = value;
    }

    /**
     * Gets the value of the isRequiredAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRequiredAccount() {
        return isRequiredAccount;
    }

    /**
     * Sets the value of the isRequiredAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRequiredAccount(Boolean value) {
        this.isRequiredAccount = value;
    }

    /**
     * Gets the value of the levelOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getLevelOption() {
        return levelOption;
    }

    /**
     * Sets the value of the levelOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setLevelOption(OptionType value) {
        this.levelOption = value;
    }

    /**
     * Gets the value of the structureItems property.
     * 
     * @return
     *     possible object is
     *     {@link StructureItems }
     *     
     */
    public StructureItems getStructureItems() {
        return structureItems;
    }

    /**
     * Sets the value of the structureItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructureItems }
     *     
     */
    public void setStructureItems(StructureItems value) {
        this.structureItems = value;
    }

    /**
     * Gets the value of the subaccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubaccountCode() {
        return subaccountCode;
    }

    /**
     * Sets the value of the subaccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubaccountCode(String value) {
        this.subaccountCode = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
