
package com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Bank complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bank">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankDetailValue" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}BankDetail" minOccurs="0"/>
 *         &lt;element name="BankID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BankIncludeSubaccounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="BankSubaccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FeesGeneralLedgerAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesGeneralLedgerSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesStructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesStructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesStructureDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeesStructureProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InterestGeneralLedgerAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestGeneralLedgerSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestStructureAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestStructureBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestStructureDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterestStructureProfitCenterCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsReadOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementComments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="StatementDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementEnteredBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementEnteredDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="StatementHasActivities" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementIncludeFutureVoidsOffsets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementLastUpdatedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementLastUpdatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="StatementNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatementRecordFees" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementRecordInterest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="StatementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bank", propOrder = {
    "bankAccountNumberCode",
    "bankDetailValue",
    "bankID",
    "bankIncludeSubaccounts",
    "bankSubaccountNumberCode",
    "feesAmount",
    "feesGeneralLedgerAccountNumberCode",
    "feesGeneralLedgerSubAccountNumberCode",
    "feesStructureAgencyCode",
    "feesStructureBranchCode",
    "feesStructureDepartmentCode",
    "feesStructureProfitCenterCode",
    "interestAmount",
    "interestGeneralLedgerAccountNumberCode",
    "interestGeneralLedgerSubAccountNumberCode",
    "interestStructureAgencyCode",
    "interestStructureBranchCode",
    "interestStructureDepartmentCode",
    "interestStructureProfitCenterCode",
    "isReadOnly",
    "statementComments",
    "statementDate",
    "statementDescription",
    "statementEnteredBy",
    "statementEnteredDate",
    "statementHasActivities",
    "statementIncludeFutureVoidsOffsets",
    "statementLastUpdatedBy",
    "statementLastUpdatedDate",
    "statementNumber",
    "statementRecordFees",
    "statementRecordInterest",
    "statementStatus",
    "timestamp"
})
public class Bank {

    @XmlElement(name = "BankAccountNumberCode", nillable = true)
    protected String bankAccountNumberCode;
    @XmlElement(name = "BankDetailValue", nillable = true)
    protected BankDetail bankDetailValue;
    @XmlElement(name = "BankID")
    protected Integer bankID;
    @XmlElement(name = "BankIncludeSubaccounts", nillable = true)
    protected Boolean bankIncludeSubaccounts;
    @XmlElement(name = "BankSubaccountNumberCode", nillable = true)
    protected String bankSubaccountNumberCode;
    @XmlElement(name = "FeesAmount", nillable = true)
    protected BigDecimal feesAmount;
    @XmlElement(name = "FeesGeneralLedgerAccountNumberCode", nillable = true)
    protected String feesGeneralLedgerAccountNumberCode;
    @XmlElement(name = "FeesGeneralLedgerSubAccountNumberCode", nillable = true)
    protected String feesGeneralLedgerSubAccountNumberCode;
    @XmlElement(name = "FeesStructureAgencyCode", nillable = true)
    protected String feesStructureAgencyCode;
    @XmlElement(name = "FeesStructureBranchCode", nillable = true)
    protected String feesStructureBranchCode;
    @XmlElement(name = "FeesStructureDepartmentCode", nillable = true)
    protected String feesStructureDepartmentCode;
    @XmlElement(name = "FeesStructureProfitCenterCode", nillable = true)
    protected String feesStructureProfitCenterCode;
    @XmlElement(name = "InterestAmount", nillable = true)
    protected BigDecimal interestAmount;
    @XmlElement(name = "InterestGeneralLedgerAccountNumberCode", nillable = true)
    protected String interestGeneralLedgerAccountNumberCode;
    @XmlElement(name = "InterestGeneralLedgerSubAccountNumberCode", nillable = true)
    protected String interestGeneralLedgerSubAccountNumberCode;
    @XmlElement(name = "InterestStructureAgencyCode", nillable = true)
    protected String interestStructureAgencyCode;
    @XmlElement(name = "InterestStructureBranchCode", nillable = true)
    protected String interestStructureBranchCode;
    @XmlElement(name = "InterestStructureDepartmentCode", nillable = true)
    protected String interestStructureDepartmentCode;
    @XmlElement(name = "InterestStructureProfitCenterCode", nillable = true)
    protected String interestStructureProfitCenterCode;
    @XmlElement(name = "IsReadOnly")
    protected Boolean isReadOnly;
    @XmlElement(name = "StatementComments", nillable = true)
    protected String statementComments;
    @XmlElement(name = "StatementDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementDate;
    @XmlElement(name = "StatementDescription", nillable = true)
    protected String statementDescription;
    @XmlElement(name = "StatementEnteredBy", nillable = true)
    protected String statementEnteredBy;
    @XmlElement(name = "StatementEnteredDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementEnteredDate;
    @XmlElement(name = "StatementHasActivities", nillable = true)
    protected Boolean statementHasActivities;
    @XmlElement(name = "StatementIncludeFutureVoidsOffsets", nillable = true)
    protected Boolean statementIncludeFutureVoidsOffsets;
    @XmlElement(name = "StatementLastUpdatedBy", nillable = true)
    protected String statementLastUpdatedBy;
    @XmlElement(name = "StatementLastUpdatedDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statementLastUpdatedDate;
    @XmlElement(name = "StatementNumber", nillable = true)
    protected String statementNumber;
    @XmlElement(name = "StatementRecordFees", nillable = true)
    protected Boolean statementRecordFees;
    @XmlElement(name = "StatementRecordInterest", nillable = true)
    protected Boolean statementRecordInterest;
    @XmlElement(name = "StatementStatus", nillable = true)
    protected String statementStatus;
    @XmlElement(name = "Timestamp", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;

    /**
     * Gets the value of the bankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNumberCode() {
        return bankAccountNumberCode;
    }

    /**
     * Sets the value of the bankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNumberCode(String value) {
        this.bankAccountNumberCode = value;
    }

    /**
     * Gets the value of the bankDetailValue property.
     * 
     * @return
     *     possible object is
     *     {@link BankDetail }
     *     
     */
    public BankDetail getBankDetailValue() {
        return bankDetailValue;
    }

    /**
     * Sets the value of the bankDetailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankDetail }
     *     
     */
    public void setBankDetailValue(BankDetail value) {
        this.bankDetailValue = value;
    }

    /**
     * Gets the value of the bankID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBankID() {
        return bankID;
    }

    /**
     * Sets the value of the bankID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBankID(Integer value) {
        this.bankID = value;
    }

    /**
     * Gets the value of the bankIncludeSubaccounts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBankIncludeSubaccounts() {
        return bankIncludeSubaccounts;
    }

    /**
     * Sets the value of the bankIncludeSubaccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBankIncludeSubaccounts(Boolean value) {
        this.bankIncludeSubaccounts = value;
    }

    /**
     * Gets the value of the bankSubaccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankSubaccountNumberCode() {
        return bankSubaccountNumberCode;
    }

    /**
     * Sets the value of the bankSubaccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankSubaccountNumberCode(String value) {
        this.bankSubaccountNumberCode = value;
    }

    /**
     * Gets the value of the feesAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFeesAmount() {
        return feesAmount;
    }

    /**
     * Sets the value of the feesAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFeesAmount(BigDecimal value) {
        this.feesAmount = value;
    }

    /**
     * Gets the value of the feesGeneralLedgerAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesGeneralLedgerAccountNumberCode() {
        return feesGeneralLedgerAccountNumberCode;
    }

    /**
     * Sets the value of the feesGeneralLedgerAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesGeneralLedgerAccountNumberCode(String value) {
        this.feesGeneralLedgerAccountNumberCode = value;
    }

    /**
     * Gets the value of the feesGeneralLedgerSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesGeneralLedgerSubAccountNumberCode() {
        return feesGeneralLedgerSubAccountNumberCode;
    }

    /**
     * Sets the value of the feesGeneralLedgerSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesGeneralLedgerSubAccountNumberCode(String value) {
        this.feesGeneralLedgerSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the feesStructureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesStructureAgencyCode() {
        return feesStructureAgencyCode;
    }

    /**
     * Sets the value of the feesStructureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesStructureAgencyCode(String value) {
        this.feesStructureAgencyCode = value;
    }

    /**
     * Gets the value of the feesStructureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesStructureBranchCode() {
        return feesStructureBranchCode;
    }

    /**
     * Sets the value of the feesStructureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesStructureBranchCode(String value) {
        this.feesStructureBranchCode = value;
    }

    /**
     * Gets the value of the feesStructureDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesStructureDepartmentCode() {
        return feesStructureDepartmentCode;
    }

    /**
     * Sets the value of the feesStructureDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesStructureDepartmentCode(String value) {
        this.feesStructureDepartmentCode = value;
    }

    /**
     * Gets the value of the feesStructureProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeesStructureProfitCenterCode() {
        return feesStructureProfitCenterCode;
    }

    /**
     * Sets the value of the feesStructureProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeesStructureProfitCenterCode(String value) {
        this.feesStructureProfitCenterCode = value;
    }

    /**
     * Gets the value of the interestAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInterestAmount() {
        return interestAmount;
    }

    /**
     * Sets the value of the interestAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInterestAmount(BigDecimal value) {
        this.interestAmount = value;
    }

    /**
     * Gets the value of the interestGeneralLedgerAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestGeneralLedgerAccountNumberCode() {
        return interestGeneralLedgerAccountNumberCode;
    }

    /**
     * Sets the value of the interestGeneralLedgerAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestGeneralLedgerAccountNumberCode(String value) {
        this.interestGeneralLedgerAccountNumberCode = value;
    }

    /**
     * Gets the value of the interestGeneralLedgerSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestGeneralLedgerSubAccountNumberCode() {
        return interestGeneralLedgerSubAccountNumberCode;
    }

    /**
     * Sets the value of the interestGeneralLedgerSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestGeneralLedgerSubAccountNumberCode(String value) {
        this.interestGeneralLedgerSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the interestStructureAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestStructureAgencyCode() {
        return interestStructureAgencyCode;
    }

    /**
     * Sets the value of the interestStructureAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestStructureAgencyCode(String value) {
        this.interestStructureAgencyCode = value;
    }

    /**
     * Gets the value of the interestStructureBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestStructureBranchCode() {
        return interestStructureBranchCode;
    }

    /**
     * Sets the value of the interestStructureBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestStructureBranchCode(String value) {
        this.interestStructureBranchCode = value;
    }

    /**
     * Gets the value of the interestStructureDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestStructureDepartmentCode() {
        return interestStructureDepartmentCode;
    }

    /**
     * Sets the value of the interestStructureDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestStructureDepartmentCode(String value) {
        this.interestStructureDepartmentCode = value;
    }

    /**
     * Gets the value of the interestStructureProfitCenterCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestStructureProfitCenterCode() {
        return interestStructureProfitCenterCode;
    }

    /**
     * Sets the value of the interestStructureProfitCenterCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestStructureProfitCenterCode(String value) {
        this.interestStructureProfitCenterCode = value;
    }

    /**
     * Gets the value of the isReadOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Sets the value of the isReadOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsReadOnly(Boolean value) {
        this.isReadOnly = value;
    }

    /**
     * Gets the value of the statementComments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementComments() {
        return statementComments;
    }

    /**
     * Sets the value of the statementComments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementComments(String value) {
        this.statementComments = value;
    }

    /**
     * Gets the value of the statementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementDate() {
        return statementDate;
    }

    /**
     * Sets the value of the statementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementDate(XMLGregorianCalendar value) {
        this.statementDate = value;
    }

    /**
     * Gets the value of the statementDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementDescription() {
        return statementDescription;
    }

    /**
     * Sets the value of the statementDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementDescription(String value) {
        this.statementDescription = value;
    }

    /**
     * Gets the value of the statementEnteredBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementEnteredBy() {
        return statementEnteredBy;
    }

    /**
     * Sets the value of the statementEnteredBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementEnteredBy(String value) {
        this.statementEnteredBy = value;
    }

    /**
     * Gets the value of the statementEnteredDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementEnteredDate() {
        return statementEnteredDate;
    }

    /**
     * Sets the value of the statementEnteredDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementEnteredDate(XMLGregorianCalendar value) {
        this.statementEnteredDate = value;
    }

    /**
     * Gets the value of the statementHasActivities property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatementHasActivities() {
        return statementHasActivities;
    }

    /**
     * Sets the value of the statementHasActivities property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatementHasActivities(Boolean value) {
        this.statementHasActivities = value;
    }

    /**
     * Gets the value of the statementIncludeFutureVoidsOffsets property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatementIncludeFutureVoidsOffsets() {
        return statementIncludeFutureVoidsOffsets;
    }

    /**
     * Sets the value of the statementIncludeFutureVoidsOffsets property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatementIncludeFutureVoidsOffsets(Boolean value) {
        this.statementIncludeFutureVoidsOffsets = value;
    }

    /**
     * Gets the value of the statementLastUpdatedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementLastUpdatedBy() {
        return statementLastUpdatedBy;
    }

    /**
     * Sets the value of the statementLastUpdatedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementLastUpdatedBy(String value) {
        this.statementLastUpdatedBy = value;
    }

    /**
     * Gets the value of the statementLastUpdatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatementLastUpdatedDate() {
        return statementLastUpdatedDate;
    }

    /**
     * Sets the value of the statementLastUpdatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatementLastUpdatedDate(XMLGregorianCalendar value) {
        this.statementLastUpdatedDate = value;
    }

    /**
     * Gets the value of the statementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementNumber() {
        return statementNumber;
    }

    /**
     * Sets the value of the statementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementNumber(String value) {
        this.statementNumber = value;
    }

    /**
     * Gets the value of the statementRecordFees property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatementRecordFees() {
        return statementRecordFees;
    }

    /**
     * Sets the value of the statementRecordFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatementRecordFees(Boolean value) {
        this.statementRecordFees = value;
    }

    /**
     * Gets the value of the statementRecordInterest property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatementRecordInterest() {
        return statementRecordInterest;
    }

    /**
     * Sets the value of the statementRecordInterest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatementRecordInterest(Boolean value) {
        this.statementRecordInterest = value;
    }

    /**
     * Gets the value of the statementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementStatus() {
        return statementStatus;
    }

    /**
     * Sets the value of the statementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementStatus(String value) {
        this.statementStatus = value;
    }

    /**
     * Gets the value of the timestamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the value of the timestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

}
