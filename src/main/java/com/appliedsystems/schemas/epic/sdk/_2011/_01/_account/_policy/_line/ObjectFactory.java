
package com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _History_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", "History");
    private final static QName _ProducerBrokerCommissions_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", "ProducerBrokerCommissions");
    private final static QName _ServiceSummaryItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", "ServiceSummaryItem");
    private final static QName _ServiceSummaryItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", "ServiceSummaryItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceSummaryItems }
     * 
     */
    public ServiceSummaryItems createServiceSummaryItems() {
        return new ServiceSummaryItems();
    }

    /**
     * Create an instance of {@link ServiceSummaryItem }
     * 
     */
    public ServiceSummaryItem createServiceSummaryItem() {
        return new ServiceSummaryItem();
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link ProducerBrokerCommissions }
     * 
     */
    public ProducerBrokerCommissions createProducerBrokerCommissions() {
        return new ProducerBrokerCommissions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link History }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", name = "History")
    public JAXBElement<History> createHistory(History value) {
        return new JAXBElement<History>(_History_QNAME, History.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProducerBrokerCommissions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", name = "ProducerBrokerCommissions")
    public JAXBElement<ProducerBrokerCommissions> createProducerBrokerCommissions(ProducerBrokerCommissions value) {
        return new JAXBElement<ProducerBrokerCommissions>(_ProducerBrokerCommissions_QNAME, ProducerBrokerCommissions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceSummaryItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", name = "ServiceSummaryItem")
    public JAXBElement<ServiceSummaryItem> createServiceSummaryItem(ServiceSummaryItem value) {
        return new JAXBElement<ServiceSummaryItem>(_ServiceSummaryItem_QNAME, ServiceSummaryItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceSummaryItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_line/", name = "ServiceSummaryItems")
    public JAXBElement<ServiceSummaryItems> createServiceSummaryItems(ServiceSummaryItems value) {
        return new JAXBElement<ServiceSummaryItems>(_ServiceSummaryItems_QNAME, ServiceSummaryItems.class, null, value);
    }

}
