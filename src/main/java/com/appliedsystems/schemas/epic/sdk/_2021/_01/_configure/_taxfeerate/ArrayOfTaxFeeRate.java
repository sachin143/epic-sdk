
package com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTaxFeeRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTaxFeeRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxFeeRate" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_taxfeerate/}TaxFeeRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTaxFeeRate", propOrder = {
    "taxFeeRates"
})
public class ArrayOfTaxFeeRate {

    @XmlElement(name = "TaxFeeRate", nillable = true)
    protected List<TaxFeeRate> taxFeeRates;

    /**
     * Gets the value of the taxFeeRates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxFeeRates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxFeeRates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxFeeRate }
     * 
     * 
     */
    public List<TaxFeeRate> getTaxFeeRates() {
        if (taxFeeRates == null) {
            taxFeeRates = new ArrayList<TaxFeeRate>();
        }
        return this.taxFeeRates;
    }

}
