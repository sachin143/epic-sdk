
package com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancelPolicyAdditionalInterestGetType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CancelPolicyAdditionalInterestGetType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CancellationID"/>
 *     &lt;enumeration value="CancelPolicyAdditionalInterestID"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CancelPolicyAdditionalInterestGetType")
@XmlEnum
public enum CancelPolicyAdditionalInterestGetType {

    @XmlEnumValue("CancellationID")
    CANCELLATION_ID("CancellationID"),
    @XmlEnumValue("CancelPolicyAdditionalInterestID")
    CANCEL_POLICY_ADDITIONAL_INTEREST_ID("CancelPolicyAdditionalInterestID");
    private final String value;

    CancelPolicyAdditionalInterestGetType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CancelPolicyAdditionalInterestGetType fromValue(String v) {
        for (CancelPolicyAdditionalInterestGetType c: CancelPolicyAdditionalInterestGetType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
