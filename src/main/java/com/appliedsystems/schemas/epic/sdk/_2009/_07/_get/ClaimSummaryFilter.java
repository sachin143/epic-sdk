
package com.appliedsystems.schemas.epic.sdk._2009._07._get;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ComparisonType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.Status;


/**
 * <p>Java class for ClaimSummaryFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimSummaryFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyClaimNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClaimID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ClaimStatus" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_claimsummaryfilter/}Status" minOccurs="0"/>
 *         &lt;element name="ClientID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CompanyClaimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyClaimNumberComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_claimsummaryfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="DateOfLossBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateOfLossEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateReportedBegins" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateReportedEnds" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DescriptionComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_claimsummaryfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="LossType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyNumberComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/_claimsummaryfilter/}ComparisonType" minOccurs="0"/>
 *         &lt;element name="ServicingRoleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServicingRoleEmployeeLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimSummaryFilter", propOrder = {
    "agencyClaimNumber",
    "claimID",
    "claimStatus",
    "clientID",
    "companyClaimNumber",
    "companyClaimNumberComparisonType",
    "dateOfLossBegins",
    "dateOfLossEnds",
    "dateReportedBegins",
    "dateReportedEnds",
    "description",
    "descriptionComparisonType",
    "lossType",
    "policyNumber",
    "policyNumberComparisonType",
    "servicingRoleCode",
    "servicingRoleEmployeeLookupCode"
})
public class ClaimSummaryFilter {

    @XmlElement(name = "AgencyClaimNumber", nillable = true)
    protected Integer agencyClaimNumber;
    @XmlElement(name = "ClaimID", nillable = true)
    protected Integer claimID;
    @XmlElement(name = "ClaimStatus")
    @XmlSchemaType(name = "string")
    protected Status claimStatus;
    @XmlElement(name = "ClientID", nillable = true)
    protected Integer clientID;
    @XmlElement(name = "CompanyClaimNumber", nillable = true)
    protected String companyClaimNumber;
    @XmlElement(name = "CompanyClaimNumberComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType companyClaimNumberComparisonType;
    @XmlElement(name = "DateOfLossBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfLossBegins;
    @XmlElement(name = "DateOfLossEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfLossEnds;
    @XmlElement(name = "DateReportedBegins", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateReportedBegins;
    @XmlElement(name = "DateReportedEnds", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateReportedEnds;
    @XmlElement(name = "Description", nillable = true)
    protected String description;
    @XmlElement(name = "DescriptionComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType descriptionComparisonType;
    @XmlElement(name = "LossType", nillable = true)
    protected String lossType;
    @XmlElement(name = "PolicyNumber", nillable = true)
    protected String policyNumber;
    @XmlElement(name = "PolicyNumberComparisonType")
    @XmlSchemaType(name = "string")
    protected ComparisonType policyNumberComparisonType;
    @XmlElement(name = "ServicingRoleCode", nillable = true)
    protected String servicingRoleCode;
    @XmlElement(name = "ServicingRoleEmployeeLookupCode", nillable = true)
    protected String servicingRoleEmployeeLookupCode;

    /**
     * Gets the value of the agencyClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgencyClaimNumber() {
        return agencyClaimNumber;
    }

    /**
     * Sets the value of the agencyClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgencyClaimNumber(Integer value) {
        this.agencyClaimNumber = value;
    }

    /**
     * Gets the value of the claimID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClaimID() {
        return claimID;
    }

    /**
     * Sets the value of the claimID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClaimID(Integer value) {
        this.claimID = value;
    }

    /**
     * Gets the value of the claimStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getClaimStatus() {
        return claimStatus;
    }

    /**
     * Sets the value of the claimStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setClaimStatus(Status value) {
        this.claimStatus = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setClientID(Integer value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the companyClaimNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyClaimNumber() {
        return companyClaimNumber;
    }

    /**
     * Sets the value of the companyClaimNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyClaimNumber(String value) {
        this.companyClaimNumber = value;
    }

    /**
     * Gets the value of the companyClaimNumberComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getCompanyClaimNumberComparisonType() {
        return companyClaimNumberComparisonType;
    }

    /**
     * Sets the value of the companyClaimNumberComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setCompanyClaimNumberComparisonType(ComparisonType value) {
        this.companyClaimNumberComparisonType = value;
    }

    /**
     * Gets the value of the dateOfLossBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfLossBegins() {
        return dateOfLossBegins;
    }

    /**
     * Sets the value of the dateOfLossBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfLossBegins(XMLGregorianCalendar value) {
        this.dateOfLossBegins = value;
    }

    /**
     * Gets the value of the dateOfLossEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfLossEnds() {
        return dateOfLossEnds;
    }

    /**
     * Sets the value of the dateOfLossEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfLossEnds(XMLGregorianCalendar value) {
        this.dateOfLossEnds = value;
    }

    /**
     * Gets the value of the dateReportedBegins property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateReportedBegins() {
        return dateReportedBegins;
    }

    /**
     * Sets the value of the dateReportedBegins property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateReportedBegins(XMLGregorianCalendar value) {
        this.dateReportedBegins = value;
    }

    /**
     * Gets the value of the dateReportedEnds property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateReportedEnds() {
        return dateReportedEnds;
    }

    /**
     * Sets the value of the dateReportedEnds property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateReportedEnds(XMLGregorianCalendar value) {
        this.dateReportedEnds = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the descriptionComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getDescriptionComparisonType() {
        return descriptionComparisonType;
    }

    /**
     * Sets the value of the descriptionComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setDescriptionComparisonType(ComparisonType value) {
        this.descriptionComparisonType = value;
    }

    /**
     * Gets the value of the lossType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLossType() {
        return lossType;
    }

    /**
     * Sets the value of the lossType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLossType(String value) {
        this.lossType = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the policyNumberComparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ComparisonType }
     *     
     */
    public ComparisonType getPolicyNumberComparisonType() {
        return policyNumberComparisonType;
    }

    /**
     * Sets the value of the policyNumberComparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComparisonType }
     *     
     */
    public void setPolicyNumberComparisonType(ComparisonType value) {
        this.policyNumberComparisonType = value;
    }

    /**
     * Gets the value of the servicingRoleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingRoleCode() {
        return servicingRoleCode;
    }

    /**
     * Sets the value of the servicingRoleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingRoleCode(String value) {
        this.servicingRoleCode = value;
    }

    /**
     * Gets the value of the servicingRoleEmployeeLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicingRoleEmployeeLookupCode() {
        return servicingRoleEmployeeLookupCode;
    }

    /**
     * Sets the value of the servicingRoleEmployeeLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicingRoleEmployeeLookupCode(String value) {
        this.servicingRoleEmployeeLookupCode = value;
    }

}
