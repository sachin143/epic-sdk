
package com.appliedsystems.schemas.epic.sdk._2013._11._account;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2013._11._account package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AttachmentAssociationType_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2013/11/_account/", "AttachmentAssociationType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2013._11._account
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentAssociationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2013/11/_account/", name = "AttachmentAssociationType")
    public JAXBElement<AttachmentAssociationType> createAttachmentAssociationType(AttachmentAssociationType value) {
        return new JAXBElement<AttachmentAssociationType>(_AttachmentAssociationType_QNAME, AttachmentAssociationType.class, null, value);
    }

}
