
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.CreditItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.DebitItems;


/**
 * <p>Java class for ApplyCreditsToDebits complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplyCreditsToDebits">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyRemainingBalanceToOption" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}OptionType" minOccurs="0"/>
 *         &lt;element name="ApplyRemainingBalanceToPolicyClientLookupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyRemainingBalanceToPolicyIncludeHistory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ApplyRemainingBalanceToPolicyLineID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Credits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/}CreditItems" minOccurs="0"/>
 *         &lt;element name="Debits" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_applycreditstodebits/}DebitItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyCreditsToDebits", propOrder = {
    "agencyCode",
    "applyRemainingBalanceToOption",
    "applyRemainingBalanceToPolicyClientLookupCode",
    "applyRemainingBalanceToPolicyIncludeHistory",
    "applyRemainingBalanceToPolicyLineID",
    "credits",
    "debits"
})
public class ApplyCreditsToDebits {

    @XmlElement(name = "AgencyCode", nillable = true)
    protected String agencyCode;
    @XmlElement(name = "ApplyRemainingBalanceToOption", nillable = true)
    protected OptionType applyRemainingBalanceToOption;
    @XmlElement(name = "ApplyRemainingBalanceToPolicyClientLookupCode", nillable = true)
    protected String applyRemainingBalanceToPolicyClientLookupCode;
    @XmlElement(name = "ApplyRemainingBalanceToPolicyIncludeHistory")
    protected Boolean applyRemainingBalanceToPolicyIncludeHistory;
    @XmlElement(name = "ApplyRemainingBalanceToPolicyLineID")
    protected Integer applyRemainingBalanceToPolicyLineID;
    @XmlElement(name = "Credits", nillable = true)
    protected CreditItems credits;
    @XmlElement(name = "Debits", nillable = true)
    protected DebitItems debits;

    /**
     * Gets the value of the agencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyCode() {
        return agencyCode;
    }

    /**
     * Sets the value of the agencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyCode(String value) {
        this.agencyCode = value;
    }

    /**
     * Gets the value of the applyRemainingBalanceToOption property.
     * 
     * @return
     *     possible object is
     *     {@link OptionType }
     *     
     */
    public OptionType getApplyRemainingBalanceToOption() {
        return applyRemainingBalanceToOption;
    }

    /**
     * Sets the value of the applyRemainingBalanceToOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionType }
     *     
     */
    public void setApplyRemainingBalanceToOption(OptionType value) {
        this.applyRemainingBalanceToOption = value;
    }

    /**
     * Gets the value of the applyRemainingBalanceToPolicyClientLookupCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplyRemainingBalanceToPolicyClientLookupCode() {
        return applyRemainingBalanceToPolicyClientLookupCode;
    }

    /**
     * Sets the value of the applyRemainingBalanceToPolicyClientLookupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplyRemainingBalanceToPolicyClientLookupCode(String value) {
        this.applyRemainingBalanceToPolicyClientLookupCode = value;
    }

    /**
     * Gets the value of the applyRemainingBalanceToPolicyIncludeHistory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyRemainingBalanceToPolicyIncludeHistory() {
        return applyRemainingBalanceToPolicyIncludeHistory;
    }

    /**
     * Sets the value of the applyRemainingBalanceToPolicyIncludeHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyRemainingBalanceToPolicyIncludeHistory(Boolean value) {
        this.applyRemainingBalanceToPolicyIncludeHistory = value;
    }

    /**
     * Gets the value of the applyRemainingBalanceToPolicyLineID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getApplyRemainingBalanceToPolicyLineID() {
        return applyRemainingBalanceToPolicyLineID;
    }

    /**
     * Sets the value of the applyRemainingBalanceToPolicyLineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setApplyRemainingBalanceToPolicyLineID(Integer value) {
        this.applyRemainingBalanceToPolicyLineID = value;
    }

    /**
     * Gets the value of the credits property.
     * 
     * @return
     *     possible object is
     *     {@link CreditItems }
     *     
     */
    public CreditItems getCredits() {
        return credits;
    }

    /**
     * Sets the value of the credits property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditItems }
     *     
     */
    public void setCredits(CreditItems value) {
        this.credits = value;
    }

    /**
     * Gets the value of the debits property.
     * 
     * @return
     *     possible object is
     *     {@link DebitItems }
     *     
     */
    public DebitItems getDebits() {
        return debits;
    }

    /**
     * Sets the value of the debits property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebitItems }
     *     
     */
    public void setDebits(DebitItems value) {
        this.debits = value;
    }

}
