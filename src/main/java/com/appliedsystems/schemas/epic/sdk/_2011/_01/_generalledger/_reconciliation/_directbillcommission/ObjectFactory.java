
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Summary_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", "Summary");
    private final static QName _ReconcileDetailItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", "ReconcileDetailItems");
    private final static QName _ReconcileDetailItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", "ReconcileDetailItem");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReconcileDetailItem }
     * 
     */
    public ReconcileDetailItem createReconcileDetailItem() {
        return new ReconcileDetailItem();
    }

    /**
     * Create an instance of {@link Summary }
     * 
     */
    public Summary createSummary() {
        return new Summary();
    }

    /**
     * Create an instance of {@link ReconcileDetailItems }
     * 
     */
    public ReconcileDetailItems createReconcileDetailItems() {
        return new ReconcileDetailItems();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Summary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", name = "Summary")
    public JAXBElement<Summary> createSummary(Summary value) {
        return new JAXBElement<Summary>(_Summary_QNAME, Summary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReconcileDetailItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", name = "ReconcileDetailItems")
    public JAXBElement<ReconcileDetailItems> createReconcileDetailItems(ReconcileDetailItems value) {
        return new JAXBElement<ReconcileDetailItems>(_ReconcileDetailItems_QNAME, ReconcileDetailItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReconcileDetailItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/_directbillcommission/", name = "ReconcileDetailItem")
    public JAXBElement<ReconcileDetailItem> createReconcileDetailItem(ReconcileDetailItem value) {
        return new JAXBElement<ReconcileDetailItem>(_ReconcileDetailItem_QNAME, ReconcileDetailItem.class, null, value);
    }

}
