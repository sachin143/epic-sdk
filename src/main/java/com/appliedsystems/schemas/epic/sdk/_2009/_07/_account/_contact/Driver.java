
package com.appliedsystems.schemas.epic.sdk._2009._07._account._contact;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Driver complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Driver">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccidentPreventionCourseDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="CommercialExperienceBeganDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateLicensed" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateLicensedMA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DriverTraining" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriversLicenseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GoodStudent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StateLicensed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StudentOver100MilesFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Driver", propOrder = {
    "accidentPreventionCourseDate",
    "commercialExperienceBeganDate",
    "dateLicensed",
    "dateLicensedMA",
    "driverTraining",
    "driverType",
    "driversLicenseNumber",
    "goodStudent",
    "stateLicensed",
    "studentOver100MilesFlag"
})
public class Driver {

    @XmlElement(name = "AccidentPreventionCourseDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar accidentPreventionCourseDate;
    @XmlElement(name = "CommercialExperienceBeganDate", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commercialExperienceBeganDate;
    @XmlElement(name = "DateLicensed", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateLicensed;
    @XmlElement(name = "DateLicensedMA", nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateLicensedMA;
    @XmlElement(name = "DriverTraining", nillable = true)
    protected String driverTraining;
    @XmlElement(name = "DriverType", nillable = true)
    protected String driverType;
    @XmlElement(name = "DriversLicenseNumber", nillable = true)
    protected String driversLicenseNumber;
    @XmlElement(name = "GoodStudent", nillable = true)
    protected String goodStudent;
    @XmlElement(name = "StateLicensed", nillable = true)
    protected String stateLicensed;
    @XmlElement(name = "StudentOver100MilesFlag")
    protected Boolean studentOver100MilesFlag;

    /**
     * Gets the value of the accidentPreventionCourseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccidentPreventionCourseDate() {
        return accidentPreventionCourseDate;
    }

    /**
     * Sets the value of the accidentPreventionCourseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccidentPreventionCourseDate(XMLGregorianCalendar value) {
        this.accidentPreventionCourseDate = value;
    }

    /**
     * Gets the value of the commercialExperienceBeganDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommercialExperienceBeganDate() {
        return commercialExperienceBeganDate;
    }

    /**
     * Sets the value of the commercialExperienceBeganDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommercialExperienceBeganDate(XMLGregorianCalendar value) {
        this.commercialExperienceBeganDate = value;
    }

    /**
     * Gets the value of the dateLicensed property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateLicensed() {
        return dateLicensed;
    }

    /**
     * Sets the value of the dateLicensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateLicensed(XMLGregorianCalendar value) {
        this.dateLicensed = value;
    }

    /**
     * Gets the value of the dateLicensedMA property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateLicensedMA() {
        return dateLicensedMA;
    }

    /**
     * Sets the value of the dateLicensedMA property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateLicensedMA(XMLGregorianCalendar value) {
        this.dateLicensedMA = value;
    }

    /**
     * Gets the value of the driverTraining property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverTraining() {
        return driverTraining;
    }

    /**
     * Sets the value of the driverTraining property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverTraining(String value) {
        this.driverTraining = value;
    }

    /**
     * Gets the value of the driverType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverType() {
        return driverType;
    }

    /**
     * Sets the value of the driverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverType(String value) {
        this.driverType = value;
    }

    /**
     * Gets the value of the driversLicenseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriversLicenseNumber() {
        return driversLicenseNumber;
    }

    /**
     * Sets the value of the driversLicenseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriversLicenseNumber(String value) {
        this.driversLicenseNumber = value;
    }

    /**
     * Gets the value of the goodStudent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoodStudent() {
        return goodStudent;
    }

    /**
     * Sets the value of the goodStudent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoodStudent(String value) {
        this.goodStudent = value;
    }

    /**
     * Gets the value of the stateLicensed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateLicensed() {
        return stateLicensed;
    }

    /**
     * Sets the value of the stateLicensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateLicensed(String value) {
        this.stateLicensed = value;
    }

    /**
     * Gets the value of the studentOver100MilesFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStudentOver100MilesFlag() {
        return studentOver100MilesFlag;
    }

    /**
     * Sets the value of the studentOver100MilesFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStudentOver100MilesFlag(Boolean value) {
        this.studentOver100MilesFlag = value;
    }

}
