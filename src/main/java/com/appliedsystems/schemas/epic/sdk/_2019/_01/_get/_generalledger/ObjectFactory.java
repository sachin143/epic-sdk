
package com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BankGetResult_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/_generalledger/", "BankGetResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BankGetResult }
     * 
     */
    public BankGetResult createBankGetResult() {
        return new BankGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankGetResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/_generalledger/", name = "BankGetResult")
    public JAXBElement<BankGetResult> createBankGetResult(BankGetResult value) {
        return new JAXBElement<BankGetResult>(_BankGetResult_QNAME, BankGetResult.class, null, value);
    }

}
