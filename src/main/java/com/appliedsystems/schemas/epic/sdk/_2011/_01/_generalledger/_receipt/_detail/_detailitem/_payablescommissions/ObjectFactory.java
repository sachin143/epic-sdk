
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CashOnAccountItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "CashOnAccountItems");
    private final static QName _CashOnAccountItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "CashOnAccountItem");
    private final static QName _PayStatementItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "PayStatementItem");
    private final static QName _PayItem_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "PayItem");
    private final static QName _PayStatementItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "PayStatementItems");
    private final static QName _PayItems_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", "PayItems");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CashOnAccountItem }
     * 
     */
    public CashOnAccountItem createCashOnAccountItem() {
        return new CashOnAccountItem();
    }

    /**
     * Create an instance of {@link CashOnAccountItems }
     * 
     */
    public CashOnAccountItems createCashOnAccountItems() {
        return new CashOnAccountItems();
    }

    /**
     * Create an instance of {@link PayItems }
     * 
     */
    public PayItems createPayItems() {
        return new PayItems();
    }

    /**
     * Create an instance of {@link PayStatementItems }
     * 
     */
    public PayStatementItems createPayStatementItems() {
        return new PayStatementItems();
    }

    /**
     * Create an instance of {@link PayItem }
     * 
     */
    public PayItem createPayItem() {
        return new PayItem();
    }

    /**
     * Create an instance of {@link PayStatementItem }
     * 
     */
    public PayStatementItem createPayStatementItem() {
        return new PayStatementItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashOnAccountItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "CashOnAccountItems")
    public JAXBElement<CashOnAccountItems> createCashOnAccountItems(CashOnAccountItems value) {
        return new JAXBElement<CashOnAccountItems>(_CashOnAccountItems_QNAME, CashOnAccountItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashOnAccountItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "CashOnAccountItem")
    public JAXBElement<CashOnAccountItem> createCashOnAccountItem(CashOnAccountItem value) {
        return new JAXBElement<CashOnAccountItem>(_CashOnAccountItem_QNAME, CashOnAccountItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayStatementItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "PayStatementItem")
    public JAXBElement<PayStatementItem> createPayStatementItem(PayStatementItem value) {
        return new JAXBElement<PayStatementItem>(_PayStatementItem_QNAME, PayStatementItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "PayItem")
    public JAXBElement<PayItem> createPayItem(PayItem value) {
        return new JAXBElement<PayItem>(_PayItem_QNAME, PayItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayStatementItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "PayStatementItems")
    public JAXBElement<PayStatementItems> createPayStatementItems(PayStatementItems value) {
        return new JAXBElement<PayStatementItems>(_PayStatementItems_QNAME, PayStatementItems.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayItems }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/_detailitem/_payablescommissions/", name = "PayItems")
    public JAXBElement<PayItems> createPayItems(PayItems value) {
        return new JAXBElement<PayItems>(_PayItems_QNAME, PayItems.class, null, value);
    }

}
