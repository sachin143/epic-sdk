
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PaidItemsReturnPremium_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", "PaidItemsReturnPremium");
    private final static QName _PaidItemsReturnCommission_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", "PaidItemsReturnCommission");
    private final static QName _PaidItemsAdvancePremium_QNAME = new QName("http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", "PaidItemsAdvancePremium");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaidItemsReturnPremium }
     * 
     */
    public PaidItemsReturnPremium createPaidItemsReturnPremium() {
        return new PaidItemsReturnPremium();
    }

    /**
     * Create an instance of {@link PaidItemsReturnCommission }
     * 
     */
    public PaidItemsReturnCommission createPaidItemsReturnCommission() {
        return new PaidItemsReturnCommission();
    }

    /**
     * Create an instance of {@link PaidItemsAdvancePremium }
     * 
     */
    public PaidItemsAdvancePremium createPaidItemsAdvancePremium() {
        return new PaidItemsAdvancePremium();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidItemsReturnPremium }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", name = "PaidItemsReturnPremium")
    public JAXBElement<PaidItemsReturnPremium> createPaidItemsReturnPremium(PaidItemsReturnPremium value) {
        return new JAXBElement<PaidItemsReturnPremium>(_PaidItemsReturnPremium_QNAME, PaidItemsReturnPremium.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidItemsReturnCommission }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", name = "PaidItemsReturnCommission")
    public JAXBElement<PaidItemsReturnCommission> createPaidItemsReturnCommission(PaidItemsReturnCommission value) {
        return new JAXBElement<PaidItemsReturnCommission>(_PaidItemsReturnCommission_QNAME, PaidItemsReturnCommission.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaidItemsAdvancePremium }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_detail/_detailitem/_paiditems/", name = "PaidItemsAdvancePremium")
    public JAXBElement<PaidItemsAdvancePremium> createPaidItemsAdvancePremium(PaidItemsAdvancePremium value) {
        return new JAXBElement<PaidItemsAdvancePremium>(_PaidItemsAdvancePremium_QNAME, PaidItemsAdvancePremium.class, null, value);
    }

}
