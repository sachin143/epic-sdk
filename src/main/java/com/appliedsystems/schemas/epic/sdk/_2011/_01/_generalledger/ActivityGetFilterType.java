
package com.appliedsystems.schemas.epic.sdk._2011._01._generalledger;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivityGetFilterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivityGetFilterType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="ClosedBy"/>
 *     &lt;enumeration value="ClosedOn"/>
 *     &lt;enumeration value="Code"/>
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="EndDate"/>
 *     &lt;enumeration value="EnteredBy"/>
 *     &lt;enumeration value="EnteredOn"/>
 *     &lt;enumeration value="FollowUpStart"/>
 *     &lt;enumeration value="WhoOwner"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActivityGetFilterType")
@XmlEnum
public enum ActivityGetFilterType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("ClosedBy")
    CLOSED_BY("ClosedBy"),
    @XmlEnumValue("ClosedOn")
    CLOSED_ON("ClosedOn"),
    @XmlEnumValue("Code")
    CODE("Code"),
    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("EndDate")
    END_DATE("EndDate"),
    @XmlEnumValue("EnteredBy")
    ENTERED_BY("EnteredBy"),
    @XmlEnumValue("EnteredOn")
    ENTERED_ON("EnteredOn"),
    @XmlEnumValue("FollowUpStart")
    FOLLOW_UP_START("FollowUpStart"),
    @XmlEnumValue("WhoOwner")
    WHO_OWNER("WhoOwner");
    private final String value;

    ActivityGetFilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActivityGetFilterType fromValue(String v) {
        for (ActivityGetFilterType c: ActivityGetFilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
