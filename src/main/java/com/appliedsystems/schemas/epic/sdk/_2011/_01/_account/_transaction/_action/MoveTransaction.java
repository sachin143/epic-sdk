
package com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoveTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoveTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssociatedAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AssociatedAccountTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IgnoreDifferentDueMonths" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MoveNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MoveToNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MoveToTransactionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_moveto/}TransactionItems" minOccurs="0"/>
 *         &lt;element name="MoveToType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MoveTransactionItems" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/_movetransaction/_move/}TransactionItems" minOccurs="0"/>
 *         &lt;element name="MoveType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoveTransaction", propOrder = {
    "associatedAccountID",
    "associatedAccountTypeCode",
    "ignoreDifferentDueMonths",
    "moveNumber",
    "moveToNumber",
    "moveToTransactionItems",
    "moveToType",
    "moveTransactionItems",
    "moveType"
})
public class MoveTransaction {

    @XmlElement(name = "AssociatedAccountID")
    protected Integer associatedAccountID;
    @XmlElement(name = "AssociatedAccountTypeCode", nillable = true)
    protected String associatedAccountTypeCode;
    @XmlElement(name = "IgnoreDifferentDueMonths")
    protected Boolean ignoreDifferentDueMonths;
    @XmlElement(name = "MoveNumber")
    protected Integer moveNumber;
    @XmlElement(name = "MoveToNumber", nillable = true)
    protected Integer moveToNumber;
    @XmlElement(name = "MoveToTransactionItems", nillable = true)
    protected com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.TransactionItems moveToTransactionItems;
    @XmlElement(name = "MoveToType", nillable = true)
    protected String moveToType;
    @XmlElement(name = "MoveTransactionItems", nillable = true)
    protected com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.TransactionItems moveTransactionItems;
    @XmlElement(name = "MoveType", nillable = true)
    protected String moveType;

    /**
     * Gets the value of the associatedAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAssociatedAccountID() {
        return associatedAccountID;
    }

    /**
     * Sets the value of the associatedAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAssociatedAccountID(Integer value) {
        this.associatedAccountID = value;
    }

    /**
     * Gets the value of the associatedAccountTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedAccountTypeCode() {
        return associatedAccountTypeCode;
    }

    /**
     * Sets the value of the associatedAccountTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedAccountTypeCode(String value) {
        this.associatedAccountTypeCode = value;
    }

    /**
     * Gets the value of the ignoreDifferentDueMonths property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreDifferentDueMonths() {
        return ignoreDifferentDueMonths;
    }

    /**
     * Sets the value of the ignoreDifferentDueMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreDifferentDueMonths(Boolean value) {
        this.ignoreDifferentDueMonths = value;
    }

    /**
     * Gets the value of the moveNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMoveNumber() {
        return moveNumber;
    }

    /**
     * Sets the value of the moveNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMoveNumber(Integer value) {
        this.moveNumber = value;
    }

    /**
     * Gets the value of the moveToNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMoveToNumber() {
        return moveToNumber;
    }

    /**
     * Sets the value of the moveToNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMoveToNumber(Integer value) {
        this.moveToNumber = value;
    }

    /**
     * Gets the value of the moveToTransactionItems property.
     * 
     * @return
     *     possible object is
     *     {@link com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.TransactionItems }
     *     
     */
    public com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.TransactionItems getMoveToTransactionItems() {
        return moveToTransactionItems;
    }

    /**
     * Sets the value of the moveToTransactionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.TransactionItems }
     *     
     */
    public void setMoveToTransactionItems(com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.TransactionItems value) {
        this.moveToTransactionItems = value;
    }

    /**
     * Gets the value of the moveToType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoveToType() {
        return moveToType;
    }

    /**
     * Sets the value of the moveToType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoveToType(String value) {
        this.moveToType = value;
    }

    /**
     * Gets the value of the moveTransactionItems property.
     * 
     * @return
     *     possible object is
     *     {@link com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.TransactionItems }
     *     
     */
    public com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.TransactionItems getMoveTransactionItems() {
        return moveTransactionItems;
    }

    /**
     * Sets the value of the moveTransactionItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.TransactionItems }
     *     
     */
    public void setMoveTransactionItems(com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.TransactionItems value) {
        this.moveTransactionItems = value;
    }

    /**
     * Gets the value of the moveType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoveType() {
        return moveType;
    }

    /**
     * Sets the value of the moveType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoveType(String value) {
        this.moveType = value;
    }

}
