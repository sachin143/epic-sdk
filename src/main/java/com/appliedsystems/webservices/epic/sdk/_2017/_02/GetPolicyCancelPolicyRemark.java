
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CancelPolicyRemarksIDType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/}CancelPolicyRemarksGetType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "cancelPolicyRemarksIDType"
})
@XmlRootElement(name = "Get_Policy_CancelPolicyRemark")
public class GetPolicyCancelPolicyRemark {

    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "CancelPolicyRemarksIDType")
    @XmlSchemaType(name = "string")
    protected CancelPolicyRemarksGetType cancelPolicyRemarksIDType;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the cancelPolicyRemarksIDType property.
     * 
     * @return
     *     possible object is
     *     {@link CancelPolicyRemarksGetType }
     *     
     */
    public CancelPolicyRemarksGetType getCancelPolicyRemarksIDType() {
        return cancelPolicyRemarksIDType;
    }

    /**
     * Sets the value of the cancelPolicyRemarksIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelPolicyRemarksGetType }
     *     
     */
    public void setCancelPolicyRemarksIDType(CancelPolicyRemarksGetType value) {
        this.cancelPolicyRemarksIDType = value;
    }

}
