
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReconciliationBankObject" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_generalledger/_reconciliation/}Bank" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reconciliationBankObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_ReconciliationBank")
public class InsertGeneralLedgerReconciliationBank {

    @XmlElement(name = "ReconciliationBankObject", nillable = true)
    protected Bank reconciliationBankObject;

    /**
     * Gets the value of the reconciliationBankObject property.
     * 
     * @return
     *     possible object is
     *     {@link Bank }
     *     
     */
    public Bank getReconciliationBankObject() {
        return reconciliationBankObject;
    }

    /**
     * Sets the value of the reconciliationBankObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Bank }
     *     
     */
    public void setReconciliationBankObject(Bank value) {
        this.reconciliationBankObject = value;
    }

}
