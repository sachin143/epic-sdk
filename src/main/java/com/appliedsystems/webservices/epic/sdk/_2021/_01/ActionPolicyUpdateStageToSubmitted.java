
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="oSubmissionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/}UpdateStageToSubmitted" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "oSubmissionObject"
})
@XmlRootElement(name = "Action_Policy_UpdateStageToSubmitted")
public class ActionPolicyUpdateStageToSubmitted {

    @XmlElement(nillable = true)
    protected UpdateStageToSubmitted oSubmissionObject;

    /**
     * Gets the value of the oSubmissionObject property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateStageToSubmitted }
     *     
     */
    public UpdateStageToSubmitted getOSubmissionObject() {
        return oSubmissionObject;
    }

    /**
     * Sets the value of the oSubmissionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateStageToSubmitted }
     *     
     */
    public void setOSubmissionObject(UpdateStageToSubmitted value) {
        this.oSubmissionObject = value;
    }

}
