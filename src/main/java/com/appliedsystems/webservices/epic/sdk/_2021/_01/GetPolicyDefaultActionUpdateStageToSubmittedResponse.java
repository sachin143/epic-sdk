
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_DefaultActionUpdateStageToSubmittedResult" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_account/_policy/_action/}ArrayOfUpdateStageToSubmitted" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyDefaultActionUpdateStageToSubmittedResult"
})
@XmlRootElement(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse")
public class GetPolicyDefaultActionUpdateStageToSubmittedResponse {

    @XmlElement(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", nillable = true)
    protected ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmittedResult;

    /**
     * Gets the value of the getPolicyDefaultActionUpdateStageToSubmittedResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUpdateStageToSubmitted }
     *     
     */
    public ArrayOfUpdateStageToSubmitted getGetPolicyDefaultActionUpdateStageToSubmittedResult() {
        return getPolicyDefaultActionUpdateStageToSubmittedResult;
    }

    /**
     * Sets the value of the getPolicyDefaultActionUpdateStageToSubmittedResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUpdateStageToSubmitted }
     *     
     */
    public void setGetPolicyDefaultActionUpdateStageToSubmittedResult(ArrayOfUpdateStageToSubmitted value) {
        this.getPolicyDefaultActionUpdateStageToSubmittedResult = value;
    }

}
