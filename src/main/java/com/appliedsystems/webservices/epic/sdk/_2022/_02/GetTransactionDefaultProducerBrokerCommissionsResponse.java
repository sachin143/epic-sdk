
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultProducerBrokerCommissionsResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}TransactionGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultProducerBrokerCommissionsResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultProducerBrokerCommissionsResponse")
public class GetTransactionDefaultProducerBrokerCommissionsResponse {

    @XmlElement(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", nillable = true)
    protected TransactionGetResult getTransactionDefaultProducerBrokerCommissionsResult;

    /**
     * Gets the value of the getTransactionDefaultProducerBrokerCommissionsResult property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionGetResult }
     *     
     */
    public TransactionGetResult getGetTransactionDefaultProducerBrokerCommissionsResult() {
        return getTransactionDefaultProducerBrokerCommissionsResult;
    }

    /**
     * Sets the value of the getTransactionDefaultProducerBrokerCommissionsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionGetResult }
     *     
     */
    public void setGetTransactionDefaultProducerBrokerCommissionsResult(TransactionGetResult value) {
        this.getTransactionDefaultProducerBrokerCommissionsResult = value;
    }

}
