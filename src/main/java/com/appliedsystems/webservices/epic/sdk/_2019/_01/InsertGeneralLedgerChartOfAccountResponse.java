
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_GeneralLedger_ChartOfAccountResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertGeneralLedgerChartOfAccountResult"
})
@XmlRootElement(name = "Insert_GeneralLedger_ChartOfAccountResponse")
public class InsertGeneralLedgerChartOfAccountResponse {

    @XmlElement(name = "Insert_GeneralLedger_ChartOfAccountResult")
    protected Integer insertGeneralLedgerChartOfAccountResult;

    /**
     * Gets the value of the insertGeneralLedgerChartOfAccountResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertGeneralLedgerChartOfAccountResult() {
        return insertGeneralLedgerChartOfAccountResult;
    }

    /**
     * Sets the value of the insertGeneralLedgerChartOfAccountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertGeneralLedgerChartOfAccountResult(Integer value) {
        this.insertGeneralLedgerChartOfAccountResult = value;
    }

}
