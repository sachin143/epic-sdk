
package com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FileDataStream" type="{http://schemas.microsoft.com/Message}StreamBody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fileDataStream"
})
@XmlRootElement(name = "Upload_Attachment_File")
public class UploadAttachmentFile {

    @XmlElement(name = "FileDataStream", required = true)
    protected byte[] fileDataStream;

    /**
     * Gets the value of the fileDataStream property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFileDataStream() {
        return fileDataStream;
    }

    /**
     * Sets the value of the fileDataStream property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFileDataStream(byte[] value) {
        this.fileDataStream = value;
    }

}
