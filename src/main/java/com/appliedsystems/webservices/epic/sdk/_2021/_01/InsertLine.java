
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LineObject" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/}Line" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lineObject"
})
@XmlRootElement(name = "Insert_Line")
public class InsertLine {

    @XmlElement(name = "LineObject", nillable = true)
    protected Line lineObject;

    /**
     * Gets the value of the lineObject property.
     * 
     * @return
     *     possible object is
     *     {@link Line }
     *     
     */
    public Line getLineObject() {
        return lineObject;
    }

    /**
     * Sets the value of the lineObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Line }
     *     
     */
    public void setLineObject(Line value) {
        this.lineObject = value;
    }

}
