
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/}Transaction" minOccurs="0"/>
 *         &lt;element name="InstallmentType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/}TransactionGetInstallmentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionObject",
    "installmentType"
})
@XmlRootElement(name = "Get_Transaction_DefaultInstallments")
public class GetTransactionDefaultInstallments {

    @XmlElement(name = "TransactionObject", nillable = true)
    protected Transaction transactionObject;
    @XmlElement(name = "InstallmentType")
    @XmlSchemaType(name = "string")
    protected TransactionGetInstallmentType installmentType;

    /**
     * Gets the value of the transactionObject property.
     * 
     * @return
     *     possible object is
     *     {@link Transaction }
     *     
     */
    public Transaction getTransactionObject() {
        return transactionObject;
    }

    /**
     * Sets the value of the transactionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction }
     *     
     */
    public void setTransactionObject(Transaction value) {
        this.transactionObject = value;
    }

    /**
     * Gets the value of the installmentType property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionGetInstallmentType }
     *     
     */
    public TransactionGetInstallmentType getInstallmentType() {
        return installmentType;
    }

    /**
     * Sets the value of the installmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionGetInstallmentType }
     *     
     */
    public void setInstallmentType(TransactionGetInstallmentType value) {
        this.installmentType = value;
    }

}
