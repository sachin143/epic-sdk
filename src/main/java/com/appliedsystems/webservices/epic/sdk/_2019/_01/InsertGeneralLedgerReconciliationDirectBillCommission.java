
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReconciliationDirectBillCommissionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_reconciliation/}DirectBillCommission" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reconciliationDirectBillCommissionObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_ReconciliationDirectBillCommission")
public class InsertGeneralLedgerReconciliationDirectBillCommission {

    @XmlElement(name = "ReconciliationDirectBillCommissionObject", nillable = true)
    protected DirectBillCommission reconciliationDirectBillCommissionObject;

    /**
     * Gets the value of the reconciliationDirectBillCommissionObject property.
     * 
     * @return
     *     possible object is
     *     {@link DirectBillCommission }
     *     
     */
    public DirectBillCommission getReconciliationDirectBillCommissionObject() {
        return reconciliationDirectBillCommissionObject;
    }

    /**
     * Sets the value of the reconciliationDirectBillCommissionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectBillCommission }
     *     
     */
    public void setReconciliationDirectBillCommissionObject(DirectBillCommission value) {
        this.reconciliationDirectBillCommissionObject = value;
    }

}
