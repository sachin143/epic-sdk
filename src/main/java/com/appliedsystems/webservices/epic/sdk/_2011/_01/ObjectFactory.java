
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2011._01 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2011._01
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherTransferOfFunds }
     * 
     */
    public ActionGeneralLedgerVoucherTransferOfFunds createActionGeneralLedgerVoucherTransferOfFunds() {
        return new ActionGeneralLedgerVoucherTransferOfFunds();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerVoucherResponse }
     * 
     */
    public InsertGeneralLedgerVoucherResponse createInsertGeneralLedgerVoucherResponse() {
        return new InsertGeneralLedgerVoucherResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementVoid }
     * 
     */
    public ActionGeneralLedgerDisbursementVoid createActionGeneralLedgerDisbursementVoid() {
        return new ActionGeneralLedgerDisbursementVoid();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherVoid }
     * 
     */
    public ActionGeneralLedgerVoucherVoid createActionGeneralLedgerVoucherVoid() {
        return new ActionGeneralLedgerVoucherVoid();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursement }
     * 
     */
    public GetGeneralLedgerDisbursement createGetGeneralLedgerDisbursement() {
        return new GetGeneralLedgerDisbursement();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucherResponse }
     * 
     */
    public GetGeneralLedgerVoucherResponse createGetGeneralLedgerVoucherResponse() {
        return new GetGeneralLedgerVoucherResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursementDefaultDefaultEntryResponse }
     * 
     */
    public GetGeneralLedgerDisbursementDefaultDefaultEntryResponse createGetGeneralLedgerDisbursementDefaultDefaultEntryResponse() {
        return new GetGeneralLedgerDisbursementDefaultDefaultEntryResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursementDefaultActionPayVouchers }
     * 
     */
    public GetGeneralLedgerDisbursementDefaultActionPayVouchers createGetGeneralLedgerDisbursementDefaultActionPayVouchers() {
        return new GetGeneralLedgerDisbursementDefaultActionPayVouchers();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerVoucher }
     * 
     */
    public UpdateGeneralLedgerVoucher createUpdateGeneralLedgerVoucher() {
        return new UpdateGeneralLedgerVoucher();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherPayVouchers }
     * 
     */
    public ActionGeneralLedgerVoucherPayVouchers createActionGeneralLedgerVoucherPayVouchers() {
        return new ActionGeneralLedgerVoucherPayVouchers();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucherDefaultActionPayVouchers }
     * 
     */
    public GetGeneralLedgerVoucherDefaultActionPayVouchers createGetGeneralLedgerVoucherDefaultActionPayVouchers() {
        return new GetGeneralLedgerVoucherDefaultActionPayVouchers();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucherDefaultDefaultEntryResponse }
     * 
     */
    public GetGeneralLedgerVoucherDefaultDefaultEntryResponse createGetGeneralLedgerVoucherDefaultDefaultEntryResponse() {
        return new GetGeneralLedgerVoucherDefaultDefaultEntryResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherVoidResponse }
     * 
     */
    public ActionGeneralLedgerVoucherVoidResponse createActionGeneralLedgerVoucherVoidResponse() {
        return new ActionGeneralLedgerVoucherVoidResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucher }
     * 
     */
    public GetGeneralLedgerVoucher createGetGeneralLedgerVoucher() {
        return new GetGeneralLedgerVoucher();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherPayVouchersResponse }
     * 
     */
    public ActionGeneralLedgerVoucherPayVouchersResponse createActionGeneralLedgerVoucherPayVouchersResponse() {
        return new ActionGeneralLedgerVoucherPayVouchersResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerVoucherTransferOfFundsResponse }
     * 
     */
    public ActionGeneralLedgerVoucherTransferOfFundsResponse createActionGeneralLedgerVoucherTransferOfFundsResponse() {
        return new ActionGeneralLedgerVoucherTransferOfFundsResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursementResponse }
     * 
     */
    public GetGeneralLedgerDisbursementResponse createGetGeneralLedgerDisbursementResponse() {
        return new GetGeneralLedgerDisbursementResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerVoucherResponse }
     * 
     */
    public UpdateGeneralLedgerVoucherResponse createUpdateGeneralLedgerVoucherResponse() {
        return new UpdateGeneralLedgerVoucherResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementPayVouchersResponse }
     * 
     */
    public ActionGeneralLedgerDisbursementPayVouchersResponse createActionGeneralLedgerDisbursementPayVouchersResponse() {
        return new ActionGeneralLedgerDisbursementPayVouchersResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerDisbursementResponse }
     * 
     */
    public UpdateGeneralLedgerDisbursementResponse createUpdateGeneralLedgerDisbursementResponse() {
        return new UpdateGeneralLedgerDisbursementResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucherDefaultDefaultEntry }
     * 
     */
    public GetGeneralLedgerVoucherDefaultDefaultEntry createGetGeneralLedgerVoucherDefaultDefaultEntry() {
        return new GetGeneralLedgerVoucherDefaultDefaultEntry();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementTransferOfFundsResponse }
     * 
     */
    public ActionGeneralLedgerDisbursementTransferOfFundsResponse createActionGeneralLedgerDisbursementTransferOfFundsResponse() {
        return new ActionGeneralLedgerDisbursementTransferOfFundsResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursementDefaultDefaultEntry }
     * 
     */
    public GetGeneralLedgerDisbursementDefaultDefaultEntry createGetGeneralLedgerDisbursementDefaultDefaultEntry() {
        return new GetGeneralLedgerDisbursementDefaultDefaultEntry();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerVoucherDefaultActionPayVouchersResponse }
     * 
     */
    public GetGeneralLedgerVoucherDefaultActionPayVouchersResponse createGetGeneralLedgerVoucherDefaultActionPayVouchersResponse() {
        return new GetGeneralLedgerVoucherDefaultActionPayVouchersResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementVoidResponse }
     * 
     */
    public ActionGeneralLedgerDisbursementVoidResponse createActionGeneralLedgerDisbursementVoidResponse() {
        return new ActionGeneralLedgerDisbursementVoidResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerDisbursement }
     * 
     */
    public UpdateGeneralLedgerDisbursement createUpdateGeneralLedgerDisbursement() {
        return new UpdateGeneralLedgerDisbursement();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse }
     * 
     */
    public GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse createGetGeneralLedgerDisbursementDefaultActionPayVouchersResponse() {
        return new GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementTransferOfFunds }
     * 
     */
    public ActionGeneralLedgerDisbursementTransferOfFunds createActionGeneralLedgerDisbursementTransferOfFunds() {
        return new ActionGeneralLedgerDisbursementTransferOfFunds();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerDisbursementResponse }
     * 
     */
    public InsertGeneralLedgerDisbursementResponse createInsertGeneralLedgerDisbursementResponse() {
        return new InsertGeneralLedgerDisbursementResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerDisbursementPayVouchers }
     * 
     */
    public ActionGeneralLedgerDisbursementPayVouchers createActionGeneralLedgerDisbursementPayVouchers() {
        return new ActionGeneralLedgerDisbursementPayVouchers();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerDisbursement }
     * 
     */
    public InsertGeneralLedgerDisbursement createInsertGeneralLedgerDisbursement() {
        return new InsertGeneralLedgerDisbursement();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerVoucher }
     * 
     */
    public InsertGeneralLedgerVoucher createInsertGeneralLedgerVoucher() {
        return new InsertGeneralLedgerVoucher();
    }

}
