
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_ContactResult" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/}ContactGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getContactResult"
})
@XmlRootElement(name = "Get_ContactResponse")
public class GetContactResponse {

    @XmlElement(name = "Get_ContactResult", nillable = true)
    protected ContactGetResult getContactResult;

    /**
     * Gets the value of the getContactResult property.
     * 
     * @return
     *     possible object is
     *     {@link ContactGetResult }
     *     
     */
    public ContactGetResult getGetContactResult() {
        return getContactResult;
    }

    /**
     * Sets the value of the getContactResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactGetResult }
     *     
     */
    public void setGetContactResult(ContactGetResult value) {
        this.getContactResult = value;
    }

}
