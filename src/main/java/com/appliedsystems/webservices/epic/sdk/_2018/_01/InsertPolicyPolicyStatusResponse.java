
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Policy_PolicyStatusResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertPolicyPolicyStatusResult"
})
@XmlRootElement(name = "Insert_Policy_PolicyStatusResponse")
public class InsertPolicyPolicyStatusResponse {

    @XmlElement(name = "Insert_Policy_PolicyStatusResult")
    protected Integer insertPolicyPolicyStatusResult;

    /**
     * Gets the value of the insertPolicyPolicyStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertPolicyPolicyStatusResult() {
        return insertPolicyPolicyStatusResult;
    }

    /**
     * Sets the value of the insertPolicyPolicyStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertPolicyPolicyStatusResult(Integer value) {
        this.insertPolicyPolicyStatusResult = value;
    }

}
