
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JournalEntryVoidObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_journalentry/_action/}JournalEntryVoid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "journalEntryVoidObject"
})
@XmlRootElement(name = "Action_GeneralLedger_JournalEntryVoid")
public class ActionGeneralLedgerJournalEntryVoid {

    @XmlElement(name = "JournalEntryVoidObject", nillable = true)
    protected JournalEntryVoid journalEntryVoidObject;

    /**
     * Gets the value of the journalEntryVoidObject property.
     * 
     * @return
     *     possible object is
     *     {@link JournalEntryVoid }
     *     
     */
    public JournalEntryVoid getJournalEntryVoidObject() {
        return journalEntryVoidObject;
    }

    /**
     * Sets the value of the journalEntryVoidObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link JournalEntryVoid }
     *     
     */
    public void setJournalEntryVoidObject(JournalEntryVoid value) {
        this.journalEntryVoidObject = value;
    }

}
