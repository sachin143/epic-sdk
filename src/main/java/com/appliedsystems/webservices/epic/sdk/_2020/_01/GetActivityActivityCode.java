
package com.appliedsystems.webservices.epic.sdk._2020._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2020/01/_configure/_activity/}ActivityCodeGetType" minOccurs="0"/>
 *         &lt;element name="IncludeInactive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryValue",
    "searchType",
    "includeInactive"
})
@XmlRootElement(name = "Get_Activity_ActivityCode")
public class GetActivityActivityCode {

    @XmlElement(name = "QueryValue", nillable = true)
    protected String queryValue;
    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected ActivityCodeGetType searchType;
    @XmlElement(name = "IncludeInactive")
    protected Boolean includeInactive;

    /**
     * Gets the value of the queryValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryValue() {
        return queryValue;
    }

    /**
     * Sets the value of the queryValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryValue(String value) {
        this.queryValue = value;
    }

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityCodeGetType }
     *     
     */
    public ActivityCodeGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityCodeGetType }
     *     
     */
    public void setSearchType(ActivityCodeGetType value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the includeInactive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeInactive() {
        return includeInactive;
    }

    /**
     * Sets the value of the includeInactive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeInactive(Boolean value) {
        this.includeInactive = value;
    }

}
