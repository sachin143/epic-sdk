
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdjustorObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}Adjustor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "adjustorObject"
})
@XmlRootElement(name = "Update_Claim_Adjustor")
public class UpdateClaimAdjustor {

    @XmlElement(name = "AdjustorObject", nillable = true)
    protected Adjustor adjustorObject;

    /**
     * Gets the value of the adjustorObject property.
     * 
     * @return
     *     possible object is
     *     {@link Adjustor }
     *     
     */
    public Adjustor getAdjustorObject() {
        return adjustorObject;
    }

    /**
     * Sets the value of the adjustorObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Adjustor }
     *     
     */
    public void setAdjustorObject(Adjustor value) {
        this.adjustorObject = value;
    }

}
