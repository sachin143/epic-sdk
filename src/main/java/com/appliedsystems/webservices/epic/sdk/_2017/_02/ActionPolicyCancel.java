
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancelObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}Cancel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cancelObject"
})
@XmlRootElement(name = "Action_Policy_Cancel")
public class ActionPolicyCancel {

    @XmlElement(name = "CancelObject", nillable = true)
    protected Cancel cancelObject;

    /**
     * Gets the value of the cancelObject property.
     * 
     * @return
     *     possible object is
     *     {@link Cancel }
     *     
     */
    public Cancel getCancelObject() {
        return cancelObject;
    }

    /**
     * Sets the value of the cancelObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cancel }
     *     
     */
    public void setCancelObject(Cancel value) {
        this.cancelObject = value;
    }

}
