
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}VoucherGetType" minOccurs="0"/>
 *         &lt;element name="VoucherID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GetLimitType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}VoucherGetLimitType" minOccurs="0"/>
 *         &lt;element name="FilterType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}VoucherFilterType" minOccurs="0"/>
 *         &lt;element name="FilterField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}VoucherComparisonType" minOccurs="0"/>
 *         &lt;element name="FilterField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchType",
    "voucherID",
    "getLimitType",
    "filterType",
    "filterField1",
    "comparisonType",
    "filterField2",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_Voucher")
public class GetGeneralLedgerVoucher {

    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected VoucherGetType searchType;
    @XmlElement(name = "VoucherID")
    protected Integer voucherID;
    @XmlElement(name = "GetLimitType")
    @XmlSchemaType(name = "string")
    protected VoucherGetLimitType getLimitType;
    @XmlElement(name = "FilterType")
    @XmlSchemaType(name = "string")
    protected VoucherFilterType filterType;
    @XmlElement(name = "FilterField1", nillable = true)
    protected String filterField1;
    @XmlElement(name = "ComparisonType")
    @XmlSchemaType(name = "string")
    protected VoucherComparisonType comparisonType;
    @XmlElement(name = "FilterField2", nillable = true)
    protected String filterField2;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherGetType }
     *     
     */
    public VoucherGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherGetType }
     *     
     */
    public void setSearchType(VoucherGetType value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the voucherID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherID() {
        return voucherID;
    }

    /**
     * Sets the value of the voucherID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherID(Integer value) {
        this.voucherID = value;
    }

    /**
     * Gets the value of the getLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherGetLimitType }
     *     
     */
    public VoucherGetLimitType getGetLimitType() {
        return getLimitType;
    }

    /**
     * Sets the value of the getLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherGetLimitType }
     *     
     */
    public void setGetLimitType(VoucherGetLimitType value) {
        this.getLimitType = value;
    }

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherFilterType }
     *     
     */
    public VoucherFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherFilterType }
     *     
     */
    public void setFilterType(VoucherFilterType value) {
        this.filterType = value;
    }

    /**
     * Gets the value of the filterField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField1() {
        return filterField1;
    }

    /**
     * Sets the value of the filterField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField1(String value) {
        this.filterField1 = value;
    }

    /**
     * Gets the value of the comparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherComparisonType }
     *     
     */
    public VoucherComparisonType getComparisonType() {
        return comparisonType;
    }

    /**
     * Sets the value of the comparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherComparisonType }
     *     
     */
    public void setComparisonType(VoucherComparisonType value) {
        this.comparisonType = value;
    }

    /**
     * Gets the value of the filterField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField2() {
        return filterField2;
    }

    /**
     * Sets the value of the filterField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField2(String value) {
        this.filterField2 = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
