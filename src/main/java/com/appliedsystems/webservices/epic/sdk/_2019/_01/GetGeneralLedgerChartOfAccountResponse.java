
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ChartOfAccountResult" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/}ChartOfAccountGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerChartOfAccountResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ChartOfAccountResponse")
public class GetGeneralLedgerChartOfAccountResponse {

    @XmlElement(name = "Get_GeneralLedger_ChartOfAccountResult", nillable = true)
    protected ChartOfAccountGetResult getGeneralLedgerChartOfAccountResult;

    /**
     * Gets the value of the getGeneralLedgerChartOfAccountResult property.
     * 
     * @return
     *     possible object is
     *     {@link ChartOfAccountGetResult }
     *     
     */
    public ChartOfAccountGetResult getGetGeneralLedgerChartOfAccountResult() {
        return getGeneralLedgerChartOfAccountResult;
    }

    /**
     * Sets the value of the getGeneralLedgerChartOfAccountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChartOfAccountGetResult }
     *     
     */
    public void setGetGeneralLedgerChartOfAccountResult(ChartOfAccountGetResult value) {
        this.getGeneralLedgerChartOfAccountResult = value;
    }

}
