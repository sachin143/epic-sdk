
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Claim_PaymentExpenseResult" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/}ArrayOfPaymentExpense" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClaimPaymentExpenseResult"
})
@XmlRootElement(name = "Get_Claim_PaymentExpenseResponse")
public class GetClaimPaymentExpenseResponse {

    @XmlElement(name = "Get_Claim_PaymentExpenseResult", nillable = true)
    protected ArrayOfPaymentExpense getClaimPaymentExpenseResult;

    /**
     * Gets the value of the getClaimPaymentExpenseResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPaymentExpense }
     *     
     */
    public ArrayOfPaymentExpense getGetClaimPaymentExpenseResult() {
        return getClaimPaymentExpenseResult;
    }

    /**
     * Sets the value of the getClaimPaymentExpenseResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPaymentExpense }
     *     
     */
    public void setGetClaimPaymentExpenseResult(ArrayOfPaymentExpense value) {
        this.getClaimPaymentExpenseResult = value;
    }

}
