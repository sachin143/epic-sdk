
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.Discount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DiscountObject" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}Discount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "discountObject"
})
@XmlRootElement(name = "Action_Transaction_Discount")
public class ActionTransactionDiscount {

    @XmlElement(name = "DiscountObject", nillable = true)
    protected Discount discountObject;

    /**
     * Gets the value of the discountObject property.
     * 
     * @return
     *     possible object is
     *     {@link Discount }
     *     
     */
    public Discount getDiscountObject() {
        return discountObject;
    }

    /**
     * Sets the value of the discountObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Discount }
     *     
     */
    public void setDiscountObject(Discount value) {
        this.discountObject = value;
    }

}
