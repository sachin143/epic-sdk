
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionMoveTransactionResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}ArrayOfMoveTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionMoveTransactionResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionMoveTransactionResponse")
public class GetTransactionDefaultActionMoveTransactionResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionMoveTransactionResult", nillable = true)
    protected ArrayOfMoveTransaction getTransactionDefaultActionMoveTransactionResult;

    /**
     * Gets the value of the getTransactionDefaultActionMoveTransactionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMoveTransaction }
     *     
     */
    public ArrayOfMoveTransaction getGetTransactionDefaultActionMoveTransactionResult() {
        return getTransactionDefaultActionMoveTransactionResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionMoveTransactionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMoveTransaction }
     *     
     */
    public void setGetTransactionDefaultActionMoveTransactionResult(ArrayOfMoveTransaction value) {
        this.getTransactionDefaultActionMoveTransactionResult = value;
    }

}
