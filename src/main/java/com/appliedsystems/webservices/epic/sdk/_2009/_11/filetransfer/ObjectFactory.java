
package com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UploadAttachmentFile }
     * 
     */
    public UploadAttachmentFile createUploadAttachmentFile() {
        return new UploadAttachmentFile();
    }

    /**
     * Create an instance of {@link DownloadAddOnAttachmentFileResponse }
     * 
     */
    public DownloadAddOnAttachmentFileResponse createDownloadAddOnAttachmentFileResponse() {
        return new DownloadAddOnAttachmentFileResponse();
    }

    /**
     * Create an instance of {@link DownloadAttachmentFileResponse }
     * 
     */
    public DownloadAttachmentFileResponse createDownloadAttachmentFileResponse() {
        return new DownloadAttachmentFileResponse();
    }

    /**
     * Create an instance of {@link DownloadAttachmentFile }
     * 
     */
    public DownloadAttachmentFile createDownloadAttachmentFile() {
        return new DownloadAttachmentFile();
    }

    /**
     * Create an instance of {@link DownloadAddOnAttachmentFile }
     * 
     */
    public DownloadAddOnAttachmentFile createDownloadAddOnAttachmentFile() {
        return new DownloadAddOnAttachmentFile();
    }

    /**
     * Create an instance of {@link UploadAttachmentFileResponse }
     * 
     */
    public UploadAttachmentFileResponse createUploadAttachmentFileResponse() {
        return new UploadAttachmentFileResponse();
    }

}
