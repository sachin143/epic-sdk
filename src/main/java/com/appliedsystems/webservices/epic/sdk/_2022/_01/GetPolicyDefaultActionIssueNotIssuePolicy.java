
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ServiceSummaryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}IssueNotIssuePolicyGetType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyID",
    "serviceSummaryID",
    "searchType"
})
@XmlRootElement(name = "Get_Policy_DefaultActionIssueNotIssuePolicy")
public class GetPolicyDefaultActionIssueNotIssuePolicy {

    @XmlElement(name = "PolicyID")
    protected Integer policyID;
    @XmlElement(name = "ServiceSummaryID")
    protected Integer serviceSummaryID;
    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected IssueNotIssuePolicyGetType searchType;

    /**
     * Gets the value of the policyID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyID() {
        return policyID;
    }

    /**
     * Sets the value of the policyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyID(Integer value) {
        this.policyID = value;
    }

    /**
     * Gets the value of the serviceSummaryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceSummaryID() {
        return serviceSummaryID;
    }

    /**
     * Sets the value of the serviceSummaryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceSummaryID(Integer value) {
        this.serviceSummaryID = value;
    }

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link IssueNotIssuePolicyGetType }
     *     
     */
    public IssueNotIssuePolicyGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssueNotIssuePolicyGetType }
     *     
     */
    public void setSearchType(IssueNotIssuePolicyGetType value) {
        this.searchType = value;
    }

}
