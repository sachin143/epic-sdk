
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_DisbursementResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/}DisbursementGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerDisbursementResult"
})
@XmlRootElement(name = "Get_GeneralLedger_DisbursementResponse")
public class GetGeneralLedgerDisbursementResponse {

    @XmlElement(name = "Get_GeneralLedger_DisbursementResult", nillable = true)
    protected DisbursementGetResult getGeneralLedgerDisbursementResult;

    /**
     * Gets the value of the getGeneralLedgerDisbursementResult property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementGetResult }
     *     
     */
    public DisbursementGetResult getGetGeneralLedgerDisbursementResult() {
        return getGeneralLedgerDisbursementResult;
    }

    /**
     * Sets the value of the getGeneralLedgerDisbursementResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementGetResult }
     *     
     */
    public void setGetGeneralLedgerDisbursementResult(DisbursementGetResult value) {
        this.getGeneralLedgerDisbursementResult = value;
    }

}
