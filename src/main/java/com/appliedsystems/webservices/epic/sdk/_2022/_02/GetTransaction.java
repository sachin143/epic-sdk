
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/}TransactionFilter" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionFilterObject",
    "pageNumber"
})
@XmlRootElement(name = "Get_Transaction")
public class GetTransaction {

    @XmlElement(name = "TransactionFilterObject", nillable = true)
    protected TransactionFilter transactionFilterObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the transactionFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionFilter }
     *     
     */
    public TransactionFilter getTransactionFilterObject() {
        return transactionFilterObject;
    }

    /**
     * Sets the value of the transactionFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionFilter }
     *     
     */
    public void setTransactionFilterObject(TransactionFilter value) {
        this.transactionFilterObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
