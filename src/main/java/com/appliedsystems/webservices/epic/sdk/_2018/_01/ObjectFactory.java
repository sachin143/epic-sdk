
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2018._01 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2018._01
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertClient }
     * 
     */
    public InsertClient createInsertClient() {
        return new InsertClient();
    }

    /**
     * Create an instance of {@link InsertClientResponse }
     * 
     */
    public InsertClientResponse createInsertClientResponse() {
        return new InsertClientResponse();
    }

    /**
     * Create an instance of {@link UpdateClient }
     * 
     */
    public UpdateClient createUpdateClient() {
        return new UpdateClient();
    }

    /**
     * Create an instance of {@link InsertClientSplitReceivableTemplate }
     * 
     */
    public InsertClientSplitReceivableTemplate createInsertClientSplitReceivableTemplate() {
        return new InsertClientSplitReceivableTemplate();
    }

    /**
     * Create an instance of {@link DeleteClientSplitReceivableTemplate }
     * 
     */
    public DeleteClientSplitReceivableTemplate createDeleteClientSplitReceivableTemplate() {
        return new DeleteClientSplitReceivableTemplate();
    }

    /**
     * Create an instance of {@link InsertPolicyPolicyStatusResponse }
     * 
     */
    public InsertPolicyPolicyStatusResponse createInsertPolicyPolicyStatusResponse() {
        return new InsertPolicyPolicyStatusResponse();
    }

    /**
     * Create an instance of {@link InsertClientSplitReceivableTemplateResponse }
     * 
     */
    public InsertClientSplitReceivableTemplateResponse createInsertClientSplitReceivableTemplateResponse() {
        return new InsertClientSplitReceivableTemplateResponse();
    }

    /**
     * Create an instance of {@link GetPolicyPolicyLineType }
     * 
     */
    public GetPolicyPolicyLineType createGetPolicyPolicyLineType() {
        return new GetPolicyPolicyLineType();
    }

    /**
     * Create an instance of {@link UpdateClientResponse }
     * 
     */
    public UpdateClientResponse createUpdateClientResponse() {
        return new UpdateClientResponse();
    }

    /**
     * Create an instance of {@link InsertPolicyPolicyStatus }
     * 
     */
    public InsertPolicyPolicyStatus createInsertPolicyPolicyStatus() {
        return new InsertPolicyPolicyStatus();
    }

    /**
     * Create an instance of {@link GetPolicyPolicyLineTypeResponse }
     * 
     */
    public GetPolicyPolicyLineTypeResponse createGetPolicyPolicyLineTypeResponse() {
        return new GetPolicyPolicyLineTypeResponse();
    }

    /**
     * Create an instance of {@link UpdateClientSplitReceivableTemplateResponse }
     * 
     */
    public UpdateClientSplitReceivableTemplateResponse createUpdateClientSplitReceivableTemplateResponse() {
        return new UpdateClientSplitReceivableTemplateResponse();
    }

    /**
     * Create an instance of {@link GetClientSplitReceivableTemplate }
     * 
     */
    public GetClientSplitReceivableTemplate createGetClientSplitReceivableTemplate() {
        return new GetClientSplitReceivableTemplate();
    }

    /**
     * Create an instance of {@link GetLookupResponse }
     * 
     */
    public GetLookupResponse createGetLookupResponse() {
        return new GetLookupResponse();
    }

    /**
     * Create an instance of {@link GetLookup }
     * 
     */
    public GetLookup createGetLookup() {
        return new GetLookup();
    }

    /**
     * Create an instance of {@link GetClient }
     * 
     */
    public GetClient createGetClient() {
        return new GetClient();
    }

    /**
     * Create an instance of {@link InsertPolicyPolicyLineTypeResponse }
     * 
     */
    public InsertPolicyPolicyLineTypeResponse createInsertPolicyPolicyLineTypeResponse() {
        return new InsertPolicyPolicyLineTypeResponse();
    }

    /**
     * Create an instance of {@link GetClientSplitReceivableTemplateResponse }
     * 
     */
    public GetClientSplitReceivableTemplateResponse createGetClientSplitReceivableTemplateResponse() {
        return new GetClientSplitReceivableTemplateResponse();
    }

    /**
     * Create an instance of {@link DeleteClientSplitReceivableTemplateResponse }
     * 
     */
    public DeleteClientSplitReceivableTemplateResponse createDeleteClientSplitReceivableTemplateResponse() {
        return new DeleteClientSplitReceivableTemplateResponse();
    }

    /**
     * Create an instance of {@link InsertPolicyPolicyLineType }
     * 
     */
    public InsertPolicyPolicyLineType createInsertPolicyPolicyLineType() {
        return new InsertPolicyPolicyLineType();
    }

    /**
     * Create an instance of {@link GetClientResponse }
     * 
     */
    public GetClientResponse createGetClientResponse() {
        return new GetClientResponse();
    }

    /**
     * Create an instance of {@link UpdateClientSplitReceivableTemplate }
     * 
     */
    public UpdateClientSplitReceivableTemplate createUpdateClientSplitReceivableTemplate() {
        return new UpdateClientSplitReceivableTemplate();
    }

}
