
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ReceiptGetType" minOccurs="0"/>
 *         &lt;element name="ReceiptID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GetLimitType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ReceiptGetLimitType" minOccurs="0"/>
 *         &lt;element name="FilterType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ReceiptFilterType" minOccurs="0"/>
 *         &lt;element name="FilterField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ReceiptComparisonType" minOccurs="0"/>
 *         &lt;element name="FilterField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchType",
    "receiptID",
    "getLimitType",
    "filterType",
    "filterField1",
    "comparisonType",
    "filterField2",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_Receipt")
public class GetGeneralLedgerReceipt {

    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected ReceiptGetType searchType;
    @XmlElement(name = "ReceiptID")
    protected Integer receiptID;
    @XmlElement(name = "GetLimitType")
    @XmlSchemaType(name = "string")
    protected ReceiptGetLimitType getLimitType;
    @XmlElement(name = "FilterType")
    @XmlSchemaType(name = "string")
    protected ReceiptFilterType filterType;
    @XmlElement(name = "FilterField1", nillable = true)
    protected String filterField1;
    @XmlElement(name = "ComparisonType")
    @XmlSchemaType(name = "string")
    protected ReceiptComparisonType comparisonType;
    @XmlElement(name = "FilterField2", nillable = true)
    protected String filterField2;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptGetType }
     *     
     */
    public ReceiptGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptGetType }
     *     
     */
    public void setSearchType(ReceiptGetType value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the receiptID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptID() {
        return receiptID;
    }

    /**
     * Sets the value of the receiptID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptID(Integer value) {
        this.receiptID = value;
    }

    /**
     * Gets the value of the getLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptGetLimitType }
     *     
     */
    public ReceiptGetLimitType getGetLimitType() {
        return getLimitType;
    }

    /**
     * Sets the value of the getLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptGetLimitType }
     *     
     */
    public void setGetLimitType(ReceiptGetLimitType value) {
        this.getLimitType = value;
    }

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptFilterType }
     *     
     */
    public ReceiptFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptFilterType }
     *     
     */
    public void setFilterType(ReceiptFilterType value) {
        this.filterType = value;
    }

    /**
     * Gets the value of the filterField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField1() {
        return filterField1;
    }

    /**
     * Sets the value of the filterField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField1(String value) {
        this.filterField1 = value;
    }

    /**
     * Gets the value of the comparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptComparisonType }
     *     
     */
    public ReceiptComparisonType getComparisonType() {
        return comparisonType;
    }

    /**
     * Sets the value of the comparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptComparisonType }
     *     
     */
    public void setComparisonType(ReceiptComparisonType value) {
        this.comparisonType = value;
    }

    /**
     * Gets the value of the filterField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField2() {
        return filterField2;
    }

    /**
     * Sets the value of the filterField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField2(String value) {
        this.filterField2 = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
