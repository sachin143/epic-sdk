
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_GeneralLedger_AllocationMethodResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertGeneralLedgerAllocationMethodResult"
})
@XmlRootElement(name = "Insert_GeneralLedger_AllocationMethodResponse")
public class InsertGeneralLedgerAllocationMethodResponse {

    @XmlElement(name = "Insert_GeneralLedger_AllocationMethodResult")
    protected Integer insertGeneralLedgerAllocationMethodResult;

    /**
     * Gets the value of the insertGeneralLedgerAllocationMethodResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertGeneralLedgerAllocationMethodResult() {
        return insertGeneralLedgerAllocationMethodResult;
    }

    /**
     * Sets the value of the insertGeneralLedgerAllocationMethodResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertGeneralLedgerAllocationMethodResult(Integer value) {
        this.insertGeneralLedgerAllocationMethodResult = value;
    }

}
