
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_GeneralLedger_ReceiptResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertGeneralLedgerReceiptResult"
})
@XmlRootElement(name = "Insert_GeneralLedger_ReceiptResponse")
public class InsertGeneralLedgerReceiptResponse {

    @XmlElement(name = "Insert_GeneralLedger_ReceiptResult")
    protected Integer insertGeneralLedgerReceiptResult;

    /**
     * Gets the value of the insertGeneralLedgerReceiptResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertGeneralLedgerReceiptResult() {
        return insertGeneralLedgerReceiptResult;
    }

    /**
     * Sets the value of the insertGeneralLedgerReceiptResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertGeneralLedgerReceiptResult(Integer value) {
        this.insertGeneralLedgerReceiptResult = value;
    }

}
