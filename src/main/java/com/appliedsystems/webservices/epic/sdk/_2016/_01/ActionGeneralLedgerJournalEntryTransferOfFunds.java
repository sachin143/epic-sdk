
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferOfFundsObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/}TransferOfFunds" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferOfFundsObject"
})
@XmlRootElement(name = "Action_GeneralLedger_JournalEntryTransferOfFunds")
public class ActionGeneralLedgerJournalEntryTransferOfFunds {

    @XmlElement(name = "TransferOfFundsObject", nillable = true)
    protected TransferOfFunds transferOfFundsObject;

    /**
     * Gets the value of the transferOfFundsObject property.
     * 
     * @return
     *     possible object is
     *     {@link TransferOfFunds }
     *     
     */
    public TransferOfFunds getTransferOfFundsObject() {
        return transferOfFundsObject;
    }

    /**
     * Sets the value of the transferOfFundsObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferOfFunds }
     *     
     */
    public void setTransferOfFundsObject(TransferOfFunds value) {
        this.transferOfFundsObject = value;
    }

}
