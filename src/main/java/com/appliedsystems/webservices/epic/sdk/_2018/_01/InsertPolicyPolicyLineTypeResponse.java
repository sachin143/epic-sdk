
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Policy_PolicyLineTypeResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertPolicyPolicyLineTypeResult"
})
@XmlRootElement(name = "Insert_Policy_PolicyLineTypeResponse")
public class InsertPolicyPolicyLineTypeResponse {

    @XmlElement(name = "Insert_Policy_PolicyLineTypeResult")
    protected Integer insertPolicyPolicyLineTypeResult;

    /**
     * Gets the value of the insertPolicyPolicyLineTypeResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertPolicyPolicyLineTypeResult() {
        return insertPolicyPolicyLineTypeResult;
    }

    /**
     * Sets the value of the insertPolicyPolicyLineTypeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertPolicyPolicyLineTypeResult(Integer value) {
        this.insertPolicyPolicyLineTypeResult = value;
    }

}
