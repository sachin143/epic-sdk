
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiRemark" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/}Remark" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apiRemark"
})
@XmlRootElement(name = "Insert_Policy_CancelPolicyRemark")
public class InsertPolicyCancelPolicyRemark {

    @XmlElement(nillable = true)
    protected Remark apiRemark;

    /**
     * Gets the value of the apiRemark property.
     * 
     * @return
     *     possible object is
     *     {@link Remark }
     *     
     */
    public Remark getApiRemark() {
        return apiRemark;
    }

    /**
     * Sets the value of the apiRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link Remark }
     *     
     */
    public void setApiRemark(Remark value) {
        this.apiRemark = value;
    }

}
