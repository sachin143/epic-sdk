
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_generalledger/_reconciliation/_directbillcommission/}RecordDetailItems" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
public class GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse {

    @XmlElement(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", nillable = true)
    protected RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult;

    /**
     * Gets the value of the getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult property.
     * 
     * @return
     *     possible object is
     *     {@link RecordDetailItems }
     *     
     */
    public RecordDetailItems getGetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult() {
        return getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult;
    }

    /**
     * Sets the value of the getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordDetailItems }
     *     
     */
    public void setGetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult(RecordDetailItems value) {
        this.getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResult = value;
    }

}
