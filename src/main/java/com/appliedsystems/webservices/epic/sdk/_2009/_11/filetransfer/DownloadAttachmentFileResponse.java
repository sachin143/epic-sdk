
package com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Download_Attachment_FileResult" type="{http://schemas.microsoft.com/Message}StreamBody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "downloadAttachmentFileResult"
})
@XmlRootElement(name = "Download_Attachment_FileResponse")
public class DownloadAttachmentFileResponse {

    @XmlElement(name = "Download_Attachment_FileResult", required = true)
    protected byte[] downloadAttachmentFileResult;

    /**
     * Gets the value of the downloadAttachmentFileResult property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDownloadAttachmentFileResult() {
        return downloadAttachmentFileResult;
    }

    /**
     * Sets the value of the downloadAttachmentFileResult property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDownloadAttachmentFileResult(byte[] value) {
        this.downloadAttachmentFileResult = value;
    }

}
