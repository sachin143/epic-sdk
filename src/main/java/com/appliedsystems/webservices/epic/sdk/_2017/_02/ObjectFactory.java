
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2017._02 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2017._02
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteContact }
     * 
     */
    public DeleteContact createDeleteContact() {
        return new DeleteContact();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyRemarkResponse }
     * 
     */
    public InsertPolicyCancelPolicyRemarkResponse createInsertPolicyCancelPolicyRemarkResponse() {
        return new InsertPolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultApplyCreditsToDebits }
     * 
     */
    public GetGeneralLedgerReceiptDefaultApplyCreditsToDebits createGetGeneralLedgerReceiptDefaultApplyCreditsToDebits() {
        return new GetGeneralLedgerReceiptDefaultApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link GetClaimAdditionalPartyResponse }
     * 
     */
    public GetClaimAdditionalPartyResponse createGetClaimAdditionalPartyResponse() {
        return new GetClaimAdditionalPartyResponse();
    }

    /**
     * Create an instance of {@link GetClaimAdjustor }
     * 
     */
    public GetClaimAdjustor createGetClaimAdjustor() {
        return new GetClaimAdjustor();
    }

    /**
     * Create an instance of {@link GetClaimAdjustorResponse }
     * 
     */
    public GetClaimAdjustorResponse createGetClaimAdjustorResponse() {
        return new GetClaimAdjustorResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionUpdateStageToSubmittedResponse }
     * 
     */
    public GetPolicyDefaultActionUpdateStageToSubmittedResponse createGetPolicyDefaultActionUpdateStageToSubmittedResponse() {
        return new GetPolicyDefaultActionUpdateStageToSubmittedResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptTransferOfFunds }
     * 
     */
    public ActionGeneralLedgerReceiptTransferOfFunds createActionGeneralLedgerReceiptTransferOfFunds() {
        return new ActionGeneralLedgerReceiptTransferOfFunds();
    }

    /**
     * Create an instance of {@link GetLineResponse }
     * 
     */
    public GetLineResponse createGetLineResponse() {
        return new GetLineResponse();
    }

    /**
     * Create an instance of {@link DeleteClaimAdjustor }
     * 
     */
    public DeleteClaimAdjustor createDeleteClaimAdjustor() {
        return new DeleteClaimAdjustor();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public UpdatePolicyCancelPolicyAdditionalInterestResponse createUpdatePolicyCancelPolicyAdditionalInterestResponse() {
        return new UpdatePolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerReceiptResponse }
     * 
     */
    public DeleteGeneralLedgerReceiptResponse createDeleteGeneralLedgerReceiptResponse() {
        return new DeleteGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link ActionContactChangeMainBusinessContact }
     * 
     */
    public ActionContactChangeMainBusinessContact createActionContactChangeMainBusinessContact() {
        return new ActionContactChangeMainBusinessContact();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerActivityResponse }
     * 
     */
    public UpdateGeneralLedgerActivityResponse createUpdateGeneralLedgerActivityResponse() {
        return new UpdateGeneralLedgerActivityResponse();
    }

    /**
     * Create an instance of {@link GetPolicyCancellationRequestReasonMethodResponse }
     * 
     */
    public GetPolicyCancellationRequestReasonMethodResponse createGetPolicyCancellationRequestReasonMethodResponse() {
        return new GetPolicyCancellationRequestReasonMethodResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyActivateInactivateMultiCarrierSchedule }
     * 
     */
    public ActionPolicyActivateInactivateMultiCarrierSchedule createActionPolicyActivateInactivateMultiCarrierSchedule() {
        return new ActionPolicyActivateInactivateMultiCarrierSchedule();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyRemarkResponse }
     * 
     */
    public GetPolicyCancelPolicyRemarkResponse createGetPolicyCancelPolicyRemarkResponse() {
        return new GetPolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicy }
     * 
     */
    public UpdatePolicy createUpdatePolicy() {
        return new UpdatePolicy();
    }

    /**
     * Create an instance of {@link GetPolicyCancellationRequestReasonMethod }
     * 
     */
    public GetPolicyCancellationRequestReasonMethod createGetPolicyCancellationRequestReasonMethod() {
        return new GetPolicyCancellationRequestReasonMethod();
    }

    /**
     * Create an instance of {@link InsertClaimAdjustorResponse }
     * 
     */
    public InsertClaimAdjustorResponse createInsertClaimAdjustorResponse() {
        return new InsertClaimAdjustorResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssueEndorsement }
     * 
     */
    public ActionPolicyIssueNotIssueEndorsement createActionPolicyIssueNotIssueEndorsement() {
        return new ActionPolicyIssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssuePolicyResponse }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssuePolicyResponse createGetPolicyDefaultActionIssueNotIssuePolicyResponse() {
        return new GetPolicyDefaultActionIssueNotIssuePolicyResponse();
    }

    /**
     * Create an instance of {@link InsertClaimAdditionalParty }
     * 
     */
    public InsertClaimAdditionalParty createInsertClaimAdditionalParty() {
        return new InsertClaimAdditionalParty();
    }

    /**
     * Create an instance of {@link InsertActivityResponse }
     * 
     */
    public InsertActivityResponse createInsertActivityResponse() {
        return new InsertActivityResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionChangePolicyEffectiveExpirationDates }
     * 
     */
    public GetPolicyDefaultActionChangePolicyEffectiveExpirationDates createGetPolicyDefaultActionChangePolicyEffectiveExpirationDates() {
        return new GetPolicyDefaultActionChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionReinstateResponse }
     * 
     */
    public GetPolicyDefaultActionReinstateResponse createGetPolicyDefaultActionReinstateResponse() {
        return new GetPolicyDefaultActionReinstateResponse();
    }

    /**
     * Create an instance of {@link GetClaimServicingContacts }
     * 
     */
    public GetClaimServicingContacts createGetClaimServicingContacts() {
        return new GetClaimServicingContacts();
    }

    /**
     * Create an instance of {@link GetContact }
     * 
     */
    public GetContact createGetContact() {
        return new GetContact();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssueEndorsement }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssueEndorsement createGetPolicyDefaultActionIssueNotIssueEndorsement() {
        return new GetPolicyDefaultActionIssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionReinstate }
     * 
     */
    public GetPolicyDefaultActionReinstate createGetPolicyDefaultActionReinstate() {
        return new GetPolicyDefaultActionReinstate();
    }

    /**
     * Create an instance of {@link ActionPolicyUpdateStageToSubmitted }
     * 
     */
    public ActionPolicyUpdateStageToSubmitted createActionPolicyUpdateStageToSubmitted() {
        return new ActionPolicyUpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link ActionPolicyUpdateStageToSubmittedResponse }
     * 
     */
    public ActionPolicyUpdateStageToSubmittedResponse createActionPolicyUpdateStageToSubmittedResponse() {
        return new ActionPolicyUpdateStageToSubmittedResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyActivateInactivateMultiCarrierScheduleResponse }
     * 
     */
    public ActionPolicyActivateInactivateMultiCarrierScheduleResponse createActionPolicyActivateInactivateMultiCarrierScheduleResponse() {
        return new ActionPolicyActivateInactivateMultiCarrierScheduleResponse();
    }

    /**
     * Create an instance of {@link InsertContactResponse }
     * 
     */
    public InsertContactResponse createInsertContactResponse() {
        return new InsertContactResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancellationRequestReasonMethodResponse }
     * 
     */
    public UpdatePolicyCancellationRequestReasonMethodResponse createUpdatePolicyCancellationRequestReasonMethodResponse() {
        return new UpdatePolicyCancellationRequestReasonMethodResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerActivityResponse }
     * 
     */
    public InsertGeneralLedgerActivityResponse createInsertGeneralLedgerActivityResponse() {
        return new InsertGeneralLedgerActivityResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimAdditionalParty }
     * 
     */
    public UpdateClaimAdditionalParty createUpdateClaimAdditionalParty() {
        return new UpdateClaimAdditionalParty();
    }

    /**
     * Create an instance of {@link GetClaimResponse }
     * 
     */
    public GetClaimResponse createGetClaimResponse() {
        return new GetClaimResponse();
    }

    /**
     * Create an instance of {@link UpdateLineResponse }
     * 
     */
    public UpdateLineResponse createUpdateLineResponse() {
        return new UpdateLineResponse();
    }

    /**
     * Create an instance of {@link DeletePolicy }
     * 
     */
    public DeletePolicy createDeletePolicy() {
        return new DeletePolicy();
    }

    /**
     * Create an instance of {@link InsertClaimAdditionalPartyResponse }
     * 
     */
    public InsertClaimAdditionalPartyResponse createInsertClaimAdditionalPartyResponse() {
        return new InsertClaimAdditionalPartyResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseAddLineMidterm }
     * 
     */
    public ActionPolicyEndorseReviseAddLineMidterm createActionPolicyEndorseReviseAddLineMidterm() {
        return new ActionPolicyEndorseReviseAddLineMidterm();
    }

    /**
     * Create an instance of {@link GetActivityResponse }
     * 
     */
    public GetActivityResponse createGetActivityResponse() {
        return new GetActivityResponse();
    }

    /**
     * Create an instance of {@link ActionContactChangeMainBusinessContactResponse }
     * 
     */
    public ActionContactChangeMainBusinessContactResponse createActionContactChangeMainBusinessContactResponse() {
        return new ActionContactChangeMainBusinessContactResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionCancel }
     * 
     */
    public GetPolicyDefaultActionCancel createGetPolicyDefaultActionCancel() {
        return new GetPolicyDefaultActionCancel();
    }

    /**
     * Create an instance of {@link InsertPolicyResponse }
     * 
     */
    public InsertPolicyResponse createInsertPolicyResponse() {
        return new InsertPolicyResponse();
    }

    /**
     * Create an instance of {@link UpdateActivity }
     * 
     */
    public UpdateActivity createUpdateActivity() {
        return new UpdateActivity();
    }

    /**
     * Create an instance of {@link UpdatePolicyResponse }
     * 
     */
    public UpdatePolicyResponse createUpdatePolicyResponse() {
        return new UpdatePolicyResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimInsuredContactResponse }
     * 
     */
    public UpdateClaimInsuredContactResponse createUpdateClaimInsuredContactResponse() {
        return new UpdateClaimInsuredContactResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueCancellation }
     * 
     */
    public ActionPolicyIssueCancellation createActionPolicyIssueCancellation() {
        return new ActionPolicyIssueCancellation();
    }

    /**
     * Create an instance of {@link GetActivity }
     * 
     */
    public GetActivity createGetActivity() {
        return new GetActivity();
    }

    /**
     * Create an instance of {@link ActionPolicyRenewResponse }
     * 
     */
    public ActionPolicyRenewResponse createActionPolicyRenewResponse() {
        return new ActionPolicyRenewResponse();
    }

    /**
     * Create an instance of {@link DeleteContactResponse }
     * 
     */
    public DeleteContactResponse createDeleteContactResponse() {
        return new DeleteContactResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyProspectiveContractedStatus }
     * 
     */
    public ActionPolicyChangePolicyProspectiveContractedStatus createActionPolicyChangePolicyProspectiveContractedStatus() {
        return new ActionPolicyChangePolicyProspectiveContractedStatus();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptTransferOfFundsResponse }
     * 
     */
    public ActionGeneralLedgerReceiptTransferOfFundsResponse createActionGeneralLedgerReceiptTransferOfFundsResponse() {
        return new ActionGeneralLedgerReceiptTransferOfFundsResponse();
    }

    /**
     * Create an instance of {@link InsertContact }
     * 
     */
    public InsertContact createInsertContact() {
        return new InsertContact();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssuePolicy }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssuePolicy createGetPolicyDefaultActionIssueNotIssuePolicy() {
        return new GetPolicyDefaultActionIssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link InsertPolicy }
     * 
     */
    public InsertPolicy createInsertPolicy() {
        return new InsertPolicy();
    }

    /**
     * Create an instance of {@link DeleteLineResponse }
     * 
     */
    public DeleteLineResponse createDeleteLineResponse() {
        return new DeleteLineResponse();
    }

    /**
     * Create an instance of {@link UpdateContactResponse }
     * 
     */
    public UpdateContactResponse createUpdateContactResponse() {
        return new UpdateContactResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseAddLineMidtermResponse }
     * 
     */
    public ActionPolicyEndorseReviseAddLineMidtermResponse createActionPolicyEndorseReviseAddLineMidtermResponse() {
        return new ActionPolicyEndorseReviseAddLineMidtermResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyRemark }
     * 
     */
    public DeletePolicyCancelPolicyRemark createDeletePolicyCancelPolicyRemark() {
        return new DeletePolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerActivityResponse }
     * 
     */
    public GetGeneralLedgerActivityResponse createGetGeneralLedgerActivityResponse() {
        return new GetGeneralLedgerActivityResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse }
     * 
     */
    public GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse createGetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse() {
        return new GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerReceipt }
     * 
     */
    public InsertGeneralLedgerReceipt createInsertGeneralLedgerReceipt() {
        return new InsertGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link UpdateClaimLitigation }
     * 
     */
    public UpdateClaimLitigation createUpdateClaimLitigation() {
        return new UpdateClaimLitigation();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssueEndorsementResponse }
     * 
     */
    public ActionPolicyIssueNotIssueEndorsementResponse createActionPolicyIssueNotIssueEndorsementResponse() {
        return new ActionPolicyIssueNotIssueEndorsementResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyEffectiveExpirationDates }
     * 
     */
    public ActionPolicyChangePolicyEffectiveExpirationDates createActionPolicyChangePolicyEffectiveExpirationDates() {
        return new ActionPolicyChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link UpdateContact }
     * 
     */
    public UpdateContact createUpdateContact() {
        return new UpdateContact();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssuePolicyResponse }
     * 
     */
    public ActionPolicyIssueNotIssuePolicyResponse createActionPolicyIssueNotIssuePolicyResponse() {
        return new ActionPolicyIssueNotIssuePolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueCancellationResponse }
     * 
     */
    public GetPolicyDefaultActionIssueCancellationResponse createGetPolicyDefaultActionIssueCancellationResponse() {
        return new GetPolicyDefaultActionIssueCancellationResponse();
    }

    /**
     * Create an instance of {@link GetClaimInsuredContact }
     * 
     */
    public GetClaimInsuredContact createGetClaimInsuredContact() {
        return new GetClaimInsuredContact();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public DeletePolicyCancelPolicyAdditionalInterestResponse createDeletePolicyCancelPolicyAdditionalInterestResponse() {
        return new DeletePolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimAdditionalPartyResponse }
     * 
     */
    public UpdateClaimAdditionalPartyResponse createUpdateClaimAdditionalPartyResponse() {
        return new UpdateClaimAdditionalPartyResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse }
     * 
     */
    public GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse createGetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse() {
        return new GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyRenew }
     * 
     */
    public ActionPolicyRenew createActionPolicyRenew() {
        return new ActionPolicyRenew();
    }

    /**
     * Create an instance of {@link DeleteLine }
     * 
     */
    public DeleteLine createDeleteLine() {
        return new DeleteLine();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public InsertPolicyCancelPolicyAdditionalInterestResponse createInsertPolicyCancelPolicyAdditionalInterestResponse() {
        return new InsertPolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssueEndorsementResponse }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssueEndorsementResponse createGetPolicyDefaultActionIssueNotIssueEndorsementResponse() {
        return new GetPolicyDefaultActionIssueNotIssueEndorsementResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyRemarkResponse }
     * 
     */
    public DeletePolicyCancelPolicyRemarkResponse createDeletePolicyCancelPolicyRemarkResponse() {
        return new DeletePolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link DeleteClaimResponse }
     * 
     */
    public DeleteClaimResponse createDeleteClaimResponse() {
        return new DeleteClaimResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerReceipt }
     * 
     */
    public UpdateGeneralLedgerReceipt createUpdateGeneralLedgerReceipt() {
        return new UpdateGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link GetClaimAdditionalParty }
     * 
     */
    public GetClaimAdditionalParty createGetClaimAdditionalParty() {
        return new GetClaimAdditionalParty();
    }

    /**
     * Create an instance of {@link UpdateLine }
     * 
     */
    public UpdateLine createUpdateLine() {
        return new UpdateLine();
    }

    /**
     * Create an instance of {@link UpdateClaimAdjustorResponse }
     * 
     */
    public UpdateClaimAdjustorResponse createUpdateClaimAdjustorResponse() {
        return new UpdateClaimAdjustorResponse();
    }

    /**
     * Create an instance of {@link InsertLineResponse }
     * 
     */
    public InsertLineResponse createInsertLineResponse() {
        return new InsertLineResponse();
    }

    /**
     * Create an instance of {@link ActionClaimCloseReopenClaim }
     * 
     */
    public ActionClaimCloseReopenClaim createActionClaimCloseReopenClaim() {
        return new ActionClaimCloseReopenClaim();
    }

    /**
     * Create an instance of {@link InsertLine }
     * 
     */
    public InsertLine createInsertLine() {
        return new InsertLine();
    }

    /**
     * Create an instance of {@link GetClaimPaymentExpense }
     * 
     */
    public GetClaimPaymentExpense createGetClaimPaymentExpense() {
        return new GetClaimPaymentExpense();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssuePolicy }
     * 
     */
    public ActionPolicyIssueNotIssuePolicy createActionPolicyIssueNotIssuePolicy() {
        return new ActionPolicyIssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link ActionPolicyReinstateResponse }
     * 
     */
    public ActionPolicyReinstateResponse createActionPolicyReinstateResponse() {
        return new ActionPolicyReinstateResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyAdditionalInterest }
     * 
     */
    public DeletePolicyCancelPolicyAdditionalInterest createDeletePolicyCancelPolicyAdditionalInterest() {
        return new DeletePolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancellationRequestReasonMethod }
     * 
     */
    public UpdatePolicyCancellationRequestReasonMethod createUpdatePolicyCancellationRequestReasonMethod() {
        return new UpdatePolicyCancellationRequestReasonMethod();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerReceiptResponse }
     * 
     */
    public UpdateGeneralLedgerReceiptResponse createUpdateGeneralLedgerReceiptResponse() {
        return new UpdateGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptResponse }
     * 
     */
    public GetGeneralLedgerReceiptResponse createGetGeneralLedgerReceiptResponse() {
        return new GetGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link InsertClaimAdjustor }
     * 
     */
    public InsertClaimAdjustor createInsertClaimAdjustor() {
        return new InsertClaimAdjustor();
    }

    /**
     * Create an instance of {@link UpdateClaimPaymentExpenseResponse }
     * 
     */
    public UpdateClaimPaymentExpenseResponse createUpdateClaimPaymentExpenseResponse() {
        return new UpdateClaimPaymentExpenseResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyCancel }
     * 
     */
    public ActionPolicyCancel createActionPolicyCancel() {
        return new ActionPolicyCancel();
    }

    /**
     * Create an instance of {@link ActionClaimCloseReopenClaimResponse }
     * 
     */
    public ActionClaimCloseReopenClaimResponse createActionClaimCloseReopenClaimResponse() {
        return new ActionClaimCloseReopenClaimResponse();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyAdditionalInterest }
     * 
     */
    public InsertPolicyCancelPolicyAdditionalInterest createInsertPolicyCancelPolicyAdditionalInterest() {
        return new InsertPolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link UpdateClaimPaymentExpense }
     * 
     */
    public UpdateClaimPaymentExpense createUpdateClaimPaymentExpense() {
        return new UpdateClaimPaymentExpense();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerReceiptResponse }
     * 
     */
    public InsertGeneralLedgerReceiptResponse createInsertGeneralLedgerReceiptResponse() {
        return new InsertGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyResponse }
     * 
     */
    public DeletePolicyResponse createDeletePolicyResponse() {
        return new DeletePolicyResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyCancelResponse }
     * 
     */
    public ActionPolicyCancelResponse createActionPolicyCancelResponse() {
        return new ActionPolicyCancelResponse();
    }

    /**
     * Create an instance of {@link DeleteClaimAdditionalPartyResponse }
     * 
     */
    public DeleteClaimAdditionalPartyResponse createDeleteClaimAdditionalPartyResponse() {
        return new DeleteClaimAdditionalPartyResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimInsuredContact }
     * 
     */
    public UpdateClaimInsuredContact createUpdateClaimInsuredContact() {
        return new UpdateClaimInsuredContact();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionUpdateStageToSubmitted }
     * 
     */
    public GetPolicyDefaultActionUpdateStageToSubmitted createGetPolicyDefaultActionUpdateStageToSubmitted() {
        return new GetPolicyDefaultActionUpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link InsertActivity }
     * 
     */
    public InsertActivity createInsertActivity() {
        return new InsertActivity();
    }

    /**
     * Create an instance of {@link UpdateClaimLitigationResponse }
     * 
     */
    public UpdateClaimLitigationResponse createUpdateClaimLitigationResponse() {
        return new UpdateClaimLitigationResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimAdjustor }
     * 
     */
    public UpdateClaimAdjustor createUpdateClaimAdjustor() {
        return new UpdateClaimAdjustor();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceipt }
     * 
     */
    public GetGeneralLedgerReceipt createGetGeneralLedgerReceipt() {
        return new GetGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseExistingLineResponse }
     * 
     */
    public ActionPolicyEndorseReviseExistingLineResponse createActionPolicyEndorseReviseExistingLineResponse() {
        return new ActionPolicyEndorseReviseExistingLineResponse();
    }

    /**
     * Create an instance of {@link InsertClaim }
     * 
     */
    public InsertClaim createInsertClaim() {
        return new InsertClaim();
    }

    /**
     * Create an instance of {@link DeleteClaimAdditionalParty }
     * 
     */
    public DeleteClaimAdditionalParty createDeleteClaimAdditionalParty() {
        return new DeleteClaimAdditionalParty();
    }

    /**
     * Create an instance of {@link GetClaimServicingContactsResponse }
     * 
     */
    public GetClaimServicingContactsResponse createGetClaimServicingContactsResponse() {
        return new GetClaimServicingContactsResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerActivity }
     * 
     */
    public InsertGeneralLedgerActivity createInsertGeneralLedgerActivity() {
        return new InsertGeneralLedgerActivity();
    }

    /**
     * Create an instance of {@link GetContactResponse }
     * 
     */
    public GetContactResponse createGetContactResponse() {
        return new GetContactResponse();
    }

    /**
     * Create an instance of {@link UpdateClaimServicingContactsResponse }
     * 
     */
    public UpdateClaimServicingContactsResponse createUpdateClaimServicingContactsResponse() {
        return new UpdateClaimServicingContactsResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseExistingLine }
     * 
     */
    public ActionPolicyEndorseReviseExistingLine createActionPolicyEndorseReviseExistingLine() {
        return new ActionPolicyEndorseReviseExistingLine();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyRemark }
     * 
     */
    public UpdatePolicyCancelPolicyRemark createUpdatePolicyCancelPolicyRemark() {
        return new UpdatePolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link GetClaimLitigationResponse }
     * 
     */
    public GetClaimLitigationResponse createGetClaimLitigationResponse() {
        return new GetClaimLitigationResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerActivity }
     * 
     */
    public GetGeneralLedgerActivity createGetGeneralLedgerActivity() {
        return new GetGeneralLedgerActivity();
    }

    /**
     * Create an instance of {@link UpdateClaim }
     * 
     */
    public UpdateClaim createUpdateClaim() {
        return new UpdateClaim();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyRemark }
     * 
     */
    public GetPolicyCancelPolicyRemark createGetPolicyCancelPolicyRemark() {
        return new GetPolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link InsertClaimResponse }
     * 
     */
    public InsertClaimResponse createInsertClaimResponse() {
        return new InsertClaimResponse();
    }

    /**
     * Create an instance of {@link GetClaimLitigation }
     * 
     */
    public GetClaimLitigation createGetClaimLitigation() {
        return new GetClaimLitigation();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerActivity }
     * 
     */
    public UpdateGeneralLedgerActivity createUpdateGeneralLedgerActivity() {
        return new UpdateGeneralLedgerActivity();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptFinalizeReceipt }
     * 
     */
    public ActionGeneralLedgerReceiptFinalizeReceipt createActionGeneralLedgerReceiptFinalizeReceipt() {
        return new ActionGeneralLedgerReceiptFinalizeReceipt();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyEffectiveExpirationDatesResponse }
     * 
     */
    public ActionPolicyChangePolicyEffectiveExpirationDatesResponse createActionPolicyChangePolicyEffectiveExpirationDatesResponse() {
        return new ActionPolicyChangePolicyEffectiveExpirationDatesResponse();
    }

    /**
     * Create an instance of {@link GetPolicy }
     * 
     */
    public GetPolicy createGetPolicy() {
        return new GetPolicy();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptFinalizeReceiptResponse }
     * 
     */
    public ActionGeneralLedgerReceiptFinalizeReceiptResponse createActionGeneralLedgerReceiptFinalizeReceiptResponse() {
        return new ActionGeneralLedgerReceiptFinalizeReceiptResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangeServiceSummaryDescription }
     * 
     */
    public ActionPolicyChangeServiceSummaryDescription createActionPolicyChangeServiceSummaryDescription() {
        return new ActionPolicyChangeServiceSummaryDescription();
    }

    /**
     * Create an instance of {@link UpdateClaimServicingContacts }
     * 
     */
    public UpdateClaimServicingContacts createUpdateClaimServicingContacts() {
        return new UpdateClaimServicingContacts();
    }

    /**
     * Create an instance of {@link ActionPolicyChangeServiceSummaryDescriptionResponse }
     * 
     */
    public ActionPolicyChangeServiceSummaryDescriptionResponse createActionPolicyChangeServiceSummaryDescriptionResponse() {
        return new ActionPolicyChangeServiceSummaryDescriptionResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyProspectiveContractedStatusResponse }
     * 
     */
    public ActionPolicyChangePolicyProspectiveContractedStatusResponse createActionPolicyChangePolicyProspectiveContractedStatusResponse() {
        return new ActionPolicyChangePolicyProspectiveContractedStatusResponse();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyRemark }
     * 
     */
    public InsertPolicyCancelPolicyRemark createInsertPolicyCancelPolicyRemark() {
        return new InsertPolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link DeleteClaimAdjustorResponse }
     * 
     */
    public DeleteClaimAdjustorResponse createDeleteClaimAdjustorResponse() {
        return new DeleteClaimAdjustorResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionRenewResponse }
     * 
     */
    public GetPolicyDefaultActionRenewResponse createGetPolicyDefaultActionRenewResponse() {
        return new GetPolicyDefaultActionRenewResponse();
    }

    /**
     * Create an instance of {@link GetClaimPaymentExpenseResponse }
     * 
     */
    public GetClaimPaymentExpenseResponse createGetClaimPaymentExpenseResponse() {
        return new GetClaimPaymentExpenseResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueCancellationResponse }
     * 
     */
    public ActionPolicyIssueCancellationResponse createActionPolicyIssueCancellationResponse() {
        return new ActionPolicyIssueCancellationResponse();
    }

    /**
     * Create an instance of {@link GetLine }
     * 
     */
    public GetLine createGetLine() {
        return new GetLine();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public GetPolicyCancelPolicyAdditionalInterestResponse createGetPolicyCancelPolicyAdditionalInterestResponse() {
        return new GetPolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link GetClaimInsuredContactResponse }
     * 
     */
    public GetClaimInsuredContactResponse createGetClaimInsuredContactResponse() {
        return new GetClaimInsuredContactResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyAdditionalInterest }
     * 
     */
    public UpdatePolicyCancelPolicyAdditionalInterest createUpdatePolicyCancelPolicyAdditionalInterest() {
        return new UpdatePolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link UpdateClaimResponse }
     * 
     */
    public UpdateClaimResponse createUpdateClaimResponse() {
        return new UpdateClaimResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionCancelResponse }
     * 
     */
    public GetPolicyDefaultActionCancelResponse createGetPolicyDefaultActionCancelResponse() {
        return new GetPolicyDefaultActionCancelResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyReinstate }
     * 
     */
    public ActionPolicyReinstate createActionPolicyReinstate() {
        return new ActionPolicyReinstate();
    }

    /**
     * Create an instance of {@link DeleteClaim }
     * 
     */
    public DeleteClaim createDeleteClaim() {
        return new DeleteClaim();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerReceipt }
     * 
     */
    public DeleteGeneralLedgerReceipt createDeleteGeneralLedgerReceipt() {
        return new DeleteGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link GetClaim }
     * 
     */
    public GetClaim createGetClaim() {
        return new GetClaim();
    }

    /**
     * Create an instance of {@link GetPolicyResponse }
     * 
     */
    public GetPolicyResponse createGetPolicyResponse() {
        return new GetPolicyResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyRemarkResponse }
     * 
     */
    public UpdatePolicyCancelPolicyRemarkResponse createUpdatePolicyCancelPolicyRemarkResponse() {
        return new UpdatePolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueCancellation }
     * 
     */
    public GetPolicyDefaultActionIssueCancellation createGetPolicyDefaultActionIssueCancellation() {
        return new GetPolicyDefaultActionIssueCancellation();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionRenew }
     * 
     */
    public GetPolicyDefaultActionRenew createGetPolicyDefaultActionRenew() {
        return new GetPolicyDefaultActionRenew();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyAdditionalInterest }
     * 
     */
    public GetPolicyCancelPolicyAdditionalInterest createGetPolicyCancelPolicyAdditionalInterest() {
        return new GetPolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link UpdateActivityResponse }
     * 
     */
    public UpdateActivityResponse createUpdateActivityResponse() {
        return new UpdateActivityResponse();
    }

}
