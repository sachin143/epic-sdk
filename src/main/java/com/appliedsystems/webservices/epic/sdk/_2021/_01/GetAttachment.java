
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttachmentFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}AttachmentFilter" minOccurs="0"/>
 *         &lt;element name="AttachmentSortingObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}AttachmentSorting" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PageSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "attachmentFilterObject",
    "attachmentSortingObject",
    "pageNumber",
    "pageSize"
})
@XmlRootElement(name = "Get_Attachment")
public class GetAttachment {

    @XmlElement(name = "AttachmentFilterObject", nillable = true)
    protected AttachmentFilter attachmentFilterObject;
    @XmlElement(name = "AttachmentSortingObject", nillable = true)
    protected AttachmentSorting attachmentSortingObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;
    @XmlElement(name = "PageSize")
    protected Integer pageSize;

    /**
     * Gets the value of the attachmentFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentFilter }
     *     
     */
    public AttachmentFilter getAttachmentFilterObject() {
        return attachmentFilterObject;
    }

    /**
     * Sets the value of the attachmentFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentFilter }
     *     
     */
    public void setAttachmentFilterObject(AttachmentFilter value) {
        this.attachmentFilterObject = value;
    }

    /**
     * Gets the value of the attachmentSortingObject property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentSorting }
     *     
     */
    public AttachmentSorting getAttachmentSortingObject() {
        return attachmentSortingObject;
    }

    /**
     * Sets the value of the attachmentSortingObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentSorting }
     *     
     */
    public void setAttachmentSortingObject(AttachmentSorting value) {
        this.attachmentSortingObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageSize(Integer value) {
        this.pageSize = value;
    }

}
