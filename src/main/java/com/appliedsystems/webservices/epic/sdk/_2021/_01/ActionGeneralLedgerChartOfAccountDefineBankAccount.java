
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankAccountObject" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/}BankAccount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bankAccountObject"
})
@XmlRootElement(name = "Action_GeneralLedger_ChartOfAccountDefineBankAccount")
public class ActionGeneralLedgerChartOfAccountDefineBankAccount {

    @XmlElement(name = "BankAccountObject", nillable = true)
    protected BankAccount bankAccountObject;

    /**
     * Gets the value of the bankAccountObject property.
     * 
     * @return
     *     possible object is
     *     {@link BankAccount }
     *     
     */
    public BankAccount getBankAccountObject() {
        return bankAccountObject;
    }

    /**
     * Sets the value of the bankAccountObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccount }
     *     
     */
    public void setBankAccountObject(BankAccount value) {
        this.bankAccountObject = value;
    }

}
