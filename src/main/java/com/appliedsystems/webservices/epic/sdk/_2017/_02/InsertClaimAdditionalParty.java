
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalPartyObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}AdditionalParty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "additionalPartyObject"
})
@XmlRootElement(name = "Insert_Claim_AdditionalParty")
public class InsertClaimAdditionalParty {

    @XmlElement(name = "AdditionalPartyObject", nillable = true)
    protected AdditionalParty additionalPartyObject;

    /**
     * Gets the value of the additionalPartyObject property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalParty }
     *     
     */
    public AdditionalParty getAdditionalPartyObject() {
        return additionalPartyObject;
    }

    /**
     * Sets the value of the additionalPartyObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalParty }
     *     
     */
    public void setAdditionalPartyObject(AdditionalParty value) {
        this.additionalPartyObject = value;
    }

}
