
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/}Client" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientObject"
})
@XmlRootElement(name = "Insert_Client")
public class InsertClient {

    @XmlElement(name = "ClientObject", nillable = true)
    protected Client clientObject;

    /**
     * Gets the value of the clientObject property.
     * 
     * @return
     *     possible object is
     *     {@link Client }
     *     
     */
    public Client getClientObject() {
        return clientObject;
    }

    /**
     * Sets the value of the clientObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Client }
     *     
     */
    public void setClientObject(Client value) {
        this.clientObject = value;
    }

}
