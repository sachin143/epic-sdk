
package com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddOnAttachmentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addOnAttachmentID"
})
@XmlRootElement(name = "Download_AddOn_Attachment_File")
public class DownloadAddOnAttachmentFile {

    @XmlElement(name = "AddOnAttachmentID")
    protected Integer addOnAttachmentID;

    /**
     * Gets the value of the addOnAttachmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAddOnAttachmentID() {
        return addOnAttachmentID;
    }

    /**
     * Sets the value of the addOnAttachmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAddOnAttachmentID(Integer value) {
        this.addOnAttachmentID = value;
    }

}
