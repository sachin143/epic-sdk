
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LitigationObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}Litigation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "litigationObject"
})
@XmlRootElement(name = "Update_Claim_Litigation")
public class UpdateClaimLitigation {

    @XmlElement(name = "LitigationObject", nillable = true)
    protected Litigation litigationObject;

    /**
     * Gets the value of the litigationObject property.
     * 
     * @return
     *     possible object is
     *     {@link Litigation }
     *     
     */
    public Litigation getLitigationObject() {
        return litigationObject;
    }

    /**
     * Sets the value of the litigationObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Litigation }
     *     
     */
    public void setLitigationObject(Litigation value) {
        this.litigationObject = value;
    }

}
