
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChartOfObject" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/}ChartOfAccount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "chartOfObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_ChartOfAccount")
public class InsertGeneralLedgerChartOfAccount {

    @XmlElement(name = "ChartOfObject", nillable = true)
    protected ChartOfAccount chartOfObject;

    /**
     * Gets the value of the chartOfObject property.
     * 
     * @return
     *     possible object is
     *     {@link ChartOfAccount }
     *     
     */
    public ChartOfAccount getChartOfObject() {
        return chartOfObject;
    }

    /**
     * Sets the value of the chartOfObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChartOfAccount }
     *     
     */
    public void setChartOfObject(ChartOfAccount value) {
        this.chartOfObject = value;
    }

}
