
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyLineTypeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/}PolicyLineType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyLineTypeObject"
})
@XmlRootElement(name = "Insert_Policy_PolicyLineType")
public class InsertPolicyPolicyLineType {

    @XmlElement(name = "PolicyLineTypeObject", nillable = true)
    protected PolicyLineType policyLineTypeObject;

    /**
     * Gets the value of the policyLineTypeObject property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyLineType }
     *     
     */
    public PolicyLineType getPolicyLineTypeObject() {
        return policyLineTypeObject;
    }

    /**
     * Sets the value of the policyLineTypeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyLineType }
     *     
     */
    public void setPolicyLineTypeObject(PolicyLineType value) {
        this.policyLineTypeObject = value;
    }

}
