
package com.appliedsystems.webservices.epic.sdk._2011._12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_TransactionCodeResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/}ArrayOfTransactionCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionTransactionCodeResult"
})
@XmlRootElement(name = "Get_Transaction_TransactionCodeResponse")
public class GetTransactionTransactionCodeResponse {

    @XmlElement(name = "Get_Transaction_TransactionCodeResult", nillable = true)
    protected ArrayOfTransactionCode getTransactionTransactionCodeResult;

    /**
     * Gets the value of the getTransactionTransactionCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTransactionCode }
     *     
     */
    public ArrayOfTransactionCode getGetTransactionTransactionCodeResult() {
        return getTransactionTransactionCodeResult;
    }

    /**
     * Sets the value of the getTransactionTransactionCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTransactionCode }
     *     
     */
    public void setGetTransactionTransactionCodeResult(ArrayOfTransactionCode value) {
        this.getTransactionTransactionCodeResult = value;
    }

}
