
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_ActivityResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}ActivityGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getActivityResult"
})
@XmlRootElement(name = "Get_ActivityResponse")
public class GetActivityResponse {

    @XmlElement(name = "Get_ActivityResult", nillable = true)
    protected ActivityGetResult getActivityResult;

    /**
     * Gets the value of the getActivityResult property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityGetResult }
     *     
     */
    public ActivityGetResult getGetActivityResult() {
        return getActivityResult;
    }

    /**
     * Sets the value of the getActivityResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityGetResult }
     *     
     */
    public void setGetActivityResult(ActivityGetResult value) {
        this.getActivityResult = value;
    }

}
