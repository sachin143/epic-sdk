
package com.appliedsystems.webservices.epic.sdk._2011._12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Transaction_TransactionCodeResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertTransactionTransactionCodeResult"
})
@XmlRootElement(name = "Insert_Transaction_TransactionCodeResponse")
public class InsertTransactionTransactionCodeResponse {

    @XmlElement(name = "Insert_Transaction_TransactionCodeResult")
    protected Integer insertTransactionTransactionCodeResult;

    /**
     * Gets the value of the insertTransactionTransactionCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertTransactionTransactionCodeResult() {
        return insertTransactionTransactionCodeResult;
    }

    /**
     * Sets the value of the insertTransactionTransactionCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertTransactionTransactionCodeResult(Integer value) {
        this.insertTransactionTransactionCodeResult = value;
    }

}
