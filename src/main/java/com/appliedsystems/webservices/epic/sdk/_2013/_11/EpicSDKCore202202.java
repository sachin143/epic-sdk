
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalPartyGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdjustorGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Summary;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.CancellationRequestReasonMethodGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.AccountsReceivableWriteOff;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.AdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.JournalEntry;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Receipt;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Voucher;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyAdditionalInterestGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineTypeGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._client.SplitReceivableTemplateGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccountGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationEntriesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationMethod;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGrouping;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGroupingsGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.TaxFeeRatesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult;
import com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfDiscount;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfModifyRevenueDeferralSchedule;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.BalanceTransfer;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.Discount;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ModifyRevenueDeferralSchedule;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ReverseBalanceTransfer;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKCore_2022_02", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKCore202202 {


    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param clientFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult
     * @throws EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client")
    @WebResult(name = "Get_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClient")
    @ResponseWrapper(localName = "Get_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientResponse")
    public ClientGetResult getClient(
        @WebParam(name = "ClientFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ClientFilter clientFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client")
    @WebResult(name = "Insert_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClient")
    @ResponseWrapper(localName = "Insert_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientResponse")
    public Integer insertClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @throws EpicSDKCore202202UpdateClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client")
    @RequestWrapper(localName = "Update_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClient")
    @ResponseWrapper(localName = "Update_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientResponse")
    public void updateClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClientInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate
     * @throws EpicSDKCore202202GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client_SplitReceivableTemplate")
    @WebResult(name = "Get_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Get_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplateResponse")
    public ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplate(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplateGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @throws EpicSDKCore202202UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Update_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Update_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplateResponse")
    public void updateClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client_SplitReceivableTemplate")
    @WebResult(name = "Insert_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Insert_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplateResponse")
    public Integer insertClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateID
     * @throws EpicSDKCore202202DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Delete_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Delete_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Delete_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplateResponse")
    public void deleteClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer splitReceivableTemplateID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Insert_Activity")
    @WebResult(name = "Insert_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivity")
    @ResponseWrapper(localName = "Insert_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivityResponse")
    public Integer insertActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Update_Activity")
    @RequestWrapper(localName = "Update_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivity")
    @ResponseWrapper(localName = "Update_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivityResponse")
    public void updateActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202202GetActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Get_Activity")
    @WebResult(name = "Get_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivity")
    @ResponseWrapper(localName = "Get_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivityResponse")
    public ActivityGetResult getActivity(
        @WebParam(name = "ActivityFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityFilter activityFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param lineFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult
     * @throws EpicSDKCore202202GetLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLineAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Get_Line")
    @WebResult(name = "Get_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLine")
    @ResponseWrapper(localName = "Get_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLineResponse")
    public LineGetResult getLine(
        @WebParam(name = "LineFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        LineFilter lineFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetLineInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Insert_Line")
    @WebResult(name = "Insert_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLine")
    @ResponseWrapper(localName = "Insert_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLineResponse")
    public Integer insertLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertLineInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @throws EpicSDKCore202202UpdateLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Update_Line")
    @RequestWrapper(localName = "Update_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLine")
    @ResponseWrapper(localName = "Update_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLineResponse")
    public void updateLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateLineInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineID
     * @throws EpicSDKCore202202DeleteLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Delete_Line")
    @RequestWrapper(localName = "Delete_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLine")
    @ResponseWrapper(localName = "Delete_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLineResponse")
    public void deleteLine(
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteLineInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param contactFilterObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult
     * @throws EpicSDKCore202202GetContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Get_Contact")
    @WebResult(name = "Get_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContact")
    @ResponseWrapper(localName = "Get_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContactResponse")
    public ContactGetResult getContact(
        @WebParam(name = "ContactFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ContactFilter contactFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param id
     * @throws EpicSDKCore202202DeleteContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Delete_Contact")
    @RequestWrapper(localName = "Delete_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContact")
    @ResponseWrapper(localName = "Delete_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContactResponse")
    public void deleteContact(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Insert_Contact")
    @WebResult(name = "Insert_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContact")
    @ResponseWrapper(localName = "Insert_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContactResponse")
    public Integer insertContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @throws EpicSDKCore202202UpdateContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Update_Contact")
    @RequestWrapper(localName = "Update_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContact")
    @ResponseWrapper(localName = "Update_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContactResponse")
    public void updateContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactID
     * @throws EpicSDKCore202202ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Contact_ChangeMainBusinessContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Action_Contact_ChangeMainBusinessContact")
    @RequestWrapper(localName = "Action_Contact_ChangeMainBusinessContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContact")
    @ResponseWrapper(localName = "Action_Contact_ChangeMainBusinessContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContactResponse")
    public void actionContactChangeMainBusinessContact(
        @WebParam(name = "ContactID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer contactID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param policyFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult
     * @throws EpicSDKCore202202GetPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy")
    @WebResult(name = "Get_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicy")
    @ResponseWrapper(localName = "Get_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyResponse")
    public PolicyGetResult getPolicy(
        @WebParam(name = "PolicyFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        PolicyFilter policyFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy")
    @WebResult(name = "Insert_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicy")
    @ResponseWrapper(localName = "Insert_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyResponse")
    public Integer insertPolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @throws EpicSDKCore202202UpdatePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy")
    @RequestWrapper(localName = "Update_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicy")
    @ResponseWrapper(localName = "Update_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyResponse")
    public void updatePolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202202DeletePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy")
    @RequestWrapper(localName = "Delete_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicy")
    @ResponseWrapper(localName = "Delete_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyResponse")
    public void deletePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeletePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ActivateInactivateMultiCarrierSchedule")
    @RequestWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyActivateInactivateMultiCarrierSchedule")
    @ResponseWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyActivateInactivateMultiCarrierScheduleResponse")
    public void actionPolicyActivateInactivateMultiCarrierSchedule(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202ActionPolicyCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyCancelInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyCancelAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Cancel", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Cancel")
    @WebResult(name = "Action_Policy_CancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_Cancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyCancel")
    @ResponseWrapper(localName = "Action_Policy_CancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyCancelResponse")
    public Integer actionPolicyCancel(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202202GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionCancel", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionCancel")
    @WebResult(name = "Get_Policy_DefaultActionCancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionCancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionCancel")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionCancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionCancelResponse")
    public ArrayOfCancel getPolicyDefaultActionCancel(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202202GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancellationRequestReasonMethod")
    @WebResult(name = "Get_Policy_CancellationRequestReasonMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Get_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancellationRequestReasonMethodResponse")
    public ArrayOfCancel getPolicyCancellationRequestReasonMethod(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancellationRequestReasonMethodGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancellationRequestReasonMethod")
    @RequestWrapper(localName = "Update_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Update_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancellationRequestReasonMethodResponse")
    public void updatePolicyCancellationRequestReasonMethod(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param lineID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates
     * @throws EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates")
    @WebResult(name = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse")
    public ArrayOfChangePolicyEffectiveExpirationDates getPolicyDefaultActionChangePolicyEffectiveExpirationDates(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param changePolicyEffectiveExpirationDatesObject
     * @throws EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangePolicyEffectiveExpirationDates")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyEffectiveExpirationDatesResponse")
    public void actionPolicyChangePolicyEffectiveExpirationDates(
        @WebParam(name = "ChangePolicyEffectiveExpirationDatesObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyProspectiveContractedStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangePolicyProspectiveContractedStatus")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyProspectiveContractedStatus")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyProspectiveContractedStatusResponse")
    public void actionPolicyChangePolicyProspectiveContractedStatus(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param changeServiceSummaryDescriptionObject
     * @param messageHeader
     * @throws EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangeServiceSummaryDescription", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangeServiceSummaryDescription")
    @RequestWrapper(localName = "Action_Policy_ChangeServiceSummaryDescription", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangeServiceSummaryDescription")
    @ResponseWrapper(localName = "Action_Policy_ChangeServiceSummaryDescriptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangeServiceSummaryDescriptionResponse")
    public void actionPolicyChangeServiceSummaryDescription(
        @WebParam(name = "ChangeServiceSummaryDescriptionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param endorseReviseAddLineMidtermObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseAddLineMidterm", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_EndorseReviseAddLineMidterm")
    @WebResult(name = "Action_Policy_EndorseReviseAddLineMidtermResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseAddLineMidterm", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseAddLineMidterm")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseAddLineMidtermResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseAddLineMidtermResponse")
    public ArrayOfint actionPolicyEndorseReviseAddLineMidterm(
        @WebParam(name = "EndorseReviseAddLineMidtermObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param endorseReviseExistingLineObject
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseExistingLine", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_EndorseReviseExistingLine")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseExistingLine", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseExistingLine")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseExistingLineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseExistingLineResponse")
    public void actionPolicyEndorseReviseExistingLine(
        @WebParam(name = "EndorseReviseExistingLineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        EndorseReviseExistingLine endorseReviseExistingLineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssueEndorsementObject
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueNotIssueEndorsement")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssueEndorsementResponse")
    public void actionPolicyIssueNotIssueEndorsement(
        @WebParam(name = "IssueNotIssueEndorsementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssueEndorsement issueNotIssueEndorsementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueNotIssueEndorsement")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssueEndorsementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssueEndorsementResponse")
    public ArrayOfIssueNotIssueEndorsement getPolicyDefaultActionIssueNotIssueEndorsement(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssueEndorsementGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssuePolicyObject
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueNotIssuePolicy")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssuePolicyResponse")
    public void actionPolicyIssueNotIssuePolicy(
        @WebParam(name = "IssueNotIssuePolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssuePolicy issueNotIssuePolicyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueNotIssuePolicy")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssuePolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssuePolicyResponse")
    public ArrayOfIssueNotIssuePolicy getPolicyDefaultActionIssueNotIssuePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssuePolicyGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueCancellationObject
     * @throws EpicSDKCore202202ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueCancellation")
    @RequestWrapper(localName = "Action_Policy_IssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueCancellation")
    @ResponseWrapper(localName = "Action_Policy_IssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueCancellationResponse")
    public void actionPolicyIssueCancellation(
        @WebParam(name = "IssueCancellationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueCancellation issueCancellationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueCancellation")
    @WebResult(name = "Get_Policy_DefaultActionIssueCancellationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueCancellation")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueCancellationResponse")
    public ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellation(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reinstateObject
     * @throws EpicSDKCore202202ActionPolicyReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyReinstateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Reinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Reinstate")
    @RequestWrapper(localName = "Action_Policy_Reinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyReinstate")
    @ResponseWrapper(localName = "Action_Policy_ReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyReinstateResponse")
    public void actionPolicyReinstate(
        @WebParam(name = "ReinstateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Reinstate reinstateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate
     * @throws EpicSDKCore202202GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionReinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionReinstate")
    @WebResult(name = "Get_Policy_DefaultActionReinstateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionReinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionReinstate")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionReinstateResponse")
    public ArrayOfReinstate getPolicyDefaultActionReinstate(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param renewObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202ActionPolicyRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyRenewAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Renew", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Renew")
    @WebResult(name = "Action_Policy_RenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_Renew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyRenew")
    @ResponseWrapper(localName = "Action_Policy_RenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyRenewResponse")
    public Integer actionPolicyRenew(
        @WebParam(name = "RenewObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Renew renewObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew
     * @throws EpicSDKCore202202GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionRenew", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionRenew")
    @WebResult(name = "Get_Policy_DefaultActionRenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionRenew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionRenew")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionRenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionRenewResponse")
    public ArrayOfRenew getPolicyDefaultActionRenew(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionUpdateStageToSubmitted")
    @WebResult(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateStageToSubmittedResponse")
    public ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmitted(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param oSubmissionObject
     * @param messageHeader
     * @throws EpicSDKCore202202ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_UpdateStageToSubmitted")
    @RequestWrapper(localName = "Action_Policy_UpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Action_Policy_UpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateStageToSubmittedResponse")
    public void actionPolicyUpdateStageToSubmitted(
        @WebParam(name = "oSubmissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        UpdateStageToSubmitted oSubmissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_DeleteServiceSummaryRow", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_DeleteServiceSummaryRow")
    @RequestWrapper(localName = "Action_Policy_DeleteServiceSummaryRow", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyDeleteServiceSummaryRow")
    @ResponseWrapper(localName = "Action_Policy_DeleteServiceSummaryRowResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyDeleteServiceSummaryRowResponse")
    public void actionPolicyDeleteServiceSummaryRow(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyDeleteServiceSummaryRowMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyIdList
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateRenewalStage", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionUpdateRenewalStage")
    @WebResult(name = "Get_Policy_DefaultActionUpdateRenewalStageResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateRenewalStage", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateRenewalStage")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateRenewalStageResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateRenewalStageResponse")
    public ArrayOfUpdateRenewalStage getPolicyDefaultActionUpdateRenewalStage(
        @WebParam(name = "PolicyIdList", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ArrayOfint policyIdList,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyDefaultActionUpdateRenewalStageMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param updateRenewalStageList
     * @param changeStageToCode
     * @throws EpicSDKCore202202ActionPolicyUpdateRenewalStageMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateRenewalStageInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateRenewalStageConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionPolicyUpdateRenewalStageAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateRenewalStage", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_UpdateRenewalStage")
    @RequestWrapper(localName = "Action_Policy_UpdateRenewalStage", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateRenewalStage")
    @ResponseWrapper(localName = "Action_Policy_UpdateRenewalStageResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateRenewalStageResponse")
    public void actionPolicyUpdateRenewalStage(
        @WebParam(name = "UpdateRenewalStageList", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ArrayOfUpdateRenewalStage updateRenewalStageList,
        @WebParam(name = "ChangeStageToCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer changeStageToCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionPolicyUpdateRenewalStageAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateRenewalStageConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateRenewalStageInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionPolicyUpdateRenewalStageMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyAdditionalInterestsIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest
     * @throws EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Get_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyAdditionalInterestResponse")
    public ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "CancelPolicyAdditionalInterestsIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancelPolicyAdditionalInterestGetType cancelPolicyAdditionalInterestsIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Insert_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyAdditionalInterestResponse")
    public Integer insertPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyAdditionalInterestResponse")
    public void updatePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalInterestID
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyAdditionalInterestResponse")
    public void deletePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "AdditionalInterestID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer additionalInterestID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyRemarksIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark
     * @throws EpicSDKCore202202GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancelPolicyRemark")
    @WebResult(name = "Get_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyRemarkResponse")
    public ArrayOfRemark getPolicyCancelPolicyRemark(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "CancelPolicyRemarksIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancelPolicyRemarksGetType cancelPolicyRemarksIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy_CancelPolicyRemark")
    @WebResult(name = "Insert_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyRemarkResponse")
    public Integer insertPolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyRemarkResponse")
    public void updatePolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param remarkID
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyRemarkResponse")
    public void deletePolicyCancelPolicyRemark(
        @WebParam(name = "RemarkID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer remarkID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202InsertAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertAttachmentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Insert_Attachment")
    @WebResult(name = "Insert_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachment")
    @ResponseWrapper(localName = "Insert_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachmentResponse")
    public ArrayOfint insertAttachment(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @param attachmentFilterObject
     * @param attachmentSortingObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult
     * @throws EpicSDKCore202202GetAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetAttachmentAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Get_Attachment")
    @WebResult(name = "Get_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachment")
    @ResponseWrapper(localName = "Get_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachmentResponse")
    public AttachmentGetResult getAttachment(
        @WebParam(name = "AttachmentFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentFilter attachmentFilterObject,
        @WebParam(name = "AttachmentSortingObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentSorting attachmentSortingObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Details", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Details")
    @RequestWrapper(localName = "Update_Attachment_Details", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetails")
    @ResponseWrapper(localName = "Update_Attachment_DetailsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetailsResponse")
    public void updateAttachmentDetails(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveFolder", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveFolder")
    @RequestWrapper(localName = "Update_Attachment_MoveFolder", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolder")
    @ResponseWrapper(localName = "Update_Attachment_MoveFolderResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolderResponse")
    public void updateAttachmentMoveFolder(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveAccount")
    @RequestWrapper(localName = "Update_Attachment_MoveAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccount")
    @ResponseWrapper(localName = "Update_Attachment_MoveAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccountResponse")
    public void updateAttachmentMoveAccount(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Binary", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Binary")
    @WebResult(name = "Update_Attachment_BinaryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Update_Attachment_Binary", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinary")
    @ResponseWrapper(localName = "Update_Attachment_BinaryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinaryResponse")
    public Integer updateAttachmentBinary(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202202DeleteAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteAttachmentAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Delete_Attachment")
    @RequestWrapper(localName = "Delete_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachment")
    @ResponseWrapper(localName = "Delete_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachmentResponse")
    public void deleteAttachment(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202202ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionAttachmentReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Attachment_Reactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Action_Attachment_Reactivate")
    @RequestWrapper(localName = "Action_Attachment_Reactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivate")
    @ResponseWrapper(localName = "Action_Attachment_ReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivateResponse")
    public void actionAttachmentReactivate(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionAttachmentReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202202GetTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction")
    @WebResult(name = "Get_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransaction")
    @ResponseWrapper(localName = "Get_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionResponse")
    public TransactionGetResult getTransaction(
        @WebParam(name = "TransactionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        TransactionFilter transactionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202InsertTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Insert_Transaction")
    @WebResult(name = "Insert_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Insert_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.InsertTransaction")
    @ResponseWrapper(localName = "Insert_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.InsertTransactionResponse")
    public ArrayOfint insertTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @throws EpicSDKCore202202UpdateTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Update_Transaction")
    @RequestWrapper(localName = "Update_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.UpdateTransaction")
    @ResponseWrapper(localName = "Update_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.UpdateTransactionResponse")
    public void updateTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @param installmentType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202202GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultInstallments", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultInstallments")
    @WebResult(name = "Get_Transaction_DefaultInstallmentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultInstallments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultInstallments")
    @ResponseWrapper(localName = "Get_Transaction_DefaultInstallmentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultInstallmentsResponse")
    public TransactionGetResult getTransactionDefaultInstallments(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Transaction transactionObject,
        @WebParam(name = "InstallmentType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        TransactionGetInstallmentType installmentType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultProducerBrokerCommissions", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultProducerBrokerCommissions")
    @WebResult(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissions", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultProducerBrokerCommissions")
    @ResponseWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissionsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultProducerBrokerCommissionsResponse")
    public TransactionGetResult getTransactionDefaultProducerBrokerCommissions(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable
     * @throws EpicSDKCore202202GetTransactionReceivablesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionReceivablesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionReceivablesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionReceivablesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_Receivables", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_Receivables")
    @WebResult(name = "Get_Transaction_ReceivablesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_Receivables", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionReceivables")
    @ResponseWrapper(localName = "Get_Transaction_ReceivablesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionReceivablesResponse")
    public ArrayOfReceivable getTransactionReceivables(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionReceivablesAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionReceivablesConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionReceivablesInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionReceivablesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param accountsReceivableWriteOffObject
     * @throws EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AccountsReceivableWriteOff", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_AccountsReceivableWriteOff")
    @RequestWrapper(localName = "Action_Transaction_AccountsReceivableWriteOff", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionAccountsReceivableWriteOff")
    @ResponseWrapper(localName = "Action_Transaction_AccountsReceivableWriteOffResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionAccountsReceivableWriteOffResponse")
    public void actionTransactionAccountsReceivableWriteOff(
        @WebParam(name = "AccountsReceivableWriteOffObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        AccountsReceivableWriteOff accountsReceivableWriteOffObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param applyCreditsToDebitsObject
     * @throws EpicSDKCore202202ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_ApplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_ApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_ApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionApplyCreditsToDebitsResponse")
    public void actionTransactionApplyCreditsToDebits(
        @WebParam(name = "ApplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        ApplyCreditsToDebits applyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param associatedAccountID
     * @param messageHeader
     * @param associatedAccountTypeCode
     * @param agencyCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits
     * @throws EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionApplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionApplyCreditsToDebitsResponse")
    public ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebits(
        @WebParam(name = "AssociatedAccountTypeCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        String associatedAccountTypeCode,
        @WebParam(name = "AssociatedAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer associatedAccountID,
        @WebParam(name = "AgencyCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        String agencyCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param financeTransactionObject
     * @throws EpicSDKCore202202ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_FinanceTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_FinanceTransaction")
    @RequestWrapper(localName = "Action_Transaction_FinanceTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionFinanceTransaction")
    @ResponseWrapper(localName = "Action_Transaction_FinanceTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionFinanceTransactionResponse")
    public void actionTransactionFinanceTransaction(
        @WebParam(name = "FinanceTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        FinanceTransaction financeTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @throws EpicSDKCore202202ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_MoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_MoveTransaction")
    @RequestWrapper(localName = "Action_Transaction_MoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionMoveTransaction")
    @ResponseWrapper(localName = "Action_Transaction_MoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionMoveTransactionResponse")
    public void actionTransactionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction
     * @throws EpicSDKCore202202GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionMoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionMoveTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionMoveTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionMoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionMoveTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionMoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionMoveTransactionResponse")
    public ArrayOfMoveTransaction getTransactionDefaultActionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction
     * @throws EpicSDKCore202202GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionReverseTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionReverseTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionReverseTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionReverseTransactionResponse")
    public ArrayOfReverseTransaction getTransactionDefaultActionReverseTransaction(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reverseTransactionObject
     * @throws EpicSDKCore202202ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_ReverseTransaction")
    @RequestWrapper(localName = "Action_Transaction_ReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionReverseTransaction")
    @ResponseWrapper(localName = "Action_Transaction_ReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionReverseTransactionResponse")
    public void actionTransactionReverseTransaction(
        @WebParam(name = "ReverseTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        ReverseTransaction reverseTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param unapplyCreditsToDebitsObject
     * @param messageHeader
     * @throws EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_UnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_UnapplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_UnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_UnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionUnapplyCreditsToDebitsResponse")
    public void actionTransactionUnapplyCreditsToDebits(
        @WebParam(name = "UnapplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        UnapplyCreditsToDebits unapplyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionDetailNumber
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits
     * @throws EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionUnapplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionUnapplyCreditsToDebitsResponse")
    public ArrayOfUnapplyCreditsToDebits getTransactionDefaultActionUnapplyCreditsToDebits(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "TransactionDetailNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionDetailNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voidPaymentObject
     * @param messageHeader
     * @throws EpicSDKCore202202ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_VoidPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_VoidPayment")
    @RequestWrapper(localName = "Action_Transaction_VoidPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionVoidPayment")
    @ResponseWrapper(localName = "Action_Transaction_VoidPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionVoidPaymentResponse")
    public void actionTransactionVoidPayment(
        @WebParam(name = "VoidPaymentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        VoidPayment voidPaymentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission
     * @throws EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionAdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionAdjustCommission")
    @WebResult(name = "Get_Transaction_DefaultActionAdjustCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionAdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionAdjustCommission")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionAdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionAdjustCommissionResponse")
    public ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommission(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustCommissionObject
     * @throws EpicSDKCore202202ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_AdjustCommission")
    @RequestWrapper(localName = "Action_Transaction_AdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionAdjustCommission")
    @ResponseWrapper(localName = "Action_Transaction_AdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionAdjustCommissionResponse")
    public void actionTransactionAdjustCommission(
        @WebParam(name = "AdjustCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        AdjustCommission adjustCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium
     * @throws EpicSDKCore202202GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionRevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionRevisePremium")
    @WebResult(name = "Get_Transaction_DefaultActionRevisePremiumResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionRevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionRevisePremium")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionRevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionRevisePremiumResponse")
    public ArrayOfRevisePremium getTransactionDefaultActionRevisePremium(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param revisePremiumObject
     * @throws EpicSDKCore202202ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_RevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_RevisePremium")
    @RequestWrapper(localName = "Action_Transaction_RevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionRevisePremium")
    @ResponseWrapper(localName = "Action_Transaction_RevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionRevisePremiumResponse")
    public void actionTransactionRevisePremium(
        @WebParam(name = "RevisePremiumObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        RevisePremium revisePremiumObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee
     * @throws EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionGenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionGenerateTaxFee")
    @WebResult(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionGenerateTaxFee")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionGenerateTaxFeeResponse")
    public ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFee(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param generateTaxFeeObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_GenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_GenerateTaxFee")
    @WebResult(name = "Action_Transaction_GenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Action_Transaction_GenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionGenerateTaxFee")
    @ResponseWrapper(localName = "Action_Transaction_GenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionGenerateTaxFeeResponse")
    public ArrayOfint actionTransactionGenerateTaxFee(
        @WebParam(name = "GenerateTaxFeeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        GenerateTaxFee generateTaxFeeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param balanceTransferObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202ActionTransactionBalanceTransferConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionBalanceTransferInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionBalanceTransferMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionBalanceTransferAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_BalanceTransfer", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_BalanceTransfer")
    @WebResult(name = "Action_Transaction_BalanceTransferResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Action_Transaction_BalanceTransfer", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionBalanceTransfer")
    @ResponseWrapper(localName = "Action_Transaction_BalanceTransferResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionBalanceTransferResponse")
    public Integer actionTransactionBalanceTransfer(
        @WebParam(name = "BalanceTransferObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        BalanceTransfer balanceTransferObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionBalanceTransferAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionBalanceTransferConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionBalanceTransferInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionBalanceTransferMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param placeOnHold
     * @param messageHeader
     * @param transactionID
     * @throws EpicSDKCore202202ActionTransactionHoldResumePaymentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionHoldResumePaymentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionHoldResumePaymentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionHoldResumePaymentsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_HoldResumePayments", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_HoldResumePayments")
    @RequestWrapper(localName = "Action_Transaction_HoldResumePayments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionHoldResumePayments")
    @ResponseWrapper(localName = "Action_Transaction_HoldResumePaymentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionHoldResumePaymentsResponse")
    public void actionTransactionHoldResumePayments(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "PlaceOnHold", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Boolean placeOnHold,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionHoldResumePaymentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionHoldResumePaymentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionHoldResumePaymentsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionHoldResumePaymentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reverseBalanceTransferObject
     * @param messageHeader
     * @throws EpicSDKCore202202ActionTransactionReverseBalanceTransferAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseBalanceTransferConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseBalanceTransferInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionReverseBalanceTransferMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseBalanceTransfer", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_ReverseBalanceTransfer")
    @RequestWrapper(localName = "Action_Transaction_ReverseBalanceTransfer", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionReverseBalanceTransfer")
    @ResponseWrapper(localName = "Action_Transaction_ReverseBalanceTransferResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionReverseBalanceTransferResponse")
    public void actionTransactionReverseBalanceTransfer(
        @WebParam(name = "ReverseBalanceTransferObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        ReverseBalanceTransfer reverseBalanceTransferObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionReverseBalanceTransferAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseBalanceTransferConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseBalanceTransferInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionReverseBalanceTransferMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfDiscount
     * @throws EpicSDKCore202202GetTransactionDefaultActionDiscountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionDiscountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionDiscountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionDiscountAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionDiscount", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionDiscount")
    @WebResult(name = "Get_Transaction_DefaultActionDiscountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionDiscount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionDiscount")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionDiscountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionDiscountResponse")
    public ArrayOfDiscount getTransactionDefaultActionDiscount(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionDiscountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionDiscountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionDiscountInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionDiscountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param discountObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202ActionTransactionDiscountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionDiscountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionDiscountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionDiscountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_Discount", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_Discount")
    @WebResult(name = "Action_Transaction_DiscountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Action_Transaction_Discount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionDiscount")
    @ResponseWrapper(localName = "Action_Transaction_DiscountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionDiscountResponse")
    public Integer actionTransactionDiscount(
        @WebParam(name = "DiscountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Discount discountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionDiscountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionDiscountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionDiscountInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionDiscountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfModifyRevenueDeferralSchedule
     * @throws EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionModifyRevenueDeferralSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Get_Transaction_DefaultActionModifyRevenueDeferralSchedule")
    @WebResult(name = "Get_Transaction_DefaultActionModifyRevenueDeferralScheduleResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionModifyRevenueDeferralSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionModifyRevenueDeferralSchedule")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionModifyRevenueDeferralScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.GetTransactionDefaultActionModifyRevenueDeferralScheduleResponse")
    public ArrayOfModifyRevenueDeferralSchedule getTransactionDefaultActionModifyRevenueDeferralSchedule(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionDefaultActionModifyRevenueDeferralScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param modifyRevenueDeferralScheduleObject
     * @throws EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ModifyRevenueDeferralSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2022/02/ITransaction_2022_02/Action_Transaction_ModifyRevenueDeferralSchedule")
    @RequestWrapper(localName = "Action_Transaction_ModifyRevenueDeferralSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionModifyRevenueDeferralSchedule")
    @ResponseWrapper(localName = "Action_Transaction_ModifyRevenueDeferralScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/", className = "com.appliedsystems.webservices.epic.sdk._2022._02.ActionTransactionModifyRevenueDeferralScheduleResponse")
    public void actionTransactionModifyRevenueDeferralSchedule(
        @WebParam(name = "ModifyRevenueDeferralScheduleObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/02/")
        ModifyRevenueDeferralSchedule modifyRevenueDeferralScheduleObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionTransactionModifyRevenueDeferralScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim")
    @WebResult(name = "Insert_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaim")
    @ResponseWrapper(localName = "Insert_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimResponse")
    public Integer insertClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param claimSummaryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult
     * @throws EpicSDKCore202202GetClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim")
    @WebResult(name = "Get_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaim")
    @ResponseWrapper(localName = "Get_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimResponse")
    public ClaimSummaryGetResult getClaim(
        @WebParam(name = "ClaimSummaryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ClaimSummaryFilter claimSummaryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @throws EpicSDKCore202202DeleteClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim")
    @RequestWrapper(localName = "Delete_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaim")
    @ResponseWrapper(localName = "Delete_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimResponse")
    public void deleteClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @throws EpicSDKCore202202UpdateClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim")
    @RequestWrapper(localName = "Update_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaim")
    @ResponseWrapper(localName = "Update_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimResponse")
    public void updateClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty
     * @throws EpicSDKCore202202GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_AdditionalParty")
    @WebResult(name = "Get_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalParty")
    @ResponseWrapper(localName = "Get_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalPartyResponse")
    public ArrayOfAdditionalParty getClaimAdditionalParty(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalPartyGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param additionalPartyID
     * @param messageHeader
     * @throws EpicSDKCore202202DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_AdditionalParty")
    @RequestWrapper(localName = "Delete_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalParty")
    @ResponseWrapper(localName = "Delete_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalPartyResponse")
    public void deleteClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalPartyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_AdditionalParty")
    @WebResult(name = "Insert_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalParty")
    @ResponseWrapper(localName = "Insert_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalPartyResponse")
    public Integer insertClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @throws EpicSDKCore202202UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_AdditionalParty")
    @RequestWrapper(localName = "Update_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalParty")
    @ResponseWrapper(localName = "Update_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalPartyResponse")
    public void updateClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor
     * @throws EpicSDKCore202202GetClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Adjustor")
    @WebResult(name = "Get_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustor")
    @ResponseWrapper(localName = "Get_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustorResponse")
    public ArrayOfAdjustor getClaimAdjustor(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdjustorGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertClaimAdjustorConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_Adjustor")
    @WebResult(name = "Insert_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustor")
    @ResponseWrapper(localName = "Insert_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustorResponse")
    public Integer insertClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @throws EpicSDKCore202202UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Adjustor")
    @RequestWrapper(localName = "Update_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustor")
    @ResponseWrapper(localName = "Update_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustorResponse")
    public void updateClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param adjustorID
     * @param messageHeader
     * @throws EpicSDKCore202202DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_Adjustor")
    @RequestWrapper(localName = "Delete_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustor")
    @ResponseWrapper(localName = "Delete_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustorResponse")
    public void deleteClaimAdjustor(
        @WebParam(name = "AdjustorID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer adjustorID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact
     * @throws EpicSDKCore202202GetClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimInsuredContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_InsuredContact")
    @WebResult(name = "Get_Claim_InsuredContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContact")
    @ResponseWrapper(localName = "Get_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContactResponse")
    public ArrayOfInsuredContact getClaimInsuredContact(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param insuredContactObject
     * @throws EpicSDKCore202202UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_InsuredContact")
    @RequestWrapper(localName = "Update_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContact")
    @ResponseWrapper(localName = "Update_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContactResponse")
    public void updateClaimInsuredContact(
        @WebParam(name = "InsuredContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        InsuredContact insuredContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation
     * @throws EpicSDKCore202202GetClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimLitigationInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Litigation")
    @WebResult(name = "Get_Claim_LitigationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigation")
    @ResponseWrapper(localName = "Get_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigationResponse")
    public ArrayOfLitigation getClaimLitigation(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param litigationObject
     * @throws EpicSDKCore202202UpdateClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimLitigationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Litigation")
    @RequestWrapper(localName = "Update_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigation")
    @ResponseWrapper(localName = "Update_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigationResponse")
    public void updateClaimLitigation(
        @WebParam(name = "LitigationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Litigation litigationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense
     * @throws EpicSDKCore202202GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_PaymentExpense")
    @WebResult(name = "Get_Claim_PaymentExpenseResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpense")
    @ResponseWrapper(localName = "Get_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpenseResponse")
    public ArrayOfPaymentExpense getClaimPaymentExpense(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param paymentExpenseObject
     * @throws EpicSDKCore202202UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_PaymentExpense")
    @RequestWrapper(localName = "Update_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpense")
    @ResponseWrapper(localName = "Update_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpenseResponse")
    public void updateClaimPaymentExpense(
        @WebParam(name = "PaymentExpenseObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PaymentExpense paymentExpenseObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts
     * @throws EpicSDKCore202202GetClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimServicingContactsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetClaimServicingContactsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_ServicingContacts")
    @WebResult(name = "Get_Claim_ServicingContactsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContacts")
    @ResponseWrapper(localName = "Get_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContactsResponse")
    public ArrayOfServicingContacts getClaimServicingContacts(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param servicingContactObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_ServicingContacts")
    @RequestWrapper(localName = "Update_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContacts")
    @ResponseWrapper(localName = "Update_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContactsResponse")
    public void updateClaimServicingContacts(
        @WebParam(name = "ServicingContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ServicingContacts servicingContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param closedDate
     * @param claimID
     * @throws EpicSDKCore202202ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Claim_CloseReopenClaim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Action_Claim_CloseReopenClaim")
    @RequestWrapper(localName = "Action_Claim_CloseReopenClaim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaim")
    @ResponseWrapper(localName = "Action_Claim_CloseReopenClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaimResponse")
    public void actionClaimCloseReopenClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "ClosedDate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        XMLGregorianCalendar closedDate,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param searchTerms
     * @param lookupTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup
     * @throws EpicSDKCore202202GetLookupMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLookupAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLookupInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetLookupConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Lookup", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/ILookup/Get_Lookup")
    @WebResult(name = "Get_LookupResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Lookup", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookup")
    @ResponseWrapper(localName = "Get_LookupResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookupResponse")
    public ArrayOfLookup getLookup(
        @WebParam(name = "LookupTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        LookupTypes lookupTypeObject,
        @WebParam(name = "SearchTerms", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ArrayOfstring searchTerms,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetLookupMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param optionTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType
     * @throws EpicSDKCore202202GetOptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetOptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetOptionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetOptionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Option", action = "http://webservices.appliedsystems.com/epic/sdk/2009/07/IOption/Get_Option")
    @WebResult(name = "Get_OptionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
    @RequestWrapper(localName = "Get_Option", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOption")
    @ResponseWrapper(localName = "Get_OptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOptionResponse")
    public ArrayOfOptionType getOption(
        @WebParam(name = "OptionTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
        OptionTypes optionTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetOptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @param receiptID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_Receipt")
    @WebResult(name = "Get_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptResponse")
    public ReceiptGetResult getGeneralLedgerReceipt(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetType searchType,
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Insert_GeneralLedger_Receipt")
    @WebResult(name = "Insert_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceiptResponse")
    public Integer insertGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Update_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Update_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceiptResponse")
    public void updateGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202202DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Delete_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Delete_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceiptResponse")
    public void deleteGeneralLedgerReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @param detailItemToBeInsertedObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultApplyCreditsToDebits(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "DetailItemToBeInsertedObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        DetailItem detailItemToBeInsertedObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPayments")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultProcessOutstandingPayments(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptFinalizeReceipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptFinalizeReceipt")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceipt")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceiptResponse")
    public void actionGeneralLedgerReceiptFinalizeReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReceiptTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param journalEntryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryFilter journalEntryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntryDefaultDefaultEntry(
        @WebParam(name = "JournalEntryDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryDefaultEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Insert_GeneralLedger_JournalEntry")
    @WebResult(name = "Insert_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Insert_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntryResponse")
    public ArrayOfint insertGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @throws EpicSDKCore202202UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Update_GeneralLedger_JournalEntry")
    @RequestWrapper(localName = "Update_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Update_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntryResponse")
    public void updateGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_JournalEntryTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerJournalEntryTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryVoidObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryVoid")
    @WebResult(name = "Action_GeneralLedger_JournalEntryVoidResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoidResponse")
    public Integer actionGeneralLedgerJournalEntryVoid(
        @WebParam(name = "JournalEntryVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryVoid journalEntryVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param journalEntryID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntrySubmit", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntrySubmit")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntrySubmit", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmit")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntrySubmitResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmitResponse")
    public void actionGeneralLedgerJournalEntrySubmit(
        @WebParam(name = "JournalEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_DisbursementTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerDisbursementTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementPayVouchers")
    @WebResult(name = "Action_GeneralLedger_DisbursementPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerDisbursementPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementVoidObject
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoidResponse")
    public void actionGeneralLedgerDisbursementVoid(
        @WebParam(name = "DisbursementVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementVoid disbursementVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param disbursementID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_Disbursement")
    @WebResult(name = "Get_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementResponse")
    public DisbursementGetResult getGeneralLedgerDisbursement(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetType searchType,
        @WebParam(name = "DisbursementID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Insert_GeneralLedger_Disbursement")
    @WebResult(name = "Insert_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Insert_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursementResponse")
    public Integer insertGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @throws EpicSDKCore202202UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Update_GeneralLedger_Disbursement")
    @RequestWrapper(localName = "Update_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Update_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursementResponse")
    public void updateGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param disbursementBankAccountNumberCode
     * @param messageHeader
     * @param disbursementBankSubAccountNumberCode
     * @param disbursementDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntryResponse")
    public DisbursementGetResult getGeneralLedgerDisbursementDefaultDefaultEntry(
        @WebParam(name = "DisbursementDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementDefaultEntryID,
        @WebParam(name = "DisbursementBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankAccountNumberCode,
        @WebParam(name = "DisbursementBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankSubAccountNumberCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerVoucherTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherPayVouchers")
    @WebResult(name = "Action_GeneralLedger_VoucherPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerVoucherPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerVoucherDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherVoidObject
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoidResponse")
    public void actionGeneralLedgerVoucherVoid(
        @WebParam(name = "VoucherVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherVoid voucherVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Insert_GeneralLedger_Voucher")
    @WebResult(name = "Insert_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Insert_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucherResponse")
    public Integer insertGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @throws EpicSDKCore202202UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Update_GeneralLedger_Voucher")
    @RequestWrapper(localName = "Update_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Update_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucherResponse")
    public void updateGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param voucherID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_Voucher")
    @WebResult(name = "Get_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherResponse")
    public VoucherGetResult getGeneralLedgerVoucher(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetType searchType,
        @WebParam(name = "VoucherID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voucherDefaultEntryID
     * @param voucherAccountingMonth
     * @param messageHeader
     * @param voucherBankSubAccountNumberCode
     * @param voucherBankAccountNumberCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntryResponse")
    public VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntry(
        @WebParam(name = "VoucherDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherDefaultEntryID,
        @WebParam(name = "VoucherBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankAccountNumberCode,
        @WebParam(name = "VoucherBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankSubAccountNumberCode,
        @WebParam(name = "VoucherAccountingMonth", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherAccountingMonth,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param id
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Get_GeneralLedger_Activity")
    @WebResult(name = "Get_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivity")
    @ResponseWrapper(localName = "Get_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivityResponse")
    public ActivityGetResult getGeneralLedgerActivity(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetType searchType,
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Insert_GeneralLedger_Activity")
    @WebResult(name = "Insert_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivity")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivityResponse")
    public Integer insertGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Update_GeneralLedger_Activity")
    @RequestWrapper(localName = "Update_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivity")
    @ResponseWrapper(localName = "Update_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivityResponse")
    public void updateGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param directBillCommissionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommissionResponse")
    public DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "DirectBillCommissionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommissionFilter directBillCommissionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommissionResponse")
    public ArrayOfint insertGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void updateGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionID
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void deleteGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationDirectBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param lDepartments
     * @param lEntityLookupCodes
     * @param messageHeader
     * @param lBranches
     * @param lProfitCenters
     * @param lAgencies
     * @param sCompareByType
     * @param lIssuingCompanies
     * @param sClientOrPolicyNumber
     * @param fIncludeHistory
     * @param lLineOfBusiness
     * @param sEntityType
     * @param sDescriptionType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
    public RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies(
        @WebParam(name = "sEntityType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sEntityType,
        @WebParam(name = "lEntityLookupCodes", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lEntityLookupCodes,
        @WebParam(name = "sDescriptionType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sDescriptionType,
        @WebParam(name = "sCompareByType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sCompareByType,
        @WebParam(name = "sClientOrPolicyNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sClientOrPolicyNumber,
        @WebParam(name = "lAgencies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lAgencies,
        @WebParam(name = "lBranches", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lBranches,
        @WebParam(name = "lDepartments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lDepartments,
        @WebParam(name = "lProfitCenters", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lProfitCenters,
        @WebParam(name = "lIssuingCompanies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lIssuingCompanies,
        @WebParam(name = "lLineOfBusiness", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lLineOfBusiness,
        @WebParam(name = "fIncludeHistory", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean fIncludeHistory,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReconciliationBankTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankReopenStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankReopenStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatementResponse")
    public void actionGeneralLedgerReconciliationBankReopenStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationBankFinalizeStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param bankFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Get_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBankResponse")
    public BankGetResult getGeneralLedgerReconciliationBank(
        @WebParam(name = "BankFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankFilter bankFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBankResponse")
    public Integer insertGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBankResponse")
    public void updateGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reconciliationBankID
     * @param messageHeader
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBankResponse")
    public void deleteGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationBankID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType
     * @throws EpicSDKCore202202GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Get_Policy_PolicyLineType")
    @WebResult(name = "Get_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineType")
    @ResponseWrapper(localName = "Get_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineTypeResponse")
    public ArrayOfPolicyLineType getPolicyPolicyLineType(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineTypeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyLineTypeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyLineType")
    @WebResult(name = "Insert_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineType")
    @ResponseWrapper(localName = "Insert_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineTypeResponse")
    public Integer insertPolicyPolicyLineType(
        @WebParam(name = "PolicyLineTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineType policyLineTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyStatusObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyStatus")
    @WebResult(name = "Insert_Policy_PolicyStatusResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatus")
    @ResponseWrapper(localName = "Insert_Policy_PolicyStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatusResponse")
    public Integer insertPolicyPolicyStatus(
        @WebParam(name = "PolicyStatusObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyStatus policyStatusObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode
     * @throws EpicSDKCore202202GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Get_Transaction_TransactionCode")
    @WebResult(name = "Get_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Get_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCode")
    @ResponseWrapper(localName = "Get_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCodeResponse")
    public ArrayOfTransactionCode getTransactionTransactionCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionCodeObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Insert_Transaction_TransactionCode")
    @WebResult(name = "Insert_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Insert_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCode")
    @ResponseWrapper(localName = "Insert_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCodeResponse")
    public Integer insertTransactionTransactionCode(
        @WebParam(name = "TransactionCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCode transactionCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult
     * @throws EpicSDKCore202202GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_SalesTeams", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IConfigureSalesTeam/Get_Configure_SalesTeams")
    @WebResult(name = "Get_Configure_SalesTeamsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_Configure_SalesTeams", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeams")
    @ResponseWrapper(localName = "Get_Configure_SalesTeamsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeamsResponse")
    public SalesTeamGetResult getConfigureSalesTeams(
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Insert_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccountResponse")
    public Integer insertGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountResponse")
    public ChartOfAccountGetResult getGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccountGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean includeInactive,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param chartOfAccountObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Update_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Update_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccountResponse")
    public void updateGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @throws EpicSDKCore202202DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Delete_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccountResponse")
    public void deleteGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationMethodID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationMethod")
    @WebResult(name = "Get_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethodResponse")
    public AllocationMethodsGetResult getGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationEntriesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationMethod")
    @WebResult(name = "Insert_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethodResponse")
    public Integer insertGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethodResponse")
    public void updateGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationMethodID
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethodResponse")
    public void deleteGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationStructureGroupingsID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Get_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGroupingResponse")
    public AllocationStructureGroupingsGetResult getGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGroupingsGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Insert_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGroupingResponse")
    public Integer insertGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGroupingResponse")
    public void updateGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202202UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationStructureGroupingsID
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGroupingResponse")
    public void deleteGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202202DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccountDefineBankAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param bankAccountObject
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountDefineBankAccount")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public void actionGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "BankAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        BankAccount bankAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param ignoreNonZeroBalance
     * @param chartOfAccountID
     * @param shouldInactivateReactivateSubaccounts
     * @param ignoreSubAccountNonZeroBalances
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountInactivateReactivate")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivate")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivateResponse")
    public void actionGeneralLedgerChartOfAccountInactivateReactivate(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "IgnoreNonZeroBalance", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreNonZeroBalance,
        @WebParam(name = "ShouldInactivateReactivateSubaccounts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean shouldInactivateReactivateSubaccounts,
        @WebParam(name = "IgnoreSubAccountNonZeroBalances", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreSubAccountNonZeroBalances,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202202ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode
     * @throws EpicSDKCore202202GetActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityActivityCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetActivityActivityCodeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Get_Activity_ActivityCode")
    @WebResult(name = "Get_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Get_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCode")
    @ResponseWrapper(localName = "Get_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCodeResponse")
    public ArrayOfActivityCode getActivityActivityCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param activityCodeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202202InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Insert_Activity_ActivityCode")
    @WebResult(name = "Insert_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Insert_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCode")
    @ResponseWrapper(localName = "Insert_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCodeResponse")
    public Integer insertActivityActivityCode(
        @WebParam(name = "ActivityCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCode activityCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202202InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202202InsertActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202202InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult
     * @throws EpicSDKCore202202GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202202GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_GovernmentTaxFeeRate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureTaxFeeRate/Get_Configure_GovernmentTaxFeeRate")
    @WebResult(name = "Get_Configure_GovernmentTaxFeeRateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Configure_GovernmentTaxFeeRate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRate")
    @ResponseWrapper(localName = "Get_Configure_GovernmentTaxFeeRateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRateResponse")
    public TaxFeeRateGetResult getConfigureGovernmentTaxFeeRate(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TaxFeeRatesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202202GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
    ;

}
