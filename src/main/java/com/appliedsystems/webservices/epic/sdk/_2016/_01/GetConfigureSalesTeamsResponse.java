
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Configure_SalesTeamsResult" type="{http://schemas.appliedsystems.com/epic/sdk/2016/01/_get/}SalesTeamGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getConfigureSalesTeamsResult"
})
@XmlRootElement(name = "Get_Configure_SalesTeamsResponse")
public class GetConfigureSalesTeamsResponse {

    @XmlElement(name = "Get_Configure_SalesTeamsResult", nillable = true)
    protected SalesTeamGetResult getConfigureSalesTeamsResult;

    /**
     * Gets the value of the getConfigureSalesTeamsResult property.
     * 
     * @return
     *     possible object is
     *     {@link SalesTeamGetResult }
     *     
     */
    public SalesTeamGetResult getGetConfigureSalesTeamsResult() {
        return getConfigureSalesTeamsResult;
    }

    /**
     * Sets the value of the getConfigureSalesTeamsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesTeamGetResult }
     *     
     */
    public void setGetConfigureSalesTeamsResult(SalesTeamGetResult value) {
        this.getConfigureSalesTeamsResult = value;
    }

}
