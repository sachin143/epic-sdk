
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ActivityGetType" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GetLimitType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ActivityGetLimitType" minOccurs="0"/>
 *         &lt;element name="FilterType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ActivityGetFilterType" minOccurs="0"/>
 *         &lt;element name="FilterField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}ActivityComparisonType" minOccurs="0"/>
 *         &lt;element name="FilterField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchType",
    "id",
    "getLimitType",
    "filterType",
    "filterField1",
    "comparisonType",
    "filterField2",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_Activity")
public class GetGeneralLedgerActivity {

    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected ActivityGetType searchType;
    @XmlElement(name = "ID")
    protected Integer id;
    @XmlElement(name = "GetLimitType")
    @XmlSchemaType(name = "string")
    protected ActivityGetLimitType getLimitType;
    @XmlElement(name = "FilterType")
    @XmlSchemaType(name = "string")
    protected ActivityGetFilterType filterType;
    @XmlElement(name = "FilterField1", nillable = true)
    protected String filterField1;
    @XmlElement(name = "ComparisonType")
    @XmlSchemaType(name = "string")
    protected ActivityComparisonType comparisonType;
    @XmlElement(name = "FilterField2", nillable = true)
    protected String filterField2;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityGetType }
     *     
     */
    public ActivityGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityGetType }
     *     
     */
    public void setSearchType(ActivityGetType value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setID(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the getLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityGetLimitType }
     *     
     */
    public ActivityGetLimitType getGetLimitType() {
        return getLimitType;
    }

    /**
     * Sets the value of the getLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityGetLimitType }
     *     
     */
    public void setGetLimitType(ActivityGetLimitType value) {
        this.getLimitType = value;
    }

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityGetFilterType }
     *     
     */
    public ActivityGetFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityGetFilterType }
     *     
     */
    public void setFilterType(ActivityGetFilterType value) {
        this.filterType = value;
    }

    /**
     * Gets the value of the filterField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField1() {
        return filterField1;
    }

    /**
     * Sets the value of the filterField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField1(String value) {
        this.filterField1 = value;
    }

    /**
     * Gets the value of the comparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityComparisonType }
     *     
     */
    public ActivityComparisonType getComparisonType() {
        return comparisonType;
    }

    /**
     * Sets the value of the comparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityComparisonType }
     *     
     */
    public void setComparisonType(ActivityComparisonType value) {
        this.comparisonType = value;
    }

    /**
     * Gets the value of the filterField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField2() {
        return filterField2;
    }

    /**
     * Sets the value of the filterField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField2(String value) {
        this.filterField2 = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
