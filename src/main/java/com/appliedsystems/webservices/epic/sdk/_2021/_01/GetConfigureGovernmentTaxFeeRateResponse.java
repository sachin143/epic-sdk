
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Configure_GovernmentTaxFeeRateResult" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/}TaxFeeRateGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getConfigureGovernmentTaxFeeRateResult"
})
@XmlRootElement(name = "Get_Configure_GovernmentTaxFeeRateResponse")
public class GetConfigureGovernmentTaxFeeRateResponse {

    @XmlElement(name = "Get_Configure_GovernmentTaxFeeRateResult", nillable = true)
    protected TaxFeeRateGetResult getConfigureGovernmentTaxFeeRateResult;

    /**
     * Gets the value of the getConfigureGovernmentTaxFeeRateResult property.
     * 
     * @return
     *     possible object is
     *     {@link TaxFeeRateGetResult }
     *     
     */
    public TaxFeeRateGetResult getGetConfigureGovernmentTaxFeeRateResult() {
        return getConfigureGovernmentTaxFeeRateResult;
    }

    /**
     * Sets the value of the getConfigureGovernmentTaxFeeRateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxFeeRateGetResult }
     *     
     */
    public void setGetConfigureGovernmentTaxFeeRateResult(TaxFeeRateGetResult value) {
        this.getConfigureGovernmentTaxFeeRateResult = value;
    }

}
