
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DisbursementVoidObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_disbursement/_action/}DisbursementVoid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disbursementVoidObject"
})
@XmlRootElement(name = "Action_GeneralLedger_DisbursementVoid")
public class ActionGeneralLedgerDisbursementVoid {

    @XmlElement(name = "DisbursementVoidObject", nillable = true)
    protected DisbursementVoid disbursementVoidObject;

    /**
     * Gets the value of the disbursementVoidObject property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementVoid }
     *     
     */
    public DisbursementVoid getDisbursementVoidObject() {
        return disbursementVoidObject;
    }

    /**
     * Sets the value of the disbursementVoidObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementVoid }
     *     
     */
    public void setDisbursementVoidObject(DisbursementVoid value) {
        this.disbursementVoidObject = value;
    }

}
