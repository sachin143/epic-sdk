
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionReverseTransactionResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}ArrayOfReverseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionReverseTransactionResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionReverseTransactionResponse")
public class GetTransactionDefaultActionReverseTransactionResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionReverseTransactionResult", nillable = true)
    protected ArrayOfReverseTransaction getTransactionDefaultActionReverseTransactionResult;

    /**
     * Gets the value of the getTransactionDefaultActionReverseTransactionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReverseTransaction }
     *     
     */
    public ArrayOfReverseTransaction getGetTransactionDefaultActionReverseTransactionResult() {
        return getTransactionDefaultActionReverseTransactionResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionReverseTransactionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReverseTransaction }
     *     
     */
    public void setGetTransactionDefaultActionReverseTransactionResult(ArrayOfReverseTransaction value) {
        this.getTransactionDefaultActionReverseTransactionResult = value;
    }

}
