
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGrouping;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationStructureGroupingsObject" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}AllocationStructureGrouping" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "allocationStructureGroupingsObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_AllocationStructureGrouping")
public class InsertGeneralLedgerAllocationStructureGrouping {

    @XmlElement(name = "AllocationStructureGroupingsObject", nillable = true)
    protected AllocationStructureGrouping allocationStructureGroupingsObject;

    /**
     * Gets the value of the allocationStructureGroupingsObject property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationStructureGrouping }
     *     
     */
    public AllocationStructureGrouping getAllocationStructureGroupingsObject() {
        return allocationStructureGroupingsObject;
    }

    /**
     * Sets the value of the allocationStructureGroupingsObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationStructureGrouping }
     *     
     */
    public void setAllocationStructureGroupingsObject(AllocationStructureGrouping value) {
        this.allocationStructureGroupingsObject = value;
    }

}
