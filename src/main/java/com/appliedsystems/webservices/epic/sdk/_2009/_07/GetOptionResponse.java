
package com.appliedsystems.webservices.epic.sdk._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_OptionResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}ArrayOfOptionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getOptionResult"
})
@XmlRootElement(name = "Get_OptionResponse")
public class GetOptionResponse {

    @XmlElement(name = "Get_OptionResult", nillable = true)
    protected ArrayOfOptionType getOptionResult;

    /**
     * Gets the value of the getOptionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfOptionType }
     *     
     */
    public ArrayOfOptionType getGetOptionResult() {
        return getOptionResult;
    }

    /**
     * Sets the value of the getOptionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfOptionType }
     *     
     */
    public void setGetOptionResult(ArrayOfOptionType value) {
        this.getOptionResult = value;
    }

}
