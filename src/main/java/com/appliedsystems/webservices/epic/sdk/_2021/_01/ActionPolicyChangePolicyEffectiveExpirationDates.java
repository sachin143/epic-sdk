
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangePolicyEffectiveExpirationDatesObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}ChangePolicyEffectiveExpirationDates" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changePolicyEffectiveExpirationDatesObject"
})
@XmlRootElement(name = "Action_Policy_ChangePolicyEffectiveExpirationDates")
public class ActionPolicyChangePolicyEffectiveExpirationDates {

    @XmlElement(name = "ChangePolicyEffectiveExpirationDatesObject", nillable = true)
    protected ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject;

    /**
     * Gets the value of the changePolicyEffectiveExpirationDatesObject property.
     * 
     * @return
     *     possible object is
     *     {@link ChangePolicyEffectiveExpirationDates }
     *     
     */
    public ChangePolicyEffectiveExpirationDates getChangePolicyEffectiveExpirationDatesObject() {
        return changePolicyEffectiveExpirationDatesObject;
    }

    /**
     * Sets the value of the changePolicyEffectiveExpirationDatesObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangePolicyEffectiveExpirationDates }
     *     
     */
    public void setChangePolicyEffectiveExpirationDatesObject(ChangePolicyEffectiveExpirationDates value) {
        this.changePolicyEffectiveExpirationDatesObject = value;
    }

}
