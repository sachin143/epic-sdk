
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ReceiptResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/}ReceiptGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerReceiptResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ReceiptResponse")
public class GetGeneralLedgerReceiptResponse {

    @XmlElement(name = "Get_GeneralLedger_ReceiptResult", nillable = true)
    protected ReceiptGetResult getGeneralLedgerReceiptResult;

    /**
     * Gets the value of the getGeneralLedgerReceiptResult property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptGetResult }
     *     
     */
    public ReceiptGetResult getGetGeneralLedgerReceiptResult() {
        return getGeneralLedgerReceiptResult;
    }

    /**
     * Sets the value of the getGeneralLedgerReceiptResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptGetResult }
     *     
     */
    public void setGetGeneralLedgerReceiptResult(ReceiptGetResult value) {
        this.getGeneralLedgerReceiptResult = value;
    }

}
