
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EndorseReviseExistingLineObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}EndorseReviseExistingLine" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "endorseReviseExistingLineObject"
})
@XmlRootElement(name = "Action_Policy_EndorseReviseExistingLine")
public class ActionPolicyEndorseReviseExistingLine {

    @XmlElement(name = "EndorseReviseExistingLineObject", nillable = true)
    protected EndorseReviseExistingLine endorseReviseExistingLineObject;

    /**
     * Gets the value of the endorseReviseExistingLineObject property.
     * 
     * @return
     *     possible object is
     *     {@link EndorseReviseExistingLine }
     *     
     */
    public EndorseReviseExistingLine getEndorseReviseExistingLineObject() {
        return endorseReviseExistingLineObject;
    }

    /**
     * Sets the value of the endorseReviseExistingLineObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link EndorseReviseExistingLine }
     *     
     */
    public void setEndorseReviseExistingLineObject(EndorseReviseExistingLine value) {
        this.endorseReviseExistingLineObject = value;
    }

}
