
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsuredContactObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_claim/}InsuredContact" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insuredContactObject"
})
@XmlRootElement(name = "Update_Claim_InsuredContact")
public class UpdateClaimInsuredContact {

    @XmlElement(name = "InsuredContactObject", nillable = true)
    protected InsuredContact insuredContactObject;

    /**
     * Gets the value of the insuredContactObject property.
     * 
     * @return
     *     possible object is
     *     {@link InsuredContact }
     *     
     */
    public InsuredContact getInsuredContactObject() {
        return insuredContactObject;
    }

    /**
     * Sets the value of the insuredContactObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuredContact }
     *     
     */
    public void setInsuredContactObject(InsuredContact value) {
        this.insuredContactObject = value;
    }

}
