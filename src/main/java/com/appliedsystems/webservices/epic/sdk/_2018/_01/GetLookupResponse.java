
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_LookupResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/}ArrayOfLookup" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLookupResult"
})
@XmlRootElement(name = "Get_LookupResponse")
public class GetLookupResponse {

    @XmlElement(name = "Get_LookupResult", nillable = true)
    protected ArrayOfLookup getLookupResult;

    /**
     * Gets the value of the getLookupResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLookup }
     *     
     */
    public ArrayOfLookup getGetLookupResult() {
        return getLookupResult;
    }

    /**
     * Sets the value of the getLookupResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLookup }
     *     
     */
    public void setGetLookupResult(ArrayOfLookup value) {
        this.getLookupResult = value;
    }

}
