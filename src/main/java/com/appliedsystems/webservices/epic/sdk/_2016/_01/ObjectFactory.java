
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2016._01 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2016._01
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse }
     * 
     */
    public GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse createGetGeneralLedgerJournalEntryDefaultDefaultEntryResponse() {
        return new GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntryVoidResponse }
     * 
     */
    public ActionGeneralLedgerJournalEntryVoidResponse createActionGeneralLedgerJournalEntryVoidResponse() {
        return new ActionGeneralLedgerJournalEntryVoidResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerJournalEntryDefaultDefaultEntry }
     * 
     */
    public GetGeneralLedgerJournalEntryDefaultDefaultEntry createGetGeneralLedgerJournalEntryDefaultDefaultEntry() {
        return new GetGeneralLedgerJournalEntryDefaultDefaultEntry();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntryTransferOfFundsResponse }
     * 
     */
    public ActionGeneralLedgerJournalEntryTransferOfFundsResponse createActionGeneralLedgerJournalEntryTransferOfFundsResponse() {
        return new ActionGeneralLedgerJournalEntryTransferOfFundsResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerJournalEntryResponse }
     * 
     */
    public UpdateGeneralLedgerJournalEntryResponse createUpdateGeneralLedgerJournalEntryResponse() {
        return new UpdateGeneralLedgerJournalEntryResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerJournalEntry }
     * 
     */
    public InsertGeneralLedgerJournalEntry createInsertGeneralLedgerJournalEntry() {
        return new InsertGeneralLedgerJournalEntry();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntryTransferOfFunds }
     * 
     */
    public ActionGeneralLedgerJournalEntryTransferOfFunds createActionGeneralLedgerJournalEntryTransferOfFunds() {
        return new ActionGeneralLedgerJournalEntryTransferOfFunds();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerJournalEntryResponse }
     * 
     */
    public GetGeneralLedgerJournalEntryResponse createGetGeneralLedgerJournalEntryResponse() {
        return new GetGeneralLedgerJournalEntryResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntryVoid }
     * 
     */
    public ActionGeneralLedgerJournalEntryVoid createActionGeneralLedgerJournalEntryVoid() {
        return new ActionGeneralLedgerJournalEntryVoid();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerJournalEntry }
     * 
     */
    public UpdateGeneralLedgerJournalEntry createUpdateGeneralLedgerJournalEntry() {
        return new UpdateGeneralLedgerJournalEntry();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntrySubmitResponse }
     * 
     */
    public ActionGeneralLedgerJournalEntrySubmitResponse createActionGeneralLedgerJournalEntrySubmitResponse() {
        return new ActionGeneralLedgerJournalEntrySubmitResponse();
    }

    /**
     * Create an instance of {@link GetConfigureSalesTeams }
     * 
     */
    public GetConfigureSalesTeams createGetConfigureSalesTeams() {
        return new GetConfigureSalesTeams();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerJournalEntryResponse }
     * 
     */
    public InsertGeneralLedgerJournalEntryResponse createInsertGeneralLedgerJournalEntryResponse() {
        return new InsertGeneralLedgerJournalEntryResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerJournalEntrySubmit }
     * 
     */
    public ActionGeneralLedgerJournalEntrySubmit createActionGeneralLedgerJournalEntrySubmit() {
        return new ActionGeneralLedgerJournalEntrySubmit();
    }

    /**
     * Create an instance of {@link GetConfigureSalesTeamsResponse }
     * 
     */
    public GetConfigureSalesTeamsResponse createGetConfigureSalesTeamsResponse() {
        return new GetConfigureSalesTeamsResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerJournalEntry }
     * 
     */
    public GetGeneralLedgerJournalEntry createGetGeneralLedgerJournalEntry() {
        return new GetGeneralLedgerJournalEntry();
    }

}
