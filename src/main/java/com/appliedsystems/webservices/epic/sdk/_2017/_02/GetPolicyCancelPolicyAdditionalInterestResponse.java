
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_CancelPolicyAdditionalInterestResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/}ArrayOfAdditionalInterest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyCancelPolicyAdditionalInterestResult"
})
@XmlRootElement(name = "Get_Policy_CancelPolicyAdditionalInterestResponse")
public class GetPolicyCancelPolicyAdditionalInterestResponse {

    @XmlElement(name = "Get_Policy_CancelPolicyAdditionalInterestResult", nillable = true)
    protected ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterestResult;

    /**
     * Gets the value of the getPolicyCancelPolicyAdditionalInterestResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdditionalInterest }
     *     
     */
    public ArrayOfAdditionalInterest getGetPolicyCancelPolicyAdditionalInterestResult() {
        return getPolicyCancelPolicyAdditionalInterestResult;
    }

    /**
     * Sets the value of the getPolicyCancelPolicyAdditionalInterestResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdditionalInterest }
     *     
     */
    public void setGetPolicyCancelPolicyAdditionalInterestResult(ArrayOfAdditionalInterest value) {
        this.getPolicyCancelPolicyAdditionalInterestResult = value;
    }

}
