
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalInterestID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "additionalInterestID"
})
@XmlRootElement(name = "Delete_Policy_CancelPolicyAdditionalInterest")
public class DeletePolicyCancelPolicyAdditionalInterest {

    @XmlElement(name = "AdditionalInterestID")
    protected Integer additionalInterestID;

    /**
     * Gets the value of the additionalInterestID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAdditionalInterestID() {
        return additionalInterestID;
    }

    /**
     * Sets the value of the additionalInterestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAdditionalInterestID(Integer value) {
        this.additionalInterestID = value;
    }

}
