
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoidPaymentObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}VoidPayment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "voidPaymentObject"
})
@XmlRootElement(name = "Action_Transaction_VoidPayment")
public class ActionTransactionVoidPayment {

    @XmlElement(name = "VoidPaymentObject", nillable = true)
    protected VoidPayment voidPaymentObject;

    /**
     * Gets the value of the voidPaymentObject property.
     * 
     * @return
     *     possible object is
     *     {@link VoidPayment }
     *     
     */
    public VoidPayment getVoidPaymentObject() {
        return voidPaymentObject;
    }

    /**
     * Sets the value of the voidPaymentObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoidPayment }
     *     
     */
    public void setVoidPaymentObject(VoidPayment value) {
        this.voidPaymentObject = value;
    }

}
