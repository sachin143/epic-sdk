
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerateTaxFeeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}GenerateTaxFee" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generateTaxFeeObject"
})
@XmlRootElement(name = "Action_Transaction_GenerateTaxFee")
public class ActionTransactionGenerateTaxFee {

    @XmlElement(name = "GenerateTaxFeeObject", nillable = true)
    protected GenerateTaxFee generateTaxFeeObject;

    /**
     * Gets the value of the generateTaxFeeObject property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateTaxFee }
     *     
     */
    public GenerateTaxFee getGenerateTaxFeeObject() {
        return generateTaxFeeObject;
    }

    /**
     * Sets the value of the generateTaxFeeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateTaxFee }
     *     
     */
    public void setGenerateTaxFeeObject(GenerateTaxFee value) {
        this.generateTaxFeeObject = value;
    }

}
