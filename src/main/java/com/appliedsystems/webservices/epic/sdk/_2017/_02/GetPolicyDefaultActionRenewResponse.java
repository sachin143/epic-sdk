
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_DefaultActionRenewResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}ArrayOfRenew" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyDefaultActionRenewResult"
})
@XmlRootElement(name = "Get_Policy_DefaultActionRenewResponse")
public class GetPolicyDefaultActionRenewResponse {

    @XmlElement(name = "Get_Policy_DefaultActionRenewResult", nillable = true)
    protected ArrayOfRenew getPolicyDefaultActionRenewResult;

    /**
     * Gets the value of the getPolicyDefaultActionRenewResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRenew }
     *     
     */
    public ArrayOfRenew getGetPolicyDefaultActionRenewResult() {
        return getPolicyDefaultActionRenewResult;
    }

    /**
     * Sets the value of the getPolicyDefaultActionRenewResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRenew }
     *     
     */
    public void setGetPolicyDefaultActionRenewResult(ArrayOfRenew value) {
        this.getPolicyDefaultActionRenewResult = value;
    }

}
