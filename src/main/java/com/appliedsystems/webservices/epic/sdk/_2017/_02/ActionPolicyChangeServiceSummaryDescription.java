
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeServiceSummaryDescriptionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}ChangeServiceSummaryDescription" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "changeServiceSummaryDescriptionObject"
})
@XmlRootElement(name = "Action_Policy_ChangeServiceSummaryDescription")
public class ActionPolicyChangeServiceSummaryDescription {

    @XmlElement(name = "ChangeServiceSummaryDescriptionObject", nillable = true)
    protected ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject;

    /**
     * Gets the value of the changeServiceSummaryDescriptionObject property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeServiceSummaryDescription }
     *     
     */
    public ChangeServiceSummaryDescription getChangeServiceSummaryDescriptionObject() {
        return changeServiceSummaryDescriptionObject;
    }

    /**
     * Sets the value of the changeServiceSummaryDescriptionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeServiceSummaryDescription }
     *     
     */
    public void setChangeServiceSummaryDescriptionObject(ChangeServiceSummaryDescription value) {
        this.changeServiceSummaryDescriptionObject = value;
    }

}
