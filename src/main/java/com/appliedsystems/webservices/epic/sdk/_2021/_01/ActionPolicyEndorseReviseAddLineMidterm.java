
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EndorseReviseAddLineMidtermObject" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_policy/_action/}EndorseReviseAddLineMidterm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "endorseReviseAddLineMidtermObject"
})
@XmlRootElement(name = "Action_Policy_EndorseReviseAddLineMidterm")
public class ActionPolicyEndorseReviseAddLineMidterm {

    @XmlElement(name = "EndorseReviseAddLineMidtermObject", nillable = true)
    protected EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject;

    /**
     * Gets the value of the endorseReviseAddLineMidtermObject property.
     * 
     * @return
     *     possible object is
     *     {@link EndorseReviseAddLineMidterm }
     *     
     */
    public EndorseReviseAddLineMidterm getEndorseReviseAddLineMidtermObject() {
        return endorseReviseAddLineMidtermObject;
    }

    /**
     * Sets the value of the endorseReviseAddLineMidtermObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link EndorseReviseAddLineMidterm }
     *     
     */
    public void setEndorseReviseAddLineMidtermObject(EndorseReviseAddLineMidterm value) {
        this.endorseReviseAddLineMidtermObject = value;
    }

}
