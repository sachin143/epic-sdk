
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalPartyGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdjustorGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Summary;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.CancellationRequestReasonMethodGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.AccountsReceivableWriteOff;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.AdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.JournalEntry;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Receipt;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Voucher;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyAdditionalInterestGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineTypeGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._client.SplitReceivableTemplateGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccountGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationEntriesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationMethod;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGrouping;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGroupingsGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.TaxFeeRatesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult;
import com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKCore_2022_01", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKCore202201 {


    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param clientFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult
     * @throws EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client")
    @WebResult(name = "Get_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClient")
    @ResponseWrapper(localName = "Get_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientResponse")
    public ClientGetResult getClient(
        @WebParam(name = "ClientFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ClientFilter clientFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client")
    @WebResult(name = "Insert_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClient")
    @ResponseWrapper(localName = "Insert_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientResponse")
    public Integer insertClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @throws EpicSDKCore202201UpdateClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client")
    @RequestWrapper(localName = "Update_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClient")
    @ResponseWrapper(localName = "Update_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientResponse")
    public void updateClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate
     * @throws EpicSDKCore202201GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client_SplitReceivableTemplate")
    @WebResult(name = "Get_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Get_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplateResponse")
    public ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplate(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplateGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @throws EpicSDKCore202201UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Update_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Update_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplateResponse")
    public void updateClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client_SplitReceivableTemplate")
    @WebResult(name = "Insert_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Insert_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplateResponse")
    public Integer insertClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateID
     * @throws EpicSDKCore202201DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Delete_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Delete_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Delete_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplateResponse")
    public void deleteClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer splitReceivableTemplateID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Insert_Activity")
    @WebResult(name = "Insert_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivity")
    @ResponseWrapper(localName = "Insert_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivityResponse")
    public Integer insertActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Update_Activity")
    @RequestWrapper(localName = "Update_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivity")
    @ResponseWrapper(localName = "Update_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivityResponse")
    public void updateActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202201GetActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Get_Activity")
    @WebResult(name = "Get_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivity")
    @ResponseWrapper(localName = "Get_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivityResponse")
    public ActivityGetResult getActivity(
        @WebParam(name = "ActivityFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityFilter activityFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param lineFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult
     * @throws EpicSDKCore202201GetLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Get_Line")
    @WebResult(name = "Get_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLine")
    @ResponseWrapper(localName = "Get_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLineResponse")
    public LineGetResult getLine(
        @WebParam(name = "LineFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        LineFilter lineFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetLineInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertLineConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Insert_Line")
    @WebResult(name = "Insert_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLine")
    @ResponseWrapper(localName = "Insert_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLineResponse")
    public Integer insertLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertLineInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @throws EpicSDKCore202201UpdateLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateLineConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Update_Line")
    @RequestWrapper(localName = "Update_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLine")
    @ResponseWrapper(localName = "Update_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLineResponse")
    public void updateLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateLineInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineID
     * @throws EpicSDKCore202201DeleteLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Delete_Line")
    @RequestWrapper(localName = "Delete_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLine")
    @ResponseWrapper(localName = "Delete_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLineResponse")
    public void deleteLine(
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteLineInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param contactFilterObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult
     * @throws EpicSDKCore202201GetContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Get_Contact")
    @WebResult(name = "Get_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContact")
    @ResponseWrapper(localName = "Get_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContactResponse")
    public ContactGetResult getContact(
        @WebParam(name = "ContactFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ContactFilter contactFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param id
     * @throws EpicSDKCore202201DeleteContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Delete_Contact")
    @RequestWrapper(localName = "Delete_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContact")
    @ResponseWrapper(localName = "Delete_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContactResponse")
    public void deleteContact(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Insert_Contact")
    @WebResult(name = "Insert_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContact")
    @ResponseWrapper(localName = "Insert_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContactResponse")
    public Integer insertContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @throws EpicSDKCore202201UpdateContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Update_Contact")
    @RequestWrapper(localName = "Update_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContact")
    @ResponseWrapper(localName = "Update_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContactResponse")
    public void updateContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactID
     * @throws EpicSDKCore202201ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Contact_ChangeMainBusinessContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Action_Contact_ChangeMainBusinessContact")
    @RequestWrapper(localName = "Action_Contact_ChangeMainBusinessContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContact")
    @ResponseWrapper(localName = "Action_Contact_ChangeMainBusinessContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContactResponse")
    public void actionContactChangeMainBusinessContact(
        @WebParam(name = "ContactID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer contactID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param policyFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult
     * @throws EpicSDKCore202201GetPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy")
    @WebResult(name = "Get_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicy")
    @ResponseWrapper(localName = "Get_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyResponse")
    public PolicyGetResult getPolicy(
        @WebParam(name = "PolicyFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        PolicyFilter policyFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy")
    @WebResult(name = "Insert_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicy")
    @ResponseWrapper(localName = "Insert_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyResponse")
    public Integer insertPolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @throws EpicSDKCore202201UpdatePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy")
    @RequestWrapper(localName = "Update_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicy")
    @ResponseWrapper(localName = "Update_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyResponse")
    public void updatePolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202201DeletePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy")
    @RequestWrapper(localName = "Delete_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicy")
    @ResponseWrapper(localName = "Delete_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyResponse")
    public void deletePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeletePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ActivateInactivateMultiCarrierSchedule")
    @RequestWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyActivateInactivateMultiCarrierSchedule")
    @ResponseWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyActivateInactivateMultiCarrierScheduleResponse")
    public void actionPolicyActivateInactivateMultiCarrierSchedule(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201ActionPolicyCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyCancelInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyCancelAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Cancel", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Cancel")
    @WebResult(name = "Action_Policy_CancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_Cancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyCancel")
    @ResponseWrapper(localName = "Action_Policy_CancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyCancelResponse")
    public Integer actionPolicyCancel(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202201GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionCancel", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionCancel")
    @WebResult(name = "Get_Policy_DefaultActionCancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionCancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionCancel")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionCancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionCancelResponse")
    public ArrayOfCancel getPolicyDefaultActionCancel(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202201GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancellationRequestReasonMethod")
    @WebResult(name = "Get_Policy_CancellationRequestReasonMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Get_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancellationRequestReasonMethodResponse")
    public ArrayOfCancel getPolicyCancellationRequestReasonMethod(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancellationRequestReasonMethodGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancellationRequestReasonMethod")
    @RequestWrapper(localName = "Update_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Update_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancellationRequestReasonMethodResponse")
    public void updatePolicyCancellationRequestReasonMethod(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param lineID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates
     * @throws EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates")
    @WebResult(name = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse")
    public ArrayOfChangePolicyEffectiveExpirationDates getPolicyDefaultActionChangePolicyEffectiveExpirationDates(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param changePolicyEffectiveExpirationDatesObject
     * @throws EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangePolicyEffectiveExpirationDates")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyEffectiveExpirationDatesResponse")
    public void actionPolicyChangePolicyEffectiveExpirationDates(
        @WebParam(name = "ChangePolicyEffectiveExpirationDatesObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyProspectiveContractedStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangePolicyProspectiveContractedStatus")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyProspectiveContractedStatus")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangePolicyProspectiveContractedStatusResponse")
    public void actionPolicyChangePolicyProspectiveContractedStatus(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param changeServiceSummaryDescriptionObject
     * @param messageHeader
     * @throws EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangeServiceSummaryDescription", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_ChangeServiceSummaryDescription")
    @RequestWrapper(localName = "Action_Policy_ChangeServiceSummaryDescription", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangeServiceSummaryDescription")
    @ResponseWrapper(localName = "Action_Policy_ChangeServiceSummaryDescriptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyChangeServiceSummaryDescriptionResponse")
    public void actionPolicyChangeServiceSummaryDescription(
        @WebParam(name = "ChangeServiceSummaryDescriptionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param endorseReviseAddLineMidtermObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseAddLineMidterm", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_EndorseReviseAddLineMidterm")
    @WebResult(name = "Action_Policy_EndorseReviseAddLineMidtermResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseAddLineMidterm", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseAddLineMidterm")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseAddLineMidtermResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseAddLineMidtermResponse")
    public ArrayOfint actionPolicyEndorseReviseAddLineMidterm(
        @WebParam(name = "EndorseReviseAddLineMidtermObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param endorseReviseExistingLineObject
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseExistingLine", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_EndorseReviseExistingLine")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseExistingLine", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseExistingLine")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseExistingLineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyEndorseReviseExistingLineResponse")
    public void actionPolicyEndorseReviseExistingLine(
        @WebParam(name = "EndorseReviseExistingLineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        EndorseReviseExistingLine endorseReviseExistingLineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssueEndorsementObject
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueNotIssueEndorsement")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssueEndorsementResponse")
    public void actionPolicyIssueNotIssueEndorsement(
        @WebParam(name = "IssueNotIssueEndorsementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssueEndorsement issueNotIssueEndorsementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueNotIssueEndorsement")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssueEndorsementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssueEndorsementResponse")
    public ArrayOfIssueNotIssueEndorsement getPolicyDefaultActionIssueNotIssueEndorsement(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssueEndorsementGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssuePolicyObject
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueNotIssuePolicy")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueNotIssuePolicyResponse")
    public void actionPolicyIssueNotIssuePolicy(
        @WebParam(name = "IssueNotIssuePolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssuePolicy issueNotIssuePolicyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueNotIssuePolicy")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssuePolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueNotIssuePolicyResponse")
    public ArrayOfIssueNotIssuePolicy getPolicyDefaultActionIssueNotIssuePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueNotIssuePolicyGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueCancellationObject
     * @throws EpicSDKCore202201ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_IssueCancellation")
    @RequestWrapper(localName = "Action_Policy_IssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueCancellation")
    @ResponseWrapper(localName = "Action_Policy_IssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyIssueCancellationResponse")
    public void actionPolicyIssueCancellation(
        @WebParam(name = "IssueCancellationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        IssueCancellation issueCancellationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionIssueCancellation")
    @WebResult(name = "Get_Policy_DefaultActionIssueCancellationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueCancellation")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionIssueCancellationResponse")
    public ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellation(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer serviceSummaryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reinstateObject
     * @throws EpicSDKCore202201ActionPolicyReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyReinstateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Reinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Reinstate")
    @RequestWrapper(localName = "Action_Policy_Reinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyReinstate")
    @ResponseWrapper(localName = "Action_Policy_ReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyReinstateResponse")
    public void actionPolicyReinstate(
        @WebParam(name = "ReinstateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Reinstate reinstateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate
     * @throws EpicSDKCore202201GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionReinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionReinstate")
    @WebResult(name = "Get_Policy_DefaultActionReinstateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionReinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionReinstate")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionReinstateResponse")
    public ArrayOfReinstate getPolicyDefaultActionReinstate(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param renewObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201ActionPolicyRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyRenewMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Renew", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_Renew")
    @WebResult(name = "Action_Policy_RenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Action_Policy_Renew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyRenew")
    @ResponseWrapper(localName = "Action_Policy_RenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyRenewResponse")
    public Integer actionPolicyRenew(
        @WebParam(name = "RenewObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Renew renewObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew
     * @throws EpicSDKCore202201GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionRenew", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionRenew")
    @WebResult(name = "Get_Policy_DefaultActionRenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionRenew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionRenew")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionRenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionRenewResponse")
    public ArrayOfRenew getPolicyDefaultActionRenew(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionUpdateStageToSubmitted")
    @WebResult(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateStageToSubmittedResponse")
    public ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmitted(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param oSubmissionObject
     * @param messageHeader
     * @throws EpicSDKCore202201ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_UpdateStageToSubmitted")
    @RequestWrapper(localName = "Action_Policy_UpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Action_Policy_UpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateStageToSubmittedResponse")
    public void actionPolicyUpdateStageToSubmitted(
        @WebParam(name = "oSubmissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        UpdateStageToSubmitted oSubmissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_DeleteServiceSummaryRow", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_DeleteServiceSummaryRow")
    @RequestWrapper(localName = "Action_Policy_DeleteServiceSummaryRow", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyDeleteServiceSummaryRow")
    @ResponseWrapper(localName = "Action_Policy_DeleteServiceSummaryRowResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyDeleteServiceSummaryRowResponse")
    public void actionPolicyDeleteServiceSummaryRow(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyDeleteServiceSummaryRowMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyIdList
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateRenewalStage", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_DefaultActionUpdateRenewalStage")
    @WebResult(name = "Get_Policy_DefaultActionUpdateRenewalStageResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateRenewalStage", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateRenewalStage")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateRenewalStageResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyDefaultActionUpdateRenewalStageResponse")
    public ArrayOfUpdateRenewalStage getPolicyDefaultActionUpdateRenewalStage(
        @WebParam(name = "PolicyIdList", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ArrayOfint policyIdList,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyDefaultActionUpdateRenewalStageMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param updateRenewalStageList
     * @param changeStageToCode
     * @throws EpicSDKCore202201ActionPolicyUpdateRenewalStageAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateRenewalStageMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateRenewalStageConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionPolicyUpdateRenewalStageInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateRenewalStage", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Action_Policy_UpdateRenewalStage")
    @RequestWrapper(localName = "Action_Policy_UpdateRenewalStage", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateRenewalStage")
    @ResponseWrapper(localName = "Action_Policy_UpdateRenewalStageResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.ActionPolicyUpdateRenewalStageResponse")
    public void actionPolicyUpdateRenewalStage(
        @WebParam(name = "UpdateRenewalStageList", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        ArrayOfUpdateRenewalStage updateRenewalStageList,
        @WebParam(name = "ChangeStageToCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer changeStageToCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionPolicyUpdateRenewalStageAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateRenewalStageConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateRenewalStageInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionPolicyUpdateRenewalStageMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyAdditionalInterestsIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest
     * @throws EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Get_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyAdditionalInterestResponse")
    public ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "CancelPolicyAdditionalInterestsIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancelPolicyAdditionalInterestGetType cancelPolicyAdditionalInterestsIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Insert_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyAdditionalInterestResponse")
    public Integer insertPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyAdditionalInterestResponse")
    public void updatePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalInterestID
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyAdditionalInterestResponse")
    public void deletePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "AdditionalInterestID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer additionalInterestID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyRemarksIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark
     * @throws EpicSDKCore202201GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Get_Policy_CancelPolicyRemark")
    @WebResult(name = "Get_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.GetPolicyCancelPolicyRemarkResponse")
    public ArrayOfRemark getPolicyCancelPolicyRemark(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer id,
        @WebParam(name = "CancelPolicyRemarksIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        CancelPolicyRemarksGetType cancelPolicyRemarksIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Insert_Policy_CancelPolicyRemark")
    @WebResult(name = "Insert_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.InsertPolicyCancelPolicyRemarkResponse")
    public Integer insertPolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Update_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.UpdatePolicyCancelPolicyRemarkResponse")
    public void updatePolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param remarkID
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2022/01/Policy_2022_01/Delete_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/", className = "com.appliedsystems.webservices.epic.sdk._2022._01.DeletePolicyCancelPolicyRemarkResponse")
    public void deletePolicyCancelPolicyRemark(
        @WebParam(name = "RemarkID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2022/01/")
        Integer remarkID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201InsertAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertAttachmentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Insert_Attachment")
    @WebResult(name = "Insert_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachment")
    @ResponseWrapper(localName = "Insert_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachmentResponse")
    public ArrayOfint insertAttachment(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @param attachmentFilterObject
     * @param attachmentSortingObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult
     * @throws EpicSDKCore202201GetAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetAttachmentAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Get_Attachment")
    @WebResult(name = "Get_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachment")
    @ResponseWrapper(localName = "Get_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachmentResponse")
    public AttachmentGetResult getAttachment(
        @WebParam(name = "AttachmentFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentFilter attachmentFilterObject,
        @WebParam(name = "AttachmentSortingObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentSorting attachmentSortingObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Details", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Details")
    @RequestWrapper(localName = "Update_Attachment_Details", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetails")
    @ResponseWrapper(localName = "Update_Attachment_DetailsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetailsResponse")
    public void updateAttachmentDetails(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveFolder", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveFolder")
    @RequestWrapper(localName = "Update_Attachment_MoveFolder", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolder")
    @ResponseWrapper(localName = "Update_Attachment_MoveFolderResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolderResponse")
    public void updateAttachmentMoveFolder(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveAccount")
    @RequestWrapper(localName = "Update_Attachment_MoveAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccount")
    @ResponseWrapper(localName = "Update_Attachment_MoveAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccountResponse")
    public void updateAttachmentMoveAccount(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Binary", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Binary")
    @WebResult(name = "Update_Attachment_BinaryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Update_Attachment_Binary", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinary")
    @ResponseWrapper(localName = "Update_Attachment_BinaryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinaryResponse")
    public Integer updateAttachmentBinary(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202201DeleteAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteAttachmentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Delete_Attachment")
    @RequestWrapper(localName = "Delete_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachment")
    @ResponseWrapper(localName = "Delete_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachmentResponse")
    public void deleteAttachment(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202201ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionAttachmentReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Attachment_Reactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Action_Attachment_Reactivate")
    @RequestWrapper(localName = "Action_Attachment_Reactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivate")
    @ResponseWrapper(localName = "Action_Attachment_ReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivateResponse")
    public void actionAttachmentReactivate(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionAttachmentReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202201GetTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction")
    @WebResult(name = "Get_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransaction")
    @ResponseWrapper(localName = "Get_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionResponse")
    public TransactionGetResult getTransaction(
        @WebParam(name = "TransactionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionFilter transactionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201InsertTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Insert_Transaction")
    @WebResult(name = "Insert_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransaction")
    @ResponseWrapper(localName = "Insert_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransactionResponse")
    public ArrayOfint insertTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @throws EpicSDKCore202201UpdateTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Update_Transaction")
    @RequestWrapper(localName = "Update_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransaction")
    @ResponseWrapper(localName = "Update_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransactionResponse")
    public void updateTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @param installmentType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202201GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultInstallments", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultInstallments")
    @WebResult(name = "Get_Transaction_DefaultInstallmentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultInstallments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallments")
    @ResponseWrapper(localName = "Get_Transaction_DefaultInstallmentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallmentsResponse")
    public TransactionGetResult getTransactionDefaultInstallments(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "InstallmentType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionGetInstallmentType installmentType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultProducerBrokerCommissions", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultProducerBrokerCommissions")
    @WebResult(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissions", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissions")
    @ResponseWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissionsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissionsResponse")
    public TransactionGetResult getTransactionDefaultProducerBrokerCommissions(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable
     * @throws EpicSDKCore202201GetTransactionReceivablesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionReceivablesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionReceivablesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionReceivablesConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_Receivables", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_Receivables")
    @WebResult(name = "Get_Transaction_ReceivablesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_Receivables", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivables")
    @ResponseWrapper(localName = "Get_Transaction_ReceivablesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivablesResponse")
    public ArrayOfReceivable getTransactionReceivables(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionReceivablesAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionReceivablesConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionReceivablesInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionReceivablesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param accountsReceivableWriteOffObject
     * @throws EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AccountsReceivableWriteOff", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AccountsReceivableWriteOff")
    @RequestWrapper(localName = "Action_Transaction_AccountsReceivableWriteOff", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOff")
    @ResponseWrapper(localName = "Action_Transaction_AccountsReceivableWriteOffResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOffResponse")
    public void actionTransactionAccountsReceivableWriteOff(
        @WebParam(name = "AccountsReceivableWriteOffObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AccountsReceivableWriteOff accountsReceivableWriteOffObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param applyCreditsToDebitsObject
     * @throws EpicSDKCore202201ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ApplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_ApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_ApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebitsResponse")
    public void actionTransactionApplyCreditsToDebits(
        @WebParam(name = "ApplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ApplyCreditsToDebits applyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param associatedAccountID
     * @param messageHeader
     * @param associatedAccountTypeCode
     * @param agencyCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits
     * @throws EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionApplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebitsResponse")
    public ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebits(
        @WebParam(name = "AssociatedAccountTypeCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String associatedAccountTypeCode,
        @WebParam(name = "AssociatedAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer associatedAccountID,
        @WebParam(name = "AgencyCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String agencyCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param financeTransactionObject
     * @throws EpicSDKCore202201ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_FinanceTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_FinanceTransaction")
    @RequestWrapper(localName = "Action_Transaction_FinanceTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransaction")
    @ResponseWrapper(localName = "Action_Transaction_FinanceTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransactionResponse")
    public void actionTransactionFinanceTransaction(
        @WebParam(name = "FinanceTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        FinanceTransaction financeTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @throws EpicSDKCore202201ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_MoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_MoveTransaction")
    @RequestWrapper(localName = "Action_Transaction_MoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransaction")
    @ResponseWrapper(localName = "Action_Transaction_MoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransactionResponse")
    public void actionTransactionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction
     * @throws EpicSDKCore202201GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionMoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionMoveTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionMoveTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionMoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionMoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransactionResponse")
    public ArrayOfMoveTransaction getTransactionDefaultActionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction
     * @throws EpicSDKCore202201GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionReverseTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionReverseTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransactionResponse")
    public ArrayOfReverseTransaction getTransactionDefaultActionReverseTransaction(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reverseTransactionObject
     * @throws EpicSDKCore202201ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ReverseTransaction")
    @RequestWrapper(localName = "Action_Transaction_ReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransaction")
    @ResponseWrapper(localName = "Action_Transaction_ReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransactionResponse")
    public void actionTransactionReverseTransaction(
        @WebParam(name = "ReverseTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ReverseTransaction reverseTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param unapplyCreditsToDebitsObject
     * @param messageHeader
     * @throws EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_UnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_UnapplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_UnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_UnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebitsResponse")
    public void actionTransactionUnapplyCreditsToDebits(
        @WebParam(name = "UnapplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        UnapplyCreditsToDebits unapplyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionDetailNumber
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits
     * @throws EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionUnapplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebitsResponse")
    public ArrayOfUnapplyCreditsToDebits getTransactionDefaultActionUnapplyCreditsToDebits(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "TransactionDetailNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionDetailNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voidPaymentObject
     * @param messageHeader
     * @throws EpicSDKCore202201ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_VoidPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_VoidPayment")
    @RequestWrapper(localName = "Action_Transaction_VoidPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPayment")
    @ResponseWrapper(localName = "Action_Transaction_VoidPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPaymentResponse")
    public void actionTransactionVoidPayment(
        @WebParam(name = "VoidPaymentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        VoidPayment voidPaymentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission
     * @throws EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionAdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionAdjustCommission")
    @WebResult(name = "Get_Transaction_DefaultActionAdjustCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionAdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommission")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionAdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommissionResponse")
    public ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommission(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustCommissionObject
     * @throws EpicSDKCore202201ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AdjustCommission")
    @RequestWrapper(localName = "Action_Transaction_AdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommission")
    @ResponseWrapper(localName = "Action_Transaction_AdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommissionResponse")
    public void actionTransactionAdjustCommission(
        @WebParam(name = "AdjustCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AdjustCommission adjustCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium
     * @throws EpicSDKCore202201GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionRevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionRevisePremium")
    @WebResult(name = "Get_Transaction_DefaultActionRevisePremiumResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionRevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremium")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionRevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremiumResponse")
    public ArrayOfRevisePremium getTransactionDefaultActionRevisePremium(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param revisePremiumObject
     * @throws EpicSDKCore202201ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_RevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_RevisePremium")
    @RequestWrapper(localName = "Action_Transaction_RevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremium")
    @ResponseWrapper(localName = "Action_Transaction_RevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremiumResponse")
    public void actionTransactionRevisePremium(
        @WebParam(name = "RevisePremiumObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        RevisePremium revisePremiumObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee
     * @throws EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionGenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionGenerateTaxFee")
    @WebResult(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFee")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFeeResponse")
    public ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFee(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param generateTaxFeeObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_GenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_GenerateTaxFee")
    @WebResult(name = "Action_Transaction_GenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_Transaction_GenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFee")
    @ResponseWrapper(localName = "Action_Transaction_GenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFeeResponse")
    public ArrayOfint actionTransactionGenerateTaxFee(
        @WebParam(name = "GenerateTaxFeeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        GenerateTaxFee generateTaxFeeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim")
    @WebResult(name = "Insert_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaim")
    @ResponseWrapper(localName = "Insert_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimResponse")
    public Integer insertClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param claimSummaryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult
     * @throws EpicSDKCore202201GetClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim")
    @WebResult(name = "Get_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaim")
    @ResponseWrapper(localName = "Get_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimResponse")
    public ClaimSummaryGetResult getClaim(
        @WebParam(name = "ClaimSummaryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ClaimSummaryFilter claimSummaryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @throws EpicSDKCore202201DeleteClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim")
    @RequestWrapper(localName = "Delete_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaim")
    @ResponseWrapper(localName = "Delete_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimResponse")
    public void deleteClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @throws EpicSDKCore202201UpdateClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim")
    @RequestWrapper(localName = "Update_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaim")
    @ResponseWrapper(localName = "Update_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimResponse")
    public void updateClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty
     * @throws EpicSDKCore202201GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_AdditionalParty")
    @WebResult(name = "Get_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalParty")
    @ResponseWrapper(localName = "Get_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalPartyResponse")
    public ArrayOfAdditionalParty getClaimAdditionalParty(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalPartyGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param additionalPartyID
     * @param messageHeader
     * @throws EpicSDKCore202201DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_AdditionalParty")
    @RequestWrapper(localName = "Delete_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalParty")
    @ResponseWrapper(localName = "Delete_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalPartyResponse")
    public void deleteClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalPartyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_AdditionalParty")
    @WebResult(name = "Insert_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalParty")
    @ResponseWrapper(localName = "Insert_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalPartyResponse")
    public Integer insertClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @throws EpicSDKCore202201UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_AdditionalParty")
    @RequestWrapper(localName = "Update_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalParty")
    @ResponseWrapper(localName = "Update_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalPartyResponse")
    public void updateClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor
     * @throws EpicSDKCore202201GetClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Adjustor")
    @WebResult(name = "Get_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustor")
    @ResponseWrapper(localName = "Get_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustorResponse")
    public ArrayOfAdjustor getClaimAdjustor(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdjustorGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertClaimAdjustorConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_Adjustor")
    @WebResult(name = "Insert_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustor")
    @ResponseWrapper(localName = "Insert_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustorResponse")
    public Integer insertClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @throws EpicSDKCore202201UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Adjustor")
    @RequestWrapper(localName = "Update_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustor")
    @ResponseWrapper(localName = "Update_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustorResponse")
    public void updateClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param adjustorID
     * @param messageHeader
     * @throws EpicSDKCore202201DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_Adjustor")
    @RequestWrapper(localName = "Delete_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustor")
    @ResponseWrapper(localName = "Delete_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustorResponse")
    public void deleteClaimAdjustor(
        @WebParam(name = "AdjustorID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer adjustorID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact
     * @throws EpicSDKCore202201GetClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimInsuredContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_InsuredContact")
    @WebResult(name = "Get_Claim_InsuredContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContact")
    @ResponseWrapper(localName = "Get_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContactResponse")
    public ArrayOfInsuredContact getClaimInsuredContact(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param insuredContactObject
     * @throws EpicSDKCore202201UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_InsuredContact")
    @RequestWrapper(localName = "Update_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContact")
    @ResponseWrapper(localName = "Update_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContactResponse")
    public void updateClaimInsuredContact(
        @WebParam(name = "InsuredContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        InsuredContact insuredContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation
     * @throws EpicSDKCore202201GetClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimLitigationInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Litigation")
    @WebResult(name = "Get_Claim_LitigationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigation")
    @ResponseWrapper(localName = "Get_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigationResponse")
    public ArrayOfLitigation getClaimLitigation(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param litigationObject
     * @throws EpicSDKCore202201UpdateClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimLitigationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Litigation")
    @RequestWrapper(localName = "Update_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigation")
    @ResponseWrapper(localName = "Update_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigationResponse")
    public void updateClaimLitigation(
        @WebParam(name = "LitigationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Litigation litigationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense
     * @throws EpicSDKCore202201GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_PaymentExpense")
    @WebResult(name = "Get_Claim_PaymentExpenseResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpense")
    @ResponseWrapper(localName = "Get_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpenseResponse")
    public ArrayOfPaymentExpense getClaimPaymentExpense(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param paymentExpenseObject
     * @throws EpicSDKCore202201UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_PaymentExpense")
    @RequestWrapper(localName = "Update_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpense")
    @ResponseWrapper(localName = "Update_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpenseResponse")
    public void updateClaimPaymentExpense(
        @WebParam(name = "PaymentExpenseObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PaymentExpense paymentExpenseObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts
     * @throws EpicSDKCore202201GetClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetClaimServicingContactsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_ServicingContacts")
    @WebResult(name = "Get_Claim_ServicingContactsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContacts")
    @ResponseWrapper(localName = "Get_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContactsResponse")
    public ArrayOfServicingContacts getClaimServicingContacts(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param servicingContactObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_ServicingContacts")
    @RequestWrapper(localName = "Update_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContacts")
    @ResponseWrapper(localName = "Update_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContactsResponse")
    public void updateClaimServicingContacts(
        @WebParam(name = "ServicingContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ServicingContacts servicingContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param closedDate
     * @param claimID
     * @throws EpicSDKCore202201ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Claim_CloseReopenClaim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Action_Claim_CloseReopenClaim")
    @RequestWrapper(localName = "Action_Claim_CloseReopenClaim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaim")
    @ResponseWrapper(localName = "Action_Claim_CloseReopenClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaimResponse")
    public void actionClaimCloseReopenClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "ClosedDate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        XMLGregorianCalendar closedDate,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param searchTerms
     * @param lookupTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup
     * @throws EpicSDKCore202201GetLookupInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLookupConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLookupMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetLookupAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Lookup", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/ILookup/Get_Lookup")
    @WebResult(name = "Get_LookupResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Lookup", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookup")
    @ResponseWrapper(localName = "Get_LookupResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookupResponse")
    public ArrayOfLookup getLookup(
        @WebParam(name = "LookupTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        LookupTypes lookupTypeObject,
        @WebParam(name = "SearchTerms", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ArrayOfstring searchTerms,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetLookupMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param optionTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType
     * @throws EpicSDKCore202201GetOptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetOptionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetOptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetOptionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Option", action = "http://webservices.appliedsystems.com/epic/sdk/2009/07/IOption/Get_Option")
    @WebResult(name = "Get_OptionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
    @RequestWrapper(localName = "Get_Option", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOption")
    @ResponseWrapper(localName = "Get_OptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOptionResponse")
    public ArrayOfOptionType getOption(
        @WebParam(name = "OptionTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
        OptionTypes optionTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetOptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @param receiptID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_Receipt")
    @WebResult(name = "Get_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptResponse")
    public ReceiptGetResult getGeneralLedgerReceipt(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetType searchType,
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Insert_GeneralLedger_Receipt")
    @WebResult(name = "Insert_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceiptResponse")
    public Integer insertGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Update_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Update_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceiptResponse")
    public void updateGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202201DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Delete_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Delete_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceiptResponse")
    public void deleteGeneralLedgerReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @param detailItemToBeInsertedObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultApplyCreditsToDebits(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "DetailItemToBeInsertedObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        DetailItem detailItemToBeInsertedObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPayments")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultProcessOutstandingPayments(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptFinalizeReceipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptFinalizeReceipt")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceipt")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceiptResponse")
    public void actionGeneralLedgerReceiptFinalizeReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReceiptTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param journalEntryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryFilter journalEntryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntryDefaultDefaultEntry(
        @WebParam(name = "JournalEntryDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryDefaultEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Insert_GeneralLedger_JournalEntry")
    @WebResult(name = "Insert_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Insert_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntryResponse")
    public ArrayOfint insertGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @throws EpicSDKCore202201UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Update_GeneralLedger_JournalEntry")
    @RequestWrapper(localName = "Update_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Update_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntryResponse")
    public void updateGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_JournalEntryTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerJournalEntryTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryVoidObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryVoid")
    @WebResult(name = "Action_GeneralLedger_JournalEntryVoidResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoidResponse")
    public Integer actionGeneralLedgerJournalEntryVoid(
        @WebParam(name = "JournalEntryVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryVoid journalEntryVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param journalEntryID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntrySubmit", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntrySubmit")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntrySubmit", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmit")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntrySubmitResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmitResponse")
    public void actionGeneralLedgerJournalEntrySubmit(
        @WebParam(name = "JournalEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_DisbursementTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerDisbursementTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementPayVouchers")
    @WebResult(name = "Action_GeneralLedger_DisbursementPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerDisbursementPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementVoidObject
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoidResponse")
    public void actionGeneralLedgerDisbursementVoid(
        @WebParam(name = "DisbursementVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementVoid disbursementVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param disbursementID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_Disbursement")
    @WebResult(name = "Get_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementResponse")
    public DisbursementGetResult getGeneralLedgerDisbursement(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetType searchType,
        @WebParam(name = "DisbursementID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Insert_GeneralLedger_Disbursement")
    @WebResult(name = "Insert_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Insert_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursementResponse")
    public Integer insertGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @throws EpicSDKCore202201UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Update_GeneralLedger_Disbursement")
    @RequestWrapper(localName = "Update_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Update_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursementResponse")
    public void updateGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param disbursementBankAccountNumberCode
     * @param messageHeader
     * @param disbursementBankSubAccountNumberCode
     * @param disbursementDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntryResponse")
    public DisbursementGetResult getGeneralLedgerDisbursementDefaultDefaultEntry(
        @WebParam(name = "DisbursementDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementDefaultEntryID,
        @WebParam(name = "DisbursementBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankAccountNumberCode,
        @WebParam(name = "DisbursementBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankSubAccountNumberCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerVoucherTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherPayVouchers")
    @WebResult(name = "Action_GeneralLedger_VoucherPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerVoucherPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerVoucherDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherVoidObject
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoidResponse")
    public void actionGeneralLedgerVoucherVoid(
        @WebParam(name = "VoucherVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherVoid voucherVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Insert_GeneralLedger_Voucher")
    @WebResult(name = "Insert_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Insert_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucherResponse")
    public Integer insertGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @throws EpicSDKCore202201UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Update_GeneralLedger_Voucher")
    @RequestWrapper(localName = "Update_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Update_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucherResponse")
    public void updateGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param voucherID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_Voucher")
    @WebResult(name = "Get_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherResponse")
    public VoucherGetResult getGeneralLedgerVoucher(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetType searchType,
        @WebParam(name = "VoucherID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voucherDefaultEntryID
     * @param voucherAccountingMonth
     * @param messageHeader
     * @param voucherBankSubAccountNumberCode
     * @param voucherBankAccountNumberCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntryResponse")
    public VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntry(
        @WebParam(name = "VoucherDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherDefaultEntryID,
        @WebParam(name = "VoucherBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankAccountNumberCode,
        @WebParam(name = "VoucherBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankSubAccountNumberCode,
        @WebParam(name = "VoucherAccountingMonth", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherAccountingMonth,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param id
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Get_GeneralLedger_Activity")
    @WebResult(name = "Get_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivity")
    @ResponseWrapper(localName = "Get_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivityResponse")
    public ActivityGetResult getGeneralLedgerActivity(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetType searchType,
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Insert_GeneralLedger_Activity")
    @WebResult(name = "Insert_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivity")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivityResponse")
    public Integer insertGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Update_GeneralLedger_Activity")
    @RequestWrapper(localName = "Update_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivity")
    @ResponseWrapper(localName = "Update_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivityResponse")
    public void updateGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param directBillCommissionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommissionResponse")
    public DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "DirectBillCommissionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommissionFilter directBillCommissionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommissionResponse")
    public ArrayOfint insertGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void updateGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionID
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void deleteGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationDirectBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param lDepartments
     * @param lEntityLookupCodes
     * @param messageHeader
     * @param lBranches
     * @param lProfitCenters
     * @param lAgencies
     * @param sCompareByType
     * @param lIssuingCompanies
     * @param sClientOrPolicyNumber
     * @param fIncludeHistory
     * @param lLineOfBusiness
     * @param sEntityType
     * @param sDescriptionType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
    public RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies(
        @WebParam(name = "sEntityType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sEntityType,
        @WebParam(name = "lEntityLookupCodes", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lEntityLookupCodes,
        @WebParam(name = "sDescriptionType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sDescriptionType,
        @WebParam(name = "sCompareByType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sCompareByType,
        @WebParam(name = "sClientOrPolicyNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sClientOrPolicyNumber,
        @WebParam(name = "lAgencies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lAgencies,
        @WebParam(name = "lBranches", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lBranches,
        @WebParam(name = "lDepartments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lDepartments,
        @WebParam(name = "lProfitCenters", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lProfitCenters,
        @WebParam(name = "lIssuingCompanies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lIssuingCompanies,
        @WebParam(name = "lLineOfBusiness", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lLineOfBusiness,
        @WebParam(name = "fIncludeHistory", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean fIncludeHistory,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReconciliationBankTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankReopenStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankReopenStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatementResponse")
    public void actionGeneralLedgerReconciliationBankReopenStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationBankFinalizeStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param bankFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Get_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBankResponse")
    public BankGetResult getGeneralLedgerReconciliationBank(
        @WebParam(name = "BankFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankFilter bankFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBankResponse")
    public Integer insertGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBankResponse")
    public void updateGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reconciliationBankID
     * @param messageHeader
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBankResponse")
    public void deleteGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationBankID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType
     * @throws EpicSDKCore202201GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Get_Policy_PolicyLineType")
    @WebResult(name = "Get_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineType")
    @ResponseWrapper(localName = "Get_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineTypeResponse")
    public ArrayOfPolicyLineType getPolicyPolicyLineType(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineTypeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyLineTypeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyLineType")
    @WebResult(name = "Insert_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineType")
    @ResponseWrapper(localName = "Insert_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineTypeResponse")
    public Integer insertPolicyPolicyLineType(
        @WebParam(name = "PolicyLineTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineType policyLineTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyStatusObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyStatus")
    @WebResult(name = "Insert_Policy_PolicyStatusResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatus")
    @ResponseWrapper(localName = "Insert_Policy_PolicyStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatusResponse")
    public Integer insertPolicyPolicyStatus(
        @WebParam(name = "PolicyStatusObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyStatus policyStatusObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode
     * @throws EpicSDKCore202201GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Get_Transaction_TransactionCode")
    @WebResult(name = "Get_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Get_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCode")
    @ResponseWrapper(localName = "Get_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCodeResponse")
    public ArrayOfTransactionCode getTransactionTransactionCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionCodeObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Insert_Transaction_TransactionCode")
    @WebResult(name = "Insert_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Insert_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCode")
    @ResponseWrapper(localName = "Insert_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCodeResponse")
    public Integer insertTransactionTransactionCode(
        @WebParam(name = "TransactionCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCode transactionCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult
     * @throws EpicSDKCore202201GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_SalesTeams", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IConfigureSalesTeam/Get_Configure_SalesTeams")
    @WebResult(name = "Get_Configure_SalesTeamsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_Configure_SalesTeams", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeams")
    @ResponseWrapper(localName = "Get_Configure_SalesTeamsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeamsResponse")
    public SalesTeamGetResult getConfigureSalesTeams(
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Insert_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccountResponse")
    public Integer insertGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountResponse")
    public ChartOfAccountGetResult getGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccountGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean includeInactive,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param chartOfAccountObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Update_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Update_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccountResponse")
    public void updateGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @throws EpicSDKCore202201DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Delete_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccountResponse")
    public void deleteGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationMethodID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationMethod")
    @WebResult(name = "Get_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethodResponse")
    public AllocationMethodsGetResult getGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationEntriesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationMethod")
    @WebResult(name = "Insert_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethodResponse")
    public Integer insertGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethodResponse")
    public void updateGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationMethodID
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethodResponse")
    public void deleteGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationStructureGroupingsID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Get_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGroupingResponse")
    public AllocationStructureGroupingsGetResult getGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGroupingsGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Insert_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGroupingResponse")
    public Integer insertGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGroupingResponse")
    public void updateGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202201UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationStructureGroupingsID
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGroupingResponse")
    public void deleteGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202201DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccountDefineBankAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param bankAccountObject
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountDefineBankAccount")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public void actionGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "BankAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        BankAccount bankAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param ignoreNonZeroBalance
     * @param chartOfAccountID
     * @param shouldInactivateReactivateSubaccounts
     * @param ignoreSubAccountNonZeroBalances
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountInactivateReactivate")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivate")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivateResponse")
    public void actionGeneralLedgerChartOfAccountInactivateReactivate(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "IgnoreNonZeroBalance", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreNonZeroBalance,
        @WebParam(name = "ShouldInactivateReactivateSubaccounts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean shouldInactivateReactivateSubaccounts,
        @WebParam(name = "IgnoreSubAccountNonZeroBalances", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreSubAccountNonZeroBalances,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202201ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode
     * @throws EpicSDKCore202201GetActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetActivityActivityCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Get_Activity_ActivityCode")
    @WebResult(name = "Get_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Get_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCode")
    @ResponseWrapper(localName = "Get_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCodeResponse")
    public ArrayOfActivityCode getActivityActivityCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param activityCodeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202201InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201InsertActivityActivityCodeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Insert_Activity_ActivityCode")
    @WebResult(name = "Insert_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Insert_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCode")
    @ResponseWrapper(localName = "Insert_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCodeResponse")
    public Integer insertActivityActivityCode(
        @WebParam(name = "ActivityCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCode activityCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202201InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult
     * @throws EpicSDKCore202201GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202201GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_GovernmentTaxFeeRate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureTaxFeeRate/Get_Configure_GovernmentTaxFeeRate")
    @WebResult(name = "Get_Configure_GovernmentTaxFeeRateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Configure_GovernmentTaxFeeRate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRate")
    @ResponseWrapper(localName = "Get_Configure_GovernmentTaxFeeRateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRateResponse")
    public TaxFeeRateGetResult getConfigureGovernmentTaxFeeRate(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TaxFeeRatesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202201GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
    ;

}
