
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKFileTransfer", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKFileTransfer {


    /**
     * 
     * @param fileDataStream
     * @param messageHeader
     * @return
     *     returns java.lang.String
     * @throws EpicSDKFileTransferUploadAttachmentFileInputValidationFaultFaultFaultMessage
     * @throws EpicSDKFileTransferUploadAttachmentFileAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKFileTransferUploadAttachmentFileMethodCallFaultFaultFaultMessage
     * @throws EpicSDKFileTransferUploadAttachmentFileConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Upload_Attachment_File", action = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/IAttachmentTransfer/Upload_Attachment_File")
    @WebResult(name = "Upload_Attachment_FileResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
    @RequestWrapper(localName = "Upload_Attachment_File", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.UploadAttachmentFile")
    @ResponseWrapper(localName = "Upload_Attachment_FileResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.UploadAttachmentFileResponse")
    public String uploadAttachmentFile(
        @WebParam(name = "FileDataStream", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
        byte[] fileDataStream,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKFileTransferUploadAttachmentFileAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileConcurrencyFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileInputValidationFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @return
     *     returns byte[]
     * @throws EpicSDKFileTransferDownloadAttachmentFileConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAttachmentFileMethodCallFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAttachmentFileAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAttachmentFileInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Download_Attachment_File", action = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/IAttachmentTransfer/Download_Attachment_File")
    @WebResult(name = "Download_Attachment_FileResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
    @RequestWrapper(localName = "Download_Attachment_File", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.DownloadAttachmentFile")
    @ResponseWrapper(localName = "Download_Attachment_FileResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.DownloadAttachmentFileResponse")
    public byte[] downloadAttachmentFile(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKFileTransferDownloadAttachmentFileAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileConcurrencyFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileInputValidationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param addOnAttachmentID
     * @param messageHeader
     * @return
     *     returns byte[]
     * @throws EpicSDKFileTransferDownloadAddOnAttachmentFileConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAddOnAttachmentFileInputValidationFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAddOnAttachmentFileAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKFileTransferDownloadAddOnAttachmentFileMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Download_AddOn_Attachment_File", action = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/IAttachmentTransfer/Download_AddOn_Attachment_File")
    @WebResult(name = "Download_AddOn_Attachment_FileResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
    @RequestWrapper(localName = "Download_AddOn_Attachment_File", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.DownloadAddOnAttachmentFile")
    @ResponseWrapper(localName = "Download_AddOn_Attachment_FileResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/", className = "com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.DownloadAddOnAttachmentFileResponse")
    public byte[] downloadAddOnAttachmentFile(
        @WebParam(name = "AddOnAttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/11/filetransfer/")
        Integer addOnAttachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKFileTransferDownloadAddOnAttachmentFileAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAddOnAttachmentFileConcurrencyFaultFaultFaultMessage, EpicSDKFileTransferDownloadAddOnAttachmentFileInputValidationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAddOnAttachmentFileMethodCallFaultFaultFaultMessage
    ;

}
