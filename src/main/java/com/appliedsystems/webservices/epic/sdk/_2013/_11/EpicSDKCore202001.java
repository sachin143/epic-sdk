
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalPartyGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdjustorGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Summary;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.CancellationRequestReasonMethodGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.AccountsReceivableWriteOff;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.AdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.JournalEntry;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Receipt;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Voucher;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyAdditionalInterestGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineTypeGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._client.SplitReceivableTemplateGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccountGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKCore_2020_01", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKCore202001 {


    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param clientFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult
     * @throws EpicSDKCore202001GetClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client")
    @WebResult(name = "Get_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClient")
    @ResponseWrapper(localName = "Get_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientResponse")
    public ClientGetResult getClient(
        @WebParam(name = "ClientFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ClientFilter clientFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client")
    @WebResult(name = "Insert_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClient")
    @ResponseWrapper(localName = "Insert_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientResponse")
    public Integer insertClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @throws EpicSDKCore202001UpdateClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client")
    @RequestWrapper(localName = "Update_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClient")
    @ResponseWrapper(localName = "Update_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientResponse")
    public void updateClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClientInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate
     * @throws EpicSDKCore202001GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client_SplitReceivableTemplate")
    @WebResult(name = "Get_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Get_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplateResponse")
    public ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplate(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplateGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @throws EpicSDKCore202001UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Update_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Update_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplateResponse")
    public void updateClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client_SplitReceivableTemplate")
    @WebResult(name = "Insert_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Insert_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplateResponse")
    public Integer insertClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateID
     * @throws EpicSDKCore202001DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Delete_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Delete_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Delete_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplateResponse")
    public void deleteClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer splitReceivableTemplateID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Insert_Activity")
    @WebResult(name = "Insert_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivity")
    @ResponseWrapper(localName = "Insert_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivityResponse")
    public Integer insertActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Update_Activity")
    @RequestWrapper(localName = "Update_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivity")
    @ResponseWrapper(localName = "Update_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivityResponse")
    public void updateActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202001GetActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Get_Activity")
    @WebResult(name = "Get_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivity")
    @ResponseWrapper(localName = "Get_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivityResponse")
    public ActivityGetResult getActivity(
        @WebParam(name = "ActivityFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityFilter activityFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param lineFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult
     * @throws EpicSDKCore202001GetLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/ILine_2017_02/Get_Line")
    @WebResult(name = "Get_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetLine")
    @ResponseWrapper(localName = "Get_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetLineResponse")
    public LineGetResult getLine(
        @WebParam(name = "LineFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        LineFilter lineFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetLineInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/ILine_2017_02/Insert_Line")
    @WebResult(name = "Insert_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertLine")
    @ResponseWrapper(localName = "Insert_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertLineResponse")
    public Integer insertLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertLineInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @throws EpicSDKCore202001UpdateLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateLineAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/ILine_2017_02/Update_Line")
    @RequestWrapper(localName = "Update_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateLine")
    @ResponseWrapper(localName = "Update_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateLineResponse")
    public void updateLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateLineInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineID
     * @throws EpicSDKCore202001DeleteLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteLineConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/ILine_2017_02/Delete_Line")
    @RequestWrapper(localName = "Delete_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteLine")
    @ResponseWrapper(localName = "Delete_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteLineResponse")
    public void deleteLine(
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteLineInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param contactFilterObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult
     * @throws EpicSDKCore202001GetContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Get_Contact")
    @WebResult(name = "Get_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContact")
    @ResponseWrapper(localName = "Get_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContactResponse")
    public ContactGetResult getContact(
        @WebParam(name = "ContactFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ContactFilter contactFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param id
     * @throws EpicSDKCore202001DeleteContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Delete_Contact")
    @RequestWrapper(localName = "Delete_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContact")
    @ResponseWrapper(localName = "Delete_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContactResponse")
    public void deleteContact(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Insert_Contact")
    @WebResult(name = "Insert_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContact")
    @ResponseWrapper(localName = "Insert_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContactResponse")
    public Integer insertContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @throws EpicSDKCore202001UpdateContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Update_Contact")
    @RequestWrapper(localName = "Update_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContact")
    @ResponseWrapper(localName = "Update_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContactResponse")
    public void updateContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactID
     * @throws EpicSDKCore202001ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Contact_ChangeMainBusinessContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Action_Contact_ChangeMainBusinessContact")
    @RequestWrapper(localName = "Action_Contact_ChangeMainBusinessContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContact")
    @ResponseWrapper(localName = "Action_Contact_ChangeMainBusinessContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContactResponse")
    public void actionContactChangeMainBusinessContact(
        @WebParam(name = "ContactID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer contactID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param policyFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult
     * @throws EpicSDKCore202001GetPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy")
    @WebResult(name = "Get_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicy")
    @ResponseWrapper(localName = "Get_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyResponse")
    public PolicyGetResult getPolicy(
        @WebParam(name = "PolicyFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PolicyFilter policyFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Insert_Policy")
    @WebResult(name = "Insert_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicy")
    @ResponseWrapper(localName = "Insert_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicyResponse")
    public Integer insertPolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @throws EpicSDKCore202001UpdatePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Update_Policy")
    @RequestWrapper(localName = "Update_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicy")
    @ResponseWrapper(localName = "Update_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyResponse")
    public void updatePolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202001DeletePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Delete_Policy")
    @RequestWrapper(localName = "Delete_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicy")
    @ResponseWrapper(localName = "Delete_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicyResponse")
    public void deletePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeletePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_ActivateInactivateMultiCarrierSchedule")
    @RequestWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyActivateInactivateMultiCarrierSchedule")
    @ResponseWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyActivateInactivateMultiCarrierScheduleResponse")
    public void actionPolicyActivateInactivateMultiCarrierSchedule(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001ActionPolicyCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyCancelInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyCancelMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Cancel", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_Cancel")
    @WebResult(name = "Action_Policy_CancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Action_Policy_Cancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyCancel")
    @ResponseWrapper(localName = "Action_Policy_CancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyCancelResponse")
    public Integer actionPolicyCancel(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202001GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionCancel", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionCancel")
    @WebResult(name = "Get_Policy_DefaultActionCancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionCancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionCancel")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionCancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionCancelResponse")
    public ArrayOfCancel getPolicyDefaultActionCancel(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202001GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_CancellationRequestReasonMethod")
    @WebResult(name = "Get_Policy_CancellationRequestReasonMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Get_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancellationRequestReasonMethodResponse")
    public ArrayOfCancel getPolicyCancellationRequestReasonMethod(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        CancellationRequestReasonMethodGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Update_Policy_CancellationRequestReasonMethod")
    @RequestWrapper(localName = "Update_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Update_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancellationRequestReasonMethodResponse")
    public void updatePolicyCancellationRequestReasonMethod(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param lineID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates
     * @throws EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates")
    @WebResult(name = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse")
    public ArrayOfChangePolicyEffectiveExpirationDates getPolicyDefaultActionChangePolicyEffectiveExpirationDates(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param changePolicyEffectiveExpirationDatesObject
     * @throws EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_ChangePolicyEffectiveExpirationDates")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangePolicyEffectiveExpirationDatesResponse")
    public void actionPolicyChangePolicyEffectiveExpirationDates(
        @WebParam(name = "ChangePolicyEffectiveExpirationDatesObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyProspectiveContractedStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_ChangePolicyProspectiveContractedStatus")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangePolicyProspectiveContractedStatus")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangePolicyProspectiveContractedStatusResponse")
    public void actionPolicyChangePolicyProspectiveContractedStatus(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param changeServiceSummaryDescriptionObject
     * @param messageHeader
     * @throws EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangeServiceSummaryDescription", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_ChangeServiceSummaryDescription")
    @RequestWrapper(localName = "Action_Policy_ChangeServiceSummaryDescription", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangeServiceSummaryDescription")
    @ResponseWrapper(localName = "Action_Policy_ChangeServiceSummaryDescriptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyChangeServiceSummaryDescriptionResponse")
    public void actionPolicyChangeServiceSummaryDescription(
        @WebParam(name = "ChangeServiceSummaryDescriptionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param endorseReviseAddLineMidtermObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseAddLineMidterm", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_EndorseReviseAddLineMidterm")
    @WebResult(name = "Action_Policy_EndorseReviseAddLineMidtermResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseAddLineMidterm", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyEndorseReviseAddLineMidterm")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseAddLineMidtermResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyEndorseReviseAddLineMidtermResponse")
    public ArrayOfint actionPolicyEndorseReviseAddLineMidterm(
        @WebParam(name = "EndorseReviseAddLineMidtermObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param endorseReviseExistingLineObject
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseExistingLine", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_EndorseReviseExistingLine")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseExistingLine", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyEndorseReviseExistingLine")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseExistingLineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyEndorseReviseExistingLineResponse")
    public void actionPolicyEndorseReviseExistingLine(
        @WebParam(name = "EndorseReviseExistingLineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        EndorseReviseExistingLine endorseReviseExistingLineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssueEndorsementObject
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_IssueNotIssueEndorsement")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueNotIssueEndorsementResponse")
    public void actionPolicyIssueNotIssueEndorsement(
        @WebParam(name = "IssueNotIssueEndorsementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        IssueNotIssueEndorsement issueNotIssueEndorsementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionIssueNotIssueEndorsement")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssueEndorsementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueNotIssueEndorsementResponse")
    public ArrayOfIssueNotIssueEndorsement getPolicyDefaultActionIssueNotIssueEndorsement(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        IssueNotIssueEndorsementGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssuePolicyObject
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_IssueNotIssuePolicy")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueNotIssuePolicyResponse")
    public void actionPolicyIssueNotIssuePolicy(
        @WebParam(name = "IssueNotIssuePolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        IssueNotIssuePolicy issueNotIssuePolicyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionIssueNotIssuePolicy")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssuePolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueNotIssuePolicyResponse")
    public ArrayOfIssueNotIssuePolicy getPolicyDefaultActionIssueNotIssuePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        IssueNotIssuePolicyGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueCancellationObject
     * @throws EpicSDKCore202001ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_IssueCancellation")
    @RequestWrapper(localName = "Action_Policy_IssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueCancellation")
    @ResponseWrapper(localName = "Action_Policy_IssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyIssueCancellationResponse")
    public void actionPolicyIssueCancellation(
        @WebParam(name = "IssueCancellationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        IssueCancellation issueCancellationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionIssueCancellation")
    @WebResult(name = "Get_Policy_DefaultActionIssueCancellationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueCancellation")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionIssueCancellationResponse")
    public ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellation(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer serviceSummaryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reinstateObject
     * @throws EpicSDKCore202001ActionPolicyReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyReinstateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Reinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_Reinstate")
    @RequestWrapper(localName = "Action_Policy_Reinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyReinstate")
    @ResponseWrapper(localName = "Action_Policy_ReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyReinstateResponse")
    public void actionPolicyReinstate(
        @WebParam(name = "ReinstateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Reinstate reinstateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate
     * @throws EpicSDKCore202001GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionReinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionReinstate")
    @WebResult(name = "Get_Policy_DefaultActionReinstateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionReinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionReinstate")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionReinstateResponse")
    public ArrayOfReinstate getPolicyDefaultActionReinstate(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param renewObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001ActionPolicyRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyRenewConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Renew", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_Renew")
    @WebResult(name = "Action_Policy_RenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Action_Policy_Renew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyRenew")
    @ResponseWrapper(localName = "Action_Policy_RenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyRenewResponse")
    public Integer actionPolicyRenew(
        @WebParam(name = "RenewObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Renew renewObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew
     * @throws EpicSDKCore202001GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionRenew", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionRenew")
    @WebResult(name = "Get_Policy_DefaultActionRenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionRenew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionRenew")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionRenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionRenewResponse")
    public ArrayOfRenew getPolicyDefaultActionRenew(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted
     * @throws EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_DefaultActionUpdateStageToSubmitted")
    @WebResult(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyDefaultActionUpdateStageToSubmittedResponse")
    public ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmitted(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param oSubmissionObject
     * @param messageHeader
     * @throws EpicSDKCore202001ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Action_Policy_UpdateStageToSubmitted")
    @RequestWrapper(localName = "Action_Policy_UpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Action_Policy_UpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionPolicyUpdateStageToSubmittedResponse")
    public void actionPolicyUpdateStageToSubmitted(
        @WebParam(name = "oSubmissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        UpdateStageToSubmitted oSubmissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyAdditionalInterestsIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest
     * @throws EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Get_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancelPolicyAdditionalInterestResponse")
    public ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "CancelPolicyAdditionalInterestsIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        CancelPolicyAdditionalInterestGetType cancelPolicyAdditionalInterestsIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Insert_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Insert_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicyCancelPolicyAdditionalInterestResponse")
    public Integer insertPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Update_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancelPolicyAdditionalInterestResponse")
    public void updatePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalInterestID
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Delete_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicyCancelPolicyAdditionalInterestResponse")
    public void deletePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "AdditionalInterestID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalInterestID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyRemarksIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark
     * @throws EpicSDKCore202001GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Get_Policy_CancelPolicyRemark")
    @WebResult(name = "Get_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetPolicyCancelPolicyRemarkResponse")
    public ArrayOfRemark getPolicyCancelPolicyRemark(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "CancelPolicyRemarksIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        CancelPolicyRemarksGetType cancelPolicyRemarksIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Insert_Policy_CancelPolicyRemark")
    @WebResult(name = "Insert_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertPolicyCancelPolicyRemarkResponse")
    public Integer insertPolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Update_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdatePolicyCancelPolicyRemarkResponse")
    public void updatePolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param remarkID
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/Policy_2017_02/Delete_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeletePolicyCancelPolicyRemarkResponse")
    public void deletePolicyCancelPolicyRemark(
        @WebParam(name = "RemarkID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer remarkID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001InsertAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertAttachmentAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Insert_Attachment")
    @WebResult(name = "Insert_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertAttachment")
    @ResponseWrapper(localName = "Insert_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertAttachmentResponse")
    public ArrayOfint insertAttachment(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @param attachmentFilterObject
     * @param attachmentSortingObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult
     * @throws EpicSDKCore202001GetAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetAttachmentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Get_Attachment")
    @WebResult(name = "Get_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetAttachment")
    @ResponseWrapper(localName = "Get_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetAttachmentResponse")
    public AttachmentGetResult getAttachment(
        @WebParam(name = "AttachmentFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AttachmentFilter attachmentFilterObject,
        @WebParam(name = "AttachmentSortingObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AttachmentSorting attachmentSortingObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Details", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Update_Attachment_Details")
    @RequestWrapper(localName = "Update_Attachment_Details", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentDetails")
    @ResponseWrapper(localName = "Update_Attachment_DetailsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentDetailsResponse")
    public void updateAttachmentDetails(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveFolder", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Update_Attachment_MoveFolder")
    @RequestWrapper(localName = "Update_Attachment_MoveFolder", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentMoveFolder")
    @ResponseWrapper(localName = "Update_Attachment_MoveFolderResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentMoveFolderResponse")
    public void updateAttachmentMoveFolder(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Update_Attachment_MoveAccount")
    @RequestWrapper(localName = "Update_Attachment_MoveAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentMoveAccount")
    @ResponseWrapper(localName = "Update_Attachment_MoveAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentMoveAccountResponse")
    public void updateAttachmentMoveAccount(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Binary", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Update_Attachment_Binary")
    @WebResult(name = "Update_Attachment_BinaryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Update_Attachment_Binary", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentBinary")
    @ResponseWrapper(localName = "Update_Attachment_BinaryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateAttachmentBinaryResponse")
    public Integer updateAttachmentBinary(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202001DeleteAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteAttachmentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IAttachment_2019_01/Delete_Attachment")
    @RequestWrapper(localName = "Delete_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteAttachment")
    @ResponseWrapper(localName = "Delete_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteAttachmentResponse")
    public void deleteAttachment(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202001GetTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction")
    @WebResult(name = "Get_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransaction")
    @ResponseWrapper(localName = "Get_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionResponse")
    public TransactionGetResult getTransaction(
        @WebParam(name = "TransactionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionFilter transactionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001InsertTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Insert_Transaction")
    @WebResult(name = "Insert_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransaction")
    @ResponseWrapper(localName = "Insert_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransactionResponse")
    public ArrayOfint insertTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @throws EpicSDKCore202001UpdateTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Update_Transaction")
    @RequestWrapper(localName = "Update_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransaction")
    @ResponseWrapper(localName = "Update_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransactionResponse")
    public void updateTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @param installmentType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202001GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultInstallments", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultInstallments")
    @WebResult(name = "Get_Transaction_DefaultInstallmentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultInstallments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallments")
    @ResponseWrapper(localName = "Get_Transaction_DefaultInstallmentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallmentsResponse")
    public TransactionGetResult getTransactionDefaultInstallments(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "InstallmentType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionGetInstallmentType installmentType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultProducerBrokerCommissions", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultProducerBrokerCommissions")
    @WebResult(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissions", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissions")
    @ResponseWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissionsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissionsResponse")
    public TransactionGetResult getTransactionDefaultProducerBrokerCommissions(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable
     * @throws EpicSDKCore202001GetTransactionReceivablesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionReceivablesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionReceivablesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionReceivablesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_Receivables", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_Receivables")
    @WebResult(name = "Get_Transaction_ReceivablesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_Receivables", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivables")
    @ResponseWrapper(localName = "Get_Transaction_ReceivablesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivablesResponse")
    public ArrayOfReceivable getTransactionReceivables(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionReceivablesAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionReceivablesConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionReceivablesInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionReceivablesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param accountsReceivableWriteOffObject
     * @throws EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AccountsReceivableWriteOff", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AccountsReceivableWriteOff")
    @RequestWrapper(localName = "Action_Transaction_AccountsReceivableWriteOff", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOff")
    @ResponseWrapper(localName = "Action_Transaction_AccountsReceivableWriteOffResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOffResponse")
    public void actionTransactionAccountsReceivableWriteOff(
        @WebParam(name = "AccountsReceivableWriteOffObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AccountsReceivableWriteOff accountsReceivableWriteOffObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param applyCreditsToDebitsObject
     * @throws EpicSDKCore202001ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ApplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_ApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_ApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebitsResponse")
    public void actionTransactionApplyCreditsToDebits(
        @WebParam(name = "ApplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ApplyCreditsToDebits applyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param associatedAccountID
     * @param messageHeader
     * @param associatedAccountTypeCode
     * @param agencyCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits
     * @throws EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionApplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebitsResponse")
    public ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebits(
        @WebParam(name = "AssociatedAccountTypeCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String associatedAccountTypeCode,
        @WebParam(name = "AssociatedAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer associatedAccountID,
        @WebParam(name = "AgencyCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String agencyCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param financeTransactionObject
     * @throws EpicSDKCore202001ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_FinanceTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_FinanceTransaction")
    @RequestWrapper(localName = "Action_Transaction_FinanceTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransaction")
    @ResponseWrapper(localName = "Action_Transaction_FinanceTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransactionResponse")
    public void actionTransactionFinanceTransaction(
        @WebParam(name = "FinanceTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        FinanceTransaction financeTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @throws EpicSDKCore202001ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_MoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_MoveTransaction")
    @RequestWrapper(localName = "Action_Transaction_MoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransaction")
    @ResponseWrapper(localName = "Action_Transaction_MoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransactionResponse")
    public void actionTransactionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction
     * @throws EpicSDKCore202001GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionMoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionMoveTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionMoveTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionMoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionMoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransactionResponse")
    public ArrayOfMoveTransaction getTransactionDefaultActionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction
     * @throws EpicSDKCore202001GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionReverseTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionReverseTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransactionResponse")
    public ArrayOfReverseTransaction getTransactionDefaultActionReverseTransaction(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reverseTransactionObject
     * @throws EpicSDKCore202001ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ReverseTransaction")
    @RequestWrapper(localName = "Action_Transaction_ReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransaction")
    @ResponseWrapper(localName = "Action_Transaction_ReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransactionResponse")
    public void actionTransactionReverseTransaction(
        @WebParam(name = "ReverseTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ReverseTransaction reverseTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param unapplyCreditsToDebitsObject
     * @param messageHeader
     * @throws EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_UnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_UnapplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_UnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_UnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebitsResponse")
    public void actionTransactionUnapplyCreditsToDebits(
        @WebParam(name = "UnapplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        UnapplyCreditsToDebits unapplyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionDetailNumber
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits
     * @throws EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionUnapplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebitsResponse")
    public ArrayOfUnapplyCreditsToDebits getTransactionDefaultActionUnapplyCreditsToDebits(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "TransactionDetailNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionDetailNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voidPaymentObject
     * @param messageHeader
     * @throws EpicSDKCore202001ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_VoidPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_VoidPayment")
    @RequestWrapper(localName = "Action_Transaction_VoidPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPayment")
    @ResponseWrapper(localName = "Action_Transaction_VoidPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPaymentResponse")
    public void actionTransactionVoidPayment(
        @WebParam(name = "VoidPaymentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        VoidPayment voidPaymentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission
     * @throws EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionAdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionAdjustCommission")
    @WebResult(name = "Get_Transaction_DefaultActionAdjustCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionAdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommission")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionAdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommissionResponse")
    public ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommission(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustCommissionObject
     * @throws EpicSDKCore202001ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AdjustCommission")
    @RequestWrapper(localName = "Action_Transaction_AdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommission")
    @ResponseWrapper(localName = "Action_Transaction_AdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommissionResponse")
    public void actionTransactionAdjustCommission(
        @WebParam(name = "AdjustCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AdjustCommission adjustCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium
     * @throws EpicSDKCore202001GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionRevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionRevisePremium")
    @WebResult(name = "Get_Transaction_DefaultActionRevisePremiumResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionRevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremium")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionRevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremiumResponse")
    public ArrayOfRevisePremium getTransactionDefaultActionRevisePremium(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param revisePremiumObject
     * @throws EpicSDKCore202001ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_RevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_RevisePremium")
    @RequestWrapper(localName = "Action_Transaction_RevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremium")
    @ResponseWrapper(localName = "Action_Transaction_RevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremiumResponse")
    public void actionTransactionRevisePremium(
        @WebParam(name = "RevisePremiumObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        RevisePremium revisePremiumObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee
     * @throws EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionGenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionGenerateTaxFee")
    @WebResult(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFee")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFeeResponse")
    public ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFee(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param generateTaxFeeObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_GenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_GenerateTaxFee")
    @WebResult(name = "Action_Transaction_GenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_Transaction_GenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFee")
    @ResponseWrapper(localName = "Action_Transaction_GenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFeeResponse")
    public ArrayOfint actionTransactionGenerateTaxFee(
        @WebParam(name = "GenerateTaxFeeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        GenerateTaxFee generateTaxFeeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim")
    @WebResult(name = "Insert_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaim")
    @ResponseWrapper(localName = "Insert_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimResponse")
    public Integer insertClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param claimSummaryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult
     * @throws EpicSDKCore202001GetClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim")
    @WebResult(name = "Get_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaim")
    @ResponseWrapper(localName = "Get_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimResponse")
    public ClaimSummaryGetResult getClaim(
        @WebParam(name = "ClaimSummaryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ClaimSummaryFilter claimSummaryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @throws EpicSDKCore202001DeleteClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim")
    @RequestWrapper(localName = "Delete_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaim")
    @ResponseWrapper(localName = "Delete_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimResponse")
    public void deleteClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @throws EpicSDKCore202001UpdateClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim")
    @RequestWrapper(localName = "Update_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaim")
    @ResponseWrapper(localName = "Update_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimResponse")
    public void updateClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty
     * @throws EpicSDKCore202001GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_AdditionalParty")
    @WebResult(name = "Get_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalParty")
    @ResponseWrapper(localName = "Get_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalPartyResponse")
    public ArrayOfAdditionalParty getClaimAdditionalParty(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalPartyGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param additionalPartyID
     * @param messageHeader
     * @throws EpicSDKCore202001DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_AdditionalParty")
    @RequestWrapper(localName = "Delete_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalParty")
    @ResponseWrapper(localName = "Delete_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalPartyResponse")
    public void deleteClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalPartyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_AdditionalParty")
    @WebResult(name = "Insert_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalParty")
    @ResponseWrapper(localName = "Insert_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalPartyResponse")
    public Integer insertClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @throws EpicSDKCore202001UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_AdditionalParty")
    @RequestWrapper(localName = "Update_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalParty")
    @ResponseWrapper(localName = "Update_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalPartyResponse")
    public void updateClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor
     * @throws EpicSDKCore202001GetClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimAdjustorInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Adjustor")
    @WebResult(name = "Get_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustor")
    @ResponseWrapper(localName = "Get_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustorResponse")
    public ArrayOfAdjustor getClaimAdjustor(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdjustorGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_Adjustor")
    @WebResult(name = "Insert_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustor")
    @ResponseWrapper(localName = "Insert_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustorResponse")
    public Integer insertClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @throws EpicSDKCore202001UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Adjustor")
    @RequestWrapper(localName = "Update_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustor")
    @ResponseWrapper(localName = "Update_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustorResponse")
    public void updateClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param adjustorID
     * @param messageHeader
     * @throws EpicSDKCore202001DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_Adjustor")
    @RequestWrapper(localName = "Delete_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustor")
    @ResponseWrapper(localName = "Delete_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustorResponse")
    public void deleteClaimAdjustor(
        @WebParam(name = "AdjustorID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer adjustorID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact
     * @throws EpicSDKCore202001GetClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimInsuredContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_InsuredContact")
    @WebResult(name = "Get_Claim_InsuredContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContact")
    @ResponseWrapper(localName = "Get_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContactResponse")
    public ArrayOfInsuredContact getClaimInsuredContact(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param insuredContactObject
     * @throws EpicSDKCore202001UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_InsuredContact")
    @RequestWrapper(localName = "Update_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContact")
    @ResponseWrapper(localName = "Update_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContactResponse")
    public void updateClaimInsuredContact(
        @WebParam(name = "InsuredContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        InsuredContact insuredContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation
     * @throws EpicSDKCore202001GetClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimLitigationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Litigation")
    @WebResult(name = "Get_Claim_LitigationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigation")
    @ResponseWrapper(localName = "Get_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigationResponse")
    public ArrayOfLitigation getClaimLitigation(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param litigationObject
     * @throws EpicSDKCore202001UpdateClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimLitigationAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Litigation")
    @RequestWrapper(localName = "Update_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigation")
    @ResponseWrapper(localName = "Update_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigationResponse")
    public void updateClaimLitigation(
        @WebParam(name = "LitigationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Litigation litigationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense
     * @throws EpicSDKCore202001GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_PaymentExpense")
    @WebResult(name = "Get_Claim_PaymentExpenseResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpense")
    @ResponseWrapper(localName = "Get_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpenseResponse")
    public ArrayOfPaymentExpense getClaimPaymentExpense(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param paymentExpenseObject
     * @throws EpicSDKCore202001UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_PaymentExpense")
    @RequestWrapper(localName = "Update_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpense")
    @ResponseWrapper(localName = "Update_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpenseResponse")
    public void updateClaimPaymentExpense(
        @WebParam(name = "PaymentExpenseObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PaymentExpense paymentExpenseObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts
     * @throws EpicSDKCore202001GetClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimServicingContactsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetClaimServicingContactsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_ServicingContacts")
    @WebResult(name = "Get_Claim_ServicingContactsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContacts")
    @ResponseWrapper(localName = "Get_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContactsResponse")
    public ArrayOfServicingContacts getClaimServicingContacts(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param servicingContactObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_ServicingContacts")
    @RequestWrapper(localName = "Update_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContacts")
    @ResponseWrapper(localName = "Update_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContactsResponse")
    public void updateClaimServicingContacts(
        @WebParam(name = "ServicingContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ServicingContacts servicingContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param closedDate
     * @param claimID
     * @throws EpicSDKCore202001ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Claim_CloseReopenClaim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Action_Claim_CloseReopenClaim")
    @RequestWrapper(localName = "Action_Claim_CloseReopenClaim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaim")
    @ResponseWrapper(localName = "Action_Claim_CloseReopenClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaimResponse")
    public void actionClaimCloseReopenClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "ClosedDate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        XMLGregorianCalendar closedDate,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param searchTerms
     * @param lookupTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup
     * @throws EpicSDKCore202001GetLookupInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLookupAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLookupConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetLookupMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Lookup", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/ILookup/Get_Lookup")
    @WebResult(name = "Get_LookupResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Lookup", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookup")
    @ResponseWrapper(localName = "Get_LookupResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookupResponse")
    public ArrayOfLookup getLookup(
        @WebParam(name = "LookupTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        LookupTypes lookupTypeObject,
        @WebParam(name = "SearchTerms", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ArrayOfstring searchTerms,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetLookupMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param optionTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType
     * @throws EpicSDKCore202001GetOptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetOptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetOptionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetOptionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Option", action = "http://webservices.appliedsystems.com/epic/sdk/2009/07/IOption/Get_Option")
    @WebResult(name = "Get_OptionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
    @RequestWrapper(localName = "Get_Option", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOption")
    @ResponseWrapper(localName = "Get_OptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOptionResponse")
    public ArrayOfOptionType getOption(
        @WebParam(name = "OptionTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
        OptionTypes optionTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetOptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @param receiptID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Get_GeneralLedger_Receipt")
    @WebResult(name = "Get_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerReceiptResponse")
    public ReceiptGetResult getGeneralLedgerReceipt(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ReceiptGetType searchType,
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer receiptID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ReceiptGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ReceiptFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ReceiptComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Insert_GeneralLedger_Receipt")
    @WebResult(name = "Insert_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerReceiptResponse")
    public Integer insertGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Update_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Update_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerReceiptResponse")
    public void updateGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202001DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Delete_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Delete_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteGeneralLedgerReceiptResponse")
    public void deleteGeneralLedgerReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @param detailItemToBeInsertedObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerReceiptDefaultApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultApplyCreditsToDebits(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer receiptID,
        @WebParam(name = "DetailItemToBeInsertedObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        DetailItem detailItemToBeInsertedObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptFinalizeReceipt", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Action_GeneralLedger_ReceiptFinalizeReceipt")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionGeneralLedgerReceiptFinalizeReceipt")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionGeneralLedgerReceiptFinalizeReceiptResponse")
    public void actionGeneralLedgerReceiptFinalizeReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IReceipt2017_02/Action_GeneralLedger_ReceiptTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionGeneralLedgerReceiptTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionGeneralLedgerReceiptTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReceiptTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param journalEntryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryFilter journalEntryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntryDefaultDefaultEntry(
        @WebParam(name = "JournalEntryDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryDefaultEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Insert_GeneralLedger_JournalEntry")
    @WebResult(name = "Insert_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Insert_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntryResponse")
    public ArrayOfint insertGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @throws EpicSDKCore202001UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Update_GeneralLedger_JournalEntry")
    @RequestWrapper(localName = "Update_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Update_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntryResponse")
    public void updateGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_JournalEntryTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerJournalEntryTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryVoidObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryVoid")
    @WebResult(name = "Action_GeneralLedger_JournalEntryVoidResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoidResponse")
    public Integer actionGeneralLedgerJournalEntryVoid(
        @WebParam(name = "JournalEntryVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryVoid journalEntryVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param journalEntryID
     * @param messageHeader
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntrySubmit", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntrySubmit")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntrySubmit", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmit")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntrySubmitResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmitResponse")
    public void actionGeneralLedgerJournalEntrySubmit(
        @WebParam(name = "JournalEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_DisbursementTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerDisbursementTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementPayVouchers")
    @WebResult(name = "Action_GeneralLedger_DisbursementPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerDisbursementPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementVoidObject
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoidResponse")
    public void actionGeneralLedgerDisbursementVoid(
        @WebParam(name = "DisbursementVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementVoid disbursementVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param disbursementID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_Disbursement")
    @WebResult(name = "Get_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementResponse")
    public DisbursementGetResult getGeneralLedgerDisbursement(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetType searchType,
        @WebParam(name = "DisbursementID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Insert_GeneralLedger_Disbursement")
    @WebResult(name = "Insert_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Insert_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursementResponse")
    public Integer insertGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @throws EpicSDKCore202001UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Update_GeneralLedger_Disbursement")
    @RequestWrapper(localName = "Update_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Update_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursementResponse")
    public void updateGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param disbursementBankAccountNumberCode
     * @param messageHeader
     * @param disbursementBankSubAccountNumberCode
     * @param disbursementDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntryResponse")
    public DisbursementGetResult getGeneralLedgerDisbursementDefaultDefaultEntry(
        @WebParam(name = "DisbursementDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementDefaultEntryID,
        @WebParam(name = "DisbursementBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankAccountNumberCode,
        @WebParam(name = "DisbursementBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankSubAccountNumberCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerVoucherTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherPayVouchers")
    @WebResult(name = "Action_GeneralLedger_VoucherPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerVoucherPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerVoucherDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherVoidObject
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoidResponse")
    public void actionGeneralLedgerVoucherVoid(
        @WebParam(name = "VoucherVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherVoid voucherVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Insert_GeneralLedger_Voucher")
    @WebResult(name = "Insert_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Insert_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucherResponse")
    public Integer insertGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @throws EpicSDKCore202001UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Update_GeneralLedger_Voucher")
    @RequestWrapper(localName = "Update_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Update_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucherResponse")
    public void updateGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param voucherID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_Voucher")
    @WebResult(name = "Get_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherResponse")
    public VoucherGetResult getGeneralLedgerVoucher(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetType searchType,
        @WebParam(name = "VoucherID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voucherDefaultEntryID
     * @param voucherAccountingMonth
     * @param messageHeader
     * @param voucherBankSubAccountNumberCode
     * @param voucherBankAccountNumberCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntryResponse")
    public VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntry(
        @WebParam(name = "VoucherDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherDefaultEntryID,
        @WebParam(name = "VoucherBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankAccountNumberCode,
        @WebParam(name = "VoucherBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankSubAccountNumberCode,
        @WebParam(name = "VoucherAccountingMonth", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherAccountingMonth,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param id
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Get_GeneralLedger_Activity")
    @WebResult(name = "Get_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivity")
    @ResponseWrapper(localName = "Get_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivityResponse")
    public ActivityGetResult getGeneralLedgerActivity(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetType searchType,
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Insert_GeneralLedger_Activity")
    @WebResult(name = "Insert_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivity")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivityResponse")
    public Integer insertGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Update_GeneralLedger_Activity")
    @RequestWrapper(localName = "Update_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivity")
    @ResponseWrapper(localName = "Update_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivityResponse")
    public void updateGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param directBillCommissionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommissionResponse")
    public DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "DirectBillCommissionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommissionFilter directBillCommissionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommissionResponse")
    public ArrayOfint insertGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void updateGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionID
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void deleteGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationDirectBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param lDepartments
     * @param lEntityLookupCodes
     * @param messageHeader
     * @param lBranches
     * @param lProfitCenters
     * @param lAgencies
     * @param sCompareByType
     * @param lIssuingCompanies
     * @param sClientOrPolicyNumber
     * @param fIncludeHistory
     * @param lLineOfBusiness
     * @param sEntityType
     * @param sDescriptionType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
    public RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies(
        @WebParam(name = "sEntityType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sEntityType,
        @WebParam(name = "lEntityLookupCodes", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lEntityLookupCodes,
        @WebParam(name = "sDescriptionType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sDescriptionType,
        @WebParam(name = "sCompareByType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sCompareByType,
        @WebParam(name = "sClientOrPolicyNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sClientOrPolicyNumber,
        @WebParam(name = "lAgencies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lAgencies,
        @WebParam(name = "lBranches", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lBranches,
        @WebParam(name = "lDepartments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lDepartments,
        @WebParam(name = "lProfitCenters", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lProfitCenters,
        @WebParam(name = "lIssuingCompanies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lIssuingCompanies,
        @WebParam(name = "lLineOfBusiness", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lLineOfBusiness,
        @WebParam(name = "fIncludeHistory", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean fIncludeHistory,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReconciliationBankTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankReopenStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankReopenStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatementResponse")
    public void actionGeneralLedgerReconciliationBankReopenStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationBankFinalizeStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param bankFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Get_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBankResponse")
    public BankGetResult getGeneralLedgerReconciliationBank(
        @WebParam(name = "BankFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankFilter bankFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBankResponse")
    public Integer insertGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBankResponse")
    public void updateGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reconciliationBankID
     * @param messageHeader
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBankResponse")
    public void deleteGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationBankID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType
     * @throws EpicSDKCore202001GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Get_Policy_PolicyLineType")
    @WebResult(name = "Get_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineType")
    @ResponseWrapper(localName = "Get_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineTypeResponse")
    public ArrayOfPolicyLineType getPolicyPolicyLineType(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineTypeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyLineTypeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyLineType")
    @WebResult(name = "Insert_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineType")
    @ResponseWrapper(localName = "Insert_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineTypeResponse")
    public Integer insertPolicyPolicyLineType(
        @WebParam(name = "PolicyLineTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineType policyLineTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyStatusObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyStatus")
    @WebResult(name = "Insert_Policy_PolicyStatusResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatus")
    @ResponseWrapper(localName = "Insert_Policy_PolicyStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatusResponse")
    public Integer insertPolicyPolicyStatus(
        @WebParam(name = "PolicyStatusObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyStatus policyStatusObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode
     * @throws EpicSDKCore202001GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Get_Transaction_TransactionCode")
    @WebResult(name = "Get_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Get_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCode")
    @ResponseWrapper(localName = "Get_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCodeResponse")
    public ArrayOfTransactionCode getTransactionTransactionCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionCodeObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Insert_Transaction_TransactionCode")
    @WebResult(name = "Insert_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Insert_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCode")
    @ResponseWrapper(localName = "Insert_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCodeResponse")
    public Integer insertTransactionTransactionCode(
        @WebParam(name = "TransactionCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCode transactionCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult
     * @throws EpicSDKCore202001GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_SalesTeams", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IConfigureSalesTeam/Get_Configure_SalesTeams")
    @WebResult(name = "Get_Configure_SalesTeamsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_Configure_SalesTeams", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeams")
    @ResponseWrapper(localName = "Get_Configure_SalesTeamsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeamsResponse")
    public SalesTeamGetResult getConfigureSalesTeams(
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Insert_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Insert_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerChartOfAccountResponse")
    public Integer insertGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ChartOfAccount chartOfObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerChartOfAccountResponse")
    public ChartOfAccountGetResult getGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer chartOfAccountID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ChartOfAccountGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean includeInactive,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param chartOfAccountObject
     * @param messageHeader
     * @throws EpicSDKCore202001UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Update_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Update_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Update_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerChartOfAccountResponse")
    public void updateGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ChartOfAccount chartOfAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @throws EpicSDKCore202001DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Delete_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Delete_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerChartOfAccountResponse")
    public void deleteGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccountDefineBankAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param bankAccountObject
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountDefineBankAccount")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public void actionGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "BankAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankAccount bankAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param ignoreNonZeroBalance
     * @param chartOfAccountID
     * @param shouldInactivateReactivateSubaccounts
     * @param ignoreSubAccountNonZeroBalances
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountInactivateReactivate")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerChartOfAccountInactivateReactivate")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerChartOfAccountInactivateReactivateResponse")
    public void actionGeneralLedgerChartOfAccountInactivateReactivate(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer chartOfAccountID,
        @WebParam(name = "IgnoreNonZeroBalance", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean ignoreNonZeroBalance,
        @WebParam(name = "ShouldInactivateReactivateSubaccounts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean shouldInactivateReactivateSubaccounts,
        @WebParam(name = "IgnoreSubAccountNonZeroBalances", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean ignoreSubAccountNonZeroBalances,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202001ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode
     * @throws EpicSDKCore202001GetActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityActivityCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001GetActivityActivityCodeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Get_Activity_ActivityCode")
    @WebResult(name = "Get_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Get_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCode")
    @ResponseWrapper(localName = "Get_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCodeResponse")
    public ArrayOfActivityCode getActivityActivityCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001GetActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001GetActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001GetActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202001GetActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param activityCodeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202001InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202001InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Insert_Activity_ActivityCode")
    @WebResult(name = "Insert_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Insert_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCode")
    @ResponseWrapper(localName = "Insert_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCodeResponse")
    public Integer insertActivityActivityCode(
        @WebParam(name = "ActivityCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCode activityCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202001InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202001InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202001InsertActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202001InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

}
