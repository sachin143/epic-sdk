
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RevisePremiumObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}RevisePremium" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "revisePremiumObject"
})
@XmlRootElement(name = "Action_Transaction_RevisePremium")
public class ActionTransactionRevisePremium {

    @XmlElement(name = "RevisePremiumObject", nillable = true)
    protected RevisePremium revisePremiumObject;

    /**
     * Gets the value of the revisePremiumObject property.
     * 
     * @return
     *     possible object is
     *     {@link RevisePremium }
     *     
     */
    public RevisePremium getRevisePremiumObject() {
        return revisePremiumObject;
    }

    /**
     * Sets the value of the revisePremiumObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link RevisePremium }
     *     
     */
    public void setRevisePremiumObject(RevisePremium value) {
        this.revisePremiumObject = value;
    }

}
