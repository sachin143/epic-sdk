
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfModifyRevenueDeferralSchedule;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionModifyRevenueDeferralScheduleResult" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ArrayOfModifyRevenueDeferralSchedule" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionModifyRevenueDeferralScheduleResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionModifyRevenueDeferralScheduleResponse")
public class GetTransactionDefaultActionModifyRevenueDeferralScheduleResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionModifyRevenueDeferralScheduleResult", nillable = true)
    protected ArrayOfModifyRevenueDeferralSchedule getTransactionDefaultActionModifyRevenueDeferralScheduleResult;

    /**
     * Gets the value of the getTransactionDefaultActionModifyRevenueDeferralScheduleResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfModifyRevenueDeferralSchedule }
     *     
     */
    public ArrayOfModifyRevenueDeferralSchedule getGetTransactionDefaultActionModifyRevenueDeferralScheduleResult() {
        return getTransactionDefaultActionModifyRevenueDeferralScheduleResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionModifyRevenueDeferralScheduleResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfModifyRevenueDeferralSchedule }
     *     
     */
    public void setGetTransactionDefaultActionModifyRevenueDeferralScheduleResult(ArrayOfModifyRevenueDeferralSchedule value) {
        this.getTransactionDefaultActionModifyRevenueDeferralScheduleResult = value;
    }

}
