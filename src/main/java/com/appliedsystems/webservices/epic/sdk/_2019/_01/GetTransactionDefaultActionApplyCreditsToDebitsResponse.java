
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionApplyCreditsToDebitsResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}ArrayOfApplyCreditsToDebits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionApplyCreditsToDebitsResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse")
public class GetTransactionDefaultActionApplyCreditsToDebitsResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", nillable = true)
    protected ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebitsResult;

    /**
     * Gets the value of the getTransactionDefaultActionApplyCreditsToDebitsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfApplyCreditsToDebits }
     *     
     */
    public ArrayOfApplyCreditsToDebits getGetTransactionDefaultActionApplyCreditsToDebitsResult() {
        return getTransactionDefaultActionApplyCreditsToDebitsResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionApplyCreditsToDebitsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfApplyCreditsToDebits }
     *     
     */
    public void setGetTransactionDefaultActionApplyCreditsToDebitsResult(ArrayOfApplyCreditsToDebits value) {
        this.getTransactionDefaultActionApplyCreditsToDebitsResult = value;
    }

}
