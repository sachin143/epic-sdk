
package com.appliedsystems.webservices.epic.sdk._2011._12;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2011._12 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2011._12
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTransactionTransactionCodeResponse }
     * 
     */
    public GetTransactionTransactionCodeResponse createGetTransactionTransactionCodeResponse() {
        return new GetTransactionTransactionCodeResponse();
    }

    /**
     * Create an instance of {@link InsertTransactionTransactionCodeResponse }
     * 
     */
    public InsertTransactionTransactionCodeResponse createInsertTransactionTransactionCodeResponse() {
        return new InsertTransactionTransactionCodeResponse();
    }

    /**
     * Create an instance of {@link GetTransactionTransactionCode }
     * 
     */
    public GetTransactionTransactionCode createGetTransactionTransactionCode() {
        return new GetTransactionTransactionCode();
    }

    /**
     * Create an instance of {@link InsertActivityActivityCodeResponse }
     * 
     */
    public InsertActivityActivityCodeResponse createInsertActivityActivityCodeResponse() {
        return new InsertActivityActivityCodeResponse();
    }

    /**
     * Create an instance of {@link InsertTransactionTransactionCode }
     * 
     */
    public InsertTransactionTransactionCode createInsertTransactionTransactionCode() {
        return new InsertTransactionTransactionCode();
    }

    /**
     * Create an instance of {@link InsertActivityActivityCode }
     * 
     */
    public InsertActivityActivityCode createInsertActivityActivityCode() {
        return new InsertActivityActivityCode();
    }

}
