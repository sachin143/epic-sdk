
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_DefaultActionUpdateRenewalStageResult" type="{http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/}ArrayOfUpdateRenewalStage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyDefaultActionUpdateRenewalStageResult"
})
@XmlRootElement(name = "Get_Policy_DefaultActionUpdateRenewalStageResponse")
public class GetPolicyDefaultActionUpdateRenewalStageResponse {

    @XmlElement(name = "Get_Policy_DefaultActionUpdateRenewalStageResult", nillable = true)
    protected ArrayOfUpdateRenewalStage getPolicyDefaultActionUpdateRenewalStageResult;

    /**
     * Gets the value of the getPolicyDefaultActionUpdateRenewalStageResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUpdateRenewalStage }
     *     
     */
    public ArrayOfUpdateRenewalStage getGetPolicyDefaultActionUpdateRenewalStageResult() {
        return getPolicyDefaultActionUpdateRenewalStageResult;
    }

    /**
     * Sets the value of the getPolicyDefaultActionUpdateRenewalStageResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUpdateRenewalStage }
     *     
     */
    public void setGetPolicyDefaultActionUpdateRenewalStageResult(ArrayOfUpdateRenewalStage value) {
        this.getPolicyDefaultActionUpdateRenewalStageResult = value;
    }

}
