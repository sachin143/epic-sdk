
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SplitReceivableTemplateObject" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/}SplitReceivableTemplate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "splitReceivableTemplateObject"
})
@XmlRootElement(name = "Update_Client_SplitReceivableTemplate")
public class UpdateClientSplitReceivableTemplate {

    @XmlElement(name = "SplitReceivableTemplateObject", nillable = true)
    protected SplitReceivableTemplate splitReceivableTemplateObject;

    /**
     * Gets the value of the splitReceivableTemplateObject property.
     * 
     * @return
     *     possible object is
     *     {@link SplitReceivableTemplate }
     *     
     */
    public SplitReceivableTemplate getSplitReceivableTemplateObject() {
        return splitReceivableTemplateObject;
    }

    /**
     * Sets the value of the splitReceivableTemplateObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitReceivableTemplate }
     *     
     */
    public void setSplitReceivableTemplateObject(SplitReceivableTemplate value) {
        this.splitReceivableTemplateObject = value;
    }

}
