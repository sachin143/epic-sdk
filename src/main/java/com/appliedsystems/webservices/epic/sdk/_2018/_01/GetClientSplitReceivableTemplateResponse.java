
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Client_SplitReceivableTemplateResult" type="{http://schemas.appliedsystems.com/epic/sdk/2018/01/_account/}ArrayOfSplitReceivableTemplate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClientSplitReceivableTemplateResult"
})
@XmlRootElement(name = "Get_Client_SplitReceivableTemplateResponse")
public class GetClientSplitReceivableTemplateResponse {

    @XmlElement(name = "Get_Client_SplitReceivableTemplateResult", nillable = true)
    protected ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplateResult;

    /**
     * Gets the value of the getClientSplitReceivableTemplateResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSplitReceivableTemplate }
     *     
     */
    public ArrayOfSplitReceivableTemplate getGetClientSplitReceivableTemplateResult() {
        return getClientSplitReceivableTemplateResult;
    }

    /**
     * Sets the value of the getClientSplitReceivableTemplateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSplitReceivableTemplate }
     *     
     */
    public void setGetClientSplitReceivableTemplateResult(ArrayOfSplitReceivableTemplate value) {
        this.getClientSplitReceivableTemplateResult = value;
    }

}
