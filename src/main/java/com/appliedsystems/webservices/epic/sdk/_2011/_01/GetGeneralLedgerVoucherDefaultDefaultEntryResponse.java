
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_VoucherDefaultDefaultEntryResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/}VoucherGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerVoucherDefaultDefaultEntryResult"
})
@XmlRootElement(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse")
public class GetGeneralLedgerVoucherDefaultDefaultEntryResponse {

    @XmlElement(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", nillable = true)
    protected VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntryResult;

    /**
     * Gets the value of the getGeneralLedgerVoucherDefaultDefaultEntryResult property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherGetResult }
     *     
     */
    public VoucherGetResult getGetGeneralLedgerVoucherDefaultDefaultEntryResult() {
        return getGeneralLedgerVoucherDefaultDefaultEntryResult;
    }

    /**
     * Sets the value of the getGeneralLedgerVoucherDefaultDefaultEntryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherGetResult }
     *     
     */
    public void setGetGeneralLedgerVoucherDefaultDefaultEntryResult(VoucherGetResult value) {
        this.getGeneralLedgerVoucherDefaultDefaultEntryResult = value;
    }

}
