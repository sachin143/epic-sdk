
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DisbursementDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DisbursementBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisbursementBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disbursementDefaultEntryID",
    "disbursementBankAccountNumberCode",
    "disbursementBankSubAccountNumberCode"
})
@XmlRootElement(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntry")
public class GetGeneralLedgerDisbursementDefaultDefaultEntry {

    @XmlElement(name = "DisbursementDefaultEntryID")
    protected Integer disbursementDefaultEntryID;
    @XmlElement(name = "DisbursementBankAccountNumberCode", nillable = true)
    protected String disbursementBankAccountNumberCode;
    @XmlElement(name = "DisbursementBankSubAccountNumberCode", nillable = true)
    protected String disbursementBankSubAccountNumberCode;

    /**
     * Gets the value of the disbursementDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisbursementDefaultEntryID() {
        return disbursementDefaultEntryID;
    }

    /**
     * Sets the value of the disbursementDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisbursementDefaultEntryID(Integer value) {
        this.disbursementDefaultEntryID = value;
    }

    /**
     * Gets the value of the disbursementBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementBankAccountNumberCode() {
        return disbursementBankAccountNumberCode;
    }

    /**
     * Sets the value of the disbursementBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementBankAccountNumberCode(String value) {
        this.disbursementBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the disbursementBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementBankSubAccountNumberCode() {
        return disbursementBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the disbursementBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementBankSubAccountNumberCode(String value) {
        this.disbursementBankSubAccountNumberCode = value;
    }

}
