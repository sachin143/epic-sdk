
package com.appliedsystems.webservices.epic.sdk._2020._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Activity_ActivityCodeResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/}ArrayOfActivityCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getActivityActivityCodeResult"
})
@XmlRootElement(name = "Get_Activity_ActivityCodeResponse")
public class GetActivityActivityCodeResponse {

    @XmlElement(name = "Get_Activity_ActivityCodeResult", nillable = true)
    protected ArrayOfActivityCode getActivityActivityCodeResult;

    /**
     * Gets the value of the getActivityActivityCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfActivityCode }
     *     
     */
    public ArrayOfActivityCode getGetActivityActivityCodeResult() {
        return getActivityActivityCodeResult;
    }

    /**
     * Sets the value of the getActivityActivityCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfActivityCode }
     *     
     */
    public void setGetActivityActivityCodeResult(ArrayOfActivityCode value) {
        this.getActivityActivityCodeResult = value;
    }

}
