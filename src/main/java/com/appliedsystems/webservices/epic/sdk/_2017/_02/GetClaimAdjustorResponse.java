
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Claim_AdjustorResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}ArrayOfAdjustor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClaimAdjustorResult"
})
@XmlRootElement(name = "Get_Claim_AdjustorResponse")
public class GetClaimAdjustorResponse {

    @XmlElement(name = "Get_Claim_AdjustorResult", nillable = true)
    protected ArrayOfAdjustor getClaimAdjustorResult;

    /**
     * Gets the value of the getClaimAdjustorResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdjustor }
     *     
     */
    public ArrayOfAdjustor getGetClaimAdjustorResult() {
        return getClaimAdjustorResult;
    }

    /**
     * Sets the value of the getClaimAdjustorResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdjustor }
     *     
     */
    public void setGetClaimAdjustorResult(ArrayOfAdjustor value) {
        this.getClaimAdjustorResult = value;
    }

}
