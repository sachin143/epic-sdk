
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sEntityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lEntityLookupCodes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="sDescriptionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sCompareByType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sClientOrPolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lAgencies" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="lBranches" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="lDepartments" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="lProfitCenters" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="lIssuingCompanies" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="lLineOfBusiness" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *         &lt;element name="fIncludeHistory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sEntityType",
    "lEntityLookupCodes",
    "sDescriptionType",
    "sCompareByType",
    "sClientOrPolicyNumber",
    "lAgencies",
    "lBranches",
    "lDepartments",
    "lProfitCenters",
    "lIssuingCompanies",
    "lLineOfBusiness",
    "fIncludeHistory"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
public class GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies {

    @XmlElement(nillable = true)
    protected String sEntityType;
    @XmlElement(nillable = true)
    protected ArrayOfstring lEntityLookupCodes;
    @XmlElement(nillable = true)
    protected String sDescriptionType;
    @XmlElement(nillable = true)
    protected String sCompareByType;
    @XmlElement(nillable = true)
    protected String sClientOrPolicyNumber;
    @XmlElement(nillable = true)
    protected ArrayOfstring lAgencies;
    @XmlElement(nillable = true)
    protected ArrayOfstring lBranches;
    @XmlElement(nillable = true)
    protected ArrayOfstring lDepartments;
    @XmlElement(nillable = true)
    protected ArrayOfstring lProfitCenters;
    @XmlElement(nillable = true)
    protected ArrayOfstring lIssuingCompanies;
    @XmlElement(nillable = true)
    protected ArrayOfstring lLineOfBusiness;
    protected Boolean fIncludeHistory;

    /**
     * Gets the value of the sEntityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEntityType() {
        return sEntityType;
    }

    /**
     * Sets the value of the sEntityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEntityType(String value) {
        this.sEntityType = value;
    }

    /**
     * Gets the value of the lEntityLookupCodes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLEntityLookupCodes() {
        return lEntityLookupCodes;
    }

    /**
     * Sets the value of the lEntityLookupCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLEntityLookupCodes(ArrayOfstring value) {
        this.lEntityLookupCodes = value;
    }

    /**
     * Gets the value of the sDescriptionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSDescriptionType() {
        return sDescriptionType;
    }

    /**
     * Sets the value of the sDescriptionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSDescriptionType(String value) {
        this.sDescriptionType = value;
    }

    /**
     * Gets the value of the sCompareByType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCompareByType() {
        return sCompareByType;
    }

    /**
     * Sets the value of the sCompareByType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCompareByType(String value) {
        this.sCompareByType = value;
    }

    /**
     * Gets the value of the sClientOrPolicyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSClientOrPolicyNumber() {
        return sClientOrPolicyNumber;
    }

    /**
     * Sets the value of the sClientOrPolicyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSClientOrPolicyNumber(String value) {
        this.sClientOrPolicyNumber = value;
    }

    /**
     * Gets the value of the lAgencies property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLAgencies() {
        return lAgencies;
    }

    /**
     * Sets the value of the lAgencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLAgencies(ArrayOfstring value) {
        this.lAgencies = value;
    }

    /**
     * Gets the value of the lBranches property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLBranches() {
        return lBranches;
    }

    /**
     * Sets the value of the lBranches property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLBranches(ArrayOfstring value) {
        this.lBranches = value;
    }

    /**
     * Gets the value of the lDepartments property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLDepartments() {
        return lDepartments;
    }

    /**
     * Sets the value of the lDepartments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLDepartments(ArrayOfstring value) {
        this.lDepartments = value;
    }

    /**
     * Gets the value of the lProfitCenters property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLProfitCenters() {
        return lProfitCenters;
    }

    /**
     * Sets the value of the lProfitCenters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLProfitCenters(ArrayOfstring value) {
        this.lProfitCenters = value;
    }

    /**
     * Gets the value of the lIssuingCompanies property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLIssuingCompanies() {
        return lIssuingCompanies;
    }

    /**
     * Sets the value of the lIssuingCompanies property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLIssuingCompanies(ArrayOfstring value) {
        this.lIssuingCompanies = value;
    }

    /**
     * Gets the value of the lLineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getLLineOfBusiness() {
        return lLineOfBusiness;
    }

    /**
     * Sets the value of the lLineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setLLineOfBusiness(ArrayOfstring value) {
        this.lLineOfBusiness = value;
    }

    /**
     * Gets the value of the fIncludeHistory property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFIncludeHistory() {
        return fIncludeHistory;
    }

    /**
     * Sets the value of the fIncludeHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFIncludeHistory(Boolean value) {
        this.fIncludeHistory = value;
    }

}
