
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Claim_AdditionalPartyResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertClaimAdditionalPartyResult"
})
@XmlRootElement(name = "Insert_Claim_AdditionalPartyResponse")
public class InsertClaimAdditionalPartyResponse {

    @XmlElement(name = "Insert_Claim_AdditionalPartyResult")
    protected Integer insertClaimAdditionalPartyResult;

    /**
     * Gets the value of the insertClaimAdditionalPartyResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertClaimAdditionalPartyResult() {
        return insertClaimAdditionalPartyResult;
    }

    /**
     * Sets the value of the insertClaimAdditionalPartyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertClaimAdditionalPartyResult(Integer value) {
        this.insertClaimAdditionalPartyResult = value;
    }

}
