
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LineFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}LineFilter" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lineFilterObject",
    "pageNumber"
})
@XmlRootElement(name = "Get_Line")
public class GetLine {

    @XmlElement(name = "LineFilterObject", nillable = true)
    protected LineFilter lineFilterObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the lineFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link LineFilter }
     *     
     */
    public LineFilter getLineFilterObject() {
        return lineFilterObject;
    }

    /**
     * Sets the value of the lineFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineFilter }
     *     
     */
    public void setLineFilterObject(LineFilter value) {
        this.lineFilterObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
