
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ArrayOfUpdateRenewalStage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdateRenewalStageList" type="{http://schemas.appliedsystems.com/epic/sdk/2022/01/_account/_policy/_action/}ArrayOfUpdateRenewalStage" minOccurs="0"/>
 *         &lt;element name="ChangeStageToCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updateRenewalStageList",
    "changeStageToCode"
})
@XmlRootElement(name = "Action_Policy_UpdateRenewalStage")
public class ActionPolicyUpdateRenewalStage {

    @XmlElement(name = "UpdateRenewalStageList", nillable = true)
    protected ArrayOfUpdateRenewalStage updateRenewalStageList;
    @XmlElement(name = "ChangeStageToCode")
    protected Integer changeStageToCode;

    /**
     * Gets the value of the updateRenewalStageList property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUpdateRenewalStage }
     *     
     */
    public ArrayOfUpdateRenewalStage getUpdateRenewalStageList() {
        return updateRenewalStageList;
    }

    /**
     * Sets the value of the updateRenewalStageList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUpdateRenewalStage }
     *     
     */
    public void setUpdateRenewalStageList(ArrayOfUpdateRenewalStage value) {
        this.updateRenewalStageList = value;
    }

    /**
     * Gets the value of the changeStageToCode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChangeStageToCode() {
        return changeStageToCode;
    }

    /**
     * Sets the value of the changeStageToCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChangeStageToCode(Integer value) {
        this.changeStageToCode = value;
    }

}
