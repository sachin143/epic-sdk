
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChartOfAccountID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IgnoreNonZeroBalance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ShouldInactivateReactivateSubaccounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IgnoreSubAccountNonZeroBalances" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "chartOfAccountID",
    "ignoreNonZeroBalance",
    "shouldInactivateReactivateSubaccounts",
    "ignoreSubAccountNonZeroBalances"
})
@XmlRootElement(name = "Action_GeneralLedger_ChartOfAccountInactivateReactivate")
public class ActionGeneralLedgerChartOfAccountInactivateReactivate {

    @XmlElement(name = "ChartOfAccountID")
    protected Integer chartOfAccountID;
    @XmlElement(name = "IgnoreNonZeroBalance")
    protected Boolean ignoreNonZeroBalance;
    @XmlElement(name = "ShouldInactivateReactivateSubaccounts")
    protected Boolean shouldInactivateReactivateSubaccounts;
    @XmlElement(name = "IgnoreSubAccountNonZeroBalances")
    protected Boolean ignoreSubAccountNonZeroBalances;

    /**
     * Gets the value of the chartOfAccountID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChartOfAccountID() {
        return chartOfAccountID;
    }

    /**
     * Sets the value of the chartOfAccountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChartOfAccountID(Integer value) {
        this.chartOfAccountID = value;
    }

    /**
     * Gets the value of the ignoreNonZeroBalance property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreNonZeroBalance() {
        return ignoreNonZeroBalance;
    }

    /**
     * Sets the value of the ignoreNonZeroBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreNonZeroBalance(Boolean value) {
        this.ignoreNonZeroBalance = value;
    }

    /**
     * Gets the value of the shouldInactivateReactivateSubaccounts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShouldInactivateReactivateSubaccounts() {
        return shouldInactivateReactivateSubaccounts;
    }

    /**
     * Sets the value of the shouldInactivateReactivateSubaccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShouldInactivateReactivateSubaccounts(Boolean value) {
        this.shouldInactivateReactivateSubaccounts = value;
    }

    /**
     * Gets the value of the ignoreSubAccountNonZeroBalances property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreSubAccountNonZeroBalances() {
        return ignoreSubAccountNonZeroBalances;
    }

    /**
     * Sets the value of the ignoreSubAccountNonZeroBalances property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreSubAccountNonZeroBalances(Boolean value) {
        this.ignoreSubAccountNonZeroBalances = value;
    }

}
