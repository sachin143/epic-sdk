
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UnapplyCreditsToDebitsObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}UnapplyCreditsToDebits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "unapplyCreditsToDebitsObject"
})
@XmlRootElement(name = "Action_Transaction_UnapplyCreditsToDebits")
public class ActionTransactionUnapplyCreditsToDebits {

    @XmlElement(name = "UnapplyCreditsToDebitsObject", nillable = true)
    protected UnapplyCreditsToDebits unapplyCreditsToDebitsObject;

    /**
     * Gets the value of the unapplyCreditsToDebitsObject property.
     * 
     * @return
     *     possible object is
     *     {@link UnapplyCreditsToDebits }
     *     
     */
    public UnapplyCreditsToDebits getUnapplyCreditsToDebitsObject() {
        return unapplyCreditsToDebitsObject;
    }

    /**
     * Sets the value of the unapplyCreditsToDebitsObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnapplyCreditsToDebits }
     *     
     */
    public void setUnapplyCreditsToDebitsObject(UnapplyCreditsToDebits value) {
        this.unapplyCreditsToDebitsObject = value;
    }

}
