
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LookupTypeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_lookup/}LookupTypes" minOccurs="0"/>
 *         &lt;element name="SearchTerms" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lookupTypeObject",
    "searchTerms"
})
@XmlRootElement(name = "Get_Lookup")
public class GetLookup {

    @XmlElement(name = "LookupTypeObject")
    @XmlSchemaType(name = "string")
    protected LookupTypes lookupTypeObject;
    @XmlElement(name = "SearchTerms", nillable = true)
    protected ArrayOfstring searchTerms;

    /**
     * Gets the value of the lookupTypeObject property.
     * 
     * @return
     *     possible object is
     *     {@link LookupTypes }
     *     
     */
    public LookupTypes getLookupTypeObject() {
        return lookupTypeObject;
    }

    /**
     * Sets the value of the lookupTypeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupTypes }
     *     
     */
    public void setLookupTypeObject(LookupTypes value) {
        this.lookupTypeObject = value;
    }

    /**
     * Gets the value of the searchTerms property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring }
     *     
     */
    public ArrayOfstring getSearchTerms() {
        return searchTerms;
    }

    /**
     * Sets the value of the searchTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring }
     *     
     */
    public void setSearchTerms(ArrayOfstring value) {
        this.searchTerms = value;
    }

}
