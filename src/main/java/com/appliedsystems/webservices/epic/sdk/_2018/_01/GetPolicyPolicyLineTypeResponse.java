
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_PolicyLineTypeResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/}ArrayOfPolicyLineType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyPolicyLineTypeResult"
})
@XmlRootElement(name = "Get_Policy_PolicyLineTypeResponse")
public class GetPolicyPolicyLineTypeResponse {

    @XmlElement(name = "Get_Policy_PolicyLineTypeResult", nillable = true)
    protected ArrayOfPolicyLineType getPolicyPolicyLineTypeResult;

    /**
     * Gets the value of the getPolicyPolicyLineTypeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicyLineType }
     *     
     */
    public ArrayOfPolicyLineType getGetPolicyPolicyLineTypeResult() {
        return getPolicyPolicyLineTypeResult;
    }

    /**
     * Sets the value of the getPolicyPolicyLineTypeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicyLineType }
     *     
     */
    public void setGetPolicyPolicyLineTypeResult(ArrayOfPolicyLineType value) {
        this.getPolicyPolicyLineTypeResult = value;
    }

}
