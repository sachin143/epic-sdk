
package com.appliedsystems.webservices.epic.sdk._2020._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Activity_ActivityCodeResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertActivityActivityCodeResult"
})
@XmlRootElement(name = "Insert_Activity_ActivityCodeResponse")
public class InsertActivityActivityCodeResponse {

    @XmlElement(name = "Insert_Activity_ActivityCodeResult")
    protected Integer insertActivityActivityCodeResult;

    /**
     * Gets the value of the insertActivityActivityCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertActivityActivityCodeResult() {
        return insertActivityActivityCodeResult;
    }

    /**
     * Sets the value of the insertActivityActivityCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertActivityActivityCodeResult(Integer value) {
        this.insertActivityActivityCodeResult = value;
    }

}
