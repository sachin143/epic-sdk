
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VoucherBankAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherBankSubAccountNumberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherAccountingMonth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "voucherDefaultEntryID",
    "voucherBankAccountNumberCode",
    "voucherBankSubAccountNumberCode",
    "voucherAccountingMonth"
})
@XmlRootElement(name = "Get_GeneralLedger_VoucherDefaultDefaultEntry")
public class GetGeneralLedgerVoucherDefaultDefaultEntry {

    @XmlElement(name = "VoucherDefaultEntryID")
    protected Integer voucherDefaultEntryID;
    @XmlElement(name = "VoucherBankAccountNumberCode", nillable = true)
    protected String voucherBankAccountNumberCode;
    @XmlElement(name = "VoucherBankSubAccountNumberCode", nillable = true)
    protected String voucherBankSubAccountNumberCode;
    @XmlElement(name = "VoucherAccountingMonth", nillable = true)
    protected String voucherAccountingMonth;

    /**
     * Gets the value of the voucherDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherDefaultEntryID() {
        return voucherDefaultEntryID;
    }

    /**
     * Sets the value of the voucherDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherDefaultEntryID(Integer value) {
        this.voucherDefaultEntryID = value;
    }

    /**
     * Gets the value of the voucherBankAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherBankAccountNumberCode() {
        return voucherBankAccountNumberCode;
    }

    /**
     * Sets the value of the voucherBankAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherBankAccountNumberCode(String value) {
        this.voucherBankAccountNumberCode = value;
    }

    /**
     * Gets the value of the voucherBankSubAccountNumberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherBankSubAccountNumberCode() {
        return voucherBankSubAccountNumberCode;
    }

    /**
     * Sets the value of the voucherBankSubAccountNumberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherBankSubAccountNumberCode(String value) {
        this.voucherBankSubAccountNumberCode = value;
    }

    /**
     * Gets the value of the voucherAccountingMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherAccountingMonth() {
        return voucherAccountingMonth;
    }

    /**
     * Sets the value of the voucherAccountingMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherAccountingMonth(String value) {
        this.voucherAccountingMonth = value;
    }

}
