
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionAdjustCommissionResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_common/}ArrayOfAdjustCommission" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionAdjustCommissionResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionAdjustCommissionResponse")
public class GetTransactionDefaultActionAdjustCommissionResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionAdjustCommissionResult", nillable = true)
    protected ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommissionResult;

    /**
     * Gets the value of the getTransactionDefaultActionAdjustCommissionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdjustCommission }
     *     
     */
    public ArrayOfAdjustCommission getGetTransactionDefaultActionAdjustCommissionResult() {
        return getTransactionDefaultActionAdjustCommissionResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionAdjustCommissionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdjustCommission }
     *     
     */
    public void setGetTransactionDefaultActionAdjustCommissionResult(ArrayOfAdjustCommission value) {
        this.getTransactionDefaultActionAdjustCommissionResult = value;
    }

}
