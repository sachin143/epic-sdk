
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/_generalledger/_reconciliation/}BankFilter" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bankFilterObject",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationBank")
public class GetGeneralLedgerReconciliationBank {

    @XmlElement(name = "BankFilterObject", nillable = true)
    protected BankFilter bankFilterObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the bankFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link BankFilter }
     *     
     */
    public BankFilter getBankFilterObject() {
        return bankFilterObject;
    }

    /**
     * Sets the value of the bankFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankFilter }
     *     
     */
    public void setBankFilterObject(BankFilter value) {
        this.bankFilterObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
