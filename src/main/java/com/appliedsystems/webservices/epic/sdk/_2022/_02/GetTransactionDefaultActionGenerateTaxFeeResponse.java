
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionGenerateTaxFeeResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}ArrayOfGenerateTaxFee" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionGenerateTaxFeeResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionGenerateTaxFeeResponse")
public class GetTransactionDefaultActionGenerateTaxFeeResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", nillable = true)
    protected ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFeeResult;

    /**
     * Gets the value of the getTransactionDefaultActionGenerateTaxFeeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGenerateTaxFee }
     *     
     */
    public ArrayOfGenerateTaxFee getGetTransactionDefaultActionGenerateTaxFeeResult() {
        return getTransactionDefaultActionGenerateTaxFeeResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionGenerateTaxFeeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGenerateTaxFee }
     *     
     */
    public void setGetTransactionDefaultActionGenerateTaxFeeResult(ArrayOfGenerateTaxFee value) {
        this.getTransactionDefaultActionGenerateTaxFeeResult = value;
    }

}
