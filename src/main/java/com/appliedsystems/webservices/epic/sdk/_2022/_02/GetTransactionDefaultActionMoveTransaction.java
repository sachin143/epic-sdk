
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MoveTransactionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}MoveTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "moveTransactionObject"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionMoveTransaction")
public class GetTransactionDefaultActionMoveTransaction {

    @XmlElement(name = "MoveTransactionObject", nillable = true)
    protected MoveTransaction moveTransactionObject;

    /**
     * Gets the value of the moveTransactionObject property.
     * 
     * @return
     *     possible object is
     *     {@link MoveTransaction }
     *     
     */
    public MoveTransaction getMoveTransactionObject() {
        return moveTransactionObject;
    }

    /**
     * Sets the value of the moveTransactionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoveTransaction }
     *     
     */
    public void setMoveTransactionObject(MoveTransaction value) {
        this.moveTransactionObject = value;
    }

}
