
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Action_GeneralLedger_JournalEntryVoidResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionGeneralLedgerJournalEntryVoidResult"
})
@XmlRootElement(name = "Action_GeneralLedger_JournalEntryVoidResponse")
public class ActionGeneralLedgerJournalEntryVoidResponse {

    @XmlElement(name = "Action_GeneralLedger_JournalEntryVoidResult")
    protected Integer actionGeneralLedgerJournalEntryVoidResult;

    /**
     * Gets the value of the actionGeneralLedgerJournalEntryVoidResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActionGeneralLedgerJournalEntryVoidResult() {
        return actionGeneralLedgerJournalEntryVoidResult;
    }

    /**
     * Sets the value of the actionGeneralLedgerJournalEntryVoidResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActionGeneralLedgerJournalEntryVoidResult(Integer value) {
        this.actionGeneralLedgerJournalEntryVoidResult = value;
    }

}
