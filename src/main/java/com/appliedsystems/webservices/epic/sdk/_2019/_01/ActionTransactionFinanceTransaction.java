
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinanceTransactionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}FinanceTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "financeTransactionObject"
})
@XmlRootElement(name = "Action_Transaction_FinanceTransaction")
public class ActionTransactionFinanceTransaction {

    @XmlElement(name = "FinanceTransactionObject", nillable = true)
    protected FinanceTransaction financeTransactionObject;

    /**
     * Gets the value of the financeTransactionObject property.
     * 
     * @return
     *     possible object is
     *     {@link FinanceTransaction }
     *     
     */
    public FinanceTransaction getFinanceTransactionObject() {
        return financeTransactionObject;
    }

    /**
     * Sets the value of the financeTransactionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinanceTransaction }
     *     
     */
    public void setFinanceTransactionObject(FinanceTransaction value) {
        this.financeTransactionObject = value;
    }

}
