
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueNotIssueEndorsementObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}IssueNotIssueEndorsement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issueNotIssueEndorsementObject"
})
@XmlRootElement(name = "Action_Policy_IssueNotIssueEndorsement")
public class ActionPolicyIssueNotIssueEndorsement {

    @XmlElement(name = "IssueNotIssueEndorsementObject", nillable = true)
    protected IssueNotIssueEndorsement issueNotIssueEndorsementObject;

    /**
     * Gets the value of the issueNotIssueEndorsementObject property.
     * 
     * @return
     *     possible object is
     *     {@link IssueNotIssueEndorsement }
     *     
     */
    public IssueNotIssueEndorsement getIssueNotIssueEndorsementObject() {
        return issueNotIssueEndorsementObject;
    }

    /**
     * Sets the value of the issueNotIssueEndorsementObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssueNotIssueEndorsement }
     *     
     */
    public void setIssueNotIssueEndorsementObject(IssueNotIssueEndorsement value) {
        this.issueNotIssueEndorsementObject = value;
    }

}
