
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2022._02 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2022._02
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActionTransactionDiscount }
     * 
     */
    public ActionTransactionDiscount createActionTransactionDiscount() {
        return new ActionTransactionDiscount();
    }

    /**
     * Create an instance of {@link GetTransaction }
     * 
     */
    public GetTransaction createGetTransaction() {
        return new GetTransaction();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionApplyCreditsToDebits }
     * 
     */
    public GetTransactionDefaultActionApplyCreditsToDebits createGetTransactionDefaultActionApplyCreditsToDebits() {
        return new GetTransactionDefaultActionApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link InsertTransactionResponse }
     * 
     */
    public InsertTransactionResponse createInsertTransactionResponse() {
        return new InsertTransactionResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionHoldResumePaymentsResponse }
     * 
     */
    public ActionTransactionHoldResumePaymentsResponse createActionTransactionHoldResumePaymentsResponse() {
        return new ActionTransactionHoldResumePaymentsResponse();
    }

    /**
     * Create an instance of {@link InsertTransaction }
     * 
     */
    public InsertTransaction createInsertTransaction() {
        return new InsertTransaction();
    }

    /**
     * Create an instance of {@link ActionTransactionMoveTransaction }
     * 
     */
    public ActionTransactionMoveTransaction createActionTransactionMoveTransaction() {
        return new ActionTransactionMoveTransaction();
    }

    /**
     * Create an instance of {@link ActionTransactionUnapplyCreditsToDebitsResponse }
     * 
     */
    public ActionTransactionUnapplyCreditsToDebitsResponse createActionTransactionUnapplyCreditsToDebitsResponse() {
        return new ActionTransactionUnapplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultInstallments }
     * 
     */
    public GetTransactionDefaultInstallments createGetTransactionDefaultInstallments() {
        return new GetTransactionDefaultInstallments();
    }

    /**
     * Create an instance of {@link ActionTransactionMoveTransactionResponse }
     * 
     */
    public ActionTransactionMoveTransactionResponse createActionTransactionMoveTransactionResponse() {
        return new ActionTransactionMoveTransactionResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionUnapplyCreditsToDebits }
     * 
     */
    public GetTransactionDefaultActionUnapplyCreditsToDebits createGetTransactionDefaultActionUnapplyCreditsToDebits() {
        return new GetTransactionDefaultActionUnapplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link ActionTransactionAccountsReceivableWriteOffResponse }
     * 
     */
    public ActionTransactionAccountsReceivableWriteOffResponse createActionTransactionAccountsReceivableWriteOffResponse() {
        return new ActionTransactionAccountsReceivableWriteOffResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionRevisePremium }
     * 
     */
    public ActionTransactionRevisePremium createActionTransactionRevisePremium() {
        return new ActionTransactionRevisePremium();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionAdjustCommissionResponse }
     * 
     */
    public GetTransactionDefaultActionAdjustCommissionResponse createGetTransactionDefaultActionAdjustCommissionResponse() {
        return new GetTransactionDefaultActionAdjustCommissionResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionGenerateTaxFeeResponse }
     * 
     */
    public ActionTransactionGenerateTaxFeeResponse createActionTransactionGenerateTaxFeeResponse() {
        return new ActionTransactionGenerateTaxFeeResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionReverseTransaction }
     * 
     */
    public ActionTransactionReverseTransaction createActionTransactionReverseTransaction() {
        return new ActionTransactionReverseTransaction();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultInstallmentsResponse }
     * 
     */
    public GetTransactionDefaultInstallmentsResponse createGetTransactionDefaultInstallmentsResponse() {
        return new GetTransactionDefaultInstallmentsResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionReverseTransactionResponse }
     * 
     */
    public ActionTransactionReverseTransactionResponse createActionTransactionReverseTransactionResponse() {
        return new ActionTransactionReverseTransactionResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionUnapplyCreditsToDebitsResponse }
     * 
     */
    public GetTransactionDefaultActionUnapplyCreditsToDebitsResponse createGetTransactionDefaultActionUnapplyCreditsToDebitsResponse() {
        return new GetTransactionDefaultActionUnapplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionVoidPaymentResponse }
     * 
     */
    public ActionTransactionVoidPaymentResponse createActionTransactionVoidPaymentResponse() {
        return new ActionTransactionVoidPaymentResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionRevisePremium }
     * 
     */
    public GetTransactionDefaultActionRevisePremium createGetTransactionDefaultActionRevisePremium() {
        return new GetTransactionDefaultActionRevisePremium();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionReverseTransactionResponse }
     * 
     */
    public GetTransactionDefaultActionReverseTransactionResponse createGetTransactionDefaultActionReverseTransactionResponse() {
        return new GetTransactionDefaultActionReverseTransactionResponse();
    }

    /**
     * Create an instance of {@link UpdateTransactionResponse }
     * 
     */
    public UpdateTransactionResponse createUpdateTransactionResponse() {
        return new UpdateTransactionResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionApplyCreditsToDebits }
     * 
     */
    public ActionTransactionApplyCreditsToDebits createActionTransactionApplyCreditsToDebits() {
        return new ActionTransactionApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link ActionTransactionFinanceTransactionResponse }
     * 
     */
    public ActionTransactionFinanceTransactionResponse createActionTransactionFinanceTransactionResponse() {
        return new ActionTransactionFinanceTransactionResponse();
    }

    /**
     * Create an instance of {@link GetTransactionResponse }
     * 
     */
    public GetTransactionResponse createGetTransactionResponse() {
        return new GetTransactionResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionApplyCreditsToDebitsResponse }
     * 
     */
    public ActionTransactionApplyCreditsToDebitsResponse createActionTransactionApplyCreditsToDebitsResponse() {
        return new ActionTransactionApplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionGenerateTaxFee }
     * 
     */
    public GetTransactionDefaultActionGenerateTaxFee createGetTransactionDefaultActionGenerateTaxFee() {
        return new GetTransactionDefaultActionGenerateTaxFee();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionAdjustCommission }
     * 
     */
    public GetTransactionDefaultActionAdjustCommission createGetTransactionDefaultActionAdjustCommission() {
        return new GetTransactionDefaultActionAdjustCommission();
    }

    /**
     * Create an instance of {@link ActionTransactionReverseBalanceTransfer }
     * 
     */
    public ActionTransactionReverseBalanceTransfer createActionTransactionReverseBalanceTransfer() {
        return new ActionTransactionReverseBalanceTransfer();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionGenerateTaxFeeResponse }
     * 
     */
    public GetTransactionDefaultActionGenerateTaxFeeResponse createGetTransactionDefaultActionGenerateTaxFeeResponse() {
        return new GetTransactionDefaultActionGenerateTaxFeeResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionFinanceTransaction }
     * 
     */
    public ActionTransactionFinanceTransaction createActionTransactionFinanceTransaction() {
        return new ActionTransactionFinanceTransaction();
    }

    /**
     * Create an instance of {@link ActionTransactionDiscountResponse }
     * 
     */
    public ActionTransactionDiscountResponse createActionTransactionDiscountResponse() {
        return new ActionTransactionDiscountResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionBalanceTransferResponse }
     * 
     */
    public ActionTransactionBalanceTransferResponse createActionTransactionBalanceTransferResponse() {
        return new ActionTransactionBalanceTransferResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionHoldResumePayments }
     * 
     */
    public ActionTransactionHoldResumePayments createActionTransactionHoldResumePayments() {
        return new ActionTransactionHoldResumePayments();
    }

    /**
     * Create an instance of {@link ActionTransactionModifyRevenueDeferralSchedule }
     * 
     */
    public ActionTransactionModifyRevenueDeferralSchedule createActionTransactionModifyRevenueDeferralSchedule() {
        return new ActionTransactionModifyRevenueDeferralSchedule();
    }

    /**
     * Create an instance of {@link ActionTransactionVoidPayment }
     * 
     */
    public ActionTransactionVoidPayment createActionTransactionVoidPayment() {
        return new ActionTransactionVoidPayment();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionReverseTransaction }
     * 
     */
    public GetTransactionDefaultActionReverseTransaction createGetTransactionDefaultActionReverseTransaction() {
        return new GetTransactionDefaultActionReverseTransaction();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionDiscountResponse }
     * 
     */
    public GetTransactionDefaultActionDiscountResponse createGetTransactionDefaultActionDiscountResponse() {
        return new GetTransactionDefaultActionDiscountResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultProducerBrokerCommissionsResponse }
     * 
     */
    public GetTransactionDefaultProducerBrokerCommissionsResponse createGetTransactionDefaultProducerBrokerCommissionsResponse() {
        return new GetTransactionDefaultProducerBrokerCommissionsResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionRevisePremiumResponse }
     * 
     */
    public GetTransactionDefaultActionRevisePremiumResponse createGetTransactionDefaultActionRevisePremiumResponse() {
        return new GetTransactionDefaultActionRevisePremiumResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionModifyRevenueDeferralScheduleResponse }
     * 
     */
    public ActionTransactionModifyRevenueDeferralScheduleResponse createActionTransactionModifyRevenueDeferralScheduleResponse() {
        return new ActionTransactionModifyRevenueDeferralScheduleResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionApplyCreditsToDebitsResponse }
     * 
     */
    public GetTransactionDefaultActionApplyCreditsToDebitsResponse createGetTransactionDefaultActionApplyCreditsToDebitsResponse() {
        return new GetTransactionDefaultActionApplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionGenerateTaxFee }
     * 
     */
    public ActionTransactionGenerateTaxFee createActionTransactionGenerateTaxFee() {
        return new ActionTransactionGenerateTaxFee();
    }

    /**
     * Create an instance of {@link ActionTransactionBalanceTransfer }
     * 
     */
    public ActionTransactionBalanceTransfer createActionTransactionBalanceTransfer() {
        return new ActionTransactionBalanceTransfer();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionDiscount }
     * 
     */
    public GetTransactionDefaultActionDiscount createGetTransactionDefaultActionDiscount() {
        return new GetTransactionDefaultActionDiscount();
    }

    /**
     * Create an instance of {@link ActionTransactionAdjustCommissionResponse }
     * 
     */
    public ActionTransactionAdjustCommissionResponse createActionTransactionAdjustCommissionResponse() {
        return new ActionTransactionAdjustCommissionResponse();
    }

    /**
     * Create an instance of {@link UpdateTransaction }
     * 
     */
    public UpdateTransaction createUpdateTransaction() {
        return new UpdateTransaction();
    }

    /**
     * Create an instance of {@link ActionTransactionRevisePremiumResponse }
     * 
     */
    public ActionTransactionRevisePremiumResponse createActionTransactionRevisePremiumResponse() {
        return new ActionTransactionRevisePremiumResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultProducerBrokerCommissions }
     * 
     */
    public GetTransactionDefaultProducerBrokerCommissions createGetTransactionDefaultProducerBrokerCommissions() {
        return new GetTransactionDefaultProducerBrokerCommissions();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionMoveTransactionResponse }
     * 
     */
    public GetTransactionDefaultActionMoveTransactionResponse createGetTransactionDefaultActionMoveTransactionResponse() {
        return new GetTransactionDefaultActionMoveTransactionResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionMoveTransaction }
     * 
     */
    public GetTransactionDefaultActionMoveTransaction createGetTransactionDefaultActionMoveTransaction() {
        return new GetTransactionDefaultActionMoveTransaction();
    }

    /**
     * Create an instance of {@link ActionTransactionReverseBalanceTransferResponse }
     * 
     */
    public ActionTransactionReverseBalanceTransferResponse createActionTransactionReverseBalanceTransferResponse() {
        return new ActionTransactionReverseBalanceTransferResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionAccountsReceivableWriteOff }
     * 
     */
    public ActionTransactionAccountsReceivableWriteOff createActionTransactionAccountsReceivableWriteOff() {
        return new ActionTransactionAccountsReceivableWriteOff();
    }

    /**
     * Create an instance of {@link ActionTransactionAdjustCommission }
     * 
     */
    public ActionTransactionAdjustCommission createActionTransactionAdjustCommission() {
        return new ActionTransactionAdjustCommission();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionModifyRevenueDeferralSchedule }
     * 
     */
    public GetTransactionDefaultActionModifyRevenueDeferralSchedule createGetTransactionDefaultActionModifyRevenueDeferralSchedule() {
        return new GetTransactionDefaultActionModifyRevenueDeferralSchedule();
    }

    /**
     * Create an instance of {@link GetTransactionReceivables }
     * 
     */
    public GetTransactionReceivables createGetTransactionReceivables() {
        return new GetTransactionReceivables();
    }

    /**
     * Create an instance of {@link GetTransactionReceivablesResponse }
     * 
     */
    public GetTransactionReceivablesResponse createGetTransactionReceivablesResponse() {
        return new GetTransactionReceivablesResponse();
    }

    /**
     * Create an instance of {@link GetTransactionDefaultActionModifyRevenueDeferralScheduleResponse }
     * 
     */
    public GetTransactionDefaultActionModifyRevenueDeferralScheduleResponse createGetTransactionDefaultActionModifyRevenueDeferralScheduleResponse() {
        return new GetTransactionDefaultActionModifyRevenueDeferralScheduleResponse();
    }

    /**
     * Create an instance of {@link ActionTransactionUnapplyCreditsToDebits }
     * 
     */
    public ActionTransactionUnapplyCreditsToDebits createActionTransactionUnapplyCreditsToDebits() {
        return new ActionTransactionUnapplyCreditsToDebits();
    }

}
