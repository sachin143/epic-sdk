
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyStatusObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_policy/}PolicyStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyStatusObject"
})
@XmlRootElement(name = "Insert_Policy_PolicyStatus")
public class InsertPolicyPolicyStatus {

    @XmlElement(name = "PolicyStatusObject", nillable = true)
    protected PolicyStatus policyStatusObject;

    /**
     * Gets the value of the policyStatusObject property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyStatus }
     *     
     */
    public PolicyStatus getPolicyStatusObject() {
        return policyStatusObject;
    }

    /**
     * Sets the value of the policyStatusObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyStatus }
     *     
     */
    public void setPolicyStatusObject(PolicyStatus value) {
        this.policyStatusObject = value;
    }

}
