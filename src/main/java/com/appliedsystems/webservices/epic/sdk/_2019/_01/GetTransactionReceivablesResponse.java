
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_ReceivablesResult" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_transaction/}ArrayOfReceivable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionReceivablesResult"
})
@XmlRootElement(name = "Get_Transaction_ReceivablesResponse")
public class GetTransactionReceivablesResponse {

    @XmlElement(name = "Get_Transaction_ReceivablesResult", nillable = true)
    protected ArrayOfReceivable getTransactionReceivablesResult;

    /**
     * Gets the value of the getTransactionReceivablesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReceivable }
     *     
     */
    public ArrayOfReceivable getGetTransactionReceivablesResult() {
        return getTransactionReceivablesResult;
    }

    /**
     * Sets the value of the getTransactionReceivablesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReceivable }
     *     
     */
    public void setGetTransactionReceivablesResult(ArrayOfReceivable value) {
        this.getTransactionReceivablesResult = value;
    }

}
