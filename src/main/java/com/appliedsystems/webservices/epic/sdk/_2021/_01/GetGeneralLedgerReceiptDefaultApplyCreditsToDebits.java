
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceiptID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DetailItemToBeInsertedObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_receipt/_detail/}DetailItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "receiptID",
    "detailItemToBeInsertedObject"
})
@XmlRootElement(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
public class GetGeneralLedgerReceiptDefaultApplyCreditsToDebits {

    @XmlElement(name = "ReceiptID")
    protected Integer receiptID;
    @XmlElement(name = "DetailItemToBeInsertedObject", nillable = true)
    protected DetailItem detailItemToBeInsertedObject;

    /**
     * Gets the value of the receiptID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReceiptID() {
        return receiptID;
    }

    /**
     * Sets the value of the receiptID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReceiptID(Integer value) {
        this.receiptID = value;
    }

    /**
     * Gets the value of the detailItemToBeInsertedObject property.
     * 
     * @return
     *     possible object is
     *     {@link DetailItem }
     *     
     */
    public DetailItem getDetailItemToBeInsertedObject() {
        return detailItemToBeInsertedObject;
    }

    /**
     * Sets the value of the detailItemToBeInsertedObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailItem }
     *     
     */
    public void setDetailItemToBeInsertedObject(DetailItem value) {
        this.detailItemToBeInsertedObject = value;
    }

}
