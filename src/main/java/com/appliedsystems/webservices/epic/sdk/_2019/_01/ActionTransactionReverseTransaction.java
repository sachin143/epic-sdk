
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReverseTransactionObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_transaction/_action/}ReverseTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reverseTransactionObject"
})
@XmlRootElement(name = "Action_Transaction_ReverseTransaction")
public class ActionTransactionReverseTransaction {

    @XmlElement(name = "ReverseTransactionObject", nillable = true)
    protected ReverseTransaction reverseTransactionObject;

    /**
     * Gets the value of the reverseTransactionObject property.
     * 
     * @return
     *     possible object is
     *     {@link ReverseTransaction }
     *     
     */
    public ReverseTransaction getReverseTransactionObject() {
        return reverseTransactionObject;
    }

    /**
     * Sets the value of the reverseTransactionObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReverseTransaction }
     *     
     */
    public void setReverseTransactionObject(ReverseTransaction value) {
        this.reverseTransactionObject = value;
    }

}
