
package com.appliedsystems.webservices.epic.sdk._2011._12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionCodeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_transaction/}TransactionCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionCodeObject"
})
@XmlRootElement(name = "Insert_Transaction_TransactionCode")
public class InsertTransactionTransactionCode {

    @XmlElement(name = "TransactionCodeObject", nillable = true)
    protected TransactionCode transactionCodeObject;

    /**
     * Gets the value of the transactionCodeObject property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCode }
     *     
     */
    public TransactionCode getTransactionCodeObject() {
        return transactionCodeObject;
    }

    /**
     * Sets the value of the transactionCodeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCode }
     *     
     */
    public void setTransactionCodeObject(TransactionCode value) {
        this.transactionCodeObject = value;
    }

}
