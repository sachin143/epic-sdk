
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Action_GeneralLedger_ReceiptTransferOfFundsResult" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionGeneralLedgerReceiptTransferOfFundsResult"
})
@XmlRootElement(name = "Action_GeneralLedger_ReceiptTransferOfFundsResponse")
public class ActionGeneralLedgerReceiptTransferOfFundsResponse {

    @XmlElement(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", nillable = true)
    protected ArrayOfint actionGeneralLedgerReceiptTransferOfFundsResult;

    /**
     * Gets the value of the actionGeneralLedgerReceiptTransferOfFundsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfint }
     *     
     */
    public ArrayOfint getActionGeneralLedgerReceiptTransferOfFundsResult() {
        return actionGeneralLedgerReceiptTransferOfFundsResult;
    }

    /**
     * Sets the value of the actionGeneralLedgerReceiptTransferOfFundsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfint }
     *     
     */
    public void setActionGeneralLedgerReceiptTransferOfFundsResult(ArrayOfint value) {
        this.actionGeneralLedgerReceiptTransferOfFundsResult = value;
    }

}
