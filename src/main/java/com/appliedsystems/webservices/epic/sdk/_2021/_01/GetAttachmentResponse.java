
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_AttachmentResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}AttachmentGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAttachmentResult"
})
@XmlRootElement(name = "Get_AttachmentResponse")
public class GetAttachmentResponse {

    @XmlElement(name = "Get_AttachmentResult", nillable = true)
    protected AttachmentGetResult getAttachmentResult;

    /**
     * Gets the value of the getAttachmentResult property.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentGetResult }
     *     
     */
    public AttachmentGetResult getGetAttachmentResult() {
        return getAttachmentResult;
    }

    /**
     * Sets the value of the getAttachmentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentGetResult }
     *     
     */
    public void setGetAttachmentResult(AttachmentGetResult value) {
        this.getAttachmentResult = value;
    }

}
