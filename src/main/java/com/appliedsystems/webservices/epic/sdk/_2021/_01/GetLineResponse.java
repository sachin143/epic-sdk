
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_LineResult" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_get/}LineGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLineResult"
})
@XmlRootElement(name = "Get_LineResponse")
public class GetLineResponse {

    @XmlElement(name = "Get_LineResult", nillable = true)
    protected LineGetResult getLineResult;

    /**
     * Gets the value of the getLineResult property.
     * 
     * @return
     *     possible object is
     *     {@link LineGetResult }
     *     
     */
    public LineGetResult getGetLineResult() {
        return getLineResult;
    }

    /**
     * Sets the value of the getLineResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineGetResult }
     *     
     */
    public void setGetLineResult(LineGetResult value) {
        this.getLineResult = value;
    }

}
