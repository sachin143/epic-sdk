
package com.appliedsystems.webservices.epic.sdk._2011._12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivityCodeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_configure/_activity/}ActivityCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "activityCodeObject"
})
@XmlRootElement(name = "Insert_Activity_ActivityCode")
public class InsertActivityActivityCode {

    @XmlElement(name = "ActivityCodeObject", nillable = true)
    protected ActivityCode activityCodeObject;

    /**
     * Gets the value of the activityCodeObject property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityCode }
     *     
     */
    public ActivityCode getActivityCodeObject() {
        return activityCodeObject;
    }

    /**
     * Sets the value of the activityCodeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityCode }
     *     
     */
    public void setActivityCodeObject(ActivityCode value) {
        this.activityCodeObject = value;
    }

}
