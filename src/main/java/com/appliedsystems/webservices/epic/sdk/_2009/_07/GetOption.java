
package com.appliedsystems.webservices.epic.sdk._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OptionTypeObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_common/_optiontype/}OptionTypes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "optionTypeObject"
})
@XmlRootElement(name = "Get_Option")
public class GetOption {

    @XmlElement(name = "OptionTypeObject")
    @XmlSchemaType(name = "string")
    protected OptionTypes optionTypeObject;

    /**
     * Gets the value of the optionTypeObject property.
     * 
     * @return
     *     possible object is
     *     {@link OptionTypes }
     *     
     */
    public OptionTypes getOptionTypeObject() {
        return optionTypeObject;
    }

    /**
     * Sets the value of the optionTypeObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionTypes }
     *     
     */
    public void setOptionTypeObject(OptionTypes value) {
        this.optionTypeObject = value;
    }

}
