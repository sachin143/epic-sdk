
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherVoidObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_voucher/_action/}VoucherVoid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "voucherVoidObject"
})
@XmlRootElement(name = "Action_GeneralLedger_VoucherVoid")
public class ActionGeneralLedgerVoucherVoid {

    @XmlElement(name = "VoucherVoidObject", nillable = true)
    protected VoucherVoid voucherVoidObject;

    /**
     * Gets the value of the voucherVoidObject property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherVoid }
     *     
     */
    public VoucherVoid getVoucherVoidObject() {
        return voucherVoidObject;
    }

    /**
     * Sets the value of the voucherVoidObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherVoid }
     *     
     */
    public void setVoucherVoidObject(VoucherVoid value) {
        this.voucherVoidObject = value;
    }

}
