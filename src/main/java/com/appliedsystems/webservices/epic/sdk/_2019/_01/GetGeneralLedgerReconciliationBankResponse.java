
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ReconciliationBankResult" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_get/_generalledger/}BankGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerReconciliationBankResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationBankResponse")
public class GetGeneralLedgerReconciliationBankResponse {

    @XmlElement(name = "Get_GeneralLedger_ReconciliationBankResult", nillable = true)
    protected BankGetResult getGeneralLedgerReconciliationBankResult;

    /**
     * Gets the value of the getGeneralLedgerReconciliationBankResult property.
     * 
     * @return
     *     possible object is
     *     {@link BankGetResult }
     *     
     */
    public BankGetResult getGetGeneralLedgerReconciliationBankResult() {
        return getGeneralLedgerReconciliationBankResult;
    }

    /**
     * Sets the value of the getGeneralLedgerReconciliationBankResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankGetResult }
     *     
     */
    public void setGetGeneralLedgerReconciliationBankResult(BankGetResult value) {
        this.getGeneralLedgerReconciliationBankResult = value;
    }

}
