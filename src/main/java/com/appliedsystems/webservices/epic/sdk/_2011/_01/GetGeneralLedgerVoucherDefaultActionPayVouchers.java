
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PayVouchersObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/}PayVouchers" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "payVouchersObject"
})
@XmlRootElement(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchers")
public class GetGeneralLedgerVoucherDefaultActionPayVouchers {

    @XmlElement(name = "PayVouchersObject", nillable = true)
    protected PayVouchers payVouchersObject;

    /**
     * Gets the value of the payVouchersObject property.
     * 
     * @return
     *     possible object is
     *     {@link PayVouchers }
     *     
     */
    public PayVouchers getPayVouchersObject() {
        return payVouchersObject;
    }

    /**
     * Sets the value of the payVouchersObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayVouchers }
     *     
     */
    public void setPayVouchersObject(PayVouchers value) {
        this.payVouchersObject = value;
    }

}
