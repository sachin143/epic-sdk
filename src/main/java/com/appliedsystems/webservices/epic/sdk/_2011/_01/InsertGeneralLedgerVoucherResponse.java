
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_GeneralLedger_VoucherResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertGeneralLedgerVoucherResult"
})
@XmlRootElement(name = "Insert_GeneralLedger_VoucherResponse")
public class InsertGeneralLedgerVoucherResponse {

    @XmlElement(name = "Insert_GeneralLedger_VoucherResult")
    protected Integer insertGeneralLedgerVoucherResult;

    /**
     * Gets the value of the insertGeneralLedgerVoucherResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertGeneralLedgerVoucherResult() {
        return insertGeneralLedgerVoucherResult;
    }

    /**
     * Sets the value of the insertGeneralLedgerVoucherResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertGeneralLedgerVoucherResult(Integer value) {
        this.insertGeneralLedgerVoucherResult = value;
    }

}
