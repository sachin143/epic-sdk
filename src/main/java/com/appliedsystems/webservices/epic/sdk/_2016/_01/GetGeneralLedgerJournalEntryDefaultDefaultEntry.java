
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="JournalEntryDefaultEntryID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "journalEntryDefaultEntryID"
})
@XmlRootElement(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
public class GetGeneralLedgerJournalEntryDefaultDefaultEntry {

    @XmlElement(name = "JournalEntryDefaultEntryID")
    protected Integer journalEntryDefaultEntryID;

    /**
     * Gets the value of the journalEntryDefaultEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getJournalEntryDefaultEntryID() {
        return journalEntryDefaultEntryID;
    }

    /**
     * Sets the value of the journalEntryDefaultEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setJournalEntryDefaultEntryID(Integer value) {
        this.journalEntryDefaultEntryID = value;
    }

}
