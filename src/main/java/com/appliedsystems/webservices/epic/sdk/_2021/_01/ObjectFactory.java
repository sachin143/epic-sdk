
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.appliedsystems.webservices.epic.sdk._2021._01 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.appliedsystems.webservices.epic.sdk._2021._01
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyRemarkResponse }
     * 
     */
    public InsertPolicyCancelPolicyRemarkResponse createInsertPolicyCancelPolicyRemarkResponse() {
        return new InsertPolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultApplyCreditsToDebits }
     * 
     */
    public GetGeneralLedgerReceiptDefaultApplyCreditsToDebits createGetGeneralLedgerReceiptDefaultApplyCreditsToDebits() {
        return new GetGeneralLedgerReceiptDefaultApplyCreditsToDebits();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionUpdateStageToSubmittedResponse }
     * 
     */
    public GetPolicyDefaultActionUpdateStageToSubmittedResponse createGetPolicyDefaultActionUpdateStageToSubmittedResponse() {
        return new GetPolicyDefaultActionUpdateStageToSubmittedResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptTransferOfFunds }
     * 
     */
    public ActionGeneralLedgerReceiptTransferOfFunds createActionGeneralLedgerReceiptTransferOfFunds() {
        return new ActionGeneralLedgerReceiptTransferOfFunds();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerAllocationStructureGrouping }
     * 
     */
    public InsertGeneralLedgerAllocationStructureGrouping createInsertGeneralLedgerAllocationStructureGrouping() {
        return new InsertGeneralLedgerAllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerChartOfAccount }
     * 
     */
    public UpdateGeneralLedgerChartOfAccount createUpdateGeneralLedgerChartOfAccount() {
        return new UpdateGeneralLedgerChartOfAccount();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerAllocationMethodResponse }
     * 
     */
    public InsertGeneralLedgerAllocationMethodResponse createInsertGeneralLedgerAllocationMethodResponse() {
        return new InsertGeneralLedgerAllocationMethodResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerChartOfAccountResponse }
     * 
     */
    public UpdateGeneralLedgerChartOfAccountResponse createUpdateGeneralLedgerChartOfAccountResponse() {
        return new UpdateGeneralLedgerChartOfAccountResponse();
    }

    /**
     * Create an instance of {@link GetLineResponse }
     * 
     */
    public GetLineResponse createGetLineResponse() {
        return new GetLineResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public UpdatePolicyCancelPolicyAdditionalInterestResponse createUpdatePolicyCancelPolicyAdditionalInterestResponse() {
        return new UpdatePolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerReceiptResponse }
     * 
     */
    public DeleteGeneralLedgerReceiptResponse createDeleteGeneralLedgerReceiptResponse() {
        return new DeleteGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link GetPolicyCancellationRequestReasonMethodResponse }
     * 
     */
    public GetPolicyCancellationRequestReasonMethodResponse createGetPolicyCancellationRequestReasonMethodResponse() {
        return new GetPolicyCancellationRequestReasonMethodResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyActivateInactivateMultiCarrierSchedule }
     * 
     */
    public ActionPolicyActivateInactivateMultiCarrierSchedule createActionPolicyActivateInactivateMultiCarrierSchedule() {
        return new ActionPolicyActivateInactivateMultiCarrierSchedule();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyRemarkResponse }
     * 
     */
    public GetPolicyCancelPolicyRemarkResponse createGetPolicyCancelPolicyRemarkResponse() {
        return new GetPolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicy }
     * 
     */
    public UpdatePolicy createUpdatePolicy() {
        return new UpdatePolicy();
    }

    /**
     * Create an instance of {@link GetPolicyCancellationRequestReasonMethod }
     * 
     */
    public GetPolicyCancellationRequestReasonMethod createGetPolicyCancellationRequestReasonMethod() {
        return new GetPolicyCancellationRequestReasonMethod();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssueEndorsement }
     * 
     */
    public ActionPolicyIssueNotIssueEndorsement createActionPolicyIssueNotIssueEndorsement() {
        return new ActionPolicyIssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssuePolicyResponse }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssuePolicyResponse createGetPolicyDefaultActionIssueNotIssuePolicyResponse() {
        return new GetPolicyDefaultActionIssueNotIssuePolicyResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerChartOfAccountDefineBankAccount }
     * 
     */
    public GetGeneralLedgerChartOfAccountDefineBankAccount createGetGeneralLedgerChartOfAccountDefineBankAccount() {
        return new GetGeneralLedgerChartOfAccountDefineBankAccount();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionChangePolicyEffectiveExpirationDates }
     * 
     */
    public GetPolicyDefaultActionChangePolicyEffectiveExpirationDates createGetPolicyDefaultActionChangePolicyEffectiveExpirationDates() {
        return new GetPolicyDefaultActionChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionReinstateResponse }
     * 
     */
    public GetPolicyDefaultActionReinstateResponse createGetPolicyDefaultActionReinstateResponse() {
        return new GetPolicyDefaultActionReinstateResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssueEndorsement }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssueEndorsement createGetPolicyDefaultActionIssueNotIssueEndorsement() {
        return new GetPolicyDefaultActionIssueNotIssueEndorsement();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionReinstate }
     * 
     */
    public GetPolicyDefaultActionReinstate createGetPolicyDefaultActionReinstate() {
        return new GetPolicyDefaultActionReinstate();
    }

    /**
     * Create an instance of {@link UpdateAttachmentDetailsResponse }
     * 
     */
    public UpdateAttachmentDetailsResponse createUpdateAttachmentDetailsResponse() {
        return new UpdateAttachmentDetailsResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyUpdateStageToSubmitted }
     * 
     */
    public ActionPolicyUpdateStageToSubmitted createActionPolicyUpdateStageToSubmitted() {
        return new ActionPolicyUpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link ActionPolicyUpdateStageToSubmittedResponse }
     * 
     */
    public ActionPolicyUpdateStageToSubmittedResponse createActionPolicyUpdateStageToSubmittedResponse() {
        return new ActionPolicyUpdateStageToSubmittedResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyActivateInactivateMultiCarrierScheduleResponse }
     * 
     */
    public ActionPolicyActivateInactivateMultiCarrierScheduleResponse createActionPolicyActivateInactivateMultiCarrierScheduleResponse() {
        return new ActionPolicyActivateInactivateMultiCarrierScheduleResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancellationRequestReasonMethodResponse }
     * 
     */
    public UpdatePolicyCancellationRequestReasonMethodResponse createUpdatePolicyCancellationRequestReasonMethodResponse() {
        return new UpdatePolicyCancellationRequestReasonMethodResponse();
    }

    /**
     * Create an instance of {@link UpdateAttachmentMoveFolder }
     * 
     */
    public UpdateAttachmentMoveFolder createUpdateAttachmentMoveFolder() {
        return new UpdateAttachmentMoveFolder();
    }

    /**
     * Create an instance of {@link ActionAttachmentReactivateResponse }
     * 
     */
    public ActionAttachmentReactivateResponse createActionAttachmentReactivateResponse() {
        return new ActionAttachmentReactivateResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerAllocationStructureGrouping }
     * 
     */
    public UpdateGeneralLedgerAllocationStructureGrouping createUpdateGeneralLedgerAllocationStructureGrouping() {
        return new UpdateGeneralLedgerAllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link UpdateLineResponse }
     * 
     */
    public UpdateLineResponse createUpdateLineResponse() {
        return new UpdateLineResponse();
    }

    /**
     * Create an instance of {@link DeletePolicy }
     * 
     */
    public DeletePolicy createDeletePolicy() {
        return new DeletePolicy();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerAllocationStructureGroupingResponse }
     * 
     */
    public UpdateGeneralLedgerAllocationStructureGroupingResponse createUpdateGeneralLedgerAllocationStructureGroupingResponse() {
        return new UpdateGeneralLedgerAllocationStructureGroupingResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseAddLineMidterm }
     * 
     */
    public ActionPolicyEndorseReviseAddLineMidterm createActionPolicyEndorseReviseAddLineMidterm() {
        return new ActionPolicyEndorseReviseAddLineMidterm();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerChartOfAccountDefineBankAccount }
     * 
     */
    public ActionGeneralLedgerChartOfAccountDefineBankAccount createActionGeneralLedgerChartOfAccountDefineBankAccount() {
        return new ActionGeneralLedgerChartOfAccountDefineBankAccount();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionCancel }
     * 
     */
    public GetPolicyDefaultActionCancel createGetPolicyDefaultActionCancel() {
        return new GetPolicyDefaultActionCancel();
    }

    /**
     * Create an instance of {@link UpdateAttachmentBinaryResponse }
     * 
     */
    public UpdateAttachmentBinaryResponse createUpdateAttachmentBinaryResponse() {
        return new UpdateAttachmentBinaryResponse();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerAllocationStructureGrouping }
     * 
     */
    public DeleteGeneralLedgerAllocationStructureGrouping createDeleteGeneralLedgerAllocationStructureGrouping() {
        return new DeleteGeneralLedgerAllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerAllocationMethod }
     * 
     */
    public GetGeneralLedgerAllocationMethod createGetGeneralLedgerAllocationMethod() {
        return new GetGeneralLedgerAllocationMethod();
    }

    /**
     * Create an instance of {@link InsertPolicyResponse }
     * 
     */
    public InsertPolicyResponse createInsertPolicyResponse() {
        return new InsertPolicyResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyResponse }
     * 
     */
    public UpdatePolicyResponse createUpdatePolicyResponse() {
        return new UpdatePolicyResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueCancellation }
     * 
     */
    public ActionPolicyIssueCancellation createActionPolicyIssueCancellation() {
        return new ActionPolicyIssueCancellation();
    }

    /**
     * Create an instance of {@link UpdateAttachmentDetails }
     * 
     */
    public UpdateAttachmentDetails createUpdateAttachmentDetails() {
        return new UpdateAttachmentDetails();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerChartOfAccountResponse }
     * 
     */
    public InsertGeneralLedgerChartOfAccountResponse createInsertGeneralLedgerChartOfAccountResponse() {
        return new InsertGeneralLedgerChartOfAccountResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyRenewResponse }
     * 
     */
    public ActionPolicyRenewResponse createActionPolicyRenewResponse() {
        return new ActionPolicyRenewResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerChartOfAccount }
     * 
     */
    public InsertGeneralLedgerChartOfAccount createInsertGeneralLedgerChartOfAccount() {
        return new InsertGeneralLedgerChartOfAccount();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyProspectiveContractedStatus }
     * 
     */
    public ActionPolicyChangePolicyProspectiveContractedStatus createActionPolicyChangePolicyProspectiveContractedStatus() {
        return new ActionPolicyChangePolicyProspectiveContractedStatus();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptTransferOfFundsResponse }
     * 
     */
    public ActionGeneralLedgerReceiptTransferOfFundsResponse createActionGeneralLedgerReceiptTransferOfFundsResponse() {
        return new ActionGeneralLedgerReceiptTransferOfFundsResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerChartOfAccountResponse }
     * 
     */
    public GetGeneralLedgerChartOfAccountResponse createGetGeneralLedgerChartOfAccountResponse() {
        return new GetGeneralLedgerChartOfAccountResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssuePolicy }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssuePolicy createGetPolicyDefaultActionIssueNotIssuePolicy() {
        return new GetPolicyDefaultActionIssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerAllocationMethodResponse }
     * 
     */
    public GetGeneralLedgerAllocationMethodResponse createGetGeneralLedgerAllocationMethodResponse() {
        return new GetGeneralLedgerAllocationMethodResponse();
    }

    /**
     * Create an instance of {@link InsertPolicy }
     * 
     */
    public InsertPolicy createInsertPolicy() {
        return new InsertPolicy();
    }

    /**
     * Create an instance of {@link DeleteLineResponse }
     * 
     */
    public DeleteLineResponse createDeleteLineResponse() {
        return new DeleteLineResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseAddLineMidtermResponse }
     * 
     */
    public ActionPolicyEndorseReviseAddLineMidtermResponse createActionPolicyEndorseReviseAddLineMidtermResponse() {
        return new ActionPolicyEndorseReviseAddLineMidtermResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyRemark }
     * 
     */
    public DeletePolicyCancelPolicyRemark createDeletePolicyCancelPolicyRemark() {
        return new DeletePolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse }
     * 
     */
    public GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse createGetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse() {
        return new GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse();
    }

    /**
     * Create an instance of {@link DeleteAttachmentResponse }
     * 
     */
    public DeleteAttachmentResponse createDeleteAttachmentResponse() {
        return new DeleteAttachmentResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerReceipt }
     * 
     */
    public InsertGeneralLedgerReceipt createInsertGeneralLedgerReceipt() {
        return new InsertGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultProcessOutstandingPayments }
     * 
     */
    public GetGeneralLedgerReceiptDefaultProcessOutstandingPayments createGetGeneralLedgerReceiptDefaultProcessOutstandingPayments() {
        return new GetGeneralLedgerReceiptDefaultProcessOutstandingPayments();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssueEndorsementResponse }
     * 
     */
    public ActionPolicyIssueNotIssueEndorsementResponse createActionPolicyIssueNotIssueEndorsementResponse() {
        return new ActionPolicyIssueNotIssueEndorsementResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyEffectiveExpirationDates }
     * 
     */
    public ActionPolicyChangePolicyEffectiveExpirationDates createActionPolicyChangePolicyEffectiveExpirationDates() {
        return new ActionPolicyChangePolicyEffectiveExpirationDates();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssuePolicyResponse }
     * 
     */
    public ActionPolicyIssueNotIssuePolicyResponse createActionPolicyIssueNotIssuePolicyResponse() {
        return new ActionPolicyIssueNotIssuePolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueCancellationResponse }
     * 
     */
    public GetPolicyDefaultActionIssueCancellationResponse createGetPolicyDefaultActionIssueCancellationResponse() {
        return new GetPolicyDefaultActionIssueCancellationResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerAllocationStructureGrouping }
     * 
     */
    public GetGeneralLedgerAllocationStructureGrouping createGetGeneralLedgerAllocationStructureGrouping() {
        return new GetGeneralLedgerAllocationStructureGrouping();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerAllocationMethod }
     * 
     */
    public UpdateGeneralLedgerAllocationMethod createUpdateGeneralLedgerAllocationMethod() {
        return new UpdateGeneralLedgerAllocationMethod();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public DeletePolicyCancelPolicyAdditionalInterestResponse createDeletePolicyCancelPolicyAdditionalInterestResponse() {
        return new DeletePolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse }
     * 
     */
    public GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse createGetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse() {
        return new GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse }
     * 
     */
    public GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse createGetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse() {
        return new GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyRenew }
     * 
     */
    public ActionPolicyRenew createActionPolicyRenew() {
        return new ActionPolicyRenew();
    }

    /**
     * Create an instance of {@link GetAttachmentResponse }
     * 
     */
    public GetAttachmentResponse createGetAttachmentResponse() {
        return new GetAttachmentResponse();
    }

    /**
     * Create an instance of {@link DeleteLine }
     * 
     */
    public DeleteLine createDeleteLine() {
        return new DeleteLine();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public InsertPolicyCancelPolicyAdditionalInterestResponse createInsertPolicyCancelPolicyAdditionalInterestResponse() {
        return new InsertPolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueNotIssueEndorsementResponse }
     * 
     */
    public GetPolicyDefaultActionIssueNotIssueEndorsementResponse createGetPolicyDefaultActionIssueNotIssueEndorsementResponse() {
        return new GetPolicyDefaultActionIssueNotIssueEndorsementResponse();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerAllocationMethodResponse }
     * 
     */
    public DeleteGeneralLedgerAllocationMethodResponse createDeleteGeneralLedgerAllocationMethodResponse() {
        return new DeleteGeneralLedgerAllocationMethodResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyRemarkResponse }
     * 
     */
    public DeletePolicyCancelPolicyRemarkResponse createDeletePolicyCancelPolicyRemarkResponse() {
        return new DeletePolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerReceipt }
     * 
     */
    public UpdateGeneralLedgerReceipt createUpdateGeneralLedgerReceipt() {
        return new UpdateGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerAllocationMethod }
     * 
     */
    public InsertGeneralLedgerAllocationMethod createInsertGeneralLedgerAllocationMethod() {
        return new InsertGeneralLedgerAllocationMethod();
    }

    /**
     * Create an instance of {@link UpdateLine }
     * 
     */
    public UpdateLine createUpdateLine() {
        return new UpdateLine();
    }

    /**
     * Create an instance of {@link InsertLineResponse }
     * 
     */
    public InsertLineResponse createInsertLineResponse() {
        return new InsertLineResponse();
    }

    /**
     * Create an instance of {@link InsertLine }
     * 
     */
    public InsertLine createInsertLine() {
        return new InsertLine();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueNotIssuePolicy }
     * 
     */
    public ActionPolicyIssueNotIssuePolicy createActionPolicyIssueNotIssuePolicy() {
        return new ActionPolicyIssueNotIssuePolicy();
    }

    /**
     * Create an instance of {@link ActionPolicyReinstateResponse }
     * 
     */
    public ActionPolicyReinstateResponse createActionPolicyReinstateResponse() {
        return new ActionPolicyReinstateResponse();
    }

    /**
     * Create an instance of {@link DeletePolicyCancelPolicyAdditionalInterest }
     * 
     */
    public DeletePolicyCancelPolicyAdditionalInterest createDeletePolicyCancelPolicyAdditionalInterest() {
        return new DeletePolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerAllocationMethodResponse }
     * 
     */
    public UpdateGeneralLedgerAllocationMethodResponse createUpdateGeneralLedgerAllocationMethodResponse() {
        return new UpdateGeneralLedgerAllocationMethodResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancellationRequestReasonMethod }
     * 
     */
    public UpdatePolicyCancellationRequestReasonMethod createUpdatePolicyCancellationRequestReasonMethod() {
        return new UpdatePolicyCancellationRequestReasonMethod();
    }

    /**
     * Create an instance of {@link UpdateGeneralLedgerReceiptResponse }
     * 
     */
    public UpdateGeneralLedgerReceiptResponse createUpdateGeneralLedgerReceiptResponse() {
        return new UpdateGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceiptResponse }
     * 
     */
    public GetGeneralLedgerReceiptResponse createGetGeneralLedgerReceiptResponse() {
        return new GetGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerChartOfAccountInactivateReactivateResponse }
     * 
     */
    public ActionGeneralLedgerChartOfAccountInactivateReactivateResponse createActionGeneralLedgerChartOfAccountInactivateReactivateResponse() {
        return new ActionGeneralLedgerChartOfAccountInactivateReactivateResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerChartOfAccountDefineBankAccountResponse }
     * 
     */
    public ActionGeneralLedgerChartOfAccountDefineBankAccountResponse createActionGeneralLedgerChartOfAccountDefineBankAccountResponse() {
        return new ActionGeneralLedgerChartOfAccountDefineBankAccountResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyCancel }
     * 
     */
    public ActionPolicyCancel createActionPolicyCancel() {
        return new ActionPolicyCancel();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyAdditionalInterest }
     * 
     */
    public InsertPolicyCancelPolicyAdditionalInterest createInsertPolicyCancelPolicyAdditionalInterest() {
        return new InsertPolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerChartOfAccountResponse }
     * 
     */
    public DeleteGeneralLedgerChartOfAccountResponse createDeleteGeneralLedgerChartOfAccountResponse() {
        return new DeleteGeneralLedgerChartOfAccountResponse();
    }

    /**
     * Create an instance of {@link UpdateAttachmentMoveFolderResponse }
     * 
     */
    public UpdateAttachmentMoveFolderResponse createUpdateAttachmentMoveFolderResponse() {
        return new UpdateAttachmentMoveFolderResponse();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerReceiptResponse }
     * 
     */
    public InsertGeneralLedgerReceiptResponse createInsertGeneralLedgerReceiptResponse() {
        return new InsertGeneralLedgerReceiptResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerChartOfAccount }
     * 
     */
    public GetGeneralLedgerChartOfAccount createGetGeneralLedgerChartOfAccount() {
        return new GetGeneralLedgerChartOfAccount();
    }

    /**
     * Create an instance of {@link DeletePolicyResponse }
     * 
     */
    public DeletePolicyResponse createDeletePolicyResponse() {
        return new DeletePolicyResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyCancelResponse }
     * 
     */
    public ActionPolicyCancelResponse createActionPolicyCancelResponse() {
        return new ActionPolicyCancelResponse();
    }

    /**
     * Create an instance of {@link InsertAttachmentResponse }
     * 
     */
    public InsertAttachmentResponse createInsertAttachmentResponse() {
        return new InsertAttachmentResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionUpdateStageToSubmitted }
     * 
     */
    public GetPolicyDefaultActionUpdateStageToSubmitted createGetPolicyDefaultActionUpdateStageToSubmitted() {
        return new GetPolicyDefaultActionUpdateStageToSubmitted();
    }

    /**
     * Create an instance of {@link GetConfigureGovernmentTaxFeeRateResponse }
     * 
     */
    public GetConfigureGovernmentTaxFeeRateResponse createGetConfigureGovernmentTaxFeeRateResponse() {
        return new GetConfigureGovernmentTaxFeeRateResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerReceipt }
     * 
     */
    public GetGeneralLedgerReceipt createGetGeneralLedgerReceipt() {
        return new GetGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link UpdateAttachmentBinary }
     * 
     */
    public UpdateAttachmentBinary createUpdateAttachmentBinary() {
        return new UpdateAttachmentBinary();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseExistingLineResponse }
     * 
     */
    public ActionPolicyEndorseReviseExistingLineResponse createActionPolicyEndorseReviseExistingLineResponse() {
        return new ActionPolicyEndorseReviseExistingLineResponse();
    }

    /**
     * Create an instance of {@link GetAttachment }
     * 
     */
    public GetAttachment createGetAttachment() {
        return new GetAttachment();
    }

    /**
     * Create an instance of {@link ActionPolicyEndorseReviseExistingLine }
     * 
     */
    public ActionPolicyEndorseReviseExistingLine createActionPolicyEndorseReviseExistingLine() {
        return new ActionPolicyEndorseReviseExistingLine();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerAllocationMethod }
     * 
     */
    public DeleteGeneralLedgerAllocationMethod createDeleteGeneralLedgerAllocationMethod() {
        return new DeleteGeneralLedgerAllocationMethod();
    }

    /**
     * Create an instance of {@link GetConfigureGovernmentTaxFeeRate }
     * 
     */
    public GetConfigureGovernmentTaxFeeRate createGetConfigureGovernmentTaxFeeRate() {
        return new GetConfigureGovernmentTaxFeeRate();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyRemark }
     * 
     */
    public UpdatePolicyCancelPolicyRemark createUpdatePolicyCancelPolicyRemark() {
        return new UpdatePolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyRemark }
     * 
     */
    public GetPolicyCancelPolicyRemark createGetPolicyCancelPolicyRemark() {
        return new GetPolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link InsertAttachment }
     * 
     */
    public InsertAttachment createInsertAttachment() {
        return new InsertAttachment();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptFinalizeReceipt }
     * 
     */
    public ActionGeneralLedgerReceiptFinalizeReceipt createActionGeneralLedgerReceiptFinalizeReceipt() {
        return new ActionGeneralLedgerReceiptFinalizeReceipt();
    }

    /**
     * Create an instance of {@link ActionAttachmentReactivate }
     * 
     */
    public ActionAttachmentReactivate createActionAttachmentReactivate() {
        return new ActionAttachmentReactivate();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyEffectiveExpirationDatesResponse }
     * 
     */
    public ActionPolicyChangePolicyEffectiveExpirationDatesResponse createActionPolicyChangePolicyEffectiveExpirationDatesResponse() {
        return new ActionPolicyChangePolicyEffectiveExpirationDatesResponse();
    }

    /**
     * Create an instance of {@link GetPolicy }
     * 
     */
    public GetPolicy createGetPolicy() {
        return new GetPolicy();
    }

    /**
     * Create an instance of {@link UpdateAttachmentMoveAccountResponse }
     * 
     */
    public UpdateAttachmentMoveAccountResponse createUpdateAttachmentMoveAccountResponse() {
        return new UpdateAttachmentMoveAccountResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerReceiptFinalizeReceiptResponse }
     * 
     */
    public ActionGeneralLedgerReceiptFinalizeReceiptResponse createActionGeneralLedgerReceiptFinalizeReceiptResponse() {
        return new ActionGeneralLedgerReceiptFinalizeReceiptResponse();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerAllocationStructureGroupingResponse }
     * 
     */
    public GetGeneralLedgerAllocationStructureGroupingResponse createGetGeneralLedgerAllocationStructureGroupingResponse() {
        return new GetGeneralLedgerAllocationStructureGroupingResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangeServiceSummaryDescription }
     * 
     */
    public ActionPolicyChangeServiceSummaryDescription createActionPolicyChangeServiceSummaryDescription() {
        return new ActionPolicyChangeServiceSummaryDescription();
    }

    /**
     * Create an instance of {@link ActionPolicyChangeServiceSummaryDescriptionResponse }
     * 
     */
    public ActionPolicyChangeServiceSummaryDescriptionResponse createActionPolicyChangeServiceSummaryDescriptionResponse() {
        return new ActionPolicyChangeServiceSummaryDescriptionResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyChangePolicyProspectiveContractedStatusResponse }
     * 
     */
    public ActionPolicyChangePolicyProspectiveContractedStatusResponse createActionPolicyChangePolicyProspectiveContractedStatusResponse() {
        return new ActionPolicyChangePolicyProspectiveContractedStatusResponse();
    }

    /**
     * Create an instance of {@link InsertPolicyCancelPolicyRemark }
     * 
     */
    public InsertPolicyCancelPolicyRemark createInsertPolicyCancelPolicyRemark() {
        return new InsertPolicyCancelPolicyRemark();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerAllocationStructureGroupingResponse }
     * 
     */
    public DeleteGeneralLedgerAllocationStructureGroupingResponse createDeleteGeneralLedgerAllocationStructureGroupingResponse() {
        return new DeleteGeneralLedgerAllocationStructureGroupingResponse();
    }

    /**
     * Create an instance of {@link UpdateAttachmentMoveAccount }
     * 
     */
    public UpdateAttachmentMoveAccount createUpdateAttachmentMoveAccount() {
        return new UpdateAttachmentMoveAccount();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionRenewResponse }
     * 
     */
    public GetPolicyDefaultActionRenewResponse createGetPolicyDefaultActionRenewResponse() {
        return new GetPolicyDefaultActionRenewResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyIssueCancellationResponse }
     * 
     */
    public ActionPolicyIssueCancellationResponse createActionPolicyIssueCancellationResponse() {
        return new ActionPolicyIssueCancellationResponse();
    }

    /**
     * Create an instance of {@link GetLine }
     * 
     */
    public GetLine createGetLine() {
        return new GetLine();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyAdditionalInterestResponse }
     * 
     */
    public GetPolicyCancelPolicyAdditionalInterestResponse createGetPolicyCancelPolicyAdditionalInterestResponse() {
        return new GetPolicyCancelPolicyAdditionalInterestResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyAdditionalInterest }
     * 
     */
    public UpdatePolicyCancelPolicyAdditionalInterest createUpdatePolicyCancelPolicyAdditionalInterest() {
        return new UpdatePolicyCancelPolicyAdditionalInterest();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionCancelResponse }
     * 
     */
    public GetPolicyDefaultActionCancelResponse createGetPolicyDefaultActionCancelResponse() {
        return new GetPolicyDefaultActionCancelResponse();
    }

    /**
     * Create an instance of {@link ActionPolicyReinstate }
     * 
     */
    public ActionPolicyReinstate createActionPolicyReinstate() {
        return new ActionPolicyReinstate();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerChartOfAccount }
     * 
     */
    public DeleteGeneralLedgerChartOfAccount createDeleteGeneralLedgerChartOfAccount() {
        return new DeleteGeneralLedgerChartOfAccount();
    }

    /**
     * Create an instance of {@link InsertGeneralLedgerAllocationStructureGroupingResponse }
     * 
     */
    public InsertGeneralLedgerAllocationStructureGroupingResponse createInsertGeneralLedgerAllocationStructureGroupingResponse() {
        return new InsertGeneralLedgerAllocationStructureGroupingResponse();
    }

    /**
     * Create an instance of {@link DeleteAttachment }
     * 
     */
    public DeleteAttachment createDeleteAttachment() {
        return new DeleteAttachment();
    }

    /**
     * Create an instance of {@link DeleteGeneralLedgerReceipt }
     * 
     */
    public DeleteGeneralLedgerReceipt createDeleteGeneralLedgerReceipt() {
        return new DeleteGeneralLedgerReceipt();
    }

    /**
     * Create an instance of {@link GetPolicyResponse }
     * 
     */
    public GetPolicyResponse createGetPolicyResponse() {
        return new GetPolicyResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicyCancelPolicyRemarkResponse }
     * 
     */
    public UpdatePolicyCancelPolicyRemarkResponse createUpdatePolicyCancelPolicyRemarkResponse() {
        return new UpdatePolicyCancelPolicyRemarkResponse();
    }

    /**
     * Create an instance of {@link ActionGeneralLedgerChartOfAccountInactivateReactivate }
     * 
     */
    public ActionGeneralLedgerChartOfAccountInactivateReactivate createActionGeneralLedgerChartOfAccountInactivateReactivate() {
        return new ActionGeneralLedgerChartOfAccountInactivateReactivate();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionIssueCancellation }
     * 
     */
    public GetPolicyDefaultActionIssueCancellation createGetPolicyDefaultActionIssueCancellation() {
        return new GetPolicyDefaultActionIssueCancellation();
    }

    /**
     * Create an instance of {@link GetGeneralLedgerChartOfAccountDefineBankAccountResponse }
     * 
     */
    public GetGeneralLedgerChartOfAccountDefineBankAccountResponse createGetGeneralLedgerChartOfAccountDefineBankAccountResponse() {
        return new GetGeneralLedgerChartOfAccountDefineBankAccountResponse();
    }

    /**
     * Create an instance of {@link GetPolicyDefaultActionRenew }
     * 
     */
    public GetPolicyDefaultActionRenew createGetPolicyDefaultActionRenew() {
        return new GetPolicyDefaultActionRenew();
    }

    /**
     * Create an instance of {@link GetPolicyCancelPolicyAdditionalInterest }
     * 
     */
    public GetPolicyCancelPolicyAdditionalInterest createGetPolicyCancelPolicyAdditionalInterest() {
        return new GetPolicyCancelPolicyAdditionalInterest();
    }

}
