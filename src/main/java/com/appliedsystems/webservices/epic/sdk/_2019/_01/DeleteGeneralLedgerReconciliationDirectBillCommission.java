
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReconciliationDirectBillCommissionID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reconciliationDirectBillCommissionID"
})
@XmlRootElement(name = "Delete_GeneralLedger_ReconciliationDirectBillCommission")
public class DeleteGeneralLedgerReconciliationDirectBillCommission {

    @XmlElement(name = "ReconciliationDirectBillCommissionID")
    protected Integer reconciliationDirectBillCommissionID;

    /**
     * Gets the value of the reconciliationDirectBillCommissionID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReconciliationDirectBillCommissionID() {
        return reconciliationDirectBillCommissionID;
    }

    /**
     * Sets the value of the reconciliationDirectBillCommissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReconciliationDirectBillCommissionID(Integer value) {
        this.reconciliationDirectBillCommissionID = value;
    }

}
