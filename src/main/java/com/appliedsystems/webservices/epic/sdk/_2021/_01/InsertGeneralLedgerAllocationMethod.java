
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationMethod;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocationMethodObject" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_configure/_generalledger/}AllocationMethod" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "allocationMethodObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_AllocationMethod")
public class InsertGeneralLedgerAllocationMethod {

    @XmlElement(name = "AllocationMethodObject", nillable = true)
    protected AllocationMethod allocationMethodObject;

    /**
     * Gets the value of the allocationMethodObject property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationMethod }
     *     
     */
    public AllocationMethod getAllocationMethodObject() {
        return allocationMethodObject;
    }

    /**
     * Sets the value of the allocationMethodObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationMethod }
     *     
     */
    public void setAllocationMethodObject(AllocationMethod value) {
        this.allocationMethodObject = value;
    }

}
