
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalPartyGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdjustorGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Summary;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.CancellationRequestReasonMethodGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.AccountsReceivableWriteOff;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.AdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.JournalEntry;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Receipt;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Voucher;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyAdditionalInterestGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineTypeGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._client.SplitReceivableTemplateGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccountGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationEntriesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationMethod;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGrouping;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGroupingsGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.TaxFeeRatesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKCore_2021_02", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/02/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKCore202102 {


    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param clientFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult
     * @throws EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client")
    @WebResult(name = "Get_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClient")
    @ResponseWrapper(localName = "Get_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientResponse")
    public ClientGetResult getClient(
        @WebParam(name = "ClientFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ClientFilter clientFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client")
    @WebResult(name = "Insert_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClient")
    @ResponseWrapper(localName = "Insert_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientResponse")
    public Integer insertClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @throws EpicSDKCore202102UpdateClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client")
    @RequestWrapper(localName = "Update_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClient")
    @ResponseWrapper(localName = "Update_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientResponse")
    public void updateClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate
     * @throws EpicSDKCore202102GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client_SplitReceivableTemplate")
    @WebResult(name = "Get_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Get_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplateResponse")
    public ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplate(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplateGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @throws EpicSDKCore202102UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Update_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Update_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplateResponse")
    public void updateClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client_SplitReceivableTemplate")
    @WebResult(name = "Insert_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Insert_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplateResponse")
    public Integer insertClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateID
     * @throws EpicSDKCore202102DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Delete_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Delete_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Delete_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplateResponse")
    public void deleteClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer splitReceivableTemplateID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Insert_Activity")
    @WebResult(name = "Insert_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivity")
    @ResponseWrapper(localName = "Insert_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivityResponse")
    public Integer insertActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Update_Activity")
    @RequestWrapper(localName = "Update_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivity")
    @ResponseWrapper(localName = "Update_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivityResponse")
    public void updateActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202102GetActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Get_Activity")
    @WebResult(name = "Get_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivity")
    @ResponseWrapper(localName = "Get_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivityResponse")
    public ActivityGetResult getActivity(
        @WebParam(name = "ActivityFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityFilter activityFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param lineFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult
     * @throws EpicSDKCore202102GetLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Get_Line")
    @WebResult(name = "Get_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLine")
    @ResponseWrapper(localName = "Get_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLineResponse")
    public LineGetResult getLine(
        @WebParam(name = "LineFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        LineFilter lineFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetLineInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Insert_Line")
    @WebResult(name = "Insert_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLine")
    @ResponseWrapper(localName = "Insert_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLineResponse")
    public Integer insertLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertLineInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @throws EpicSDKCore202102UpdateLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Update_Line")
    @RequestWrapper(localName = "Update_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLine")
    @ResponseWrapper(localName = "Update_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLineResponse")
    public void updateLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateLineInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineID
     * @throws EpicSDKCore202102DeleteLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteLineAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Delete_Line")
    @RequestWrapper(localName = "Delete_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLine")
    @ResponseWrapper(localName = "Delete_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLineResponse")
    public void deleteLine(
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteLineInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param contactFilterObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult
     * @throws EpicSDKCore202102GetContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Get_Contact")
    @WebResult(name = "Get_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContact")
    @ResponseWrapper(localName = "Get_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContactResponse")
    public ContactGetResult getContact(
        @WebParam(name = "ContactFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ContactFilter contactFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param id
     * @throws EpicSDKCore202102DeleteContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Delete_Contact")
    @RequestWrapper(localName = "Delete_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContact")
    @ResponseWrapper(localName = "Delete_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContactResponse")
    public void deleteContact(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Insert_Contact")
    @WebResult(name = "Insert_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContact")
    @ResponseWrapper(localName = "Insert_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContactResponse")
    public Integer insertContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @throws EpicSDKCore202102UpdateContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Update_Contact")
    @RequestWrapper(localName = "Update_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContact")
    @ResponseWrapper(localName = "Update_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContactResponse")
    public void updateContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactID
     * @throws EpicSDKCore202102ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Contact_ChangeMainBusinessContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Action_Contact_ChangeMainBusinessContact")
    @RequestWrapper(localName = "Action_Contact_ChangeMainBusinessContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContact")
    @ResponseWrapper(localName = "Action_Contact_ChangeMainBusinessContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContactResponse")
    public void actionContactChangeMainBusinessContact(
        @WebParam(name = "ContactID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer contactID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param policyFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult
     * @throws EpicSDKCore202102GetPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy")
    @WebResult(name = "Get_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicy")
    @ResponseWrapper(localName = "Get_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyResponse")
    public PolicyGetResult getPolicy(
        @WebParam(name = "PolicyFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        PolicyFilter policyFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy")
    @WebResult(name = "Insert_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicy")
    @ResponseWrapper(localName = "Insert_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyResponse")
    public Integer insertPolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @throws EpicSDKCore202102UpdatePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy")
    @RequestWrapper(localName = "Update_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicy")
    @ResponseWrapper(localName = "Update_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyResponse")
    public void updatePolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202102DeletePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy")
    @RequestWrapper(localName = "Delete_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicy")
    @ResponseWrapper(localName = "Delete_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyResponse")
    public void deletePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeletePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ActivateInactivateMultiCarrierSchedule")
    @RequestWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyActivateInactivateMultiCarrierSchedule")
    @ResponseWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyActivateInactivateMultiCarrierScheduleResponse")
    public void actionPolicyActivateInactivateMultiCarrierSchedule(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102ActionPolicyCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyCancelInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Cancel", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Cancel")
    @WebResult(name = "Action_Policy_CancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_Cancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyCancel")
    @ResponseWrapper(localName = "Action_Policy_CancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyCancelResponse")
    public Integer actionPolicyCancel(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202102GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionCancel", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionCancel")
    @WebResult(name = "Get_Policy_DefaultActionCancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionCancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionCancel")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionCancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionCancelResponse")
    public ArrayOfCancel getPolicyDefaultActionCancel(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202102GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancellationRequestReasonMethod")
    @WebResult(name = "Get_Policy_CancellationRequestReasonMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Get_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancellationRequestReasonMethodResponse")
    public ArrayOfCancel getPolicyCancellationRequestReasonMethod(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancellationRequestReasonMethodGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancellationRequestReasonMethod")
    @RequestWrapper(localName = "Update_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Update_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancellationRequestReasonMethodResponse")
    public void updatePolicyCancellationRequestReasonMethod(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param lineID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates
     * @throws EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates")
    @WebResult(name = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse")
    public ArrayOfChangePolicyEffectiveExpirationDates getPolicyDefaultActionChangePolicyEffectiveExpirationDates(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param changePolicyEffectiveExpirationDatesObject
     * @throws EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangePolicyEffectiveExpirationDates")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyEffectiveExpirationDatesResponse")
    public void actionPolicyChangePolicyEffectiveExpirationDates(
        @WebParam(name = "ChangePolicyEffectiveExpirationDatesObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyProspectiveContractedStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangePolicyProspectiveContractedStatus")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyProspectiveContractedStatus")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyProspectiveContractedStatusResponse")
    public void actionPolicyChangePolicyProspectiveContractedStatus(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param changeServiceSummaryDescriptionObject
     * @param messageHeader
     * @throws EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangeServiceSummaryDescription", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangeServiceSummaryDescription")
    @RequestWrapper(localName = "Action_Policy_ChangeServiceSummaryDescription", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangeServiceSummaryDescription")
    @ResponseWrapper(localName = "Action_Policy_ChangeServiceSummaryDescriptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangeServiceSummaryDescriptionResponse")
    public void actionPolicyChangeServiceSummaryDescription(
        @WebParam(name = "ChangeServiceSummaryDescriptionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param endorseReviseAddLineMidtermObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseAddLineMidterm", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_EndorseReviseAddLineMidterm")
    @WebResult(name = "Action_Policy_EndorseReviseAddLineMidtermResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseAddLineMidterm", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseAddLineMidterm")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseAddLineMidtermResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseAddLineMidtermResponse")
    public ArrayOfint actionPolicyEndorseReviseAddLineMidterm(
        @WebParam(name = "EndorseReviseAddLineMidtermObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param endorseReviseExistingLineObject
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseExistingLine", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_EndorseReviseExistingLine")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseExistingLine", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseExistingLine")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseExistingLineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseExistingLineResponse")
    public void actionPolicyEndorseReviseExistingLine(
        @WebParam(name = "EndorseReviseExistingLineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        EndorseReviseExistingLine endorseReviseExistingLineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssueEndorsementObject
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueNotIssueEndorsement")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssueEndorsementResponse")
    public void actionPolicyIssueNotIssueEndorsement(
        @WebParam(name = "IssueNotIssueEndorsementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssueEndorsement issueNotIssueEndorsementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueNotIssueEndorsement")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssueEndorsementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssueEndorsementResponse")
    public ArrayOfIssueNotIssueEndorsement getPolicyDefaultActionIssueNotIssueEndorsement(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssueEndorsementGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssuePolicyObject
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueNotIssuePolicy")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssuePolicyResponse")
    public void actionPolicyIssueNotIssuePolicy(
        @WebParam(name = "IssueNotIssuePolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssuePolicy issueNotIssuePolicyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueNotIssuePolicy")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssuePolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssuePolicyResponse")
    public ArrayOfIssueNotIssuePolicy getPolicyDefaultActionIssueNotIssuePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssuePolicyGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueCancellationObject
     * @throws EpicSDKCore202102ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueCancellation")
    @RequestWrapper(localName = "Action_Policy_IssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueCancellation")
    @ResponseWrapper(localName = "Action_Policy_IssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueCancellationResponse")
    public void actionPolicyIssueCancellation(
        @WebParam(name = "IssueCancellationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueCancellation issueCancellationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueCancellation")
    @WebResult(name = "Get_Policy_DefaultActionIssueCancellationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueCancellation")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueCancellationResponse")
    public ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellation(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reinstateObject
     * @throws EpicSDKCore202102ActionPolicyReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyReinstateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Reinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Reinstate")
    @RequestWrapper(localName = "Action_Policy_Reinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyReinstate")
    @ResponseWrapper(localName = "Action_Policy_ReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyReinstateResponse")
    public void actionPolicyReinstate(
        @WebParam(name = "ReinstateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Reinstate reinstateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate
     * @throws EpicSDKCore202102GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionReinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionReinstate")
    @WebResult(name = "Get_Policy_DefaultActionReinstateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionReinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionReinstate")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionReinstateResponse")
    public ArrayOfReinstate getPolicyDefaultActionReinstate(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param renewObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102ActionPolicyRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyRenewMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Renew", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Renew")
    @WebResult(name = "Action_Policy_RenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_Renew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyRenew")
    @ResponseWrapper(localName = "Action_Policy_RenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyRenewResponse")
    public Integer actionPolicyRenew(
        @WebParam(name = "RenewObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Renew renewObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew
     * @throws EpicSDKCore202102GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionRenew", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionRenew")
    @WebResult(name = "Get_Policy_DefaultActionRenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionRenew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionRenew")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionRenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionRenewResponse")
    public ArrayOfRenew getPolicyDefaultActionRenew(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted
     * @throws EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionUpdateStageToSubmitted")
    @WebResult(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionUpdateStageToSubmittedResponse")
    public ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmitted(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param oSubmissionObject
     * @param messageHeader
     * @throws EpicSDKCore202102ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_UpdateStageToSubmitted")
    @RequestWrapper(localName = "Action_Policy_UpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Action_Policy_UpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyUpdateStageToSubmittedResponse")
    public void actionPolicyUpdateStageToSubmitted(
        @WebParam(name = "oSubmissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        UpdateStageToSubmitted oSubmissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyAdditionalInterestsIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest
     * @throws EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Get_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyAdditionalInterestResponse")
    public ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "CancelPolicyAdditionalInterestsIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancelPolicyAdditionalInterestGetType cancelPolicyAdditionalInterestsIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Insert_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyAdditionalInterestResponse")
    public Integer insertPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyAdditionalInterestResponse")
    public void updatePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalInterestID
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyAdditionalInterestResponse")
    public void deletePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "AdditionalInterestID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer additionalInterestID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyRemarksIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark
     * @throws EpicSDKCore202102GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancelPolicyRemark")
    @WebResult(name = "Get_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyRemarkResponse")
    public ArrayOfRemark getPolicyCancelPolicyRemark(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "CancelPolicyRemarksIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancelPolicyRemarksGetType cancelPolicyRemarksIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy_CancelPolicyRemark")
    @WebResult(name = "Insert_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyRemarkResponse")
    public Integer insertPolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyRemarkResponse")
    public void updatePolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param remarkID
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyRemarkResponse")
    public void deletePolicyCancelPolicyRemark(
        @WebParam(name = "RemarkID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer remarkID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102InsertAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertAttachmentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Insert_Attachment")
    @WebResult(name = "Insert_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachment")
    @ResponseWrapper(localName = "Insert_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachmentResponse")
    public ArrayOfint insertAttachment(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @param attachmentFilterObject
     * @param attachmentSortingObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult
     * @throws EpicSDKCore202102GetAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetAttachmentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Get_Attachment")
    @WebResult(name = "Get_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachment")
    @ResponseWrapper(localName = "Get_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachmentResponse")
    public AttachmentGetResult getAttachment(
        @WebParam(name = "AttachmentFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentFilter attachmentFilterObject,
        @WebParam(name = "AttachmentSortingObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentSorting attachmentSortingObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Details", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Details")
    @RequestWrapper(localName = "Update_Attachment_Details", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetails")
    @ResponseWrapper(localName = "Update_Attachment_DetailsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetailsResponse")
    public void updateAttachmentDetails(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveFolder", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveFolder")
    @RequestWrapper(localName = "Update_Attachment_MoveFolder", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolder")
    @ResponseWrapper(localName = "Update_Attachment_MoveFolderResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolderResponse")
    public void updateAttachmentMoveFolder(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveAccount")
    @RequestWrapper(localName = "Update_Attachment_MoveAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccount")
    @ResponseWrapper(localName = "Update_Attachment_MoveAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccountResponse")
    public void updateAttachmentMoveAccount(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Binary", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Binary")
    @WebResult(name = "Update_Attachment_BinaryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Update_Attachment_Binary", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinary")
    @ResponseWrapper(localName = "Update_Attachment_BinaryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinaryResponse")
    public Integer updateAttachmentBinary(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202102DeleteAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteAttachmentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Delete_Attachment")
    @RequestWrapper(localName = "Delete_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachment")
    @ResponseWrapper(localName = "Delete_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachmentResponse")
    public void deleteAttachment(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202102ActionAttachmentReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Attachment_Reactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Action_Attachment_Reactivate")
    @RequestWrapper(localName = "Action_Attachment_Reactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivate")
    @ResponseWrapper(localName = "Action_Attachment_ReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivateResponse")
    public void actionAttachmentReactivate(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionAttachmentReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202102GetTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction")
    @WebResult(name = "Get_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransaction")
    @ResponseWrapper(localName = "Get_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionResponse")
    public TransactionGetResult getTransaction(
        @WebParam(name = "TransactionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionFilter transactionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102InsertTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Insert_Transaction")
    @WebResult(name = "Insert_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransaction")
    @ResponseWrapper(localName = "Insert_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransactionResponse")
    public ArrayOfint insertTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @throws EpicSDKCore202102UpdateTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Update_Transaction")
    @RequestWrapper(localName = "Update_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransaction")
    @ResponseWrapper(localName = "Update_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransactionResponse")
    public void updateTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @param installmentType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202102GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultInstallments", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultInstallments")
    @WebResult(name = "Get_Transaction_DefaultInstallmentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultInstallments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallments")
    @ResponseWrapper(localName = "Get_Transaction_DefaultInstallmentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallmentsResponse")
    public TransactionGetResult getTransactionDefaultInstallments(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "InstallmentType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionGetInstallmentType installmentType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultProducerBrokerCommissions", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultProducerBrokerCommissions")
    @WebResult(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissions", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissions")
    @ResponseWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissionsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissionsResponse")
    public TransactionGetResult getTransactionDefaultProducerBrokerCommissions(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable
     * @throws EpicSDKCore202102GetTransactionReceivablesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionReceivablesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionReceivablesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionReceivablesConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_Receivables", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_Receivables")
    @WebResult(name = "Get_Transaction_ReceivablesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_Receivables", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivables")
    @ResponseWrapper(localName = "Get_Transaction_ReceivablesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivablesResponse")
    public ArrayOfReceivable getTransactionReceivables(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionReceivablesAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionReceivablesConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionReceivablesInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionReceivablesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param accountsReceivableWriteOffObject
     * @throws EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AccountsReceivableWriteOff", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AccountsReceivableWriteOff")
    @RequestWrapper(localName = "Action_Transaction_AccountsReceivableWriteOff", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOff")
    @ResponseWrapper(localName = "Action_Transaction_AccountsReceivableWriteOffResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOffResponse")
    public void actionTransactionAccountsReceivableWriteOff(
        @WebParam(name = "AccountsReceivableWriteOffObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AccountsReceivableWriteOff accountsReceivableWriteOffObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param applyCreditsToDebitsObject
     * @throws EpicSDKCore202102ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ApplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_ApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_ApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebitsResponse")
    public void actionTransactionApplyCreditsToDebits(
        @WebParam(name = "ApplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ApplyCreditsToDebits applyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param associatedAccountID
     * @param messageHeader
     * @param associatedAccountTypeCode
     * @param agencyCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits
     * @throws EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionApplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebitsResponse")
    public ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebits(
        @WebParam(name = "AssociatedAccountTypeCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String associatedAccountTypeCode,
        @WebParam(name = "AssociatedAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer associatedAccountID,
        @WebParam(name = "AgencyCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String agencyCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param financeTransactionObject
     * @throws EpicSDKCore202102ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_FinanceTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_FinanceTransaction")
    @RequestWrapper(localName = "Action_Transaction_FinanceTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransaction")
    @ResponseWrapper(localName = "Action_Transaction_FinanceTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransactionResponse")
    public void actionTransactionFinanceTransaction(
        @WebParam(name = "FinanceTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        FinanceTransaction financeTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @throws EpicSDKCore202102ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_MoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_MoveTransaction")
    @RequestWrapper(localName = "Action_Transaction_MoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransaction")
    @ResponseWrapper(localName = "Action_Transaction_MoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransactionResponse")
    public void actionTransactionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction
     * @throws EpicSDKCore202102GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionMoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionMoveTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionMoveTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionMoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionMoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransactionResponse")
    public ArrayOfMoveTransaction getTransactionDefaultActionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction
     * @throws EpicSDKCore202102GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionReverseTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionReverseTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransactionResponse")
    public ArrayOfReverseTransaction getTransactionDefaultActionReverseTransaction(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reverseTransactionObject
     * @throws EpicSDKCore202102ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ReverseTransaction")
    @RequestWrapper(localName = "Action_Transaction_ReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransaction")
    @ResponseWrapper(localName = "Action_Transaction_ReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransactionResponse")
    public void actionTransactionReverseTransaction(
        @WebParam(name = "ReverseTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ReverseTransaction reverseTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param unapplyCreditsToDebitsObject
     * @param messageHeader
     * @throws EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_UnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_UnapplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_UnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_UnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebitsResponse")
    public void actionTransactionUnapplyCreditsToDebits(
        @WebParam(name = "UnapplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        UnapplyCreditsToDebits unapplyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionDetailNumber
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits
     * @throws EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionUnapplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebitsResponse")
    public ArrayOfUnapplyCreditsToDebits getTransactionDefaultActionUnapplyCreditsToDebits(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "TransactionDetailNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionDetailNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voidPaymentObject
     * @param messageHeader
     * @throws EpicSDKCore202102ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_VoidPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_VoidPayment")
    @RequestWrapper(localName = "Action_Transaction_VoidPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPayment")
    @ResponseWrapper(localName = "Action_Transaction_VoidPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPaymentResponse")
    public void actionTransactionVoidPayment(
        @WebParam(name = "VoidPaymentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        VoidPayment voidPaymentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission
     * @throws EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionAdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionAdjustCommission")
    @WebResult(name = "Get_Transaction_DefaultActionAdjustCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionAdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommission")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionAdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommissionResponse")
    public ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommission(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustCommissionObject
     * @throws EpicSDKCore202102ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AdjustCommission")
    @RequestWrapper(localName = "Action_Transaction_AdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommission")
    @ResponseWrapper(localName = "Action_Transaction_AdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommissionResponse")
    public void actionTransactionAdjustCommission(
        @WebParam(name = "AdjustCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AdjustCommission adjustCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium
     * @throws EpicSDKCore202102GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionRevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionRevisePremium")
    @WebResult(name = "Get_Transaction_DefaultActionRevisePremiumResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionRevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremium")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionRevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremiumResponse")
    public ArrayOfRevisePremium getTransactionDefaultActionRevisePremium(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param revisePremiumObject
     * @throws EpicSDKCore202102ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_RevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_RevisePremium")
    @RequestWrapper(localName = "Action_Transaction_RevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremium")
    @ResponseWrapper(localName = "Action_Transaction_RevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremiumResponse")
    public void actionTransactionRevisePremium(
        @WebParam(name = "RevisePremiumObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        RevisePremium revisePremiumObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee
     * @throws EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionGenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionGenerateTaxFee")
    @WebResult(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFee")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFeeResponse")
    public ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFee(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param generateTaxFeeObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_GenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_GenerateTaxFee")
    @WebResult(name = "Action_Transaction_GenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_Transaction_GenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFee")
    @ResponseWrapper(localName = "Action_Transaction_GenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFeeResponse")
    public ArrayOfint actionTransactionGenerateTaxFee(
        @WebParam(name = "GenerateTaxFeeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        GenerateTaxFee generateTaxFeeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim")
    @WebResult(name = "Insert_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaim")
    @ResponseWrapper(localName = "Insert_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimResponse")
    public Integer insertClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param claimSummaryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult
     * @throws EpicSDKCore202102GetClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim")
    @WebResult(name = "Get_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaim")
    @ResponseWrapper(localName = "Get_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimResponse")
    public ClaimSummaryGetResult getClaim(
        @WebParam(name = "ClaimSummaryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ClaimSummaryFilter claimSummaryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @throws EpicSDKCore202102DeleteClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim")
    @RequestWrapper(localName = "Delete_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaim")
    @ResponseWrapper(localName = "Delete_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimResponse")
    public void deleteClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @throws EpicSDKCore202102UpdateClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim")
    @RequestWrapper(localName = "Update_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaim")
    @ResponseWrapper(localName = "Update_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimResponse")
    public void updateClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty
     * @throws EpicSDKCore202102GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_AdditionalParty")
    @WebResult(name = "Get_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalParty")
    @ResponseWrapper(localName = "Get_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalPartyResponse")
    public ArrayOfAdditionalParty getClaimAdditionalParty(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalPartyGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param additionalPartyID
     * @param messageHeader
     * @throws EpicSDKCore202102DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_AdditionalParty")
    @RequestWrapper(localName = "Delete_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalParty")
    @ResponseWrapper(localName = "Delete_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalPartyResponse")
    public void deleteClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalPartyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_AdditionalParty")
    @WebResult(name = "Insert_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalParty")
    @ResponseWrapper(localName = "Insert_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalPartyResponse")
    public Integer insertClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @throws EpicSDKCore202102UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_AdditionalParty")
    @RequestWrapper(localName = "Update_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalParty")
    @ResponseWrapper(localName = "Update_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalPartyResponse")
    public void updateClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor
     * @throws EpicSDKCore202102GetClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Adjustor")
    @WebResult(name = "Get_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustor")
    @ResponseWrapper(localName = "Get_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustorResponse")
    public ArrayOfAdjustor getClaimAdjustor(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdjustorGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertClaimAdjustorInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_Adjustor")
    @WebResult(name = "Insert_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustor")
    @ResponseWrapper(localName = "Insert_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustorResponse")
    public Integer insertClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @throws EpicSDKCore202102UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimAdjustorInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Adjustor")
    @RequestWrapper(localName = "Update_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustor")
    @ResponseWrapper(localName = "Update_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustorResponse")
    public void updateClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param adjustorID
     * @param messageHeader
     * @throws EpicSDKCore202102DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_Adjustor")
    @RequestWrapper(localName = "Delete_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustor")
    @ResponseWrapper(localName = "Delete_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustorResponse")
    public void deleteClaimAdjustor(
        @WebParam(name = "AdjustorID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer adjustorID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact
     * @throws EpicSDKCore202102GetClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimInsuredContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_InsuredContact")
    @WebResult(name = "Get_Claim_InsuredContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContact")
    @ResponseWrapper(localName = "Get_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContactResponse")
    public ArrayOfInsuredContact getClaimInsuredContact(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param insuredContactObject
     * @throws EpicSDKCore202102UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_InsuredContact")
    @RequestWrapper(localName = "Update_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContact")
    @ResponseWrapper(localName = "Update_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContactResponse")
    public void updateClaimInsuredContact(
        @WebParam(name = "InsuredContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        InsuredContact insuredContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation
     * @throws EpicSDKCore202102GetClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimLitigationInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Litigation")
    @WebResult(name = "Get_Claim_LitigationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigation")
    @ResponseWrapper(localName = "Get_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigationResponse")
    public ArrayOfLitigation getClaimLitigation(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param litigationObject
     * @throws EpicSDKCore202102UpdateClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimLitigationAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Litigation")
    @RequestWrapper(localName = "Update_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigation")
    @ResponseWrapper(localName = "Update_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigationResponse")
    public void updateClaimLitigation(
        @WebParam(name = "LitigationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Litigation litigationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense
     * @throws EpicSDKCore202102GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_PaymentExpense")
    @WebResult(name = "Get_Claim_PaymentExpenseResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpense")
    @ResponseWrapper(localName = "Get_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpenseResponse")
    public ArrayOfPaymentExpense getClaimPaymentExpense(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param paymentExpenseObject
     * @throws EpicSDKCore202102UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_PaymentExpense")
    @RequestWrapper(localName = "Update_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpense")
    @ResponseWrapper(localName = "Update_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpenseResponse")
    public void updateClaimPaymentExpense(
        @WebParam(name = "PaymentExpenseObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PaymentExpense paymentExpenseObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts
     * @throws EpicSDKCore202102GetClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimServicingContactsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetClaimServicingContactsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_ServicingContacts")
    @WebResult(name = "Get_Claim_ServicingContactsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContacts")
    @ResponseWrapper(localName = "Get_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContactsResponse")
    public ArrayOfServicingContacts getClaimServicingContacts(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param servicingContactObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_ServicingContacts")
    @RequestWrapper(localName = "Update_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContacts")
    @ResponseWrapper(localName = "Update_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContactsResponse")
    public void updateClaimServicingContacts(
        @WebParam(name = "ServicingContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ServicingContacts servicingContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param closedDate
     * @param claimID
     * @throws EpicSDKCore202102ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Claim_CloseReopenClaim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Action_Claim_CloseReopenClaim")
    @RequestWrapper(localName = "Action_Claim_CloseReopenClaim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaim")
    @ResponseWrapper(localName = "Action_Claim_CloseReopenClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaimResponse")
    public void actionClaimCloseReopenClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "ClosedDate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        XMLGregorianCalendar closedDate,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param searchTerms
     * @param lookupTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup
     * @throws EpicSDKCore202102GetLookupInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLookupConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLookupMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetLookupAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Lookup", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/ILookup/Get_Lookup")
    @WebResult(name = "Get_LookupResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Lookup", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookup")
    @ResponseWrapper(localName = "Get_LookupResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookupResponse")
    public ArrayOfLookup getLookup(
        @WebParam(name = "LookupTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        LookupTypes lookupTypeObject,
        @WebParam(name = "SearchTerms", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ArrayOfstring searchTerms,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetLookupMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param optionTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType
     * @throws EpicSDKCore202102GetOptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetOptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetOptionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetOptionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Option", action = "http://webservices.appliedsystems.com/epic/sdk/2009/07/IOption/Get_Option")
    @WebResult(name = "Get_OptionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
    @RequestWrapper(localName = "Get_Option", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOption")
    @ResponseWrapper(localName = "Get_OptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOptionResponse")
    public ArrayOfOptionType getOption(
        @WebParam(name = "OptionTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
        OptionTypes optionTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetOptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @param receiptID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_Receipt")
    @WebResult(name = "Get_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptResponse")
    public ReceiptGetResult getGeneralLedgerReceipt(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetType searchType,
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Insert_GeneralLedger_Receipt")
    @WebResult(name = "Insert_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceiptResponse")
    public Integer insertGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Update_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Update_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceiptResponse")
    public void updateGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202102DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Delete_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Delete_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceiptResponse")
    public void deleteGeneralLedgerReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @param detailItemToBeInsertedObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultApplyCreditsToDebits(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "DetailItemToBeInsertedObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        DetailItem detailItemToBeInsertedObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPayments")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultProcessOutstandingPayments(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptFinalizeReceipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptFinalizeReceipt")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceipt")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceiptResponse")
    public void actionGeneralLedgerReceiptFinalizeReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReceiptTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param journalEntryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryFilter journalEntryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntryDefaultDefaultEntry(
        @WebParam(name = "JournalEntryDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryDefaultEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Insert_GeneralLedger_JournalEntry")
    @WebResult(name = "Insert_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Insert_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntryResponse")
    public ArrayOfint insertGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @throws EpicSDKCore202102UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Update_GeneralLedger_JournalEntry")
    @RequestWrapper(localName = "Update_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Update_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntryResponse")
    public void updateGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_JournalEntryTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerJournalEntryTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryVoidObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryVoid")
    @WebResult(name = "Action_GeneralLedger_JournalEntryVoidResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoidResponse")
    public Integer actionGeneralLedgerJournalEntryVoid(
        @WebParam(name = "JournalEntryVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryVoid journalEntryVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param journalEntryID
     * @param messageHeader
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntrySubmit", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntrySubmit")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntrySubmit", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmit")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntrySubmitResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmitResponse")
    public void actionGeneralLedgerJournalEntrySubmit(
        @WebParam(name = "JournalEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_DisbursementTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerDisbursementTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementPayVouchers")
    @WebResult(name = "Action_GeneralLedger_DisbursementPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerDisbursementPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementVoidObject
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoidResponse")
    public void actionGeneralLedgerDisbursementVoid(
        @WebParam(name = "DisbursementVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementVoid disbursementVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param disbursementID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_Disbursement")
    @WebResult(name = "Get_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementResponse")
    public DisbursementGetResult getGeneralLedgerDisbursement(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetType searchType,
        @WebParam(name = "DisbursementID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Insert_GeneralLedger_Disbursement")
    @WebResult(name = "Insert_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Insert_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursementResponse")
    public Integer insertGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @throws EpicSDKCore202102UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Update_GeneralLedger_Disbursement")
    @RequestWrapper(localName = "Update_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Update_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursementResponse")
    public void updateGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param disbursementBankAccountNumberCode
     * @param messageHeader
     * @param disbursementBankSubAccountNumberCode
     * @param disbursementDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntryResponse")
    public DisbursementGetResult getGeneralLedgerDisbursementDefaultDefaultEntry(
        @WebParam(name = "DisbursementDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementDefaultEntryID,
        @WebParam(name = "DisbursementBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankAccountNumberCode,
        @WebParam(name = "DisbursementBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankSubAccountNumberCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerVoucherTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherPayVouchers")
    @WebResult(name = "Action_GeneralLedger_VoucherPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerVoucherPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerVoucherDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherVoidObject
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoidResponse")
    public void actionGeneralLedgerVoucherVoid(
        @WebParam(name = "VoucherVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherVoid voucherVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Insert_GeneralLedger_Voucher")
    @WebResult(name = "Insert_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Insert_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucherResponse")
    public Integer insertGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @throws EpicSDKCore202102UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Update_GeneralLedger_Voucher")
    @RequestWrapper(localName = "Update_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Update_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucherResponse")
    public void updateGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param voucherID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_Voucher")
    @WebResult(name = "Get_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherResponse")
    public VoucherGetResult getGeneralLedgerVoucher(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetType searchType,
        @WebParam(name = "VoucherID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voucherDefaultEntryID
     * @param voucherAccountingMonth
     * @param messageHeader
     * @param voucherBankSubAccountNumberCode
     * @param voucherBankAccountNumberCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntryResponse")
    public VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntry(
        @WebParam(name = "VoucherDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherDefaultEntryID,
        @WebParam(name = "VoucherBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankAccountNumberCode,
        @WebParam(name = "VoucherBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankSubAccountNumberCode,
        @WebParam(name = "VoucherAccountingMonth", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherAccountingMonth,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param id
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Get_GeneralLedger_Activity")
    @WebResult(name = "Get_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivity")
    @ResponseWrapper(localName = "Get_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivityResponse")
    public ActivityGetResult getGeneralLedgerActivity(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetType searchType,
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Insert_GeneralLedger_Activity")
    @WebResult(name = "Insert_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivity")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivityResponse")
    public Integer insertGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Update_GeneralLedger_Activity")
    @RequestWrapper(localName = "Update_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivity")
    @ResponseWrapper(localName = "Update_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivityResponse")
    public void updateGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param directBillCommissionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommissionResponse")
    public DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "DirectBillCommissionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommissionFilter directBillCommissionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommissionResponse")
    public ArrayOfint insertGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void updateGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionID
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void deleteGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationDirectBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param lDepartments
     * @param lEntityLookupCodes
     * @param messageHeader
     * @param lBranches
     * @param lProfitCenters
     * @param lAgencies
     * @param sCompareByType
     * @param lIssuingCompanies
     * @param sClientOrPolicyNumber
     * @param fIncludeHistory
     * @param lLineOfBusiness
     * @param sEntityType
     * @param sDescriptionType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
    public RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies(
        @WebParam(name = "sEntityType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sEntityType,
        @WebParam(name = "lEntityLookupCodes", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lEntityLookupCodes,
        @WebParam(name = "sDescriptionType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sDescriptionType,
        @WebParam(name = "sCompareByType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sCompareByType,
        @WebParam(name = "sClientOrPolicyNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sClientOrPolicyNumber,
        @WebParam(name = "lAgencies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lAgencies,
        @WebParam(name = "lBranches", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lBranches,
        @WebParam(name = "lDepartments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lDepartments,
        @WebParam(name = "lProfitCenters", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lProfitCenters,
        @WebParam(name = "lIssuingCompanies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lIssuingCompanies,
        @WebParam(name = "lLineOfBusiness", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lLineOfBusiness,
        @WebParam(name = "fIncludeHistory", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean fIncludeHistory,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReconciliationBankTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankReopenStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankReopenStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatementResponse")
    public void actionGeneralLedgerReconciliationBankReopenStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationBankFinalizeStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param bankFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Get_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBankResponse")
    public BankGetResult getGeneralLedgerReconciliationBank(
        @WebParam(name = "BankFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankFilter bankFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBankResponse")
    public Integer insertGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBankResponse")
    public void updateGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reconciliationBankID
     * @param messageHeader
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBankResponse")
    public void deleteGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationBankID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType
     * @throws EpicSDKCore202102GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Get_Policy_PolicyLineType")
    @WebResult(name = "Get_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineType")
    @ResponseWrapper(localName = "Get_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineTypeResponse")
    public ArrayOfPolicyLineType getPolicyPolicyLineType(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineTypeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyLineTypeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyLineType")
    @WebResult(name = "Insert_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineType")
    @ResponseWrapper(localName = "Insert_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineTypeResponse")
    public Integer insertPolicyPolicyLineType(
        @WebParam(name = "PolicyLineTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineType policyLineTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyStatusObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyStatus")
    @WebResult(name = "Insert_Policy_PolicyStatusResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatus")
    @ResponseWrapper(localName = "Insert_Policy_PolicyStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatusResponse")
    public Integer insertPolicyPolicyStatus(
        @WebParam(name = "PolicyStatusObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyStatus policyStatusObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode
     * @throws EpicSDKCore202102GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Get_Transaction_TransactionCode")
    @WebResult(name = "Get_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Get_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCode")
    @ResponseWrapper(localName = "Get_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCodeResponse")
    public ArrayOfTransactionCode getTransactionTransactionCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionCodeObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Insert_Transaction_TransactionCode")
    @WebResult(name = "Insert_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Insert_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCode")
    @ResponseWrapper(localName = "Insert_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCodeResponse")
    public Integer insertTransactionTransactionCode(
        @WebParam(name = "TransactionCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCode transactionCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult
     * @throws EpicSDKCore202102GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_SalesTeams", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IConfigureSalesTeam/Get_Configure_SalesTeams")
    @WebResult(name = "Get_Configure_SalesTeamsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_Configure_SalesTeams", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeams")
    @ResponseWrapper(localName = "Get_Configure_SalesTeamsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeamsResponse")
    public SalesTeamGetResult getConfigureSalesTeams(
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Insert_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccountResponse")
    public Integer insertGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountResponse")
    public ChartOfAccountGetResult getGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccountGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean includeInactive,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param chartOfAccountObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Update_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Update_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccountResponse")
    public void updateGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @throws EpicSDKCore202102DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Delete_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccountResponse")
    public void deleteGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationMethodID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationMethod")
    @WebResult(name = "Get_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethodResponse")
    public AllocationMethodsGetResult getGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationEntriesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationMethod")
    @WebResult(name = "Insert_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethodResponse")
    public Integer insertGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethodResponse")
    public void updateGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationMethodID
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethodResponse")
    public void deleteGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationStructureGroupingsID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Get_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGroupingResponse")
    public AllocationStructureGroupingsGetResult getGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGroupingsGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Insert_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGroupingResponse")
    public Integer insertGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGroupingResponse")
    public void updateGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202102UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationStructureGroupingsID
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGroupingResponse")
    public void deleteGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202102DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccountDefineBankAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param bankAccountObject
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountDefineBankAccount")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public void actionGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "BankAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        BankAccount bankAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param ignoreNonZeroBalance
     * @param chartOfAccountID
     * @param shouldInactivateReactivateSubaccounts
     * @param ignoreSubAccountNonZeroBalances
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountInactivateReactivate")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivate")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivateResponse")
    public void actionGeneralLedgerChartOfAccountInactivateReactivate(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "IgnoreNonZeroBalance", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreNonZeroBalance,
        @WebParam(name = "ShouldInactivateReactivateSubaccounts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean shouldInactivateReactivateSubaccounts,
        @WebParam(name = "IgnoreSubAccountNonZeroBalances", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreSubAccountNonZeroBalances,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202102ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode
     * @throws EpicSDKCore202102GetActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetActivityActivityCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Get_Activity_ActivityCode")
    @WebResult(name = "Get_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Get_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCode")
    @ResponseWrapper(localName = "Get_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCodeResponse")
    public ArrayOfActivityCode getActivityActivityCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param activityCodeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202102InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102InsertActivityActivityCodeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Insert_Activity_ActivityCode")
    @WebResult(name = "Insert_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Insert_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCode")
    @ResponseWrapper(localName = "Insert_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCodeResponse")
    public Integer insertActivityActivityCode(
        @WebParam(name = "ActivityCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCode activityCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult
     * @throws EpicSDKCore202102GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202102GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_GovernmentTaxFeeRate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureTaxFeeRate/Get_Configure_GovernmentTaxFeeRate")
    @WebResult(name = "Get_Configure_GovernmentTaxFeeRateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Configure_GovernmentTaxFeeRate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRate")
    @ResponseWrapper(localName = "Get_Configure_GovernmentTaxFeeRateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRateResponse")
    public TaxFeeRateGetResult getConfigureGovernmentTaxFeeRate(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TaxFeeRatesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202102GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
    ;

}
