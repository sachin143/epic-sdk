
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReinstateObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}Reinstate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reinstateObject"
})
@XmlRootElement(name = "Action_Policy_Reinstate")
public class ActionPolicyReinstate {

    @XmlElement(name = "ReinstateObject", nillable = true)
    protected Reinstate reinstateObject;

    /**
     * Gets the value of the reinstateObject property.
     * 
     * @return
     *     possible object is
     *     {@link Reinstate }
     *     
     */
    public Reinstate getReinstateObject() {
        return reinstateObject;
    }

    /**
     * Sets the value of the reinstateObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Reinstate }
     *     
     */
    public void setReinstateObject(Reinstate value) {
        this.reinstateObject = value;
    }

}
