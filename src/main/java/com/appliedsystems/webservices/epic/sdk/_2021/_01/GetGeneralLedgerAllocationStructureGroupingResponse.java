
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_AllocationStructureGroupingResult" type="{http://schemas.appliedsystems.com/epic/sdk/2021/01/_get/}AllocationStructureGroupingsGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerAllocationStructureGroupingResult"
})
@XmlRootElement(name = "Get_GeneralLedger_AllocationStructureGroupingResponse")
public class GetGeneralLedgerAllocationStructureGroupingResponse {

    @XmlElement(name = "Get_GeneralLedger_AllocationStructureGroupingResult", nillable = true)
    protected AllocationStructureGroupingsGetResult getGeneralLedgerAllocationStructureGroupingResult;

    /**
     * Gets the value of the getGeneralLedgerAllocationStructureGroupingResult property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationStructureGroupingsGetResult }
     *     
     */
    public AllocationStructureGroupingsGetResult getGetGeneralLedgerAllocationStructureGroupingResult() {
        return getGeneralLedgerAllocationStructureGroupingResult;
    }

    /**
     * Sets the value of the getGeneralLedgerAllocationStructureGroupingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationStructureGroupingsGetResult }
     *     
     */
    public void setGetGeneralLedgerAllocationStructureGroupingResult(AllocationStructureGroupingsGetResult value) {
        this.getGeneralLedgerAllocationStructureGroupingResult = value;
    }

}
