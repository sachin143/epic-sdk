
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DirectBillCommissionFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/}DirectBillCommissionFilter" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "directBillCommissionFilterObject",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationDirectBillCommission")
public class GetGeneralLedgerReconciliationDirectBillCommission {

    @XmlElement(name = "DirectBillCommissionFilterObject", nillable = true)
    protected DirectBillCommissionFilter directBillCommissionFilterObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the directBillCommissionFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link DirectBillCommissionFilter }
     *     
     */
    public DirectBillCommissionFilter getDirectBillCommissionFilterObject() {
        return directBillCommissionFilterObject;
    }

    /**
     * Sets the value of the directBillCommissionFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectBillCommissionFilter }
     *     
     */
    public void setDirectBillCommissionFilterObject(DirectBillCommissionFilter value) {
        this.directBillCommissionFilterObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
