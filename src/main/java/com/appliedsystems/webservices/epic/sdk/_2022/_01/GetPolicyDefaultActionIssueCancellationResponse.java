
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Policy_DefaultActionIssueCancellationResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}ArrayOfIssueCancellation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPolicyDefaultActionIssueCancellationResult"
})
@XmlRootElement(name = "Get_Policy_DefaultActionIssueCancellationResponse")
public class GetPolicyDefaultActionIssueCancellationResponse {

    @XmlElement(name = "Get_Policy_DefaultActionIssueCancellationResult", nillable = true)
    protected ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellationResult;

    /**
     * Gets the value of the getPolicyDefaultActionIssueCancellationResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfIssueCancellation }
     *     
     */
    public ArrayOfIssueCancellation getGetPolicyDefaultActionIssueCancellationResult() {
        return getPolicyDefaultActionIssueCancellationResult;
    }

    /**
     * Sets the value of the getPolicyDefaultActionIssueCancellationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfIssueCancellation }
     *     
     */
    public void setGetPolicyDefaultActionIssueCancellationResult(ArrayOfIssueCancellation value) {
        this.getPolicyDefaultActionIssueCancellationResult = value;
    }

}
