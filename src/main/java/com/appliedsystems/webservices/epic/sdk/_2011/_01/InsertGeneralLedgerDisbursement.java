
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DisbursementObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}Disbursement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "disbursementObject"
})
@XmlRootElement(name = "Insert_GeneralLedger_Disbursement")
public class InsertGeneralLedgerDisbursement {

    @XmlElement(name = "DisbursementObject", nillable = true)
    protected Disbursement disbursementObject;

    /**
     * Gets the value of the disbursementObject property.
     * 
     * @return
     *     possible object is
     *     {@link Disbursement }
     *     
     */
    public Disbursement getDisbursementObject() {
        return disbursementObject;
    }

    /**
     * Sets the value of the disbursementObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Disbursement }
     *     
     */
    public void setDisbursementObject(Disbursement value) {
        this.disbursementObject = value;
    }

}
