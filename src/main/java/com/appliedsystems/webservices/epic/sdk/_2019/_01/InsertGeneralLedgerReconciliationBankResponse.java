
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_GeneralLedger_ReconciliationBankResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertGeneralLedgerReconciliationBankResult"
})
@XmlRootElement(name = "Insert_GeneralLedger_ReconciliationBankResponse")
public class InsertGeneralLedgerReconciliationBankResponse {

    @XmlElement(name = "Insert_GeneralLedger_ReconciliationBankResult")
    protected Integer insertGeneralLedgerReconciliationBankResult;

    /**
     * Gets the value of the insertGeneralLedgerReconciliationBankResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertGeneralLedgerReconciliationBankResult() {
        return insertGeneralLedgerReconciliationBankResult;
    }

    /**
     * Sets the value of the insertGeneralLedgerReconciliationBankResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertGeneralLedgerReconciliationBankResult(Integer value) {
        this.insertGeneralLedgerReconciliationBankResult = value;
    }

}
