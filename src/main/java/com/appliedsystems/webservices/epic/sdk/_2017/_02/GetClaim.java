
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaimSummaryFilterObject" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/}ClaimSummaryFilter" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "claimSummaryFilterObject",
    "pageNumber"
})
@XmlRootElement(name = "Get_Claim")
public class GetClaim {

    @XmlElement(name = "ClaimSummaryFilterObject", nillable = true)
    protected ClaimSummaryFilter claimSummaryFilterObject;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the claimSummaryFilterObject property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimSummaryFilter }
     *     
     */
    public ClaimSummaryFilter getClaimSummaryFilterObject() {
        return claimSummaryFilterObject;
    }

    /**
     * Sets the value of the claimSummaryFilterObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimSummaryFilter }
     *     
     */
    public void setClaimSummaryFilterObject(ClaimSummaryFilter value) {
        this.claimSummaryFilterObject = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
