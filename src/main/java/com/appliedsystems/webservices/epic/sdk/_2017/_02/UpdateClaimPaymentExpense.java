
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentExpenseObject" type="{http://schemas.appliedsystems.com/epic/sdk/2017/02/_account/_claim/}PaymentExpense" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paymentExpenseObject"
})
@XmlRootElement(name = "Update_Claim_PaymentExpense")
public class UpdateClaimPaymentExpense {

    @XmlElement(name = "PaymentExpenseObject", nillable = true)
    protected PaymentExpense paymentExpenseObject;

    /**
     * Gets the value of the paymentExpenseObject property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentExpense }
     *     
     */
    public PaymentExpense getPaymentExpenseObject() {
        return paymentExpenseObject;
    }

    /**
     * Sets the value of the paymentExpenseObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentExpense }
     *     
     */
    public void setPaymentExpenseObject(PaymentExpense value) {
        this.paymentExpenseObject = value;
    }

}
