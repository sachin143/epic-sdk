
package com.appliedsystems.webservices.epic.sdk._2018._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_ClientResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/}ClientGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClientResult"
})
@XmlRootElement(name = "Get_ClientResponse")
public class GetClientResponse {

    @XmlElement(name = "Get_ClientResult", nillable = true)
    protected ClientGetResult getClientResult;

    /**
     * Gets the value of the getClientResult property.
     * 
     * @return
     *     possible object is
     *     {@link ClientGetResult }
     *     
     */
    public ClientGetResult getGetClientResult() {
        return getClientResult;
    }

    /**
     * Sets the value of the getClientResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientGetResult }
     *     
     */
    public void setGetClientResult(ClientGetResult value) {
        this.getClientResult = value;
    }

}
