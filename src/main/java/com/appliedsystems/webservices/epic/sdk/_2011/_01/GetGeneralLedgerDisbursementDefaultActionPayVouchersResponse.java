
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/_action/}ArrayOfPayVouchers" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerDisbursementDefaultActionPayVouchersResult"
})
@XmlRootElement(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse")
public class GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse {

    @XmlElement(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", nillable = true)
    protected ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchersResult;

    /**
     * Gets the value of the getGeneralLedgerDisbursementDefaultActionPayVouchersResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPayVouchers }
     *     
     */
    public ArrayOfPayVouchers getGetGeneralLedgerDisbursementDefaultActionPayVouchersResult() {
        return getGeneralLedgerDisbursementDefaultActionPayVouchersResult;
    }

    /**
     * Sets the value of the getGeneralLedgerDisbursementDefaultActionPayVouchersResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPayVouchers }
     *     
     */
    public void setGetGeneralLedgerDisbursementDefaultActionPayVouchersResult(ArrayOfPayVouchers value) {
        this.getGeneralLedgerDisbursementDefaultActionPayVouchersResult = value;
    }

}
