
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ArrayOfDiscount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Transaction_DefaultActionDiscountResult" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ArrayOfDiscount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionDefaultActionDiscountResult"
})
@XmlRootElement(name = "Get_Transaction_DefaultActionDiscountResponse")
public class GetTransactionDefaultActionDiscountResponse {

    @XmlElement(name = "Get_Transaction_DefaultActionDiscountResult", nillable = true)
    protected ArrayOfDiscount getTransactionDefaultActionDiscountResult;

    /**
     * Gets the value of the getTransactionDefaultActionDiscountResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDiscount }
     *     
     */
    public ArrayOfDiscount getGetTransactionDefaultActionDiscountResult() {
        return getTransactionDefaultActionDiscountResult;
    }

    /**
     * Sets the value of the getTransactionDefaultActionDiscountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDiscount }
     *     
     */
    public void setGetTransactionDefaultActionDiscountResult(ArrayOfDiscount value) {
        this.getTransactionDefaultActionDiscountResult = value;
    }

}
