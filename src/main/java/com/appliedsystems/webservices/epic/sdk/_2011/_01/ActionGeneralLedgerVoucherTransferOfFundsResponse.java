
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Action_GeneralLedger_VoucherTransferOfFundsResult" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionGeneralLedgerVoucherTransferOfFundsResult"
})
@XmlRootElement(name = "Action_GeneralLedger_VoucherTransferOfFundsResponse")
public class ActionGeneralLedgerVoucherTransferOfFundsResponse {

    @XmlElement(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", nillable = true)
    protected ArrayOfint actionGeneralLedgerVoucherTransferOfFundsResult;

    /**
     * Gets the value of the actionGeneralLedgerVoucherTransferOfFundsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfint }
     *     
     */
    public ArrayOfint getActionGeneralLedgerVoucherTransferOfFundsResult() {
        return actionGeneralLedgerVoucherTransferOfFundsResult;
    }

    /**
     * Sets the value of the actionGeneralLedgerVoucherTransferOfFundsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfint }
     *     
     */
    public void setActionGeneralLedgerVoucherTransferOfFundsResult(ArrayOfint value) {
        this.actionGeneralLedgerVoucherTransferOfFundsResult = value;
    }

}
