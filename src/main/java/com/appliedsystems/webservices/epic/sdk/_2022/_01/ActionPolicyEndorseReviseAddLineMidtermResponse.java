
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Action_Policy_EndorseReviseAddLineMidtermResult" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionPolicyEndorseReviseAddLineMidtermResult"
})
@XmlRootElement(name = "Action_Policy_EndorseReviseAddLineMidtermResponse")
public class ActionPolicyEndorseReviseAddLineMidtermResponse {

    @XmlElement(name = "Action_Policy_EndorseReviseAddLineMidtermResult", nillable = true)
    protected ArrayOfint actionPolicyEndorseReviseAddLineMidtermResult;

    /**
     * Gets the value of the actionPolicyEndorseReviseAddLineMidtermResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfint }
     *     
     */
    public ArrayOfint getActionPolicyEndorseReviseAddLineMidtermResult() {
        return actionPolicyEndorseReviseAddLineMidtermResult;
    }

    /**
     * Sets the value of the actionPolicyEndorseReviseAddLineMidtermResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfint }
     *     
     */
    public void setActionPolicyEndorseReviseAddLineMidtermResult(ArrayOfint value) {
        this.actionPolicyEndorseReviseAddLineMidtermResult = value;
    }

}
