
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.BalanceTransfer;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BalanceTransferObject" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}BalanceTransfer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "balanceTransferObject"
})
@XmlRootElement(name = "Action_Transaction_BalanceTransfer")
public class ActionTransactionBalanceTransfer {

    @XmlElement(name = "BalanceTransferObject", nillable = true)
    protected BalanceTransfer balanceTransferObject;

    /**
     * Gets the value of the balanceTransferObject property.
     * 
     * @return
     *     possible object is
     *     {@link BalanceTransfer }
     *     
     */
    public BalanceTransfer getBalanceTransferObject() {
        return balanceTransferObject;
    }

    /**
     * Sets the value of the balanceTransferObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link BalanceTransfer }
     *     
     */
    public void setBalanceTransferObject(BalanceTransfer value) {
        this.balanceTransferObject = value;
    }

}
