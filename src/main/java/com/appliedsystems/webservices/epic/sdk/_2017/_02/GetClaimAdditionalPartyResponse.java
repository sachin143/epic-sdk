
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_Claim_AdditionalPartyResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_account/_claim/}ArrayOfAdditionalParty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClaimAdditionalPartyResult"
})
@XmlRootElement(name = "Get_Claim_AdditionalPartyResponse")
public class GetClaimAdditionalPartyResponse {

    @XmlElement(name = "Get_Claim_AdditionalPartyResult", nillable = true)
    protected ArrayOfAdditionalParty getClaimAdditionalPartyResult;

    /**
     * Gets the value of the getClaimAdditionalPartyResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAdditionalParty }
     *     
     */
    public ArrayOfAdditionalParty getGetClaimAdditionalPartyResult() {
        return getClaimAdditionalPartyResult;
    }

    /**
     * Sets the value of the getClaimAdditionalPartyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAdditionalParty }
     *     
     */
    public void setGetClaimAdditionalPartyResult(ArrayOfAdditionalParty value) {
        this.getClaimAdditionalPartyResult = value;
    }

}
