
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiAdditionalInterests" type="{http://schemas.appliedsystems.com/epic/sdk/2011/12/_account/_policy/_action/_cancel/}AdditionalInterest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apiAdditionalInterests"
})
@XmlRootElement(name = "Update_Policy_CancelPolicyAdditionalInterest")
public class UpdatePolicyCancelPolicyAdditionalInterest {

    @XmlElement(nillable = true)
    protected AdditionalInterest apiAdditionalInterests;

    /**
     * Gets the value of the apiAdditionalInterests property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalInterest }
     *     
     */
    public AdditionalInterest getApiAdditionalInterests() {
        return apiAdditionalInterests;
    }

    /**
     * Sets the value of the apiAdditionalInterests property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalInterest }
     *     
     */
    public void setApiAdditionalInterests(AdditionalInterest value) {
        this.apiAdditionalInterests = value;
    }

}
