
package com.appliedsystems.webservices.epic.sdk._2021._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RenewObject" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_account/_policy/_action/}Renew" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "renewObject"
})
@XmlRootElement(name = "Action_Policy_Renew")
public class ActionPolicyRenew {

    @XmlElement(name = "RenewObject", nillable = true)
    protected Renew renewObject;

    /**
     * Gets the value of the renewObject property.
     * 
     * @return
     *     possible object is
     *     {@link Renew }
     *     
     */
    public Renew getRenewObject() {
        return renewObject;
    }

    /**
     * Sets the value of the renewObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Renew }
     *     
     */
    public void setRenewObject(Renew value) {
        this.renewObject = value;
    }

}
