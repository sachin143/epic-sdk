
package com.appliedsystems.webservices.epic.sdk._2011._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}DisbursementGetType" minOccurs="0"/>
 *         &lt;element name="DisbursementID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="GetLimitType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}DisbursementGetLimitType" minOccurs="0"/>
 *         &lt;element name="FilterType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}DisbursementFilterType" minOccurs="0"/>
 *         &lt;element name="FilterField1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ComparisonType" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_generalledger/}DisbursementComparisonType" minOccurs="0"/>
 *         &lt;element name="FilterField2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchType",
    "disbursementID",
    "getLimitType",
    "filterType",
    "filterField1",
    "comparisonType",
    "filterField2",
    "pageNumber"
})
@XmlRootElement(name = "Get_GeneralLedger_Disbursement")
public class GetGeneralLedgerDisbursement {

    @XmlElement(name = "SearchType")
    @XmlSchemaType(name = "string")
    protected DisbursementGetType searchType;
    @XmlElement(name = "DisbursementID")
    protected Integer disbursementID;
    @XmlElement(name = "GetLimitType")
    @XmlSchemaType(name = "string")
    protected DisbursementGetLimitType getLimitType;
    @XmlElement(name = "FilterType")
    @XmlSchemaType(name = "string")
    protected DisbursementFilterType filterType;
    @XmlElement(name = "FilterField1", nillable = true)
    protected String filterField1;
    @XmlElement(name = "ComparisonType")
    @XmlSchemaType(name = "string")
    protected DisbursementComparisonType comparisonType;
    @XmlElement(name = "FilterField2", nillable = true)
    protected String filterField2;
    @XmlElement(name = "PageNumber")
    protected Integer pageNumber;

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementGetType }
     *     
     */
    public DisbursementGetType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementGetType }
     *     
     */
    public void setSearchType(DisbursementGetType value) {
        this.searchType = value;
    }

    /**
     * Gets the value of the disbursementID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisbursementID() {
        return disbursementID;
    }

    /**
     * Sets the value of the disbursementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisbursementID(Integer value) {
        this.disbursementID = value;
    }

    /**
     * Gets the value of the getLimitType property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementGetLimitType }
     *     
     */
    public DisbursementGetLimitType getGetLimitType() {
        return getLimitType;
    }

    /**
     * Sets the value of the getLimitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementGetLimitType }
     *     
     */
    public void setGetLimitType(DisbursementGetLimitType value) {
        this.getLimitType = value;
    }

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementFilterType }
     *     
     */
    public DisbursementFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementFilterType }
     *     
     */
    public void setFilterType(DisbursementFilterType value) {
        this.filterType = value;
    }

    /**
     * Gets the value of the filterField1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField1() {
        return filterField1;
    }

    /**
     * Sets the value of the filterField1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField1(String value) {
        this.filterField1 = value;
    }

    /**
     * Gets the value of the comparisonType property.
     * 
     * @return
     *     possible object is
     *     {@link DisbursementComparisonType }
     *     
     */
    public DisbursementComparisonType getComparisonType() {
        return comparisonType;
    }

    /**
     * Sets the value of the comparisonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisbursementComparisonType }
     *     
     */
    public void setComparisonType(DisbursementComparisonType value) {
        this.comparisonType = value;
    }

    /**
     * Gets the value of the filterField2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterField2() {
        return filterField2;
    }

    /**
     * Sets the value of the filterField2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterField2(String value) {
        this.filterField2 = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

}
