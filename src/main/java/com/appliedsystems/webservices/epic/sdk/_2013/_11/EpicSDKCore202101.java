
package com.appliedsystems.webservices.epic.sdk._2013._11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdditionalPartyGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Adjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.AdjustorGetType;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Litigation;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ServicingContacts;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.Summary;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.OptionTypes;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Transaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.TransactionGetInstallmentType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.InsuredContact;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Cancel;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.CancellationRequestReasonMethodGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangePolicyEffectiveExpirationDates;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ChangeServiceSummaryDescription;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.EndorseReviseExistingLine;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueCancellation;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsement;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssueEndorsementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.IssueNotIssuePolicyGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Reinstate;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.Renew;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.AccountsReceivableWriteOff;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.FinanceTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.GenerateTaxFee;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.MoveTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ReverseTransaction;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.RevisePremium;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.UnapplyCreditsToDebits;
import com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.VoidPayment;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.AdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ActivityGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Disbursement;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.DisbursementGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.JournalEntry;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Receipt;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ReceiptGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.Voucher;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherComparisonType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherFilterType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetLimitType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.VoucherGetType;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.PayVouchers;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.TransferOfFunds;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.DisbursementVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.JournalEntryVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.DetailItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.DirectBillCommission;
import com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.VoucherVoid;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentSorting;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.DirectBillCommissionFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.LineFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.AdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyAdditionalInterestGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.CancelPolicyRemarksGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.Remark;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyLineTypeGetType;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.PolicyStatus;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCode;
import com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.TransactionCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.UpdateStageToSubmitted;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult;
import com.appliedsystems.schemas.epic.sdk._2016._01._get.TransactionFilter;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.PaymentExpense;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.Line;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.EndorseReviseAddLineMidterm;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable;
import com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account.SplitReceivableTemplate;
import com.appliedsystems.schemas.epic.sdk._2018._01._account._client.SplitReceivableTemplateGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ChartOfAccountGetType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.BankAccount;
import com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.Bank;
import com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult;
import com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.BankFilter;
import com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ActivityCodeGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationEntriesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationMethod;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGrouping;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.AllocationStructureGroupingsGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.TaxFeeRatesGetType;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult;
import com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "EpicSDKCore_2021_01", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
@XmlSeeAlso({
    com.appliedsystems.schemas.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment._attachedtoitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._claim._summary._reportinghistory._claimcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._client._confidentialclientaccessitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencydefinedcodeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._common._relationshipitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._account._contact._personalclassifications._classificationitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._common._optiontype.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._fault.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._claimsummaryfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._get._clientfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._claim._insuredcontact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._cancel._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._changepolicyeffectiveexpirationdates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuecancellation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissueendorsement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._issuenotissuepolicy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._reinstate._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action._renew._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._commissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._line._producerbrokercommissions._producerbrokerscheduleitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._applycreditstodebits._credititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._financetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._generatetaxfee._generatetaxfeeitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._move._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._movetransaction._moveto._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._reversetransaction._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._installments._summaryitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._revisepremium._splitreceivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action._unapplycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissions._splititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._invoice.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._taskitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._common._adjustcommission._commissionsplititem._producerbrokercommissionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action._payvouchers._eligiblevouchers._eligiblevoucheritem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._disbursement._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._journalentry._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._applycreditstodebits._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._receipt._detail._detailitem._payablescommissions.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._reconciliation._directbillcommission._summary.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._voucher._detail._detailitem._paiditems._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._activityfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._attachment.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger._journalentry.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._linefilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._policyfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._bank.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._01._get.reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2013._11._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action._updatestagetosubmitted._lineitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._configure._salesteam.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2016._01._get._transactionfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpense.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._claim._paymentexpenseitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._contact._identificationnumberitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._action._endorsereviseaddlinemidterm.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._policy._line.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction._receivable._receivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._common._paymentmethoditem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission._recorddetailitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2017._02._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._account._splitreceivabletemplate._splitreceivableitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2018._01._get._contactfilter.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._common._structureitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger._reconciliation.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2020._01._configure._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._account._client._employeeclassitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._common._activity.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._generalledger.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._policyitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerate._transactionitem.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._configure._taxfeerates.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._generalledger._receipt._processoutstandingpayments.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._get.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2021._01._taxfeerate.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._01._account._policy._action.ObjectFactory.class,
    com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._07.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2009._11.filetransfer.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2011._12.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2016._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2017._02.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2018._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2019._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2020._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2021._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._01.ObjectFactory.class,
    com.appliedsystems.webservices.epic.sdk._2022._02.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    com.microsoft.schemas._2003._10.serialization.arrays.ObjectFactory.class
})
public interface EpicSDKCore202101 {


    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param clientFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult
     * @throws EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client")
    @WebResult(name = "Get_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClient")
    @ResponseWrapper(localName = "Get_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientResponse")
    public ClientGetResult getClient(
        @WebParam(name = "ClientFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ClientFilter clientFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client")
    @WebResult(name = "Insert_ClientResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClient")
    @ResponseWrapper(localName = "Insert_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientResponse")
    public Integer insertClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param clientObject
     * @throws EpicSDKCore202101UpdateClientConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client")
    @RequestWrapper(localName = "Update_Client", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClient")
    @ResponseWrapper(localName = "Update_ClientResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientResponse")
    public void updateClient(
        @WebParam(name = "ClientObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Client clientObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClientMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2018._01._account.ArrayOfSplitReceivableTemplate
     * @throws EpicSDKCore202101GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Get_Client_SplitReceivableTemplate")
    @WebResult(name = "Get_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Get_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetClientSplitReceivableTemplateResponse")
    public ArrayOfSplitReceivableTemplate getClientSplitReceivableTemplate(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplateGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @throws EpicSDKCore202101UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Update_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Update_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Update_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.UpdateClientSplitReceivableTemplateResponse")
    public void updateClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Insert_Client_SplitReceivableTemplate")
    @WebResult(name = "Insert_Client_SplitReceivableTemplateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Insert_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertClientSplitReceivableTemplateResponse")
    public Integer insertClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        SplitReceivableTemplate splitReceivableTemplateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param splitReceivableTemplateID
     * @throws EpicSDKCore202101DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Client_SplitReceivableTemplate", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IClient_2018_01/Delete_Client_SplitReceivableTemplate")
    @RequestWrapper(localName = "Delete_Client_SplitReceivableTemplate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplate")
    @ResponseWrapper(localName = "Delete_Client_SplitReceivableTemplateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.DeleteClientSplitReceivableTemplateResponse")
    public void deleteClientSplitReceivableTemplate(
        @WebParam(name = "SplitReceivableTemplateID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Integer splitReceivableTemplateID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteClientSplitReceivableTemplateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteClientSplitReceivableTemplateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteClientSplitReceivableTemplateInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteClientSplitReceivableTemplateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Insert_Activity")
    @WebResult(name = "Insert_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivity")
    @ResponseWrapper(localName = "Insert_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertActivityResponse")
    public Integer insertActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Update_Activity")
    @RequestWrapper(localName = "Update_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivity")
    @ResponseWrapper(localName = "Update_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateActivityResponse")
    public void updateActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202101GetActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IActivity_2017_02/Get_Activity")
    @WebResult(name = "Get_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivity")
    @ResponseWrapper(localName = "Get_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetActivityResponse")
    public ActivityGetResult getActivity(
        @WebParam(name = "ActivityFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityFilter activityFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param lineFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.LineGetResult
     * @throws EpicSDKCore202101GetLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLineMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Get_Line")
    @WebResult(name = "Get_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLine")
    @ResponseWrapper(localName = "Get_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetLineResponse")
    public LineGetResult getLine(
        @WebParam(name = "LineFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        LineFilter lineFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetLineInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertLineAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Insert_Line")
    @WebResult(name = "Insert_LineResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLine")
    @ResponseWrapper(localName = "Insert_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertLineResponse")
    public Integer insertLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertLineInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineObject
     * @throws EpicSDKCore202101UpdateLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateLineAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Update_Line")
    @RequestWrapper(localName = "Update_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLine")
    @ResponseWrapper(localName = "Update_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateLineResponse")
    public void updateLine(
        @WebParam(name = "LineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Line lineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateLineInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param lineID
     * @throws EpicSDKCore202101DeleteLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteLineInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteLineConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Line", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/ILine_2021_01/Delete_Line")
    @RequestWrapper(localName = "Delete_Line", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLine")
    @ResponseWrapper(localName = "Delete_LineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteLineResponse")
    public void deleteLine(
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteLineInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param contactFilterObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult
     * @throws EpicSDKCore202101GetContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Get_Contact")
    @WebResult(name = "Get_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContact")
    @ResponseWrapper(localName = "Get_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetContactResponse")
    public ContactGetResult getContact(
        @WebParam(name = "ContactFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ContactFilter contactFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param id
     * @throws EpicSDKCore202101DeleteContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Delete_Contact")
    @RequestWrapper(localName = "Delete_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContact")
    @ResponseWrapper(localName = "Delete_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteContactResponse")
    public void deleteContact(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Insert_Contact")
    @WebResult(name = "Insert_ContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContact")
    @ResponseWrapper(localName = "Insert_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertContactResponse")
    public Integer insertContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactObject
     * @throws EpicSDKCore202101UpdateContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Contact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Update_Contact")
    @RequestWrapper(localName = "Update_Contact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContact")
    @ResponseWrapper(localName = "Update_ContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateContactResponse")
    public void updateContact(
        @WebParam(name = "ContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Contact contactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param contactID
     * @throws EpicSDKCore202101ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Contact_ChangeMainBusinessContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IContact_2017_02/Action_Contact_ChangeMainBusinessContact")
    @RequestWrapper(localName = "Action_Contact_ChangeMainBusinessContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContact")
    @ResponseWrapper(localName = "Action_Contact_ChangeMainBusinessContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionContactChangeMainBusinessContactResponse")
    public void actionContactChangeMainBusinessContact(
        @WebParam(name = "ContactID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer contactID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionContactChangeMainBusinessContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionContactChangeMainBusinessContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionContactChangeMainBusinessContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionContactChangeMainBusinessContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param policyFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult
     * @throws EpicSDKCore202101GetPolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy")
    @WebResult(name = "Get_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicy")
    @ResponseWrapper(localName = "Get_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyResponse")
    public PolicyGetResult getPolicy(
        @WebParam(name = "PolicyFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        PolicyFilter policyFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertPolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy")
    @WebResult(name = "Insert_PolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicy")
    @ResponseWrapper(localName = "Insert_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyResponse")
    public Integer insertPolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyObject
     * @throws EpicSDKCore202101UpdatePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy")
    @RequestWrapper(localName = "Update_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicy")
    @ResponseWrapper(localName = "Update_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyResponse")
    public void updatePolicy(
        @WebParam(name = "PolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Policy policyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202101DeletePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy")
    @RequestWrapper(localName = "Delete_Policy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicy")
    @ResponseWrapper(localName = "Delete_PolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyResponse")
    public void deletePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeletePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ActivateInactivateMultiCarrierSchedule")
    @RequestWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierSchedule", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyActivateInactivateMultiCarrierSchedule")
    @ResponseWrapper(localName = "Action_Policy_ActivateInactivateMultiCarrierScheduleResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyActivateInactivateMultiCarrierScheduleResponse")
    public void actionPolicyActivateInactivateMultiCarrierSchedule(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyActivateInactivateMultiCarrierScheduleMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101ActionPolicyCancelInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyCancelConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Cancel", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Cancel")
    @WebResult(name = "Action_Policy_CancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_Cancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyCancel")
    @ResponseWrapper(localName = "Action_Policy_CancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyCancelResponse")
    public Integer actionPolicyCancel(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202101GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionCancel", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionCancel")
    @WebResult(name = "Get_Policy_DefaultActionCancelResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionCancel", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionCancel")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionCancelResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionCancelResponse")
    public ArrayOfCancel getPolicyDefaultActionCancel(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionCancelAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionCancelConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionCancelInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionCancelMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfCancel
     * @throws EpicSDKCore202101GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancellationRequestReasonMethod")
    @WebResult(name = "Get_Policy_CancellationRequestReasonMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Get_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancellationRequestReasonMethodResponse")
    public ArrayOfCancel getPolicyCancellationRequestReasonMethod(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancellationRequestReasonMethodGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param cancelObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancellationRequestReasonMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancellationRequestReasonMethod")
    @RequestWrapper(localName = "Update_Policy_CancellationRequestReasonMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancellationRequestReasonMethod")
    @ResponseWrapper(localName = "Update_Policy_CancellationRequestReasonMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancellationRequestReasonMethodResponse")
    public void updatePolicyCancellationRequestReasonMethod(
        @WebParam(name = "CancelObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Cancel cancelObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancellationRequestReasonMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param lineID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfChangePolicyEffectiveExpirationDates
     * @throws EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates")
    @WebResult(name = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesResponse")
    public ArrayOfChangePolicyEffectiveExpirationDates getPolicyDefaultActionChangePolicyEffectiveExpirationDates(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "LineID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer lineID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param changePolicyEffectiveExpirationDatesObject
     * @throws EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyEffectiveExpirationDates", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangePolicyEffectiveExpirationDates")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDates", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyEffectiveExpirationDates")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyEffectiveExpirationDatesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyEffectiveExpirationDatesResponse")
    public void actionPolicyChangePolicyEffectiveExpirationDates(
        @WebParam(name = "ChangePolicyEffectiveExpirationDatesObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChangePolicyEffectiveExpirationDates changePolicyEffectiveExpirationDatesObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyEffectiveExpirationDatesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @throws EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangePolicyProspectiveContractedStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangePolicyProspectiveContractedStatus")
    @RequestWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyProspectiveContractedStatus")
    @ResponseWrapper(localName = "Action_Policy_ChangePolicyProspectiveContractedStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangePolicyProspectiveContractedStatusResponse")
    public void actionPolicyChangePolicyProspectiveContractedStatus(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangePolicyProspectiveContractedStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param changeServiceSummaryDescriptionObject
     * @param messageHeader
     * @throws EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_ChangeServiceSummaryDescription", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_ChangeServiceSummaryDescription")
    @RequestWrapper(localName = "Action_Policy_ChangeServiceSummaryDescription", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangeServiceSummaryDescription")
    @ResponseWrapper(localName = "Action_Policy_ChangeServiceSummaryDescriptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyChangeServiceSummaryDescriptionResponse")
    public void actionPolicyChangeServiceSummaryDescription(
        @WebParam(name = "ChangeServiceSummaryDescriptionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChangeServiceSummaryDescription changeServiceSummaryDescriptionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyChangeServiceSummaryDescriptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param endorseReviseAddLineMidtermObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseAddLineMidterm", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_EndorseReviseAddLineMidterm")
    @WebResult(name = "Action_Policy_EndorseReviseAddLineMidtermResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseAddLineMidterm", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseAddLineMidterm")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseAddLineMidtermResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseAddLineMidtermResponse")
    public ArrayOfint actionPolicyEndorseReviseAddLineMidterm(
        @WebParam(name = "EndorseReviseAddLineMidtermObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        EndorseReviseAddLineMidterm endorseReviseAddLineMidtermObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseAddLineMidtermMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param endorseReviseExistingLineObject
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_EndorseReviseExistingLine", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_EndorseReviseExistingLine")
    @RequestWrapper(localName = "Action_Policy_EndorseReviseExistingLine", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseExistingLine")
    @ResponseWrapper(localName = "Action_Policy_EndorseReviseExistingLineResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyEndorseReviseExistingLineResponse")
    public void actionPolicyEndorseReviseExistingLine(
        @WebParam(name = "EndorseReviseExistingLineObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        EndorseReviseExistingLine endorseReviseExistingLineObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyEndorseReviseExistingLineAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseExistingLineConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseExistingLineInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyEndorseReviseExistingLineMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssueEndorsementObject
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueNotIssueEndorsement")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssueEndorsementResponse")
    public void actionPolicyIssueNotIssueEndorsement(
        @WebParam(name = "IssueNotIssueEndorsementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssueEndorsement issueNotIssueEndorsementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssueEndorsement
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueNotIssueEndorsement")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssueEndorsementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssueEndorsement")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssueEndorsementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssueEndorsementResponse")
    public ArrayOfIssueNotIssueEndorsement getPolicyDefaultActionIssueNotIssueEndorsement(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssueEndorsementGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssueEndorsementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueNotIssuePolicyObject
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueNotIssuePolicy")
    @RequestWrapper(localName = "Action_Policy_IssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Action_Policy_IssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueNotIssuePolicyResponse")
    public void actionPolicyIssueNotIssuePolicy(
        @WebParam(name = "IssueNotIssuePolicyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssuePolicy issueNotIssuePolicyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param searchType
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueNotIssuePolicy
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueNotIssuePolicy", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueNotIssuePolicy")
    @WebResult(name = "Get_Policy_DefaultActionIssueNotIssuePolicyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicy", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssuePolicy")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueNotIssuePolicyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueNotIssuePolicyResponse")
    public ArrayOfIssueNotIssuePolicy getPolicyDefaultActionIssueNotIssuePolicy(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueNotIssuePolicyGetType searchType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueNotIssuePolicyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param issueCancellationObject
     * @throws EpicSDKCore202101ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_IssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_IssueCancellation")
    @RequestWrapper(localName = "Action_Policy_IssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueCancellation")
    @ResponseWrapper(localName = "Action_Policy_IssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyIssueCancellationResponse")
    public void actionPolicyIssueCancellation(
        @WebParam(name = "IssueCancellationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        IssueCancellation issueCancellationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @param serviceSummaryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfIssueCancellation
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionIssueCancellation", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionIssueCancellation")
    @WebResult(name = "Get_Policy_DefaultActionIssueCancellationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionIssueCancellation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueCancellation")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionIssueCancellationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionIssueCancellationResponse")
    public ArrayOfIssueCancellation getPolicyDefaultActionIssueCancellation(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "ServiceSummaryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer serviceSummaryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionIssueCancellationAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueCancellationConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueCancellationInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionIssueCancellationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reinstateObject
     * @throws EpicSDKCore202101ActionPolicyReinstateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyReinstateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Reinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Reinstate")
    @RequestWrapper(localName = "Action_Policy_Reinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyReinstate")
    @ResponseWrapper(localName = "Action_Policy_ReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyReinstateResponse")
    public void actionPolicyReinstate(
        @WebParam(name = "ReinstateObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Reinstate reinstateObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfReinstate
     * @throws EpicSDKCore202101GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionReinstate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionReinstate")
    @WebResult(name = "Get_Policy_DefaultActionReinstateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionReinstate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionReinstate")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionReinstateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionReinstateResponse")
    public ArrayOfReinstate getPolicyDefaultActionReinstate(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionReinstateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionReinstateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionReinstateInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionReinstateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param renewObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101ActionPolicyRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyRenewAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyRenewConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_Renew", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_Renew")
    @WebResult(name = "Action_Policy_RenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_Policy_Renew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyRenew")
    @ResponseWrapper(localName = "Action_Policy_RenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyRenewResponse")
    public Integer actionPolicyRenew(
        @WebParam(name = "RenewObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Renew renewObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._policy._action.ArrayOfRenew
     * @throws EpicSDKCore202101GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionRenew", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionRenew")
    @WebResult(name = "Get_Policy_DefaultActionRenewResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionRenew", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionRenew")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionRenewResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionRenewResponse")
    public ArrayOfRenew getPolicyDefaultActionRenew(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionRenewAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionRenewConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionRenewInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionRenewMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyID
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._account._policy._action.ArrayOfUpdateStageToSubmitted
     * @throws EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_DefaultActionUpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_DefaultActionUpdateStageToSubmitted")
    @WebResult(name = "Get_Policy_DefaultActionUpdateStageToSubmittedResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Get_Policy_DefaultActionUpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyDefaultActionUpdateStageToSubmittedResponse")
    public ArrayOfUpdateStageToSubmitted getPolicyDefaultActionUpdateStageToSubmitted(
        @WebParam(name = "PolicyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer policyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyDefaultActionUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param oSubmissionObject
     * @param messageHeader
     * @throws EpicSDKCore202101ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Policy_UpdateStageToSubmitted", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Action_Policy_UpdateStageToSubmitted")
    @RequestWrapper(localName = "Action_Policy_UpdateStageToSubmitted", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyUpdateStageToSubmitted")
    @ResponseWrapper(localName = "Action_Policy_UpdateStageToSubmittedResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionPolicyUpdateStageToSubmittedResponse")
    public void actionPolicyUpdateStageToSubmitted(
        @WebParam(name = "oSubmissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        UpdateStageToSubmitted oSubmissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionPolicyUpdateStageToSubmittedAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyUpdateStageToSubmittedConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyUpdateStageToSubmittedInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionPolicyUpdateStageToSubmittedMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyAdditionalInterestsIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfAdditionalInterest
     * @throws EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Get_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyAdditionalInterestResponse")
    public ArrayOfAdditionalInterest getPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "CancelPolicyAdditionalInterestsIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancelPolicyAdditionalInterestGetType cancelPolicyAdditionalInterestsIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy_CancelPolicyAdditionalInterest")
    @WebResult(name = "Insert_Policy_CancelPolicyAdditionalInterestResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyAdditionalInterestResponse")
    public Integer insertPolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param apiAdditionalInterests
     * @param messageHeader
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyAdditionalInterestResponse")
    public void updatePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "apiAdditionalInterests", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AdditionalInterest apiAdditionalInterests,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalInterestID
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyAdditionalInterest", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy_CancelPolicyAdditionalInterest")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterest", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyAdditionalInterest")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyAdditionalInterestResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyAdditionalInterestResponse")
    public void deletePolicyCancelPolicyAdditionalInterest(
        @WebParam(name = "AdditionalInterestID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer additionalInterestID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyAdditionalInterestMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param cancelPolicyRemarksIDType
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._account._policy._action._cancel.ArrayOfRemark
     * @throws EpicSDKCore202101GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Get_Policy_CancelPolicyRemark")
    @WebResult(name = "Get_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Get_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetPolicyCancelPolicyRemarkResponse")
    public ArrayOfRemark getPolicyCancelPolicyRemark(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer id,
        @WebParam(name = "CancelPolicyRemarksIDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        CancelPolicyRemarksGetType cancelPolicyRemarksIDType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Insert_Policy_CancelPolicyRemark")
    @WebResult(name = "Insert_Policy_CancelPolicyRemarkResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Insert_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertPolicyCancelPolicyRemarkResponse")
    public Integer insertPolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertPolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param apiRemark
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Update_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Update_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Update_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdatePolicyCancelPolicyRemarkResponse")
    public void updatePolicyCancelPolicyRemark(
        @WebParam(name = "apiRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Remark apiRemark,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdatePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdatePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param remarkID
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Policy_CancelPolicyRemark", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/Policy_2021_01/Delete_Policy_CancelPolicyRemark")
    @RequestWrapper(localName = "Delete_Policy_CancelPolicyRemark", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyRemark")
    @ResponseWrapper(localName = "Delete_Policy_CancelPolicyRemarkResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeletePolicyCancelPolicyRemarkResponse")
    public void deletePolicyCancelPolicyRemark(
        @WebParam(name = "RemarkID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer remarkID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeletePolicyCancelPolicyRemarkAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyRemarkConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyRemarkInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeletePolicyCancelPolicyRemarkMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101InsertAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertAttachmentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Insert_Attachment")
    @WebResult(name = "Insert_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachment")
    @ResponseWrapper(localName = "Insert_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertAttachmentResponse")
    public ArrayOfint insertAttachment(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @param attachmentFilterObject
     * @param attachmentSortingObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentGetResult
     * @throws EpicSDKCore202101GetAttachmentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetAttachmentInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Get_Attachment")
    @WebResult(name = "Get_AttachmentResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachment")
    @ResponseWrapper(localName = "Get_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetAttachmentResponse")
    public AttachmentGetResult getAttachment(
        @WebParam(name = "AttachmentFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentFilter attachmentFilterObject,
        @WebParam(name = "AttachmentSortingObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AttachmentSorting attachmentSortingObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Details", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Details")
    @RequestWrapper(localName = "Update_Attachment_Details", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetails")
    @ResponseWrapper(localName = "Update_Attachment_DetailsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentDetailsResponse")
    public void updateAttachmentDetails(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateAttachmentDetailsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentDetailsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentDetailsInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentDetailsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveFolder", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveFolder")
    @RequestWrapper(localName = "Update_Attachment_MoveFolder", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolder")
    @ResponseWrapper(localName = "Update_Attachment_MoveFolderResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveFolderResponse")
    public void updateAttachmentMoveFolder(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateAttachmentMoveFolderAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveFolderConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveFolderInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveFolderMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_MoveAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_MoveAccount")
    @RequestWrapper(localName = "Update_Attachment_MoveAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccount")
    @ResponseWrapper(localName = "Update_Attachment_MoveAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentMoveAccountResponse")
    public void updateAttachmentMoveAccount(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateAttachmentMoveAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentMoveAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param attachmentObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Attachment_Binary", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Update_Attachment_Binary")
    @WebResult(name = "Update_Attachment_BinaryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Update_Attachment_Binary", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinary")
    @ResponseWrapper(localName = "Update_Attachment_BinaryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateAttachmentBinaryResponse")
    public Integer updateAttachmentBinary(
        @WebParam(name = "AttachmentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Attachment attachmentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202101DeleteAttachmentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteAttachmentConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteAttachmentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteAttachmentMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Attachment", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Delete_Attachment")
    @RequestWrapper(localName = "Delete_Attachment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachment")
    @ResponseWrapper(localName = "Delete_AttachmentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteAttachmentResponse")
    public void deleteAttachment(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteAttachmentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param attachmentID
     * @throws EpicSDKCore202101ActionAttachmentReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Attachment_Reactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IAttachment_2021_01/Action_Attachment_Reactivate")
    @RequestWrapper(localName = "Action_Attachment_Reactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivate")
    @ResponseWrapper(localName = "Action_Attachment_ReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionAttachmentReactivateResponse")
    public void actionAttachmentReactivate(
        @WebParam(name = "AttachmentID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer attachmentID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionAttachmentReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionAttachmentReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionAttachmentReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionAttachmentReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202101GetTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction")
    @WebResult(name = "Get_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransaction")
    @ResponseWrapper(localName = "Get_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionResponse")
    public TransactionGetResult getTransaction(
        @WebParam(name = "TransactionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionFilter transactionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101InsertTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Insert_Transaction")
    @WebResult(name = "Insert_TransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransaction")
    @ResponseWrapper(localName = "Insert_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertTransactionResponse")
    public ArrayOfint insertTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @throws EpicSDKCore202101UpdateTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateTransactionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Transaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Update_Transaction")
    @RequestWrapper(localName = "Update_Transaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransaction")
    @ResponseWrapper(localName = "Update_TransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateTransactionResponse")
    public void updateTransaction(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @param installmentType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202101GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultInstallments", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultInstallments")
    @WebResult(name = "Get_Transaction_DefaultInstallmentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultInstallments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallments")
    @ResponseWrapper(localName = "Get_Transaction_DefaultInstallmentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultInstallmentsResponse")
    public TransactionGetResult getTransactionDefaultInstallments(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "InstallmentType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransactionGetInstallmentType installmentType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultInstallmentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultInstallmentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultInstallmentsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultInstallmentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.TransactionGetResult
     * @throws EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultProducerBrokerCommissions", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultProducerBrokerCommissions")
    @WebResult(name = "Get_Transaction_DefaultProducerBrokerCommissionsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissions", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissions")
    @ResponseWrapper(localName = "Get_Transaction_DefaultProducerBrokerCommissionsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultProducerBrokerCommissionsResponse")
    public TransactionGetResult getTransactionDefaultProducerBrokerCommissions(
        @WebParam(name = "TransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Transaction transactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultProducerBrokerCommissionsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._transaction.ArrayOfReceivable
     * @throws EpicSDKCore202101GetTransactionReceivablesMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionReceivablesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionReceivablesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionReceivablesInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_Receivables", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_Receivables")
    @WebResult(name = "Get_Transaction_ReceivablesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_Receivables", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivables")
    @ResponseWrapper(localName = "Get_Transaction_ReceivablesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionReceivablesResponse")
    public ArrayOfReceivable getTransactionReceivables(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionReceivablesAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionReceivablesConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionReceivablesInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionReceivablesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param accountsReceivableWriteOffObject
     * @throws EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AccountsReceivableWriteOff", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AccountsReceivableWriteOff")
    @RequestWrapper(localName = "Action_Transaction_AccountsReceivableWriteOff", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOff")
    @ResponseWrapper(localName = "Action_Transaction_AccountsReceivableWriteOffResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAccountsReceivableWriteOffResponse")
    public void actionTransactionAccountsReceivableWriteOff(
        @WebParam(name = "AccountsReceivableWriteOffObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AccountsReceivableWriteOff accountsReceivableWriteOffObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAccountsReceivableWriteOffMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param applyCreditsToDebitsObject
     * @throws EpicSDKCore202101ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ApplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_ApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_ApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionApplyCreditsToDebitsResponse")
    public void actionTransactionApplyCreditsToDebits(
        @WebParam(name = "ApplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ApplyCreditsToDebits applyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param associatedAccountID
     * @param messageHeader
     * @param associatedAccountTypeCode
     * @param agencyCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfApplyCreditsToDebits
     * @throws EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionApplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionApplyCreditsToDebitsResponse")
    public ArrayOfApplyCreditsToDebits getTransactionDefaultActionApplyCreditsToDebits(
        @WebParam(name = "AssociatedAccountTypeCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String associatedAccountTypeCode,
        @WebParam(name = "AssociatedAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer associatedAccountID,
        @WebParam(name = "AgencyCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String agencyCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param financeTransactionObject
     * @throws EpicSDKCore202101ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_FinanceTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_FinanceTransaction")
    @RequestWrapper(localName = "Action_Transaction_FinanceTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransaction")
    @ResponseWrapper(localName = "Action_Transaction_FinanceTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionFinanceTransactionResponse")
    public void actionTransactionFinanceTransaction(
        @WebParam(name = "FinanceTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        FinanceTransaction financeTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionFinanceTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionFinanceTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionFinanceTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionFinanceTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @throws EpicSDKCore202101ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_MoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_MoveTransaction")
    @RequestWrapper(localName = "Action_Transaction_MoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransaction")
    @ResponseWrapper(localName = "Action_Transaction_MoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionMoveTransactionResponse")
    public void actionTransactionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param moveTransactionObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfMoveTransaction
     * @throws EpicSDKCore202101GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionMoveTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionMoveTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionMoveTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionMoveTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionMoveTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionMoveTransactionResponse")
    public ArrayOfMoveTransaction getTransactionDefaultActionMoveTransaction(
        @WebParam(name = "MoveTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        MoveTransaction moveTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionMoveTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionMoveTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionMoveTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionMoveTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfReverseTransaction
     * @throws EpicSDKCore202101GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionReverseTransaction")
    @WebResult(name = "Get_Transaction_DefaultActionReverseTransactionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransaction")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionReverseTransactionResponse")
    public ArrayOfReverseTransaction getTransactionDefaultActionReverseTransaction(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reverseTransactionObject
     * @throws EpicSDKCore202101ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_ReverseTransaction", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_ReverseTransaction")
    @RequestWrapper(localName = "Action_Transaction_ReverseTransaction", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransaction")
    @ResponseWrapper(localName = "Action_Transaction_ReverseTransactionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionReverseTransactionResponse")
    public void actionTransactionReverseTransaction(
        @WebParam(name = "ReverseTransactionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ReverseTransaction reverseTransactionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionReverseTransactionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionReverseTransactionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionReverseTransactionInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionReverseTransactionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param unapplyCreditsToDebitsObject
     * @param messageHeader
     * @throws EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_UnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_UnapplyCreditsToDebits")
    @RequestWrapper(localName = "Action_Transaction_UnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Action_Transaction_UnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionUnapplyCreditsToDebitsResponse")
    public void actionTransactionUnapplyCreditsToDebits(
        @WebParam(name = "UnapplyCreditsToDebitsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        UnapplyCreditsToDebits unapplyCreditsToDebitsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionDetailNumber
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfUnapplyCreditsToDebits
     * @throws EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionUnapplyCreditsToDebits")
    @WebResult(name = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionUnapplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionUnapplyCreditsToDebitsResponse")
    public ArrayOfUnapplyCreditsToDebits getTransactionDefaultActionUnapplyCreditsToDebits(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "TransactionDetailNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionDetailNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionUnapplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voidPaymentObject
     * @param messageHeader
     * @throws EpicSDKCore202101ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_VoidPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_VoidPayment")
    @RequestWrapper(localName = "Action_Transaction_VoidPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPayment")
    @ResponseWrapper(localName = "Action_Transaction_VoidPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionVoidPaymentResponse")
    public void actionTransactionVoidPayment(
        @WebParam(name = "VoidPaymentObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        VoidPayment voidPaymentObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionVoidPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionVoidPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionVoidPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionVoidPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._common.ArrayOfAdjustCommission
     * @throws EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionAdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionAdjustCommission")
    @WebResult(name = "Get_Transaction_DefaultActionAdjustCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionAdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommission")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionAdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionAdjustCommissionResponse")
    public ArrayOfAdjustCommission getTransactionDefaultActionAdjustCommission(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustCommissionObject
     * @throws EpicSDKCore202101ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_AdjustCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_AdjustCommission")
    @RequestWrapper(localName = "Action_Transaction_AdjustCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommission")
    @ResponseWrapper(localName = "Action_Transaction_AdjustCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionAdjustCommissionResponse")
    public void actionTransactionAdjustCommission(
        @WebParam(name = "AdjustCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        AdjustCommission adjustCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionAdjustCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAdjustCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAdjustCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionAdjustCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfRevisePremium
     * @throws EpicSDKCore202101GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionRevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionRevisePremium")
    @WebResult(name = "Get_Transaction_DefaultActionRevisePremiumResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionRevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremium")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionRevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionRevisePremiumResponse")
    public ArrayOfRevisePremium getTransactionDefaultActionRevisePremium(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param revisePremiumObject
     * @throws EpicSDKCore202101ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_RevisePremium", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_RevisePremium")
    @RequestWrapper(localName = "Action_Transaction_RevisePremium", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremium")
    @ResponseWrapper(localName = "Action_Transaction_RevisePremiumResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionRevisePremiumResponse")
    public void actionTransactionRevisePremium(
        @WebParam(name = "RevisePremiumObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        RevisePremium revisePremiumObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionRevisePremiumAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionRevisePremiumConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionRevisePremiumInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionRevisePremiumMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transactionID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._transaction._action.ArrayOfGenerateTaxFee
     * @throws EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_DefaultActionGenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Get_Transaction_DefaultActionGenerateTaxFee")
    @WebResult(name = "Get_Transaction_DefaultActionGenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFee")
    @ResponseWrapper(localName = "Get_Transaction_DefaultActionGenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetTransactionDefaultActionGenerateTaxFeeResponse")
    public ArrayOfGenerateTaxFee getTransactionDefaultActionGenerateTaxFee(
        @WebParam(name = "TransactionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer transactionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionDefaultActionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param generateTaxFeeObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Transaction_GenerateTaxFee", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/ITransaction_2019_01/Action_Transaction_GenerateTaxFee")
    @WebResult(name = "Action_Transaction_GenerateTaxFeeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_Transaction_GenerateTaxFee", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFee")
    @ResponseWrapper(localName = "Action_Transaction_GenerateTaxFeeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionTransactionGenerateTaxFeeResponse")
    public ArrayOfint actionTransactionGenerateTaxFee(
        @WebParam(name = "GenerateTaxFeeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        GenerateTaxFee generateTaxFeeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionTransactionGenerateTaxFeeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionGenerateTaxFeeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionGenerateTaxFeeInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionTransactionGenerateTaxFeeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim")
    @WebResult(name = "Insert_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaim")
    @ResponseWrapper(localName = "Insert_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimResponse")
    public Integer insertClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param claimSummaryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult
     * @throws EpicSDKCore202101GetClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim")
    @WebResult(name = "Get_ClaimResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaim")
    @ResponseWrapper(localName = "Get_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimResponse")
    public ClaimSummaryGetResult getClaim(
        @WebParam(name = "ClaimSummaryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ClaimSummaryFilter claimSummaryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @throws EpicSDKCore202101DeleteClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim")
    @RequestWrapper(localName = "Delete_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaim")
    @ResponseWrapper(localName = "Delete_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimResponse")
    public void deleteClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param summaryObject
     * @throws EpicSDKCore202101UpdateClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim")
    @RequestWrapper(localName = "Update_Claim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaim")
    @ResponseWrapper(localName = "Update_ClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimResponse")
    public void updateClaim(
        @WebParam(name = "SummaryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Summary summaryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdditionalParty
     * @throws EpicSDKCore202101GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_AdditionalParty")
    @WebResult(name = "Get_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalParty")
    @ResponseWrapper(localName = "Get_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdditionalPartyResponse")
    public ArrayOfAdditionalParty getClaimAdditionalParty(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalPartyGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param additionalPartyID
     * @param messageHeader
     * @throws EpicSDKCore202101DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_AdditionalParty")
    @RequestWrapper(localName = "Delete_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalParty")
    @ResponseWrapper(localName = "Delete_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdditionalPartyResponse")
    public void deleteClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer additionalPartyID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_AdditionalParty")
    @WebResult(name = "Insert_Claim_AdditionalPartyResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalParty")
    @ResponseWrapper(localName = "Insert_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdditionalPartyResponse")
    public Integer insertClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param additionalPartyObject
     * @throws EpicSDKCore202101UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_AdditionalParty", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_AdditionalParty")
    @RequestWrapper(localName = "Update_Claim_AdditionalParty", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalParty")
    @ResponseWrapper(localName = "Update_Claim_AdditionalPartyResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdditionalPartyResponse")
    public void updateClaimAdditionalParty(
        @WebParam(name = "AdditionalPartyObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdditionalParty additionalPartyObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimAdditionalPartyAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdditionalPartyConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdditionalPartyInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdditionalPartyMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param idType
     * @param messageHeader
     * @param id
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfAdjustor
     * @throws EpicSDKCore202101GetClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimAdjustorMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Adjustor")
    @WebResult(name = "Get_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustor")
    @ResponseWrapper(localName = "Get_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimAdjustorResponse")
    public ArrayOfAdjustor getClaimAdjustor(
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "IDType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        AdjustorGetType idType,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdjustorAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertClaimAdjustorConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Insert_Claim_Adjustor")
    @WebResult(name = "Insert_Claim_AdjustorResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustor")
    @ResponseWrapper(localName = "Insert_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertClaimAdjustorResponse")
    public Integer insertClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param adjustorObject
     * @throws EpicSDKCore202101UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Adjustor")
    @RequestWrapper(localName = "Update_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustor")
    @ResponseWrapper(localName = "Update_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimAdjustorResponse")
    public void updateClaimAdjustor(
        @WebParam(name = "AdjustorObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Adjustor adjustorObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param adjustorID
     * @param messageHeader
     * @throws EpicSDKCore202101DeleteClaimAdjustorInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_Claim_Adjustor", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Delete_Claim_Adjustor")
    @RequestWrapper(localName = "Delete_Claim_Adjustor", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustor")
    @ResponseWrapper(localName = "Delete_Claim_AdjustorResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.DeleteClaimAdjustorResponse")
    public void deleteClaimAdjustor(
        @WebParam(name = "AdjustorID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer adjustorID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteClaimAdjustorAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdjustorConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdjustorInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteClaimAdjustorMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._account._claim.ArrayOfInsuredContact
     * @throws EpicSDKCore202101GetClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimInsuredContactAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimInsuredContactMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_InsuredContact")
    @WebResult(name = "Get_Claim_InsuredContactResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContact")
    @ResponseWrapper(localName = "Get_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimInsuredContactResponse")
    public ArrayOfInsuredContact getClaimInsuredContact(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param insuredContactObject
     * @throws EpicSDKCore202101UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_InsuredContact", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_InsuredContact")
    @RequestWrapper(localName = "Update_Claim_InsuredContact", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContact")
    @ResponseWrapper(localName = "Update_Claim_InsuredContactResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimInsuredContactResponse")
    public void updateClaimInsuredContact(
        @WebParam(name = "InsuredContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        InsuredContact insuredContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimInsuredContactAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimInsuredContactConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimInsuredContactInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimInsuredContactMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfLitigation
     * @throws EpicSDKCore202101GetClaimLitigationConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimLitigationMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_Litigation")
    @WebResult(name = "Get_Claim_LitigationResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigation")
    @ResponseWrapper(localName = "Get_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimLitigationResponse")
    public ArrayOfLitigation getClaimLitigation(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param litigationObject
     * @throws EpicSDKCore202101UpdateClaimLitigationInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimLitigationAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimLitigationMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimLitigationConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_Litigation", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_Litigation")
    @RequestWrapper(localName = "Update_Claim_Litigation", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigation")
    @ResponseWrapper(localName = "Update_Claim_LitigationResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimLitigationResponse")
    public void updateClaimLitigation(
        @WebParam(name = "LitigationObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Litigation litigationObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimLitigationAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimLitigationConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimLitigationInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimLitigationMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._account._claim.ArrayOfPaymentExpense
     * @throws EpicSDKCore202101GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_PaymentExpense")
    @WebResult(name = "Get_Claim_PaymentExpenseResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpense")
    @ResponseWrapper(localName = "Get_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimPaymentExpenseResponse")
    public ArrayOfPaymentExpense getClaimPaymentExpense(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param paymentExpenseObject
     * @throws EpicSDKCore202101UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_PaymentExpense", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_PaymentExpense")
    @RequestWrapper(localName = "Update_Claim_PaymentExpense", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpense")
    @ResponseWrapper(localName = "Update_Claim_PaymentExpenseResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimPaymentExpenseResponse")
    public void updateClaimPaymentExpense(
        @WebParam(name = "PaymentExpenseObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        PaymentExpense paymentExpenseObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimPaymentExpenseAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimPaymentExpenseConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimPaymentExpenseInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimPaymentExpenseMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param claimID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._account._claim.ArrayOfServicingContacts
     * @throws EpicSDKCore202101GetClaimServicingContactsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetClaimServicingContactsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Get_Claim_ServicingContacts")
    @WebResult(name = "Get_Claim_ServicingContactsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContacts")
    @ResponseWrapper(localName = "Get_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetClaimServicingContactsResponse")
    public ArrayOfServicingContacts getClaimServicingContacts(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param servicingContactObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_Claim_ServicingContacts", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Update_Claim_ServicingContacts")
    @RequestWrapper(localName = "Update_Claim_ServicingContacts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContacts")
    @ResponseWrapper(localName = "Update_Claim_ServicingContactsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateClaimServicingContactsResponse")
    public void updateClaimServicingContacts(
        @WebParam(name = "ServicingContactObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ServicingContacts servicingContactObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateClaimServicingContactsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimServicingContactsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimServicingContactsInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateClaimServicingContactsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param closedDate
     * @param claimID
     * @throws EpicSDKCore202101ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_Claim_CloseReopenClaim", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IClaim_2017_02/Action_Claim_CloseReopenClaim")
    @RequestWrapper(localName = "Action_Claim_CloseReopenClaim", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaim")
    @ResponseWrapper(localName = "Action_Claim_CloseReopenClaimResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.ActionClaimCloseReopenClaimResponse")
    public void actionClaimCloseReopenClaim(
        @WebParam(name = "ClaimID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer claimID,
        @WebParam(name = "ClosedDate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        XMLGregorianCalendar closedDate,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionClaimCloseReopenClaimAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionClaimCloseReopenClaimConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionClaimCloseReopenClaimInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionClaimCloseReopenClaimMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param searchTerms
     * @param lookupTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup
     * @throws EpicSDKCore202101GetLookupInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLookupAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLookupConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetLookupMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Lookup", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/ILookup/Get_Lookup")
    @WebResult(name = "Get_LookupResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Lookup", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookup")
    @ResponseWrapper(localName = "Get_LookupResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetLookupResponse")
    public ArrayOfLookup getLookup(
        @WebParam(name = "LookupTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        LookupTypes lookupTypeObject,
        @WebParam(name = "SearchTerms", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        ArrayOfstring searchTerms,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetLookupMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param optionTypeObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfOptionType
     * @throws EpicSDKCore202101GetOptionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetOptionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetOptionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetOptionAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Option", action = "http://webservices.appliedsystems.com/epic/sdk/2009/07/IOption/Get_Option")
    @WebResult(name = "Get_OptionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
    @RequestWrapper(localName = "Get_Option", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOption")
    @ResponseWrapper(localName = "Get_OptionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/", className = "com.appliedsystems.webservices.epic.sdk._2009._07.GetOptionResponse")
    public ArrayOfOptionType getOption(
        @WebParam(name = "OptionTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2009/07/")
        OptionTypes optionTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetOptionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @param receiptID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_Receipt")
    @WebResult(name = "Get_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptResponse")
    public ReceiptGetResult getGeneralLedgerReceipt(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetType searchType,
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ReceiptComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Insert_GeneralLedger_Receipt")
    @WebResult(name = "Insert_GeneralLedger_ReceiptResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerReceiptResponse")
    public Integer insertGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Update_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Update_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerReceiptResponse")
    public void updateGeneralLedgerReceipt(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202101DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_Receipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Delete_GeneralLedger_Receipt")
    @RequestWrapper(localName = "Delete_GeneralLedger_Receipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceipt")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerReceiptResponse")
    public void deleteGeneralLedgerReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @param detailItemToBeInsertedObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebits", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebits")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultApplyCreditsToDebitsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultApplyCreditsToDebits(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "DetailItemToBeInsertedObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        DetailItem detailItemToBeInsertedObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultApplyCreditsToDebitsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param receiptObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.ReceiptGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments")
    @WebResult(name = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPayments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPayments")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReceiptDefaultProcessOutstandingPaymentsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsResponse")
    public ReceiptGetResult getGeneralLedgerReceiptDefaultProcessOutstandingPayments(
        @WebParam(name = "ReceiptObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Receipt receiptObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReceiptDefaultProcessOutstandingPaymentsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param receiptID
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptFinalizeReceipt", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptFinalizeReceipt")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceipt", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceipt")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptFinalizeReceiptResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptFinalizeReceiptResponse")
    public void actionGeneralLedgerReceiptFinalizeReceipt(
        @WebParam(name = "ReceiptID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer receiptID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptFinalizeReceiptMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReceiptTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IReceipt2021_01/Action_GeneralLedger_ReceiptTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReceiptTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReceiptTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerReceiptTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReceiptTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReceiptTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param journalEntryFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryFilter journalEntryFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Get_GeneralLedger_JournalEntryDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_JournalEntryDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetGeneralLedgerJournalEntryDefaultDefaultEntryResponse")
    public JournalEntryGetResult getGeneralLedgerJournalEntryDefaultDefaultEntry(
        @WebParam(name = "JournalEntryDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryDefaultEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerJournalEntryDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Insert_GeneralLedger_JournalEntry")
    @WebResult(name = "Insert_GeneralLedger_JournalEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Insert_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.InsertGeneralLedgerJournalEntryResponse")
    public ArrayOfint insertGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryObject
     * @throws EpicSDKCore202101UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_JournalEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Update_GeneralLedger_JournalEntry")
    @RequestWrapper(localName = "Update_GeneralLedger_JournalEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntry")
    @ResponseWrapper(localName = "Update_GeneralLedger_JournalEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.UpdateGeneralLedgerJournalEntryResponse")
    public void updateGeneralLedgerJournalEntry(
        @WebParam(name = "JournalEntryObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntry journalEntryObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerJournalEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerJournalEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerJournalEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerJournalEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_JournalEntryTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerJournalEntryTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param journalEntryVoidObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntryVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntryVoid")
    @WebResult(name = "Action_GeneralLedger_JournalEntryVoidResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntryVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntryVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntryVoidResponse")
    public Integer actionGeneralLedgerJournalEntryVoid(
        @WebParam(name = "JournalEntryVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        JournalEntryVoid journalEntryVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntryVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param journalEntryID
     * @param messageHeader
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_JournalEntrySubmit", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IJournalEntry_2016_01/Action_GeneralLedger_JournalEntrySubmit")
    @RequestWrapper(localName = "Action_GeneralLedger_JournalEntrySubmit", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmit")
    @ResponseWrapper(localName = "Action_GeneralLedger_JournalEntrySubmitResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.ActionGeneralLedgerJournalEntrySubmitResponse")
    public void actionGeneralLedgerJournalEntrySubmit(
        @WebParam(name = "JournalEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer journalEntryID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerJournalEntrySubmitMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_DisbursementTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerDisbursementTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementPayVouchers")
    @WebResult(name = "Action_GeneralLedger_DisbursementPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerDisbursementPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerDisbursementDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementVoidObject
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_DisbursementVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Action_GeneralLedger_DisbursementVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_DisbursementVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_DisbursementVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerDisbursementVoidResponse")
    public void actionGeneralLedgerDisbursementVoid(
        @WebParam(name = "DisbursementVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementVoid disbursementVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerDisbursementVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerDisbursementVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param disbursementID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_Disbursement")
    @WebResult(name = "Get_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementResponse")
    public DisbursementGetResult getGeneralLedgerDisbursement(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetType searchType,
        @WebParam(name = "DisbursementID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        DisbursementComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Insert_GeneralLedger_Disbursement")
    @WebResult(name = "Insert_GeneralLedger_DisbursementResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Insert_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerDisbursementResponse")
    public Integer insertGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param disbursementObject
     * @throws EpicSDKCore202101UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Disbursement", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Update_GeneralLedger_Disbursement")
    @RequestWrapper(localName = "Update_GeneralLedger_Disbursement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursement")
    @ResponseWrapper(localName = "Update_GeneralLedger_DisbursementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerDisbursementResponse")
    public void updateGeneralLedgerDisbursement(
        @WebParam(name = "DisbursementObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Disbursement disbursementObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerDisbursementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerDisbursementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerDisbursementInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerDisbursementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param disbursementBankAccountNumberCode
     * @param messageHeader
     * @param disbursementBankSubAccountNumberCode
     * @param disbursementDefaultEntryID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DisbursementGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IDisbursement/Get_GeneralLedger_DisbursementDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_DisbursementDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerDisbursementDefaultDefaultEntryResponse")
    public DisbursementGetResult getGeneralLedgerDisbursementDefaultDefaultEntry(
        @WebParam(name = "DisbursementDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer disbursementDefaultEntryID,
        @WebParam(name = "DisbursementBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankAccountNumberCode,
        @WebParam(name = "DisbursementBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String disbursementBankSubAccountNumberCode,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerDisbursementDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_VoucherTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerVoucherTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherPayVouchers")
    @WebResult(name = "Action_GeneralLedger_VoucherPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchers")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherPayVouchersResponse")
    public ArrayOfint actionGeneralLedgerVoucherPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param payVouchersObject
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._generalledger._action.ArrayOfPayVouchers
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultActionPayVouchers")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchers", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchers")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultActionPayVouchersResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultActionPayVouchersResponse")
    public ArrayOfPayVouchers getGeneralLedgerVoucherDefaultActionPayVouchers(
        @WebParam(name = "PayVouchersObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        PayVouchers payVouchersObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultActionPayVouchersMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherVoidObject
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_VoucherVoid", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Action_GeneralLedger_VoucherVoid")
    @RequestWrapper(localName = "Action_GeneralLedger_VoucherVoid", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoid")
    @ResponseWrapper(localName = "Action_GeneralLedger_VoucherVoidResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.ActionGeneralLedgerVoucherVoidResponse")
    public void actionGeneralLedgerVoucherVoid(
        @WebParam(name = "VoucherVoidObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherVoid voucherVoidObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerVoucherVoidAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherVoidConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherVoidInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerVoucherVoidMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Insert_GeneralLedger_Voucher")
    @WebResult(name = "Insert_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Insert_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.InsertGeneralLedgerVoucherResponse")
    public Integer insertGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param voucherObject
     * @throws EpicSDKCore202101UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Update_GeneralLedger_Voucher")
    @RequestWrapper(localName = "Update_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Update_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.UpdateGeneralLedgerVoucherResponse")
    public void updateGeneralLedgerVoucher(
        @WebParam(name = "VoucherObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Voucher voucherObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param voucherID
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Voucher", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_Voucher")
    @WebResult(name = "Get_GeneralLedger_VoucherResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_Voucher", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucher")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherResponse")
    public VoucherGetResult getGeneralLedgerVoucher(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetType searchType,
        @WebParam(name = "VoucherID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherID,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        VoucherComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerVoucherAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param voucherDefaultEntryID
     * @param voucherAccountingMonth
     * @param messageHeader
     * @param voucherBankSubAccountNumberCode
     * @param voucherBankAccountNumberCode
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.VoucherGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", action = "http://webservices.appliedsystems.com/epic/sdk/2011/01/IVoucher/Get_GeneralLedger_VoucherDefaultDefaultEntry")
    @WebResult(name = "Get_GeneralLedger_VoucherDefaultDefaultEntryResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntry", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntry")
    @ResponseWrapper(localName = "Get_GeneralLedger_VoucherDefaultDefaultEntryResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/", className = "com.appliedsystems.webservices.epic.sdk._2011._01.GetGeneralLedgerVoucherDefaultDefaultEntryResponse")
    public VoucherGetResult getGeneralLedgerVoucherDefaultDefaultEntry(
        @WebParam(name = "VoucherDefaultEntryID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        Integer voucherDefaultEntryID,
        @WebParam(name = "VoucherBankAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankAccountNumberCode,
        @WebParam(name = "VoucherBankSubAccountNumberCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherBankSubAccountNumberCode,
        @WebParam(name = "VoucherAccountingMonth", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/01/")
        String voucherAccountingMonth,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerVoucherDefaultDefaultEntryMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param filterField1
     * @param comparisonType
     * @param getLimitType
     * @param filterField2
     * @param id
     * @param filterType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get.ActivityGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Get_GeneralLedger_Activity")
    @WebResult(name = "Get_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Get_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivity")
    @ResponseWrapper(localName = "Get_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.GetGeneralLedgerActivityResponse")
    public ActivityGetResult getGeneralLedgerActivity(
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetType searchType,
        @WebParam(name = "ID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer id,
        @WebParam(name = "GetLimitType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetLimitType getLimitType,
        @WebParam(name = "FilterType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityGetFilterType filterType,
        @WebParam(name = "FilterField1", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField1,
        @WebParam(name = "ComparisonType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        ActivityComparisonType comparisonType,
        @WebParam(name = "FilterField2", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        String filterField2,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Insert_GeneralLedger_Activity")
    @WebResult(name = "Insert_GeneralLedger_ActivityResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
    @RequestWrapper(localName = "Insert_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivity")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.InsertGeneralLedgerActivityResponse")
    public Integer insertGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param activityObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_Activity", action = "http://webservices.appliedsystems.com/epic/sdk/2017/02/IGeneralLedgerActivity_2017_02/Update_GeneralLedger_Activity")
    @RequestWrapper(localName = "Update_GeneralLedger_Activity", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivity")
    @ResponseWrapper(localName = "Update_GeneralLedger_ActivityResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/", className = "com.appliedsystems.webservices.epic.sdk._2017._02.UpdateGeneralLedgerActivityResponse")
    public void updateGeneralLedgerActivity(
        @WebParam(name = "ActivityObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2017/02/")
        Activity activityObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerActivityMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param directBillCommissionFilterObject
     * @param pageNumber
     * @param messageHeader
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDirectBillCommissionResponse")
    public DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "DirectBillCommissionFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommissionFilter directBillCommissionFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationDirectBillCommission")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationDirectBillCommissionResponse")
    public ArrayOfint insertGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionObject
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void updateGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        DirectBillCommission reconciliationDirectBillCommissionObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationDirectBillCommissionID
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationDirectBillCommission")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommission", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommission")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationDirectBillCommissionResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationDirectBillCommissionResponse")
    public void deleteGeneralLedgerReconciliationDirectBillCommission(
        @WebParam(name = "ReconciliationDirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationDirectBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationDirectBillCommissionMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param lDepartments
     * @param lEntityLookupCodes
     * @param messageHeader
     * @param lBranches
     * @param lProfitCenters
     * @param lAgencies
     * @param sCompareByType
     * @param lIssuingCompanies
     * @param sClientOrPolicyNumber
     * @param fIncludeHistory
     * @param lLineOfBusiness
     * @param sEntityType
     * @param sDescriptionType
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2017._02._generalledger._reconciliation._directbillcommission.RecordDetailItems
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies")
    @WebResult(name = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePolicies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationDefaultDirectBillRecordAvailablePoliciesResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesResponse")
    public RecordDetailItems getGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePolicies(
        @WebParam(name = "sEntityType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sEntityType,
        @WebParam(name = "lEntityLookupCodes", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lEntityLookupCodes,
        @WebParam(name = "sDescriptionType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sDescriptionType,
        @WebParam(name = "sCompareByType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sCompareByType,
        @WebParam(name = "sClientOrPolicyNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        String sClientOrPolicyNumber,
        @WebParam(name = "lAgencies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lAgencies,
        @WebParam(name = "lBranches", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lBranches,
        @WebParam(name = "lDepartments", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lDepartments,
        @WebParam(name = "lProfitCenters", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lProfitCenters,
        @WebParam(name = "lIssuingCompanies", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lIssuingCompanies,
        @WebParam(name = "lLineOfBusiness", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        ArrayOfstring lLineOfBusiness,
        @WebParam(name = "fIncludeHistory", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Boolean fIncludeHistory,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationDefaultDirectBillRecordAvailablePoliciesMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPayment", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionCloseWithoutPaymentResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPayment(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionCloseWithoutPaymentMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param directBillCommissionID
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationDirectBillCommissionFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatement(
        @WebParam(name = "DirectBillCommissionID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer directBillCommissionID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationDirectBillCommissionFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param transferOfFundsObject
     * @return
     *     returns com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankTransferOfFunds")
    @WebResult(name = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFunds", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFunds")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankTransferOfFundsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankTransferOfFundsResponse")
    public ArrayOfint actionGeneralLedgerReconciliationBankTransferOfFunds(
        @WebParam(name = "TransferOfFundsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        TransferOfFunds transferOfFundsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankTransferOfFundsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankReopenStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankReopenStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankReopenStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankReopenStatementResponse")
    public void actionGeneralLedgerReconciliationBankReopenStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankReopenStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param bankReconciliationID
     * @param messageHeader
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Action_GeneralLedger_ReconciliationBankFinalizeStatement")
    @RequestWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatement", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatement")
    @ResponseWrapper(localName = "Action_GeneralLedger_ReconciliationBankFinalizeStatementResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.ActionGeneralLedgerReconciliationBankFinalizeStatementResponse")
    public void actionGeneralLedgerReconciliationBankFinalizeStatement(
        @WebParam(name = "BankReconciliationID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer bankReconciliationID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerReconciliationBankFinalizeStatementMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param bankFilterObject
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get._generalledger.BankGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Get_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Get_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Get_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.GetGeneralLedgerReconciliationBankResponse")
    public BankGetResult getGeneralLedgerReconciliationBank(
        @WebParam(name = "BankFilterObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        BankFilter bankFilterObject,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer pageNumber,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Insert_GeneralLedger_ReconciliationBank")
    @WebResult(name = "Insert_GeneralLedger_ReconciliationBankResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.InsertGeneralLedgerReconciliationBankResponse")
    public Integer insertGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param reconciliationBankObject
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Update_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Update_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Update_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.UpdateGeneralLedgerReconciliationBankResponse")
    public void updateGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Bank reconciliationBankObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param reconciliationBankID
     * @param messageHeader
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ReconciliationBank", action = "http://webservices.appliedsystems.com/epic/sdk/2019/01/IReconciliation_2019_01/Delete_GeneralLedger_ReconciliationBank")
    @RequestWrapper(localName = "Delete_GeneralLedger_ReconciliationBank", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBank")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ReconciliationBankResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/", className = "com.appliedsystems.webservices.epic.sdk._2019._01.DeleteGeneralLedgerReconciliationBankResponse")
    public void deleteGeneralLedgerReconciliationBank(
        @WebParam(name = "ReconciliationBankID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2019/01/")
        Integer reconciliationBankID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerReconciliationBankAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationBankConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationBankInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerReconciliationBankMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._policy.ArrayOfPolicyLineType
     * @throws EpicSDKCore202101GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Get_Policy_PolicyLineType")
    @WebResult(name = "Get_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Get_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineType")
    @ResponseWrapper(localName = "Get_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.GetPolicyPolicyLineTypeResponse")
    public ArrayOfPolicyLineType getPolicyPolicyLineType(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineTypeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param policyLineTypeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyLineType", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyLineType")
    @WebResult(name = "Insert_Policy_PolicyLineTypeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyLineType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineType")
    @ResponseWrapper(localName = "Insert_Policy_PolicyLineTypeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyLineTypeResponse")
    public Integer insertPolicyPolicyLineType(
        @WebParam(name = "PolicyLineTypeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyLineType policyLineTypeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertPolicyPolicyLineTypeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyLineTypeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyLineTypeInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyLineTypeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param policyStatusObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Policy_PolicyStatus", action = "http://webservices.appliedsystems.com/epic/sdk/2018/01/IConfigurePolicy/Insert_Policy_PolicyStatus")
    @WebResult(name = "Insert_Policy_PolicyStatusResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
    @RequestWrapper(localName = "Insert_Policy_PolicyStatus", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatus")
    @ResponseWrapper(localName = "Insert_Policy_PolicyStatusResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/", className = "com.appliedsystems.webservices.epic.sdk._2018._01.InsertPolicyPolicyStatusResponse")
    public Integer insertPolicyPolicyStatus(
        @WebParam(name = "PolicyStatusObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2018/01/")
        PolicyStatus policyStatusObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertPolicyPolicyStatusAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyStatusConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyStatusInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertPolicyPolicyStatusMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._transaction.ArrayOfTransactionCode
     * @throws EpicSDKCore202101GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Get_Transaction_TransactionCode")
    @WebResult(name = "Get_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Get_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCode")
    @ResponseWrapper(localName = "Get_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.GetTransactionTransactionCodeResponse")
    public ArrayOfTransactionCode getTransactionTransactionCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param transactionCodeObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Transaction_TransactionCode", action = "http://webservices.appliedsystems.com/epic/sdk/2011/12/IConfigureTransaction/Insert_Transaction_TransactionCode")
    @WebResult(name = "Insert_Transaction_TransactionCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
    @RequestWrapper(localName = "Insert_Transaction_TransactionCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCode")
    @ResponseWrapper(localName = "Insert_Transaction_TransactionCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/", className = "com.appliedsystems.webservices.epic.sdk._2011._12.InsertTransactionTransactionCodeResponse")
    public Integer insertTransactionTransactionCode(
        @WebParam(name = "TransactionCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2011/12/")
        TransactionCode transactionCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertTransactionTransactionCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionTransactionCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionTransactionCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertTransactionTransactionCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param pageSize
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2016._01._get.SalesTeamGetResult
     * @throws EpicSDKCore202101GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_SalesTeams", action = "http://webservices.appliedsystems.com/epic/sdk/2016/01/IConfigureSalesTeam/Get_Configure_SalesTeams")
    @WebResult(name = "Get_Configure_SalesTeamsResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
    @RequestWrapper(localName = "Get_Configure_SalesTeams", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeams")
    @ResponseWrapper(localName = "Get_Configure_SalesTeamsResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/", className = "com.appliedsystems.webservices.epic.sdk._2016._01.GetConfigureSalesTeamsResponse")
    public SalesTeamGetResult getConfigureSalesTeams(
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2016/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetConfigureSalesTeamsAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetConfigureSalesTeamsConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetConfigureSalesTeamsInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetConfigureSalesTeamsMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Insert_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Insert_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerChartOfAccountResponse")
    public Integer insertGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._get.ChartOfAccountGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountResponse")
    public ChartOfAccountGetResult getGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccountGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean includeInactive,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param chartOfAccountObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Update_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Update_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerChartOfAccountResponse")
    public void updateGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        ChartOfAccount chartOfAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @throws EpicSDKCore202101DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_ChartOfAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_ChartOfAccount")
    @RequestWrapper(localName = "Delete_GeneralLedger_ChartOfAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccount")
    @ResponseWrapper(localName = "Delete_GeneralLedger_ChartOfAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerChartOfAccountResponse")
    public void deleteGeneralLedgerChartOfAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerChartOfAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerChartOfAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerChartOfAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerChartOfAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationMethodID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationMethodsGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationMethod")
    @WebResult(name = "Get_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationMethodResponse")
    public AllocationMethodsGetResult getGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationEntriesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationMethod")
    @WebResult(name = "Insert_GeneralLedger_AllocationMethodResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationMethodResponse")
    public Integer insertGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationMethodObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationMethodResponse")
    public void updateGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationMethod allocationMethodObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationMethodID
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationMethod", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationMethod")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationMethod", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethod")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationMethodResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationMethodResponse")
    public void deleteGeneralLedgerAllocationMethod(
        @WebParam(name = "AllocationMethodID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationMethodID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerAllocationMethodAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationMethodConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationMethodInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationMethodMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param allocationStructureGroupingsID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.AllocationStructureGroupingsGetResult
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Get_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Get_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerAllocationStructureGroupingResponse")
    public AllocationStructureGroupingsGetResult getGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGroupingsGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Insert_GeneralLedger_AllocationStructureGrouping")
    @WebResult(name = "Insert_GeneralLedger_AllocationStructureGroupingResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Insert_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Insert_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.InsertGeneralLedgerAllocationStructureGroupingResponse")
    public Integer insertGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param allocationStructureGroupingsObject
     * @param messageHeader
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Update_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Update_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Update_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Update_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.UpdateGeneralLedgerAllocationStructureGroupingResponse")
    public void updateGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        AllocationStructureGrouping allocationStructureGroupingsObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202101UpdateGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param allocationStructureGroupingsID
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Delete_GeneralLedger_AllocationStructureGrouping", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Delete_GeneralLedger_AllocationStructureGrouping")
    @RequestWrapper(localName = "Delete_GeneralLedger_AllocationStructureGrouping", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGrouping")
    @ResponseWrapper(localName = "Delete_GeneralLedger_AllocationStructureGroupingResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.DeleteGeneralLedgerAllocationStructureGroupingResponse")
    public void deleteGeneralLedgerAllocationStructureGrouping(
        @WebParam(name = "AllocationStructureGroupingsID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer allocationStructureGroupingsID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingAuthenticationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingConcurrencyFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingInputValidationFaultFaultFaultMessage, EpicSDKCore202101DeleteGeneralLedgerAllocationStructureGroupingMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param chartOfAccountID
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Get_GeneralLedger_ChartOfAccountDefineBankAccount")
    @WebResult(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param bankAccountObject
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountDefineBankAccount")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccount", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccount")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountDefineBankAccountResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountDefineBankAccountResponse")
    public void actionGeneralLedgerChartOfAccountDefineBankAccount(
        @WebParam(name = "BankAccountObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        BankAccount bankAccountObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountDefineBankAccountMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param ignoreNonZeroBalance
     * @param chartOfAccountID
     * @param shouldInactivateReactivateSubaccounts
     * @param ignoreSubAccountNonZeroBalances
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureGeneralLedger/Action_GeneralLedger_ChartOfAccountInactivateReactivate")
    @RequestWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivate")
    @ResponseWrapper(localName = "Action_GeneralLedger_ChartOfAccountInactivateReactivateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.ActionGeneralLedgerChartOfAccountInactivateReactivateResponse")
    public void actionGeneralLedgerChartOfAccountInactivateReactivate(
        @WebParam(name = "ChartOfAccountID", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer chartOfAccountID,
        @WebParam(name = "IgnoreNonZeroBalance", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreNonZeroBalance,
        @WebParam(name = "ShouldInactivateReactivateSubaccounts", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean shouldInactivateReactivateSubaccounts,
        @WebParam(name = "IgnoreSubAccountNonZeroBalances", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Boolean ignoreSubAccountNonZeroBalances,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateInputValidationFaultFaultFaultMessage, EpicSDKCore202101ActionGeneralLedgerChartOfAccountInactivateReactivateMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param includeInactive
     * @param messageHeader
     * @param searchType
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2011._12._configure._activity.ArrayOfActivityCode
     * @throws EpicSDKCore202101GetActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetActivityActivityCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Get_Activity_ActivityCode")
    @WebResult(name = "Get_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Get_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCode")
    @ResponseWrapper(localName = "Get_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.GetActivityActivityCodeResponse")
    public ArrayOfActivityCode getActivityActivityCode(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCodeGetType searchType,
        @WebParam(name = "IncludeInactive", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        Boolean includeInactive,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param messageHeader
     * @param activityCodeObject
     * @return
     *     returns java.lang.Integer
     * @throws EpicSDKCore202101InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityActivityCodeInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Insert_Activity_ActivityCode", action = "http://webservices.appliedsystems.com/epic/sdk/2020/01/IConfigureActivity/Insert_Activity_ActivityCode")
    @WebResult(name = "Insert_Activity_ActivityCodeResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
    @RequestWrapper(localName = "Insert_Activity_ActivityCode", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCode")
    @ResponseWrapper(localName = "Insert_Activity_ActivityCodeResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/", className = "com.appliedsystems.webservices.epic.sdk._2020._01.InsertActivityActivityCodeResponse")
    public Integer insertActivityActivityCode(
        @WebParam(name = "ActivityCodeObject", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2020/01/")
        ActivityCode activityCodeObject,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101InsertActivityActivityCodeAuthenticationFaultFaultFaultMessage, EpicSDKCore202101InsertActivityActivityCodeConcurrencyFaultFaultFaultMessage, EpicSDKCore202101InsertActivityActivityCodeInputValidationFaultFaultFaultMessage, EpicSDKCore202101InsertActivityActivityCodeMethodCallFaultFaultFaultMessage
    ;

    /**
     * 
     * @param pageNumber
     * @param messageHeader
     * @param searchType
     * @param pageSize
     * @param queryValue
     * @return
     *     returns com.appliedsystems.schemas.epic.sdk._2021._01._get.TaxFeeRateGetResult
     * @throws EpicSDKCore202101GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage
     * @throws EpicSDKCore202101GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage
     */
    @WebMethod(operationName = "Get_Configure_GovernmentTaxFeeRate", action = "http://webservices.appliedsystems.com/epic/sdk/2021/01/IConfigureTaxFeeRate/Get_Configure_GovernmentTaxFeeRate")
    @WebResult(name = "Get_Configure_GovernmentTaxFeeRateResult", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
    @RequestWrapper(localName = "Get_Configure_GovernmentTaxFeeRate", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRate")
    @ResponseWrapper(localName = "Get_Configure_GovernmentTaxFeeRateResponse", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/", className = "com.appliedsystems.webservices.epic.sdk._2021._01.GetConfigureGovernmentTaxFeeRateResponse")
    public TaxFeeRateGetResult getConfigureGovernmentTaxFeeRate(
        @WebParam(name = "QueryValue", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        String queryValue,
        @WebParam(name = "SearchType", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        TaxFeeRatesGetType searchType,
        @WebParam(name = "PageNumber", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageNumber,
        @WebParam(name = "PageSize", targetNamespace = "http://webservices.appliedsystems.com/epic/sdk/2021/01/")
        Integer pageSize,
        @WebParam(name = "MessageHeader", targetNamespace = "http://schemas.appliedsystems.com/epic/sdk/2009/07/", header = true, partName = "MessageHeader")
        MessageHeader messageHeader)
        throws EpicSDKCore202101GetConfigureGovernmentTaxFeeRateAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetConfigureGovernmentTaxFeeRateConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetConfigureGovernmentTaxFeeRateInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetConfigureGovernmentTaxFeeRateMethodCallFaultFaultFaultMessage
    ;

}
