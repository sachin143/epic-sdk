
package com.appliedsystems.webservices.epic.sdk._2022._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2022._02._account._transaction._action.ModifyRevenueDeferralSchedule;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModifyRevenueDeferralScheduleObject" type="{http://schemas.appliedsystems.com/epic/sdk/2022/02/_account/_transaction/_action/}ModifyRevenueDeferralSchedule" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modifyRevenueDeferralScheduleObject"
})
@XmlRootElement(name = "Action_Transaction_ModifyRevenueDeferralSchedule")
public class ActionTransactionModifyRevenueDeferralSchedule {

    @XmlElement(name = "ModifyRevenueDeferralScheduleObject", nillable = true)
    protected ModifyRevenueDeferralSchedule modifyRevenueDeferralScheduleObject;

    /**
     * Gets the value of the modifyRevenueDeferralScheduleObject property.
     * 
     * @return
     *     possible object is
     *     {@link ModifyRevenueDeferralSchedule }
     *     
     */
    public ModifyRevenueDeferralSchedule getModifyRevenueDeferralScheduleObject() {
        return modifyRevenueDeferralScheduleObject;
    }

    /**
     * Sets the value of the modifyRevenueDeferralScheduleObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModifyRevenueDeferralSchedule }
     *     
     */
    public void setModifyRevenueDeferralScheduleObject(ModifyRevenueDeferralSchedule value) {
        this.modifyRevenueDeferralScheduleObject = value;
    }

}
