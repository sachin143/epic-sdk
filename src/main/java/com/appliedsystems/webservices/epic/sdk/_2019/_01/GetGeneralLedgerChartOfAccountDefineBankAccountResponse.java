
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2019._01._configure._generalledger._chartofaccount.ArrayOfBankAccount;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ChartOfAccountDefineBankAccountResult" type="{http://schemas.appliedsystems.com/epic/sdk/2019/01/_configure/_generalledger/_chartofaccount/}ArrayOfBankAccount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerChartOfAccountDefineBankAccountResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResponse")
public class GetGeneralLedgerChartOfAccountDefineBankAccountResponse {

    @XmlElement(name = "Get_GeneralLedger_ChartOfAccountDefineBankAccountResult", nillable = true)
    protected ArrayOfBankAccount getGeneralLedgerChartOfAccountDefineBankAccountResult;

    /**
     * Gets the value of the getGeneralLedgerChartOfAccountDefineBankAccountResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBankAccount }
     *     
     */
    public ArrayOfBankAccount getGetGeneralLedgerChartOfAccountDefineBankAccountResult() {
        return getGeneralLedgerChartOfAccountDefineBankAccountResult;
    }

    /**
     * Sets the value of the getGeneralLedgerChartOfAccountDefineBankAccountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBankAccount }
     *     
     */
    public void setGetGeneralLedgerChartOfAccountDefineBankAccountResult(ArrayOfBankAccount value) {
        this.getGeneralLedgerChartOfAccountDefineBankAccountResult = value;
    }

}
