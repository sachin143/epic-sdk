
package com.appliedsystems.webservices.epic.sdk._2019._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.DirectBillCommissionGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_ReconciliationDirectBillCommissionResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/}DirectBillCommissionGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerReconciliationDirectBillCommissionResult"
})
@XmlRootElement(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResponse")
public class GetGeneralLedgerReconciliationDirectBillCommissionResponse {

    @XmlElement(name = "Get_GeneralLedger_ReconciliationDirectBillCommissionResult", nillable = true)
    protected DirectBillCommissionGetResult getGeneralLedgerReconciliationDirectBillCommissionResult;

    /**
     * Gets the value of the getGeneralLedgerReconciliationDirectBillCommissionResult property.
     * 
     * @return
     *     possible object is
     *     {@link DirectBillCommissionGetResult }
     *     
     */
    public DirectBillCommissionGetResult getGetGeneralLedgerReconciliationDirectBillCommissionResult() {
        return getGeneralLedgerReconciliationDirectBillCommissionResult;
    }

    /**
     * Sets the value of the getGeneralLedgerReconciliationDirectBillCommissionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectBillCommissionGetResult }
     *     
     */
    public void setGetGeneralLedgerReconciliationDirectBillCommissionResult(DirectBillCommissionGetResult value) {
        this.getGeneralLedgerReconciliationDirectBillCommissionResult = value;
    }

}
