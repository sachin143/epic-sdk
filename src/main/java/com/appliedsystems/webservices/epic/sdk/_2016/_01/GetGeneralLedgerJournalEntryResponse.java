
package com.appliedsystems.webservices.epic.sdk._2016._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2011._01._get._generalledger.JournalEntryGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_GeneralLedger_JournalEntryResult" type="{http://schemas.appliedsystems.com/epic/sdk/2011/01/_get/_generalledger/}JournalEntryGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGeneralLedgerJournalEntryResult"
})
@XmlRootElement(name = "Get_GeneralLedger_JournalEntryResponse")
public class GetGeneralLedgerJournalEntryResponse {

    @XmlElement(name = "Get_GeneralLedger_JournalEntryResult", nillable = true)
    protected JournalEntryGetResult getGeneralLedgerJournalEntryResult;

    /**
     * Gets the value of the getGeneralLedgerJournalEntryResult property.
     * 
     * @return
     *     possible object is
     *     {@link JournalEntryGetResult }
     *     
     */
    public JournalEntryGetResult getGetGeneralLedgerJournalEntryResult() {
        return getGeneralLedgerJournalEntryResult;
    }

    /**
     * Sets the value of the getGeneralLedgerJournalEntryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JournalEntryGetResult }
     *     
     */
    public void setGetGeneralLedgerJournalEntryResult(JournalEntryGetResult value) {
        this.getGeneralLedgerJournalEntryResult = value;
    }

}
