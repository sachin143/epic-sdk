
package com.appliedsystems.webservices.epic.sdk._2022._01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Insert_Policy_CancelPolicyRemarkResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insertPolicyCancelPolicyRemarkResult"
})
@XmlRootElement(name = "Insert_Policy_CancelPolicyRemarkResponse")
public class InsertPolicyCancelPolicyRemarkResponse {

    @XmlElement(name = "Insert_Policy_CancelPolicyRemarkResult")
    protected Integer insertPolicyCancelPolicyRemarkResult;

    /**
     * Gets the value of the insertPolicyCancelPolicyRemarkResult property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsertPolicyCancelPolicyRemarkResult() {
        return insertPolicyCancelPolicyRemarkResult;
    }

    /**
     * Sets the value of the insertPolicyCancelPolicyRemarkResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsertPolicyCancelPolicyRemarkResult(Integer value) {
        this.insertPolicyCancelPolicyRemarkResult = value;
    }

}
