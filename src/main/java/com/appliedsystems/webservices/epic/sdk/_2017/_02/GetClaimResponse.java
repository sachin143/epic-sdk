
package com.appliedsystems.webservices.epic.sdk._2017._02;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClaimSummaryGetResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Get_ClaimResult" type="{http://schemas.appliedsystems.com/epic/sdk/2009/07/_get/}ClaimSummaryGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getClaimResult"
})
@XmlRootElement(name = "Get_ClaimResponse")
public class GetClaimResponse {

    @XmlElement(name = "Get_ClaimResult", nillable = true)
    protected ClaimSummaryGetResult getClaimResult;

    /**
     * Gets the value of the getClaimResult property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimSummaryGetResult }
     *     
     */
    public ClaimSummaryGetResult getGetClaimResult() {
        return getClaimResult;
    }

    /**
     * Sets the value of the getClaimResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimSummaryGetResult }
     *     
     */
    public void setGetClaimResult(ClaimSummaryGetResult value) {
        this.getClaimResult = value;
    }

}
