package com.insuredmine;

import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;

import java.util.List;

public class ResponseBase {

    String Message;

    public void setData(String data) {
        Data = data;
    }

    public String getData() {
        return Data;
    }

    String Data;
    public int getClientId() {
        return ClientId;
    }


    public void setClientId(int clientId) {
        this.ClientId = clientId;
    }

    int ClientId;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    int Status;

    public boolean isError() {
        return IsError;
    }

    public void setError(boolean Error) {
        IsError = Error;
    }

    boolean IsError =false;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

}
