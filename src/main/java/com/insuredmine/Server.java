/*
 * Copyright 2014 Red Hat, Inc.
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  and Apache License v2.0 which accompanies this distribution.
 *
 *  The Eclipse Public License is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  The Apache License v2.0 is available at
 *  http://www.opensource.org/licenses/apache2.0.php
 *
 *  You may elect to redistribute this code under either of these licenses.
 */

package com.insuredmine;

import com.insuredmine.activity.controller.ActivityController;
import com.insuredmine.client.controller.ClientController;
import com.insuredmine.common.Common.CommonController;
import com.insuredmine.contact.controller.ContactController;
import com.insuredmine.policy.controller.PolicyController;
import io.netty.handler.codec.compression.CompressionOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.SocketAddress;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import java.util.Date;
/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class Server extends AbstractVerticle {

    @Override
    public void start() {
        System.out.println(new Date());
//        setUpInitialData();
        Vertx vertx=Vertx.vertx();

        vertx.eventBus().addInboundInterceptor(handler -> {
            handler.next();
        });
        final VertxOptions vertOptions = new VertxOptions();
        vertOptions.setMaxEventLoopExecuteTime(3000000000L);
        Router router = Router.router(vertx);
        ClientController client= new ClientController();
        ContactController contact= new ContactController();
        PolicyController policyController= new PolicyController();
        ActivityController activityController= new ActivityController();
        CommonController commonController= new CommonController();


        router.route().handler(BodyHandler.create());

        router.route().handler(LoggerHandler.create());
        router.get("/epic/api/v1/client/:clientID").handler(client::handleGetClient);
        router.get("/epic/api/v1/client/lookup/:lookupCode").handler(client::handleGetClientByLookupCode);
        router.post("/epic/api/v1/client").handler(client::handleAddClient);

        router.post("/epic/api/v1/upload").handler(activityController::handleAddAttachment);
        router.put("/epic/api/v1/client/:clientID").handler(client::handleUpdateClient);
        router.get("/epic/api/v1/contact/all/:clientID").handler(contact::handleGetClientContacts);
        router.get("/epic/api/v1/contact/:clientID").handler(contact::handleGetClientContactByID);
        router.put("/epic/api/v1/contact/:clientID").handler(contact::handleupdateClientContact);
        router.post("/epic/api/v1/contact").handler(contact::handleAddClientContact);
        router.post("/epic/api/v1/contact").handler(contact::handleAddClientContact);
        router.get("/epic/api/v1/policy/all/:clientID").handler(policyController::handleGetPolicyListByClientID);
        router.get("/epic/api/v1/policy/:policyID").handler(policyController::handleGetPolicyByPolicyID);
        router.post("/epic/api/v1/policy").handler(policyController::handleAddPolicy);
        router.put("/epic/api/v1/policy/:policyID").handler(policyController::handlePolicyUpdate);
        router.get("/epic/api/v1/lookup/:lookupTypeName").handler(activityController::handleGetLookup);
        router.get("/epic/api/v1/activity/:AccountID").handler(activityController::handleGetActivity);
        router.post("/epic/api/v1/activity").handler(activityController::handleAddActivity);
        router.get("/epic/api/v1/vehicles/:lineID").handler(commonController::getVehicles);

        router.get("/epic/api/v1/activity/attachment/:AttachmentID").handler(activityController::handleGetAttachment);
        router.get("/epic/api/v1/activity/attachment/download/:AttachmentID").handler(activityController::handleDownloadAttachment);

//        router.put("/epic/api/v1/policy/:policyID").handler(activityController::handleUpdateClient);

        SocketAddress socketAddress=SocketAddress.inetSocketAddress(Main.PORT,"localhost");
        HttpServerOptions serverOptions = new HttpServerOptions();
        serverOptions.setCompressionSupported(true);

        vertx.createHttpServer(serverOptions).requestHandler(router).listen(socketAddress).andThen(httpServerAsyncResult -> System.out.println("server started at "+httpServerAsyncResult.result().actualPort()));
    }


}