package com.insuredmine.policy.service;

import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.ArrayOfPolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyFilter;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.PolicyGetResult;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.insuredmine.EpicCoreInstance;
import com.insuredmine.Main;
import io.vertx.core.json.JsonObject;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class PolicyService {
    static final String DATABASENAME = (Main.DatabaseName);
    static final String AUTHENTICATIONKEY = (Main.AuthenticationKey);

    public static Policy getPolicyByPolicyID(int policyID) throws EpicSDKCore202202GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyAuthenticationFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        PolicyFilter clientFilter = new PolicyFilter();
        clientFilter.setPolicyID((policyID));
        PolicyGetResult clientGetResult = EpicCoreInstance.getInstance().getServiceBindingCore5().getPolicy(clientFilter, 0, messageHeader);
   return clientGetResult.getPolicies().getPolicies().get(0);
    }

    public static List<Policy> getPolicyByClientID(int clientID) throws EpicSDKCore201901GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetPolicyInputValidationFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        PolicyFilter clientFilter = new PolicyFilter();
        clientFilter.setPolicyID((clientID));
        PolicyGetResult clientGetResult = EpicCoreInstance.getInstance().getServiceBindingCore().getPolicy(clientFilter, 0, messageHeader);
        ArrayOfPolicy clientsList = clientGetResult.getPolicies();
        return clientsList.getPolicies();
    }

    public static int addPolicy(JsonObject clientRequestObject)
            throws EpicSDKCore201901InsertPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyAuthenticationFaultFaultFaultMessage {
        Policy policyInsertObject = new Policy();
        int AccountID= clientRequestObject.getInteger("AccountID",0);

        String EffectiveDate = clientRequestObject.getString("EffectiveDate", "");
        String ExpirationDate = clientRequestObject.getString("ExpirationDate", "");
        String Source= clientRequestObject.getString("Source","");
        String PolicyNumber= clientRequestObject.getString("PolicyNumber","");
        String AgencyCode= clientRequestObject.getString("AgencyCode","");
        String BranchCode= clientRequestObject.getString("BranchCode","");
        String PolicyTypeCode= clientRequestObject.getString("PolicyTypeCode","");
        String Description= clientRequestObject.getString("Description","");
        String StatusCode= clientRequestObject.getString("StatusCode","");
        String DepartmentCode= clientRequestObject.getString("DepartmentCode","");
        String LineTypeCode= clientRequestObject.getString("LineTypeCode","");
        String ProfitCenterCode= clientRequestObject.getString("ProfitCenterCode","");
        String IssuingCompanyLookupCode = clientRequestObject.getString("IssuingCompanyLookupCode", "");



        String DirectBilled= clientRequestObject.getString("DirectBilled","");
        String LocationCode= clientRequestObject.getString("LocationCode","");

        Double EstimatedPremium= clientRequestObject.getDouble("EstimatedPremium",0d);

        policyInsertObject.setAccountID(AccountID);
        XMLGregorianCalendar gregorianEffectiveDate = createGregorianDate(EffectiveDate);
        XMLGregorianCalendar gregorianExpirationDate = createGregorianDate(ExpirationDate);
        if (gregorianEffectiveDate != null) {
            policyInsertObject.setEffectiveDate(gregorianEffectiveDate);
        }
        if (gregorianExpirationDate != null) {
            policyInsertObject.setExpirationDate(gregorianExpirationDate);
        }
        policyInsertObject.setSource((Source));
        policyInsertObject.setPolicyNumber((PolicyNumber));
        policyInsertObject.setAgencyCode((AgencyCode));
        policyInsertObject.setBranchCode((BranchCode));
        policyInsertObject.setPolicyTypeCode((PolicyTypeCode));
        policyInsertObject.setDescription((Description));
        policyInsertObject.setStatusCode((StatusCode));
        policyInsertObject.setDepartmentCode((DepartmentCode));
        policyInsertObject.setLineTypeCode((LineTypeCode));
        policyInsertObject.setProfitCenterCode((ProfitCenterCode));
        policyInsertObject.setEstimatedPremium((new BigDecimal(EstimatedPremium)));
        OptionType optionType =new OptionType();
        optionType.setOptionName((DirectBilled));
        optionType.setValue(1);
        policyInsertObject.setBillingModeOption((optionType));
        policyInsertObject.setIssuingCompanyLookupCode(IssuingCompanyLookupCode);
        policyInsertObject.setIssuingLocationCode((LocationCode));
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        return EpicCoreInstance.getInstance().getServiceBindingCore().insertPolicy(policyInsertObject, messageHeader);
    }

    public static Policy updatePolicy(JsonObject clientRequestObject,int PolicyID)
            throws EpicSDKCore201901InsertPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore201901UpdatePolicyInputValidationFaultFaultFaultMessage, EpicSDKCore201901UpdatePolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdatePolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore201901UpdatePolicyMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyAuthenticationFaultFaultFaultMessage {
        int AccountID= clientRequestObject.getInteger("AccountID",0);
        Policy policyInsertObject =  getPolicyByPolicyID(PolicyID);
        String ExpirationDate = clientRequestObject.getString("ExpirationDate", "");
        String Source= clientRequestObject.getString("Source","");
        String PolicyNumber= clientRequestObject.getString("PolicyNumber","");
        String AgencyCode= clientRequestObject.getString("AgencyCode","");
        String BranchCode= clientRequestObject.getString("BranchCode","");
        String PolicyTypeCode= clientRequestObject.getString("PolicyTypeCode","");
        String Description= clientRequestObject.getString("Description","");
        String StatusCode= clientRequestObject.getString("StatusCode","");
        String DepartmentCode= clientRequestObject.getString("DepartmentCode","");
        String LineTypeCode= clientRequestObject.getString("LineTypeCode","");
        String EffectiveDate = clientRequestObject.getString("EffectiveDate", "");
        String IssuingCompanyLookupCode = clientRequestObject.getString("IssuingCompanyLookupCode", "");
        String DirectBilled= clientRequestObject.getString("DirectBilled","");
        String LocationCode= clientRequestObject.getString("LocationCode","");

        Double EstimatedPremium= clientRequestObject.getDouble("EstimatedPremium",0d);

        policyInsertObject.setAccountID(AccountID);

        XMLGregorianCalendar gregorianEffectiveDate = createGregorianDate(EffectiveDate);
        XMLGregorianCalendar gregorianExpirationDate = createGregorianDate(ExpirationDate);
        if (gregorianEffectiveDate != null) {
            policyInsertObject.setEffectiveDate(gregorianEffectiveDate);
        }
        if (gregorianExpirationDate != null) {
            policyInsertObject.setExpirationDate(gregorianExpirationDate);
        }
        policyInsertObject.setSource(Source);
        policyInsertObject.setPolicyNumber(PolicyNumber);
        policyInsertObject.setAgencyCode((AgencyCode));
        policyInsertObject.setBranchCode((BranchCode));
        policyInsertObject.setPolicyTypeCode((PolicyTypeCode));
        policyInsertObject.setDescription((Description));
        policyInsertObject.setStatusCode((StatusCode));
        policyInsertObject.setDepartmentCode((DepartmentCode));
        policyInsertObject.setLineTypeCode((LineTypeCode));
//        policyInsertObject.setProfitCenterCode(("PFC"));
        policyInsertObject.setEstimatedPremium((new BigDecimal(EstimatedPremium)));
        OptionType optionType =new OptionType();
        optionType.setOptionName((DirectBilled));
        optionType.setValue(1);
        policyInsertObject.setBillingModeOption(optionType);
//        ;setBillingModeOption((optionType));
        policyInsertObject.setIssuingCompanyLookupCode(IssuingCompanyLookupCode);
        policyInsertObject.setIssuingLocationCode((LocationCode));
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
         EpicCoreInstance.getInstance().getServiceBindingCore().updatePolicy(policyInsertObject, messageHeader);
        return policyInsertObject;
    }

    private static XMLGregorianCalendar createGregorianDate(String Date) {
        GregorianCalendar c = new GregorianCalendar();
        XMLGregorianCalendar gregorianDate;
        c.setTime(new Date(Date));
        try {
            gregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            return gregorianDate;
        } catch (DatatypeConfigurationException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


//    public static Client updateCLient(JsonObject clientRequestObject)
//            throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901UpdateClientInputValidationFaultFaultFaultMessage {
//        com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory clientObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();
//        com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory AccountObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory();
//        ObjectFactory sharedObjectFactory = new ObjectFactory();
//
//
//        boolean IsPersonal = clientRequestObject.getBoolean("IsPersonal", false);
//        String AccountName = clientRequestObject.getString("AccountName", "");
//        String PrimaryContactLast = clientRequestObject.getString("PrimaryContactLast", "");
//        String PrimaryContactEmail = clientRequestObject.getString("PrimaryContactEmail", "");
//        String PrimaryContactEmailDescription = clientRequestObject.getString("PrimaryContactEmailDescription", "");
//        String AccountEmail = clientRequestObject.getString("AccountEmail", "");
//        String EmployeeLookupCode = clientRequestObject.getString("EmployeeLookupCode", "");
//        int ClientID = clientRequestObject.getInteger("ClientID", 0);
//        String ClientLookupCode = clientRequestObject.getString("ClientLookupCode", "");
//        Client client = getClient(ClientID);
//        JsonObject addressDefaultField = createNullAddressJSONObject();
//        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
//        String City = address.getString("City");
//        String CountryCode = address.getString("CountryCode");
//        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
//        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
//        String County = address.getString("County");
//        String Street1 = address.getString("Street1");
//        String Street2 = address.getString("Street2");
//        String Street3 = address.getString("Street3");
//
//        String PrimaryContactFirst = clientRequestObject.getString("PrimaryContactFirst", "");
//        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
//        String BranchCode = clientRequestObject.getString("BranchCode", "");
//        client.setIsPersonal(IsPersonal);
//        client.setClientID(ClientID);
//        client.setAccountName(clientObjectFactory.createClientAccountName(AccountName));
//        AgencyStructureItem structureItem = createAgencyStructureItem(AgencyCode, BranchCode);
//        AgencyStructureItems structureItems = new AgencyStructureItems();
//        client.setPrimaryContactFirst(clientObjectFactory.createClientPrimaryContactFirst(PrimaryContactFirst));
//        client.setPrimaryContactLast(clientObjectFactory.createClientPrimaryContactLast(PrimaryContactLast));
//        client.setPrimaryContactEmail(clientObjectFactory.createClientPrimaryContactEmail(PrimaryContactEmail));
//        structureItems.getAgencyStructureItem().add(structureItem);
//        Account account = new Account();
//        com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory agencyStructureObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory();
//        ServicingRoleItem servicingRoleItem = agencyStructureObjectFactory.createServicingRoleItem();
//
//        client.setClientLookupCode(clientObjectFactory.createClientClientLookupCode(ClientLookupCode));
//
//        servicingRoleItem.setEmployeeLookupCode(
//                agencyStructureObjectFactory.createServicingRoleItemEmployeeLookupCode(EmployeeLookupCode));
////        servicingRoleItem.setRole( );
//        ServicingRolesItems servicingRolesItems = client.getServicingContacts().getValue();
//
//        servicingRolesItems.getServicingRoleItem().add(servicingRoleItem);
////        client.setServicingContacts(        agencyStructureObjectFactory.createServicingRolesItems(servicingRolesItems) );
//        account.setAccountEmail(AccountObjectFactory.createAccountAccountEmail(AccountEmail));
//        account.setStructure(AccountObjectFactory.createAccountStructure(structureItems));
////         new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory().createClientAccountValue(account);
//        Address accountAddress = createAddress(AccountObjectFactory, sharedObjectFactory,CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
//
//
//        client.setAccountValue(new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory().createClientAccountValue(account));
//
//        EpicCoreInstance.getInstance().getServiceBindingCore().updateClient(client, messageHeader);
//        return client;
//    }


}
