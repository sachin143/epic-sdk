package com.insuredmine.policy;

import com.appliedsystems.schemas.epic.sdk._2011._01._account.ArrayOfPolicy;
import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.insuredmine.ResponseBase;

import java.util.List;
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)

public class PolicyResponse  extends ResponseBase {
    String Message;

    public int getPolicyID() {
        return PolicyID;
    }

    public void setPolicyID(int policyID) {
        PolicyID = policyID;
    }

    public int PolicyID;

    int ClientId;


    public List<Policy> getPolicies() {
        return Policies;
    }

    public void setPolicies(List<Policy> Policies) {
        this.Policies = Policies;
    }

    List<Policy> Policies;

    public Policy getPolicy() {
        return Policy;
    }

    public void setPolicy(Policy policy) {
        this.Policy = policy;
    }

    Policy Policy;







    public PolicyResponse(ArrayOfPolicy policy, String message) {
        this.Message = message;

    }


public PolicyResponse(){

}

}
