package com.insuredmine.policy.controller;

import com.appliedsystems.schemas.epic.sdk._2011._01._account.Policy;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insuredmine.client.controller.ApiError;
import com.insuredmine.policy.PolicyResponse;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import java.util.List;

import static com.insuredmine.policy.service.PolicyService.*;

public class PolicyController {


    public void handleGetPolicyByPolicyID(RoutingContext routingContext) {
        String policyID = routingContext.request().getParam("policyID");
        HttpServerResponse response = routingContext.response();
        if (policyID == null) {
            sendError(400, response, "contact ID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Policy policy = getPolicyByPolicyID(new Integer(policyID));
                if (policy== null) {
                    sendError(404, response, "cannot find policies by ClientID.");
                } else {
                    PolicyResponse policyResponse =new PolicyResponse();
                    policyResponse.setStatus(200);
                    policyResponse.setPolicyID(Integer.parseInt(policyID));
                    policyResponse.setClientId(Integer.parseInt(policyID));
                    policyResponse.setPolicy(policy);
//                clientContactResponse
                    policyResponse.setMessage("policies list get successfully.");
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(policyResponse));
                }
            } catch (Exception e) {
                sendError(400, response, e.getMessage());
            }


        }
    }
    public void handleGetPolicyListByClientID(RoutingContext routingContext) {
        String clientID = routingContext.request().getParam("clientID");
        HttpServerResponse response = routingContext.response();
        if (clientID == null) {
            sendError(400, response, "contact ID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                List<Policy> policyList = getPolicyByClientID(new Integer(clientID));
                if (policyList== null) {
                    sendError(404, response, "cannot find policies by ClientID.");
                } else {
                    PolicyResponse policyResponse =new PolicyResponse();
                    policyResponse.setStatus(200);
                    policyResponse.setClientId(Integer.parseInt(clientID));
                    policyResponse.setPolicies(policyList);
//                clientContactResponse
                    policyResponse.setMessage("policies list get successfully.");
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(policyResponse));
                }
            } catch (Exception e) {
                sendError(400, response, e.getMessage());
            }


        }
    }

    public void handleAddPolicy(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {

            ObjectMapper objectMapper = new ObjectMapper();

            int policy = addPolicy(bodyObj);
            if (policy ==  0) {
                sendError(400, response, "cannot create policy.");
            } else {
                PolicyResponse clientContactResponse = new PolicyResponse();
                clientContactResponse.setPolicyID(policy);
                clientContactResponse.setStatus(200);
//                clientContactResponse
                clientContactResponse.setMessage("policy created successfully.");
                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
            }
        }

        catch (EpicSDKCore201901InsertPolicyMethodCallFaultFaultFaultMessage e) {
                       sendError(400, response, e.getMessage());
;
        } catch (EpicSDKCore201901InsertPolicyInputValidationFaultFaultFaultMessage e) {
                       sendError(400, response, e.getMessage());
;
        } catch (EpicSDKCore201901InsertPolicyConcurrencyFaultFaultFaultMessage e) {
                       sendError(400, response, e.getMessage());
;
        } catch (JsonProcessingException e) {
                       sendError(400, response, e.getMessage());
;
        } catch (EpicSDKCore201901InsertPolicyAuthenticationFaultFaultFaultMessage e) {
                       sendError(400, response, e.getMessage());
;
        }
        catch (Exception e) {
            sendError(500, response, e.getMessage());
        }

    }

    public void handlePolicyUpdate(RoutingContext routingContext) {
        String policyID = routingContext.request().getParam("policyID");


        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            if (policyID == null) {
                sendError(400, response, "contact ID Missing");
            }
            else {
                Policy policy = updatePolicy(bodyObj,new Integer(policyID));
                if (policy == null) {
                    sendError(400, response, "cannot update policy.");
                } else {
                    PolicyResponse clientContactResponse = new PolicyResponse();
//                    clientContactResponse.setContactID(contact.get());
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientId(policy.getAccountID());
                    clientContactResponse.setPolicy(policy);
//                clientContactResponse
                    clientContactResponse.setMessage("Policy updated successfully.");
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            }
        }  catch (Exception e) {
            sendError(400, response, e.getMessage());
            ;
        }

    }


    public static void sendError(int statusCode, HttpServerResponse response, String errorMessage) {
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError error = new ApiError(statusCode, errorMessage,true);

        try {
            response.setStatusCode(statusCode).send(objectMapper.writeValueAsString(error));
        } catch (JsonProcessingException e) {
            response.setStatusCode(statusCode).send("Internal Server Error.");
        }
    }
}

