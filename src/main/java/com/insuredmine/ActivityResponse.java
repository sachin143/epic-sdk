package com.insuredmine;

import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.insuredmine.activity.service.AttachmentData;

import java.util.List;

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)

public class ActivityResponse {

Client Client;
int ClientID;

    public int getActivityID() {
        return ActivityID;
    }

    public void setActivityID(int activityID) {
        ActivityID = activityID;
    }

    int ActivityID;

    public List<Integer> getAttachmentID() {
        return AttachmentID;
    }

    public void setAttachmentID(List<Integer> attachmentID) {
        AttachmentID = attachmentID;
    }

    private List<Integer> AttachmentID;

    public List<AttachmentData> getAttachmentList() {
        return AttachmentList;
    }

    public void setAttachmentList(List<AttachmentData> attachmentList) {
        AttachmentList = attachmentList;
    }

    private
    List<AttachmentData> AttachmentList;

    public List<Activity> getActivities() {
        return Activities;
    }

    public void setActivities(List<Activity> attachments) {
        Activities = attachments;
    }

    private List<Activity> Activities;



    public boolean isError() {
        return Error;
    }

    public void setError(boolean error) {
        Error = error;
    }

    boolean Error;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    int Status;

ContactGetResult Contacts;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    String Message;

public ActivityResponse(){
}
    public ActivityResponse(Client client, int clientID, ContactGetResult contacts) {
        this.Client = client;
        ClientID = clientID;
        this.Contacts = contacts;
    }

    public Client getClient() {
        return Client;
    }

    public void setClient(Client client) {
        this.Client = client;
    }

    public int getClientID() {
        return ClientID;
    }

    public void setClientID(int clientID) {
        ClientID = clientID;
    }

    public ContactGetResult getContacts() {
        return Contacts;
    }

    public void setContacts(ContactGetResult contacts) {
        this.Contacts = contacts;
    }
}
