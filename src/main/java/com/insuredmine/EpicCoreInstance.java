package com.insuredmine;

import com.appliedsystems.webservices.epic.sdk._2013._11.EpicSDKCore;

public class EpicCoreInstance {


    private EpicCoreInstance() {
    }

    // The single instance of the class
    private static EpicSDKCore instance;

    // Public static method to get the instance (lazy initialization)
    public static synchronized EpicSDKCore getInstance() {
        if (instance == null) {
            instance = new EpicSDKCore();
        }
        return instance;
    }

}
