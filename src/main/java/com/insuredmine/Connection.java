package com.insuredmine;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import java.util.Arrays;
import java.util.Properties;

  class Connection {
    private static MongoClient mongoClient;


    private Connection(){

    }

    public static MongoClient getConnection() {
        if (mongoClient == null) {
        return  getClient();
        }
        return mongoClient;
    }

    private static MongoClient getClient() {

        mongoClient=MongoClients.create("mongodb+srv://insuredmineProd:1S5rcaM5HEwVgMky@insuredmineprodcluster.wtld4.mongodb.net/insuredmine?retryWrites=true&w=majority");
  return  mongoClient;

    }

  }