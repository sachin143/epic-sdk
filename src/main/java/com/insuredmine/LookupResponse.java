package com.insuredmine;

import com.appliedsystems.schemas.epic.sdk._2009._07._common.Lookup;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;

import java.util.List;

public class LookupResponse extends ResponseBase{
    public List<Lookup> getLookup() {
        return lookup;
    }

    public void setLookup(List<Lookup> lookup) {
        this.lookup = lookup;
    }

    private List<Lookup> lookup;



}
