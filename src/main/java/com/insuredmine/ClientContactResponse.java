package com.insuredmine;

import com.appliedsystems.schemas.epic.sdk._2017._02._account.ArrayOfContact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)

public class ClientContactResponse {

    public int getClientId() {
        return ClientId;
    }

    public void setClientId(int clientId) {
        this.ClientId = clientId;
    }

    int ClientId;
int ContactID;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        this.Status = status;
    }

    int Status;

    public boolean isError() {
        return IsError;
    }

    public void setError(boolean error) {
        IsError = error;
    }

    boolean IsError =false;
    List<Contact> Contacts;

    public Contact getContact() {
        return Contact;
    }

    public void setContact(Contact contact) {
        this.Contact = contact;
    }

    Contact Contact;

    String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public ClientContactResponse( int contactID, List<Contact> contacts, String message) {
        ContactID = contactID;
        this.Contacts = contacts;
        this.Message = message;
    }


public ClientContactResponse(){

}



    public int getContactID() {
        return ContactID;
    }

    public void setContactID(int contactID) {
        ContactID = contactID;
    }

    public List<Contact> getContacts() {
        return Contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.Contacts = contacts;
    }
}
