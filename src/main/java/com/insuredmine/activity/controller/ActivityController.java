package com.insuredmine.activity.controller;

import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.FileDetailItem;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.Lookup;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.AttachmentFilter;
import com.appliedsystems.schemas.epic.sdk._2013._11._account.AttachmentAssociationType;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insuredmine.ActivityResponse;
import com.insuredmine.LookupResponse;
import com.insuredmine.Main;
import com.insuredmine.ResponseBase;
import com.insuredmine.activity.service.AttachmentData;
import com.insuredmine.client.controller.ApiError;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.insuredmine.activity.service.ActivityService.*;
import static com.insuredmine.client.service.ClientService.*;

public class ActivityController {
    private static final Logger clientLogger = LogManager.getLogger(Main.class);
    public void handleGetAttachment(RoutingContext routingContext) {
        clientLogger.info(routingContext.request().absoluteURI());
        JsonObject bodyObj = routingContext.body().asJsonObject();
        int AttachmentID = Integer.parseInt(routingContext.request().getParam("AttachmentID"));

        HttpServerResponse response = routingContext.response();
        if (AttachmentID ==0) {
            sendError(400, response, "AccountID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();


                AttachmentFilter attachmentFilter =new AttachmentFilter();

                attachmentFilter.setAttachmentAssociationType(AttachmentAssociationType.ACCOUNT_ID);
                attachmentFilter.setAttachmentAssociationID(AttachmentID);


                List<AttachmentData> activityList = getAttachmentByFilter(attachmentFilter);
                ActivityResponse clientContactResponse = new ActivityResponse();
                clientContactResponse.setAttachmentList(activityList);
                clientContactResponse.setMessage("Attachment Get Successful.");
                clientContactResponse.setStatus(200);
                clientContactResponse.setClientID(1);
                clientContactResponse.setContacts(null);


                byte[] demBytes = null; // Instead of null, specify your bytes here.

//                FileDetailItem attachFile=    activityList.getAttachment().getFiles().getFileDetailItems().get(0);
//                String fileName = (attachFile.getFileName() !=null && !attachFile.getFileName().isEmpty())?attachFile.getFileName() :UUID.randomUUID().toString();


//             String filepath=   createTempFile(fileName+attachFile.getExtension(), (activityList.getFileBuffer()));
//                response.sendFile(filepath);

                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));

            }  catch (Exception e) {
                clientLogger.error(e);
                sendError(500, response, e.getMessage());
            }


        }
    }
    public void handleDownloadAttachment(RoutingContext routingContext) {
        clientLogger.info(routingContext.request().absoluteURI());
        JsonObject bodyObj = routingContext.body().asJsonObject();
        int AttachmentID = Integer.parseInt(routingContext.request().getParam("AttachmentID"));

        HttpServerResponse response = routingContext.response();
        if (AttachmentID ==0) {
            sendError(400, response, "AccountID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                byte[]
                        activityList = getAttachmentBytes(AttachmentID);
                class FIleResponse extends  ResponseBase {
                    public byte[] getFile() {
                        return file;
                    }

                    public void setFile(byte[] file) {
                        this.setData(file) ;
                    }

                    byte[] file;

                    public void setData(byte[] data) {
                   file=data;
                    }
                }
                FIleResponse clientContactResponse = new FIleResponse();
                clientContactResponse.setData(activityList);
                clientContactResponse.setMessage("Attachment Get Successful.");
                clientContactResponse.setStatus(200);
                byte[] demBytes = null; // Instead of null, specify your bytes here.

//                FileDetailItem attachFile=    activityList.getAttachment().getFiles().getFileDetailItems().get(0);
//                String fileName = (attachFile.getFileName() !=null && !attachFile.getFileName().isEmpty())?attachFile.getFileName() :UUID.randomUUID().toString();


//             String filepath=   createTempFile(fileName+attachFile.getExtension(), (activityList.getFileBuffer()));
//                response.sendFile(filepath);

                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));

            }  catch (Exception e) {
                clientLogger.error(e);
                sendError(500, response, e.getMessage());
            }


        }
    }

    public void handleGetActivity(RoutingContext routingContext) {
        String productID = routingContext.request().getParam("clientID");
        clientLogger.info(routingContext.request().absoluteURI());
        JsonObject bodyObj = routingContext.body().asJsonObject();
        int AccountID = Integer.parseInt(routingContext.request().getParam("AccountID"));

        HttpServerResponse response = routingContext.response();
        if (AccountID ==0) {
            sendError(400, response, "AccountID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                List<Activity> activityList = getActivity(AccountID);
                if (activityList == null) {
                    sendError(404, response, "Activity Not Found.");
                } else {
                    ActivityResponse clientContactResponse = new ActivityResponse();
                    clientContactResponse.setActivities(activityList);
                    clientContactResponse.setMessage("Activity Get Successful.");
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientID(1);
                    clientContactResponse.setContacts(null);
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            }  catch (Exception e) {
                clientLogger.error(e);
                sendError(500, response, e.getMessage());
            }


        }
    }
    public void handleGetLookup(RoutingContext routingContext) {

        clientLogger.info(routingContext.request().absoluteURI());
        String lookupTypeName = routingContext.request().getParam("lookupTypeName");
        String query = routingContext.request().query();
        List<String> values = new ArrayList<>();
if(query!=null){

    String[] pairs = routingContext.request().query()

            .split("&");

    for (String pair : pairs) {
        String[] keyValue = pair.split("=");
        String value = null;
        try {
            value = URLDecoder.decode(keyValue[1], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        values.add(value);
    }
}

        HttpServerResponse response = routingContext.response();
        if (false) {
            sendError(400, response, "query Missing");
        } else {
                ObjectMapper objectMapper = new ObjectMapper();
            List<Lookup> lookupCode = null;
            try {
                lookupCode = getLookupCodeByID(lookupTypeName,values);
            } catch (Throwable e) {
                                    sendError(400, response, String.valueOf(e));
            }
            if (lookupCode == null) {
                    sendError(404, response, "Client Not Found.");
                } else {
                    LookupResponse clientContactResponse = new LookupResponse();
                    clientContactResponse.setLookup(lookupCode);

                    clientContactResponse.setMessage("Lookup Get Successful.");
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientId(1);
                try {
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                } catch (JsonProcessingException e) {
                    sendError(404, response, String.valueOf(e));
                }
            }


        }
    }

    public void handleAddActivity(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            int activityId = addActivity(bodyObj);
            clientLogger.info(routingContext.request().absoluteURI());
            clientLogger.info(routingContext.request());
            if (activityId == 0) {
                sendError(404, response, "cannot create activity.");
            } else {
                ActivityResponse clientUpdateResponse = new ActivityResponse();
                clientUpdateResponse.setActivityID(activityId);
                clientUpdateResponse.setStatus(200);
                clientUpdateResponse.setMessage("Activity insert successful.");
//                clientUpdateResponse.setClientID(activityId);
                clientUpdateResponse.setContacts(null);
                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientUpdateResponse));
            }
        } catch (Exception e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        }
    }


    public void handleAddAttachment(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<Integer> activityId = addAttachment(bodyObj);
            clientLogger.info(routingContext.request().absoluteURI());
            clientLogger.info(routingContext.request());
            if (activityId.isEmpty()) {
                sendError(404, response, "cannot create activity.");
            } else {
                ActivityResponse clientUpdateResponse = new ActivityResponse();
//                clientUpdateResponse.setActivityID();
                clientUpdateResponse.setStatus(200);
                clientUpdateResponse.setMessage("Attachment insert successful.");
                clientUpdateResponse.setAttachmentID(activityId);
                clientUpdateResponse.setContacts(null);
                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientUpdateResponse));
            }
        } catch (Exception e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        }
    }

    public void sendError(int statusCode, HttpServerResponse response, String errorMessage) {
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError error = new ApiError(statusCode, errorMessage, true);

        try {
            response.setStatusCode(statusCode).send(objectMapper.writeValueAsString(new ApiError(statusCode, errorMessage, true)));
        } catch (JsonProcessingException e) {
            response.setStatusCode(statusCode).send("Internal Server Error.");
        }
    }
    String createTempFile(String fileName, byte[] content) throws IOException {
        String dir = System.getProperty("java.io.tmpdir");
        File file = new File(dir + fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file.getPath();
    }
}

