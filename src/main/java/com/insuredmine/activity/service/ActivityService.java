package com.insuredmine.activity.service;

import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;

import com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.*;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.ArrayOfLookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.Lookup;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._common._lookup.LookupTypes;
import com.appliedsystems.schemas.epic.sdk._2011._01._common.Activity;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.CloseDetail;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.Detail;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.TaskItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.TaskItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.NoteItem;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common.NoteItems;
import com.appliedsystems.schemas.epic.sdk._2011._01._common._activity._common._noteitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2011._01._get.*;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.insuredmine.EpicCoreInstance;
import com.insuredmine.Main;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

public class ActivityService {
    static final com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory activityObjectFactory = new com.appliedsystems.schemas.epic.sdk._2011._01._common.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory
            activityObjectFactory1 = new com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory
            attachmentObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._attachment.ObjectFactory();

    static final com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory commonObjectFactory = new com.appliedsystems.schemas.epic.sdk._2011._01._common._activity.ObjectFactory();
    static final String DATABASENAME = Main.DatabaseName;
    static final String AUTHENTICATIONKEY = Main.AuthenticationKey;
    static final com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory policyObjectFactory = new com.appliedsystems.schemas.epic.sdk._2011._01._get.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory objectFactory =new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();

    public static List<Activity> getActivity(int AccountID) throws EpicSDKCore202202GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetLookupMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetLookupMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetOptionAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetOptionConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetOptionMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetOptionInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetActivityMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetActivityInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetActivityConcurrencyFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        ActivityFilter activityFilter = new ActivityFilter();
//        int AccountID = activityRequestObject.getInteger("AccountID", 0);
        activityFilter.setAccountTypeCode(("CUST"));
//activityFilter.setActivityID((65536));


        activityFilter.setAccountID((AccountID));
//        String AgencyCode = activityRequestObject.getString("AgencyCode", "");
//        String BranchCode = activityRequestObject.getString("BranchCode", "");
//        String DepartmentCode = activityRequestObject.getString("DepartmentCode", "");
        List<Activity> getResult= EpicCoreInstance.getInstance().getServiceBindingCore5().getActivity(activityFilter,0,messageHeader ).getActivities().getActivities();
        return  getResult;    }


    public static   byte[] getAttachmentBytes(int AttachmentID )   {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        byte[] ticketName;
        try {
            ticketName = EpicCoreInstance.getInstance().getFileTransferServiceBindingCore().downloadAttachmentFile(AttachmentID, messageHeader);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return ticketName;
    }


    public static List<AttachmentData> getAttachmentByFilter(AttachmentFilter attachmentFilter ) throws EpicSDKCore201901GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileMethodCallFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileInputValidationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferDownloadAttachmentFileConcurrencyFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        AttachmentSorting attachmentSorting = new AttachmentSorting();
//        attachmentFilter.setAttachmentID((attachmentID));


        AttachmentGetResult clientGetResult = EpicCoreInstance.getInstance().
                getServiceBindingCore()
                .getAttachment(attachmentFilter, attachmentSorting,0,0, messageHeader);


        List<AttachmentData> attachmentList= new ArrayList<AttachmentData>();

        clientGetResult.getAttachments().getAttachments().forEach(attachment -> {

            AttachmentData attachmentData= new AttachmentData();
            attachmentData.setAttachment(attachment);
//            byte[] ticketName;
//            try {
//                ticketName = EpicCoreInstance.getInstance().getFileTransferServiceBindingCore().downloadAttachmentFile(attachment.getAttachmentID(), messageHeader);
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//            attachmentData.setFileBuffer(ticketName);
            attachmentList.add(attachmentData);
        });

        return attachmentList;
    }
    public static List<Lookup> getLookupCodeByID(String lookupTypeName,List<String> searchParam) throws EpicSDKCore201901GetPolicyAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetPolicyConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetPolicyMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetPolicyInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetLookupInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetLookupConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetLookupAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetLookupMethodCallFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        LookupTypes lookupTypes =LookupTypes.fromValue(lookupTypeName);

        List<String> list=new ArrayList<String>();
        ArrayOfstring arrayOfstring= new ArrayOfstring();
        arrayOfstring.getString().addAll(searchParam);

        ArrayOfLookup clientGetResult = EpicCoreInstance.getInstance().getServiceBindingCore().getLookup(lookupTypes, arrayOfstring, messageHeader);
       List<Lookup>  lookupList = clientGetResult.getLookups();
        return lookupList;
    }

    public static int addActivity(JsonObject clientRequestObject)
            throws EpicSDKCore201901InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertActivityMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertActivityAuthenticationFaultFaultFaultMessage {
        Activity activityInsertObject = new Activity();
        int AccountID = clientRequestObject.getInteger("AccountID", 0);
        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
        String AccountTypeCode = clientRequestObject.getString("AccountTypeCode", "");


        String AttachmentsYesNo = clientRequestObject.getString("AttachmentsYesNo", "No");

        String AssociatedToType = clientRequestObject.getString("AssociatedToType", "");

        String WhoOwnerCode = clientRequestObject.getString("WhoOwnerCode", "");
        Double DetailAmount = clientRequestObject.getDouble("DetailAmount", 0d);

        String BranchCode = clientRequestObject.getString("BranchCode", "");
        String IssuingCompanyLookupCode = clientRequestObject.getString("IssuingCompanyLookupCode", "");
        String ActivityCode = clientRequestObject.getString("ActivityCode", "");
        String TaskDescription = clientRequestObject.getString("TaskDescription", "");
        String ActivityDescription = clientRequestObject.getString("ActivityDescription", "");
        int AssociatedToID = clientRequestObject.getInteger("AssociatedToID", 0);

        String Status = clientRequestObject.getString("Status", "");
        String DueDate = clientRequestObject.getString("DueDate", "");

        String StartDate = clientRequestObject.getString("StartDate", "");

        // note

        String AccessLevel = clientRequestObject.getString("AccessLevel", "");

        String NoteText = clientRequestObject.getString("NoteText", "");

        String StatusOption = clientRequestObject.getString("StatusOption", "Open");
        int StatusOptionValue = clientRequestObject.getInteger("StatusOptionValue", 0);
        String closedStatus = clientRequestObject.getString("ClosedStatus", "");

        activityInsertObject.setAccountID(AccountID);
        activityInsertObject.setDescription((ActivityDescription));
        activityInsertObject.setActivityCode((ActivityCode));
        activityInsertObject.setAccountTypeCode((AccountTypeCode));

        activityInsertObject.setAssociatedToType((AssociatedToType)); // for attachment
        activityInsertObject.setAssociatedToID(AssociatedToID);

        //to set activity status options
        OptionType option = new OptionType();
            option.setOptionName(StatusOption);
            option.setValue(StatusOptionValue);

        CloseDetail closeDetail = new CloseDetail();
        closeDetail.setClosedStatus(closedStatus);
        closeDetail.setIgnoreOpenTasks(true);
        activityInsertObject.setStatusOption(option);
        activityInsertObject.setCloseDetailValue(closeDetail);
        activityInsertObject.setWhoOwnerCode((WhoOwnerCode));
        Detail detail = new Detail();
        activityInsertObject.setAttachmentsYesNo(AttachmentsYesNo);
        detail.setAmount((BigDecimal.valueOf(DetailAmount)));
        activityInsertObject.setDetailValue((detail));
        detail.setIssuingCompanyLookupCode((IssuingCompanyLookupCode));
        NoteItems noteItems=new NoteItems();
        NoteItem noteItem=new NoteItem();
        noteItem.setNoteText(NoteText);
        noteItem.setAccessLevel(AccessLevel);
        noteItem.setFlag(Flags.INSERT);
        noteItems.getNoteItems().add(noteItem);
        detail.setNotes(noteItems);
        TaskItems taskItems = new TaskItems();
        TaskItem taskItem = new TaskItem();

        taskItem.setDescription((TaskDescription));
        taskItem.setStatus((Status));
        XMLGregorianCalendar gregorianStartDate = createGregorianDate(StartDate);
        if (gregorianStartDate != null) {
            taskItem.setDueDate((gregorianStartDate));
            taskItem.setDueTime((gregorianStartDate));
            detail.setFollowUpStartDate(gregorianStartDate);
            detail.setFollowUpStartTime((gregorianStartDate));
        }


        XMLGregorianCalendar gregorianDueDate = createGregorianDate(DueDate);
        if (gregorianDueDate != null) {

            taskItem.setDueDate((gregorianDueDate));
            taskItem.setDueTime((gregorianDueDate));
        }
        taskItems.getTaskItems().add(taskItem);
        activityInsertObject.setDetailValue((detail));
        activityInsertObject.setTasks(taskItems);
        activityInsertObject.setAgencyCode((AgencyCode));
        activityInsertObject.setBranchCode((BranchCode));
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        return EpicCoreInstance.getInstance().getServiceBindingCore().insertActivity(activityInsertObject, messageHeader);
    }
    public static List<Integer> addAttachment(JsonObject clientRequestObject)
            throws EpicSDKCore201901InsertActivityConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertActivityMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertActivityInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertActivityAuthenticationFaultFaultFaultMessage, EpicSDKCore201901InsertAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertAttachmentMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertAttachmentAuthenticationFaultFaultFaultMessage, EpicSDKCore201901InsertAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore201901UpdateAttachmentBinaryMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateAttachmentBinaryConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdateAttachmentBinaryInputValidationFaultFaultFaultMessage, EpicSDKCore201901UpdateAttachmentBinaryAuthenticationFaultFaultFaultMessage, MalformedURLException, EpicSDKFileTransferUploadAttachmentFileConcurrencyFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileMethodCallFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileAuthenticationFaultFaultFaultMessage, EpicSDKFileTransferUploadAttachmentFileInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetAttachmentAuthenticationFaultFaultFaultMessage {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);

        Attachment attachmentInsertObject = new Attachment();
//        attachmentInsertObject.setAgencyStructures();
        int AccountID = clientRequestObject.getInteger("AccountID", 0);
        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
        String BranchCode = clientRequestObject.getString("BranchCode", "");
        String DepartmentCode = clientRequestObject.getString("DepartmentCode", "");

        JsonArray fileList = new JsonArray("[]");
        fileList = clientRequestObject.getJsonArray("files", fileList);
        if (fileList.isEmpty()) {
            throw new Error("files not found");
        }
        String AccountTypeCode = clientRequestObject.getString("AccountTypeCode", "");

        String Folder= clientRequestObject.getString("Folder", "");

        String Description = clientRequestObject.getString("Description", "");
        attachmentInsertObject.setAccountID(AccountID);
        attachmentInsertObject.setDescription((Description));
        AgencyStructureItem agencyStructureItem =new AgencyStructureItem();
        agencyStructureItem.setAgencyCode((AgencyCode));
        agencyStructureItem.setBranchCode((BranchCode));
        agencyStructureItem.setDepartmentCode((DepartmentCode));


        AgencyStructureItems agencyStructureItems =new AgencyStructureItems();
        agencyStructureItems.getAgencyStructureItems().add(agencyStructureItem);
        attachmentInsertObject.setAgencyStructures((agencyStructureItems));
        attachmentInsertObject.setAccountTypeCode((AccountTypeCode) );
        FileDetailItems fileDetailItems = attachmentObjectFactory.createFileDetailItems();
        for ( int i = 0; i < fileList.getList().toArray().length; i++) {
            FileDetailItem fileDetailItem = attachmentObjectFactory.createFileDetailItem();

            JsonObject fileObject = fileList.getJsonObject(i);
            String url = fileObject.getString("url", "");
            String name = fileObject.getString("name", "");

            if (!name.isEmpty()) {
                String[] ext = name.split("\\.(?=[^\\.]+$)");
                fileDetailItem.setFileName((ext[0]));

                fileDetailItem.setExtension((ext[ext.length - 1]));
            }
            byte[] fileBuffer;
            try {
                fileBuffer = downloadUrl(url  )
                        ;
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            String ticketName = EpicCoreInstance.getInstance().getFileTransferServiceBindingCore().uploadAttachmentFile(fileBuffer, messageHeader);
            fileDetailItem.setTicketName((ticketName));
            assert fileBuffer != null;
            fileDetailItem.setLength(fileBuffer.length);
            fileDetailItems.getFileDetailItems().add(fileDetailItem);

        }
        AttachedToItem attachedToItem =new AttachedToItem();
        AttachedToItems attachedToItems =new AttachedToItems();
attachedToItem.setAttachedToID(AccountID);
        attachedToItem.setAttachedToType(("Account"));
        attachedToItems.getAttachedToItems().add(attachedToItem);
        attachmentInsertObject.setClientAccessible(true);
        attachmentInsertObject.setAttachedTos((attachedToItems));

        attachmentInsertObject.setFolder((Folder));

//         EpicCoreInstance.getInstance().getServiceBindingCore().getAttachment(attachmentFilterObject,attachmentSorting,1,1, messageHeader).getInt();

        attachmentInsertObject.setFiles((fileDetailItems));
        return EpicCoreInstance.getInstance().getServiceBindingCore().insertAttachment(attachmentInsertObject, messageHeader).getInt();
    }


    private static XMLGregorianCalendar createGregorianDate(String Date) {
        String isoDateString = "2023-12-25T12:34:56+05:30";
        DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;

        ZonedDateTime zdt = ZonedDateTime.parse(Date, formatter);
        GregorianCalendar gc = GregorianCalendar.from(zdt);
        DatatypeFactory datatypeFactory = null;
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
        XMLGregorianCalendar xmlGregorianCalendar = datatypeFactory.newXMLGregorianCalendar(gc);

        return xmlGregorianCalendar;
    }


//    public static Client updateCLient(JsonObject clientRequestObject)
//            throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901UpdateClientInputValidationFaultFaultFaultMessage {
//        com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory clientObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();
//        com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory AccountObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory();
//        ObjectFactory sharedObjectFactory = new ObjectFactory();
//
//
//        boolean IsPersonal = clientRequestObject.getBoolean("IsPersonal", false);
//        String AccountName = clientRequestObject.getString("AccountName", "");
//        String PrimaryContactLast = clientRequestObject.getString("PrimaryContactLast", "");
//        String PrimaryContactEmail = clientRequestObject.getString("PrimaryContactEmail", "");
//        String PrimaryContactEmailDescription = clientRequestObject.getString("PrimaryContactEmailDescription", "");
//        String AccountEmail = clientRequestObject.getString("AccountEmail", "");
//        String EmployeeLookupCode = clientRequestObject.getString("EmployeeLookupCode", "");
//        int ClientID = clientRequestObject.getInteger("ClientID", 0);
//        String ClientLookupCode = clientRequestObject.getString("ClientLookupCode", "");
//        Client client = getClient(ClientID);
//        JsonObject addressDefaultField = createNullAddressJSONObject();
//        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
//        String City = address.getString("City");
//        String CountryCode = address.getString("CountryCode");
//        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
//        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
//        String County = address.getString("County");
//        String Street1 = address.getString("Street1");
//        String Street2 = address.getString("Street2");
//        String Street3 = address.getString("Street3");
//
//        String PrimaryContactFirst = clientRequestObject.getString("PrimaryContactFirst", "");
//        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
//        String BranchCode = clientRequestObject.getString("BranchCode", "");
//        client.setIsPersonal(IsPersonal);
//        client.setClientID(ClientID);
//        client.setAccountName(clientObjectFactory.createClientAccountName(AccountName));
//        AgencyStructureItem structureItem = createAgencyStructureItem(AgencyCode, BranchCode);
//        AgencyStructureItems structureItems = new AgencyStructureItems();
//        client.setPrimaryContactFirst(clientObjectFactory.createClientPrimaryContactFirst(PrimaryContactFirst));
//        client.setPrimaryContactLast(clientObjectFactory.createClientPrimaryContactLast(PrimaryContactLast));
//        client.setPrimaryContactEmail(clientObjectFactory.createClientPrimaryContactEmail(PrimaryContactEmail));
//        structureItems.getAgencyStructureItem().add(structureItem);
//        Account account = new Account();
//        com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory agencyStructureObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory();
//        ServicingRoleItem servicingRoleItem = agencyStructureObjectFactory.createServicingRoleItem();
//
//        client.setClientLookupCode(clientObjectFactory.createClientClientLookupCode(ClientLookupCode));
//
//        servicingRoleItem.setEmployeeLookupCode(
//                agencyStructureObjectFactory.createServicingRoleItemEmployeeLookupCode(EmployeeLookupCode));
////        servicingRoleItem.setRole( );
//        ServicingRolesItems servicingRolesItems = client.getServicingContacts().getValue();
//
//        servicingRolesItems.getServicingRoleItem().add(servicingRoleItem);
////        client.setServicingContacts(        agencyStructureObjectFactory.createServicingRolesItems(servicingRolesItems) );
//        account.setAccountEmail(AccountObjectFactory.createAccountAccountEmail(AccountEmail));
//        account.setStructure(AccountObjectFactory.createAccountStructure(structureItems));
////         new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory().createClientAccountValue(account);
//        Address accountAddress = createAddress(AccountObjectFactory, sharedObjectFactory,CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
//
//
//        client.setAccountValue(new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory().createClientAccountValue(account));
//
//        EpicCoreInstance.getInstance().getServiceBindingCore().updateClient(client, messageHeader);
//        return client;
//    }

    private static byte[] downloadUrl(String toDownload) throws UnsupportedEncodingException {
        byte[] fileByteArray = new ByteArrayOutputStream().toByteArray();

        try {

//            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            URL escaped = new URL(escapeHtml4(String.valueOf(toDownload)));


            HttpURLConnection connection = (HttpURLConnection) escaped.openConnection();

            // Set the request method (GET, POST, etc.)
            connection.setRequestMethod("GET");
            HttpClient client = HttpClients.createDefault();

            HttpGet httpGet = new HttpGet(escaped.toURI());
            HttpResponse response;
            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                if (entity != null) {

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            // Connect to the server
            connection.connect();

            // Check if the connection is successful
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // Get the input stream from the connection
                InputStream inputStream = connection.getInputStream();

                // Create a ByteArrayOutputStream to hold the data
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                // Buffer for reading the input stream
                byte[] buffer = new byte[1024];
                int bytesRead;

                // Read the input stream and write to the ByteArrayOutputStream
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                }

                // Convert the ByteArrayOutputStream to a byte array
                fileByteArray= byteArrayOutputStream.toByteArray();

                // Close the input stream
                inputStream.close();

                // Disconnect the connection
                connection.disconnect();

                // Use the byte array as needed
                System.out.println("Byte array length: " + fileByteArray.length);
            } else {
                System.out.println("Failed to connect: " + connection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        return fileByteArray;
    }
}
