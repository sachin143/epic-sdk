package com.insuredmine.activity.service;

import com.appliedsystems.schemas.epic.sdk._2009._07._account.Attachment;

public class  AttachmentData{
    Attachment attachment;

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public byte[] getFileBuffer() {
        return fileBuffer;
    }

    public void setFileBuffer(byte[] fileBuffer) {
        this.fileBuffer = fileBuffer;
    }

    byte[] fileBuffer;
}