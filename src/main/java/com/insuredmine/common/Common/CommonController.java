package com.insuredmine.common.Common;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.insuredmine.ResponseBase;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.codec.binary.Base64;
import sun.net.www.http.HttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;

import static com.insuredmine.Main.CHILD_PORT;
import static com.insuredmine.Main.CHILD_SERVER_URL;
import static com.insuredmine.policy.controller.PolicyController.sendError;


public class CommonController {
    public void getVehicles(RoutingContext routingContext) {
        String lineID = routingContext.request().getParam("lineID");
        HttpServerResponse response = routingContext.response();
        ObjectMapper objectMapper = new ObjectMapper();

        try{

        URL url = new URL(String.format(CHILD_SERVER_URL+"vehicleGet/"+lineID));
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            try (Response response1 = client.newCall(request).execute()) {
                if (response1.isSuccessful() ) {
                String data=    Objects.requireNonNull(response1.body()).string();
                    System.out.println(data);
                    ResponseBase responseBase = new ResponseBase();
            responseBase.setStatus(200);
//                clientContactResponse
            responseBase.setMessage("Policy updated successfully.");
            responseBase.setData(objectMapper.readValue(data,String.class));
            response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(responseBase));

                }else {
                    sendError(400, response, objectMapper.writeValueAsString(response1.body()));
                }
            }

//        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
//            // Get the input stream from the connection
//            InputStream inputStream = conn.getInputStream();
//
//            // Create a ByteArrayOutputStream to hold the data
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//
//            // Buffer for reading the input stream
//            byte[] buffer = new byte[1024];
//            int bytesRead;
//
//            // Read the input stream and write to the ByteArrayOutputStream
//            while ((bytesRead = inputStream.read(buffer)) != -1) {
//                byteArrayOutputStream.write(buffer, 0, bytesRead);
//            }
//
//            // Convert the ByteArrayOutputStream to a byte array
//
//            byte[] fileByteArray = byteArrayOutputStream.toByteArray();
//
//            // Close the input stream
//            inputStream.close();
//
//            // Disconnect the connection
//            conn.disconnect();
//            // Use the byte array as needed
//            ResponseBase responseBase = new ResponseBase();
//            responseBase.setStatus(200);
////                clientContactResponse
//            responseBase.setMessage("Policy updated successfully.");
//            responseBase.setData(objectMapper.readValue(fileByteArray,String.class));
//            response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(responseBase));
//
//        } else {
//            System.out.println("Failed to connect: " + conn.getResponseCode());
//        }
    }catch (Exception err){
            sendError(400, response, err.getMessage());
        }
    }
}
