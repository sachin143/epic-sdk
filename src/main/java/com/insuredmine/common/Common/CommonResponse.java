package com.insuredmine.common.Common;

import com.appliedsystems.schemas.epic.sdk._2009._07._common.Lookup;
import com.insuredmine.ResponseBase;

import java.util.List;

public class CommonResponse extends ResponseBase {
    public List<Lookup> getLookup() {
        return lookup;
    }

    public void setLookup(List<Lookup> lookup) {
        this.lookup = lookup;
    }
    private List<Lookup> lookup;
}
