package com.insuredmine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Objects;

public class Main {
    private static final Logger mainLogger = LogManager.getLogger(Main.class);
    public static int PORT=0;
    public static int CHILD_PORT=0;

    public static String DatabaseName="";
    public static String AuthenticationKey="";
    public static  String CHILD_SERVER_URL;


    public static void main(String[] args) throws Exception {

        if(args.length>=4){
if(!Objects.equals(args[0], "")){
    PORT= Integer.parseInt(args[0]);
}
            if(!Objects.equals(args[1], "")){
                DatabaseName= (args[1]);
            }
            if(!Objects.equals(args[2], "")){
                AuthenticationKey= (args[2]);
            }

            if(!Objects.equals(args[3], "")){
                CHILD_PORT= Integer.parseInt((args[3]));
               CHILD_SERVER_URL  = String.format("http://localhost:%s/epic/api/v2/", CHILD_PORT);
            }
            System.out.println("port: "+PORT);
            System.out.println("DatabaseName: "+DatabaseName);
            System.out.println("AuthenticationKey: "+AuthenticationKey);
            System.out.println("CHILD_PORT: "+CHILD_PORT);

        }else {
            System.out.println("argument missing app exiting");
            System.exit(1);
        };
        System.setProperty("http.agent", "Chrome");
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");
        mainLogger.info("server starting");
//            Connection.getConnection().getDatabase("insuredmine").getCollection("agencies").find().forEach( s-> System.out.println(s));
        new Server().start();
//        EpicCoreInstance.getInstance().getServiceBindingCore1();
        mainLogger.info("server started");

    }
}




