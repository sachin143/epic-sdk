package com.insuredmine.client.service;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.ArrayOfClient;
import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._client.Account;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.AgencyStructureItem;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.AgencyStructureItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ServicingRoleItem;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ServicingRolesItems;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._common._agencystructureitem.Flags;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ClientGetResult;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.insuredmine.EpicCoreInstance;
import com.insuredmine.Main;
import io.vertx.core.json.JsonObject;

import javax.xml.bind.JAXBElement;

public class ClientService {
    static final String DATABASENAME = Main.DatabaseName;
    static final String AUTHENTICATIONKEY = Main.AuthenticationKey;

    public static com.appliedsystems.schemas.epic.sdk._2009._07._account.Client getClient(int clientID) throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage {

        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);

        ClientFilter clientFilter = new ClientFilter();
        clientFilter.setClientID((clientID));
        ClientGetResult clientGetResult = EpicCoreInstance.getInstance().getServiceBindingCore().getClient(clientFilter, 0, messageHeader);
        ArrayOfClient clientsList = clientGetResult.getClients();

        return clientsList.getClients().get(0);

    }

    public static com.appliedsystems.schemas.epic.sdk._2009._07._account.Client getClientByLookup(String lookupCode) throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage {

        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);

        ClientFilter clientFilter = new ClientFilter();
        clientFilter.setLookupCode(lookupCode);
        ClientGetResult clientGetResult = EpicCoreInstance.getInstance().getServiceBindingCore().getClient(clientFilter, 0, messageHeader);
        ArrayOfClient clientsList = clientGetResult.getClients();

        return clientsList.getClients().get(0);

    }

    public static int addClient(JsonObject clientRequestObject)
            throws EpicSDKCore201901InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage {
        com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory clientObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();
        com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory AccountObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory();
        ObjectFactory sharedObjectFactory = new ObjectFactory();
        boolean IsPersonal = clientRequestObject.getBoolean("IsPersonal", false);
        boolean IsCommercial = clientRequestObject.getBoolean("IsCommercial", false);
        String AccountName = clientRequestObject.getString("AccountName", "");
        String PrimaryContactLast = clientRequestObject.getString("PrimaryContactLast", "");
        String PrimaryContactEmail = clientRequestObject.getString("PrimaryContactEmail", "");
        String PrimaryContactEmailDescription = clientRequestObject.getString("PrimaryContactEmailDescription", "");
        String AccountEmail = clientRequestObject.getString("AccountEmail", "");
        String EmployeeLookupCode = clientRequestObject.getString("EmployeeLookupCode", "");

        JsonObject addressDefaultField = createNullAddressJSONObject();
        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
        String City = address.getString("City");
        String CountryCode = address.getString("CountryCode");
        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
        String County = address.getString("County");
        String Street1 = address.getString("Street1");
        String Street2 = address.getString("Street2");
        String Street3 = address.getString("Street3");


        String PrimaryContactFirst = clientRequestObject.getString("PrimaryContactFirst", "");
        String PrimaryContactMiddle = clientRequestObject.getString("PrimaryContactMiddle", "");

        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
        String BranchCode = clientRequestObject.getString("BranchCode", "");
        com.appliedsystems.schemas.epic.sdk._2009._07._account.Client client = clientObjectFactory.createClient();
        client.setIsPersonal(IsPersonal);
        client.setIsCommercial(IsCommercial);

        if(IsCommercial){
            OptionType option =new OptionType();
            option.setOptionName("Business");
            option.setValue(1);
            client.setFormatOption(option);
            client.setBusinessName(AccountName);
        }

        client.setAccountName((AccountName));
        AgencyStructureItem structureItem = createAgencyStructureItem(AgencyCode, BranchCode);
        AgencyStructureItems structureItems = new AgencyStructureItems();
        client.setPrimaryContactFirst((PrimaryContactFirst));
        client.setPrimaryContactLast((PrimaryContactLast));
        client.setPrimaryContactMiddle((PrimaryContactMiddle));
        client.setPrimaryContactEmailDescription((PrimaryContactEmailDescription));

        client.setPrimaryContactEmail((PrimaryContactEmail));
        com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory agencyStructureObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory();
        ServicingRoleItem servicingRoleItem =new ServicingRoleItem();
        servicingRoleItem.setEmployeeLookupCode((EmployeeLookupCode));
        servicingRoleItem.setRole(("Client Service Rep"));
//        servicingRoleItem.setDescription(agencyStructureObjectFactory.createServicingRoleItemDescription("sachin"));
        ServicingRolesItems servicingRolesItems = new ServicingRolesItems();
        servicingRolesItems.getServicingRoleItems().add(servicingRoleItem);
//        client.setServicingContacts((servicingRolesItems));
        structureItems.getAgencyStructureItems().add(structureItem);
        Account account = new Account();
        account.setAccountEmail((AccountEmail));
        account.setStructure((structureItems));
        Address accountAddress = createAddress(AccountObjectFactory, sharedObjectFactory, CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
        account.setAddress((accountAddress));
        client.setAccountValue((account));
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        return EpicCoreInstance.getInstance().getServiceBindingCore().insertClient(client, messageHeader);
    }


    public static Client updateCLient(JsonObject clientRequestObject,int ClientID)
            throws EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901UpdateClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdateClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901UpdateClientInputValidationFaultFaultFaultMessage {
        com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory clientObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();
        com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory AccountObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory();
        ObjectFactory sharedObjectFactory = new ObjectFactory();


        boolean IsPersonal = clientRequestObject.getBoolean("IsPersonal", false);
        boolean IsCommercial = clientRequestObject.getBoolean("IsCommercial", false);
        String AccountName = clientRequestObject.getString("AccountName", "");
        String PrimaryContactLast = clientRequestObject.getString("PrimaryContactLast", "");
        String PrimaryContactEmail = clientRequestObject.getString("PrimaryContactEmail", "");
        String PrimaryContactEmailDescription = clientRequestObject.getString("PrimaryContactEmailDescription", "");
        String AccountEmail = clientRequestObject.getString("AccountEmail", "");
        String EmployeeLookupCode = clientRequestObject.getString("EmployeeLookupCode", "");
        String ClientLookupCode = clientRequestObject.getString("ClientLookupCode", "");
        Client client = getClient(ClientID);
        JsonObject addressDefaultField = createNullAddressJSONObject();
        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
        String City = address.getString("City");
        String CountryCode = address.getString("CountryCode");
        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
        String County = address.getString("County");
        String Street1 = address.getString("Street1");
        String Street2 = address.getString("Street2");
        String Street3 = address.getString("Street3");

        String PrimaryContactFirst = clientRequestObject.getString("PrimaryContactFirst", "");
        String AgencyCode = clientRequestObject.getString("AgencyCode", "");
        String BranchCode = clientRequestObject.getString("BranchCode", "");
        client.setIsPersonal(IsPersonal);
        client.setIsCommercial(IsCommercial);

        if(IsCommercial){
            OptionType option =new OptionType();
            option.setOptionName("Business");
            option.setValue(1);
            client.setFormatOption(option);
            client.setBusinessName(AccountName);
        }

        client.setClientID(ClientID);
        client.setAccountName((AccountName));
        AgencyStructureItem structureItem = createAgencyStructureItem(AgencyCode, BranchCode);
        AgencyStructureItems structureItems = client.getAccountValue().getStructure();
        client.setPrimaryContactFirst((PrimaryContactFirst));
        client.setPrimaryContactLast((PrimaryContactLast));
        client.setPrimaryContactEmail((PrimaryContactEmail));
        client.setPrimaryContactEmailDescription((PrimaryContactEmailDescription));


        structureItems.getAgencyStructureItems().add(structureItem);
        Account account = client.getAccountValue();
        com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory agencyStructureObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory();
        ServicingRoleItem servicingRoleItem = agencyStructureObjectFactory.createServicingRoleItem();

        client.setClientLookupCode((ClientLookupCode));

        servicingRoleItem.setEmployeeLookupCode(
                (EmployeeLookupCode));
//        servicingRoleItem.setRole( );
        ServicingRolesItems servicingRolesItems = client.getServicingContacts();

        client.setServicingContacts(servicingRolesItems);
//        client.setServicingContacts(        agencyStructureObjectFactory.createServicingRolesItems(servicingRolesItems) );
        account.setAccountEmail((AccountEmail));
        account.setStructure((structureItems));
//         new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory().createClientAccountValue(account);
        Address accountAddress = createAddress(AccountObjectFactory, sharedObjectFactory, CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
        account.setAddress((accountAddress));

        client.setAccountValue((account));
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        EpicCoreInstance.getInstance().getServiceBindingCore().updateClient(client, messageHeader);
        return client;
    }

    public static Address createAddress(com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory AccountObjectFactory, ObjectFactory sharedObjectFactory, String CountryCode, String City, String StateOrProvinceCode, String ZipOrPostalCode, String County, String Street1, String Street2, String Street3) {
        Address accountAddress = new Address();
        accountAddress.setCity((City));
        accountAddress.setCounty((County));
        accountAddress.setCountryCode((CountryCode));
        accountAddress.setZipOrPostalCode((ZipOrPostalCode));
        accountAddress.setStateOrProvinceCode((StateOrProvinceCode));
        accountAddress.setStreet1((Street1));
        accountAddress.setStreet2((Street2));
        accountAddress.setStreet3((Street3));
        return accountAddress;
    }

    public static JsonObject createNullAddressJSONObject() {
        return new JsonObject().putNull("Street3").putNull("Street2").putNull("Street1").putNull("County").putNull("City").putNull("CountryCode").putNull("StateOrProvinceCode").putNull("ZipOrPostalCode");
    }

    private static AgencyStructureItem createAgencyStructureItem(String AgencyCode, String BranchCode) {
        AgencyStructureItem structureItem = new AgencyStructureItem();
        com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory agencyStructureObjectFactory = new com.appliedsystems.schemas.epic.sdk._2009._07._account._common.ObjectFactory();
        structureItem.setAgencyCode((AgencyCode));
        structureItem.setBranchCode((BranchCode));
        structureItem.setFlag(Flags.VIEW);
        return structureItem;
    }


}
