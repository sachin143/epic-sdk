package com.insuredmine.client;

import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)

public class ClientResponse {

Client Client;
int ClientID;

    public boolean isError() {
        return Error;
    }

    public void setError(boolean error) {
        Error = error;
    }

    boolean Error;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    int Status;

ContactGetResult Contacts;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    String Message;

public  ClientResponse(){
}
    public ClientResponse(Client client, int clientID, ContactGetResult contacts) {
        this.Client = client;
        ClientID = clientID;
        this.Contacts = contacts;
    }

    public Client getClient() {
        return Client;
    }

    public void setClient(Client client) {
        this.Client = client;
    }

    public int getClientID() {
        return ClientID;
    }

    public void setClientID(int clientID) {
        ClientID = clientID;
    }

    public ContactGetResult getContacts() {
        return Contacts;
    }

    public void setContacts(ContactGetResult contacts) {
        this.Contacts = contacts;
    }
}
