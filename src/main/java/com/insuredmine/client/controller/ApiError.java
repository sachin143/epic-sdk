package com.insuredmine.client.controller;

public class ApiError {

    private int status;
    private String message;

    public boolean isError() {
        return IsError;
    }

    public void setError(boolean error) {
        IsError = error;
    }

    private boolean IsError=false;

    public ApiError(int status, String message,boolean isError) {
        this.status = status;
        this.setError(isError);
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
