package com.insuredmine.client.controller;

import com.appliedsystems.schemas.epic.sdk._2009._07._account.Client;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insuredmine.Main;
import com.insuredmine.client.ClientResponse;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.insuredmine.client.service.ClientService.*;

public class ClientController {
    private static final Logger clientLogger = LogManager.getLogger(Main.class);

    public void handleGetClient(RoutingContext routingContext) {
        String productID = routingContext.request().getParam("clientID");
        clientLogger.info(routingContext.request().absoluteURI());

        HttpServerResponse response = routingContext.response();
        if (productID == null) {
            sendError(400, response, "clientID Missing");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Client client = getClient(new Integer(productID));
                if (client == null) {
                    sendError(404, response, "Client Not Found.");
                } else {
                    ClientResponse clientContactResponse = new ClientResponse();
                    clientContactResponse.setClient(client);
                    clientContactResponse.setMessage("Client Get Successful.");
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientID(client.getClientID());
                    clientContactResponse.setContacts(null);
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            } catch (EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (JsonProcessingException e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (Exception e) {
                clientLogger.error(e);
                sendError(500, response, e.getMessage());
            }


        }
    }

    public void handleGetClientByLookupCode(RoutingContext routingContext) {
        String uniqueCode = routingContext.request().getParam("lookupCode");
        clientLogger.info(routingContext.request().absoluteURI());

        HttpServerResponse response = routingContext.response();
        if (uniqueCode == null) {
            sendError(400, response, "client lookup Missing");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Client client = getClientByLookup(uniqueCode);
                if (client == null) {
                    sendError(404, response, "Client Not Found.");
                } else {
                    ClientResponse clientContactResponse = new ClientResponse();
                    clientContactResponse.setClient(client);
                    clientContactResponse.setMessage("Client Get Successful.");
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientID(client.getClientID());
                    clientContactResponse.setContacts(null);
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            } catch (EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (JsonProcessingException e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage e) {
                clientLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (Exception e) {
                clientLogger.error(e);
                sendError(500, response, e.getMessage());
            }


        }
    }

    public void handleAddClient(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            int clientId = addClient(bodyObj);
            clientLogger.info(routingContext.request().absoluteURI());
            clientLogger.info(routingContext.request());
            if (clientId == 0) {
                sendError(404, response, "cannot create client.");
            } else {
                ClientResponse clientUpdateResponse = new ClientResponse();
                clientUpdateResponse.setClient(null);
                clientUpdateResponse.setStatus(200);
                clientUpdateResponse.setMessage("Client update successful.");
                clientUpdateResponse.setClientID(clientId);
                clientUpdateResponse.setContacts(null);
                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientUpdateResponse));
            }
        } catch (EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901InsertClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901InsertClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (JsonProcessingException e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        } catch (EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901InsertClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901InsertClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (Exception e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        }
    }

    public void handleUpdateClient(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        String clientID = routingContext.request().getParam("clientID");

        HttpServerResponse response = routingContext.response();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            clientLogger.info(routingContext.request().absoluteURI());
            clientLogger.info(bodyObj);

            Client updatedCLient = updateCLient(bodyObj, Integer.parseInt(clientID));
            ClientResponse clientUpdateResponse = new ClientResponse();
            clientUpdateResponse.setClient(updatedCLient);
            clientUpdateResponse.setMessage("Client Updated Successful.");
            clientUpdateResponse.setStatus(200);
            clientUpdateResponse.setClientID(updatedCLient.getClientID());
            clientUpdateResponse.setContacts(null);
            response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientUpdateResponse));
        } catch (EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901UpdateClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (JsonProcessingException e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901UpdateClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901UpdateClientAuthenticationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901UpdateClientInputValidationFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage e) {
            clientLogger.error(e);
            sendError(400, response, e.getMessage());
        } catch (Exception e) {
            clientLogger.error(e);
            sendError(500, response, e.getMessage());
        }

    }


    public void sendError(int statusCode, HttpServerResponse response, String errorMessage) {
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError error = new ApiError(statusCode, errorMessage, true);

        try {
            response.setStatusCode(statusCode).send(objectMapper.writeValueAsString(new ApiError(statusCode, errorMessage, true)));
        } catch (JsonProcessingException e) {
            response.setStatusCode(statusCode).send("Internal Server Error.");
        }
    }

}

