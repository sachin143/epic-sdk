package com.insuredmine.contact.service;
import com.appliedsystems.schemas.epic.sdk._2009._07.MessageHeader;
import com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.*;
import com.appliedsystems.schemas.epic.sdk._2009._07._common.OptionType;
import com.appliedsystems.schemas.epic.sdk._2009._07._get.ContactFilter;
import com.appliedsystems.schemas.epic.sdk._2009._07._shared.Address;
import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.BusinessInformation;
import com.appliedsystems.schemas.epic.sdk._2017._02._get.ContactGetResult;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.insuredmine.EpicCoreInstance;
import com.insuredmine.Main;
import com.insuredmine.client.service.ClientService;
import io.vertx.core.json.JsonObject;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.insuredmine.client.service.ClientService.createNullAddressJSONObject;

public class ClientContactService {
    static final String DATABASENAME = Main.DatabaseName;
    static final String AUTHENTICATIONKEY =
            Main.AuthenticationKey;
static final com.appliedsystems.schemas.epic.sdk._2009._07._account._contact.ObjectFactory ContactObjectFactory =new ObjectFactory();
static final com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory accountObjectFactory =new com.appliedsystems.schemas.epic.sdk._2017._02._account.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory accountObjectFactory07=new com.appliedsystems.schemas.epic.sdk._2009._07._account.ObjectFactory();
static final  com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory sharedObjectFactory =new com.appliedsystems.schemas.epic.sdk._2009._07._shared.ObjectFactory();
static  final  com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory clientFactory=new com.appliedsystems.schemas.epic.sdk._2009._07._account._client.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory ContactObjectFactory17=new com.appliedsystems.schemas.epic.sdk._2017._02._account._contact.ObjectFactory();
    static final com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory CommonObjectFactory= new com.appliedsystems.schemas.epic.sdk._2009._07._common.ObjectFactory();
    public static List
            <Contact> getClientContactByClientID(int clientId) throws EpicSDKCore201901GetContactConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetContactInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetContactMethodCallFaultFaultFaultMessage, EpicSDKCore201901GetContactAuthenticationFaultFaultFaultMessage {
        MessageHeader messageHeader= getMessageHeader();
        ContactFilter contactFilterObject =new ContactFilter();
        contactFilterObject.setAccountTypeCode(("CUST"));
        contactFilterObject.setAccountID((clientId));

        ContactGetResult contact;
            contact = EpicCoreInstance.getInstance().getServiceBindingCore().getContact(contactFilterObject,0, messageHeader);
    return contact.getContacts().getContacts();
    }
    public static Contact getClientContactByContactID(int contactId) {
        MessageHeader messageHeader= getMessageHeader();
        ContactFilter contactFilterObject =new ContactFilter();
//        contactFilterObject.setContactID(new com.appliedsystems.schemas.epic.sdk._2009._07._get.ObjectFactory().createContactFilterContactID("CUST"));
        contactFilterObject.setContactID((contactId));
        contactFilterObject.setAccountTypeCode(("CUST"));
        ContactGetResult contact;
        try {
            contact = EpicCoreInstance.getInstance().getServiceBindingCore().getContact(contactFilterObject,0, messageHeader);
            return contact.getContacts().getContacts().get(0);
        } catch (EpicSDKCore201901GetContactAuthenticationFaultFaultFaultMessage e) {
            throw new RuntimeException(e);
        } catch (EpicSDKCore201901GetContactConcurrencyFaultFaultFaultMessage e) {
            throw new RuntimeException(e);
        } catch (EpicSDKCore201901GetContactInputValidationFaultFaultFaultMessage e) {
            throw new RuntimeException(e);
        } catch (EpicSDKCore201901GetContactMethodCallFaultFaultFaultMessage e) {
            throw new RuntimeException(e);
        }

    }

    private static MessageHeader getMessageHeader() {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setDatabaseName(DATABASENAME);
        messageHeader.setAuthenticationKey(AUTHENTICATIONKEY);
        return messageHeader;
    }

    public static int addClientContact(JsonObject clientRequestObject)
            throws EpicSDKCore201901InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage, EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage, EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage, EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage, EpicSDKCore201901InsertContactInputValidationFaultFaultFaultMessage, EpicSDKCore201901InsertContactConcurrencyFaultFaultFaultMessage, EpicSDKCore201901InsertContactAuthenticationFaultFaultFaultMessage, EpicSDKCore201901InsertContactMethodCallFaultFaultFaultMessage {
        Contact contactObject =new Contact();
        MessageHeader messageHeader= getMessageHeader();

        boolean PrimaryContact = clientRequestObject.getBoolean("PrimaryContact", false);
        int AccountID = clientRequestObject.getInteger("AccountID", 0);
        String Email1 = clientRequestObject.getString("Email1", "");
        String Email2 = clientRequestObject.getString("Email2", "");
        String Phone1Number = clientRequestObject.getString("Phone1Number", "");
        String Phone2Number = clientRequestObject.getString("Phone2Number", "");
        String Phone3Number = clientRequestObject.getString("Phone3Number", "");
        String Phone4Number = clientRequestObject.getString("Phone4Number", "");
        String Phone5Number = clientRequestObject.getString("Phone5Number", "");

        String Phone1Extension = clientRequestObject.getString("Phone1Extension", "");
        String Phone2Extension = clientRequestObject.getString("Phone2Extension", "");
        String Phone3Extension = clientRequestObject.getString("Phone3Extension", "");
        String Phone4Extension = clientRequestObject.getString("Phone4Extension", "");
        String Phone5Extension = clientRequestObject.getString("Phone5Extension", "");
        boolean MainBusinessContact = clientRequestObject.getBoolean("MainBusinessContact", false);
        String Website = clientRequestObject.getString("Website", "");
        String OptionName = clientRequestObject.getString("OptionName", "");
        String BusinessName = clientRequestObject.getString("BusinessName", "");
        int OptionValue = clientRequestObject.getInteger("OptionValue", 0);

        //Driver details

        String DriversLicenseNumber = clientRequestObject.getString("DriversLicenseNumber", "");

        // Employer details
        String EmployerName = clientRequestObject.getString("EmployerName", "");

        // PersonalClassifications details
        String Gender = clientRequestObject.getString("Gender", "");
        String DateOfBirth = clientRequestObject.getString("DateOfBirth", "");
        XMLGregorianCalendar gregorianDateOfBirth=null;

       if(DateOfBirth!="") {
           GregorianCalendar c = new GregorianCalendar();
           c.setTime(new Date(DateOfBirth));

            try {
                gregorianDateOfBirth = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            } catch (DatatypeConfigurationException e) {
                System.out.println(e.getMessage());
            }
        }
        //business information

        String NAICS = clientRequestObject.getString("NAICS", "");
        String SIC = clientRequestObject.getString("SIC", "");

        // contact information
        String FirstName = clientRequestObject.getString("FirstName", "");
        String LastName = clientRequestObject.getString("LastName", "");
        String MiddleName = clientRequestObject.getString("MiddleName", "");

        JsonObject addressDefaultField = createNullAddressJSONObject();
        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
        // address Information
        String City = address.getString("City");
        String CountryCode = address.getString("CountryCode");
        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
        String County = address.getString("County");
        String Street1 = address.getString("Street1");
        String Street2 = address.getString("Street2");
        String Street3 = address.getString("Street3");


        ContactInformation contactInfo = new ContactInformation();
        OptionType businessOption=new OptionType();
        BusinessInformation businessInformation=new BusinessInformation();
        Driver driver=new Driver();
        Employer employer =new Employer();
        PersonalClassifications personalClassifications =new PersonalClassifications();

        personalClassifications.setGender((Gender));
        if(gregorianDateOfBirth!=null){

            personalClassifications.setDateOfBirth((gregorianDateOfBirth));
        }
        employer.setEmployerName((EmployerName));
        driver.setDriversLicenseNumber((DriversLicenseNumber));
        contactInfo.setEmail1((Email1));
        contactInfo.setEmail2((Email2));
        contactInfo.setPhone1Number((Phone1Number));
        contactInfo.setPhone2Number((Phone2Number));
        contactInfo.setPhone3Number((Phone3Number));
        contactInfo.setPhone4Number((Phone4Number));
        contactInfo.setPhone5Number((Phone5Number));

        contactInfo.setPhone1Extension((Phone1Extension));
        contactInfo.setPhone2Extension((Phone2Extension));
        contactInfo.setPhone3Extension((Phone3Extension));
        contactInfo.setPhone4Extension((Phone4Extension));
        contactInfo.setPhone5Extension((Phone5Extension));

        contactInfo.setPhone1NumberType(("Residence"));
        contactInfo.setPhone2NumberType(("Business"));
        contactInfo.setPhone3NumberType(("Fax"));
        contactInfo.setPhone4NumberType(("Mobile"));
        contactInfo.setPhone5NumberType(("Other"));
        contactInfo.setServicingContactMethod(("Mail"));
        businessInformation.setNAICS((NAICS));
        businessInformation.setSIC((SIC));

        businessOption.setOptionName((OptionName));
        businessOption.setValue(OptionValue);

        contactInfo.setBillingContactMethod(("Mail"));
        contactInfo.setWebsite((Website));
        contactInfo.setContactVia(("Phone"));

        contactObject.setContactInfo((contactInfo));
        contactObject.setBusinessName((BusinessName));

        contactObject.setMainBusinessContact(MainBusinessContact);
        contactObject.setAccountID(AccountID);
        contactObject.setDriverValue((driver));
        contactObject.setEmployerValue((employer));
        contactObject.setPersonalClassificationsValue((personalClassifications));
        contactObject.setBusinessIndividualOption((businessOption));
        contactObject.setPrimaryContact(PrimaryContact);
        contactObject.setFirstName((FirstName));
        contactObject.setLastName((LastName));
        contactObject.setMiddleName((MiddleName));
        Address accountAddress= ClientService.createAddress(clientFactory ,sharedObjectFactory,CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
        contactObject.setAddress((accountAddress));
        contactObject.setAccountType(("CUST"));
        int clientId=EpicCoreInstance.getInstance().getServiceBindingCore().insertContact(contactObject, messageHeader);


        return clientId;
    }

    public static Contact updateClientContact(int contactId,JsonObject clientRequestObject)
            throws EpicSDKCore201901UpdateContactInputValidationFaultFaultFaultMessage, EpicSDKCore201901UpdateContactAuthenticationFaultFaultFaultMessage, EpicSDKCore201901UpdateContactConcurrencyFaultFaultFaultMessage, EpicSDKCore201901UpdateContactMethodCallFaultFaultFaultMessage {
        Contact contactObject =getClientContactByContactID(contactId);

        MessageHeader messageHeader= getMessageHeader();
//
        boolean PrimaryContact = clientRequestObject.getBoolean("PrimaryContact", false);
        int AccountID = clientRequestObject.getInteger("AccountID", 0);
        String Email1 = clientRequestObject.getString("Email1", "");
        String Email2 = clientRequestObject.getString("Email2", "");
        String Phone1Number = clientRequestObject.getString("Phone1Number", "");
        String Phone2Number = clientRequestObject.getString("Phone2Number", "");
        String Phone3Number = clientRequestObject.getString("Phone3Number", "");
        String Phone4Number = clientRequestObject.getString("Phone4Number", "");
        String Phone5Number = clientRequestObject.getString("Phone5Number", "");

        String Phone1Extension = clientRequestObject.getString("Phone1Extension", "");
        String Phone2Extension = clientRequestObject.getString("Phone2Extension", "");
        String Phone3Extension = clientRequestObject.getString("Phone3Extension", "");
        String Phone4Extension = clientRequestObject.getString("Phone4Extension", "");
        String Phone5Extension = clientRequestObject.getString("Phone5Extension", "");



        String Website = clientRequestObject.getString("Website", "");
        String OptionName = clientRequestObject.getString("OptionName", "");
        int OptionValue = clientRequestObject.getInteger("OptionValue", 0);

        String BusinessName = clientRequestObject.getString("BusinessName", "");
//
//        //Driver details
//
        String DriversLicenseNumber = clientRequestObject.getString("DriversLicenseNumber", "");
//
//        // Employer details
        String EmployerName = clientRequestObject.getString("EmployerName", "");
//
//        // PersonalClassifications details
        String Gender = clientRequestObject.getString("Gender", "Unknown");
        String DateOfBirth = clientRequestObject.getString("DateOfBirth", "");
        XMLGregorianCalendar gregorianDateOfBirth=null;

        if(DateOfBirth!="") {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date(DateOfBirth));

            try {
                gregorianDateOfBirth = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            } catch (DatatypeConfigurationException e) {
                System.out.println(e.getMessage());
            }
        }
//        //business information
//
        String NAICS = clientRequestObject.getString("NAICS", "");
        String SIC = clientRequestObject.getString("SIC", "");
//
//        // contact information
        String FirstName = clientRequestObject.getString("FirstName", "");
        String LastName = clientRequestObject.getString("LastName", "");
        String MiddleName = clientRequestObject.getString("MiddleName", "");

        JsonObject addressDefaultField = createNullAddressJSONObject();
        JsonObject address = clientRequestObject.getJsonObject("Address", addressDefaultField);
//        // address Information
        String City = address.getString("City");
        String CountryCode = address.getString("CountryCode");
        String StateOrProvinceCode = address.getString("StateOrProvinceCode");
        String ZipOrPostalCode = address.getString("ZipOrPostalCode");
        String County = address.getString("County");
        String Street1 = address.getString("Street1");
        String Street2 = address.getString("Street2");
        String Street3 = address.getString("Street3");
//
//
        ContactInformation contactInfo = contactObject.getContactInfo();
        OptionType businessOption=contactObject.getBusinessIndividualOption();
        BusinessInformation businessInformation=contactObject.getBusinessInfo();
        Driver driver=contactObject.getDriverValue();
        Employer employer =contactObject.getEmployerValue();
        PersonalClassifications personalClassifications = contactObject.getPersonalClassificationsValue();
        if(!Gender.isEmpty()) {
            personalClassifications.setGender((Gender));
        }

        if(gregorianDateOfBirth!=null){
            personalClassifications.setDateOfBirth((gregorianDateOfBirth));

        }
        employer.setEmployerName((EmployerName));
        driver.setDriversLicenseNumber((DriversLicenseNumber));
        contactInfo.setEmail1((Email1));
        contactInfo.setEmail2((Email2));
        contactInfo.setPhone1Number((Phone1Number));
        contactInfo.setPhone2Number((Phone2Number));
        contactInfo.setPhone3Number((Phone3Number));
        contactInfo.setPhone4Number((Phone4Number));
        contactInfo.setPhone5Number((Phone5Number));

        contactInfo.setPhone1Extension((Phone1Extension));
        contactInfo.setPhone2Extension((Phone2Extension));
        contactInfo.setPhone3Extension((Phone3Extension));
        contactInfo.setPhone4Extension((Phone4Extension));
        contactInfo.setPhone5Extension((Phone5Extension));

        contactInfo.setPhone1NumberType(("Residence"));
        contactInfo.setPhone2NumberType(("Business"));
        contactInfo.setPhone3NumberType(("Fax"));
        contactInfo.setPhone4NumberType(("Mobile"));
        contactInfo.setPhone5NumberType(("Other"));
        contactInfo.setServicingContactMethod(("Mail"));

        businessInformation.setNAICS((NAICS));
        businessInformation.setSIC((SIC));
//
        businessOption.setOptionName((OptionName));
        businessOption.setValue(OptionValue);
        contactInfo.setBillingContactMethod(("Mail"));
        contactInfo.setWebsite((Website));
//
////        if(contactInfo.getContactVia()==null){
////
////        }
          contactInfo.setContactVia(("Phone"));
        contactObject.setBusinessName((BusinessName));
//        contactObject.setAccountID(AccountID);
//        contactObject.setDriverValue((driver));
//        contactObject.setEmployerValue((employer));
//        contactObject.setPersonalClassificationsValue((personalClassifications));
//        contactObject.setBusinessIndividualOption((businessOption));
        contactObject.setPrimaryContact(PrimaryContact);
        contactObject.setFirstName((FirstName));
        contactObject.setLastName((LastName));
        contactObject.setMiddleName((MiddleName));
        Address accountAddress= ClientService.createAddress(clientFactory ,sharedObjectFactory,CountryCode, City, StateOrProvinceCode, ZipOrPostalCode, County, Street1, Street2, Street3);
        contactObject.setAddress(accountAddress);
        //        contactObject.setAddress(clientFactory.createAccountAddress(accountAddress));
        contactObject.setAccountType(("CUST"));

        contactObject.getContactInfo().setEmail1((Email1));
        contactObject.getContactInfo().setEmail2((Email2));

        EpicCoreInstance.getInstance().getServiceBindingCore().updateContact(contactObject, messageHeader);


        return contactObject;
    }


}
