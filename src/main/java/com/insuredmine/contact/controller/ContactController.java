package com.insuredmine.contact.controller;

import com.appliedsystems.schemas.epic.sdk._2017._02._account.Contact;
import com.appliedsystems.webservices.epic.sdk._2013._11.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insuredmine.ClientContactResponse;
import com.insuredmine.Main;
import com.insuredmine.client.controller.ApiError;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.insuredmine.contact.service.ClientContactService.*;

public class ContactController {
    private static final Logger contactLogger = LogManager.getLogger(Main.class);


    public void handleGetClientContacts(RoutingContext routingContext) {
        String clientId = routingContext.request().getParam("clientID");
        HttpServerResponse response = routingContext.response();
        if (clientId == null) {
            sendError(400, response, "ClientID Missing.");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                List<Contact> contacts = getClientContactByClientID(new Integer(clientId));
                if (contacts == null) {
                    sendError(200, response, "cannot find client contacts.");
                } else {
                    ClientContactResponse clientContactResponse = new ClientContactResponse();
                    clientContactResponse.setMessage("Client get by client ID successful.");

                    clientContactResponse.setClientId(new Integer(clientId));
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setContacts(contacts);
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            } catch (JsonProcessingException e) {
                contactLogger.error(e);
                sendError(500, response, e.getMessage());
            } catch (EpicSDKCore201901GetContactConcurrencyFaultFaultFaultMessage e) {
                contactLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetContactInputValidationFaultFaultFaultMessage e) {
                contactLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetContactMethodCallFaultFaultFaultMessage e) {
                contactLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (EpicSDKCore201901GetContactAuthenticationFaultFaultFaultMessage e) {
                contactLogger.error(e);
                sendError(400, response, e.getMessage());
            } catch (Exception e) {
                contactLogger.error(e);
                sendError(500, response, e.getMessage());

            }


        }
    }

    public void handleGetClientContactByID(RoutingContext routingContext) {
        String contactId = routingContext.request().getParam("clientID");
        HttpServerResponse response = routingContext.response();
        if (contactId == null) {
            sendError(400, response, "contact ID Missing");
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                Contact contact = getClientContactByContactID(new Integer(contactId));
                if (contact == null) {
                    sendError(404, response, "cannot find client contacts.");
                } else {

                    ClientContactResponse clientContactResponse = new ClientContactResponse();
                    clientContactResponse.setContactID(new Integer(contactId));
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientId(contact.getAccountID());
                    clientContactResponse.setContact(contact);
//                clientContactResponse
                    clientContactResponse.setMessage("Client get by Contact ID successfully.");

                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            } catch (Exception e) {
                sendError(400, response, e.getMessage());
            }


        }
    }

    public void handleAddClientContact(RoutingContext routingContext) {
        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {

            ObjectMapper objectMapper = new ObjectMapper();

            int contactId = addClientContact(bodyObj);
            if (contactId == 0) {
                sendError(404, response, "cannot find contact");
            } else {
                ClientContactResponse clientContactResponse = new ClientContactResponse();
                clientContactResponse.setContactID(contactId);
                clientContactResponse.setStatus(200);
//                clientContactResponse
                clientContactResponse.setMessage("Client contact created successfully.");
                response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
            }
        } catch (EpicSDKCore202102InsertClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());

        } catch (EpicSDKCore201901InsertClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102InsertClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202101GetClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102GetClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202202GetClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertContactInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202101GetClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901GetClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901GetClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901GetClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202202GetClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102InsertClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201InsertClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertContactAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (JsonProcessingException e) {
            sendError(500, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201InsertClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertContactMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202101GetClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102GetClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202202GetClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201InsertClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201GetClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201InsertClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertContactConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102GetClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102GetClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201GetClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202101GetClientInputValidationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201GetClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202201GetClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202202GetClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertClientAuthenticationFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore202102InsertClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901InsertClientConcurrencyFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);


        } catch (EpicSDKCore201901GetClientMethodCallFaultFaultFaultMessage e) {
            sendError(400, response, e.getMessage());
            contactLogger.error(e);

        }

    }

    public void handleupdateClientContact(RoutingContext routingContext) {
        String contactId = routingContext.request().getParam("clientID");


        JsonObject bodyObj = routingContext.body().asJsonObject();
        HttpServerResponse response = routingContext.response();
        try {

            ObjectMapper objectMapper = new ObjectMapper();
            if (contactId == null) {
                sendError(400, response, "contact ID Missing");
            } else {
                Contact contact = updateClientContact(new Integer(contactId), bodyObj);
                if (contact == null) {
                    sendError(404, response, "cannot find contact");
                } else {
                    ClientContactResponse clientContactResponse = new ClientContactResponse();
                    clientContactResponse.setContactID(contact.getContactID());
                    clientContactResponse.setStatus(200);
                    clientContactResponse.setClientId(contact.getAccountID());
                    clientContactResponse.setContact(contact);
//                clientContactResponse
                    clientContactResponse.setMessage("Client contact created successfully.");
                    response.putHeader("content-type", "application/json").end(objectMapper.writeValueAsString(clientContactResponse));
                }
            }
        } catch (Exception e) {
            sendError(400, response, e.getMessage());
            ;
        }

    }


    public void sendError(int statusCode, HttpServerResponse response, String errorMessage) {
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError error = new ApiError(statusCode, errorMessage, true);

        try {
            response.setStatusCode(statusCode).send(objectMapper.writeValueAsString(error));
        } catch (JsonProcessingException e) {
            response.setStatusCode(statusCode).send("Internal Server Error.");
        }
    }
}

